  MEMBER

  INCLUDE('absql.inc'),ONCE
  INCLUDE('abfile.inc'),ONCE
  INCLUDE('dynstr.inc'),ONCE


  MAP
ExecuteSqlStatement PROCEDURE(STRING sqlCode, FILE sqlTable, ErrorClass errorHandler),BYTE,PRIVATE
  END

TextFile            FILE,DRIVER('ASCII')
record                RECORD
line                    STRING(1000)
                      END
                    END
Access:TextFile     CLASS(FileManager)
Init                  PROCEDURE(ErrorClass errorHandler)
                    END

StatementList       QUEUE,TYPE
statement             &STRING
                    END

!region Exposed Functions
ExecuteSql          PROCEDURE(STRING sqlCode, <STRING eosMarker>, FILE sqlTable, ErrorClass errorHandler)
ds                    &IDynStr
returnCode            BYTE
i                     LONG(1)
eosLen                LONG,AUTO
strLen                LONG,AUTO
  CODE
  IF OMITTED(eosMarker)
    RETURN ExecuteSqlStatement(sqlCode, sqlTable, errorHandler)
  END
  eosLen = LEN(eosMarker) - 1
  strLen = LEN(CLIP(sqlCode))
  ds &= NewDynStr()
  LOOP i = 1 TO strlen + 1
    IF i = strlen + 1 OR (i < strlen - eosLen AND sqlCode[i : i + eosLen] = eosMarker)
      returnCode = ExecuteSqlStatement(ds.Str(), sqlTable, errorHandler)
      IF returnCode <> Level:Benign
        BREAK
      END
      ds.kill()
      i += eosLen
    ELSE
      ds.cat(sqlCode[i])
    END
  END
  DisposeDynStr(ds)
  RETURN returnCode
  
ExecuteSqlScript    PROCEDURE(STRING sqlFile, <STRING eosMarker>, FILE sqlTable, ErrorClass errorHandler)
ds                    &IDynStr
returnCode            BYTE
  CODE
  TextFile{PROP:Name} = sqlFile
  Access:TextFile.Init(errorHandler)
  returnCode = Access:TextFile.Open()
  IF returnCode = Level:Benign
    ds &= NewDynStr()
    SET(TextFile)
    LOOP
      IF Access:TextFile.Next() <> Level:Benign THEN BREAK .
      ds.cat(CLIP(TextFile.line))
    END
    IF OMITTED(eosMarker)
      returnCode = ExecuteSqlStatement(ds.Str(), sqlTable, errorHandler)
    ELSE
      returnCode = ExecuteSql(ds.Str(), eosMarker, sqlTable, errorHandler)
    END
    DisposeDynStr(ds)
    Access:TextFile.Close()
  END
  RETURN returnCode

ExecuteSqlStatement PROCEDURE(STRING sqlCode, FILE sqlTable, ErrorClass errorHandler)
  CODE
  sqlTable{PROP:SQL} = sqlCode
  IF ERRORCODE()
    errorHandler.ThrowMessage(Msg:ExecutionOfSqlFailed, sqlCode)
    RETURN Level:Notify
  END
  RETURN Level:Benign
!endregion
!region SQLExecutor
SQLExecutor.Destruct    PROCEDURE()
  CODE
  IF NOT SELF.statements &= NULL
    LOOP
      GET(SELF.statements, 1)
      IF ERRORCODE()
        BREAK
      END
      DISPOSE(SELF.statements.statement)
      DELETE(SELF.statements)
    END
    DISPOSE(SELF.statements)
  END
  IF NOT SELF.delimiter &= NULL
    DISPOSE(SELF.delimiter)
  END
  
SQLExecutor.Init    PROCEDURE(ErrorClass errorHandler, FILE sqlTable, <STRING delimiter>)
  CODE
  SELF.errorHandler &= errorHandler
  SELF.f &= sqlTable
  IF NOT OMITTED(delimiter)
    SELF.delimiter &= NEW STRING(SIZE(delimiter))
    SELF.delimiter = delimiter
  END
  
SQLExecutor.Load    PROCEDURE(STRING fileName)
ds                    &IDynStr
returnCode            BYTE
strlen                LONG,AUTO
dLen                  LONG,AUTO
  CODE
  IF SELF.errorHandler &= NULL
    RETURN Level:Program
  END
  SELF.statements &= NEW StatementList
  TextFile{PROP:Name} = fileName
  Access:TextFile.Init(SELF.errorHandler)
  returnCode = Access:TextFile.Open()
  IF returnCode = Level:Benign
    ds &= NewDynStr()
    IF SELF.delimiter &= NULL
      dLen = 0
    ELSE
      dLen = LEN(SELF.delimiter)
    END
    SET(TextFile)
    LOOP
      IF Access:TextFile.Next() <> Level:Benign THEN BREAK .
      strlen = LEN(CLIP(TextFile.line))
      IF SELF.delimiter &= NULL OR SUB(TextFile.line, strlen - dLen + 1, dLen) = SELF.delimiter
        ds.cat(SUB(TextFile.line, 1, strlen - dLen))
        SELF.statements.statement &= NEW STRING(ds.StrLen())
        SELF.statements.statement = ds.Str()
        ds.Kill()
        ADD(SELF.statements)
      ELSE
        ds.cat(CLIP(TextFile.line))
      END
    END
    IF ds.StrLen() > 0
      SELF.statements.statement &= NEW STRING(ds.StrLen())
      SELF.statements.statement = ds.Str()
      ADD(SELF.statements)
    END
    DisposeDynStr(ds)
    Access:TextFile.Close()
  END
  RETURN returnCode
  
SQLExecutor.ExecuteStatement    PROCEDURE(LONG statementNumber)
  CODE
  IF SELF.statements &= NULL
    RETURN Level:Program
  END
  GET(SELF.statements, statementNumber)
  IF ERRORCODE()
    SELF.errorHandler.ThrowMessage(Msg:FetchOfSqlFailed, statementNumber)
  END
  RETURN ExecuteSqlStatement(SELF.statements.statement, SELF.f, SELF.errorHandler)

SQLExecutor.StatementCount  PROCEDURE()
  CODE
  IF SELF.statements &= NULL
    RETURN 0
  END
  RETURN RECORDS(SELF.statements)
!endregion

Access:TextFile.Init    PROCEDURE(ErrorClass errorHandler)
  CODE
  SELF.Buffer &= textFile.record
  SELF.Init(TextFile, errorHandler)
  SELF.Init()
  SELF.LazyOpen = FALSE

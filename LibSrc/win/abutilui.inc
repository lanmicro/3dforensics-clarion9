!!
!! ABUTILUI.INC - user interface structures and controls for popup calendar
!!
!! CAUTION - Do NOT rename or remove variables or screen controls
!!
!! Window structure for the Standard popup Calendar
!!
 SECTION('CalendarClassWindow')
Screen WINDOW,AT(,,109,116),FONT('Arial',8,,FONT:bold),CENTER,SYSTEM,GRAY
       BOX,AT(0,0,109,14),USE(?BoxTitleBackground),COLOR(0FF8000H),FILL(0FF8000H)
       BOX,AT(0,14,109,101),USE(?BoxBodyBackground),COLOR(COLOR:White),FILL(COLOR:White)
       STRING(@s15),AT(28,2,52,9),USE(?MonthYear),TRN,CENTER,FONT('Microsoft Sans Serif',8,COLOR:White,FONT:bold)
       STRING('Sun'),AT(7,15),USE(?Sun),TRN,CENTER,FONT('Arial',7,COLOR:Navy,)
       STRING('Mon'),AT(21,15,10,8),USE(?Mon),TRN,CENTER,FONT('Arial',7,COLOR:Navy,FONT:regular)
       STRING('Tue'),AT(32,15,16,8),USE(?Tue),TRN,CENTER,FONT('Arial',7,COLOR:Navy,FONT:regular)
       STRING('Wed'),AT(49,15,11,8),USE(?Wed),TRN,CENTER,FONT('Arial',7,COLOR:Navy,FONT:regular)
       STRING('Thu'),AT(60,15,16,8),USE(?Thu),TRN,CENTER,FONT('Arial',7,COLOR:Navy,FONT:regular)
       STRING('Fri'),AT(73,15,16,8),USE(?Fri),TRN,CENTER,FONT('Arial',7,COLOR:Navy,FONT:regular)
       STRING('Sat'),AT(88,15,16,8),USE(?Sat),TRN,CENTER,FONT('Arial',7,COLOR:Navy,FONT:regular)
       LINE,AT(5,21,98,0),USE(?Line1),COLOR(COLOR:Black)
       BUTTON,AT(5,23,14,12),USE(?Day1),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(19,23,14,12),USE(?Day2),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(33,23,14,12),USE(?Day3),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(47,23,14,12),USE(?Day4),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(61,23,14,12),USE(?Day5),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(75,23,14,12),USE(?Day6),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(89,23,14,12),USE(?Day7),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,35,14,12),USE(?Day8),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(19,35,14,12),USE(?Day9),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(33,35,14,12),USE(?Day10),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(47,35,14,12),USE(?Day11),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(61,35,14,12),USE(?Day12),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(75,35,14,12),USE(?Day13),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(89,35,14,12),USE(?Day14),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,47,14,12),USE(?Day15),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(19,47,14,12),USE(?Day16),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(33,47,14,12),USE(?Day17),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(47,47,14,12),USE(?Day18),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(61,47,14,12),USE(?Day19),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(75,47,14,12),USE(?Day20),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(89,47,14,12),USE(?Day21),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,59,14,12),USE(?Day22),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(19,59,14,12),USE(?Day23),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(33,59,14,12),USE(?Day24),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(47,59,14,12),USE(?Day25),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(61,59,14,12),USE(?Day26),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(75,59,14,12),USE(?Day27),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(89,59,14,12),USE(?Day28),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,71,14,12),USE(?Day29),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(19,71,14,12),USE(?Day30),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(33,71,14,12),USE(?Day31),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(47,71,14,12),USE(?Day32),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(61,71,14,12),USE(?Day33),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(75,71,14,12),USE(?Day34),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(89,71,14,12),USE(?Day35),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,83,14,12),USE(?Day36),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(19,83,14,12),USE(?Day37),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(33,83,14,12),USE(?Day38),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(47,83,14,12),USE(?Day39),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(61,83,14,12),USE(?Day40),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(75,83,14,12),USE(?Day41),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(89,83,14,12),USE(?Day42),SKIP,FLAT,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       LINE,AT(5,98,98,0),USE(?Line2),COLOR(COLOR:Black)
       BUTTON('&Today'),AT(5,101,30,13),USE(?Today),FLAT,FONT('Microsoft Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
       BUTTON('&Close'),AT(73,101,30,13),USE(?Exit),FLAT,FONT('Microsoft Sans Serif',8,COLOR:Black,FONT:regular,CHARSET:ANSI)
       BUTTON,AT(1,2,9,9),USE(?PrevYear),IMM,FLAT,TIP('Previous Year'),ICON('VCRFIRST.ICO'),DELAY(50)
       BUTTON,AT(10,2,9,9),USE(?PrevMonth),IMM,FLAT,TIP('Previous Month'),ICON('VCRPRIOR.ICO'),DELAY(50)
       BUTTON,AT(19,2,9,9),USE(?Less7),IMM,FLAT,TIP('Previous Week'),ICON('VCRUP.ICO'),DELAY(50)
       BUTTON,AT(80,2,9,9),USE(?More7),IMM,FLAT,TIP('Next Week'),ICON('VCRDOWN.ICO'),DELAY(50)
       BUTTON,AT(89,2,9,9),USE(?NextMonth),IMM,FLAT,TIP('Next Month'),ICON('VCRNEXT.ICO'),DELAY(50)
       BUTTON,AT(98,2,9,9),USE(?NextYear),IMM,FLAT,TIP('Next Year'),ICON('VCRLAST.ICO'),DELAY(50)
     END
!!
!! Window structure for the Small popup Calendar
!!
 SECTION('CalendarSmallClassWindow')
Screen WINDOW('Calendar'),AT(,,208,136),FONT('Arial',8,,FONT:bold),CENTER,TIMER(100),SYSTEM,GRAY,DOUBLE
       PANEL,AT(1,0,207,136),USE(?BoxBodyBackground),FILL(COLOR:Gray),BEVEL(-1,1)
       STRING(@t6),AT(41,5),USE(?CurrTime),TRN,FONT('Arial',8,COLOR:White,,CHARSET:ANSI)
       STRING(@s15),AT(9,16,105,15),USE(?MonthYear),TRN,CENTER,FONT('Microsoft Sans Serif',12,COLOR:Lime,FONT:bold)
       STRING(@D2),AT(130,16,59,15),USE(?TheDate),TRN,RIGHT,FONT('Microsoft Sans Serif',12,COLOR:Lime,FONT:bold,CHARSET:ANSI)
       STRING('Sun'),AT(7,34),USE(?Sun),TRN,CENTER,FONT('Arial',7,COLOR:White,)
       STRING('Mon'),AT(25,34),USE(?Mon),TRN,CENTER,FONT('Arial',7,COLOR:White,FONT:regular)
       STRING('Tue'),AT(37,34,16,10),USE(?Tue),TRN,CENTER,FONT('Arial',7,COLOR:White,FONT:regular)
       STRING('Wed'),AT(57,34),USE(?Wed),TRN,CENTER,FONT('Arial',7,COLOR:White,FONT:regular)
       STRING('Thu'),AT(69,34,16,10),USE(?Thu),TRN,CENTER,FONT('Arial',7,COLOR:White,FONT:regular)
       STRING('Fri'),AT(85,34,16,10),USE(?Fri),TRN,CENTER,FONT('Arial',7,COLOR:White,FONT:regular)
       STRING('Sat'),AT(101,34,16,10),USE(?Sat),TRN,CENTER,FONT('Arial',7,COLOR:White,FONT:regular)
       BUTTON,AT(5,46,16,14),USE(?Day1),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(21,46,16,14),USE(?Day2),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(37,46,16,14),USE(?Day3),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(53,46,16,14),USE(?Day4),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(69,46,16,14),USE(?Day5),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(85,46,16,14),USE(?Day6),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(101,46,16,14),USE(?Day7),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,60,16,14),USE(?Day8),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(21,60,16,14),USE(?Day9),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(37,60,16,14),USE(?Day10),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(53,60,16,14),USE(?Day11),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(69,60,16,14),USE(?Day12),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(85,60,16,14),USE(?Day13),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(101,60,16,14),USE(?Day14),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,74,16,14),USE(?Day15),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(21,74,16,14),USE(?Day16),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(37,74,16,14),USE(?Day17),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(53,74,16,14),USE(?Day18),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(69,74,16,14),USE(?Day19),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(85,74,16,14),USE(?Day20),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(101,74,16,14),USE(?Day21),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,88,16,14),USE(?Day22),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(21,88,16,14),USE(?Day23),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(37,88,16,14),USE(?Day24),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(53,88,16,14),USE(?Day25),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(69,88,16,14),USE(?Day26),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(85,88,16,14),USE(?Day27),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(101,88,16,14),USE(?Day28),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,102,16,14),USE(?Day29),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(21,102,16,14),USE(?Day30),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(37,102,16,14),USE(?Day31),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(53,102,16,14),USE(?Day32),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(69,102,16,14),USE(?Day33),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(85,102,16,14),USE(?Day34),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(101,102,16,14),USE(?Day35),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(5,116,16,14),USE(?Day36),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(21,116,16,14),USE(?Day37),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(37,116,16,14),USE(?Day38),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(53,116,16,14),USE(?Day39),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(69,116,16,14),USE(?Day40),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(85,116,16,14),USE(?Day41),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON,AT(101,116,16,14),USE(?Day42),SKIP,FONT('Arial',9,COLOR:Black,,CHARSET:ANSI)
       BUTTON('&Today'),AT(123,46,82,14),USE(?Today),FONT('Microsoft Sans Serif',8,,FONT:bold)
       BUTTON('- Month'),AT(123,60,41,14),USE(?PrevMonth),FONT('Microsoft Sans Serif',8,,FONT:bold)
       BUTTON('+ &Month'),AT(123,74,41,14),USE(?NextMonth),FONT('Microsoft Sans Serif',8,,FONT:bold)
       BUTTON('- Year'),AT(123,88,41,14),USE(?PrevYear),FONT('Microsoft Sans Serif',8,,FONT:bold)
       BUTTON('+ &Year'),AT(123,102,41,14),USE(?NextYear),FONT('Microsoft Sans Serif',8,,FONT:bold)
       BUTTON('- 7 Day'),AT(164,60,41,14),USE(?Less7),FONT('Microsoft Sans Serif',8,,FONT:bold)
       BUTTON('+ 7 &Day'),AT(164,74,41,14),USE(?More7),FONT('Microsoft Sans Serif',8,,FONT:bold)
       BUTTON('- 15 Days'),AT(164,88,41,14),USE(?Less15),FONT('Microsoft Sans Serif',8,,FONT:bold)
       BUTTON('+ 15 Days'),AT(164,102,41,14),USE(?More15),FONT('Microsoft Sans Serif',8,,FONT:bold)
       BUTTON('&Close'),AT(123,116,82,14),USE(?Exit),LEFT
     END
!!
!! Window structure for the Small popup Calendar Touch Friendly
!!
 SECTION('CalendarSmallTouchClassWindow')
Screen WINDOW,AT(,,130,120),CENTER,GRAY,IMM,SYSTEM,FONT('Arial',8,,FONT:bold), |
            TIMER(100),NOFRAME
        PANEL,AT(0,0,128,20),USE(?BoxBodyBackgroundTop),FILL(COLOR:Gray)
        PANEL,AT(0,20,128,75),USE(?BoxBodyBackgroundMid),FILL(COLOR:Gray)
        PANEL,AT(0,95,128,24),USE(?BoxBodyBackgroundBottom),FILL(COLOR:Gray)
        REGION,AT(2,48,126,20),USE(?PANELReadOnlyButtons)
        BUTTON,AT(4,3,16,16),USE(?Today),SKIP,FONT('Microsoft Sans Serif',16,, |
                FONT:bold),ICON('MiniCal.ico'),FLAT,TRN
        BUTTON('Mon'),AT(4,48,38,20),USE(?MonthText),SKIP,FONT('Microsoft Sans S' & |
                'erif',16,,FONT:bold)
        BUTTON('Day'),AT(46,48,38,20),USE(?DayText),SKIP,FONT('Microsoft Sans Se' & |
                'rif',16,,FONT:bold)
        BUTTON('Year'),AT(86,48,38,20),USE(?YearText),SKIP,FONT('Microsoft Sans ' & |
                'Serif',16,,FONT:bold)
        PROMPT('Monday'),AT(19,2,105,12),USE(?TheDateDOW),TRN,FONT('Segoe UI',9, |
                COLOR:White,FONT:regular,CHARSET:ANSI),RIGHT
        STRING(@D18),AT(19,10,105,12),USE(?TheDate),TRN,RIGHT,FONT('Segoe UI',9, |
                COLOR:White,FONT:regular,CHARSET:ANSI)
        BUTTON('-'),AT(4,71,38,20),USE(?PrevMonth),FONT('Microsoft Sans Serif', |
                18,,FONT:bold),IMM,DELAY(2)
        BUTTON('+'),AT(4,25,38,20),USE(?NextMonth),FONT('Microsoft Sans Serif', |
                18,,FONT:bold),IMM,DELAY(2)
        BUTTON('-'),AT(46,71,38,20),USE(?Less1),FONT('Microsoft Sans Serif',18,, |
                FONT:bold),IMM,DELAY(2)
        BUTTON('+'),AT(46,25,38,20),USE(?More1),FONT('Microsoft Sans Serif',18,, |
                FONT:bold),IMM,DELAY(2)
        BUTTON('-'),AT(86,71,38,20),USE(?PrevYear),FONT('Microsoft Sans Serif', |
                18,,FONT:bold),IMM,DELAY(2)
        BUTTON('+'),AT(86,25,38,20),USE(?NextYear),FONT('Microsoft Sans Serif', |
                18,,FONT:bold),IMM
        BUTTON('&Set'),AT(6,100,58,14),USE(?Exit),FONT('Microsoft Sans Serif',8, |
                ,FONT:bold)
        BUTTON('&Cancel'),AT(65,100,58,14),USE(?Cancel)
    END
!!
!! Window structure for the Small popup Calendar Touch Friendly
!!
 SECTION('CalendarSmallTouchIconsClassWindow')
Screen WINDOW,AT(,,128,124),CENTER,SYSTEM,FONT('Arial',8,,FONT:bold),TIMER(100), |
            NOFRAME
        PANEL,AT(0,0,128,20),USE(?BoxBodyBackgroundTop),FILL(COLOR:Gray)
        PANEL,AT(0,20,128,75),USE(?BoxBodyBackgroundMid),FILL(COLOR:Gray)
        PANEL,AT(0,95,128,29),USE(?BoxBodyBackgroundBottom),FILL(COLOR:Gray)
        REGION,AT(2,48,126,20),USE(?PANELReadOnlyButtons)
        BUTTON,AT(4,3,16,16),USE(?Today),SKIP,FONT('Microsoft Sans Serif',16,, |
                FONT:bold),ICON('MiniCal.ico'),FLAT,TRN
        PROMPT('Monday'),AT(19,2,105,12),USE(?TheDateDOW),TRN,FONT('Segoe UI',9, |
                COLOR:White,FONT:regular,CHARSET:ANSI),RIGHT
        STRING(@D18),AT(19,10,105,12),USE(?TheDate),TRN,RIGHT,FONT('Segoe UI',9, |
                COLOR:White,FONT:regular,CHARSET:ANSI)
        BUTTON('Mon'),AT(4,48,38,20),USE(?MonthText),SKIP,FONT('Segoe UI Light', |
                16,,FONT:regular)
        BUTTON('Day'),AT(46,48,38,20),USE(?DayText),SKIP,FONT('Segoe UI Light', |
                16,,FONT:regular)
        BUTTON('Year'),AT(86,48,38,20),USE(?YearText),SKIP,FONT('Segoe UI Light', |
                16,,FONT:regular)
        BUTTON('+'),AT(4,25,38,20),USE(?NextMonth),FONT('Segoe UI Light',18,,FONT:bold), |
                IMM,DELAY(2)
        BUTTON('-'),AT(4,71,38,20),USE(?PrevMonth),FONT('Segoe UI Light',18,,FONT:bold), |
                IMM,DELAY(2)
        BUTTON('+'),AT(46,25,38,20),USE(?More1),FONT('Segoe UI Light',18,,FONT:bold), |
                IMM,DELAY(2)
        BUTTON('-'),AT(46,71,38,20),USE(?Less1),FONT('Segoe UI Light',18,,FONT:bold), |
                IMM,DELAY(2)
        BUTTON('+'),AT(86,25,38,20),USE(?NextYear),FONT('Segoe UI Light',18,,FONT:bold), |
                IMM,DELAY(2)
        BUTTON('-'),AT(86,71,38,20),USE(?PrevYear),FONT('Segoe UI Light',18,,FONT:bold), |
                IMM,DELAY(2)
        BUTTON,AT(25,98,30,24),USE(?Exit),FONT('Microsoft Sans Serif',8,,FONT:bold), |
                ICON('MiniOk.ico'),FLAT,TRN
        BUTTON,AT(74,98,30,24),USE(?Cancel),ICON('MiniCancel.ico'),FLAT,TRN
    END
!!
!! Window structure for the Small popup Calendar Touch Friendly
!!
 SECTION('CalendarSmallTouchRegionsClassWindow')
Screen WINDOW,AT(,,130,124),CENTER,SYSTEM,FONT('Arial',8,,FONT:bold),TIMER(100), |
            NOFRAME
        PANEL,AT(0,0,130,20),USE(?BoxBodyBackgroundTop),FILL(COLOR:Gray)
        PANEL,AT(0,20,130,75),USE(?BoxBodyBackgroundMid),FILL(COLOR:Gray)
        PANEL,AT(0,95,130,29),USE(?BoxBodyBackgroundBottom),FILL(COLOR:Gray)
        BUTTON,AT(4,3,16,16),USE(?Today),SKIP,FONT('Microsoft Sans Serif',16,, |
                FONT:bold),ICON('MiniCal.ico'),FLAT,TRN
        PROMPT('Monday'),AT(20,2,105,12),USE(?TheDateDOW),TRN,FONT('Segoe UI',9, |
                COLOR:White,FONT:regular,CHARSET:ANSI),RIGHT
        STRING(@D18),AT(20,10,105,12),USE(?TheDate),TRN,RIGHT,FONT('Segoe UI',9, |
                COLOR:White,FONT:regular,CHARSET:ANSI)
        BUTTON,AT(26,98,30,24),USE(?Exit),SKIP,FONT('Microsoft Sans Serif',8,, |
                FONT:bold),ICON('MiniOk.ico'),FLAT,TRN
        BUTTON,AT(74,98,30,24),USE(?Cancel),SKIP,ICON('MiniCancel.ico'),FLAT,TRN
        BOX,AT(7,26,38,36),USE(?BOXMonthTop1),FILL(0E0E0E0H),ROUND,LINEWIDTH(0)
        BOX,AT(7,56,38,37),USE(?BOXMonthMid1),FILL(0E0E0E0H),ROUND,LINEWIDTH(0)
        BOX,AT(47,26,38,36),USE(?BOXMonthTop2),FILL(0E0E0E0H),ROUND,LINEWIDTH(0)
        BOX,AT(47,56,38,37),USE(?BOXMonthMid2),FILL(0E0E0E0H),ROUND,LINEWIDTH(0)
        BOX,AT(87,26,38,36),USE(?BOXMonthTop3),FILL(0E0E0E0H),ROUND,LINEWIDTH(0)
        BOX,AT(87,56,38,37),USE(?BOXMonthMid3),FILL(0E0E0E0H),ROUND,LINEWIDTH(0)
        REGION,AT(7,26,38,27),USE(?NextMonthPanel)
        REGION,AT(47,26,38,27),USE(?More1Panel)
        REGION,AT(87,26,38,27),USE(?NextYearPanel)
        REGION,AT(87,64,38,27),USE(?PrevYearPanel)
        REGION,AT(47,64,38,27),USE(?Less1Panel)
        REGION,AT(7,64,38,27),USE(?PrevMonthPanel)
        PROMPT('+'),AT(20,24),USE(?PROMPT1),TRN,FONT('Segoe UI Light',20)
        PROMPT('+'),AT(60,24),USE(?PROMPT1:2),TRN,FONT('Segoe UI Light',20)
        PROMPT('+'),AT(100,24),USE(?PROMPT1:3),TRN,FONT('Segoe UI Light',20)
        PROMPT('-'),AT(102,68),USE(?PROMPT1:4),TRN,FONT('Segoe UI Light',20)
        PROMPT('-'),AT(62,68),USE(?PROMPT1:5),TRN,FONT('Segoe UI Light',20)
        PROMPT('-'),AT(22,68),USE(?PROMPT1:6),TRN,FONT('Segoe UI Light',20)
        PROMPT('2012'),AT(87,46,38),USE(?YearText:2),TRN,FONT('Segoe UI Light',20), |
                CENTER
        PROMPT('31'),AT(47,46,38),USE(?DayText:2),TRN,FONT('Segoe UI Light',20),CENTER
        PROMPT('Jan'),AT(7,46,38),USE(?MonthText:2),TRN,FONT('Segoe UI Light',20), |
                CENTER
        BOX,AT(7,26,38,66),USE(?BOXBorde1),COLOR(COLOR:White),ROUND,LINEWIDTH(2)
        BOX,AT(47,26,38,66),USE(?BOXBorde2),COLOR(COLOR:White),ROUND,LINEWIDTH(2)
        BOX,AT(87,26,38,66),USE(?BOXBorde3),COLOR(COLOR:White),ROUND,LINEWIDTH(2)
    END
!!
!! Day Group declaration with Day labels
!!
 SECTION('CalendarDayGroup')
Day_group            GROUP,STATIC
d1                   STRING('Mon')
d2                   STRING('Tue')
d3                   STRING('Wed')
d4                   STRING('Thu')
d5                   STRING('Fri')
d6                   STRING('Sat')
d7                   STRING('Sun')
                     END
!!
!! Month Group declaration with Day labels
!!
 SECTION('CalendarMonthGroup')
Month_group          GROUP,STATIC
m1                   STRING('January   ')
m2                   STRING('February  ')
m3                   STRING('March     ')
m4                   STRING('April     ')
m5                   STRING('May       ')
m6                   STRING('June      ')
m7                   STRING('July      ')
m8                   STRING('August    ')
m9                   STRING('September ')
m10                  STRING('October   ')
m11                  STRING('November  ')
m12                  STRING('December  ')
                     END

#Template(StringTheory,'CapeSoft StringTheory - Version1.93'),Family('cw20'),Family('abc')
#!-----------------------------------------------------------------------------------
#! Copyright (c) by CapeSoft Software - www.capesoft.com - All Rights Reserved
   #Include('cape01.tpw')
   #Include('cape02.tpw')
#!- - - - -  - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -
#GROUP(%ReadGlobal,%pa,%force)
  #INSERT(%SetFamily)
  #insert(%ReadClassesPR,'StringTheory.Inc',%pa,%force)
#!- - - - -  - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -
#SYSTEM
  #EQUATE(%StringTheoryTPLVersion,'1.93')
#!- - - - -  - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -
#Extension(Activate_StringTheory,'Activate CapeSoft StringTheory - Version:1.93'),Application
  #sheet
    #TAB('General')
      #BOXED
        #IMAGE('StringTheory_extension.jpg')
        #DISPLAY('StringTheory'),prop(PROP:FontStyle,700),prop(PROP:FontName,'Tahoma'),at(,110)
        #DISPLAY('Version ' & %StringTheoryTPLVersion),prop(PROP:FontStyle,700),prop(PROP:FontName,'Tahoma')
        #DISPLAY('Copyright � 2010-2013 by CapeSoft Software'),prop(PROP:FontName,'Tahoma')
        #DISPLAY('www.capesoft.com'),prop(PROP:FontName,'Tahoma')
      #ENDBOXED
      #Display()
      #BOXED('Debugging')
        #PROMPT('Disable All StringTheory Features',Check),%NoGloStringTheory,At(10)
      #ENDBOXED
      #Boxed('Settings')
        #Prompt('Include MD5 code',Check),%md5,default(0),at(10)
      #EndBoxed
    #ENDTAB
    #TAB('Multi-Dll'),Where(%NoGloStringTheory=0)
            #Boxed('')
              #Prompt('This is part of a Multi-DLL program',Check),%MultiDLL,default(%ProgramExtension='DLL'),at(10)
              #Enable(%MultiDLL=1)
                #Enable(%ProgramExtension='DLL')
                  #Prompt('Export StringTheory Class from this DLL',Check),%RootDLL,at(10)
                #EndEnable
              #EndEnable
            #EndBoxed
    #ENDTAB
    #TAB('Classes')
      #Insert(%GlobalDeclareClassesPR)
    #ENDTAB
  #endsheet
#!- - - - -  - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -
#AT(%AfterGlobalIncludes),where(%NoGloStringTheory=0)
  include('StringTheory.Inc'),ONCE
#ENDAT
#!- - - - -  - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -
#ATStart
  #IF(%NoGloStringTheory = 0)
    #INSERT(%ReadGlobal,2,0)
  #ENDIF
#ENDAt
#!- - - - -  - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -
#AtEnd
  #IF(%NoGloStringTheory = 0)
    #INSERT(%EndGlobal)
  #ENDIF
#ENDAt
#!- - - - -  - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -
#AT(%DllExportList),Where(%programExtension = 'DLL' and %RootDLL=1 and %MultiDll=1 and %NoGloStringTheory=0)
#insert(%ExportClassesPR,'StringTheory.Inc')
#ENDAT
#!- - - - -  - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -
#AT(%CustomGlobalDeclarations),where(%NoGloStringTheory=0)
  #INSERT(%Defines,1,'StringTheoryLinkMode','StringTheoryDllMode',%MultiDLL,%RootDll)
  #IF(%MultiDLL=0 or %RootDll=1)
    #IF(%md5)
      #Project('ojmd5.c')
      #PDEFINE('MD5',1)
    #Else
      #PDEFINE('MD5',0)
    #ENDIF
  #ENDIF
#ENDAT
#!----------------------------------------------------------------
#AT(%mpCompileAll),where(%NoGloStringTheory=0)
 #IF(%md5)
%#compile "ojmd5.c"
  #ENDIF
#ENDAT
#!----------------------------------------------------------------
#AT(%mpCompileAll7),where(%NoGloStringTheory=0)
 #IF(%md5)
    <Compile Include="ojmd5.c" />
  #ENDIF
#ENDAT
#!----------------------------------------------------------------
#AT(%mpDefineAll) ,where(%NoGloStringTheory=0)
#INSERT(%Defines,2,'StringTheoryLinkMode','StringTheoryDllMode',%MultiDLL,%RootDll)
  #IF(%MultiDLL=0 or %RootDll=1)
    #IF(%md5)
%#pragma define(MD5=>1)
    #Else
%#pragma define(MD5=>0)
    #ENDIF
  #ENDIF
#ENDAT
#!----------------------------------------------------------------
#AT(%mpDefineAll7) ,where(%NoGloStringTheory=0)
#INSERT(%Defines,3,'StringTheoryLinkMode','StringTheoryDllMode',%MultiDLL,%RootDll)
  #IF(%MultiDLL=0 or %RootDll=1)
    #IF(%md5)
%%3bMD5=&gt;1%%3b
    #Else
%%3bMD5=&gt;0%%3b
    #ENDIF
  #ENDIF
#ENDAT
#!-----------------------------------------------------------------------------------
#AT(%BeforeGlobalIncludes),where(%NoGloStringTheory=0)
StringTheory:TemplateVersion equate('%StringTheoryTPLVersion')
#endat
#!#################################################################################################
#! End of GLOBAL EXTENSION
#!#################################################################################################
#Extension(StringTheoryLocal,'StringTheory Local Extension'),Description('StringTheory Object (' & %StringTheoryObject & ')'),PROCEDURE,Multi,req(Activate_StringTheory(StringTheory))
  #Sheet
    #PREPARE
      #INSERT(%ReadGlobal,3,0)
    #ENDPREPARE
    #tab('General')
      #Boxed('Debugging'),prop(PROP:FontName,'Tahoma')
        #Prompt('Do Not Generate This Object',check),%NoStringTheory,at(10),prop(PROP:FontName,'Tahoma')
      #EndBoxed
      #Display()
      #boxed('Options'),section
        #PROMPT('Object Name:',@S255),%StringTheoryObject,Req,default('ThisStringTheory' & %ActiveTemplateInstance)
        #prompt('Class Name:',@s255),%StringTheoryClassName,req,default('StringTheory')
      #ENDBOXED
    #endtab
  #EndSheet
#!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
#atstart
  #if(%NoGloStringTheory=0 and %NoStringTheory=0)
    #insert(%AtStartInitialisation)
    #insert(%AddObjectPR,%StringTheoryClassName,%StringTheoryObject,'Local Objects')
  #endIf
#endat
#!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
#AT(%LocalDataClasses),where(%NoGloStringTheory=0 and %NoStringTheory=0)
#INSERT(%GenerateClassDeclaration,%StringTheoryClassName,%StringTheoryObject,'Local Objects','CapeSoft Objects')
#ENDAT
#!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
#AT(%DataSectionAfterWindow),where(%Family = 'cw20' and %cwversion <= 6000 and %NoGloStringTheory=0 and %NoStringTheory=0)
#insert(%GenerateClassDeclaration,%StringTheoryClassName,%StringTheoryObject,'Local Objects','CapeSoft Objects')
#EndAt
#!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
#AT(%DataSection),where(%ProcedureTemplate = 'Source' and (%Family = 'cw20' or  %cwversion < 7000) and %NoGloStringTheory=0 and %NoStringTheory=0)
#insert(%GenerateClassDeclaration,%StringTheoryClassName,%StringTheoryObject,'Local Objects','CapeSoft Objects')
#EndAt
#!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
#AT(%LocalProcedures),where(%NoGloStringTheory=0 and %NoStringTheory=0)
#INSERT(%GenerateMethods,%StringTheoryClassName,%StringTheoryObject,'Local Objects','CapeSoft Objects')
#ENDAT
#!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
#AT(%dMethodCodeSection,%ActiveTemplate & %ActiveTemplateInstance,%eMethodID),priority(5000),DESCRIPTION('Parent Call')
#INSERT(%ParentCall)
#ENDAT
#!---------------------------------------------------------------------------------------


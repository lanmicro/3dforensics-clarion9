#TEMPLATE(ThinNET, 'Thin@ Template v3.0'),FAMILY('ABC'),FAMILY('CW20')
#!
#SYSTEM
#EQUATE(%ThinNETVersionNumber,'3.0')
#EQUATE(%ThinNETVersion,'Version ' & %ThinNETVersionNumber & ', September 27, 2013')
#!
#! ----------------------------------- ThinNET template module ------------------------------------
#!
#EXTENSION(ThinNET,'Thin@ Global Extension'),APPLICATION(ThinNETProcedure(ThinNET)),FIRST,SINGLE
#PREPARE
#ENDPREPARE
#INSERT(%ThinBox)
#DISPLAY
#DISPLAY
#SHEET, HSCROLL
#TAB('General')
    #PROMPT('Enable Thin@ ',CHECK),%ThinNETEnabled,DEFAULT(1),AT(10)
    #PROMPT('Enable Thin@ external dll support',CHECK),%ThinNETExternalDllEnabled,DEFAULT(1),AT(10)
#ENDTAB
#TAB('Visual appearance')
  #PROMPT('Sheet Tab Style:',DROP('TabStyle:Default[0]|TabStyle:BlackAndWhite[1]|TabStyle:Colored[2]|TabStyle:Boxed[3]')),%SheetTabStyle,Default(1), AT(75,,95), PROMPTAT(10)
  #PROMPT('Enable Listbox Header Colors',CHECK),%EnableListboxHeaderColorss,DEFAULT(0),AT(10)
  #ENABLE(%EnableListboxHeaderColorss)
    #BUTTON('Listbox Header Color Settings'), AT(,,180)
      #SHEET,HSCROLL
        #TAB('Listbox Header Color Settings')
          #BOXED('Listbox Header Colors'), SECTION
            #PROMPT ('Normal',COLOR),%GlobalcNormal,Default(-1), at(85,5), promptat(10,5)
            #PROMPT ('AccentTxt',COLOR),%GlobalcAccentTxt,Default(0DADADAH), at(85), promptat(10)
            #PROMPT ('ListHeaderBack',COLOR),%GlobalcListHeaderBack,Default(0DADADAH), at(85), promptat(10)
            #PROMPT ('ListHeaderFore',COLOR),%GlobalcListHeaderFore,Default(0000000H), at(85), promptat(10)
            #PROMPT ('ListHeaderGRBack',COLOR),%GlobalcListHeaderGRBack,Default(0DADADAH), at(85), promptat(10)
            #PROMPT ('ListHeaderGrFore',COLOR),%GlobalcListHeaderGrFore,Default(0000000H), at(85), promptat(10)
            #PROMPT ('ListHeaderArrow',COLOR),%GlobalcListHeaderArrow,Default(0808080h), at(85), promptat(10)
            #PROMPT ('ListHeaderArrow3D',COLOR),%GlobalcListHeaderArrow3D,Default(0FFFFFFh), at(85), promptat(10)
          #ENDBOXED  
        #ENDTAB
      #ENDSHEET
    #ENDBUTTON
  #ENDENABLE
  #PROMPT('Enable Window Scaling',OPTION),%GlobalWindowScaling,DEFAULT('Disable'),AT(10)  
  #PROMPT('Disable',RADIO)
  #PROMPT('Enable',RADIO)
  #PROMPT('Enable without Thin@',RADIO)
#ENDTAB
#ENDSHEET
#!------------------------------------------------------------------------
#AT(%ShipList)
#IF(NOT %ApplicationLocalLibrary AND %Target32)
#IF(VAREXISTS(%ThinNETEnabled) AND %ThinNETEnabled)
#INDENT(-2)
  The follow files are required by Thin@:
-------------------------------------------------------
___    ThinN@.DLL - Thin@ Runtime Library
___    RisNet.DLL - Thin@ Socket Library
___    7z.exe - Compression program used by Thin@
#IF( SUB(%CWVersion,1,2) >= 73 )
___    ClaOLE.DLL - CW 32 Bit OLE Support DLL
___    ClaDOS.DLL - CW 32 Bit DOS Support DLL
___    ClaASC.DLL - CW 32 Bit DOS Support DLL
#ELSIF( SUB(%CWVersion,1,2) >= 70 AND SUB(%CWVersion,1,2) <= 72 )
___    C70OLE.DLL - CW 32 Bit OLE Support DLL
___    C70DOS.DLL - CW 32 Bit DOS Support DLL
___    C70ASC.DLL - CW 32 Bit DOS Support DLL
#ELSIF( SUB(%CWVersion,1,2) = '63' OR SUB(%CWVersion,1,2) = '62' OR SUB(%CWVersion,1,2) = '61' )
___    C60OLEX.DLL - CW 32 Bit OLE Support DLL
___    C60DOSX.DLL - CW 32 Bit DOS Support DLL
___    C60ASCX.DLL - CW 32 Bit DOS Support DLL
#ENDIF
#INDENT(+2)
#ENDIF
#ENDIF
#ENDAT
#!
#AT (%CustomGlobalDeclarations)
#DECLARE(%ThinNetCWVersion)
#SET(%ThinNetCWVersion,SUB(%CWVersion,1,2))
#IF(%Target32 AND %ThinNETEnabled)
  #!#IF(~%GlobalExternal)
  #PROJECT('ThinN@.lib')
  #IF( SUB(%CWVersion,1,2) >= 70 )  
    #PROJECT('C%V%OLE%X%%L%.LIB')
    #PROJECT('C%V%DOS%X%%L%.LIB')
    #PROJECT('library(RisNet.dll), CopyToOutputDirectory=Always')
    #PROJECT('None(7z.exe),CopyToOutputDirectory=Always')
  #ENDIF
  #!#ENDIF
#ENDIF
#IF(%ApplicationLocalLibrary)
  #ERROR('Thin@ Does Not Support the Local Link Mode! Please recompile your application using Standalone(DLL)!')
#ENDIF
#ENDAT
#AT (%CustomGlobalDeclarations),WHERE(%ThinNETEnabled)
  #PDEFINE('_RisNetPresent_',1)
  #PDEFINE('_ThinNetPresent_',1)
#ENDAT
#AT (%AfterGlobalIncludes)
#IF(%TARGET32 AND %ThinNETEnabled)
  INCLUDE('RisApiClasses.INC'),ONCE
  INCLUDE('ThinN@.INC'),ONCE
#IF(%ThinNETExternalDllEnabled)
  INCLUDE('ThinN@External.INC'),ONCE
#ENDIF
#ENDIF
#ENDAT
#!#AT(%GlobalData),FIRST
#AT(%GlobalData)
ThinNETVersion    CSTRING('%ThinNETVersion')
ThinNETVersionNumber    CSTRING('%ThinNETVersionNumber')
ThinCWVersionNumber CSTRING('%ThinNetCWVersion')
ThinetDLLVersionInfo CSTRING(20)
#ENDAT
#!
#AT(%AfterFileDeclarations),FIRST
#IF(~%GlobalExternal AND %ThinNETEnabled)
ThinNETMgr   CLASS(NetManager) ! Declare Thin@ Manager class
CompressFile          PROCEDURE(STRING FileName,STRING Archive),DERIVED
CompressFiles         PROCEDURE(*QUEUE QF,*? FileName,STRING Archive),DERIVED
DecompressFile        PROCEDURE(STRING Archive,STRING FilePath,<STRING vPassword>,<STRING vParams>,LONG vTimeout=0),DERIVED
PrintPreview          PROCEDURE(*QUEUE PrintPreviewQueue,*? FileName,LONG pLandscape,BYTE SkipPreview=0,LONG vZoom=0),BYTE,DERIVED
ExecuteClientSource   PROCEDURE(STRING Var1,<STRING Var2>,<STRING Var3>,<STRING Var4>,<STRING Var5>,<STRING Var6>,<STRING Var7>),STRING,DERIVED,PROC  
AfterCreatingWindow   PROCEDURE(),DERIVED
BeforePaintingControl PROCEDURE(LONG Feq,BYTE Created=0,BYTE Last=0),BYTE,DERIVED
AfterPaintingControl  PROCEDURE(LONG Feq,BYTE Created=0,BYTE Last=0),DERIVED
TakeClientEvent       PROCEDURE(*Window WindowHandle),BYTE,PROC,DERIVED
             END

#IF(%ThinNETExternalDllEnabled)
! Thin@ external dll support class
ThinNETExternalMgr CLASS(ThinExternalClass)
Active                  PROCEDURE(),DERIVED,BYTE
AddListControl          PROCEDURE(*Window WindowHandle,LONG Feq,*QUEUE ListQueue),DERIVED
AddListControl          PROCEDURE(*Window WindowHandle,LONG Feq,? DataString),DERIVED
TakeEvent               PROCEDURE(*WINDOW Wind),DERIVED
StartLongRunningProcess PROCEDURE,DERIVED
EndLongRunningProcess   PROCEDURE,DERIVED
Message                 PROCEDURE(STRING pText,<STRING pCaption>,<STRING pIcon>,<STRING pButtons>,UNSIGNED pDefault=0,UNSIGNED pStyle=0),UNSIGNED,PROC,DERIVED
Popup                   PROCEDURE(STRING Selections,SIGNED XPar=_nopos,SIGNED Ypar=_nopos),SIGNED,DERIVED
DownloadFiles           PROCEDURE(*QUEUE SourceQueue,*? FileName,STRING Sign,<STRING FilePath>),STRING,DERIVED
DownloadFile            PROCEDURE(STRING FileName,STRING Sign,<STRING FilePath>,<STRING TargetName>,<*? ChangeCheck>),STRING,DERIVED
UploadFile              PROCEDURE(STRING FileName,<STRING FilePath>),DERIVED
CancelCloseWindow 	    PROCEDURE,DERIVED
Select                  PROCEDURE(SIGNED vControl=0,SIGNED vPosition=0,SIGNED vEndPosition=0),DERIVED
ForceCloseWindow        PROCEDURE,BYTE,DERIVED
PressKey                PROCEDURE(UNSIGNED pKeyCode),DERIVED
ShowProgressDialog      PROCEDURE(? pDone,? pTotal,BYTE pCancel=0,<? pTitle>,<? pFormatExpression>),BYTE,PROC,DERIVED
Press                   PROCEDURE(STRING Expression),DERIVED
GetClientPath           PROCEDURE(),STRING,DERIVED
SetClientPath           PROCEDURE(STRING PathExpression),STRING,DERIVED
FileDialog              PROCEDURE(<STRING Title>,*? FileName,<STRING Extensions>,SIGNED Flag=0),PROC,BOOL,DERIVED
FileDialoga             PROCEDURE(<STRING Title>,*? FileName,<STRING Extensions>,SIGNED Flag=0,<*SIGNED pIndex>),PROC,BOOL,DERIVED
ColorDialog             PROCEDURE(<STRING Title>,*? Rgb,SIGNED Suppress=0),SIGNED,PROC,DERIVED
FontDialog              PROCEDURE(<STRING vTitle>,*? vTypeFace,<*? vSize>,<*? vColor>,<*? vStyle>,SIGNED vAdded= 0),BOOL,PROC,DERIVED
FontDialoga             PROCEDURE(<STRING vTitle>,*? vTypeFace,<*? vSize>,<*? vColor>,<*? vStyle>,<*? vCharset>,SIGNED vAdded= 0),BOOL,DERIVED
PrinterDialog           PROCEDURE(<STRING Title>,BOOL Flag=FALSE),BOOL,PROC,DERIVED
MouseX                  PROCEDURE,SIGNED,DERIVED
MouseY                  PROCEDURE,SIGNED,DERIVED
Destroy                 PROCEDURE(SIGNED FirstControl=0,SIGNED EndControl=0),DERIVED
SetClientPrinter        PROCEDURE(STRING PrinterName),DERIVED
DisplayControlImage     PROCEDURE(LONG Feq),DERIVED
DisplayThread           PROCEDURE(LONG ThreadNo=0),DERIVED
AddListEIPControl       PROCEDURE(LONG pListFeq,LONG Column,LONG ControlFeq),DERIVED
AddOption               PROCEDURE(*Window WindowHandle,LONG Id,? OptionVar),DERIVED
DragId                  PROCEDURE(<*? vThread>,<*? vControl>),STRING,DERIVED
SkipCurrentAccept       PROCEDURE(<LONG Param>),LONG,DERIVED,PROC
Halt                    PROCEDURE(UNSIGNED errorlevel=0,<STRING message>),DERIVED
AddImageFolder          PROCEDURE(STRING ImagePath,BYTE OnTop=0),DERIVED
AddImageLibrary         PROCEDURE(STRING ImageLibrary,<STRING ImageLibrarySubdirectory>,<STRING ImageLibraryPassword>,BYTE ForceDownloadCheck=0),DERIVED
FilePath                PROCEDURE(),STRING,DERIVED
OpenWindow              PROCEDURE(*WINDOW Wind),DERIVED
CloseWindow             PROCEDURE(*WINDOW Wind),DERIVED
Display                 PROCEDURE(*Window WindowHandle,BYTE ForceEnd=0),BYTE,PROC,DERIVED
AddOCXControl           PROCEDURE(*Window WindowHandle, LONG Feq, STRING CreateStatement),DERIVED
SetOCXProperty          PROCEDURE(LONG Feq,STRING Expression,<STRING SetValue>,<*WINDOW WindowHandle>),DERIVED
GetOCXProperty          PROCEDURE(LONG Feq,STRING Property,<*WINDOW WindowHandle>),STRING,DERIVED
OCXRegisterEventProc    PROCEDURE(LONG Feq,LONG EventProcAddress),DERIVED
OCXGetLastEventName     PROCEDURE(SHORT Reference),STRING,DERIVED
OCXGetParamCount        PROCEDURE(SHORT Reference),STRING,DERIVED
OCXGetParam             PROCEDURE(SHORT Reference,LONG Count),STRING,DERIVED
OCXRegister             PROCEDURE(STRING FileName,BYTE ForceRegistration = 0),BYTE,PROC,DERIVED
OCXBind                 PROCEDURE(STRING BindName, <*? Variable>, <BYTE BindType>),DERIVED
GetWindowMaxState       PROCEDURE(*WINDOW WindowHandle),BYTE,PROC,DERIVED
SetWindowMaxState       PROCEDURE(*WINDOW WindowHandle, BYTE MaxState),DERIVED
ForwardKey              PROCEDURE(LONG Feq, UNSIGNED pKeyCode, LONG SelStart=0),DERIVED
SetDefaultListboxHeaderColors PROCEDURE(BYTE Enabled=1, LONG cNormal = -2, LONG cAccentTxt = -2, LONG cListHeaderBack = -2, LONG cListHeaderFore = -2, LONG cListHeaderGRBack = -2, LONG cListHeaderGrFore = -2, LONG cListHeaderArrow = -2, LONG cListHeaderArrow3D = -2),DERIVED
SetListboxHeaderColors  PROCEDURE(*Window WindowHandle, LONG Feq, SHORT Enabled=1, LONG cNormal = -2, LONG cAccentTxt = -2, LONG cListHeaderBack = -2, LONG cListHeaderFore = -2, LONG cListHeaderGRBack = -2, LONG cListHeaderGrFore = -2, LONG cListHeaderArrow = -2, LONG cListHeaderArrow3D = -2),DERIVED
SetDefaultTabStyle      PROCEDURE(BYTE pTabStyle = 1),DERIVED
SetTabStyle             PROCEDURE(*Window WindowHandle, LONG Feq, BYTE pTabStyle = 1),DERIVED
RunThinetApplication    PROCEDURE(STRING ApplicationName),DERIVED
RunOnClient             PROCEDURE(? ExecuteString,? WaitFlag,<? ShowFlag>),LONG,PROC,DERIVED
Exists                  PROCEDURE(STRING PathExpression),LONG,PROC,DERIVED
ClientPrinter           PROCEDURE,STRING,PROC,DERIVED
State                   PROCEDURE,BYTE,PROC,DERIVED
CurrentThread           PROCEDURE,LONG,PROC,DERIVED
ThreadBusy              PROCEDURE,LONG,PROC,DERIVED
ClientPath              PROCEDURE,STRING,PROC,DERIVED
FileDirPath             PROCEDURE,STRING,PROC,DERIVED
SetTopWindow            PROCEDURE(STRING WindowCaption),ULONG,PROC,DERIVED
ShellExecute            PROCEDURE(ULONG HWND, STRING pOperation, STRING pFile, STRING pParameters, STRING pDirectory, ULONG pShowCmd),ULONG,PROC,DERIVED
SendMail                PROCEDURE(STRING SendTo, STRING Subject, STRING MessageText, STRING Attachment, BYTE pSend = 0, BYTE SendFromServer = 0),PROC,LONG,DERIVED
ShowWindow              PROCEDURE(ULONG HWND, ULONG ShowCmd),BYTE,PROC,DERIVED
ClientStartPath         PROCEDURE,STRING,DERIVED
 END
 
#ENDIF
#ELSE
#IF(%ThinNETEnabled)
#IF(%AppTemplateFamily = 'CLARION')
ThinNETMgr   NetManager,EXTERNAL,DLL(dll_mode)  ! Declare Thin@ Manager class
#IF(%ThinNETExternalDllEnabled)
ThinNETExternalMgr ThinExternalClass,EXTERNAL,DLL(dll_mode)  ! Declare Thin@ Manager external dll support class
#ENDIF
#ELSE
ThinNETMgr   NetManager,EXTERNAL,DLL(_ABCDllMode_)  ! Declare Thin@ Manager class
#IF(%ThinNETExternalDllEnabled)
ThinNETExternalMgr ThinExternalClass,EXTERNAL,DLL(_ABCDllMode_)  ! Declare Thin@ Manager external dll support class
#ENDIF
#ENDIF
#ENDIF
#ENDIF
#ENDAT
#AT(%ProgramProcedures)
#IF(~%GlobalExternal AND %ThinNETEnabled)

ThinNETMgr.CompressFile PROCEDURE(STRING FileName,STRING Archive)
 CODE
#EMBED(%ThinNETCompressFileRoutine, 'Thin@ Compress file routine')
  PARENT.CompressFile(FileName,Archive)

ThinNETMgr.CompressFiles PROCEDURE(*QUEUE QF,*? FileName,STRING Archive)
 CODE
#EMBED(%ThinNETCompressFilesRoutine, 'Thin@ Compress files routine')
  PARENT.CompressFiles(QF,FileName,Archive)

ThinNETMgr.DecompressFile PROCEDURE(STRING Archive,STRING FilePath,<STRING vPassword>,<STRING vParams>,LONG vTimeout=0)
 CODE
#EMBED(%ThinNETDecompressFileRoutine, 'Thin@ Decompress files routine')
  IF NOT OMITTED(vPassword) AND NOT OMITTED(vParams) THEN
    PARENT.DecompressFile(Archive,FilePath,vPassword,vParams,vTimeout)
  ELSIF NOT OMITTED(vPassword) AND OMITTED(vParams) THEN
    PARENT.DecompressFile(Archive,FilePath,vPassword,,vTimeout)
  ELSIF OMITTED(vPassword) AND NOT OMITTED(vParams) THEN
    PARENT.DecompressFile(Archive,FilePath,,vParams,vTimeout)
  ELSIF OMITTED(vPassword) AND OMITTED(vParams) THEN
    PARENT.DecompressFile(Archive,FilePath)
  END
    
ThinNETMgr.PrintPreview PROCEDURE(*QUEUE PrintPreviewQueue,*? FileName,LONG pLandscape,BYTE SkipPreview=0,LONG vZoom=0)
#EMBED(%ThinNETPrintPreviewDATARoutine, 'Thin@ print preview DATA routine')
 CODE
#EMBED(%ThinNETPrintPreviewCODERoutine, 'Thin@ print preview CODE routine')
  RETURN PARENT.PrintPreview(PrintPreviewQueue,FileName,pLandscape,SkipPreview,vZoom)

ThinNETMgr.ExecuteClientSource PROCEDURE(STRING Var1,<STRING Var2>,<STRING Var3>,<STRING Var4>,<STRING Var5>,<STRING Var6>,<STRING Var7>)
#EMBED(%ThinNETExecuteClientSourceDATARoutine, 'Thin@ ExecuteClientSource DATA routine')
 CODE
#EMBED(%ThinNETExecuteClientSourceCODERoutine, 'Thin@ ExecuteClientSource CODE routine')
  RETURN PARENT.ExecuteClientSource(Var1,Var2,Var3,Var4,Var5,Var6,Var7)

ThinNETMgr.AfterCreatingWindow PROCEDURE()
#EMBED(%ThinNETAfterCreatingWindowDATARoutine, 'Thin@ After creating window DATA routine')
 CODE
#EMBED(%ThinNETAfterCreatingWindowCODERoutine, 'Thin@ After creating window CODE routine')
  PARENT.AfterCreatingWindow()

ThinNETMgr.BeforePaintingControl  PROCEDURE(LONG Feq,BYTE Created=0,BYTE Last=0)
#EMBED(%ThinNETBeforePaintingControlDATARoutine, 'Thin@ BeforePaintingControl DATA routine')
 CODE
#EMBED(%ThinNETBeforePaintingControlCODERoutine, 'Thin@ BeforePaintingControl CODE routine (return 0 to skip thin@ paints of this control)')
  RETURN PARENT.BeforePaintingControl(Feq,Created,Last)

ThinNETMgr.AfterPaintingControl  PROCEDURE(LONG Feq,BYTE Created=0,BYTE Last=0)
#EMBED(%ThinNETAfterPaintingControlDATARoutine, 'Thin@ AfterPaintingControl DATA routine')
 CODE
#EMBED(%ThinNETAfterPaintingControlCODERoutine, 'Thin@ AfterPaintingControl CODE routine')
  PARENT.AfterPaintingControl(Feq,Created,Last)

ThinNETMgr.TakeClientEvent PROCEDURE(*Window WindowHandle)
#EMBED(%ThinNETTakeClientEventDATARoutine, 'Thin@ TakeClientEvent DATA routine')
 CODE
#EMBED(%ThinNETTakeClientEventCODERoutine, 'Thin@ TakeClientEvent CODE routine (inside ACCEPT loop)')
  RETURN PARENT.TakeClientEvent(WindowHandle)

#IF(%ThinNETExternalDllEnabled)
! Thin@ external dll support class
ThinNETExternalMgr.Active PROCEDURE
 CODE
 RETURN ThinNetMgr.Active
 	
ThinNETExternalMgr.AddListControl PROCEDURE(*Window WindowHandle,LONG Feq,*QUEUE ListQueue)
 CODE
  ThinNetMgr.AddListControl(WindowHandle,Feq,ListQueue)

ThinNETExternalMgr.AddListControl PROCEDURE(*Window WindowHandle,LONG Feq,? DataString)
 CODE
  ThinNetMgr.AddListControl(WindowHandle,Feq,DataString)

ThinNETExternalMgr.TakeEvent PROCEDURE(*WINDOW Wind)
 CODE
  ThinNetMgr.TakeEvent(Wind)

ThinNETExternalMgr.StartLongRunningProcess PROCEDURE
 CODE
 IF ThinNetMgr.Active THEN ThinNetMgr.StartLongRunningProcess().

ThinNETExternalMgr.EndLongRunningProcess PROCEDURE
 CODE
 IF ThinNetMgr.Active THEN ThinNetMgr.EndLongRunningProcess().

ThinNETExternalMgr.Message PROCEDURE(STRING pText,<STRING pCaption>,<STRING pIcon>,<STRING pButtons>,UNSIGNED pDefault=0,UNSIGNED pStyle=0)
 CODE
 IF ThinNetMgr.Active THEN
  IF NOT OMITTED(pButtons) THEN
      RETURN Risnet:Message(pText,pCaption,pIcon,pButtons,pDefault,pStyle)
  ELSIF NOT OMITTED(pIcon) THEN
      RETURN Risnet:Message(pText,pCaption,pIcon,,pDefault,pStyle)
  ELSIF NOT OMITTED(pCaption) THEN
      RETURN Risnet:Message(pText,pCaption,,,pDefault,pStyle)
  ELSE RETURN Risnet:Message(pText,,,,pDefault,pStyle).
 END

ThinNETExternalMgr.Popup PROCEDURE(STRING Selections,SIGNED XPar=_nopos,SIGNED Ypar=_nopos)
 CODE
 IF ThinNetMgr.Active THEN RETURN RisNet:Popup(Selections,Xpar,Ypar)   .

ThinNETExternalMgr.DownloadFiles PROCEDURE(*QUEUE SourceQueue,*? FileName,STRING Sign,<STRING FilePath>)
 CODE
 IF ThinNetMgr.Active THEN
  IF NOT OMITTED(FilePath) THEN
    RETURN Risnet:DownloadFiles(SourceQueue,FileName,Sign,FilePath)
  ELSE RETURN Risnet:DownloadFiles(SourceQueue,FileName,Sign).
 END

ThinNETExternalMgr.DownloadFile PROCEDURE(STRING FileName,STRING Sign,<STRING FilePath>,<STRING TargetName>,<*? ChangeCheck>)
 CODE
 IF ThinNetMgr.Active THEN
 	  IF NOT OMITTED(ChangeCheck) AND NOT OMITTED(TargetName) AND NOT OMITTED(FilePath) THEN  			
      RETURN Risnet:DownloadFile(FileName,Sign,FilePath,TargetName,ChangeCheck)
    ELSIF NOT OMITTED(ChangeCheck) AND NOT OMITTED(TargetName) AND OMITTED(FilePath) THEN  			
      RETURN Risnet:DownloadFile(FileName,Sign,,TargetName,ChangeCheck)
    ELSIF NOT OMITTED(ChangeCheck) AND OMITTED(TargetName) AND NOT OMITTED(FilePath) THEN  			
      RETURN Risnet:DownloadFile(FileName,Sign,FilePath,,ChangeCheck)
    ELSIF OMITTED(ChangeCheck) AND NOT OMITTED(TargetName) AND NOT OMITTED(FilePath) THEN  			
      RETURN Risnet:DownloadFile(FileName,Sign,FilePath,TargetName)
    ELSIF NOT OMITTED(ChangeCheck) AND OMITTED(TargetName) AND OMITTED(FilePath) THEN  			
      RETURN Risnet:DownloadFile(FileName,Sign,,,ChangeCheck)
    ELSIF OMITTED(ChangeCheck) AND OMITTED(TargetName) AND NOT OMITTED(FilePath) THEN  			
      RETURN Risnet:DownloadFile(FileName,Sign,FilePath)
    ELSIF OMITTED(ChangeCheck) AND NOT OMITTED(TargetName) AND OMITTED(FilePath) THEN  			
      RETURN Risnet:DownloadFile(FileName,Sign,,TargetName).
 END
 
ThinNETExternalMgr.UploadFile PROCEDURE(STRING FileName,<STRING FilePath>)
 CODE
 IF ThinNetMgr.Active THEN
 	 IF NOT OMITTED(FilePath ) THEN
 	   Risnet:UploadFile(FileName,FilePath)
 	 ELSE
 	   Risnet:UploadFile(FileName)
 	 END
 END

ThinNETExternalMgr.CancelCloseWindow PROCEDURE
 CODE
 IF ThinNetMgr.Active THEN Risnet:CancelCloseWindow().

ThinNETExternalMgr.Select PROCEDURE(SIGNED vControl=0,SIGNED vPosition=0,SIGNED vEndPosition=0)
 CODE
 IF ThinNetMgr.Active THEN Risnet:Select(vControl, vPosition, vEndPosition).
 
ThinNETExternalMgr.ForceCloseWindow PROCEDURE
 CODE
 IF ThinNetMgr.Active THEN RETURN Risnet:ForceCloseWindow().

ThinNETExternalMgr.PressKey PROCEDURE(UNSIGNED pKeyCode)
 CODE
 IF ThinNetMgr.Active THEN Risnet:PressKey(pKeyCode).
 
ThinNETExternalMgr.ShowProgressDialog PROCEDURE(? pDone,? pTotal,BYTE pCancel=0,<? pTitle>,<? pFormatExpression>)
 CODE
 IF ThinNetMgr.Active THEN
 	 IF NOT OMITTED(pTitle) AND NOT OMITTED(pFormatExpression) THEN
 	   RETURN Risnet:ShowProgressDialog(pDone,pTotal,pCancel,pTitle,pFormatExpression)
 	 ELSIF NOT OMITTED(pTitle) AND OMITTED(pFormatExpression) THEN
 	   RETURN Risnet:ShowProgressDialog(pDone,pTotal,pCancel,pTitle)
 	 ELSIF OMITTED(pTitle) AND NOT OMITTED(pFormatExpression) THEN
 	   RETURN Risnet:ShowProgressDialog(pDone,pTotal,pCancel,,pFormatExpression)
 	 END
 END

ThinNETExternalMgr.Press PROCEDURE(STRING Expression)
 CODE
 IF ThinNetMgr.Active THEN Risnet:Press(Expression).
 
ThinNETExternalMgr.GetClientPath PROCEDURE
 CODE
 IF ThinNetMgr.Active THEN RETURN Risnet:GetClientPath().
 	 
ThinNETExternalMgr.SetClientPath PROCEDURE(STRING PathExpression)
 CODE
 IF ThinNetMgr.Active THEN RETURN Risnet:SetClientPath(PathExpression).

ThinNETExternalMgr.FileDialog PROCEDURE(<STRING Title>,*? FileName,<STRING Extensions>,SIGNED Flag=0)
 CODE
 IF ThinNetMgr.Active THEN
 	 IF NOT OMITTED(Title) AND NOT OMITTED(Extensions) THEN
 	   RETURN Risnet:FileDialog(Title,FileName,Extensions,Flag)
 	 ELSIF NOT OMITTED(Title) AND OMITTED(Extensions) THEN
 	   RETURN Risnet:FileDialog(Title,FileName,,Flag)
 	 ELSIF OMITTED(Title) AND NOT OMITTED(Extensions) THEN
 	   RETURN Risnet:FileDialog(,FileName,Extensions,Flag)
 	 ELSIF OMITTED(Title) AND OMITTED(Extensions) THEN
 	   RETURN Risnet:FileDialog(,FileName,,Flag)
 	 END
 END

ThinNETExternalMgr.FileDialoga PROCEDURE(<STRING Title>,*? FileName,<STRING Extensions>,SIGNED Flag=0,<*SIGNED pIndex>)
 CODE
 IF ThinNetMgr.Active THEN
 	 IF NOT OMITTED(Title) AND NOT OMITTED(Extensions) AND NOT OMITTED(pIndex) THEN
 	   RETURN Risnet:FileDialoga(Title,FileName,Extensions,Flag,pIndex)
 	 ELSIF NOT OMITTED(Title) AND NOT OMITTED(Extensions) AND OMITTED(pIndex) THEN
 	   RETURN Risnet:FileDialoga(Title,FileName,Extensions,Flag)
 	 ELSIF NOT OMITTED(Title) AND OMITTED(Extensions) AND NOT OMITTED(pIndex) THEN
 	   RETURN Risnet:FileDialoga(Title,FileName,,Flag,pIndex)
 	 ELSIF OMITTED(Title) AND NOT OMITTED(Extensions) AND NOT OMITTED(pIndex) THEN
 	   RETURN Risnet:FileDialoga(,FileName,Extensions,Flag,pIndex)
 	 ELSIF NOT OMITTED(Title) AND OMITTED(Extensions) AND OMITTED(pIndex) THEN
 	   RETURN Risnet:FileDialoga(Title,FileName,,Flag)
 	 ELSIF OMITTED(Title) AND NOT OMITTED(Extensions) AND OMITTED(pIndex) THEN
 	   RETURN Risnet:FileDialoga(,FileName,Extensions,Flag)
 	 ELSIF OMITTED(Title) AND OMITTED(Extensions) AND OMITTED(pIndex) THEN
 	   RETURN Risnet:FileDialoga(,FileName,,Flag)
 	 END
 END

ThinNETExternalMgr.ColorDialog PROCEDURE(<STRING Title>,*? Rgb,SIGNED Suppress=0)
 CODE
 IF ThinNetMgr.Active THEN
 	 IF NOT OMITTED(Title) THEN
 	   RETURN Risnet:ColorDialog(Title,Rgb,Suppress)
 	 ELSE
 	   RETURN Risnet:ColorDialog(,Rgb,Suppress)
 	 END
 END

ThinNETExternalMgr.FontDialog PROCEDURE(<STRING vTitle>,*? vTypeFace,<*? vSize>,<*? vColor>,<*? vStyle>,SIGNED vAdded= 0)
 CODE
 IF ThinNetMgr.Active THEN
 	 IF OMITTED(vSize) THEN
 	   RETURN Risnet:FontDialog(vTitle,vTypeFace,,,,vAdded)
 	 ELSIF OMITTED(vColor) THEN
 	   RETURN Risnet:FontDialog(vTitle,vTypeFace,vSize,,,vAdded)
 	 ELSIF OMITTED(vStyle) THEN
 	   RETURN Risnet:FontDialog(vTitle,vTypeFace,vSize,vColor,,vAdded)
 	 ELSE 
 	   RETURN Risnet:FontDialog(vTitle,vTypeFace,vSize,vColor,vStyle,vAdded)
 	 END
 END

ThinNETExternalMgr.FontDialoga PROCEDURE(<STRING vTitle>,*? vTypeFace,<*? vSize>,<*? vColor>,<*? vStyle>,<*? vCharset>,SIGNED vAdded=0)
 CODE
 IF ThinNetMgr.Active THEN
 	 IF OMITTED(vSize) THEN
 	   RETURN Risnet:FontDialoga(vTitle,vTypeFace,,,,,vAdded)
 	 ELSIF OMITTED(vColor) THEN
 	   RETURN Risnet:FontDialoga(vTitle,vTypeFace,vSize,,,,vAdded)
 	 ELSIF OMITTED(vStyle) THEN
 	   RETURN Risnet:FontDialoga(vTitle,vTypeFace,vSize,vColor,,,vAdded)
 	 ELSIF OMITTED(vCharset) THEN
 	   RETURN Risnet:FontDialoga(vTitle,vTypeFace,vSize,vColor,vStyle,,vAdded)
 	 ELSE 
 	   RETURN Risnet:FontDialoga(vTitle,vTypeFace,vSize,vColor,vStyle,vCharset,vAdded)
 	 END
 END

ThinNETExternalMgr.PrinterDialog  PROCEDURE(<STRING Title>,BOOL Flag=FALSE)
 CODE
 IF ThinNetMgr.Active THEN
 	 IF NOT OMITTED(Title) THEN
 	   RETURN Risnet:PrinterDialog(Title,Flag)
 	 ELSE
 	   RETURN Risnet:PrinterDialog(,Flag)
 	 END
 END

ThinNETExternalMgr.MouseX PROCEDURE
 CODE
 IF ThinNetMgr.Active THEN RETURN Risnet:MouseX().

ThinNETExternalMgr.MouseY PROCEDURE
 CODE
 IF ThinNetMgr.Active THEN RETURN Risnet:MouseY().

ThinNETExternalMgr.Destroy PROCEDURE(SIGNED FirstControl=0,SIGNED EndControl=0)
 CODE
 IF ThinNetMgr.Active THEN Risnet:Destroy(FirstControl,EndControl).

ThinNETExternalMgr.SetClientPrinter PROCEDURE(STRING PrinterName)
 CODE
 IF ThinNetMgr.Active THEN ThinNETMgr.SetClientPrinter(PrinterName).

ThinNETExternalMgr.DisplayControlImage PROCEDURE(LONG Feq)
 CODE
 IF ThinNetMgr.Active THEN ThinNETMgr.DisplayControlImage(Feq).

ThinNETExternalMgr.DisplayThread PROCEDURE(LONG ThreadNo=0)
 CODE
 IF ThinNetMgr.Active THEN ThinNETMgr.DisplayThread(ThreadNo).

ThinNETExternalMgr.AddListEIPControl PROCEDURE(LONG pListFeq,LONG Column,LONG ControlFeq)
 CODE
 IF ThinNetMgr.Active THEN ThinNETMgr.AddListEIPControl(pListFeq,Column,ControlFeq).

ThinNETExternalMgr.AddOption PROCEDURE(*Window WindowHandle,LONG Id,? OptionVar)
 CODE
 IF ThinNetMgr.Active THEN ThinNETMgr.AddOption(WindowHandle,Id,OptionVar).

ThinNETExternalMgr.DragId PROCEDURE(<*? vThread>,<*? vControl>)
 CODE
 IF ThinNetMgr.Active THEN 
  IF NOT OMITTED(vThread) AND NOT OMITTED(vControl) THEN
 	  RETURN Risnet:DragId(vThread,vControl)
  ELSIF NOT OMITTED(vThread) AND OMITTED(vControl) THEN
 	  RETURN Risnet:DragId(vThread)
  ELSIF OMITTED(vThread) AND NOT OMITTED(vControl) THEN
 	  RETURN Risnet:DragId(vControl)
  ELSIF OMITTED(vThread) AND OMITTED(vControl) THEN
 	  RETURN Risnet:DragId()
  END
 END
 	  
ThinNETExternalMgr.SkipCurrentAccept PROCEDURE(<LONG Param>)
 CODE
 IF ThinNetMgr.Active THEN 
  IF NOT OMITTED(Param) THEN
  	ThinNETMgr.SkipCurrentAccept = Param
  END
  RETURN ThinNETMgr.SkipCurrentAccept
 END

ThinNETExternalMgr.Halt PROCEDURE(UNSIGNED errorlevel=0,<STRING message>)
 CODE 
  IF ThinNetMgr.Active THEN 
    IF OMITTED(message) THEN
  	  Risnet:Halt(errorlevel)
    ELSE
  	  HALT_OLD(errorlevel,message)
    END
  END

ThinNETExternalMgr.AddImageFolder PROCEDURE(STRING ImagePath,BYTE OnTop=0)
 CODE
  IF ThinNetMgr.Active THEN ThinNetMgr.AddImageFolder(ImagePath,OnTop).

ThinNETExternalMgr.AddImageLibrary PROCEDURE(STRING ImageLibrary,<STRING ImageLibrarySubdirectory>,<STRING ImageLibraryPassword>,BYTE ForceDownloadCheck=0)
 CODE
  IF ThinNetMgr.Active THEN
  IF NOT OMITTED(ImageLibrarySubdirectory) AND NOT OMITTED(ImageLibraryPassword) THEN
  	 ThinNetMgr.AddImageLibrary(ImageLibrary,ImageLibrarySubdirectory,ImageLibraryPassword,ForceDownloadCheck)
  ELSIF NOT OMITTED(ImageLibrarySubdirectory) AND OMITTED(ImageLibraryPassword) THEN
  	 ThinNetMgr.AddImageLibrary(ImageLibrary,ImageLibrarySubdirectory,,ForceDownloadCheck)
  ELSIF OMITTED(ImageLibrarySubdirectory) AND NOT OMITTED(ImageLibraryPassword) THEN
  	 ThinNetMgr.AddImageLibrary(ImageLibrary,,ImageLibraryPassword,ForceDownloadCheck)
  ELSIF OMITTED(ImageLibrarySubdirectory) AND OMITTED(ImageLibraryPassword) THEN
  	 ThinNetMgr.AddImageLibrary(ImageLibrary,,,ForceDownloadCheck)
  END
  END

ThinNETExternalMgr.FilePath PROCEDURE
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN ThinNetMGR.FilePath
  END

ThinNETExternalMgr.OpenWindow PROCEDURE(*WINDOW Wind)
 CODE
  IF ThinNetMgr.Active THEN ThinNetMgr.OpenWindow(Wind).

ThinNETExternalMgr.CloseWindow PROCEDURE(*WINDOW Wind)
 CODE
  IF ThinNetMgr.Active THEN ThinNetMgr.CloseWindow(Wind).

ThinNETExternalMgr.Display PROCEDURE(*Window WindowHandle,BYTE ForceEnd=0)
 CODE
  IF ThinNetMgr.Active THEN RETURN ThinNetMgr.Display(WindowHandle, ForceEnd).

ThinNETExternalMgr.AddOCXControl PROCEDURE(*Window WindowHandle, LONG Feq, STRING CreateStatement)
 CODE
  IF ThinNetMgr.Active THEN ThinNetMgr.AddOCXControl(WindowHandle, Feq, CreateStatement).

ThinNETExternalMgr.SetOCXProperty PROCEDURE(LONG Feq,STRING Expression,<STRING SetValue>,<*WINDOW WindowHandle>)
 CODE
  IF ThinNetMgr.Active THEN
  	 IF NOT OMITTED(SetValue) AND NOT OMITTED(WindowHandle) THEN
  	 	  RisNet:SetOcxProperty(Feq, Expression, SetValue, WindowHandle)
  	 ELSIF NOT OMITTED(SetValue) THEN
  	 	  RisNet:SetOcxProperty(Feq, Expression, SetValue)
  	 ELSIF NOT OMITTED(WindowHandle) THEN
  	 	  RisNet:SetOcxProperty(Feq, Expression,, WindowHandle)
  	 ELSE
  	 	  RisNet:SetOcxProperty(Feq, Expression)
  	 END
  END
  	
ThinNETExternalMgr.GetOCXProperty PROCEDURE(LONG Feq,STRING Property,<*WINDOW WindowHandle>)
 CODE
  IF ThinNetMgr.Active THEN 
  	 IF NOT OMITTED(WindowHandle) THEN
  	 	  RETURN RisNet:GetOcxProperty(Feq, Property, WindowHandle)
  	 ELSE
  	 	  RETURN RisNet:GetOcxProperty(Feq, Property)
  	 END
  END

ThinNETExternalMgr.OCXRegisterEventProc PROCEDURE(LONG Feq,LONG EventProcAddress)
 CODE
  IF ThinNetMgr.Active THEN RisNet:OCXRegisterEventProc(Feq, EventProcAddress).

ThinNETExternalMgr.OCXGetLastEventName PROCEDURE(SHORT Reference)
 CODE
  IF ThinNetMgr.Active THEN RETURN RisNet:OCXGetLastEventName(Reference) ELSE RETURN ''.

ThinNETExternalMgr.OCXGetParamCount PROCEDURE(SHORT Reference)
 CODE
  IF ThinNetMgr.Active THEN RETURN RisNet:OCXGetParamCount(Reference) ELSE RETURN 0.

ThinNETExternalMgr.OCXGetParam PROCEDURE(SHORT Reference,LONG Count)
 CODE
  IF ThinNetMgr.Active THEN RETURN RisNet:OCXGetParam(Reference, Count) ELSE RETURN ''.

ThinNETExternalMgr.OCXRegister PROCEDURE(STRING FileName,BYTE ForceRegistration = 0)
 CODE
  IF ThinNetMgr.Active THEN RETURN RisNet:OCXRegister(FileName, ForceRegistration) ELSE RETURN 0.

ThinNETExternalMgr.OCXBind PROCEDURE(STRING BindName, <*? Variable>, <BYTE BindType>)
 CODE
  IF ThinNetMgr.Active THEN 
    IF ~OMITTED(Variable) AND ~OMITTED(BindType) THEN
      RisNet:OCXBind(BindName, Variable, BindType)
    ELSIF ~OMITTED(Variable) AND OMITTED(BindType) THEN
      RisNet:OCXBind(BindName, Variable)
    ELSIF OMITTED(Variable) AND ~OMITTED(BindType) THEN
      RisNet:OCXBind(BindName,,BindType)
    ELSIF OMITTED(Variable) AND OMITTED(BindType) THEN
      RisNet:OCXBind(BindName)
    END
  END
  
ThinNETExternalMgr.GetWindowMaxState PROCEDURE(*WINDOW WindowHandle)
 CODE
  IF ThinNetMgr.Active THEN RETURN RisNet:GetWindowMaxState(WindowHandle) ELSE RETURN 0.

ThinNETExternalMgr.SetWindowMaxState PROCEDURE(*WINDOW WindowHandle, BYTE MaxState)
 CODE
  IF ThinNetMgr.Active THEN RisNet:SetWindowMaxState(WindowHandle, MaxState).

ThinNETExternalMgr.ForwardKey PROCEDURE(LONG Feq, UNSIGNED pKeyCode, LONG SelStart=0)
 CODE
  IF ThinNetMgr.Active THEN RisNet:ForwardKey(Feq, pKeyCode, SelStart).

ThinNETExternalMgr.SetDefaultListboxHeaderColors PROCEDURE(BYTE Enabled=1, LONG cNormal = -2, LONG cAccentTxt = -2, LONG cListHeaderBack = -2, LONG cListHeaderFore = -2, LONG cListHeaderGRBack = -2, LONG cListHeaderGrFore = -2, LONG cListHeaderArrow = -2, LONG cListHeaderArrow3D = -2)
 CODE
  IF ThinNetMgr.Active THEN RisNet:SetDefaultListboxHeaderColors(Enabled, cNormal, cAccentTxt, cListHeaderBack, cListHeaderFore, cListHeaderGRBack, cListHeaderGrFore, cListHeaderArrow, cListHeaderArrow3D).

ThinNETExternalMgr.SetListboxHeaderColors PROCEDURE(*Window WindowHandle, LONG Feq, SHORT Enabled=1, LONG cNormal = -2, LONG cAccentTxt = -2, LONG cListHeaderBack = -2, LONG cListHeaderFore = -2, LONG cListHeaderGRBack = -2, LONG cListHeaderGrFore = -2, LONG cListHeaderArrow = -2, LONG cListHeaderArrow3D = -2)
 CODE
  IF ThinNetMgr.Active THEN RisNet:SetListboxHeaderColors(WindowHandle, Feq, Enabled, cNormal, cAccentTxt, cListHeaderBack, cListHeaderFore, cListHeaderGRBack, cListHeaderGrFore, cListHeaderArrow, cListHeaderArrow3D).

ThinNETExternalMgr.SetDefaultTabStyle PROCEDURE(BYTE pTabStyle = 1)
 CODE
  IF ThinNetMgr.Active THEN RisNet:SetDefaultTabStyle(pTabStyle).

ThinNETExternalMgr.SetTabStyle PROCEDURE(*Window WindowHandle, LONG Feq, BYTE pTabStyle = 1)
 CODE
  IF ThinNetMgr.Active THEN RisNet:SetTabStyle(WindowHandle, Feq, pTabStyle).

ThinNETExternalMgr.RunThinetApplication PROCEDURE(STRING ApplicationName)
 CODE
  IF ThinNetMgr.Active THEN RisNet:RunThinetApplication(ApplicationName).

ThinNETExternalMgr.RunOnClient PROCEDURE(? ExecuteString,? WaitFlag,<? ShowFlag>)
 CODE
  IF ThinNetMgr.Active
    IF NOT OMITTED(ShowFlag) THEN 
      RETURN RisNet:RunOnClient(ExecuteString,WaitFlag,Showflag)
    ELSE
      RETURN RisNet:RunOnClient(ExecuteString,WaitFlag)
    END    	
  END

ThinNETExternalMgr.Exists PROCEDURE(STRING PathExpression)
 CODE
  IF ThinNetMgr.Active 
     RETURN RisNet:Exists(PathExpression)
  END

ThinNETExternalMgr.ClientPrinter PROCEDURE
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN ThinNetMgr.ClientPrinter
  END

ThinNETExternalMgr.State PROCEDURE
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN ThinNetMgr.State
  END

ThinNETExternalMgr.CurrentThread PROCEDURE
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN ThinNetMgr.CurrentThread
  END

ThinNETExternalMgr.ThreadBusy PROCEDURE
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN ThinNetMgr.ThreadBusy
  END

ThinNETExternalMgr.ClientPath PROCEDURE
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN ThinNetMgr.ClientPath
  END

ThinNETExternalMgr.FileDirPath PROCEDURE
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN ThinNetMgr.FileDirPath
  END

ThinNETExternalMgr.SetTopWindow PROCEDURE(STRING WindowCaption)
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN RisNet:SetTopWindow(WindowCaption)
  END
  
ThinNETExternalMgr.ShellExecute PROCEDURE(ULONG HWND, STRING pOperation, STRING pFile, STRING pParameters, STRING pDirectory, ULONG pShowCmd)
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN RisNet:ShellExecute(HWND, pOperation, pFile, pParameters, pDirectory, pShowCmd)
  END

ThinNETExternalMgr.SendMail PROCEDURE(STRING SendTo, STRING Subject, STRING MessageText, STRING Attachment, BYTE pSend = 0, BYTE SendFromServer = 0)
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN RisNet:SendMail(SendTo, Subject, MessageText, Attachment, pSend, SendFromServer)
  END

ThinNETExternalMgr.ShowWindow PROCEDURE(ULONG HWND, ULONG ShowCmd)
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN RisNet:ShowWindow(HWND, ShowCmd)
  END

ThinNETExternalMgr.ClientStartPath PROCEDURE
 CODE
  IF ThinNetMgr.Active THEN
  	 RETURN ThinNetMgr.ClientstartPath
  END
  RETURN ''

#ENDIF
#ENDIF
#ENDAT 
#! 
#AT(%BeginningExports)
#IF(~%GlobalExternal)
;ThinN@Section
#CALL(%AddExpItem(ABC),'$ThinNETMgr')                             
#IF(%ThinNETExternalDllEnabled)
#CALL(%AddExpItem(ABC),'$ThinNETExternalMgr')                             
#ENDIF
;End of ThinN@Section
#ENDIF
#ENDAT
#!
#!#! Starting Thin@ mode
#AT(%ProgramSetup),WHERE(UPPER(%ProgramExtension)='EXE' AND %ThinNETEnabled)
#! Check if Clarion is using Thin@ version on BUILTINS.CLW file and replace the file with the Thin@ version if not
 OMIT('***',_ThinNetBuiltins_=1)
 #!It does not appear that the Thin@ version of BUILTINS.CLW is in your LIBSRC Folder. This can happen after upgrading Clarion versions.
#IF( SUB(%CWVersion,1,2) >= 70 )
  #REPLACE(%CWRoot & 'libsrc\win\builtins.clw', %CWRoot & 'accessory\libsrc\win\builtins.clw')
#ELSIF( SUB(%CWVersion,1,2) = '63' OR SUB(%CWVersion,1,2) = '62' OR SUB(%CWVersion,1,2) = '61' )
  #REPLACE(%CWRoot & 'libsrc\builtins.clw', %CWRoot & '3rdparty\libsrc\builtins.clw')
#ENDIF
 !End OMIT('***',_ThinNetBuiltins_=1)
! Starting Thin@
#EMBED(%ThinNETBeforeStarting, 'Thin@ Before Starting')
ThinNETMgr.Start('%ThinNETVersionNumber')
#EMBED(%ThinNETAfterStarting, 'Thin@ After Starting')
#! Sheet tab style and Listbox header colors support
IF ThinNetMgr.Active THEN
 RisNet:SetDefaultTabStyle(%SheetTabStyle)
#IF(%EnableListboxHeaderColorss=1)
 RisNet:SetDefaultListboxHeaderColors(1, %GlobalcNormal, %GlobalcAccentTxt, %GlobalcListHeaderBack, %GlobalcListHeaderFore, %GlobalcListHeaderGRBack, %GlobalcListHeaderGrFore, %GlobalcListHeaderArrow, %GlobalcListHeaderArrow3D)
#ENDIF
END !IF ThinMgr.Active
#ENDAT
#! Ending thin@ mode
#AT(%ProgramEnd),WHERE(UPPER(%ProgramExtension)='EXE')
! Ending thin@
#EMBED(%ThinNETBeforeEnding, 'ThinNET before ending')
#!ThinNetMgr.Kill()
#EMBED(%ThinNETAfterEnding, 'ThinNET after ending')
#ENDAT
#!
#! GPR Reporter support
#AT(%dMethodCodeSection),WHERE(((VAREXISTS(%dMethodIDGPF) AND %dMethodIDGPF = 'Initialize () ,VIRTUAL') OR (VAREXISTS(%dMethodID) AND %dMethodID = 'Initialize () ,VIRTUAL')) AND %ThinNETEnabled AND UPPER(%ProgramExtension)='EXE' AND VAREXISTS(%DisableGeneric) AND %DisableGeneric=0),PRIORITY(101)
  ! Thin@ support for GPF reporter in Thin@ mode
  IF ThinNETMgr.Active THEN
     %csGPFObject.RestartProgram=0
     %csGPFObject.CloseQuietly = 1
  END
#ENDAT
#!
#EXTENSION (ThinNETProcedure, 'Thin@ Procedure Extension'),PROCEDURE,FIRST,SINGLE
#INSERT(%ThinBox)
#DISPLAY
#DISPLAY
#SHEET 
#TAB('General')
      #PROMPT('Enable Thin@ ',CHECK),%ThinNETProcEnabled,DEFAULT(1),AT(10)
      #PROMPT('Force window timers to refresh',CHECK),%ThinNETTimerRefresh,DEFAULT(0),AT(10)
      #PROMPT('MDI Tab bar style:',DROP('None[-1]|Black and white[1]|Colored[2]|Squared[3]|Boxed[4]')),%ThinNETTabBarStyle,Default(3)
      #PROMPT('Enable Window Scaling',CHECK),%LocalWindowScaling,DEFAULT(1),AT(10)  
      #BOXED('Maximize window size')
      #PROMPT('Width',CHECK),%MaximizeWindowWidth,DEFAULT(0),AT(96)
      #PROMPT('Height',CHECK),%MaximizeWindowHeight,DEFAULT(0),AT(150,267)
      #PROMPT('Margin x',@n5),%MaximizeWindowMarginX,DEFAULT(0)
      #PROMPT('Margin y',@n5),%MaximizeWindowMarginy,DEFAULT(0)
      #ENDBOXED
#ENDTAB
#ENDSHEET
#! Field declarations
#FIELD #!, WHERE(VAREXISTS(%ThinNETEnabled) AND %ThinNETEnabled)
  #PROMPT('Thin@ - ignore this control',CHECK),%HideOnClient,DEFAULT(0),AT(15)
  #ENABLE(%HideOnClient=0)
  #BUTTON('Thin@ Control Options'), AT(,,180), WHERE(%HideOnClient=0)
    #SHEET,HSCROLL
      #TAB('Thin@ Control Options')
        #BOXED('Thin@ synchronization options'), WHERE(%HideOnClient=0 AND (%ControlType='BUTTON' OR %ControlType='CHECK' OR %ControlType='COMBO' OR %ControlType='ENTRY' OR %ControlType='LIST' OR %ControlType='OLE' OR %ControlType='OPTION' OR %ControlType='RADIO' OR %ControlType='SHEET' OR %ControlType='SPIN' OR %ControlType='TEXT' OR %ControlType='REGION')), SECTION, HIDE
          #BOXED('Refresh window for specified events on control'), SECTION
            #PROMPT('EVENT:Accepted',CHECK),%SynOnAccept,DEFAULT(1), AT(10,4)
            #ENABLE(%ControlType <> 'REGION')
              #PROMPT('EVENT:Selected',CHECK),%SynOnSelect,DEFAULT(1), AT(10)
            #ENDENABLE
          #ENDBOXED
        #ENDBOXED
  #!      
        #BOXED('Thin@ - Noyantis ChartPro properties'), WHERE(VAREXISTS(%NYS012AppDisable) AND ~%NYS012AppDisable AND %ControlType='OLE' AND %HideOnClient=0), SECTION, HIDE
          #PROMPT('Transfer images instead of OCX control.',CHECK),%ChartProTranferImages,DEFAULT(0),AT(15,4)
          #DISPLAY('The client-side will not require OCX registration.'),AT(15)
        #ENDBOXED        
  #!
        #BOXED('Thin@ RTF properties'), WHERE(%HideOnClient=0 AND %ControlType='TEXT' AND INSTRING('rtf',LOWER(%ControlStatement),1,1)), HIDE
          #PROMPT('RTF max size(default 250k): ',@n10.),%ThinControlMaxSize,DEFAULT(250000), AT(120,,50), PROMPTAT(10)
        #ENDBOXED  
  #!      
        #BOXED('Thin@ textbox properties'), WHERE(%HideOnClient=0 AND %ControlType='TEXT' AND ~INSTRING('rtf',LOWER(%ControlStatement),1,1)), SECTION, HIDE
          #PROMPT('Text max size(default 32k): ',@n10.),%ThinControlMaxSize,DEFAULT(32000), AT(,,50), PROMPTAT(10)
        #ENDBOXED  
  #!      
        #BOXED('Sheet Tab Style'), WHERE(%HideOnClient=0 AND %ControlType='SHEET'), SECTION, HIDE
          #PROMPT('Override Global',CHECK),%OverrideGlobalTabStyle,DEFAULT(0), AT(10)
          #ENABLE(%OverrideGlobalTabStyle=1)
            #PROMPT('Sheet Tab Style:',DROP('TabStyle:Default[0]|TabStyle:BlackAndWhite[1]|TabStyle:Colored[2]|TabStyle:Boxed[3]')),%SheetTabStyle,Default(1), AT(80,,95), PROMPTAT(10)
          #ENDENABLE
        #ENDBOXED
  #!
        #BOXED('Thin@ listbox pipe parsing'), WHERE(%HideOnClient=0 AND INSTRING(%ControlType,'LIST,COMBO',1,1)<>0), SECTION, HIDE
          #PROMPT('Correct pipes "|" in listbox data',CHECK),%ThinControlListboxPipes,DEFAULT(0), AT(10)
        #ENDBOXED              
      #ENDTAB
#!      
      #TAB('Listbox Header Colors'), WHERE(%EnableListboxHeaderColorss=1 AND %HideOnClient=0 AND (%ControlType='LIST' OR %ControlType='COMBO' OR %ControlType='DROPLIST' OR %ControlType='DROPCOMBO'))
      #PROMPT('Enable for a non-Thin@ application',CHECK),%EnableWithoutThinet, DEFAULT(0), AT(10)
      #PROMPT('Override Global Listbox Header Colors',CHECK),%OverrideGlobalListboxHeaderColors,DEFAULT(0),AT(10)
        #ENABLE(%OverrideGlobalListboxHeaderColors=1), CLEAR
          #BOXED('Listbox Header Colors'), CLEAR, SECTION
            #PROMPT ('Normal',COLOR),%cNormal, default(%GlobalcNormal),at(85,5), promptat(10,5)
            #PROMPT ('AccentTxt',COLOR),%cAccentTxt, default(%GlobalcAccentTxt),at(85), promptat(10)
            #PROMPT ('ListHeaderBack',COLOR),%cListHeaderBack, default(%GlobalcListHeaderBack), at(85), promptat(10)
            #PROMPT ('ListHeaderFore',COLOR),%cListHeaderFore, default(%GlobalcListHeaderFore),at(85), promptat(10)
            #PROMPT ('ListHeaderGRBack',COLOR),%cListHeaderGRBack, default(%GlobalcListHeaderGRBack),at(85), promptat(10)
            #PROMPT ('ListHeaderGrFore',COLOR),%cListHeaderGrFore, default(%GlobalcListHeaderGrFore),at(85), promptat(10)
            #PROMPT ('ListHeaderArrow',COLOR),%cListHeaderArrow, default(%GlobalcListHeaderArrow),at(85), promptat(10)
            #PROMPT ('ListHeaderArrow3D',COLOR),%cListHeaderArrow3D, default(%GlobalcListHeaderArrow3D),at(85), promptat(10)
          #ENDBOXED
        #ENDENABLE
      #ENDTAB
#!      
    #ENDSHEET
  #ENDBUTTON  
  #ENDENABLE
#ENDFIELD
#!
#!
#! ----------------------------------------------------------------------------------------
#! Thin@ - Insight Graph support
#! ----------------------------------------------------------------------------------------
#AT (%DataSectionBeforeWindow), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='Insight_Graph(Insight)')
  #CONTEXT(%Procedure,%ActiveTemplate)
  #ENDCONTEXT
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
GrafFEQ%ActiveTemplateInstance SIGNED ! Thin@ Insight Graph support
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT

#AT(%AfterWindowOpening),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='Insight_Graph(Insight)')
  IF ThinNetMgr.Active THEN ! Thin@ Insight Graph support
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
    ! Thin@ - replicate the Insight Graph regions into images
  	GrafFEQ%ActiveTemplateInstance = CREATE(0, CREATE:IMAGE, %Control{PROP:PARENT}); GrafFEQ%ActiveTemplateInstance{PROP:use} = GrafFEQ%ActiveTemplateInstance
  	GETPOSITION(%Control,X#,Y#,W#,H#); GrafFEQ%ActiveTemplateInstance{PROP:XPos}=X#; GrafFEQ%ActiveTemplateInstance{PROP:YPos}=Y#
  	GrafFEQ%ActiveTemplateInstance{PROP:Width}=W#; GrafFEQ%ActiveTemplateInstance{PROP:Height}=H#
  	UNHIDE(GrafFEQ%ActiveTemplateInstance)
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  END !IF
  #ENDFOR
#ENDAT
 
#AT(%ResetCodeSectionAfter,,,'4) After Adding Variables'),PRIORITY(5000), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='Insight_Graph(Insight)')
  #IF(NOT VAREXISTS(%pObject))
  #DECLARE(%pObject)
  #ENDIF
  #SET(%pObject,%Object)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance AND %Object = %pObject)
  ! Thin@ Insight Graph support
  IF (ThinNetMgr.Active AND %Control{prop:visible} <> '') THEN  
     IF ThinNETMgr.State=0 THEN
          ThinNETMgr.RecieveDataFromClient()
          ThinNETMgr.State = 1
     END
     PARENT.Draw()
     ApiCreateDirectory(ThinNetMgr.FileDirPath)
     PARENT.SaveAs(ThinNetMgr.FileDirPath & '%DefaultObjectName.bmp')
     ChangedCheck#=1
     IF RisNet:DownloadFile(ThinNetMgr.FileDirPath & '%DefaultObjectName.bmp','',,,ChangedCheck#).
     ! Only refresh image on the client if image changed
     IF ChangedCheck# THEN
       ThinNetMgr.DisplayControlImage(GrafFEQ%ControlInstance)
       GrafFEQ%ControlInstance{PROP:TEXT} = ThinNetMgr.FileDirPath & '%DefaultObjectName.bmp'
     END
     REMOVE(ThinNetMgr.FileDirPath & '%DefaultObjectName.bmp')
	END !IF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT 
#! 
#!
#! ****************************************************************************************
#! Thin@ - Noyantis support: ChartPro, CommandBars and ShortcutBar support
#! ****************************************************************************************
#! ----------------------------------------------------------------------------------------
#! Global Noyantis Support Section
#! ----------------------------------------------------------------------------------------
#!
#!Active template test
#!#AT (%DataSectionBeforeWindow), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
#!  #FOR(%ActiveTemplate)
#!  #CONTEXT(%Procedure,%ActiveTemplate)
#!    ActiveTemplate: %ActiveTemplate
#!      #ENDCONTEXT
#!  #ENDFOR
#!#ENDAT
#!   
#!
#ATSTART
  #IF(%TARGET32 AND %ThinNETEnabled AND VAREXISTS(%NYSFamily) AND NOT VAREXISTS(%NoyantisControlName))
   #DECLARE(%NYSObject)
   #DECLARE(%NYSCurrentObject)
   #DECLARE(%NoyantisControlName)  
   #DECLARE(%NoyantisProcedure),MULTI
  #ENDIF  
#ENDAT
#!
#AT (%DataSectionBeforeWindow), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='NYS_ChartPro(NYS_ChartProTpl)' OR %ActiveTemplate='NYS_ShortcutBarNav(NYS_ShortcutBarTpl)'  OR %ActiveTemplate='NYS_ShortcutBarFrame(NYS_ShortcutBarTpl)'  OR %ActiveTemplate='NYS_CommandBarsNav(NYS_CommandBarsTpl)' OR %ActiveTemplate='NYS_CommandBarsCtrl(NYS_CommandBarsTpl)' OR %ActiveTemplate='NYS_CommandBarsFrame(NYS_CommandBarsTpl)' OR INSTRING('G_CALC', UPPER(%ActiveTemplate),1,1)<>0)
  #CONTEXT(%Procedure,%ActiveTemplate)
  #FIND(%NoyantisProcedure,%Procedure)
  #IF(%NoyantisProcedure='')
  #ADD(%NoyantisProcedure, %Procedure)
pNoyantisThread  SIGNED,AUTO ! Thin@ Noyantis support
NoyantisWind &Window ! Thin@ Noyantis support
vParamName       CSTRING(500) ! Thin@ Noyantis support
  #ENDIF    
      #ENDCONTEXT
    #ENDFOR
#ENDAT
#!
#!
#! Global Noyantis support code BEFORE PARENT call
#!   
#AT(%VirtualProcs,,, 'CODE'),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND VAREXISTS(%NYSObject)), FIRST
  #IF(%VMQMethod ='InitOCX' OR %VMQMethod='OCXDirectCommand' OR %VMQMethod='OCXDirectValue' OR %VMQMethod ='RegisterEventProc' OR %VMQMethod ='GetOCXLastEventName' OR %VMQMethod ='GetOCXParamCount' OR %VMQMethod ='GetOCXParam' OR %VMQMethod ='TakeWindowEvent' OR %VMQMethod ='GetHandle' OR %VMQMethod ='AddNavMenu' OR %VMQMethod ='OCXBind' OR %VMQMethod ='OCXUnbind' OR %VMQMethod ='AddIcon' OR %VMQMethod ='LoadEXEIcon' OR %VMQMethod ='AddIconCalcLoadCmd' OR %VMQMethod ='MDISetChildSelections' OR %VMQMethod ='MDISetChildSelected' OR %VMQMethod ='MDICtrlsShow' OR %VMQMethod ='TakeEvent' OR %VMQMethod ='CalcOverallHeight')
    #FOR(%ActiveTemplate), WHERE(INSTRING('G_CALC', UPPER(%ActiveTemplate),1,1)<>0 OR %ActiveTemplate='NYS_ShortcutBarFrame(NYS_ShortcutBarTpl)'  OR %ActiveTemplate='NYS_ShortcutBarNav(NYS_ShortcutBarTpl)'  OR %ActiveTemplate='NYS_ChartPro(NYS_ChartProTpl)' OR %ActiveTemplate='NYS_CommandBarsNav(NYS_CommandBarsTpl)' OR %ActiveTemplate='NYS_CommandBarsCtrl(NYS_CommandBarsTpl)' OR %ActiveTemplate='NYS_CommandBarsFrame(NYS_CommandBarsTpl)' OR INSTRING('G_CALC', UPPER(%ActiveTemplate),1,1)<>0)
    #INSERT(%CheckNoyantisControlName)
      #SET(%NYSObject,%NYSCurrentObject)
    #FOR(%ActiveTemplateInstance)
      #CONTEXT(%Procedure,%ActiveTemplateInstance)
        #IF(%ActiveTemplate<>'NYS_ShortcutBarFrame(NYS_ShortcutBarTpl)' AND %ActiveTemplate<>'NYS_CommandBarsFrame(NYS_CommandBarsTpl)')
          #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
            #INSERT(%NYSFirst)
         #ENDFOR
        #ELSE
          #INSERT(%NYSFirst)
        #ENDIF
      #ENDCONTEXT
    #ENDFOR
  #ENDFOR      
  #ENDIF
#ENDAT
#!
#! Global Noyantis support code AFTER PARENT call
#! 
#AT(%VirtualProcs,,, 'CODE'),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND VAREXISTS(%NYSObject)), LAST
  #IF(%VMQMethod ='InitOCX' OR %VMQMethod='OCXDirectCommand' OR %VMQMethod='OCXDirectValue' OR %VMQMethod ='RegisterEventProc' OR %VMQMethod ='GetOCXLastEventName' OR %VMQMethod ='GetOCXParamCount' OR %VMQMethod ='GetOCXParam' OR %VMQMethod ='TakeWindowEvent' OR %VMQMethod ='GetHandle' OR %VMQMethod ='AddNavMenu' OR %VMQMethod ='OCXBind' OR %VMQMethod ='OCXUnbind' OR %VMQMethod ='AddIcon' OR %VMQMethod ='LoadEXEIcon' OR %VMQMethod ='AddIconCalcLoadCmd' OR %VMQMethod ='MDISetChildSelections' OR %VMQMethod ='MDISetChildSelected' OR %VMQMethod ='MDICtrlsShow' OR %VMQMethod ='TakeEvent' OR %VMQMethod ='CalcOverallHeight')
    #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='NYS_ShortcutBarFrame(NYS_ShortcutBarTpl)'  OR %ActiveTemplate='NYS_ShortcutBarNav(NYS_ShortcutBarTpl)'  OR %ActiveTemplate='NYS_ChartPro(NYS_ChartProTpl)' OR %ActiveTemplate='NYS_CommandBarsNav(NYS_CommandBarsTpl)' OR %ActiveTemplate='NYS_CommandBarsCtrl(NYS_CommandBarsTpl)' OR %ActiveTemplate='NYS_CommandBarsFrame(NYS_CommandBarsTpl)' OR INSTRING('G_CALC', UPPER(%ActiveTemplate),1,1)<>0)
    #FOR(%ActiveTemplateInstance)
      #CONTEXT(%Procedure,%ActiveTemplateInstance)
        #IF(%ActiveTemplate<>'NYS_ShortcutBarFrame(NYS_ShortcutBarTpl)' AND %ActiveTemplate<>'NYS_CommandBarsFrame(NYS_CommandBarsTpl)')
          #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
            #INSERT(%NYSLast)
        #ENDFOR
        #ELSE
          #INSERT(%NYSLast)
        #ENDIF
      #ENDCONTEXT
    #ENDFOR
  #ENDFOR      
  #ENDIF 
#ENDAT
#!
#! ----------------------------------------------------------------------------------------
#! Noyantis Support Extensions
#! ----------------------------------------------------------------------------------------
#!
#! ChartPro (SaveAsImage) support Extension 
#!
#AT (%DataSectionBeforeWindow), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND %ChartProTranferImages)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='NYS_ChartPro(NYS_ChartProTpl)')
  #CONTEXT(%Procedure,%ActiveTemplate)
  #ENDCONTEXT
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
ChartProGraphFEQ%ActiveTemplateInstance SIGNED !Thin@ ChartPro support
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT
#!
#AT(%AfterWindowOpening), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND %ChartProTranferImages)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='NYS_ChartPro(NYS_ChartProTpl)')
  IF ThinNetMgr.Active THEN !Thin@ ChartPro support
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)
      #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
      ! Thin@ - replicate the ChartPro OCX control into images
  	  ChartProGraphFEQ%ActiveTemplateInstance = CREATE(0, CREATE:IMAGE, %Control{PROP:PARENT})
  	  ChartProGraphFEQ%ActiveTemplateInstance{PROP:use} = ChartProGraphFEQ%ActiveTemplateInstance
  	  GETPOSITION(%Control,X#,Y#,W#,H#)
  	  ChartProGraphFEQ%ActiveTemplateInstance{PROP:XPos}=X#
  	  ChartProGraphFEQ%ActiveTemplateInstance{PROP:YPos}=Y#+50
  	  ChartProGraphFEQ%ActiveTemplateInstance{PROP:Width}=W#
  	  ChartProGraphFEQ%ActiveTemplateInstance{PROP:Height}=H#
  	  UNHIDE(ChartProGraphFEQ%ActiveTemplateInstance)
      #ENDFOR
    #ENDCONTEXT
    #ENDFOR
  END !IF
  #ENDFOR
#ENDAT
#! 
#AT(%VirtualProcs,,'InitComplete', 'CODE'), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND (VAREXISTS(%NYS012AppDisable) AND %ChartProTranferImages))
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='NYS_ChartPro(NYS_ChartProTpl)')
    #INSERT(%CheckNoyantisControlName)
    #SET(%NYSObject,%NYSCurrentObject)  
    #FOR(%ActiveTemplateInstance)
      #CONTEXT(%Procedure,%ActiveTemplateInstance)
      #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
        #INSERT(%CheckNoyantisControlName)
        #IF(%NYSCurrentObject = %NYSObject)
  ! Thin@ ChartPro support
  IF (ThinNetMgr.Active AND %Control{prop:visible} <> '') THEN  
    IF ThinNETMgr.State=0 THEN
      ThinNETMgr.RecieveDataFromClient()
      ThinNETMgr.State = 1
    END
     !PARENT.Draw()
     ApiCreateDirectory(ThinNetMgr.FileDirPath)
     PARENT.SaveAsImage(ThinNetMgr.FileDirPath & '%NYS012OCXObj.bmp', ChartProGraphFEQ%ActiveTemplateInstance{prop:width}*1.5, ChartProGraphFEQ%ActiveTemplateInstance{prop:height}*1.63)
     ChangedCheck#=1
     IF RisNet:DownloadFile(ThinNetMgr.FileDirPath & '%NYS012OCXObj.bmp','',,,ChangedCheck#).
     ! Only refresh image on the client if image changed
     IF ChangedCheck# THEN
       ThinNetMgr.DisplayControlImage(ChartProGraphFEQ%ControlInstance)
       ChartProGraphFEQ%ControlInstance{PROP:TEXT} = ThinNetMgr.FileDirPath & '%NYS012OCXObj.bmp'
     END
     REMOVE(ThinNetMgr.FileDirPath & '%NYS012OCXObj.bmp')
	END !IF
	      #ENDIF
      #ENDFOR
      #ENDCONTEXT
    #ENDFOR
  #ENDFOR  
#ENDAT 
#!
#!
#!
#! ****************************************************************************************
#! Thin@ - Clarion Toolbar support
#! ****************************************************************************************
#AT(%WindowManagerMethodCodeSection,'TakeAccepted','(),BYTE'),PRIORITY(3040),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
#FOR(%ActiveTemplate)
#! UltraTreeToolbarButtons(UltraC4)
#IF(%ActiveTemplate='FrameBrowseControl(ABC)')
    IF ThinNetMgr.PreviousThread <> THREAD()
      ThinNetMgr.Display(,,ThinNetMgr.PreviousHandle)
      ThinNetMgr.CurrentThread=ThinNetMgr.PreviousThread  
      POST(EVENT:Accepted,ACCEPTED(),ThinNetMgr.PreviousThread)
      CYCLE
    END   
#ENDIF
#ENDFOR
#ENDAT
#!
#!
#! ----------------------------------------------------------------------------------------
#! Fomin report support
#! ----------------------------------------------------------------------------------------
#AT(%ProcedureSetup),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND %ProcedureTemplate = 'RunTimeReport')
! Thin@
IF ThinNETMgr.Active THEN
#!   IF ThinNetMgr.State=0 THEN ThinNETMgr.RecieveDataFromClient().
   ThinNETMgr.StartLongRunningProcess()
END
#ENDAT
#!
#AT(%BeforePrintPreview),WHERE(%ProcedureTemplate = 'RunTimeReport' AND %ThinNETEnabled AND %ThinNETProcEnabled),LAST
! Thin@
IF ThinNETMgr.Active AND LocalResponse = RequestCompleted THEN
  ThinNETMgr.EndLongRunningProcess()
  ThinNETMgr.DownloadFiles(PrintPreviewQueue, PrintPreviewQueue.FileName,'REPORT<-4->'& %Report{PROP:Landscape} & '<-5->' & FRB::SkipPreview)
  FREE(PrintPreviewQueue)
ELSIF ThinNETMgr.Active  THEN
  ThinNETMgr.EndLongRunningProcess()
ELSE
#ENDAT
#AT(%BeforeClosingReport),WHERE(%ProcedureTemplate = 'RunTimeReport' AND %ThinNETEnabled AND %ThinNETProcEnabled),FIRST
! Thin@
END 
IF ThinNETMgr.Active  THEN
   ThinNETMgr.EndLongRunningProcess()
   ThinNETMgr.CloseReport()
END
#ENDAT
#! End Fomin report support 
#!
#!
#!
#! ----------------------------------------------------------------------------------------
#! CPCS report support
#! ----------------------------------------------------------------------------------------
#!#AT(%ProcedureSetup),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND (%ProcedureTemplate = 'UnivReport' OR %ProcedureTemplate = 'UnivAbcReport'))
#AT(%AfterOpeningWindow),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND (%ProcedureTemplate = 'UnivReport' OR %ProcedureTemplate = 'UnivAbcReport'))
  ! Thin@
  IF ThinNETMGR.Active THEN ThinNETMgr.Openwindow(ProgressWindow).
#PRIORITY(7001)
  ! Thin@
  IF ThinNETMGR.Active THEN ThinNETMgr.StartLongRunningProcess().
#ENDAT
#! 
#AT(%EndOfReportGeneration),WHERE((%ProcedureTemplate = 'UnivReport' OR %ProcedureTemplate = 'UnivAbcReport') AND %ThinNETEnabled AND %ThinNETProcEnabled),LAST
! Thin@
  IF ThinNETMgr.Active THEN !AND LocalResponse = RequestCompleted
    ENDPAGE(Report)
    ThinNETMgr.EndLongRunningProcess()
    IF PreviewReq AND ~RECORDS(PrintPreviewQueue)
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation,CPCS:ButtonOk)
    ELSIF ~PreviewReq AND ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation,CPCS:ButtonOk)
    ELSE
      ThinNETMgr.DownloadFiles(PrintPreviewQueue, PrintPreviewQueue.FileName,'REPORT<-4->'& Report{PROP:Landscape} & '<-5->' & CHOOSE(PreviewReq=0,1,0))
    END
    FREE(PrintPreviewQueue)
  ELSIF ThinNETMgr.Active  THEN
    ThinNETMgr.EndLongRunningProcess()
  ELSE
#ENDAT
#!
#AT(%BeforeClosingReport),WHERE((%ProcedureTemplate = 'UnivReport' OR %ProcedureTemplate = 'UnivAbcReport') AND %ThinNETEnabled AND %ThinNETProcEnabled),FIRST
! Thin@
END 
IF ThinNETMgr.Active  THEN
   ThinNETMgr.EndLongRunningProcess()
#!   ThinNETMgr.CloseReport()
END
#ENDAT
#!
#AT(%AfterClosingReport),WHERE((%ProcedureTemplate = 'UnivReport' OR %ProcedureTemplate = 'UnivAbcReport') AND %ThinNETEnabled AND %ThinNETProcEnabled),FIRST
! Thin@
   IF ThinNETMGR.Active THEN ThinNETMgr.Closewindow(ProgressWindow).
#ENDAT
#!
#! End CpCS report support 
#!
#!
#! ----------------------------------------------------------------------------------------
#! Lodestar RPM report support
#! ----------------------------------------------------------------------------------------
#!
#AT(%LSiBeforePreview),LAST,WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='rpm_STANDARD_RPT(Lodestar_RPM)')
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)    
 !Thin@ support
 IF ~ThinNetMgr.Active THEN
    #ENDCONTEXT
    #ENDFOR
  #ENDFOR  
#ENDAT
#!
#AT(%LSiAfterPreview), WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='rpm_STANDARD_RPT(Lodestar_RPM)')
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)    
 ELSE
 	 RPMxt_SupportRef &= RPMxt_Support !24.03.2012 - added SetVariables directly to the print preview
   ThinNetMgr.ExecuteClientSource('RPM','SetVariables',RisNet:QueueToXML(RPMQueue), RisNet:QueueToXML(RPMxt_Support.TargetQue),RisNet:GroupToXML(RPMxt_SupportRef), RisNet:GroupToXML(LSiOptions), RisNet:GroupToXML(RPMxt_ExtOpsGroup))
   RPMxt_Support.TargetID = ThinNetMgr.ExecuteClientSource('RPM', 'RPMxt_Support.TargetID')	
 	
   IF RPMxt_Support.TargetID
   		RPMxt_Support.SaveAs(SELF.PreviewQueue)
      GlobalResponse = RequestCompleted
   ELSE
      ThinNetMgr.ExecuteClientSource('RPM','LSiOptions',RisNet:GroupToXML(LSiOptions))
      ThinNetMgr.ExecuteClientSource('RPM','RPMxt_ExtOpsGroup',RisNet:GroupToXML(RPMxt_ExtOpsGroup))
      ENDPAGE(SELF.Report)
      ThinNETMgr.EndLongRunningProcess()
      ThinNETMgr.DownloadFiles(SELF.PreviewQueue, SELF.PreviewQueue.FileName,'REPORT<-4->'& %Report{PROP:Landscape} & '<-5->' & SELF.SkipPreview &'<-6->'& SELF.Zoom)
      FREE(SELF.PreviewQueue)
   END !IF
END !IF ThinNetMgr.Active
    #ENDCONTEXT
    #ENDFOR
  #ENDFOR  
#ENDAT
#!
#AT(%LSiReportCanceled), WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='rpm_STANDARD_RPT(Lodestar_RPM)')
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)   
 !Thin@ support
 IF ThinNetMgr.Active THEN ThinNETMgr.EndLongRunningProcess().
    #ENDCONTEXT
    #ENDFOR
  #ENDFOR  
#ENDAT
#!
#AT(%DestDlgBeforeCall), WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='rpm_STANDARD_RPT(Lodestar_RPM)')
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)  
 !Thin@ support
 IF ~ThinNetMgr.Active THEN
    #ENDCONTEXT
    #ENDFOR
  #ENDFOR   	
#ENDAT
#!
#AT(%DestDlgBeforeCase), WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='rpm_STANDARD_RPT(Lodestar_RPM)')
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)
   ELSE
   	    RPMxt_SupportRef &= RPMxt_Support
   		  ThinNetMgr.ExecuteClientSource('RPM','SetVariables',RisNet:QueueToXML(RPMQueue), RisNet:QueueToXML(RPMxt_Support.TargetQue),RisNet:GroupToXML(RPMxt_SupportRef), RisNet:GroupToXML(LSiOptions), RisNet:GroupToXML(RPMxt_ExtOpsGroup))
        RPM_DestReturn = ThinNetMgr.ExecuteClientSource('RPM','rpm_Destination')
        RPMxt_Support.TargetID = ThinNetMgr.ExecuteClientSource('RPM', 'RPMxt_Support.TargetID')
        !RPMxt_SupportRef &= RPMxt_Support
        !RPM_DestReturn = ThinNetMgr.ExecuteClientSource('RPM','rpm_Destination',RisNet:QueueToXML(RPMQueue), RisNet:QueueToXML(RPMxt_Support.TargetQue),RisNet:GroupToXML(RPMxt_SupportRef), RisNet:GroupToXML(LSiOptions), RisNet:GroupToXML(RPMxt_ExtOpsGroup))
        !RPMxt_Support.TargetID = ThinNetMgr.ExecuteClientSource('RPM', 'RPMxt_Support.TargetID')
   END !IF ThinNetMgr.Active
    #ENDCONTEXT
    #ENDFOR
  #ENDFOR   	   
#ENDAT
#!
#AT(%RPMxtAfterLocalClassDecs), WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='rpm_STANDARD_RPT(Lodestar_RPM)')
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)
RPMxt_SupportRef &RPMxt_Support !Thin@ support
    #ENDCONTEXT
    #ENDFOR
  #ENDFOR   
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'Kill'), PRIORITY(4700), WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='rpm_STANDARD_RPT(Lodestar_RPM)')
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)
 IF ThinNETMgr.Active AND ThinNetMgr.SkipCurrentAccept THEN ThinNetMgr.SkipCurrentAccept -= 1. !Thin@ support
    #ENDCONTEXT
    #ENDFOR
  #ENDFOR    	
#ENDAT
#!
#! End RPM report support 
#!
#! ----------------------------------------------------------------------------------------
#! Report support ABC
#! ----------------------------------------------------------------------------------------
#AT(%WindowManagerMethodCodeSection,'AskPreview'),PRIORITY(3800),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND ~VAREXISTS(%RPMActive))
#IF (%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue')
  ! Thin@
  IF ThinNETMgr.Active AND SELF.Response = RequestCompleted THEN
    ENDPAGE(SELF.Report)
    ThinNETMgr.EndLongRunningProcess()
    ThinNETMgr.DownloadFiles(SELF.PreviewQueue, SELF.PreviewQueue.FileName,'REPORT<-4->'& %Report{PROP:Landscape} & '<-5->' & SELF.SkipPreview &'<-6->'& SELF.Zoom)
    FREE(SELF.PreviewQueue)
    #IF(%EnablePrintPreview=1)
    #!Previewer.PrintOk = ThinNETMgr.Printed
    #ENDIF
  ELSIF ThinNETMgr.Active  THEN
    ThinNETMgr.EndLongRunningProcess()
  ELSE
#ENDIF
#ENDAT
#AT(%WindowManagerMethodCodeSection,'Kill'),PRIORITY(4000),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
#IF (%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue')
  ! Thin@
  IF ThinNETMgr.Active  THEN
     ThinNETMgr.EndLongRunningProcess()
  END
#ENDIF
#ENDAT
#AT(%WindowManagerMethodCodeSection,'AskPreview'),LAST,WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND ~VAREXISTS(%RPMActive))
#IF (%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue')
  ! Thin@
  END
#ENDIF
#ENDAT
#!#AT(%WindowManagerMethodCodeSection,'Init','(),BYTE'),PRIORITY(6030),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled)
#AT(%AfterOpeningWindow),WHERE(%AppTemplateFamily <> 'CLARION' AND %ThinNETEnabled AND %ThinNETProcEnabled)
#IF(%TARGET32 AND %ThinNETEnabled)
#IF (%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue' OR %ProcedureTemplate = 'Process' OR %ProcedureTemplate = 'UnivAbcProcess') 
  ! Thin@
  IF ThinNETMgr.Active THEN
     ThinNETMgr.StartLongRunningProcess()
  END
#ENDIF
#ENDIF
#ENDAT
#!
#!
#!
#! ----------------------------------------------------------------------------------------
#! Report support Legacy
#! ----------------------------------------------------------------------------------------
#AT(%PrintPreviewBefore),WHERE(%AppTemplateFamily = 'CLARION' AND ~VAREXISTS(%RPMActive))
#IF (%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue' )
! Thin@
IF ThinNETMgr.Active THEN
  ENDPAGE(%Report)
  ThinNETMgr.EndLongRunningProcess()
  ThinNETMgr.DownloadFiles(PrintPreviewQueue, PrintPreviewQueue.FileName,'REPORT<-4->'& %Report{PROP:Landscape} & '<-5->' & SkipPreview)
  FREE(PrintPreviewQueue)
ELSIF ThinNETMgr.Active  THEN
  ThinNETMgr.EndLongRunningProcess()
ELSE
#ENDIF
#ENDAT
#AT(%PrintPreviewAfter),WHERE(%AppTemplateFamily = 'CLARION' AND ~VAREXISTS(%RPMActive))
END
#ENDAT
#AT(%AfterOpeningWindow),WHERE(%AppTemplateFamily = 'CLARION' AND %ThinNETEnabled AND %ThinNETProcEnabled)
#IF(%TARGET32)
#IF (%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue' OR %ProcedureTemplate = 'Process' OR %ProcedureTemplate = 'UnivProcess')
! Thin@
IF ThinNETMgr.Active THEN
#!   IF ThinNetMgr.State=0 THEN ThinNETMgr.RecieveDataFromClient().
   ThinNETMgr.StartLongRunningProcess()
END
#ENDIF
#ENDIF
#ENDAT
#AT(%AfterClosingReport),WHERE(%AppTemplateFamily = 'CLARION' AND %ThinNETEnabled AND %ThinNETProcEnabled)
#IF(%TARGET32)
#IF (%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue')
#!! Thin@
#!IF ThinNetMgr.Active THEN ThinNETMgr.CloseReport().
#!END
#ENDIF
#ENDIF
#ENDAT
#!
#!
#! ----------------------------------------------------------------------------------------
#! Window support Legacy
#! -----------------------------------------------------------------------------------------------------------------------------------------------
#AT(%DataSection),WHERE(%AppTemplateFamily = 'CLARION' AND %TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
! Thin@
WindowClosed BYTE
#ENDAT
#AT(%PostWindowEventHandling,'CloseWindow'),WHERE(%AppTemplateFamily = 'CLARION' AND %TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
! Thin@
IF ThinNetMgr.Active THEN WindowClosed = True.
#ENDAT
#AT(%BeforeWindowClosing),WHERE(%AppTemplateFamily = 'CLARION' AND %TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
#IF(%ProcedureTemplate = 'Process' OR %ProcedureTemplate = 'UnivProcess')
#! Process template support
! Thin@
IF ThinNETMgr.Active THEN
   RisNet:ShowProgressDialog(100,100,,0{PROP:Text})
   IF ThinNetMgr.SkipCurrentAccept THEN ThinNetMgr.SkipCurrentAccept -= 1. ! Reset possible thread counter
   ThinNetMgr.EndLongRunningProcess()
END
#ENDIF
#IF(%Window)
#IF(%ProcedureTemplate <> 'UnivReport' AND %ProcedureTemplate <> 'UnivAbcReport')
! Thin@
IF ThinNETMgr.Active THEN ThinNETMgr.CloseWindow(%Window).
#ENDIF
#!! Thin@
#!IF ThinNetMgr.Active AND NOT WindowClosed AND WindowOpened AND %Window{PROP:Handle} THEN
#!  POST(EVENT:CLOSEWINDOW)
#!  ACCEPT
#!    IF EVENT()=EVENT:CLOSEWINDOW THEN ThinNetMgr.TakeEvent(%Window); BREAK.
#!  END
#!END
#ENDIF
#ENDAT
#AT(%AfterWindowOpening),PRIORITY(0),WHERE(%AppTemplateFamily = 'CLARION' AND %TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
#IF(%Window)
#!  #IF(%ProcedureTemplate <> 'Report' AND %ProcedureTemplate <> 'ReportQueue' AND %ProcedureTemplate <> 'UnivReport' AND %ProcedureTemplate <> 'UnivAbcReport')
#IF(%ProcedureTemplate <> 'UnivReport' AND %ProcedureTemplate <> 'UnivAbcReport')
! Thin@
IF ThinNETMgr.Active THEN ThinNETMgr.OpenWindow(%Window).
#ENDIF
#ENDIF
#ENDAT
#AT(%AfterWindowOpening),WHERE(%AppTemplateFamily = 'CLARION' AND %TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
#INDENT(-10)
#DECLARE(%LocalTemp)
#IF(%Window)
! Thin@
#! Options enabled without in non-Thin@ mode
#! Listbox header colors (non-Thin@)
   #FOR(%Control),WHERE(%EnableListboxHeaderColorss=1 AND %EnableWithoutThinet=1 AND (%ControlType='LIST' OR %ControlType='COMBO' OR %ControlType='DROPLIST' OR %ControlType='DROPCOMBO'))
   RisNet:SetListboxHeaderColors(%Window, %Control, 1, %cNormal, %cAccentTxt, %cListHeaderBack, %cListHeaderFore, %cListHeaderGRBack, %cListHeaderGrFore, %cListHeaderArrow, %cListHeaderArrow3D)
   #ENDFOR
#!   
IF ThinNETMgr.Active THEN
  #IF(%ProcedureTemplate <> 'Report' AND %ProcedureTemplate <> 'ReportQueue' AND %ProcedureTemplate <> 'UnivReport' AND %ProcedureTemplate <> 'UnivAbcReport')
   #IF(%ThinNETTimerRefresh)
   ThinNETMgr.AddOption(%Window,0,'forcedtimerrefresh=1')     
   #ENDIF
   #IF(VAREXISTS(%ThinNETTabBarStyle) AND %ThinNETTabBarStyle <> 3)
   ThinNETMgr.AddOption(%Window,0,'tabbarstyle=%ThinNETTabBarStyle')     
   #ENDIF   
#! Tab Style   
   #FOR(%Control),WHERE(%ControlType='SHEET' AND %OverrideGlobalTabStyle=1)
   RisNet:SetTabStyle(%Window, %Control, %SheetTabStyle)
   #ENDFOR
#! Listbox header colors   
   #FOR(%Control),WHERE(%EnableListboxHeaderColorss=1 AND %OverrideGlobalListboxHeaderColors=1 AND %EnableWithoutThinet=0 AND (%ControlType='LIST' OR %ControlType='COMBO' OR %ControlType='DROPLIST' OR %ControlType='DROPCOMBO'))
   RisNet:SetListboxHeaderColors(%Window, %Control, 1, %cNormal, %cAccentTxt, %cListHeaderBack, %cListHeaderFore, %cListHeaderGRBack, %cListHeaderGrFore, %cListHeaderArrow, %cListHeaderArrow3D)
   #ENDFOR
#!      
   #FOR(%Control),WHERE(%ControlType='LIST' OR %ControlType='COMBO' OR %ControlType='SPIN')
      #IF(%HideOnClient=1)
            ThinNETMgr.AddOption(%Window,%Control,4)     
      #ELSE
   #IF(%ControlFrom<>'')
 ThinNETMgr.AddListControl(%Window,%Control,%ControlFrom)
   #ENDIF
      #ENDIF
   #ENDFOR
   #FOR(%Control),WHERE(%ControlType<>'ITEM' AND %ControlType<>'MENU' AND %ControlType<>'SEPARATOR' AND %ControlType<>'MENUBAR')
      #IF(%HideOnClient=1 OR (%ControlType='OLE' AND %ChartProTranferImages=1))
            ThinNETMgr.AddOption(%Window,%Control,4)     
      #ELSE
   #IF(VAREXISTS(%SynOnAccept))
   #IF(%SynOnAccept<>1 OR %SynOnSelect<>1) 
    #IF(%SynOnAccept=1 AND %SynOnSelect=1) 
      #SET(%LocalTemp,3)
    #ELSIF (%SynOnAccept=1) 
      #SET(%LocalTemp,1)
    #ELSIF (%SynOnSelect=1)   	
      #SET(%LocalTemp,2)
    #ELSE
      #SET(%LocalTemp,0)
    #ENDIF
 ThinNETMgr.AddOption(%Window,%Control,%LocalTemp)
   #ENDIF
   #ENDIF
   #! Add size for text and rtf controls
   #IF(VAREXISTS(%ThinControlMaxSize) AND %ControlType='TEXT' AND INSTRING('rtf',LOWER(%ControlStatement),1,1) AND ~%ThinControlMaxSize=0 AND ~%ThinControlMaxSize=250000)
   ThinNETMgr.AddOption(%Window,%Control,'size=%ThinControlMaxSize')     
   #ENDIF
   #IF(VAREXISTS(%ThinControlMaxSize) AND %ControlType='TEXT' AND ~INSTRING('rtf',LOWER(%ControlStatement),1,1) AND ~%ThinControlMaxSize=0 AND ~%ThinControlMaxSize=32000)
   ThinNETMgr.AddOption(%Window,%Control,'size=%ThinControlMaxSize')     
   #ENDIF
   #IF(VAREXISTS(%ThinControlListboxPipes) AND INSTRING(%ControlType,'LIST,COMBO',1,1)<>0 AND ~%ThinControlListboxPipes=0)
   ThinNETMgr.AddOption(%Window,%Control,'parsepipe=1')     
   #ENDIF   
      #ENDIF
  #ENDFOR
  #ENDIF
END
#ENDIF
#INDENT(+10)
#ENDAT
#!
#! Clarion proces support on next record (not needed for ABC)
#AT(%GetNextRecordNextsucceeds),PRIORITY(4000)
 #IF(%ProcedureTemplate = 'Process' OR %ProcedureTemplate = 'UnivProcess')
 IF ThinNETMgr.Active THEN 
 	  IF RisNet:ShowProgressDialog(?%ThermometerUseVariable{PROP:Progress},?%ThermometerUseVariable{PROP:Range,2},1,0{PROP:Text}) THEN POST(EVENT:Accepted, %CancelControl).
 END
 #ENDIF
#ENDAT
#! Window event handling routine
#AT(%AcceptLoopAfterFieldHandling),WHERE(%AppTemplateFamily = 'CLARION' AND %TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
#!#IF(%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue')
#!#ELSE
#IF(%Window)
  #! Nettalk ftp client support
  #IF(~VAREXISTS(%tmpString))
  #DECLARE(%NetFTPOjbect)
  #DECLARE(%tmpString)
  #ENDIF
  #SET(%tmpString,'')
  #FOR(%ActiveTemplate),WHERE(INSTRING('nettalk',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #IF((%NoNetTalk=0) and (%NoThisObj=0) AND (%ABCBaseClass='NetFTPClientControl'))
    #SET(%NetFTPOjbect,%ThisObjectName)
    #SET(%tmpString,%tmpString &CHOOSE(%tmpString<>'',' AND ','')&'~'& %ThisObjectName &'.busy')
  #ENDIF
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
  #! Support for process template
  #IF(%ProcedureTemplate = 'Process' OR %ProcedureTemplate = 'UnivProcess')
 IF ThinNETMgr.Active THEN 
 	  IF RisNet:ShowProgressDialog(?%ThermometerUseVariable{PROP:Progress},?%ThermometerUseVariable{PROP:Range,2},1,0{PROP:Text}) THEN POST(EVENT:Accepted, %CancelControl).
 END
  #ELSE
  #IF(%tmpString<>'')
IF %tmpString THEN
	pTimer#=0{PROP:Timer}
  #ENDIF
! Thin@
#IF(%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue')
IF ThinNETMgr.Active AND TARGET{PROP:Timer}=0 THEN ThinNETMgr.TakeEvent(%Window)
ELSIF ThinNETMgr.Active AND TARGET{PROP:Timer}<>0 THEN 
  IF RisNet:ShowProgressDialog(?%ThermometerUseVariable{PROP:Progress},?%ThermometerUseVariable{PROP:Range,2},1,0{PROP:Text}) THEN 
		 IF ThinNetMgr.SkipCurrentAccept THEN ThinNetMgr.SkipCurrentAccept -= 1.
		 POST(EVENT:Accepted, %CancelControl)
  END
END
#ELSE
IF ThinNETMgr.Active THEN ThinNETMgr.TakeEvent(%Window).
#ENDIF
  #IF(%tmpString<>'')
  0{PROP:Timer}=pTimer#
ELSIF ThinNETMgr.Active THEN RisNet:ShowProgressDialog(%NetFTPOjbect.ProgressControl{PROP:Progress},%NetFTPOjbect.ProgressControl{PROP:Range,2},,'NetTalk processing..').
  #ENDIF
  #ENDIF
#ENDIF
#!#ENDIF
#ENDAT
#! Legacy change error option box - cancel message
#AT(%BeforeUpdateOfPrimary),WHERE(%AppTemplateFamily = 'CLARION' AND %TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
! Thin@ - cancelling possible window close
RisNet:CancelCloseWindow()
#ENDAT
#!
#!
#! ----------------------------------------------------------------------------------------
#! ABC support for windows
#! -----------------------------------------------------------------------------------------------------------------------------------------------
#AT(%WindowManagerMethodCodeSection,'Kill','(),BYTE'),PRIORITY(4800)
#IF(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
#IF(%Window)
#!#IF(%ProcedureTemplate <> 'Report' AND %ProcedureTemplate <> 'ReportQueue' AND %ProcedureTemplate <> 'UnivReport' AND %ProcedureTemplate <> 'UnivAbcReport')
#IF(%ProcedureTemplate = 'Process' OR %ProcedureTemplate = 'UnivAbcProcess')
#! Process template support
! Thin@
IF ThinNETMgr.Active THEN
   RisNet:ShowProgressDialog(100,100,,0{PROP:Text})
   IF ThinNetMgr.SkipCurrentAccept THEN ThinNetMgr.SkipCurrentAccept -= 1. ! Reset possible thread counter
   ThinNetMgr.EndLongRunningProcess()
END
#ENDIF
#IF(%ProcedureTemplate <> 'UnivReport' AND %ProcedureTemplate <> 'UnivAbcReport')
! Thin@
IF ThinNETMgr.Active THEN ThinNETMgr.CloseWindow(%Window).
#ENDIF
#ENDIF
#ENDIF
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'Init','(),BYTE'),PRIORITY(8000),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND %AppTemplateFamily <> 'CLARION')
#DECLARE(%LocalTemp)
#IF(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
#IF(%Window)
#!#IF(%ProcedureTemplate <> 'Report' AND %ProcedureTemplate <> 'ReportQueue' AND %ProcedureTemplate <> 'UnivReport' AND %ProcedureTemplate <> 'UnivAbcReport')
#IF(%ProcedureTemplate <> 'UnivReport' AND %ProcedureTemplate <> 'UnivAbcReport')
! Thin@
IF ThinNETMgr.Active THEN ThinNETMgr.OpenWindow(%Window).
#ENDIF
#ENDIF
#ENDIF
#ENDAT
#AT(%AfterOpeningWindow),WHERE(%AppTemplateFamily <> 'CLARION')
#INDENT(-10)
#IF(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #IF(%ProcedureTemplate <> 'Report' AND %ProcedureTemplate <> 'ReportQueue' AND %ProcedureTemplate <> 'UnivReport' AND %ProcedureTemplate <> 'UnivAbcReport')
! Thin@
#! Options enabled without in non-Thin@ mode
#! Listbox header colors (non-Thin@)
   #FOR(%Control),WHERE(%EnableListboxHeaderColorss=1 AND %EnableWithoutThinet=1 AND (%ControlType='LIST' OR %ControlType='COMBO' OR %ControlType='DROPLIST' OR %ControlType='DROPCOMBO'))
   RisNet:SetListboxHeaderColors(%Window, %Control, 1, %cNormal, %cAccentTxt, %cListHeaderBack, %cListHeaderFore, %cListHeaderGRBack, %cListHeaderGrFore, %cListHeaderArrow, %cListHeaderArrow3D)
   #ENDFOR
#!   
IF ThinNETMgr.Active THEN
   #IF(%ThinNETTimerRefresh)
   ThinNETMgr.AddOption(%Window,0,'forcedtimerrefresh=1')     
   #ENDIF
   #IF(VAREXISTS(%ThinNETTabBarStyle) AND %ThinNETTabBarStyle <> 3)
   ThinNETMgr.AddOption(%Window,0,'tabbarstyle=%ThinNETTabBarStyle')     
   #ENDIF   
#! Tab Style   
   #FOR(%Control),WHERE(%ControlType='SHEET' AND %OverrideGlobalTabStyle=1)
   RisNet:SetTabStyle(%Window, %Control, %SheetTabStyle)
   #ENDFOR
#! Listbox header colors   
   #FOR(%Control),WHERE(%EnableListboxHeaderColorss=1 AND %OverrideGlobalListboxHeaderColors=1 AND %EnableWithoutThinet=0 AND (%ControlType='LIST' OR %ControlType='COMBO' OR %ControlType='DROPLIST' OR %ControlType='DROPCOMBO'))
   RisNet:SetListboxHeaderColors(%Window, %Control, 1, %cNormal, %cAccentTxt, %cListHeaderBack, %cListHeaderFore, %cListHeaderGRBack, %cListHeaderGrFore, %cListHeaderArrow, %cListHeaderArrow3D)
   #ENDFOR
#!
   #FOR(%Control),WHERE(%ControlType='LIST' OR %ControlType='COMBO' OR %ControlType='SPIN')
      #IF(%HideOnClient=1)
            ThinNETMgr.AddOption(%Window,%Control,4)     
      #ELSE
   #IF(%ControlFrom<>'')
   ThinNETMgr.AddListControl(%Window,%Control,%ControlFrom)
   #ENDIF
      #ENDIF
   #ENDFOR
   #FOR(%Control),WHERE(%ControlType<>'ITEM' AND %ControlType<>'MENU' AND %ControlType<>'SEPARATOR' AND %ControlType<>'MENUBAR')
      #IF(%HideOnClient=1 OR (%ControlType='OLE' AND %ChartProTranferImages=1))
            ThinNETMgr.AddOption(%Window,%Control,4)     
      #ELSE
   #IF(VAREXISTS(%SynOnAccept))
   #IF(%SynOnAccept<>1 OR %SynOnSelect<>1) 
    #IF(%SynOnAccept=1 AND %SynOnSelect=1) 
      #SET(%LocalTemp,3)
    #ELSIF (%SynOnAccept=1) 
      #SET(%LocalTemp,1)
    #ELSIF (%SynOnSelect=1)   	
      #SET(%LocalTemp,2)
    #ELSE
      #SET(%LocalTemp,0)
    #ENDIF
   ThinNETMgr.AddOption(%Window,%Control,%LocalTemp)
   #ENDIF
   #ENDIF
   #! Add size for text and rtf controls
   #IF(VAREXISTS(%ThinControlMaxSize) AND %ControlType='TEXT' AND INSTRING('rtf',LOWER(%ControlStatement),1,1) AND ~%ThinControlMaxSize=0 AND ~%ThinControlMaxSize=250000)
   ThinNETMgr.AddOption(%Window,%Control,'size=%ThinControlMaxSize')     
   #ENDIF
   #IF(VAREXISTS(%ThinControlMaxSize) AND %ControlType='TEXT' AND ~INSTRING('rtf',LOWER(%ControlStatement),1,1) AND ~%ThinControlMaxSize=0 AND ~%ThinControlMaxSize=32000)
   ThinNETMgr.AddOption(%Window,%Control,'size=%ThinControlMaxSize')     
   #ENDIF
   #IF(VAREXISTS(%ThinControlListboxPipes) AND INSTRING(%ControlType,'LIST,COMBO',1,1)<>0 AND ~%ThinControlListboxPipes=0)
   ThinNETMgr.AddOption(%Window,%Control,'parsepipe=1')     
   #ENDIF   
      #ENDIF
  #ENDFOR
END !IF ThinNetMgr.Active
  #ENDIF
#ENDIF
#INDENT(+10)
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'TakeEvent','(),BYTE'),PRIORITY(6301) #! ,PRIORITY(2501)
#IF(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
#!#IF(%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue')
#!! Thin@
#!IF ThinNetMgr.Active AND EVENT()=EVENT:CloseWindow THEN ThinNETMgr.CloseReport().
#!#ELSE
! Thin@
  #IF(%ProcedureTemplate <> 'Process' AND %ProcedureTemplate <> 'UnivAbcProcess')
IF ThinNetMgr.Active AND ReturnValue = Level:Notify AND Event() = Event:Completed THEN RisNet:CancelCloseWindow().
  #ENDIF
  #! Nettalk ftp client support
  #IF(~VAREXISTS(%tmpString))
  #DECLARE(%NetFTPOjbect)
  #DECLARE(%tmpString)
  #ENDIF
  #SET(%tmpString,'')
  #FOR(%ActiveTemplate),WHERE(INSTRING('nettalk',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #IF((%NoNetTalk=0) and (%NoThisObj=0) AND (%ABCBaseClass='NetFTPClientControl'))
    #SET(%NetFTPOjbect,%ThisObjectName)
    #SET(%tmpString,%tmpString &CHOOSE(%tmpString<>'',' AND ','')&'~'& %ThisObjectName &'.busy')
  #ENDIF
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
  #! Support for process template
  #IF(%ProcedureTemplate = 'Process' OR %ProcedureTemplate = 'UnivAbcProcess')
 IF ThinNETMgr.Active THEN 
 	  IF RisNet:ShowProgressDialog(?%ThermometerUseVariable{PROP:Progress},?%ThermometerUseVariable{PROP:Range,2},1,0{PROP:Text}) THEN POST(EVENT:Accepted, %CancelControl).
 END
  #ELSE
  #IF(%tmpString<>'')
IF %tmpString THEN
	pTimer#=0{PROP:Timer}
  #ENDIF
! Thin@
#IF(%ProcedureTemplate = 'Report' OR %ProcedureTemplate = 'ReportQueue')
IF ThinNETMgr.Active AND TARGET{PROP:Timer}=0 THEN ThinNETMgr.TakeEvent(%Window)
ELSIF ThinNETMgr.Active AND TARGET{PROP:Timer}<>0 THEN 
  IF RisNet:ShowProgressDialog(?%ThermometerUseVariable{PROP:Progress},?%ThermometerUseVariable{PROP:Range,2},1,0{PROP:Text}) THEN 
		 IF ThinNetMgr.SkipCurrentAccept THEN ThinNetMgr.SkipCurrentAccept -= 1.
		 POST(EVENT:Accepted, %CancelControl)
  END
END
#ELSE
IF ThinNETMgr.Active THEN ThinNETMgr.TakeEvent(%Window).
#ENDIF
  #IF(%tmpString<>'')
0{PROP:Timer}=pTimer#
ELSIF ThinNETMgr.Active THEN RisNet:ShowProgressDialog(%NetFTPOjbect.ProgressControl{PROP:Progress},%NetFTPOjbect.ProgressControl{PROP:Range,2},,'NetTalk processing..').
  #ENDIF
  #ENDIF
#!#ENDIF
#ENDIF
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'TakeCloseEvent','(),BYTE'),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND INSTRING('window',LOWER(%ProcedureTemplate),1,1)),PRIORITY(1)
! Thin@
IF ThinNETMgr.Active THEN
  IF RisNet:ForceCloseWindow() THEN RETURN Level:Benign.
  IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
    IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
      IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
        IF BAND(SELF.CancelAction,Cancel:Save)
          IF BAND(SELF.CancelAction,Cancel:Query)
            CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No+Button:Cancel,Button:Cancel)
            OF Button:Yes
              RisNet:CancelCloseWindow()
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            OF BUTTON:Cancel
              RisNet:CancelCloseWindow()
              RETURN Level:Notify
            END
          ELSE
            RisNet:CancelCloseWindow()
            POST(Event:Accepted,SELF.OKControl)
            RETURN Level:Notify
          END
        ELSE
          IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
            RisNet:CancelCloseWindow()
            RETURN Level:Notify
          END
        END
      END
    END
    IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
      IF SELF.Primary.CancelAutoInc() THEN
        RisNet:CancelCloseWindow()
        RETURN Level:Notify
      END
    END
    IF SELF.LastInsertedPosition
      SELF.Response = RequestCompleted
      SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
    END
  END
  RETURN Level:Benign
END
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'InsertAction','(),BYTE'),WHERE(%ProcedureTemplate <> 'Process' AND %ProcedureTemplate <> 'UnivAbcProcess' AND %ThinNETEnabled AND %ThinNETProcEnabled),PRIORITY(7501)
! Thin@ support
IF ThinNETMgr.Active THEN
	IF ~ReturnValue = Level:Benign OR (ReturnValue = Level:Benign AND SELF.Response<>RequestCompleted) THEN RisNet:CancelCloseWindow().
END
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'DeleteAction','(),BYTE'),WHERE(%ProcedureTemplate <> 'Process' AND %ProcedureTemplate <> 'UnivAbcProcess' AND  %ThinNETEnabled AND %ThinNETProcEnabled),PRIORITY(7501)
! Thin@ support
IF ThinNETMgr.Active THEN
	IF ~ReturnValue = Level:Benign OR (ReturnValue = Level:Benign AND SELF.Response<>RequestCompleted) THEN RisNet:CancelCloseWindow().
END
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'ChangeAction','(),BYTE'),WHERE(%ProcedureTemplate <> 'Process' AND %ProcedureTemplate <> 'UnivAbcProcess' AND %ThinNETEnabled AND %ThinNETProcEnabled),PRIORITY(7501)
! Thin@ support
IF ThinNETMgr.Active THEN
	IF ~ReturnValue = Level:Benign OR (ReturnValue = Level:Benign AND SELF.Response<>RequestCompleted) THEN RisNet:CancelCloseWindow().
END
#ENDAT
#!
#! ----------------------------------------------------------------------------------------
#! Thin@ - Edit In Place support
#! ----------------------------------------------------------------------------------------
#AT(%BrowserMethodCodeSection),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND %AppTemplateFamily <> 'CLARION') 
#PRIORITY(50)
  #IF(NOT VAREXISTS(%pActiveInstance))
  #DECLARE(%pActiveInstance)
  #ENDIF
  #IF(%pClassMethod='TakeEvent' AND %pClassMethodPrototype='()')
  #SET(%pActiveInstance,%ControlInstance)
  #FOR(%ActiveTemplate),WHERE(INSTRING('browseupdatebuttons',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance = %pActiveInstance AND VAREXISTS(%EditInPlace) AND %EditInPlace)  
  ! Thin@
IF ThinNETMgr.Active AND FIELD()=0 THEN CurrentChoice# = SELF.CurrentChoice.
  #BREAK
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
  #ENDIF
#PRIORITY(5001)
  #IF(%pClassMethod='TakeEvent' AND %pClassMethodPrototype='()')
  #FOR(%ActiveTemplate),WHERE(INSTRING('browseupdatebuttons',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance = %pActiveInstance AND VAREXISTS(%EditInPlace) AND %EditInPlace)  
  ! Thin@
IF ThinNETMgr.Active AND FIELD()=0 THEN SELF.CurrentChoice = CurrentChoice#.
  #BREAK
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
  #ENDIF
#ENDAT
#!
#AT(%BrowserEIPManagerMethodCodeSection) 
#PRIORITY(7500)
#IF(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
   #IF(%EditInPlace AND %pClassMethod='TakeEvent' AND %pClassMethodPrototype='(),BYTE')
     #IF(%AppTemplateFamily <> 'CLARION') 
 ! Thin@
IF ReturnValue=Level:Benign THEN ThisWindow.TakeEvent() ELSE
IF EVENT()<>EVENT:CLOSEWINDOW THEN IF ThinNETMgr.Active THEN ThinNETMgr.TakeEvent(%Window)...
     #ELSE
IF EVENT()<>EVENT:CLOSEWINDOW THEN IF ThinNETMgr.Active THEN ThinNETMgr.TakeEvent(%Window)..
	   #ENDIF     
   #ENDIF  
#ENDIF
#PRIORITY(5001)
#IF(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
    #IF(%EditInPlace)
        #IF(%pClassMethod='ResetColumn')
! Thin@
IF ThinNETMgr.Active THEN ThinNETMgr.AddListEIPControl(SELF.ListControl,SELF.EQ.Column,SELF.EQ.Control.Feq).
        #ENDIF  
        #IF(%pClassMethod='ClearColumn')
! Thin@
IF ThinNETMgr.Active AND SELF.LastColumn=0 THEN ThinNETMgr.AddListEIPControl(SELF.ListControl,SELF.Column,0).	
        #ENDIF          
    #ENDIF
#ENDIF
#ENDAT
#!
#AT(%EditInPlaceManagerMethodCodeSection) 
#PRIORITY(7800)
#IF(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
    #IF(%EditInPlace)
        #IF(%pClassMethod='CreateControl')
! Thin@
IF ThinNETMgr.Active AND (SELF.Feq{PROP:Type}=CREATE:List OR SELF.Feq{PROP:Type}=CREATE:DropList OR |
	 SELF.Feq{PROP:Type}= CREATE:Combo OR SELF.Feq{PROP:Type}=CREATE:DropCombo) THEN ThinNETMgr.AddListControl(%Window,SELF.Feq,SELF.Feq{PROP:FROM}).
        #ENDIF  
    #ENDIF
#ENDIF
#ENDAT
#! 
#! ----------------------------------------------------------------------------------------
#! Thin@ - listbox column header sorting support
#! ----------------------------------------------------------------------------------------
#AT(%WindowManagerMethodDataSection,'TakeEvent','(),BYTE'),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
#IF(NOT VAREXISTS(%ExistsColumnHeader))
    #DECLARE(%ExistsColumnHeader)
  #ENDIF
  #SET(%ExistsColumnHeader,0)
  #FOR(%ActiveTemplate),WHERE(INSTRING('browsebox',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
  #IF(VAREXISTS(%EnableSortHeader))
  #IF(%EnableSortHeader)  
    #SET(%ExistsColumnHeader,1)
  #ENDIF
  #ENDIF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
  #IF(%ExistsColumnHeader=1)
! Thin@ listbox column sort header support
pSortHeaderClass &SortHeaderClassType
A                ANY
Label            STRING(300)
  #ENDIF
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'TakeEvent','(),BYTE'),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled),PRIORITY(3000) 
  #FOR(%ActiveTemplate),WHERE(INSTRING('browsebox',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
  #IF(VAREXISTS(%EnableSortHeader))
  #IF(%EnableSortHeader)
! Thin@ listbox column sort header support
IF ThinNETMgr.Active THEN
  IF EVENT()=EVENT:AlertKey AND (KEYCODE()=MouseLeft OR KEYCODE()=ShiftMouseLeft OR KEYCODE()=CtrlMouseLeft) AND |
     %Control{PROP:Visible} AND INRANGE(MOUSEX(),%Control{PROP:XPos},%Control{PROP:XPos}+%Control{PROP:Width}-1) AND |
     INRANGE(MOUSEY(),%Control{PROP:YPos},%Control{PROP:YPos}+%Control{PROP:HeaderHeight}*5) AND |
     %Control{PROPLIST:MouseDownZone} = LISTZONE:header AND %Control{PROPLIST:MouseDownField}>0 THEN
     IF KEYCODE()=ShiftMouseLeft THEN ThinNETMgr.MouseY = %Control{PROP:Ypos}+5.
     n#=0; pSortHeaderClass &= BRW%ActiveTemplateInstance::SortHeader
     LOOP
       n# += 1
       Label = WHO(pSortHeaderClass,n#)
       IF NOT Label
         A &= WHAT(pSortHeaderClass,n#) 
         IF A &= NULL THEN Break.
         A &= NULL         
       END
       IF LOWER(Label) = 'regioncontrol' THEN 
         A &= WHAT(pSortHeaderClass,n#)
         IF NOT A &= NULL AND A>0 THEN POST(EVENT:MouseUp,A).
       	 BREAK
       END
     END
  END
END  
  #ENDIF
  #ENDIF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#! ----------------------------------------------------------------------------------------
#! Thin@ - Query By Example support
#! ----------------------------------------------------------------------------------------
#AT(%ProcedureSetup),WHERE(%TARGET32 AND %ThinNetEnabled AND %ThinNetProcEnabled)
  #FOR(%ActiveTemplate)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #IF(VAREXISTS(%QBEFamily) AND %QBEFamily<>'')
    #ADD(%ModuleIncludeList,'RisAbQuery.INC')
  #ENDIF
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#AT(%LocalDataClasses),WHERE(%TARGET32 AND %ThinNetEnabled AND %ThinNetProcEnabled)
  #IF(NOT VAREXISTS(%pTemp))
  #DECLARE(%pTemp)
  #DECLARE(%pTemp2)
  #ENDIF
  #FOR(%ActiveTemplate)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
  #IF(VAREXISTS(%QBEFamily) AND %QBEFamily<>'')
  #IF(%Interface='FORM')
  #SET(%pTemp,'RisQueryFormClass')
  #SET(%pTemp2,'RisQueryFormVisual')
  #ELSE
  #SET(%pTemp,'RisQueryListClass')
  #SET(%pTemp2,'RisQueryListVisual')
  #ENDIF
! Thin@ Query by example class declaration

RisQBE%ControlInstance CLASS(%pTemp)  ! Thin@ Qbe class
  END

RisQBV%ControlInstance CLASS(%pTemp2) ! Thin@ Qbe visual class
TakeEvent PROCEDURE(),BYTE,PROC,DERIVED
 END
  #IF(%Interface<>'FORM')

RisQBVEIP%ControlInstance CLASS(RisQEIPManager)
ResetColumn          PROCEDURE,DERIVED
ClearColumn          PROCEDURE,DERIVED
TakeEvent            PROCEDURE,DERIVED,BYTE,PROC
AddListControl       PROCEDURE(LONG Control,*QUEUE QueueSource),DERIVED
 END
  #ENDIF
   
  #ENDIF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'Init','(),BYTE'),PRIORITY(8125),AUTO
  #FOR(%ActiveTemplate)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
  #IF(VAREXISTS(%QBEFamily) AND %QBEFamily<>'')
! Thin@ Query by example intialization
  #IF(%Interface<>'FORM')
RisQBV%ControlInstance.EIP &= RisQBVEIP%ControlInstance
  #ENDIF
RisQBE%ControlInstance.Init(RisQBV%ControlInstance, INIMgr, '%QBEFamily', GlobalErrors)
  #IF(%QkSupport) #! jvn specific to Browse.
RisQBE%ControlInstance.QkSupport = True
  #IF(%QkMenuIcon)
RisQBE%ControlInstance.QkMenuIcon = '%QkMenuIcon'
  #ENDIF
  #IF(%QkIcon)
RisQBE%ControlInstance.QkIcon = '%QkIcon'
  #ENDIF
  #ENDIF
  #ENDIF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#AT(%WindowManagerMethodCodeSection,'Init','(),BYTE'),PRIORITY(8505),AUTO
  #FOR(%ActiveTemplate)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
  #IF(VAREXISTS(%QBEFamily) AND %QBEFamily<>'')

! Thin@ Query by example support
  #IF(%AutoPopulate)
%ManagerName.UpdateQuery(RisQBE%ControlInstance,%CaselessAutoPopulate)
  #ELSE
%ManagerName.Query &= RisQBE%ControlInstance
  #FOR(%QBEFields)
    #CALL(%QBESetTitle(ABC),%QBEField,%QBETitle)
    #IF(%QBEPicture)
RisQBE%ControlInstance.AddItem('%(CHOOSE(%QBENoCase,'UPPER('&%QBEField&')',%QBEField))','%'QBETitle','%QBEPicture',%QBEForceEditPicture)
    #ELSE
       #CALL(%QBESetPicture(ABC),%QBEField,%QBEPicture)
RisQBE%ControlInstancee.AddItem('%(CHOOSE(%QBENoCase,'UPPER('&%QBEField&')',%QBEField))','%'QBETitle','%QBEPicture',%QBEForceEditPicture)
    #ENDIF
  #ENDFOR
  #ENDIF
  #ENDIF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#AT(%LocalProcedures),PRIORITY(8000),WHERE(%TARGET32 AND %ThinNetEnabled AND %ThinNetProcEnabled)
  #FOR(%ActiveTemplate)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
  #IF(VAREXISTS(%QBEFamily) AND %QBEFamily<>'')

! Thin@ Query by example procedure definition

RisQBV%ControlInstance.TakeEvent PROCEDURE
 CODE
  ReturnValue#=PARENT.TakeEvent()

  IF EVENT()=EVENT:OpenWindow THEN
  #IF(%Interface<>'FORM')
   IF ThinNetMgr.Active THEN ThinNetMgr.AddListControl(RisQBE%ControlInstance.Window,FEQ:ListBox,SELF.MyValsQueue).
  #ENDIF
   IF ThinNetMgr.Active THEN ThinNetMgr.AddListControl(RisQBE%ControlInstance.Window,FEQ:SaveListBox,SELF.MyQueries).
  END

  IF ThinNetMgr.Active THEN ThinNetMgr.TakeEvent(RisQBV%ControlInstance.MyWindow).

  RETURN ReturnValue#

  #IF(%Interface<>'FORM')
RisQBVEIP%ControlInstance.TakeEvent PROCEDURE
 CODE
  ReturnValue#=PARENT.TakeEvent()

  IF ReturnValue#=Level:Benign THEN RISQBV%ControlInstance.TakeEvent() ELSE
  IF EVENT()<>EVENT:CLOSEWINDOW THEN IF ThinNetMgr.Active THEN ThinNetMgr.TakeEvent(RisQBV%ControlInstance.MyWindow)...

  RETURN ReturnValue#

RisQBVEIP%ControlInstance.ResetColumn           PROCEDURE
 CODE
   PARENT.ResetColumn

   IF ThinNETMgr.Active THEN 
   	  ThinNETMgr.AddListEIPControl(SELF.ListControl,SELF.EQ.Column,SELF.EQ.Control.Feq)
   END
 
RisQBVEIP%ControlInstance.ClearColumn           PROCEDURE
 CODE
   IF ThinNETMgr.Active AND SELF.LastColumn THEN ThinNETMgr.AddListEIPControl(SELF.ListControl,SELF.Column,0).	

   PARENT.ClearColumn


RisQBVEIP%ControlInstance.AddListControl PROCEDURE(LONG Control,*QUEUE QueueSource)
 CODE
   IF ThinNetMgr.Active THEN ThinNetMgr.AddListControl(RisQBV%ControlInstance.MyWindow,Control,QueueSource).
   	
  #ENDIF
  #ENDIF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#! 
#! ----------------------------------------------------------------------------------------
#! Thin@ - Standard clarion calendar class support
#! ----------------------------------------------------------------------------------------
#AT(%ProcedureSetup),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate),WHERE(INSTRING('calendarbutton',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #ADD(%ModuleIncludeList,'RisCalendarClass.INC')
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#AT(%LocalDataClasses),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate),WHERE(INSTRING('calendarbutton',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
  ! Thin@ support
Ris%[17]CalendarObjectName CLASS(RisCalendarClass)
#!SetUp                  PROCEDURE(),DERIVED                 
#!TakeEvent              PROCEDURE(),PROC,DERIVED            
                     END
                     
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT
#!
#AT(%ControlEventHandling,,'Accepted'),PRIORITY(5000)
  #FOR(%ActiveTemplate),WHERE(INSTRING('calendarbutton',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #IF(NOT VAREXISTS(%pActiveControl))
  #DECLARE(%pActiveControl)
  #ENDIF
  #SET(%pActiveControl,%Control)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance AND %Control=%pActiveControl)
IF ThinNetMgr.Active THEN
#IF(%GlobalChangeColor)
Ris%CalendarObjectName.SetColor(%GlobalColorSaturday,%GlobalColorSunday,%GlobalColorOther,%GlobalColorHoliday)
#ENDIF
#CASE(%GlobalSelectOnClose)
#OF('Select')
Ris%CalendarObjectName.SelectOnClose = True
#OF('Cancel')
Ris%CalendarObjectName.SelectOnClose = False
#ENDCASE
#IF(%CalendarEntryUse)
Ris%CalendarObjectName.Ask('%'CalendarTitle',%CalendarEntryUse)
#ELSE
Ris%CalendarObjectName.Ask('%'CalendarTitle')
#ENDIF
ELSE
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#PRIORITY(5050)
  #FOR(%ActiveTemplate),WHERE(INSTRING('calendarbutton',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #IF(NOT VAREXISTS(%pActiveControl))
  #DECLARE(%pActiveControl)
  #ENDIF
  #SET(%pActiveControl,%Control)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance AND %Control=%pActiveControl)
END
IF ThinNetMgr.Active THEN
IF Ris%CalendarObjectName.Response = RequestCompleted THEN
#IF(%CalendarEntryUse)
%CalendarEntryUse=Ris%CalendarObjectName.SelectedDate
DISPLAY(%LookControl)
#ENDIF
#IF(%RefreshWindow)
#IF(~%AppTemplateFamily='CLARION')
%WindowManager.Reset(True)
#ENDIF
#ENDIF
END
ELSE
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#PRIORITY(6000)
  #FOR(%ActiveTemplate),WHERE(INSTRING('calendarbutton',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #IF(NOT VAREXISTS(%pActiveControl))
  #DECLARE(%pActiveControl)
  #ENDIF
  #SET(%pActiveControl,%Control)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance AND %Control=%pActiveControl)
END
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT
#!
#! ----------------------------------------------------------------------------------------
#! Thin@ - RTF control support
#! ----------------------------------------------------------------------------------------
#AT(%WindowManagerMethodCodeSection,'Init','(),BYTE'),PRIORITY(8470),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND ~%AppTemplateFamily='CLARION')
  #FOR(%ActiveTemplate),WHERE(INSTRING('rtftextcontrol',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)  
#?! Thin@ - %RTFControl RTF control
IF ThinNetMgr.Active THEN ThinNetMgr.AddRTFControl(%Window, %RTFControl, %RTFControlObjectName).
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT
#!
#! Thin@ - Legacy RTF control support
#AT(%AfterControlsSetup),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND %AppTemplateFamily='CLARION')
  #FOR(%ActiveTemplate),WHERE(INSTRING('rtftextcontrol',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)  
#?! Thin@ - %RTFControl RTF control
IF ThinNetMgr.Active THEN ThinNetMgr.AddRTFControl(%Window, %RTFControl, %RTFControlObjectName).
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT
#!
#! ----------------------------------------------------------------------------------------
#! Thin@ - listbox format manager support
#! ----------------------------------------------------------------------------------------
#AT(%ProcedureSetup),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #IF(VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager)
    #ADD(%ModuleIncludeList,'RisFormatManager.INC')
  #ENDIF
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#AT(%DataSectionBeforeWindow),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate),WHERE(INSTRING('browsebox',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
    #IF(VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager)
! Thin@ list format manager declaration
BRW%ActiveTemplateInstance::RisFormatManager CLASS(RisListFormatManagerClass)
AddListControl              PROCEDURE(*Window WindowHandle,LONG ListBox, *QUEUE ListQueue),DERIVED
TakeEvent                   PROCEDURE(*Window WindowHandle),DERIVED
 END
    #ENDIF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#AT(%LocalProcedures),PRIORITY(8000),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate),WHERE(INSTRING('browsebox',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
    #IF(VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager))
    #IF(%GlobalEnableListFormatManager AND NOT %DisableListFormatManager)

! Thin@ list format manager procedures declaration
BRW%ActiveTemplateInstance::RisFormatManager.AddListControl PROCEDURE(*Window WindowHandle,LONG ListBox, *QUEUE ListQueue)
 CODE
  IF ThinNetMgr.Active THEN ThinNetMgr.AddListControl(WindowHandle,ListBox,ListQueue).

BRW%ActiveTemplateInstance::RisFormatManager.TakeEvent PROCEDURE(*Window WindowHandle)
 CODE
  IF ThinNetMgr.Active THEN ThinNetMgr.TakeEvent(WindowHandle).
    #ENDIF
    #ENDIF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#AT(%BeforeWindowClosing),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate),WHERE(INSTRING('browsebox',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #IF(VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager)
! Thin@ List Format Manager destructor
IF ThinNetMgr.Active THEN
   BRW%ActiveTemplateInstance::RisFormatManager.Kill()
END
  #ENDIF
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#ENDAT
#!
#! Legacy templaes listbox format manager support functions
#!
#AT(%PrepareAlerts),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND %AppTemplateFamily = 'CLARION') 
#INSERT(%ListBoxFormatManagerInitialization)
END
#!
#ENDAT
#AT(%BrowseBoxSelectSortDiferentSortAfter),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND %AppTemplateFamily = 'CLARION' AND VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager AND %ListFormatManagerAllowPopupInEmptyList='Enable')
  #FIX(%Control,%ListControl)
! Thin@
IF ThinNetMgr.Active THEN
 IF BRW%ControlInstance::SortOrder <> BRW%ControlInstance::LastSortOrder THEN
   #IF(%ListFormatManagerMenuAppendSortOrder)
   BRW%ControlInstance::RisFormatManager.SetCurrentFormat(CHOOSE(BRW%ControlInstance::SortOrder>%(ITEMS(%SortOrder)),2,BRW%ControlInstance::SortOrder+2),%(%StripPling(%GlobalLFMSortOrderMenuText))&CHOOSE(BRW%ControlInstance::SortOrder>%(ITEMS(%SortOrder)),1,BRW%ControlInstance::SortOrder+1))
   #ELSE
   BRW%ControlInstance::RisFormatManager.SetCurrentFormat(CHOOSE(BRW%ControlInstance::SortOrder>%(ITEMS(%SortOrder)),2,BRW%ControlInstance::SortOrder+2),%(%StripPling(%GlobalLFMSortOrderMenuText)))
   #ENDIF
 END
END
#ENDAT
#!
#AT(%BrowseBoxPopupRecords),FIRST,WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND %AppTemplateFamily = 'CLARION' AND VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager AND %ListFormatManagerAllowPopupInEmptyList='Enable')
  #FIX(%Control,%ListControl)
! Process Thin@ List Format Manager (there are records in the list)
IF ThinNetMgr.Active AND %InstancePrefix:PopupChoiceOn THEN
   BRW%ControlInstance::PopupTextExt = BRW%ControlInstance::PopupText
   BRW%ControlInstance::RisFormatManager.MakePopup(BRW%ControlInstance::PopupTextExt)
   BRW%ControlInstance::PopupChoice = POPUP(CLIP(BRW%ControlInstance::PopupTextExt))
   EXECUTE(%InstancePrefix:PopupChoice)
   ELSE
     IF %InstancePrefix:RisFormatManager.DispatchChoice(%InstancePrefix:PopupChoice)
     ELSE
     END
   END
   EXIT
END
#ENDAT
#!
#! Abc templates listbox format manager support functions
#!
#AT(%WindowManagerMethodCodeSection,'Init','(),BYTE'),PRIORITY(8900),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND %AppTemplateFamily <> 'CLARION')
#INSERT(%ListBoxFormatManagerInitialization)
#PRIORITY(8901)
  SELF.SetAlerts()
ELSE
#PRIORITY(9300)
END
#ENDAT
#!
#AT(%BrowserMethodCodeSection),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled AND %AppTemplateFamily <> 'CLARION' ) 
#PRIORITY(5300)
#IF(%pClassMethod='SetSort' AND %pClassMethodPrototype='(BYTE NewOrder,BYTE Force),BYTE' AND VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager AND %ListFormatManagerSortOrder)
! Thin@
IF ThinNetMgr.Active THEN
 IF BRW%ControlInstance::LastSortOrder <> NewOrder THEN
   #IF(%ListFormatManagerMenuAppendSortOrder)
   BRW%ControlInstance::RisFormatManager.SetCurrentFormat(CHOOSE(NewOrder>%(ITEMS(%SortOrder)),2,NewOrder+2),%(%StripPling(%GlobalLFMSortOrderMenuText))&CHOOSE(NewOrder>%(ITEMS(%SortOrder)),1,NewOrder+1))
   #ELSE
   BRW%ControlInstance::RisFormatManager.SetCurrentFormat(CHOOSE(NewOrder>%(ITEMS(%SortOrder)),2,NewOrder+2),%(%StripPling(%GlobalLFMSortOrderMenuText)))
   #ENDIF
 END
ELSE
#ENDIF
#PRIORITY(7530)
#IF(%pClassMethod='SetSort' AND %pClassMethodPrototype='(BYTE NewOrder,BYTE Force),BYTE' AND VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager AND %ListFormatManagerSortOrder)
END
#ENDIF
#PRIORITY(3600)
#IF(%pClassMethod='TakeNewSelection' AND VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager AND %ListFormatManagerAllowPopupInEmptyList='Enable')
! Thin@
IF ThinNetMgr.Active THEN
 IF %InstancePrefix:PopupChoiceOn THEN
   IF KEYCODE() = MouseRightUp
      BRW%ControlInstance::PopupTextExt=''
      %InstancePrefix:PopupChoiceExec=True
      BRW%ControlInstance::RisFormatManager.MakePopup(BRW%ControlInstance::PopupTextExt)
      IF SELF.Popup.GetItems() THEN
         BRW%ControlInstance::PopupTextExt='|-|'&CLIP(BRW%ControlInstance::PopupTextExt)
      END
      BRW%ControlInstance::RisFormatManager.SetPopupChoice(SELF.Popup.GetItems(True)+1,0)
      SELF.Popup.AddMenu(CLIP(BRW%ControlInstance::PopupTextExt),SELF.Popup.GetItems()+1)
      BRW%ControlInstance::RisFormatManager.SetPopupChoice(,SELF.Popup.GetItems(True))
   ELSE
      %InstancePrefix:PopupChoiceExec=False
   END
 END
ELSE
#ENDIF
#PRIORITY(4900)
#IF(%pClassMethod='TakeNewSelection' AND VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager AND %ListFormatManagerAllowPopupInEmptyList='Enable')
END
#ENDIF
#PRIORITY(6300)
#IF(%pClassMethod='TakeNewSelection' AND VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager AND %ListFormatManagerAllowPopupInEmptyList='Enable')
! Thin@
IF ThinNetMgr.Active THEN
 IF %InstancePrefix:PopupChoiceOn AND %InstancePrefix:PopupChoiceExec=True THEN
   %InstancePrefix:PopupChoiceExec=False
   %InstancePrefix:PopupChoice=SELF.Popup.GetLastNumberSelection()
   SELF.Popup.DeleteMenu(BRW%ControlInstance::PopupTextExt)
   IF BRW%ControlInstance::RisFormatManager.DispatchChoice(%InstancePrefix:PopupChoice)
   ELSE
   END
 END
ELSE
#ENDIF
#PRIORITY(7520)
#IF(%pClassMethod='TakeNewSelection' AND VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager AND %ListFormatManagerAllowPopupInEmptyList='Enable')
END
#ENDIF
#ENDAT
#!
#! ----------------------------------------------------------------------------------------
#! Thin@ - ASCII Viewer template support
#! ----------------------------------------------------------------------------------------
#AT(%ViewerMethodCodeSection,'1','Init','(FILE AsciiFile,*STRING FileLine,*STRING Filename,UNSIGNED ListBox,ErrorClass ErrHandler,BYTE Enables=0),BYTE'),PRIORITY(7500),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND ~%AppTemplateFamily='CLARION')
    #FOR(%ActiveTemplate),WHERE(INSTRING('asciiviewcontrol',LOWER(%ActiveTemplate),1,1)<>0) 
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)
    #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)  
! Thin@ ASCII Viewer Support
n#=0
LOOP
 n# += 1
 Label = WHO(SELF,n#)
 IF NOT Label
    A &= WHAT(SELF,n#) 
    IF A &= NULL THEN Break.
    A &= NULL         
 END
 IF LOWER(Label) = 'displayqueue' THEN MyLoc = n#; BREAK.
END
IF MyLoc THEN
   MyPS = WHAT(SELF,MyLoc)
   pDisplayQueue &= ( MyAddress )
END
IF ThinNETMgr.Active AND ~pDisplayQueue&=NULL THEN ThinNETMgr.AddListControl(%WINDOW,ListBox,pDisplayQueue).
! End Thin@ ASCII Viewer Support
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT

#AT(%ViewerMethodDataSection,'1','Init','(FILE AsciiFile,*STRING FileLine,*STRING Filename,UNSIGNED ListBox,ErrorClass ErrHandler,BYTE Enables=0),BYTE'),PRIORITY(5000),WHERE(%ThinNETEnabled AND %ThinNETProcEnabled AND ~%AppTemplateFamily='CLARION')
    #FOR(%ActiveTemplate),WHERE(INSTRING('asciiviewcontrol',LOWER(%ActiveTemplate),1,1)<>0) 
    #FOR(%ActiveTemplateInstance)
    #CONTEXT(%Procedure,%ActiveTemplateInstance)
    #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
!Thin@ ASCII Viewer Support
pDisplayQueue   &QUEUE
A               ANY
MyLoc           LONG
Label           STRING(300)
MyAddress       LONG
MyPS            STRING(4),OVER(MyAddress)      
!End Thin@ ASCII Viewer Support
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT
#!
#! ----------------------------------------------------------------------------------------
#! Thin@ - SVGraph Support
#! ----------------------------------------------------------------------------------------
#AT (%DataSectionBeforeWindow), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='Graph(SVGraph)')
  #CONTEXT(%Procedure,%ActiveTemplate)
  #ENDCONTEXT
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
SVGrafFEQ%ActiveTemplateInstance SIGNED !Thin@ SVGraph support
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT

#AT (%DataSectionBeforeWindow), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='Graph(SVGraph)')
  #CONTEXT(%Procedure,%ActiveTemplate)
  #ENDCONTEXT
SVSaveFileName CSTRING(255)
  #ENDFOR  
#ENDAT

#AT(%AfterWindowOpening),WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='Graph(SVGraph)')
  IF ThinNetMgr.Active THEN !Thin@ SVGraph support
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
    ! Thin@ - replicate the SVGraph regions into images
                SVGrafFEQ%ActiveTemplateInstance = CREATE(0, CREATE:IMAGE, %Control{PROP:PARENT}); SVGrafFEQ%ActiveTemplateInstance{PROP:use} = SVGrafFEQ%ActiveTemplateInstance
                GETPOSITION(%Control,X#,Y#,W#,H#); SVGrafFEQ%ActiveTemplateInstance{PROP:XPos}=X#; SVGrafFEQ%ActiveTemplateInstance{PROP:YPos}=Y#
                SVGrafFEQ%ActiveTemplateInstance{PROP:Width}=W#; SVGrafFEQ%ActiveTemplateInstance{PROP:Height}=H#
                UNHIDE(SVGrafFEQ%ActiveTemplateInstance)
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  END !IF
  #ENDFOR
#ENDAT

#AT(%GraphMethodCodeSection,,,'(byte parRefresh=false)'),PRIORITY(5001), WHERE(%TARGET32 AND %ThinNETEnabled AND %ThinNETProcEnabled)
  #FOR(%ActiveTemplate), WHERE(%ActiveTemplate='Graph(SVGraph)')
  #IF(NOT VAREXISTS(%pObject))
  #DECLARE(%pObject)
  #ENDIF
  #SET(%pObject,%ThisObjectName)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance AND %ThisObjectName = %pObject)
  ! Thin@ - SVGraph support
  IF (ThinNetMgr.Active AND %Control{prop:visible} <> '') THEN  
     IF ThinNETMgr.State=0 THEN
          ThinNETMgr.RecieveDataFromClient()
          ThinNETMgr.State = 1
     END
     SVSaveFileName = SELF.eSaveFileName
     SELF.eSaveFileName = '%ThisObjectName' & ThinNetMgr.Stats.ClientSerial & '.wmf'
     SELF.SaveGraph()
     ChangedCheck#=1
     IF RisNet:DownloadFile(SELF.eSaveFileName,'',,,ChangedCheck#).
     ! Only refresh image on the client if image changed
     IF ChangedCheck# THEN
       ThinNetMgr.DisplayControlImage(SVGrafFEQ%ControlInstance)
       SVGrafFEQ%ControlInstance{PROP:TEXT} = SELF.eSaveFileName
     END
     REMOVE(SELF.eSaveFileName)
     SELF.eSaveFileName = SVSaveFileName
                END !IF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR  
#ENDAT
#!
#! ----------------------------------------------------------------------------------------
#! Thin@ - Window scaling - ABC
#! ----------------------------------------------------------------------------------------
#AT(%WindowManagerMethodCodeSection,'Init','(),BYTE'),PRIORITY(8150),WHERE(%TARGET32 AND %AppTemplateFamily <> 'CLARION')
#IF(%GlobalWindowScaling<>'Disable' OR %MaximizeWindowWidth<>0 OR %MaximizeWindowHeight<>0 OR %MaximizeWindowMarginX<>0 OR %MaximizeWindowMarginY<>0)
#?! Thin@ - Window scaling
 #IF(%GlobalWindowScaling='Enable')
IF ThinNetMgr.Active THEN
 #ENDIF
 #IF(%GlobalWindowScaling<>'Disable' AND %LocalWindowScaling=1)
RisNet:ScaleWindow(%Window, 1, %MaximizeWindowWidth, %MaximizeWindowHeight, %MaximizeWindowMarginX, %MaximizeWindowMarginY)
 #ELSE
RisNet:ScaleWindow(%Window, 0, %MaximizeWindowWidth, %MaximizeWindowHeight, %MaximizeWindowMarginX, %MaximizeWindowMarginY)
 #ENDIF
IF ~ThinNetMgr.Active THEN %Window{PROP:Hide}=0.
 #IF(%GlobalWindowScaling='Enable')
END
 #ENDIF
#ENDIF
#PRIORITY(8005)
#IF(%GlobalWindowScaling<>'Disable' OR %MaximizeWindowWidth<>0 OR %MaximizeWindowHeight<>0 OR %MaximizeWindowMarginX<>0 OR %MaximizeWindowMarginY<>0)
#?! Thin@ - Window scaling - hide window to avoid flickering
IF ~ThinNetMgr.Active THEN %Window{PROP:Hide}=1.
#ENDIF
#ENDAT
#! ----------------------------------------------------------------------------------------
#! Thin@ - Window scaling - CLARION
#! ----------------------------------------------------------------------------------------
#AT(%AfterControlsSetup),WHERE(%TARGET32 AND %AppTemplateFamily = 'CLARION')
#IF(%GlobalWindowScaling<>'Disable' OR %MaximizeWindowWidth<>0 OR %MaximizeWindowHeight<>0 OR %MaximizeWindowMarginX<>0 OR %MaximizeWindowMarginY<>0)
#?! Thin@ - Window scaling
 #IF(%GlobalWindowScaling='Enable')
IF ThinNetMgr.Active THEN
 #ENDIF
 #IF(%GlobalWindowScaling<>'Disable' AND %LocalWindowScaling=1)
RisNet:ScaleWindow(%Window, 1, %MaximizeWindowWidth, %MaximizeWindowHeight, %MaximizeWindowMarginX, %MaximizeWindowMarginY)
 #ELSE
RisNet:ScaleWindow(%Window, 0, %MaximizeWindowWidth, %MaximizeWindowHeight, %MaximizeWindowMarginX, %MaximizeWindowMarginY)
 #ENDIF
%Window{PROP:Hide}=0 
 #IF(%GlobalWindowScaling='Enable')
END
 #ENDIF
#ENDIF
#ENDAT
#AT(%AfterWindowOpening),WHERE(%TARGET32 AND %AppTemplateFamily = 'CLARION'),FIRST
#?! Thin@ - Window scaling - hide window to avoid flickering
#IF(%GlobalWindowScaling='Enable')
IF ThinNetMgr.Active THEN
#ENDIF
#IF(%GlobalWindowScaling<>'Disable')
%Window{PROP:Hide}=1
#ENDIF
#IF(%GlobalWindowScaling='Enable')
END
#ENDIF
#ENDAT
#!
#GROUP(%StripPling,%Incoming)
  #IF(SUB(%Incoming,1,1)='!')
    #RETURN(SUB(%Incoming,2))
  #ELSIF(SUB(%Incoming,1,1)='=')
    #RETURN('EVALUATE(' & %StripPling(SUB(%Incoming, 2)) & ')')
  #ELSE
    #RETURN( '''' & QUOTE(%Incoming) & '''' )
  #ENDIF
#!
#! ----------------------------------------------------------------------------------------
#! List format manager - Initialization group
#! ----------------------------------------------------------------------------------------
#GROUP(%ListBoxFormatManagerInitialization)
! Thin@
IF ThinNetMgr.Active THEN
  #FOR(%ActiveTemplate),WHERE(INSTRING('browsebox',LOWER(%ActiveTemplate),1,1)<>0)
  #FOR(%ActiveTemplateInstance)
  #CONTEXT(%Procedure,%ActiveTemplateInstance)
  #FOR(%Control),WHERE(%ControlInstance=%ActiveTemplateInstance)
    #IF(NOT VAREXISTS(%pFieldCounter))
    #DECLARE(%pFieldCounter,LONG)
    #ENDIF
    #IF(NOT VAREXISTS(%TempConstructor))
    #DECLARE(%TempConstructor)
    #ENDIF
    #SET(%pFieldCounter,0)
    #FOR(%ControlField)
      #SET(%pFieldCounter,%pFieldCounter + 1)
      #IF(%ControlFieldHasIcon)
        #SET(%pFieldCounter,%pFieldCounter + 1)
      #ENDIF
      #IF(%ControlFieldHasColor)
        #SET(%pFieldCounter,%pFieldCounter + 4)
      #ENDIF
      #IF(%ControlFieldHasStyle)
        #SET(%pFieldCounter,%pFieldCounter + 1)
      #ENDIF
      #IF(%ControlHasTip)
        #SET(%pFieldCounter,%pFieldCounter + 1)
      #ENDIF
      #IF(%ControlHasTree)
        #SET(%pFieldCounter,%pFieldCounter + 1)
      #ENDIF
    #ENDFOR
    #IF(VAREXISTS(%GlobalEnableListFormatManager) AND VAREXISTS(%DisableListFormatManager) AND %GlobalEnableListFormatManager AND NOT %DisableListFormatManager)
! Thin@ list format manager initialization
      #IF(%ListFormatManagerSave)
  BRW%ActiveTemplateInstance::RisFormatManager.SaveFormat=True
      #ENDIF
      #IF(%TableOrigin='Application')
  BRW%ActiveTemplateInstance::RisFormatManager.Init('%'Application','%'Procedure',%GlobalUserFieldListFormatManager,%ListControl,%ActiveTemplateInstance,%InstancePrefix:PopupTextExt,%ControlFrom,%pFieldCounter,%FileEquate,%FileEquate.Record,FALSE)
      #ELSE
  BRW%ActiveTemplateInstance::RisFormatManager.Init('%'Application','%'Procedure',%GlobalUserFieldListFormatManager,%ListControl,%ActiveTemplateInstance,%InstancePrefix:PopupTextExt,%ControlFrom,%pFieldCounter,%DictionaryTableOrigin,%DictionaryTableOrigin.Record,FALSE)
      #ENDIF
      #SET(%TempConstructor,%False)
      #IF(%ListFormatManagerCheckType = 'Text')
        #SET(%TempConstructor,'CHECK_TEXT')
      #ENDIF
      #IF(%ListFormatManagerItemsSortBy = 'Code')
        #IF(%TempConstructor)
          #SET(%TempConstructor,%TempConstructor & ',SORT_CODE')
        #ELSE
          #SET(%TempConstructor,',SORT_CODE')
        #ENDIF
      #ENDIF
      #IF(%ListFormatManagerSaveWindowPosition = 'Disable')
        #IF(%TempConstructor)
          #IF(%ListFormatManagerItemsSortBy = 'Text')
            #SET(%TempConstructor,%TempConstructor & ',,FALSE')
          #ELSE
            #SET(%TempConstructor,%TempConstructor & ',FALSE')
          #ENDIF
        #ELSE
          #SET(%TempConstructor,',,FALSE')
        #ENDIF
      #END
      #IF(%TempConstructor)
BRW%ActiveTemplateInstance::RisFormatManager.BindInterface(%TempConstructor)
      #ENDIF
    #ENDIF
  #ENDFOR
  #ENDCONTEXT
  #ENDFOR
  #ENDFOR
#! ----------------------------------------------------------------------------------------
#GROUP(%InsertNETControls)
#! Deprecated
#GROUP(%ThinBox)
#BOXED(''),SECTION,AT(0,5,201,184)
      #IMAGE('Thin@.jpg')
      #DISPLAY('Thin@ - Thin Client Technology'),PROP(PROP:FontName,'Calibri'),PROP(PROP:FontSize,11),PROP(PROP:FontStyle,700),PROP(PROP:Background,0FFFFFFH),AT(38,115,,13)
      #DISPLAY(%ThinNETVersion),PROP(PROP:FontName,'Calibri'),PROP(PROP:FontSize,9),PROP(PROP:FontStyle,700),PROP(PROP:Background,0FFFFFFH),AT(58,128)
      #DISPLAY('Copyright �2005-2012, RIS d.o.o.'),PROP(PROP:FontName,'Calibri'),PROP(PROP:FontSize,9),PROP(PROP:Background,0FFFFFFH),AT(44,143)
      #DISPLAY('http://www.thinetsolution.com/'),PROP(PROP:FontName,'Calibri'),PROP(PROP:FontSize,9),PROP(PROP:Background,0FFFFFFH),AT(44,153)
#ENDBOXED
#!
#! Thin@ Noyantis Support
#!
#GROUP(%CheckNoyantisControlName)
#IF(%ActiveTemplate='NYS_ChartPro(NYS_ChartProTpl)')
  #SET(%NoyantisControlName, %NYS012OCXCtrl)
  #SET(%NYSCurrentObject, %NYS012OCXObj)
#ELSIF(%ActiveTemplate='NYS_ShortcutBarNav(NYS_ShortcutBarTpl)')
  #SET(%NoyantisControlName, %NYS001OCXCtrl)
  #SET(%NYSCurrentObject, %NYS001OCXObj)
#ELSIF(%ActiveTemplate='NYS_ShortcutBarFrame(NYS_ShortcutBarTpl)')
  #SET(%NoyantisControlName, 'SELF.OCXCtrl')
  #SET(%NYSCurrentObject, %NYS001OCXObj)
#ELSIF(%ActiveTemplate='NYS_CommandBarsNav(NYS_CommandBarsTpl)' OR %ActiveTemplate='NYS_CommandBarsCtrl(NYS_CommandBarsTpl)')
  #SET(%NoyantisControlName, %NYS002OCXCtrl)
  #SET(%NYSCurrentObject, %NYS002OCXObj)
#ELSIF(%ActiveTemplate='NYS_CommandBarsFrame(NYS_CommandBarsTpl)')
  #SET(%NoyantisControlName, 'SELF.OCXCtrl')
  #SET(%NYSCurrentObject, %NYS002OCXObj)
#ENDIF
#!
#GROUP(%NYSFirst)
#INDENT(-12)
            #INSERT(%CheckNoyantisControlName)
            #IF(%NYSCurrentObject = %NYSObject OR INSTRING('G_CALC', %ActiveTemplate,1,1)>0)
#! Start: Virtual methods which should not override the PARENT call but which should be called after the PARENT call
              #IF(%VMQMethod <> 'TakeEvent')            
                #IF(VAREXISTS(%NYS012AppDisable))
    IF ~ThinNetMgr.Active OR (ThinNetMgr.Active AND %ChartProTranferImages=1) THEN ! Thin@ Noyantis Support
                #ELSE
    IF ~ThinNetMgr.Active THEN ! Thin@ Noyantis Support    
                #ENDIF
     	        #ENDIF
            #ENDIF  
#INDENT(+12)    
#!
#GROUP(%NYSLast)
#INDENT(-12)
          #INSERT(%CheckNoyantisControlName)
            #IF(%NYSCurrentObject = %NYSObject  OR INSTRING('G_CALC', %ActiveTemplate,1,1)>0)
#! Virtual methods which should not override the PARENT call but called after the PARENT call - dpavlic 08.03.2012
              #IF(%VMQMethod <> 'TakeEvent')            
    ELSE
    	        #ENDIF
#! Virtual methods which should override the PARENT call
              #IF(%VMQMethod ='InitOCX')
      NoyantisWind &= SYSTEM{PROP:Target}
      ThinNetMgr.AddOcxControl(NoyantisWind, %NoyantisControlName, CLIP(SELF.AppName))
      ! Add Thin@ client callback skip events
#! CommandBars speed optimization
      RisNet:OCXAddSkipEvent(SELF.OCXCtrl, 2, ,NoyantisWind)
      RisNet:OCXAddSkipEvent(SELF.OCXCtrl, 0, 'ControlSelected',NoyantisWind)
      RisNet:OCXAddSkipEvent(SELF.OCXCtrl, 0, 'TrackingModeChanged',NoyantisWind)
      RisNet:OCXAddSkipEvent(SELF.OCXCtrl, 0, 'InitCommandsPopup',NoyantisWind)
      RisNet:OCXAddSkipEvent(SELF.OCXCtrl, 0, 'Update',NoyantisWind)
#! ShortcutBar speed optimization
      RisNet:OCXAddSkipEvent(SELF.OCXCtrl, 0, 'ClientSizeChanged',NoyantisWind)
      RisNet:OCXAddSkipEvent(SELF.OCXCtrl, 0, 'HighlightedChanged',NoyantisWind)
              #ELSIF(%VMQMethod ='OCXDirectCommand')
  	pNoyantisThread = SELF.TargetThreadModify()
    NoyantisWind &= SYSTEM{PROP:Target}
    IF OMITTED(3) = TRUE OR (OMITTED(3) = FALSE AND paramValue = '')
      IF SUB(paramCommand, 1, 16) = 'Icons.LoadIcon("' THEN
        Risnet:SetOCXProperty(SELF.OCXCtrl, 'Icons.LoadIcon("' & CLIP(ThinNetMgr.ClientPath) &'\'& SUB(paramCommand, 17, LEN(paramCommand)),,NoyantisWind) !'
      ELSE
        Risnet:SetOCXProperty(SELF.OCXCtrl,CLIP(paramCommand),,NoyantisWind)
      END
    ELSE
     Risnet:SetOCXProperty(SELF.OCXCtrl,CLIP(paramCommand),paramValue,NoyantisWind)
    END 
    SELF.TargetThreadRestore(pNoyantisThread)  
    	        #ELSIF(%VMQMethod ='OCXDirectValue')
 		pNoyantisThread = SELF.TargetThreadModify()
    NoyantisWind &= SYSTEM{PROP:Target}
    #!! Thin@ - In thin@ mode if Position is fetch return 0 and if .Visible is fetch return 0
    #!IF INSTRING('.Position',CLIP(paramCommand),1,1) OR SUB(CLIP(paramCommand),1,17)='StatusBar.Visible' THEN 
    #!    	pRetVal =0
    #!ELSIF INSTRING('.Visible',CLIP(paramCommand),1,1) AND NOT SUB(CLIP(paramCommand),1,10)='StatusBar.' THEN
    #!      pRetVal = -1
    IF (SUB(CLIP(paramCommand),1,4)='Add(' OR SUB(CLIP(paramCommand),1,5)='Add2(' OR SUB(CLIP(paramCommand),1,24)='CreateCommandBarControl(' OR INSTRING('.Add(',CLIP(paramCommand),1,1)<>0 OR INSTRING('.Add2(',CLIP(paramCommand),1,1)<>0 OR |
    	CLIP(paramCommand)='StatusBar' OR SUB(CLIP(paramCommand),1,7)='AddItem' OR SUB(CLIP(paramCommand),1,13)='CreateElement' OR SUB(CLIP(paramCommand),1,13)='AddTabToolBar' OR SUB(CLIP(paramCommand),1,13)='ActiveMenuBar' OR SUB(CLIP(paramCommand),1,12)='AddRibbonBar' OR INSTRING('.AddItem(',CLIP(paramCommand),1,1)<>0 OR | 
      SUB(CLIP(paramCommand),1,5)='Item(' OR INSTRING('.Id',CLIP(paramCommand),1,1)<>0 OR INSTRING('.BarId',CLIP(paramCommand),1,1)<>0 OR INSTRING('.InsertTab',CLIP(paramCommand),1,1)<>0 OR |
      INSTRING('.Parameter',CLIP(paramCommand),1,1)<>0 OR INSTRING('.AddGroup',CLIP(paramCommand),1,1)<>0 OR INSTRING('.Parent',CLIP(paramCommand),1,1)<>0 OR INSTRING('.AddSystemButton',CLIP(paramCommand),1,1)<>0 OR INSTRING('.InsertCategory',CLIP(paramCommand),1,1)<>0)             
      pRetVal = RisNet:GetOCXProperty(SELF.OCXCtrl,CLIP(paramCommand),NoyantisWind,TRUE) ! Bind the result and cache it on the client
    ELSE
      pRetVal = RisNet:GetOCXProperty(SELF.OCXCtrl,CLIP(paramCommand),NoyantisWind)
    END
    SELF.TargetThreadRestore(pNoyantisThread)
  	RETURN pRetVal
              #ELSIF(%VMQMethod ='RegisterEventProc')
      Risnet:OCXRegisterEventProc(paramOCXCtrl, paramAddress)              
    	        #ELSIF(%VMQMethod ='GetOCXLastEventName')
      pRetVal = RisNet:OCXGetLastEventName(paramReference)
              #ELSIF(%VMQMethod ='GetOCXParamCount')
      pRetVal = RisNet:OCXGetParamCount(paramReference)              
              #ELSIF(%VMQMethod ='GetOCXParam')
      pRetVal = RisNet:OCXGetParam(paramReference, paramNumber)              
              #ELSIF(%VMQMethod ='TakeWindowEvent')
      IF EVENT()=EVENT:OPENWINDOW THEN ThinNetMgr.OpenWindow(paramWindow).
      ThinNetMgr.TakeEvent(paramWindow)           
              #ELSIF(%VMQMethod ='GetHandle')
      NoyantisWind &= SYSTEM{PROP:Target}
      pRetVal = RisNet:GetOCXProperty(paramControl, PROP:Handle,NoyantisWind)
              #ELSIF(%VMQMethod ='AddNavMenu')
      pRetVal = PARENT.AddNavMenu(paramNavID, paramNavQueue, paramLineHeight, paramSelected)
      NoyantisWind &= SYSTEM{PROP:Target}
      ThinNetMgr.AddListcontrol(NoyantisWind, SELF.GetNavMenuFeq(paramNavID), paramNavQueue)
      ThinNetMgr.AddOption(NoyantisWind, SELF.GetNavMenuFeq(paramNavID), 5)
      			 #ELSIF(%VMQMethod ='OCXBind')
		  RisNet:OCXBind(paramBindName, paramVariable, 1)
             #ELSIF(%VMQMethod ='OCXUnbind')		  
		  RisNet:OCXBind(paramBindName)
             #ELSIF(%VMQMethod ='AddIcon')		  
		  ThinNetMgr.AddFileToDownloadList(pName)
             #ELSIF(%VMQMethod ='AddIconCalcLoadCmd')			  
		  IF SUB(paramName,1,1)='~' THEN
		  	  vParamName = CLIP(SUB(ParamName,2,LEN(ParamName)-1))
      ELSE
      	  vParamName = CLIP(ParamName)
      END
      !PARENT.AddIconCalcLoadCmd(vparamName, paramCmd, paramCmdVal, paramWidth, paramHeight)
      IF SUB(UPPER(ParamName),-3,3)='ICO' THEN
      	 paramCmd = 'LoadIcon'; paramCmdVal = '"' & vParamName & '"'
      ELSE
      	 paramCmd = 'LoadBitmap'; paramCmdVal = '"' & vParamName & '"'
      END
      ThinNetMgr.AddFileToDownloadList(vparamName)
             #ELSIF(%VMQMethod ='MDISetChildSelections')			  
             #ELSIF(%VMQMethod ='MDISetChildSelected')			  
             #ELSIF(%VMQMethod ='MDICtrlsShow')			  
             #ELSIF(%VMQMethod ='TakeEvent' AND VAREXISTS(%NYS002AppDisable))
                #IF(%NYS002AddCtrlType='FRAME')
                    IF ThinNetMgr.Active AND EVENT() = EVENT:Sized THEN !Resize command bars on resize of main frame - Thin@ support
                 	  	%NYS002OCXObj.SyncOCXWidth()  	 
                      ThinNetMgr.DisplayThread(%NYS002OCXObj.OCXThread)   
                    END  
                #ENDIF
             #ELSIF(%VMQMethod ='CalcOverallHeight')
                 #!pRetVal = PARENT.CalcOverallHeight(1)
              	 pNoyantisThread = SELF.TargetThreadModify()
                 NoyantisWind &= SYSTEM{PROP:Target}
                 pRetVal = RisNet:GetOCXProperty(SELF.OCXCtrl,'CommandBarsClass_CalcOverallHeight()',NoyantisWind)
                 SELF.TargetThreadRestore(pNoyantisThread)                 
             #ENDIF
#! Start: Virtual methods which should not override the PARENT call but which should be called after the PARENT call
              #IF(%VMQMethod <> 'TakeEvent')            
    END
    	        #ENDIF
            #ENDIF
#INDENT(+12)
#!-------------------------------------------------------
#Template(NetTalkWizards,'NetTalk Wizards'),Family('ABC')
#!-------------------------------------------------------
#UTILITY(ServerAppWizard,'Create WebServer Application'),WIZARD(Application(ABC))
  #SHEET
    #insert(%WelcomeTab)
    #insert(%ApplicationThemeOptionsTab)
    #insert(%ApplicationMenuOptionsTab)
    #insert(%ApplicationFormatOptionsTab)
    #INSERT(%GlobalExtensionsTab)
    #INSERT(%SecwinWarningTab)
    #Insert(%ProcedureTypesTab)
    #Insert(%FilesTab)
    #Insert(%ServerOptionsTab)
    #Insert(%HeaderFooterTab)
    #Insert(%IndexPageTab)
    #Insert(%BrowseOptionsTab)
    #Insert(%FormOptionsTab)
    #Insert(%DoneTab)
  #ENDSHEET
#INSERT(%DeclareUtilityVariables)
  #SET(%InstanceCounter,1)
  #CREATE('ntw.txa')
#INSERT(%ServerAppTxa)
#INSERT(%GenerateServerGlobalExtensions)
#INSERT(%ServerAppTxaEnd)
    #IF(%mbGlobal)
#INSERT(%MessageBoxProcsTxa)
    #ENDIF
  #CLOSE('ntw.txa')
  #IMPORT('ntw.txa'),REPLACE
#INSERT(%BrowsesGroup)
#INSERT(%FormsGroup)
#INSERT(%Others)
#!-------------------------------------------------------
#Group(%BrowsesGroup)
  #IF(%genBrowses)
#INSERT(%GenerateServerBrowses)
#IMPORT('ntwb.txa'),REPLACE
  #ENDIF
#!-------------------------------------------------------
#Group(%FormsGroup)
  #IF(%genForms)
#INSERT(%GenerateServerForms)
#IMPORT('ntwf.txa'),REPLACE
  #ENDIF
#!-------------------------------------------------------
#Group(%others)
  #IF(%genWebServer or %genWebHandler or  %genHeader or %genFooter or %genIndexPage or %genLoginForm or %swGlobal)
#INSERT(%GenerateServerProcedures)
#IMPORT('ntwp.txa'),REPLACE
  #ENDIF
  #IF(%swGlobal)
#import('SecwininNTWSABC.txa')
  #ENDIF
  #IF(%fmGlobal)
    #CALL(%IsDriver,'MSSQL'),%temp1
    #IF(%temp1=0)
      #CALL(%IsDriver,'ODBC'),%temp1
      #IF(%temp1=0)
        #CALL(%IsDriver,'ORACLE'),%temp1
        #IF(%temp1=0)
          #CALL(%IsDriver,'SYBASE'),%temp1
        #ENDIF
      #ENDIF
    #ENDIF
    #IF(%temp1)
#import('FM3ConnectABC.txa')
    #ENDIF
  #ENDIF
#!-------------------------------------------------------
#UTILITY(ServerProcsWizard,'Create WebServer Procedures'),WIZARD
  #SHEET
    #INSERT(%GlobalExtensionsTab)
    #Insert(%ProcedureTypesTab)
    #Insert(%FilesTab)
    #Insert(%ServerOptionsTab)
    #Insert(%HeaderFooterTab)
    #Insert(%IndexPageTab)
    #Insert(%BrowseOptionsTab)
    #Insert(%FormOptionsTab)
    #Insert(%DoneTab)
  #ENDSHEET
#INSERT(%DeclareUtilityVariables)
  #CREATE('ntw.txa')
  #CLOSE('ntw.txa')
  #IMPORT('ntw.txa'),REPLACE
#INSERT(%BrowsesGroup)
#INSERT(%FormsGroup)
#INSERT(%Others)
#!-------------------------------------------------------
#GROUP(%DeclareUtilityVariables)
  #Declare(%i)
  #Declare(%counter)
  #Declare(%Temp1)
  #Declare(%Temp2)
  #Declare(%rFiles)
  #Declare(%InstanceCounter)
  #Declare(%BrowseFields),MULTI
  #Declare(%BrowseFieldButton,%BrowseFields)
  #Declare(%BrowseFieldCheck,%BrowseFields)
  #Declare(%BrowseFieldCondition,%BrowseFields)
  #Declare(%BrowseChild,%BrowseFields)
  #Declare(%BrowseValueFieldInChild,%BrowseFields)
  #Declare(%BrowseDescFieldInChild,%BrowseFields)
  #!
  #Declare(%FormFields),MULTI
  #Declare(%FormField,%FormFields)
  #Declare(%FormFieldEquate,%FormFields)
  #Declare(%FormFieldNumber,%FormFields)
  #Declare(%FormFieldType,%FormFields)
  #Declare(%FormFieldInclude,%FormFields)
  #Declare(%FormFieldLookupFile,%FormFields)
  #Declare(%FormFieldLookupValue,%FormFields)
  #Declare(%FormFieldLookupDesc,%FormFields)
  #Declare(%FormFieldCallProcedure,%FormFields)
  #Declare(%FormTab,%FormFields)
  #Declare(%FormTabs),UNIQUE
  #Declare(%TabsNumber,%FormTabs)
  #Declare(%TabsLookups,%FormTabs)
  #Declare(%LoginForm)
  #IF(%swGlobal)
    #SET(%LoginForm,'SecwinWebLoginForm')
  #ELSE
    #SET(%LoginForm,'LoginForm')
  #ENDIF
#!-------------------------------------------------------
#GROUP(%GenerateServerGlobalExtensions)
#INSERT(%NetTalkTxa)
    #IF(%ntGlobalst)
#INSERT(%StringTheoryTxa)
    #ENDIF
    #IF(%weGlobal)
#INSERT(%WinEventTxa)
    #ENDIF
    #IF(%ssGlobal)
#INSERT(%SelfServiceTxa)
    #ENDIF
    #IF(%fmGlobal)
#INSERT(%Fm3Txa)
    #ENDIF
    #IF(%mbGlobal)
#INSERT(%MessageBoxTxa)
    #ENDIF
    #IF(%gpfGlobal)
#INSERT(%HyperactiveTxa)
#INSERT(%GPFReporterTxa)
    #ENDIF
    #IF(%igGlobal)
#INSERT(%InsightTxa)
    #ENDIF
    #IF(%drGlobal)
#INSERT(%DrawTxa)
    #ENDIF
    #IF(%swGlobal)
#Insert(%SecwinTxa)
    #ENDIF
    #IF(%trPdfGlobal)
#Insert(%PDFXTools41Txa)
    #ENDIF
    #IF(%svpdfGlobal)
#Insert(%SVPDFX)
    #ENDIF
#!-------------------------------------------------------
#GROUP(%GenerateServerProcedures)
  #IF(%genWebServer or %genWebHandler or  %genHeader or %genFooter or %genIndexPage or (%genLoginForm and %swglobal = 0))
    #CREATE('ntwp.txa')
    #IF(%genWebServer)
#INSERT(%WebserverTxa)
    #ENDIF
    #IF(%genWebHandler)
#INSERT(%WebHandlerTxa)
    #ENDIF
    #IF(%genHeader)
#INSERT(%HeaderTxa)
    #ENDIF
    #IF(%genFooter)
#INSERT(%FooterTxa)
    #ENDIF
    #IF(%genIndexPage)
#INSERT(%IndexPageTxa)
    #ENDIF
    #IF(%genLoginForm and %swglobal = 0)
#INSERT(%LoginFormTxa)
    #ENDIF
    #CLOSE('ntwp.txa')
  #ENDIF
#!-------------------------------------------------------
#GROUP(%GenerateServerBrowses)
  #IF(%genBrowses)
    #CREATE('ntwb.txa')
    #FOR(%wsFiles)
#INSERT(%BrowseTxa,%wsFiles)
    #ENDFOR
    #CLOSE('ntwb.txa')
  #ENDIF
#!-------------------------------------------------------
#GROUP(%GenerateServerForms)
  #IF(%genForms)
    #CREATE('ntwf.txa')
    #FOR(%wsFiles)
#INSERT(%FormTxa,%wsFiles)
    #ENDFOR
    #CLOSE('ntwf.txa')
  #ENDIF
#!----------------------------------------------------
#Group(%WelcomeTab)
    #Tab('Welcome'),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Welcome'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #Display('Welcome to the NetTalk Wizard for generating a Web App from a dictionary.')
      #Display('')
      #Display('If you are new to the Wizard, and to NetTalk, then here are some pointers;')
      #Display('')
      #Display('1. Spend a bit of time experimenting.')
      #Display('   There are lots of options, and some are not always obvious as to what will happen.')
      #Display('')
      #Display('2. Don''t worry too much about getting something wrong.')
      #Display('    All these options can be set, or reset, manually in your application later on.')
      #Display('')
    #endTab
#!----------------------------------------------------
#Group(%ApplicationThemeOptionsTab)
    #Tab('Application Theme Options'),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Application Theme Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #Prompt('Theme',DROP('Absolution|Aristo|MetroBlue|MetroOrange|Redmond|ShoeStrap1|ShoeStrap2|ShoeStrap3|Smoothness|Sunny|Ui-Lightness|Whales|Windows')),%wstheme,default('ShoeStrap3'),at(100)
      #Boxed,Where(%wstheme='xxxx'),section,at(,60)
        #Image('ThemeAbsolution.bmp'),at(,,,250)
      #EndBoxed
      #Boxed,Where(%wstheme='Absolution'),section,at(,60)
        #Image('ThemeAbsolution.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='Aristo'),section,at(,60)
        #Image('ThemeAristo.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='MetroBlue'),section,at(,60)
        #Image('ThemeMetroBlue.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='MetroOrange'),section,at(,60)
        #Image('ThemeMetroOrange.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='Redmond'),section,at(,60)
        #Image('ThemeRedmond.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='ShoeStrap1'),section,at(,60)
        #Image('ThemeShoeStrap1.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='ShoeStrap2'),section,at(,60)
        #Image('ThemeShoeStrap2.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='ShoeStrap3'),section,at(,60)
        #Image('ThemeShoeStrap3.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='ShoeStrap3'),section,at(,60)
        #Image('ThemeShoeStrap3.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='Smoothness'),section,at(,60)
        #Image('ThemeSmoothness.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='Sunny'),section,at(,60)
        #Image('ThemeSunny.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='Ui-Lightness'),section,at(,60)
        #Image('ThemeUi-Lightness.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='Whales'),section,at(,60)
        #Image('ThemeWhales.bmp')
      #EndBoxed
      #Boxed,Where(%wstheme='Windows'),section,at(,60)
        #Image('ThemeWindows.bmp')
      #EndBoxed
      #!Prompt('Custom Theme File',@s255),%wsCustomTheme,default('CustomT.Css')
      #!Prompt('Custom Style File',@s255),%wsCustomCss,default('Custom.Css')
    #EndTab
#!----------------------------------------------------
#Group(%ApplicationMenuOptionsTab)
    #Tab('Application Menu Options'),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Application Menu Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #prompt('Include Menu',Check),%wsMenu,default(1),at(10)
      #prompt('Link Procedures to menu as procedures (not recommended for wizarding large dictionaries)',Check),%wsLinkMenu,default(1),at(10)
      #enable(%wsMenu =1),section
        #Display('Menu Type:')
        #Prompt('',OPTION),%wsMenuStyle,Default('Net:Web:Ddm')
          #Prompt('DoubleDrop',RADIO),value('Net:Web:Ddm'),ICON('ntwizM2.bmp'),at(10,10,320,40)
          #Prompt('Accordion',RADIO),value('Net:Web:Outlook'),ICON('ntwizM1.bmp'),at(10,60,130,200)
          #Prompt('TaskPanel',RADIO),value('Net:Web:TaskPanel'),ICON('ntwizM4.bmp'),at(180,60,130,200)
      #endenable
    #EndTab
#!----------------------------------------------------
#Group(%ApplicationFormatOptionsTab)
    #Tab('Application Behaviour Options'),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Application Behaviour Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #Prompt('Behaviour',DROP('Menu items and buttons open new pages[1]|Menu items open a page, buttons open a popup[2]|Menu Items open a Popup[3]')),%wsLayout,default(2),at(100,,200)
      #Boxed,Where(%wsLayout=1),section,at(,60)
        #Image('ntwizA1.bmp')
      #EndBoxed
      #Boxed,Where(%wsLayout=2),section,at(,60)
        #Image('ntwizA2.bmp')
      #EndBoxed
      #Boxed,Where(%wsLayout=3),section,at(,60)
        #Image('ntwizA3.bmp')
      #EndBoxed
    #EndTab
#!----------------------------------------------------
#GROUP(%GlobalExtensionsTab)
    #TAB('Global Extensions')
      #prepare
        #IF(not VarExists(%SecwinInDict))
          #declare(%SecwinInDict)
        #ENDIF
        #SET(%SecwinInDict,0)
        #For(%file),where(%File='Secwin_Operators')
          #SET(%SecwinInDict,1)
        #EndFor
      #EndPrepare
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle,700),at(80,0,,12)
        #Display('Global Extensions'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #Prompt('NetTalk Global Extension',Check),%ntGlobal,default(1),at(10)
      #Prompt('NetTalk WebServer Global Extension',Check),%ntGlobalws,default(1),at(10)
      #Prompt('StringTheory Global Extension (required by NetTalk)',Check),%ntGlobalst,default(1),at(10)
      #Prepare
        #IF(not REGISTERED('Activate_SelfService(SelfService)'))
          #SET(%ssGlobal,0)
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('Activate_SelfService(SelfService)'))
        #Prompt('Add SelfService',Check),%ssGlobal,default(1),at(10)
      #ENDenable
      #!
      #Prepare
        #IF(not REGISTERED('ActivateFileManager3(FM3)'))
          #SET(%fmGlobal,0)
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('ActivateFileManager3(FM3)'))
        #Prompt('Add File Manager 3',Check),%fmGlobal,default(1),at(10)
      #ENDenable
      #!
      #Prepare
        #IF(not REGISTERED('Activate_CSMesBox(CSMesBox)'))
          #SET(%mbGlobal,0)
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('Activate_CSMesBox(CSMesBox)'))
        #Prompt('Add Message Box',Check),%mbGlobal,default(1),at(10)
      #EndEnable
      #!
      #Prepare
        #IF(not REGISTERED('Activate_Insight(Insight)'))
          #SET(%igGlobal,0)
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('Activate_Insight(Insight)'))
        #prompt('Add Insight Graphing',Check),%igGlobal,default(1),at(10)
      #EndEnable
      #!
      #Prepare
        #IF(not REGISTERED('GloDraw(Draw)'))
          #SET(%drGlobal,0)
        #Else
          #IF(%igGlobal)
            #SET(%drGlobal,1)
          #ENDIF
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('GloDraw(Draw)'))
        #prompt('Add Draw (Required by Insight Graphing 2)',Check),%drGlobal,default(1),at(10)
      #EndEnable
      #!
      #Prepare
        #IF(not REGISTERED('User_Login_Web(SecWin10)') or 1 )
          #SET(%swGlobal,0)
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('User_Login_Web(SecWin10)') and 0)
        #prompt('Add Secwin 6',Check),%swGlobal,default(1),at(10)
      #EndEnable
      #!
      #Prepare
        #IF(not REGISTERED('Activate_GPF(CapeSoftGPF)'))
          #SET(%gpfGlobal,0)
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('Activate_GPF(CapeSoftGPF)'))
        #prompt('Add GPF Reporter',Check),%gpfGlobal,default(1),at(10)
      #EndEnable
      #!
      #Prepare
        #IF(not REGISTERED('EnableWinEvent(WinEvent)'))
          #SET(%weGlobal,0)
        #ELSE
          #IF(%ssGlobal or %gpfGlobal)
            #SET(%weGlobal,1)
          #ENDIF
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('EnableWinEvent(WinEvent)'))
        #enable(%ssGlobal=0 and %gpfGlobal=0)
          #prompt('Add WinEvent',Check),%weGlobal,default(1),at(10)
        #endEnable
      #EndEnable
      #!
      #Prepare
        #IF(not REGISTERED('PDFXToolsGlobal41(PDFXTools41)'))
          #SET(%trPdfGlobal,0)
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('PDFXToolsGlobal41(PDFXTools41)'))
        #Enable(%svpdfGlobal=0)
          #prompt('Add PDF-Tools 4.1 PDF Reports',Check),%trPdfGlobal,default(1),at(10)
        #EndEnable
      #EndEnable
      #!
      #Prepare
        #IF(not REGISTERED('SVReportToPDFGlobal(SVReportToPDFSupport)'))
          #SET(%svpdfGlobal,0)
        #ELSE
          #IF(%trPdfGlobal)
            #SET(%svpdfGlobal,0)
          #ENDIF
        #ENDIF
      #EndPrepare
      #Enable(REGISTERED('SVReportToPDFGlobal(SVReportToPDFSupport)'))
        #enable(%trPdfGlobal=0)
          #prompt('Add SV PDF Reports',Check),%svpdfGlobal,default(1),at(10)
        #EndEnable
      #EndEnable
      #!
      #button('Clear All'),WhenAccepted(%ClearAllGlobalExtensions())
      #EndButton
    #EndTab
#!----------------------------------------------------
#group(%ClearAllGlobalExtensions)
  #SET(%ntGlobal,0)
  #SET(%ntGlobalws,0)
  #SET(%ntGlobalst,0)
  #SET(%ssGlobal,0)
  #SET(%fmGlobal,0)
  #SET(%mbGlobal,0)
  #SET(%igGlobal,0)
  #SET(%drGlobal,0)
  #SET(%swGlobal,0)
  #SET(%gpfGlobal,0)
  #SET(%weGlobal,0)
  #SET(%svpdfGlobal,0)
  #SET(%trPdfGlobal,0)
#!----------------------------------------------------
#GROUP(%SecwinWarningTab)
    #TAB('Secwin'),FINISH(1),Where((%swGlobal=1) and (%SecwinInDict=0))
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle,700),at(80,0,,12)
        #Display('Secwin'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #display('To support Secwin the Secwin tables must be in your dictionary.'),PROP(PROP:FontColor, 00000FFH)
      #display('If they are not there, quit the wizard now, and go and import'),PROP(PROP:FontColor, 00000FFH)
      #display('them - as per the Secwin documentation.'),PROP(PROP:FontColor, 00000FFH)
    #EndTab
#!----------------------------------------------------
#GROUP(%ProcedureTypesTab)
    #TAB('Procedures'),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle,700),at(80,0,,12)
        #Display('Procedures'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #prompt('Generate WebServer',Check),%genWebServer,default(1),at(10)
      #prompt('Generate WebHandler',Check),%genWebHandler,default(1),at(10)
      #prompt('Generate Generic Header',Check),%genHeader,default(1),at(10)
      #prompt('Generate Generic Footer',Check),%genFooter,default(1),at(10)
      #prompt('Generate IndexPage',Check),%genIndexPage,default(1),at(10)
      #prepare
        #IF(%swglobal=1)
          #SET(%genLoginForm,0)
        #ENDIF
      #endprepare
      #enable(%swglobal=0)
        #prompt('Generate LoginForm',Check),%genLoginForm,default(1),at(10)
      #endEnable
      #prepare
        #IF(%DictionaryFile='')
          #SET(%genBrowses,0)
          #SET(%genForms,0)
        #ENDIF
      #EndPrepare
      #Enable(%DictionaryFile <> '')
        #prompt('Generate Browses',Check),%genBrowses,default(1),at(10)
        #prompt('Generate Forms',Check),%genForms,default(1),at(10)
      #EndEnable
    #EndTab
#!----------------------------------------------------
#GROUP(%JustBrowseOptionsTab)
    #TAB('New Browse Procedure Options'),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle,700),at(80,0,,12)
        #Display('New Browse Procedure Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #prepare
        #IF(not varExists(%wsFiles))
          #Declare(%wsFiles),Unique
        #ENDIF
      #EndPrepare
      #Prompt('Table:',FILE),%xgenTable,WhenAccepted(%SetFormName())
      #prompt('Generate Browse',Check),%genBrowses,default(1),at(10)
      #enable(%genBrowses=1)
        #Prompt('Browse Name',@s255),%xgenBrowseName,default(%QuickProcedure)
      #endEnable
      #Prompt('Link to Form',Check),%LinkToForm,default(1),at(10)
      #Enable(%LinkToForm)
        #Prompt('Call Form As Popup',Check),%wsLayout,default(1),at(10)
        #Prompt('Form Name',@s255),%xgenFormName
        #prompt('Generate Form',Check),%genForms,default(0),at(10)
      #EndEnable
    #EndTab
#!----------------------------------------------------
#Group(%SetFormName)
  #ADD(%wsFiles,%xgenTable)
  #IF(%xgenFormName='')
    #SET(%xgenFormName,'Update' & %xGenTable)
  #ENDIF
#!----------------------------------------------------
#Group(%SetFormName2)
  #ADD(%wsFiles,%xgenTable)
#!----------------------------------------------------
#GROUP(%JustFormOptionsTab)
    #TAB('New Form Procedure Options'),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle,700),at(80,0,,12)
        #Display('New Form Procedure Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #prepare
        #IF(not varExists(%wsFiles))
          #Declare(%wsFiles),Unique
        #ENDIF
      #EndPrepare
      #Prompt('Table:',FILE),%xgenTable,WhenAccepted(%SetFormName2())
      #Prompt('Form Name',@s255),%xgenFormName,default(%QuickProcedure)
    #EndTab
#!----------------------------------------------------
#GROUP(%FilesTab)
    #TAB('Select Files for Browse and Form'),WHERE((%genBrowses=1 or %genForms = 1) and %DictionaryFile <> ''),FINISH(1)
      #prepare
        #IF(not varExists(%FileList))
          #Declare(%FileList),Unique
        #ENDIF
        #FOR(%File)
          #IF(Instring('Secwin_',%File,1,1))
            #CYCLE
          #ENDIF
          #IF(ITEMS(%key)=0)
            #CYCLE
          #ENDIF
          #ADD(%FileList,%File)
        #EndFor
      #EndPrepare
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Select Files for Browse and Form'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #PROMPT('File Selection',FROM(%FileList)),%wsFiles,INLINE,SELECTION('File Selection')
    #ENDTAB
#!----------------------------------------------------
#GROUP(%ServerOptionsTab)
    #Tab('Server Options'),Where(%genWebServer=1),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Server Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #display('Use Port 80 for normal servers, 443 for SSL servers when you deploy, but 88 for development.')
      #prompt('Port',@s10),%wsPort,default(88)
      #prompt('Create SSL Site ?',Check),%wsSSL,default(0),at(10)
      #enable(%wsSSL=1)
        #prompt('Certificate name',@s255),%wsCert
      #endenable
    #ENDTAB
#!----------------------------------------------------
#GROUP(%HeaderFooterTab)
    #TAB('Header and Footer Options'),WHERE(%genHeader=1 or %genFooter=1),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Header and Footer Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #boxed('Header'),Where(%GenHeader=1)
        #prompt('Logo File Name',@s250),%wsLogo,default('images/logo.jpg')
        #prompt('Heading Text',@s255),%wsHeading,default(%Application)
      #endBoxed
      #boxed('Footer'),Where(%GenFooter=1)
        #prompt('Copyright Message',@s255),%wsCopyright,default('Copyright <!-- Net:d:year -->')
        #Prompt('Include Session Timer in Footer (when user is logged in)',Check),%wsSessionTimer,at(10)
      #endBoxed
    #EndTab
#!----------------------------------------------------
#GROUP(%IndexPageTab)
    #TAB('IndexPage Options'),Where(%GenIndexPage=1),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('IndexPage Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #Prompt('Page Name',@s255),%wsIndexPageName,default('Index.htm')
      #Prompt('Site Title',@s255),%wsSiteTitle,default('Welcome')
    #ENDTAB
#!----------------------------------------------------
#GROUP(%BrowseOptionsTab)
    #Tab('Browse Options'),WHERE(%genBrowses=1 and %DictionaryFile <> ''),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Browse Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #prompt('Include ID field in browses',Check),%wsIncBrowseID,at(10),default(0)
      #prompt('Include In-Row Copy Button',Check),%wsInRowCopy,at(10),default(1)
      #prompt('Include In-Row Change Button',Check),%wsInRowChange,at(10),default(1)
      #prompt('Include In-Row Delete Button',Check),%wsInRowDelete,at(10),default(1)
      #prompt('Include In-Row View',Check),%wsInRowView,at(10),default(0)
      #prompt('Include In-Row Select',Check),%wsInRowSelect,at(10),default(1)
      #Button('Locators')
          #prompt('Placeholder',EXPR),%wsLocatorPlaceHolder,default('Search')
          #Prompt('Locator Position',OPTION),%wsLocatorPosition,at(5),default(1)
            #prompt('No Locator',radio),at(10),value(0)
            #prompt('Above Browse',radio),at(10),value(1)
            #prompt('Below Browse',radio),at(10),value(2)
            #prompt('Above and Below Browse',radio),at(10),value(3)
          #Enable(%wsLocatorPosition <> 0)
            #Prompt('Locator Type',DROP('Position|Starts-With[Range]|Contains|Search')),%wsLocatorType,Default('Search')
            #Enable(%wsLocatorType<>'Position')
              #Prompt('Table Blank until Locator entered',Check),%wsLocatorBlank,at(10),default(0)
              #Enable(%wsLocatorBlank=1)
                #prompt('Message waiting for locator:',EXPR),%wsLocatorMessage,default('Enter a search term in the locator')
              #EndEnable
            #EndEnable
            #Prompt('Include Search Button',Check),%wsSearchButton,At(10),default(1)
            #Prompt('Include Clear Button',Check),%wsClearButton,At(10),default(0)
            #Prompt('Field Size',EXPR),%wsBrowseLocatorSize,default(20)
            #prompt('Hide Locator Prompt',Check),%wsBrowseLocatorPromptHide,at(10),default(0)
            #prompt('Immediate Locating',Check),%wsBrowseLocatorImmediate,at(10),default(1)
          #EndEnable
      #EndButton
    #EndTab
#!----------------------------------------------------
#GROUP(%FormOptionsTab)
    #Tab('Form Options'),WHERE(%genForms=1 and %DictionaryFile <> ''),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Form Options'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #prompt('Include ID field in forms (as display field)',Check),%wsIncBrowseID,at(10),default(0)
    #EndTab
#!----------------------------------------------------
#GROUP(%DoneTab)
    #Tab('Done'),FINISH(1)
      #Boxed(''),section
        #Image('ntlogo.jpg'),at(10,0)
        #Display('NetTalk Wizard'),PROP(PROP:FontColor, 0FF6666H),PROP(PROP:FontSize, 12),PROP(PROP:FontStyle, 700),at(80,0,,12)
        #Display('Done'),PROP(PROP:FontColor, 0FF8888H),PROP(PROP:FontSize, 12),at(80,14,,12)
      #EndBoxed
      #Display('Click on FINISH to generate the procedures')
    #EndTab
#!----------------------------------------------------
#GROUP(%MakeList,%x,%y),Auto
  #Declare(%t)
  #Declare(%a)
  #SET(%t,%x)
  #SET(%a,%t)
  #LOOP WHILE (%t < %y)
    #SET(%t,%t+1)
    #SET(%a,%a & ',' & %t)
  #ENDLOOP
  #RETURN(%a)
#!----------------------------------------------------
#GROUP(%splitfilename,%f),Auto
  #declare(%a)
  #declare(%x)
  #loop
    #IF(%x=1)
      #SET(%a,upper(sub(%f,1,1)))
    #ELSIF(sub(%f,%x,1) >= 'A' and sub(%f,%x,1) <= 'Z' and (sub(%f,%x-1,1) <= 'A' or sub(%f,%x-1,1) >= 'Z'))
      #Set(%a,%a & ' ' & sub(%f,%x,1))
    #ELSE
      #Set(%a,%a & sub(%f,%x,1))
    #ENDIF
    #Set(%x,%x+1)
    #IF(%x > LEN(%f))
      #BREAK
    #ENDIF
  #ENDLOOP
  #SET(%x,1)
  #RETURN(%a)
#!----------------------------------------------------
#GROUP(%ForeignIdField,%fil),AUTO
  #FIX(%file,%fil)
  #FOR(%Key),Where(%KeyPrimary)
    #FOR(%KeyField)
      #RETURN(%KeyField)
    #ENDFOR
  #ENDFOR
#!----------------------------------------------------
#GROUP(%GetDescriptionField,%fil),AUTO
  #Declare(%counter)
  #FIX(%file,%fil)
#!
  #FOR(%Key),Where((ITEMS(%KeyField)=1) AND (%KeyDuplicate=0))
    #FOR(%KeyField), Where(Instring('GUID',upper(%KeyField),1,1)=0 and %KeyField<>'ID' and %keyfield <> 'CODE')
      #FIX(%Field,%KeyField)
      #IF((%FieldType='STRING' or %FieldType='CSTRING' or %FieldType='PSTRING'))
        #RETURN(%Field)
      #ENDIF
    #ENDFOR
  #ENDFOR
#!
  #FOR(%Key),Where(ITEMS(%KeyField)=1)
    #FOR(%KeyField), Where(Instring('GUID',upper(%KeyField),1,1)=0 and %KeyField<>'ID' and %keyfield <> 'CODE')
      #FIX(%Field,%KeyField)
      #IF((%FieldType='STRING' or %FieldType='CSTRING' or %FieldType='PSTRING'))
        #RETURN(%Field)
      #ENDIF
    #ENDFOR
  #ENDFOR
#!
  #FOR(%Key)
    #FOR(%KeyField)
      #IF(Instring('GUID',upper(%KeyField),1,1)=0 and %KeyField<>'ID' and %keyfield <> 'CODE')
        #FIX(%Field,%KeyField)
        #IF((%FieldType='STRING' or %FieldType='CSTRING' or %FieldType='PSTRING'))
          #RETURN(%Field)
        #ENDIF
      #ENDIF
      #BREAK
    #ENDFOR
  #ENDFOR
#!
  #FOR(%field),Where((%FieldType='STRING' or %FieldType='CSTRING' or %FieldType='PSTRING') and (Instring('GUID',upper(%Field),1,1)=0))
    #RETURN(%Field)
  #ENDFOR
#!
  #FOR(%Key)
    #FOR(%KeyField)
      #Fix(%Field,%KeyField)
      #IF(upper(sub(%FieldPicture,2,1)) = 'D')
        #RETURN(%KeyField)
      #ELSE
        #BREAK
      #ENDIF
    #ENDFOR
  #ENDFOR
#!
  #FOR(%Key),Where(%KeyPrimary)
    #FOR(%KeyField)
      #RETURN(%KeyField)
    #ENDFOR
  #ENDFOR
#!
  #FOR(%Field)
    #RETURN(%field)
  #ENDFOR
#!----------------------------------------------------
#GROUP(%GetForeignDetails,%parentfield,* %childfile,* %childidfield,* %childdescfield),Auto
  #Find(%Field,%parentfield)
  #Set(%childfile,'')
  #set(%childidfield,'')
  #set(%childdescfield,'')
  #FOR(%Relation),Where(%Relation=%FieldLookup or (%FieldValidation<>'INFILE' and %FileRelationType='MANY:1'))
    #FOR(%FileKeyField),Where(%FileKeyField=%Field)
      #Set(%childfile,%Relation)
      #Set(%childidfield,%FileKeyFieldLink)
      #CALL(%GetDescriptionField,%Relation),%childdescfield
    #ENDFOR
  #ENDFOR
#!----------------------------------------------------
#GROUP(%ServerAppTxa)
[APPLICATION]
VERSION 28
TODO ABC ToDo
PROCEDURE WebServer
[COMMON]
FROM ABC
[PROMPTS]
%%WindowFrameDragging LONG  (0)
#!-------------------------------------------------------
#GROUP(%ServerAppTxaEnd)
[PERSIST]
[PROJECT]
-- Generator
[PROGRAM]
[COMMON]
FROM ABC ABC
[END]
[MODULE]
[COMMON]
FROM ABC GENERATED
[END]
[MODULE]
[COMMON]
FROM ABC GENERATED
[PROCEDURE]
NAME WebServer
[COMMON]
[END]
#!----------------------------------------------------
#GROUP(%NetTalkTxa)
[ADDITION]
NAME NetTalk Activate_NetTalk
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
%%DynamicDLL LONG  (1)
%%NTSecwin4IsHere LONG  (%swGlobal)
#!- - - - - - - - - - - - - - - - - - - - - - - - - -
[ADDITION]
NAME NetTalk NetWebServerGlobal
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter - 1)
PARENT %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 2)
[PROMPTS]
%%gPopups DEFAULT  ('Default')
#!----------------------------------------------------
#GROUP(%WinEventTxa)
[ADDITION]
NAME WinEvent EnableWinEvent
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
%%gloAutoDown LONG  (1)
#!----------------------------------------------------
#GROUP(%StringTheoryTxa)
[ADDITION]
NAME StringTheory Activate_StringTheory
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter +1)
[PROMPTS]
%%md5 LONG  (0)
#!----------------------------------------------------
#GROUP(%SelfServiceTxa)
[ADDITION]
NAME SelfService Activate_SelfService
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter +1)
[PROMPTS]
%%ssServiceNameNew DEFAULT  ('''<<YourServiceName>''')
%%ssDisplayNameNew DEFAULT  ('''<<Display Name>''')
%%ssDescriptionNew DEFAULT  ('''<<Your Service Description>''')
%%ssInstallUserNameNew DEFAULT  ('''.\UserName''')
%%ssInstallPasswordNew DEFAULT  ('''Password''')
%%ssServiceName DEFAULT  ('<<YourServiceName>')
%%ssDisplayName DEFAULT  ('<<Display Name>')
%%ssDescription DEFAULT  ('<<Your Service Description>')
%%ssInstallUserName DEFAULT  ('.\UserName')
%%ssInstallPassword DEFAULT  ('Password')
#!----------------------------------------------------
#GROUP(%FM3Txa)
[ADDITION]
NAME FM3 ActivateFileManager3
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter +1)
[PROMPTS]
#CALL(%IsDriver,'CLARION'),%temp1
%%ClaDrv LONG %temp1
#CALL(%IsDriver,'TOPSPEED'),%temp1
%%TpsDrv LONG %temp1
#CALL(%IsDriver,'BTRIEVE'),%temp1
%%BtrDrv LONG %temp1
#CALL(%IsDriver,'MSSQL'),%temp1
%%mssDrv LONG %temp1
#CALL(%IsDriver,'ORACLE'),%temp1
%%oraDrv LONG %temp1
#CALL(%IsDriver,'SYBASE'),%temp1
%%asaDrv LONG %temp1
#CALL(%IsDriver,'ODBC'),%temp1
%%OdbcDrv LONG %temp1
%%CTSBProc PROCEDURE (SQL_Connect)
#!----------------------------------------------------
#GROUP(%IsDriver,%pDriver)
  #FOR(%wsFiles)
    #FIX(%file,%wsfiles)
    #IF(%FileDriver=%pDriver)
      #RETURN 1
    #ENDIF
  #ENDFOR
  #RETURN 0
#!----------------------------------------------------
#GROUP(%MessageBoxTxa)
[ADDITION]
NAME CSMesBox Activate_CSMesBox
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
%%UseThreadSafeMesBox DEFAULT  ('1')
%%UseDSAll LONG  (1)
%%MesMsgProcedure PROCEDURE  (ds_Message)
%%MesStopProcedure PROCEDURE  (ds_Stop)
%%MesHaltProcedure PROCEDURE  (ds_Halt)
%%MesDisplayTimeOut LONG  (1)
%%MesTimeOutAll DEFAULT  ('500')
#!----------------------------------------------------
#Group(%PDFXTools41Txa)
[ADDITION]
NAME PDFXTools41 PDFXToolsGlobal41
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
#!----------------------------------------------------
#Group(%SVPDFX)
[ADDITION]
NAME SVReportToPDFSupport SVReportToPDFGlobal
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
#!----------------------------------------------------
#GROUP(%InsightTxa)
[ADDITION]
NAME Insight Activate_Insight
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
%%TemplateLevel DEFAULT  ('3')
#!----------------------------------------------------
#GROUP(%DrawTxa)
[ADDITION]
NAME Draw GloDraw
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
#!----------------------------------------------------
#GROUP(%SecwinTxa)
[ADDITION]
NAME Secwin10 Activate_Security
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
  #SET(%temp1,RANDOM(10000000,99999999))
%%SecEncryptionKey DEFAULT  ('''%temp1''')
%%AllowCreate DEFAULT  (1)
#!----------------------------------------------------
#GROUP(%HyperactiveTxa)
[ADDITION]
NAME HyperActive GloHyperActive
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
#!----------------------------------------------------
#GROUP(%GPFReporterTxa)
[ADDITION]
NAME CapeSoftGPF Activate_GPF
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
%%WinEventHere LONG  (1)
%%HyperActiveHere LONG  (1)
%%SelfServiceHere LONG  (1)
%%GPFEmail DEFAULT  ('The developer <<support@example.com>')
%%GPFDumpFileName DEFAULT  ('GPFReport.log')
%%GPFMBOptions DEFAULT  ('4')
%%GPFRestartProgram LONG  (1)
#!----------------------------------------------------
#GROUP(%PDFTools41Txa)
[ADDITION]
NAME PDFXTools41 PDFXToolsGlobal41
[INSTANCE]
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
#!----------------------------------------------------
#GROUP(%svPDFTxa)
[ADDITION]
NAME SVReportToPDFSupport SVReportToPDFGlobal
[INSTANCE]
INSTANCE %InstanceCounter
#SET(%InstanceCounter,%InstanceCounter + 1)
[PROMPTS]
#!----------------------------------------------------
#GROUP(%WebserverTxa)
[PROCEDURE]
NAME WebServer
PROTOTYPE '(<<NetWebServer pServer>),name(''WebServer'')'
[COMMON]
FROM ABC Window
[DATA]
WebLog                   GROUP,PRE(web)
EnableLogging              LONG
!!> INITIAL('1')
LastGet                    STRING(4096)
LastPost                   STRING(4096)
StartDate                  LONG
StartTime                  LONG
PagesServed                LONG
                         END
LogQueue                 QUEUE,PRE(LogQ)
Port                       LONG
Date                       LONG
Time                       LONG
Desc                       STRING(4096)
                         END
[PROMPTS]
%%Parameters DEFAULT  ('(<NetWebServer pServer>)')
[ADDITION]
NAME ABC CloseButton
[INSTANCE]
INSTANCE 2
[ADDITION]
NAME NetTalk IncludeNetTalkObject
[INSTANCE]
INSTANCE 3
[PROMPTS]
%%ClassItem UNIQUE DEFAULT  ('Default')
%%ThisObjectName DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('ThisWebServer')

%%ABCBaseClass DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('NetWebServer')

%%ListenOnPort DEPEND %%ClassItem DEFAULT TIMES 1
#IF(%wsport)
  #SET(%temp1,%wsport)
#ELSE
  #IF(%wsSSL)
    #SET(%temp1,443)
  #ELSE
    #SET(%temp1,88)
  #ENDIF
#ENDIF
WHEN  ('Default') ('%wsPort')

%%WebHandlerProc DEPEND %%ClassItem PROCEDURE TIMES 1
WHEN  ('Default') (WebHandler)

%%theme DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('''%wsTheme''')

%%WebDirectory DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('''web''')

%%WebDatePicture DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('@D6')

%%NetSuppressErrorMsg DEPEND %%ClassItem LONG TIMES 1
WHEN  ('Default') (1)

%%GenericPageHeaderTag DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('''<<!-- Net:PageHeaderTag -->''')

%%GenericPageFooterTag DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('''<<!-- Net:PageFooterTag -->''')

%%WebDefaultPage DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('''IndexPage''')

%%WebFormStyle DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('Net:Web:Tab')

%%WebLoginPage DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('''%LoginForm''')

%%WebLoginPageControl DEPEND %%ClassItem LONG TIMES 1
WHEN  ('Default') (1)

%%WebSecure DEPEND %%ClassItem LONG TIMES 1
WHEN  ('Default') (%wsSSL)

%%Certificate DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('''certificates\%wsCert''')

%%IncludeHTMLEditor DEPEND %%ClassItem LONG TIMES 1
WHEN  ('Default') (0)

%%ServerCombine DEPEND %%ClassItem LONG TIMES 1
WHEN  ('Default') (1)

%%ServerPreCompressed DEPEND %%ClassItem LONG TIMES 1
WHEN  ('Default') (1)

%%ServerTimeout DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('90001')

%%ServerCharset DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('''utf-8''')

%%netStoreAs DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('net:StoreAsUTF8')

%%Styles DEPEND %%ClassItem MULTI LONG TIMES 1
WHEN  ('Default') (1)

#!IF(%wsCustomCss)
%%StyleName DEPEND %%Styles DEFAULT TIMES 1
WHEN  ('Default')TIMES 1
WHEN  (1) ('''Custom.Css''')
#!ENDIF

#!IF(%wsCustomTheme)
%%ThemeFiles DEPEND %%ClassItem MULTI LONG TIMES 1
WHEN  ('Default') (1)

%%ThemeName DEPEND %%ThemeFiles DEFAULT TIMES 1
WHEN  ('Default')TIMES 1
WHEN  (1) ('''CustomT.Css''')
#!ENDIF

[ADDITION]
NAME NetTalk NetWebServerLogging
[INSTANCE]
INSTANCE 1
[PROMPTS]
%%LogFile FILE  ()
%%LogField FIELD  ()
%%LogName FIELD  ()
[ADDITION]
NAME NetTalk NetWebServerPerformance
[INSTANCE]
INSTANCE 4
  #IF(%swGlobal)
[ADDITION]
NAME SecWin10 User_Login
[INSTANCE]
INSTANCE 5
  #ENDIF
[WINDOW]
Window  WINDOW('NetTalk Web Server'),AT(,,441,315),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),DOUBLE,AUTO,GRAY,IMM,SYSTEM
          SHEET,AT(5,5,431,290),USE(?SHEET1)
            TAB('log'),USE(?TabLog)
              GROUP('Logging Group'),AT(13,30,415,245),USE(?LogGroup),#ORIG(?LogGroup),#SEQ(1)
                LIST,AT(18,35,405,70),USE(?LogQueue),VSCROLL,COLOR(,COLOR:Black,00F0F0F0h),GRID(00F0F0F0h),FORMAT('51L(2)|M~IP Address~@s30@51L(2)|M~Date~@D17B@36L(2)|M~Time~@T4B@1020L(2)|M~Description~@s255@'),FROM(LogQueue),#ORIG(?LogQueue),#SEQ(1)
                TEXT,AT(18,110,200,135),USE(web:LastGet),VSCROLL,#ORIG(web:LastGet),#SEQ(1)
                TEXT,AT(223,110,200,135),USE(web:LastPost),VSCROLL,#ORIG(web:LastPost),#SEQ(1)
                OPTION('Log:'),AT(18,249,141,22),USE(web:EnableLogging),BOXED,#ORIG(?Option1),#SEQ(1)
                  RADIO('No'),AT(25,257),USE(?Option1:Radio1),VALUE('0'),#ORIG(?Option1:Radio1),#SEQ(1)
                  RADIO('Screen'),AT(53,257),USE(?Option1:Radio2),VALUE('1'),#ORIG(?Option1:Radio2),#SEQ(1)
                  RADIO('Screen && Disk'),AT(98,257),USE(?Option1:Radio3),VALUE('2'),#ORIG(?Option1:Radio3),#SEQ(1)
                END
                BUTTON('Clear'),AT(162,254,45,16),USE(?Clear),#ORIG(?Clear),#SEQ(1)
              END
            END
            TAB('Performance'),USE(?TabPerformance)
              STRING('Started At:'),AT(14,29),USE(?StartDate),TRN,#ORIG(?StartDate),#SEQ(4)
              STRING(@d17),AT(92,29),USE(wp:StartDateA),TRN,#ORIG(wp:StartDateA),#SEQ(4)
              STRING(@t1),AT(157,29),USE(wp:StartTimeA),TRN,#ORIG(wp:StartTimeA),#SEQ(4)
              GROUP('Number of Requests'),AT(14,43,173,82),USE(?Perfrequests),BOXED,#ORIG(?Perfrequests),#SEQ(4)
                STRING('Total:'),AT(23,55),USE(?NumberOfRequests),TRN,#ORIG(?NumberOfRequests),#SEQ(4)
                STRING(@n14),AT(86,55),USE(wp:NumberOfRequests),RIGHT,TRN,#ORIG(wp:NumberOfRequests),#SEQ(4)
                STRING('Spiders:'),AT(23,68),USE(?NumberOfSpiderRequests),TRN,#ORIG(?NumberOfSpiderRequests),#SEQ(4)
                STRING(@n14),AT(86,68),USE(wp:NumberOfSpiderRequests),RIGHT,TRN,#ORIG(wp:NumberOfSpiderRequests),#SEQ(4)
                STRING('Not Found (404):'),AT(23,81),USE(?NumberOf404Requests),TRN,#ORIG(?NumberOf404Requests),#SEQ(4)
                STRING(@n14),AT(86,81),USE(wp:NumberOf404Requests),RIGHT,TRN,#ORIG(wp:NumberOf404Requests),#SEQ(4)
                STRING('Too Busy (500):'),AT(23,94),USE(?NumberOf500Requests),TRN,#ORIG(?NumberOf500Requests),#SEQ(4)
                STRING(@n14),AT(86,94),USE(wp:NumberOf500Requests),RIGHT,TRN,#ORIG(wp:NumberOf500Requests),#SEQ(4)
                STRING('Total Time:'),AT(23,107),USE(?TotalRequestTime),TRN,#ORIG(?TotalRequestTime),#SEQ(4)
                STRING(@n10.2),AT(103,107),USE(wp:TotalRequestTime),RIGHT,TRN,#ORIG(wp:TotalRequestTime),#SEQ(4)
              END
              GROUP('Performance Breakdown'),AT(14,128,412,60),USE(?PerfBreakdown),BOXED,#ORIG(?PerfBreakdown),#SEQ(4)
                STRING('<< 0.5'),AT(136,136),USE(?LT05),FONT(,,,FONT:bold),RIGHT,TRN,#ORIG(?LT05),#SEQ(4)
                STRING('<< 1'),AT(207,136),USE(?LT1),FONT(,,,FONT:bold),RIGHT,TRN,#ORIG(?LT1),#SEQ(4)
                STRING('<< 2'),AT(271,136),USE(?LT2),FONT(,,,FONT:bold),RIGHT,TRN,#ORIG(?LT2),#SEQ(4)
                STRING('<< 5'),AT(335,136),USE(?LT5),FONT(,,,FONT:bold),RIGHT,TRN,#ORIG(?LT5),#SEQ(4)
                STRING('> 5'),AT(399,136),USE(?GT5),FONT(,,,FONT:bold),RIGHT,TRN,#ORIG(?GT5),#SEQ(4)
                STRING('Number of requests:'),AT(23,149),USE(?Numberbreakdown),TRN,#ORIG(?Numberbreakdown),#SEQ(4)
                STRING(@n11),AT(99,149),USE(wp:NumberOfRequestsLTHalf),RIGHT,TRN,#ORIG(wp:NumberOfRequestsLTHalf),#SEQ(4)
                STRING(@n11),AT(163,149),USE(wp:NumberOfRequestsLTOne),RIGHT,TRN,#ORIG(wp:NumberOfRequestsLTOne),#SEQ(4)
                STRING(@n11),AT(227,149),USE(wp:NumberOfRequestsLTTwo),RIGHT,TRN,#ORIG(wp:NumberOfRequestsLTTwo),#SEQ(4)
                STRING(@n11),AT(291,149),USE(wp:NumberOfRequestsLTFive),RIGHT,TRN,#ORIG(wp:NumberOfRequestsLTFive),#SEQ(4)
                STRING(@n11),AT(355,149),USE(wp:NumberOfRequestsGTFive),RIGHT,TRN,#ORIG(wp:NumberOfRequestsGTFive),#SEQ(4)
                STRING('Total Response Time (s):'),AT(23,162),USE(?totaltimeBreakdown),TRN,#ORIG(?totaltimeBreakdown),#SEQ(4)
                STRING(@n11),AT(99,162),USE(wp:RequestTimeLTHalf),RIGHT,TRN,#ORIG(wp:RequestTimeLTHalf),#SEQ(4)
                STRING(@n11),AT(163,162),USE(wp:RequestTimeLTOne),RIGHT,TRN,#ORIG(wp:RequestTimeLTOne),#SEQ(4)
                STRING(@n11),AT(227,162),USE(wp:RequestTimeLTTwo),RIGHT,TRN,#ORIG(wp:RequestTimeLTTwo),#SEQ(4)
                STRING(@n11),AT(291,162),USE(wp:RequestTimeLTFive),RIGHT,TRN,#ORIG(wp:RequestTimeLTFive),#SEQ(4)
                STRING(@n11),AT(355,162),USE(wp:RequestTimeGTFive),RIGHT,TRN,#ORIG(wp:RequestTimeGTFive),#SEQ(4)
                STRING('Average Response Time (s):'),AT(23,175),USE(?timeBreakdown),TRN,#ORIG(?timeBreakdown),#SEQ(4)
                STRING(@n4.2),AT(133,175),USE(wp:AverageRequestTimeLTHalf),RIGHT,TRN,#ORIG(wp:AverageRequestTimeLTHalf),#SEQ(4)
                STRING(@n4.2),AT(197,175),USE(wp:AverageRequestTimeLTOne),RIGHT,TRN,#ORIG(wp:AverageRequestTimeLTOne),#SEQ(4)
                STRING(@n4.2),AT(261,175),USE(wp:AverageRequestTimeLTTwo),RIGHT,TRN,#ORIG(wp:AverageRequestTimeLTTwo),#SEQ(4)
                STRING(@n4.2),AT(325,175),USE(wp:AverageRequestTimeLTFive),RIGHT,TRN,#ORIG(wp:AverageRequestTimeLTFive),#SEQ(4)
                STRING(@n10),AT(359,175),USE(wp:AverageRequestTimeGTFive),RIGHT,TRN,#ORIG(wp:AverageRequestTimeGTFive),#SEQ(4)
              END
              GROUP('Resources'),AT(14,195,232,90),USE(?PerfResources),BOXED,#ORIG(?PerfResources),#SEQ(4)
                STRING('Current'),AT(128,203),USE(?Current),FONT(,,,FONT:bold),TRN,#ORIG(?Current),#SEQ(4)
                STRING('Maximum'),AT(182,203),USE(?Maximum),FONT(,,,FONT:bold),TRN,#ORIG(?Maximum),#SEQ(4)
                STRING('Threads:'),AT(23,216),USE(?Threads),TRN,#ORIG(?Threads),#SEQ(4)
                STRING(@n11),AT(99,216),USE(wp:NumberOfThreads),RIGHT,TRN,#ORIG(wp:NumberOfThreads),#SEQ(4)
                STRING(@n11),AT(162,216),USE(wp:MaximumThreads),RIGHT,TRN,#ORIG(wp:MaximumThreads),#SEQ(4)
                STRING('Sessions'),AT(23,229),USE(?Sessions),TRN,#ORIG(?Sessions),#SEQ(4)
                STRING(@n11),AT(99,229),USE(wp:NumberOfSessions),RIGHT,TRN,#ORIG(wp:NumberOfSessions),#SEQ(4)
                STRING(@n11),AT(162,229),USE(wp:MaximumSessions),RIGHT,TRN,#ORIG(wp:MaximumSessions),#SEQ(4)
                STRING('Session Data:'),AT(23,242),USE(?SessionData),TRN,#ORIG(?SessionData),#SEQ(4)
                STRING(@n11),AT(99,242),USE(wp:NumberOfSessionData),RIGHT,TRN,#ORIG(wp:NumberOfSessionData),#SEQ(4)
                STRING(@n11),AT(162,242),USE(wp:MaximumSessionData),RIGHT,TRN,#ORIG(wp:MaximumSessionData),#SEQ(4)
                STRING('Thread Pool:'),AT(23,255),USE(?threadPool),TRN,#ORIG(?threadPool),#SEQ(4)
                STRING(@n11),AT(99,255),USE(wp:NumberOfThreadPool),RIGHT,TRN,#ORIG(wp:NumberOfThreadPool),#SEQ(4)
                STRING(@n11),AT(162,255),USE(wp:MaximumThreadPool),RIGHT,TRN,#ORIG(wp:MaximumThreadPool),#SEQ(4)
                STRING('Connections:'),AT(23,268),USE(?Connections),TRN,#ORIG(?threadPool),#SEQ(4)
                STRING(@n11),AT(99,268),USE(wp:NumberOfConnections),RIGHT,TRN,#ORIG(wp:NumberOfConnections),#SEQ(4)
                STRING(@n11),AT(162,268),USE(wp:MaximumConnections),RIGHT,TRN,#ORIG(wp:MaximumConnections),#SEQ(4)
              END
              BUTTON('Clear'),AT(251,267,45,16),USE(?ClearStatsButton),#ORIG(?ClearStatsButton),#SEQ(4)
            END
          END
          BUTTON('Close'),AT(391,298,45,14),USE(?Close),#ORIG(?Close),#SEQ(2)
        END
#!----------------------------------------------------
#GROUP(%WebHandlerTxa)
[PROCEDURE]
NAME WebHandler
PROTOTYPE '(String p_String)'
[COMMON]
FROM NetTalk NetWebHandleRequest
CATEGORY 'NetWebHandleRequest'
[PROMPTS]
[ADDITION]
NAME NetTalk IncludeNetTalkObject
[INSTANCE]
INSTANCE 2
[PROMPTS]
%%ClassItem UNIQUE DEFAULT  ('Default')

%%ThisObjectName DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('p_web')

%%ABCBaseClass DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('NetWebServerWorker')

%%NetSuppressErrorMsg DEPEND %%ClassItem LONG TIMES 1
WHEN  ('Default') (1)

%%SimpleMode DEPEND %%ClassItem DEFAULT TIMES 1
WHEN  ('Default') ('Client')
[CALLS]
PageFooterTag
PageHeaderTag
IndexPage
%LoginForm
#!----------------------------------------------------
#GROUP(%HeaderTxa)
[PROCEDURE]
NAME PageHeaderTag
PROTOTYPE '(NetWebServerWorker p_web)'
GLOBAL
[COMMON]
DESCRIPTION '***** Includes Menu *****'
FROM NetTalk NetWebSource
[PROMPTS]
%%GenerateDiv LONG  (1)
%%htmls MULTI LONG  (1,2,3)
%%HtmlWhat DEPEND %%htmls DEFAULT TIMES 3
WHEN  (1) ('1')
WHEN  (2) ('1')
WHEN  (3) ('1')

%%HtmlProc DEPEND %%htmls PROCEDURE TIMES 0

%%HtmlParams DEPEND %%htmls DEFAULT TIMES 3
WHEN  (1) ('p_web')
WHEN  (2) ('p_web')
WHEN  (3) ('p_web')

%%HtmlReturn DEPEND %%htmls FIELD TIMES 0

%%htmlCondition DEPEND %%htmls DEFAULT TIMES 3
WHEN  (1) ('p_web.PageName = p_web.site.LoginPage')
WHEN  (2) ('p_web.GetSessionLoggedIn() = 0 and p_web.PageName <<> p_web.site.LoginPage')
WHEN  (3) ('p_web.GetSessionLoggedIn() and p_web.PageName <<> p_web.site.LoginPage')

%%htmlLocation DEPEND %%htmls DEFAULT TIMES 3
WHEN  (1) ('201')
WHEN  (2) ('201')
WHEN  (3) ('201')

%%HtmlName DEPEND %%htmls DEFAULT TIMES 3
WHEN  (1) ('HeadingPlain')
WHEN  (2) ('HeadingWhenLoggedOut')
WHEN  (3) ('HeadingWhenLoggedIn')

#IF(%wsLogo)
  #SET(%temp1,'<<img src="'&QUOTE(%wsLogo)&'" />')
#ELSE
  #SET(%Temp1,'')
#ENDIF
#SET(%temp2,QUOTE(%wsHeading))
%%Html DEPEND %%htmls DEFAULT TIMES 3
WHEN  (1) ('<<div class="nt-left nt-site-header-logo">%temp1<</div><<h1>%temp2<</h1>')
WHEN  (2) ('<<div class="nt-left nt-site-header-logo">%temp1<</div><<h1>%temp2<</h1>')
WHEN  (3) ('<<div class="nt-left nt-site-header-logo">%temp1<</div><<h1>%temp2<</h1>')

%%DropList3 DEFAULT  ('Before <<body>[101]|After <<body>[201]|In Processed code[301]')
%%CssDiv DEFAULT  ('''nt-site-header-6 ui-widget-header ui-corner-top''')
%%IsHeader LONG  (1)
%%MobileCssDiv DEFAULT  ('''''')

#IF(%wsMenu=1)
#INSERT(%MenuTxa)
#ENDIF
#!----------------------------------------------------
#GROUP(%MenuTxa)
[ADDITION]
NAME NetTalk NetWebMenu
[INSTANCE]
INSTANCE 3
[PROMPTS]
%%MenuStyle DEFAULT  ('%wsMenuStyle')
#IF(%swGlobal)
%%Menus MULTI LONG  (1,2,3,4,5)
#ELSE
%%Menus MULTI LONG  (1,2,3,4)
#ENDIF

#IF(%swGlobal)
%%Menu DEPEND %%Menus DEFAULT TIMES 5
#ELSE
%%Menu DEPEND %%Menus DEFAULT TIMES 4
#ENDIF
WHEN  (1) ('''Home''')
WHEN  (2) ('''Browse''')
WHEN  (3) ('''Login''')
WHEN  (4) ('''Logout''')
#IF(%swGlobal)
WHEN  (5) ('''Security''')
#ENDIF

%%MenuCondition DEPEND %%Menus DEFAULT TIMES 2
WHEN  (3) ('p_web.GetSessionLoggedIn() = 0 and p_web.PageName <<> p_web.site.LoginPage')
WHEN  (4) ('p_web.GetSessionLoggedIn() = 1 and p_web.PageName <<> p_web.site.LoginPage')

%%MenuURL DEPEND %%Menus DEFAULT TIMES 1
WHEN  (1) ('''IndexPage''')

%%MenuProc DEPEND %%Menus PROCEDURE TIMES 4
WHEN  (1) ()
WHEN  (2) ()
WHEN  (3) (%LoginForm)
WHEN  (4) (%LoginForm)

#IF(%swGlobal)
%%MenuLoggedIn DEPEND %%Menus LONG TIMES 5
#ELSE
%%MenuLoggedIn DEPEND %%Menus LONG TIMES 4
#ENDIF
WHEN  (1) (0)
WHEN  (2) (0)
WHEN  (3) (0)
WHEN  (4) (1)
#IF(%swGlobal)
WHEN  (5) (1)
#ENDIF

#IF(%wsLayout=3)
%%OpenMenuAs DEFAULT  ('Popup')
%%MenuProcPopup DEPEND %%Menus LONG TIMES 4
WHEN  (1) (0)
WHEN  (2) (0)
WHEN  (3) (1)
WHEN  (4) (1)
#ENDIF

#IF(%swGlobal)
%%MenuItems DEPEND %%Menus MULTI LONG TIMES 2
#ELSE
%%MenuItems DEPEND %%Menus MULTI LONG TIMES 1
#ENDIF
#CALL(%MakeList,1,ITEMS(%wsFiles)),%temp1
WHEN  (2) (%temp1)
#IF(%swGlobal)
#CALL(%MakeList,ITEMS(%wsFiles)+1,ITEMS(%wsFiles)+3),%temp1
WHEN  (5) (%temp1)
#ENDIF
#!--
#SET(%rFiles,ITEMS(%wsFiles))
#SET(%Temp1,1)
#IF(%swGlobal)
%%MenuItem DEPEND %%MenuItems DEFAULT TIMES 2
#ELSE
%%MenuItem DEPEND %%MenuItems DEFAULT TIMES 1
#ENDIF
WHEN  (2) TIMES %rFiles
#FOR(%wsFiles)
  #CALL(%splitfilename,%wsFiles),%temp2
WHEN  (%temp1) ('''%temp2''')
  #SET(%Temp1,%Temp1+1)
#ENDFOR
#!--
#IF(%swGlobal)
WHEN  (5)TIMES 3
#SET(%Temp1,ITEMS(%wsFiles)+1)
WHEN  (%temp1) ('''Set Menu Access''')
#SET(%Temp1,ITEMS(%wsFiles)+2)
WHEN  (%temp1) ('''Operators''')
#SET(%Temp1,ITEMS(%wsFiles)+3)
WHEN  (%temp1) ('''User Groups''')
#ENDIF
#SET(%rFiles,ITEMS(%wsFiles))
#SET(%Temp1,1)
%%MenuItemLevel DEPEND %%MenuItems DEFAULT TIMES 1
WHEN  (2) TIMES %rFiles
#FOR(%wsFiles)
WHEN  (%temp1) ('1')
  #SET(%Temp1,%Temp1+1)
#ENDFOR
#!--
#! creates a very large app tree, that comes undone with large dicts' Trying the URL approach instead.
#! if this is put back, then note the existing procs set for secwin stuff.
  #IF(%wsLinkMenu)
    #SET(%rFiles,ITEMS(%wsFiles))
    #SET(%Temp1,1)
%%MenuItemProc DEPEND %%MenuItems DEFAULT TIMES 1
WHEN  (2)TIMES %rFiles
    #FOR(%wsFiles)
WHEN  (%temp1) (Browse%wsFiles)
      #SET(%Temp1,%Temp1+1)
    #ENDFOR
    #SET(%rFiles,ITEMS(%wsFiles))
    #SET(%Temp1,1)
  #ELSE
%%MenuItemURL DEPEND %%MenuItems DEFAULT TIMES 1
WHEN  (2)TIMES %rFiles
    #FOR(%wsFiles)
WHEN  (%temp1) ('''Browse%wsFiles''')
      #SET(%Temp1,%Temp1+1)
    #ENDFOR
  #ENDIF
#!--
  #IF(%swGlobal)
%%MenuItemProc DEPEND %%MenuItems PROCEDURE TIMES 1
WHEN 5TIMES3
    #SET(%Temp1,ITEMS(%wsFiles)+1)
WHEN  (%temp1) ('''Set Menu Access''')
    #SET(%Temp1,ITEMS(%wsFiles)+2)
WHEN  (%temp2) ('''Operators''')
    #SET(%Temp1,ITEMS(%wsFiles)+3)
WHEN  (%temp3) ('''User Groups''')

%%MenuItemParms DEPEND %%MenuItems DEFAULT TIMES 1
WHEN  (5)TIMES 1
  #SET(%Temp1,ITEMS(%wsFiles)+1)
WHEN  (%Temp) ('''_Screen_=PageHeaderTag''')
  #ENDIF
#!--
#IF(%wsLayout=3)
  #SET(%rFiles,ITEMS(%wsFiles))
  #SET(%Temp1,1)
%%MenuItemProcPopup DEPEND %%MenuItems DEFAULT TIMES 1
WHEN  (2)TIMES %rFiles
  #FOR(%wsFiles)
WHEN  (%temp1) (1)
    #SET(%Temp1,%Temp1+1)
  #ENDFOR
  #SET(%rFiles,ITEMS(%wsFiles))
  #SET(%Temp1,1)
%%MenuItemProcTitle DEPEND %%MenuItems DEFAULT TIMES 1
WHEN  (2)TIMES %rFiles
  #FOR(%wsFiles)
WHEN  (%temp1) ('''Browse %wsFiles''')
    #SET(%Temp1,%Temp1+1)
  #ENDFOR
#ENDIF
#IF(%wsLayout=2)
  #SET(%rFiles,ITEMS(%wsFiles))
  #SET(%Temp1,1)
%%MenuItemProcAjax DEPEND %%MenuItems DEFAULT TIMES 1
WHEN  (2)TIMES %rFiles
  #FOR(%wsFiles)
WHEN  (%temp1) (1)
    #SET(%Temp1,%Temp1+1)
  #ENDFOR
#ENDIF
#!--
%%MenuTableA LONG  (0)
%%MenuId DEFAULT  ('''menu''')
#!----------------------------------------------------
#GROUP(%FooterTxa)
[PROCEDURE]
NAME PageFooterTag
PROTOTYPE '(NetWebServerWorker p_web)'
GLOBAL
[COMMON]
FROM NetTalk NetWebSource
[PROMPTS]
%%GenerateDiv LONG  (1)
%%htmls MULTI LONG  (1,2)
%%HtmlWhat DEPEND %%htmls DEFAULT TIMES 2
WHEN  (1) ('1')
WHEN  (2) ('1')

%%HtmlProc DEPEND %%htmls PROCEDURE TIMES 0

%%HtmlParams DEPEND %%htmls DEFAULT TIMES 2
WHEN  (1) ('p_web')
WHEN  (2) ('p_web')

%%HtmlReturn DEPEND %%htmls FIELD TIMES 0

%%htmlCondition DEPEND %%htmls DEFAULT TIMES 2
WHEN  (1) ('p_web.GetSessionLoggedIn() ')
WHEN  (2) ('p_web.GetSessionLoggedIn() = 0')

%%htmlLocation DEPEND %%htmls DEFAULT TIMES 2
WHEN  (1) ('201')
WHEN  (2) ('201')

%%HtmlName DEPEND %%htmls DEFAULT TIMES 2
WHEN  (1) ('LoggedIn')
WHEN  (2) ('NotLoggedIn')

#SET(%temp1,QUOTE(%wsCopyright))
#IF(%wsSessionTimer)
  #SET(%temp2,'<<div class="nt-right nt-countdown">Session Expires In:<<div id="countdown"><</div><</div>')
#ELSE
  #SET(%temp2,'')
#ENDIF
%%Html DEPEND %%htmls DEFAULT TIMES 2
WHEN  (1) ('<<div>%temp1<</div>%temp2')
WHEN  (2) ('<<div>%temp1<</div>')

%%DropList3 DEFAULT  ('Before <<body>[101]|After <<body>[201]|In Processed code[301]')
%%CssDiv DEFAULT  ('''nt-left nt-width-100 nt-site-footer''')
%%MobileCssDiv DEFAULT  ('''''')
%%IsFooter LONG  (1)
[EMBED]
EMBED %%ProcessedCode
[DEFINITION]
[SOURCE]
PROPERTY:BEGIN
PRIORITY 2500
PROPERTY:END
  if (p_web.GetSessionLoggedIn() and p_web.PageName <> p_web.site.LoginPage)
    ! parameter 1 is the session time
    ! parameter 2 is the name of the login page.
    ! parameter 3 is the id of the <div> in the html.
    p_web.Script('startCountDown('& int(p_web.site.SessionExpiryAfterHS/100) &',"'& clip(p_web.site.LoginPage) &'","countdown");')
  end

[END]
[END]
#!----------------------------------------------------
#GROUP(%LoginFormTxa)
[PROCEDURE]
NAME LoginForm
PROTOTYPE '(NetWebServerWorker p_web,long p_action=0),long,proc'
[COMMON]
FROM NetTalk NetWebForm
[DATA]
Ans                      LONG
Loc:Login                STRING(255)
Loc:Password             STRING(255)
Loc:Remember             LONG
loc:Hash                 LONG
[PROMPTS]
%%FormStyle DEFAULT  ('Net:Web:Rounded')
%%FormSource DEFAULT  ('Memory')
%%FormAction DEFAULT  ('''IndexPage''')
%%Tabs MULTI LONG  (1)
%%TabHeading DEPEND %%Tabs DEFAULT TIMES 1
WHEN  (1) ('''Login''')

%%FormFields DEPEND %%Tabs MULTI LONG TIMES 1
WHEN  (1) (1,2,3,4)

%%FormField DEPEND %%FormFields FIELD TIMES 1
WHEN  (1)TIMES 4
WHEN  (1) (Loc:Login)
WHEN  (2) (Loc:Password)
WHEN  (3) (Loc:Remember)
WHEN  (4) (loc:Hash)

%%FormFieldPrompt DEPEND %%FormFields DEFAULT TIMES 1
WHEN  (1)TIMES 3
WHEN  (1) ('''Login:''')
WHEN  (2) ('''Password:''')
WHEN  (3) ('''Remember me''')

%%FormFieldType DEPEND %%FormFields DEFAULT TIMES 1
WHEN  (1)TIMES 4
WHEN  (1) ('String')
WHEN  (2) ('String')
WHEN  (3) ('CheckBox')
WHEN  (4) ('Hidden')

%%FormFieldPasswordType DEPEND %%FormFields LONG TIMES 1
WHEN  (1)TIMES 1
WHEN  (2) (1)

%%FormFieldWidth DEPEND %%FormFields DEFAULT TIMES 1
WHEN  (1)TIMES 2
WHEN  (1) ('20')
WHEN  (2) ('20')

%%Primes MULTI LONG  (1)
%%Prime DEPEND %%Primes DEFAULT TIMES 1
WHEN  (1) ('loc:Hash')

%%PrimeTo DEPEND %%Primes DEFAULT TIMES 1
WHEN  (1) ('random(1,2000000000)')

%%PrimeForInsert DEPEND %%Primes LONG TIMES 1
WHEN  (1) (1)

%%PrimeForChange DEPEND %%Primes LONG TIMES 1
WHEN  (1) (1)

%%FieldList UNIQUE DEFAULT  ('Loc:Login','Loc:Password','Loc:Remember','loc:Hash')
%%FormFieldEquate DEPEND %%FormFields DEFAULT TIMES 1
WHEN  (1)TIMES 4
WHEN  (1) ('Loc:Login')
WHEN  (2) ('Loc:Password')
WHEN  (3) ('Loc:Remember')
WHEN  (4) ('loc:Hash')

%%IncludeCancelButton LONG  (0)
%%IncludeSaveButton LONG  (1)
%%panelheight DEFAULT  ('350')
%%csLineAfterfHeading LONG  (1)
%%csLineAfterfSubHeading LONG  (1)
%%FormTableWidth DEFAULT  ('''300''')
%%ProcedureStyle DEFAULT  (''' nt-fix-center nt-width-300px''')
%%AllowAutoComplete DEPEND %%FormFields LONG TIMES 1
WHEN  (1)TIMES 4
WHEN  (1) (0)
WHEN  (2) (0)
WHEN  (3) (0)
WHEN  (4) (0)

%%vImm DEPEND %%FormFields DEFAULT TIMES 1
WHEN  (1)TIMES 4
WHEN  (1) ('1')
WHEN  (2) ('0')
WHEN  (3) ('1')
WHEN  (4) ('0')

%%GenerateComments LONG  (1)
%%vfImm DEFAULT  ('2')
%%PopupInvalid LONG  (1)
%%AllowUpdates LONG  (1)

%%FormWindowWidth DEFAULT  ('600')
%%FormButtonsPosition DEFAULT  ('2')

[EMBED]
EMBED %%ValidateUpdate
[DEFINITION]
[SOURCE]
PROPERTY:BEGIN
PRIORITY 5000
LABEL
PROPERTY:END
  ! The HASH test prevents the same form-post from being used "twice".
  ! This improves security on public computers.

  if p_web.GetValue('loc:hash') = p_web.GetSessionValue('loc:hash')

    ! login checking goes here. In this example a simple 'Demo / Demo" login will work. Your app will
    ! probably need a stronger test than that.
    if upper(Loc:Login) = 'DEMO' and upper(Loc:Password) = 'DEMO'
      p_web.ValidateLogin()                   ! this sets the session to "logged in"
      p_web.SetSessionValue('loc:hash',0)         ! clear the hash, so this login can't get "replayed".

      ! set the session level, and any other session variables based on the logged in user.
      p_web.SetSessionLevel(1)

      ! this next bit shows how the login & password can be stored in the browser
      ! so that it is "remembered" for next time
      if loc:remember = 1
        p_web.SetCookie('loc__login',loc:login,today()+30)       ! note the expiry date. It's good
        p_web.SetCookie('loc__password',loc:password,today()+30) ! form to make sure your cookies expire sometime.
      else
        ! don't remember, so clear cookies in browser.
        p_web.DeleteCookie('loc__login')
        p_web.DeleteCookie('loc__password')
      End
    Else
      loc:invalid = 'Loc:Login'
      p_web.SetValue('retry',p_web.site.LoginPage)
      loc:Alert = 'Login Failed. Try Again.'
      p_web.DeleteCookie('loc__login')
      p_web.DeleteCookie('loc__password')
    End
  Else
    p_web.DeleteCookie('loc__login')
    p_web.DeleteCookie('loc__password')
  End
  ! delete the session values, so if the user comes back to the form, the items are not set.
  p_web.deletesessionvalue('loc:login')
  p_web.deletesessionvalue('loc:password')
  p_web.deletesessionvalue('loc:remember')
  p_web.deletevalue('loc:login')
  p_web.deletevalue('loc:password')
  p_web.deletevalue('loc:remember')

[END]
EMBED %%GenerateForm
[DEFINITION]
[SOURCE]
PROPERTY:BEGIN
PRIORITY 5000
PROPERTY:END
! change the 'save' button so it says 'login'
p_web.site.SaveButton.TextValue = 'Login'

! coming to the login screen automatically logs the user out.
! Note that logging out does not delete the session.
p_web.SetSessionLoggedIn(0)
[END]
[END]
#!----------------------------------------------------
#GROUP(%IndexPageTxa)
[PROCEDURE]
NAME IndexPage
PROTOTYPE '(NetWebServerWorker p_web)'
[COMMON]
FROM NetTalk NetWebPage
[PROMPTS]
#SET(%temp1,QUOTE(%wsIndexPageName))
%%webPageName DEFAULT  ('%temp1')
#SET(%temp1,QUOTE(%wsSiteTitle))
%%PageTitle DEFAULT  ('''%temp1''')
%%htmls MULTI LONG  (1)
%%HtmlName DEPEND %%htmls DEFAULT TIMES 1
WHEN  (1) ('body')

%%Html DEPEND %%htmls DEFAULT TIMES 1
WHEN  (1) ('<<!-- Net:PageHeaderTag --><13,10><<br /><13,10>Welcome<<br /><<br /><<13,10><<!-- Net:PageFooterTag --><13,10>')

%%HtmlWhat DEPEND %%htmls DEFAULT TIMES 1
WHEN  (1) ('1')

%%htmlLocation DEPEND %%htmls DEFAULT TIMES 1
WHEN  (1) ('201')

%%protocol DEFAULT  ('HTML')
%%ContentType DEFAULT  ('''text/html''')
%%PageBodyClass DEFAULT  ('''nt-body''')
%%PageBodyDivClass DEFAULT  ('''nt-body-div''')
%%ProcedureParameters DEFAULT  ('(NetWebServerWorker p_web)')
#!----------------------------------------------------
#GROUP(%MessageBoxProcsTxa)
[PROCEDURE]
NAME ds_Stop
PROTOTYPE '(<<string StopText>)'
GLOBAL
[COMMON]
FROM ABC Source
[PROMPTS]
%%Parameters DEFAULT  ('(<<string StopText>)')
[ADDITION]
NAME CSMesBox CSMesBox_ActivateStop
[INSTANCE]
INSTANCE 1
#!- - - - - - - - - - - - - - - - - - - - - - - - - - -
[PROCEDURE]
NAME ds_Halt
PROTOTYPE '(UNSIGNED Level=0,<<STRING HaltText>)'
GLOBAL
[COMMON]
FROM ABC Source
[PROMPTS]
%%Parameters DEFAULT  ('(UNSIGNED Level=0,<<STRING HaltText>)')
[ADDITION]
NAME CSMesBox CSMesBox_ActivateHalt
[INSTANCE]
INSTANCE 1
#!- - - - - - - - - - - - - - - - - - - - - - - - - - -
[PROCEDURE]
NAME ds_Message
PROTOTYPE '(STRING MessageTxt,<<STRING HeadingTxt>,<<STRING IconSent>,<<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE),UNSIGNED,PROC'
GLOBAL
[COMMON]
FROM ABC Window
[DATA]
[SCREENCONTROLS]
! PROMPT('FilesOpened:'),USE(?FilesOpened:Prompt)
! ENTRY(@n3),USE(FilesOpened)
[REPORTCONTROLS]
! STRING(@n3),USE(FilesOpened)
FilesOpened              BYTE
!!> IDENT(4294967295),PROMPT('FilesOpened:'),HEADER('FilesOpened'),PICTURE(@n3)
[SCREENCONTROLS]
! PROMPT('Loc : Button Pressed:'),USE(?Loc:ButtonPressed:Prompt)
! ENTRY(@n6),USE(Loc:ButtonPressed)
[REPORTCONTROLS]
! STRING(@n6),USE(Loc:ButtonPressed)
Loc:ButtonPressed        UNSIGNED
!!> IDENT(4294967294),PROMPT('Loc : Button Pressed:'),HEADER('Loc : Button Pressed'),PICTURE(@n6)
[SCREENCONTROLS]
! PROMPT('Email Link:'),USE(?EmailLink:Prompt)
! TEXT,USE(EmailLink)
[REPORTCONTROLS]
! TEXT,USE(EmailLink)
EmailLink                CSTRING(4096)
!!> IDENT(4294967293),PROMPT('Email Link:'),HEADER('Email Link'),PICTURE(@s255)
[PROMPTS]
%%Parameters DEFAULT  ('(STRING MessageTxt,<<STRING HeadingTxt>,<<STRING IconSent>,<<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE)')
%%ReturnValue FIELD  (Loc:ButtonPressed)
%%INISaveWindow LONG  (1)
[ADDITION]
NAME CSMesBox CSMesBox_MessageBoxControl
[INSTANCE]
INSTANCE 1
[PROMPTS]
[WINDOW]
window WINDOW('Caption'),AT(,,404,108),FONT('MS Sans Serif',8,,FONT:regular),GRAY
       IMAGE,AT(11,18),USE(?Image1),HIDE,#SEQ(1),#ORIG(?Image1)
       PROMPT(''''),AT(118,32),USE(?MainTextPrompt),#SEQ(1),#ORIG(?MainTextPrompt)
       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE,#SEQ(1),#ORIG(?HALink)
       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE,#SEQ(1),#ORIG(?TimerCounter)
       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE,#SEQ(1),#ORIG(DontShowThisAgain)
       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE,#SEQ(1),#ORIG(?Button1)
       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE,#SEQ(1),#ORIG(?Button2)
       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE,#SEQ(1),#ORIG(?Button3)
       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE,#SEQ(1),#ORIG(?Button4)
       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE,#SEQ(1),#ORIG(?Button5)
       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE,#SEQ(1),#ORIG(?Button6)
       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE,#SEQ(1),#ORIG(?Button7)
       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE,#SEQ(1),#ORIG(?Button8)
     END
#!----------------------------------------------------
#GROUP(%BrowseTxa,%wsFile)
  #FIX(%File,%wsFile)
  #FREE(%BrowseFields)
  #FOR(%Field),Where(%FieldID)
    #IF(EXTRACT(%FieldQuickOptions,'NOPOPULATE'))
      #CYCLE
    #ENDIF
    #! Ignore Array fields for now.
    #IF(%FieldDimension1 <>0)
      #CYCLE
    #ENDIF
    #ADD(%BrowseFields,%Field)
    #SET(%BrowseFieldButton,0)
    #! look for boolean values
    #IF(%FieldValidation='BOOLEAN')
      #SET(%BrowseFieldCheck,1)
    #ELSE
      #SET(%BrowseFieldCheck,0)
    #ENDIF
    #! look for fields which are components of foreign fields
    #insert(%GetForeignDetails,%field,%BrowseChild,%BrowseValueFieldInChild,%BrowseDescFieldInChild)
    #IF(%BrowseDescFieldInChild)
      #SET(%BrowseFieldCondition,''' loc:parent <> ''''update'& lower(%BrowseChild)&'''''''')
    #ELSE
      #SET(%BrowseFieldCondition,'')
    #ENDIF
  #ENDFOR
  #IF(%wsInRowCopy)
    #ADD(%BrowseFields,'Copy')
    #SET(%BrowseFieldButton,1)
  #ENDIF
  #IF(%wsInRowChange)
    #ADD(%BrowseFields,'Change')
    #SET(%BrowseFieldButton,1)
  #ENDIF
  #IF(%wsInRowDelete)
    #ADD(%BrowseFields,'Delete')
    #SET(%BrowseFieldButton,1)
  #ENDIF
  #IF(%wsInRowView)
    #ADD(%BrowseFields,'View')
    #SET(%BrowseFieldButton,1)
  #ENDIF
  #IF(%wsInRowSelect)
    #ADD(%BrowseFields,'Select')
    #SET(%BrowseFieldButton,1)
  #ENDIF
  #IF(%wsIncBrowseID=0)
    #FOR(%key),Where(%KeyPrimary=1)
      #FOR(%KeyField)
        #Fix(%BrowseFields,%KeyField)
        #IF(%BrowseFields)
          #DELETE(%BrowseFields,INSTANCE(%BrowseFields))
        #ENDIF
      #EndFor
    #EndFor
  #EndIf
[PROCEDURE]
  #IF(varexists(%xgenbrowsename) and %xgenbrowsename <> '')
NAME %xgenbrowsename
    #SET(%xgenbrowsename,'')
  #ELSE
NAME Browse%wsFile
  #ENDIF
PROTOTYPE '(NetWebServerWorker p_web)'
GLOBAL
[COMMON]
FROM NetTalk NetWebBrowse
[FILES]
[PRIMARY]
%wsFile
[INSTANCE]
0
[SECONDARY]
  #FOR(%BrowseFields),Where(%BrowseChild<>'')
%BrowseChild %wsFile
  #ENDFOR
[PROMPTS]
  #CALL(%splitfilename,%wsFile),%temp1
%%nHeading DEFAULT  ('''%temp1''')
%%nEmptyMessage DEFAULT  ('''no %temp1''')
  #IF(%genForms or (VarExists(%linkToForm) and %linkToForm = 1))
    #IF(VarExists(%xgenFormName) and %xgenFormName <> '')
%%nFormControl PROCEDURE  (%xgenFormName)
    #ELSE
%%nFormControl PROCEDURE  (Update%wsFile)
    #ENDIF
%%nInsert LONG  (1)
%%nCancel LONG  (1)
    #IF(%wsInRowSelect)
%%nSelect LONG  (0)
    #ELSE
%%nSelect LONG  (1)
    #ENDIF
    #IF(%wsInRowChange)
%%nChange LONG  (0)
    #ELSE
%%nChange LONG  (1)
    #ENDIF
    #IF(%wsInRowDelete)
%%nDelete LONG  (0)
    #ELSE
%%nDelete LONG  (1)
    #ENDIF
    #IF(%wsInRowCopy)
%%nCopy LONG  (0)
    #ELSE
%%nCopy LONG  (1)
    #ENDIF
    #IF(not varexists(%wsLayout))
      #Declare(%wsLayout)
      #SET(%wsLayout,1)
    #EndIf
    #IF(%wsLayout=1)
%%nFormPopup LONG  (0)
    #ELSE
%%nFormPopup LONG  (1)
    #ENDIF
  #ENDIF
#!
#CALL(%MakeList,1,ITEMS(%BrowseFields)),%temp1
%%nFields MULTI LONG (%temp1)
#SET(%temp1,ITEMS(%BrowseFields))

%%nField DEPEND %%nFields FIELD TIMES %temp1
  #FOR(%BrowseFields)
    #SET(%temp2,INSTANCE(%BrowseFields))
    #IF(%BrowseFieldButton)
WHEN  (%temp2) ()
    #ELSIF(%BrowseDescFieldInChild)
WHEN  (%temp2) (%BrowseDescFieldInChild)
    #ELSE
WHEN  (%temp2) (%BrowseFields)
    #ENDIF
  #ENDFOR

%%BrowseFieldType DEPEND %%nFields DEFAULT TIMES %temp1
  #FOR(%BrowseFields)
    #SET(%temp2,INSTANCE(%BrowseFields))
    #IF(%BrowseFieldButton)
WHEN  (%temp2) ('Button')
    #ELSE
WHEN  (%temp2) ('String')
    #ENDIF
  #ENDFOR

%%BrowseButtonAction DEPEND %%nFields DEFAULT TIMES %temp1
  #FOR(%BrowseFields)
    #SET(%temp2,INSTANCE(%BrowseFields))
    #IF(%BrowseFieldButton)
WHEN  (%temp2) ('%BrowseFields')
    #ELSE
WHEN  (%temp2) ('Change')
    #ENDIF
  #ENDFOR

%%nFieldEquate DEPEND %%nFields DEFAULT TIMES %temp1
  #FOR(%BrowseFields)
    #SET(%temp2,INSTANCE(%BrowseFields))
WHEN  (%temp2) ('%BrowseFields')
  #ENDFOR

#! conditional columns when browse is on a form
  #SET(%temp1,0)
  #FOR(%BrowseFields),Where(%BrowseFieldCondition)
    #SET(%temp1,%temp1+1)
  #ENDFOR
  #IF(%temp1>0)
%%nFieldCondition DEPEND %%nFields DEFAULT TIMES %temp1
    #FOR(%BrowseFields),Where(%BrowseFieldCondition)
      #SET(%temp2,INSTANCE(%BrowseFields))
WHEN  (%temp2) (%BrowseFieldCondition)
    #ENDFOR
  #ENDIF

#! true/false values turn into EIP checkboxes (with condition false)
  #SET(%temp1,0)
  #FOR(%BrowseFields),Where(%BrowseFieldCheck=1)
    #SET(%temp1,%temp1+1)
  #ENDFOR
  #IF(%temp1>0)
%%eip DEPEND %%nFields LONG TIMES %temp1
    #FOR(%BrowseFields),Where(%BrowseFieldCheck=1)
      #SET(%temp2,INSTANCE(%BrowseFields))
WHEN  (%temp2) (1)
    #ENDFOR
%%FormFieldType DEPEND %%nFields DEFAULT TIMES %temp1
    #FOR(%BrowseFields),Where(%BrowseFieldCheck=1)
      #SET(%temp2,INSTANCE(%BrowseFields))
WHEN  (%temp2) ('CheckBox')
    #ENDFOR
%%FormFieldCondition DEPEND %%nFields DEFAULT TIMES %temp1
    #FOR(%BrowseFields),Where(%BrowseFieldCheck=1)
      #SET(%temp2,INSTANCE(%BrowseFields))
WHEN  (%temp2) ('false')
    #ENDFOR
  #ENDIF
#! set header for child fields
  #SET(%temp1,0)
  #FOR(%BrowseFields),Where(%BrowseDescFieldInChild)
    #SET(%temp1,%temp1+1)
  #ENDFOR
  #IF(%temp1>0)
%%nFieldHeader DEPEND %%nFields DEFAULT TIMES %temp1
    #FOR(%BrowseFields),Where(%BrowseDescFieldInChild)
      #SET(%temp2,INSTANCE(%BrowseFields))
WHEN  (%temp2) ('''%BrowseChild''')
    #EndFor
  #EndIf
#! set default sorting column
  #INSERT(%GetDescriptionField,%wsFile),%temp1
  #FOR(%BrowseFields)
    #IF(%BrowseFields=%temp1)
      #SET(%temp2,INSTANCE(%BrowseFields))
%%nSortDefault DEPEND %%nFields LONG TIMES 1
WHEN  (%temp2) (1)
      #BREAK
    #ENDIF
  #ENDFOR
#! conditional relationships setting browse filter
  #SET(%temp1,0)
  #For(%Relation),Where(%FileRelationType='MANY:1' and %FileKey)
    #SET(%temp1,%temp1+1)
  #EndFor
  #IF(%temp1>0)
    #CALL(%MakeList,1,%temp1),%temp2
%%Filters MULTI LONG  (%temp2)
#!
%%FilterChild DEPEND %%Filters PROCEDURE TIMES %temp1
    #Set(%temp2,1)
    #For(%Relation),Where(%FileRelationType='MANY:1' and %FileKey)
WHEN  (%temp2) (Update%Relation)
      #SET(%temp2,%temp2+1)
    #endFor
#!

%%cRangeLimitField DEPEND %%Filters FIELD TIMES %temp1
    #Set(%temp2,1)
    #For(%Relation),Where(%FileRelationType='MANY:1' and %FileKey)
      #FOR(%FileKeyField)
WHEN  (%temp2) (%FileKeyField)
        #Break
      #EndFor
      #SET(%temp2,%temp2+1)
    #endFor
#!
%%cRangeLimitType DEPEND %%Filters DEFAULT TIMES %temp1
    #Set(%temp2,1)
    #For(%Relation),Where(%FileRelationType='MANY:1' and %FileKey)
WHEN  (%temp2) ('File Relationship')
      #SET(%temp2,%temp2+1)
    #endFor
#!
%%cRangeFile DEPEND %%Filters FILE TIMES %temp1
    #Set(%temp2,1)
    #For(%Relation),Where(%FileRelationType='MANY:1' and %FileKey)
WHEN  (%temp2) (%Relation)
      #SET(%temp2,%temp2+1)
    #endFor
  #EndIf
  #!Locators
%%LocatorPlaceHolder DEFAULT  ('''%wsLocatorPlaceHolder''')
%%LocatorPosition DEFAULT  ('%wsLocatorPosition')
%%LocatorType DEFAULT  ('%wsLocatorType')
%%LocatorBlank LONG  (%wsLocatorBlank)
%%nLocatorMessage DEFAULT  ('''%wsLocatorMessage''')
%%SearchButton LONG  (%wsSearchButton)
%%ClearButton LONG  (%wsClearButton)
%%BrowseLocatorSize DEFAULT  ('%wsBrowseLocatorSize')
%%BrowseLocatorPromptHide LONG  (%wsBrowseLocatorPromptHide)
%%BrowseLocatorImmediate LONG  (%wsBrowseLocatorImmediate)
  #!
#!----------------------------------------------------
#GROUP(%FormTxa,%wsFile),Preserve
  #Fix(%File,%wsFile)
  #SET(%temp1,1)
  #FREE(%FormFields)
  #FREE(%FormTabs)
  #SET(%counter,0)
  #FOR(%Field),Where(%FieldID)
    #IF((EXTRACT(%FieldQuickOptions,'NOPOPULATE')) or (%FieldType='BLOB'))
      #CYCLE
    #ENDIF
    #! Ignore Array fields for now.
    #IF(%FieldDimension1 <>0)
      #CYCLE
    #ENDIF
    #SET(%counter,%counter+1)
    #IF(%counter>150)
      #CYCLE
    #ENDIF
    #Add(%FormFields,%Field)
    #Set(%FormFieldNumber,%temp1)
    #Set(%FormField,%Field)
    #Set(%FormFieldEquate,%Field)
    #set(%FormFieldCallProcedure,'')
    #insert(%GetForeignDetails,%Field,%FormFieldLookupFile,%FormFieldLookupValue,%FormFieldLookupDesc)
    #IF(%FormFieldLookupFile)
      #SET(%FormFieldType,'String')
      #SET(%FormFieldInclude,''' loc:parent <> ''''update'& lower(%FormFieldLookupFile)&'''''''')
    #ELSIF(%FieldValidation='BOOLEAN')
      #SET(%FormFieldType,'CheckBox')
    #ELSIF(ITEMS(%FieldChoices))
      #FOR(%FieldScreenControl)
        #IF(EXTRACT(%FieldScreenControl,'OPTION',1))
          #SET(%FormFieldType,'Radio')
          #BREAK
        #ELSIF(EXTRACT(%FieldScreenControl,'LIST',1))
          #SET(%FormFieldType,'Drop')
          #BREAK
        #ENDIF
      #ENDFOR
      #IF(%FormFieldType='')
        #SET(%FormFieldType,'Drop')
      #ENDIF
    #ELSIF(%FieldType='STRING' or %FieldType='CSTRING' or %FieldType='PSTRING')
      #set(%temp2,EXTRACT(%FieldStruct,%FieldType,1))
      #IF(%temp2>256)
        #SET(%FormFieldType,'Text')
      #ELSE
        #SET(%FormFieldType,'String')
      #ENDIF
    #ELSIF(%FieldType='MEMO'  )
      #SET(%FormFieldType,'Text')
    #ELSIF(upper(sub(%FieldPicture,2,1)) = 'D')
      #SET(%FormFieldType,'Date')
    #ELSE
      #SET(%FormFieldType,'Number')
    #ENDIF
    #Set(%FormTab,EXTRACT(%FieldQuickOptions,'TAB',1))
    #ADD(%FormTabs,%FormTab)
    #SET(%temp1,%Temp1+1)
  #EndFor
  #For(%key),Where(%KeyPrimary)
    #For(%KeyField)
      #IF(%wsIncBrowseID=1)
        #Find(%FormFields,%Keyfield)
        #SET(%FormFieldType,'Display')
      #ELSE
        #Fix(%FormFields,%Keyfield)
        #IF(%FormFields)
          #Delete(%FormFields,INSTANCE(%FormFields))
        #ENDIF
      #EndIf
    #EndFor
  #EndFor
  #For(%Relation),Where(%FileRelationType='1:MANY')
    #FIX(%wsFiles,%Relation)
    #IF(%wsFiles)
      #Add(%FormFields,'Browse' & %Relation)
      #Set(%FormField,'')
      #Set(%FormFieldEquate,'Browse' & %Relation)
      #SET(%FormFieldType,'Browse')
      #Set(%FormFieldNumber,%temp1)
      #SET(%FormFieldCallProcedure,'Browse'&%Relation)
      #CALL(%splitfilename,%Relation),%temp1
      #Set(%FormTab,%temp1)
      #ADD(%FormTabs,%FormTab)
      #SET(%temp1,%Temp1+1)
    #ENDIF
  #EndFor
[PROCEDURE]
  #IF(varexists(%xgenformname) and %xgenformname <> '')
NAME %xgenformname
    #!SET(%xgenformname,'')
  #ELSE
NAME Update%wsFile
  #ENDIF
PROTOTYPE '(NetWebServerWorker p_web,long p_action=0),long,proc'
GLOBAL
[COMMON]
FROM NetTalk NetWebForm
[FILES]
[OTHERS]
%wsFile
  #FOR(%FormFields),Where(%FormFieldLookupFile)
%FormFieldLookupFile
  #ENDFOR
[PROMPTS]
%%FormStyle DEFAULT  ('Net:Web:Default')
%%FormSource DEFAULT  ('File')
%%FormFile FILE  (%wsFile)
  #FIX(%File,%wsFile)
  #FOR(%Key)
    #IF(%KeyPrimary=0)
      #CYCLE
    #ENDIF
%%FormKey KEY  (%Key)
    #BREAK
  #ENDFOR
  #CALL(%splitfilename,%wsFile),%temp1
%%FormHeading DEFAULT  ('''Update %temp1''')
#CALL(%MakeList,1,ITEMS(%FormTabs)),%temp1
%%Tabs MULTI LONG  (%temp1)
#SET(%Temp1,ITEMS(%FormTabs))
%%TabHeading DEPEND %%Tabs DEFAULT TIMES %Temp1
  #SET(%temp1,1)
  #FOR(%FormTabs)
    #IF(%FormTabs='')
WHEN  (%temp1) ('''General''')
    #ELSE
WHEN  (%temp1) ('''%FormTabs''')
    #EndIf
    #Set(%Temp1,%Temp1+1)
  #EndFor
  #For(%FormTabs)
    #SET(%TabsNumber,0)
    #set(%TabsLookups,0)
    #For(%FormFields),Where(%FormTab=%FormTabs)
      #SET(%TabsNumber,%TabsNumber+1)
      #IF(%FormFieldLookupFile)
        #set(%TabsLookups,%TabsLookups+1)
      #ENDIF
    #EndFor
  #EndFor

  #SET(%temp1,ITEMS(%FormTabs))
%%FormFields DEPEND %%Tabs MULTI LONG TIMES %temp1
  #SET(%temp1,1)
  #FOR(%FormTabs)
    #CALL(%MakeList,1,%TabsNumber),%temp2
WHEN  (%Temp1) (%temp2)
    #SET(%temp1,%temp1+1)
  #ENDFOR

  #SET(%temp1,ITEMS(%FormTabs))
%%FormField DEPEND %%FormFields FIELD TIMES %temp1
  #SET(%temp1,1)
  #FOR(%FormTabs)
WHEN  (%temp1)TIMES %TabsNumber
    #SET(%temp2,1)
    #For(%FormFields),Where(%FormTab=%FormTabs)
WHEN  (%temp2) (%FormField)
      #SET(%temp2,%temp2+1)
    #EndFor
    #SET(%temp1,%temp1+1)
  #ENDFOR

  #SET(%temp1,ITEMS(%FormTabs))
%%FormFieldType DEPEND %%FormFields DEFAULT TIMES %temp1
  #SET(%temp1,1)
  #FOR(%FormTabs)
WHEN  (%temp1)TIMES %TabsNumber
    #SET(%temp2,1)
    #For(%FormFields),Where(%FormTab=%FormTabs)
WHEN  (%temp2) ('%FormFieldType')
      #SET(%temp2,%temp2+1)
    #EndFor
    #SET(%temp1,%temp1+1)
  #ENDFOR

  #SET(%temp1,ITEMS(%FormTabs))
%%FormFieldButtonset DEPEND %%FormFields DEFAULT TIMES %temp1
  #SET(%temp1,1)
  #FOR(%FormTabs)
WHEN  (%temp1)TIMES %TabsNumber
    #SET(%temp2,1)
    #For(%FormFields),Where(%FormTab=%FormTabs)
      #IF(%FormFieldType='Radio' or %FormFieldType='CheckBox')
WHEN  (%temp2) (1)
      #ELSE
WHEN  (%temp2) (0)
      #ENDIF
      #SET(%temp2,%temp2+1)
    #EndFor
    #SET(%temp1,%temp1+1)
  #ENDFOR

#! prime fields
  #FIX(%File,%wsFile)
  #SET(%temp1,0)
  #FOR(%Relation),Where(%FileRelationType='MANY:1')
    #FOR(%FileKeyField)
      #SET(%temp1,%temp1+1)
    #EndFor
  #EndFor
  #FOR(%FormFields),Where(%FormFieldType='Date')
    #SET(%temp1,%temp1+1)
  #ENDFOR
  #IF(%Temp1>0)
    #CALL(%MakeList,1,%temp1),%temp2
%%primes MULTI LONG  (%temp2)

%%prime DEPEND %%primes DEFAULT TIMES %temp1
    #SET(%temp2,1)
    #FOR(%Relation),Where(%FileRelationType='MANY:1')
      #FOR(%FileKeyField)
WHEN  (%temp2) ('%FileKeyField')
        #SET(%temp2,%temp2+1)
      #ENDFOR
    #ENDFOR
    #FOR(%FormFields),Where(%FormFieldType='Date')
WHEN  (%temp2) ('%FormFields')
      #SET(%temp2,%temp2+1)
    #ENDFOR

%%primeto DEPEND %%primes DEFAULT TIMES %temp1
    #SET(%temp2,1)
    #FOR(%Relation),Where(%FileRelationType='MANY:1')
      #FOR(%FileKeyField)
WHEN  (%temp2) ('p_web.GSV(''%FileKeyFieldLink'')')
        #SET(%temp2,%temp2+1)
      #ENDFOR
    #ENDFOR
    #FOR(%FormFields),Where(%FormFieldType='Date')
WHEN  (%temp2) ('today()')
      #SET(%temp2,%temp2+1)
    #ENDFOR
  #ENDIF

#Insert(%FormLookupSetting,'FormFieldLookupButton',1,'LONG')
    #IF(not varexists(%wsLayout))
      #Declare(%wsLayout)
      #SET(%wsLayout,1)
    #EndIf
  #IF(%wsLayout=1)
#Insert(%FormLookupSetting,'FormFieldPopupLookup',0,'LONG')
  #ELSE
#Insert(%FormLookupSetting,'FormFieldPopupLookup',1,'LONG')
  #ENDIF

#Insert(%FormLookupSetting,'FormFieldLookupProcedure',2,'PROCEDURE')

#Insert(%FormLookupSetting,'FormFieldLookupTitle',3,'DEFAULT')

#Insert(%FormLookupSetting,'FormFieldLookupFile',4,'FILE')

#Insert(%FormLookupSetting,'FormFieldDropValue',5,'DEFAULT')

#Insert(%FormLookupSetting,'FormFieldLookupDescField',6,'DEFAULT')

#Insert(%FormLookupSetting,'FormFieldLookupSortField',7,'DEFAULT')

#Insert(%FormLookupSetting,'FormFieldDisplayDesc',1,'LONG')

#Insert(%FormLookupSetting,'FormFieldEquate',28,'DEFAULT')

#Insert(%FormLookupSetting,'FormFieldProcedure',29,'PROCEDURE')

#Insert(%FormLookupSetting,'FormFieldCondition',8,'DEFAULT')

#Insert(%FormLookupSetting,'FormFieldAutoComplete',1,'LONG')
#!----------------------------------------------------
#Group(%FormLookupSetting,%xxx,%what,%ty)
  #SET(%temp1,ITEMS(%FormTabs))
  #SUSPEND
#?%%%xxx DEPEND %%FormFields %ty TIMES %temp1
  #SET(%temp1,1)
  #FOR(%FormTabs)
      #IF(%what <= 20)
WHEN  (%temp1)TIMES %TabsLookups
      #ELSE
WHEN  (%temp1)TIMES %TabsNumber
      #ENDIF
      #SET(%temp2,1)
      #For(%FormFields),Where(%FormTab=%FormTabs)
        #IF(%FormFieldLookupFile)
          #CASE(%what)
          #OF('1')
WHEN  (%temp2) (1)
          #OF(2)
WHEN  (%temp2) (Browse%FormFieldLookupFile)
          #OF(3)
WHEN  (%temp2) ('''Select %FormFieldLookupFile''')
          #OF(4)
WHEN  (%temp2) (%FormFieldLookupFile)
          #OF(5)
WHEN  (%temp2) ('%FormFieldLookupValue')
          #OF(6)
WHEN  (%temp2) ('%FormFieldLookupDesc')
          #OF(7)
WHEN  (%temp2) (%FormFieldLookupDesc)
          #OF(8)
WHEN  (%temp2) (%FormFieldInclude)
          #ENDCASE
        #ENDIF
          #CASE(%what)
          #OF(28)
WHEN  (%temp2) ('%FormFieldEquate')
          #OF(29)
WHEN  (%temp2) (%FormFieldCallProcedure)
          #ENDCASE
        #SET(%temp2,%temp2+1)
      #EndFor
    #SET(%temp1,%temp1+1)
  #ENDFOR
  #RESUME
#!----------------------------------------------------
#UTILITY(ServerBrowseWizard,'Create a new NetTalk Browse (and Form?)'),WIZARD(NetWebBrowse(NetTalk))
  #SHEET
    #Insert(%JustBrowseOptionsTab)
    #Insert(%BrowseOptionsTab)
    #Insert(%FormOptionsTab)
    #Insert(%DoneTab)
  #ENDSHEET
#INSERT(%DeclareUtilityVariables)
#DECLARE(%swGlobal)
#INSERT(%BrowsesGroup)
#INSERT(%FormsGroup)
#!----------------------------------------------------
#UTILITY(ServerFormWizard,'Create a new NetTalk Form'),WIZARD(NetWebForm(NetTalk))
  #SHEET
    #PREPARE
      #DECLARE(%GenForms)
      #SET(%genForms,1)
    #ENDPREPARE
    #Insert(%JustFormOptionsTab)
    #Insert(%FormOptionsTab)
    #Insert(%DoneTab)
  #ENDSHEET
#DECLARE(%swGlobal)
#INSERT(%DeclareUtilityVariables)
#INSERT(%FormsGroup)
#!----------------------------------------------------


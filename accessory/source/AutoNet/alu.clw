! translatable messages ?

! ------------------------------------------------------------------
! AutoNET directory syncronisation program
! (c) 1997,2004 by Custom Business Software cc - All rights Reserved
! Takes 3 or 4 command line parameters, in specific order
! 1 - Source directory path
! 2 - Destination directory path
! 3 - AutoNET.log path and file name
! 4 - Path and name of program to run after completion
! 5 - Parameter to pass to program (optional)
!
! 3.04 new - if only _1_ command line, then that's the location of the autonet.log file.
!
! Thanks to Greg Miller for the touch function code...
! ------------------------------------------------------------------
! ------------------------------------------------------------------
! Version Info:
!
! 3.07 2004 08 24
! 3.08 2004 11 05 ! changed to not use upper()
! 3.09 2005 01 27 ! added UName to queue for uppercase comparison..
! 3.10 2009 12 09
! ------------------------------------------------------------------
       program
                                                 
HFile    Equate(Short)
OF_READ  Equate(0)
OF_WRITE Equate(1)

REGS   GROUP,TYPE
ax       USHORT
bx       USHORT
cx       USHORT
dx       USHORT
si       USHORT
di       USHORT
cflag    USHORT
flags    USHORT
       END

       map
        !Touch (STRING psFileSrc, STRING psFileDest),Long,Proc
        !module('windows')
        !  _lopen  (*CString,Short),HFile,Raw,Pascal,name('_lopen')
        !  _lclose (HFile),Short,Raw,Pascal,name('_lclose'),proc
        !  int86  (short intnum, *regs, *regs),short,raw,name('_int86')
        !.
       .

netqueue        QUEUE,PRE(nq)
longname                STRING(FILE:MAXFILENAME)        !FILE:MAXFILENAME is an EQUATE
shortname               STRING(13)
date                    LONG
time                    LONG
size                    LONG
attrib                  BYTE
UName                   STRING(FILE:MAXFILENAME)        ! LJD store uppercase name here for comparison.
                 END
localqueue        QUEUE,PRE(lq)
longname                STRING(FILE:MAXFILENAME)        !FILE:MAXFILENAME is an EQUATE
shortname               STRING(13)
date                    LONG
time                    LONG
size                    LONG
attrib                  BYTE
UName                   STRING(FILE:MAXFILENAME)        ! LJD store uppercase name here for comparison.
                 END
x                long
winopened        byte
Info             string(40)
mess1            string(40)
mess2            string(40)
mess3            string(40)
mess4            string(40)
dispmess3        string(40)
TranslationFile  string(252)
WindowTitle      string(252)
err              string(255)
progress         long
win  WINDOW('Automatic Program Upgrading'),AT(,,204,52),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:ANSI), |
         CENTER,GRAY,DOUBLE
       STRING('3.09'),AT(193,0),USE(?String5)
       STRING(@s40),AT(17,7,169,10),USE(mess1)
       STRING(@s40),AT(17,17,169,10),USE(mess2)
       STRING(@s40),AT(17,27,169,10),USE(dispmess3)
       PROGRESS,USE(Progress),HIDE,AT(17,36,169,10),RANGE(0,500)
       STRING(@s40),AT(17,36,169,10),USE(Info)
     END

!copyfilename    string(255)
!copyfile  file,driver('DOS'),create,name(copyfilename)
!            record
!a             byte
!          . .
netpath  string(255)
localpath string(255)
wait      long
delaytime long
backtoprog string(255)
backtocmd  string(255)
targetos   byte
SettingsFile          string(255)
i         long
  code

  if command(3) <> ''
    SettingsFile = command(3)
  else
    if command(1) <> '' and command(2) = ''
      SettingsFile = command(1)
      if sub(SettingsFile,len(clip(SettingsFile)),1) <> '\' then SettingsFile = clip(SettingsFile) & '\'.
    end
    SettingsFile = clip(SettingsFile) & 'Autonet.log'
  end
  putini('Autonet Version','Version','3.10',SettingsFile)

  if command(1) = '' or command(2) = ''
    netpath = getini('Request','NetPath','',SettingsFile)
  else
    netpath = ShortPath(command(1))
  end
  if command(2) = ''
    localpath = getini('Request','LocalPath','',SettingsFile)
  else
    localpath = ShortPath(command(2))
  end

  if command(4) = ''
    backtoprog = getini('Request','Program','',SettingsFile)
  else
    backtoprog = ShortPath(command(4))
  end

  backtocmd = ''
  loop i = 5 to 10    ! allow 6 cmd parameters...
    if command(i) = '' and i = 5
      backtocmd = getini('Request','Parameters','',SettingsFile)
    elsif command(i) = '' and i > 5
      break
    else
      backtocmd = clip(backtocmd) & ' ' & ShortPath(command(i))
    end
  end
!  if command(5) = ''
!    backtocmd = getini('Request','Parameters','',SettingsFile)
!  else
!    backtocmd = ShortPath(command(5))
!  end

!  if instring('DEBUGFMALL',upper(backtocmd),1,1) then writedebug().

  delaytime = getini ('Request','DelayTime',500,SettingsFile)
  putini ('Request','DelayTime',DelayTime,SettingsFile)

  targetOS = getini ('Request','TargetOS',32,SettingsFile)
  putini ('Request','TargetOS',TargetOS,SettingsFile)

  putini ('Error','DBG1','AutoNET initiated...',SettingsFile)

  if TranslationFile = ''
    TranslationFile = '.\fm2.ini'
  end  
  mess1 = getini('AutoNet','M1','Upgrading program files from the network...',TranslationFile)
  mess2 = getini('AutoNet','M2','Please be patient....',TranslationFile)
  mess3 = getini('AutoNet','M3','Initializing :',TranslationFile)
  mess4 = getini('AutoNet','M4','Copying :',TranslationFile)
  WindowTitle = getini('AutoNet','WindowTitle','Automatic Program Upgrading',TranslationFile)
  putini('AutoNet','M1',mess1,TranslationFile)
  putini('AutoNet','M2',mess2,TranslationFile)
  putini('AutoNet','M3',mess3,TranslationFile)
  putini('AutoNet','M4',mess4,TranslationFile)
  putini('AutoNet','WindowTitle',WindowTitle,TranslationFile)
  
  wait = clock()

  if netpath[len(clip(netpath))] <> '\' then netpath = clip(netpath) & '\'.
  if localpath[len(clip(localpath))] <> '\' then localpath = clip(localpath) & '\'.
  if netpath = '' or localpath = '' or netpath = localpath then return.

  putini ('Error','DBG2','About to get directory listings...',SettingsFile)

  winopened = 0
  directory(netqueue,clip(netpath) & '*.*',0ffh-18h)     ! all files, no directories
  directory(localqueue,clip(localpath) & '*.*',0ffh-18h) ! all files, no directories

  putini ('Error','DBG3','Got directory listings, about to loop...',SettingsFile)

  sort(netqueue,nq:longname)
  sort(localqueue,lq:longname)
  loop x = 1 to records(localqueue)
    get(localqueue,x)
!    lq:longname = upper(lq:longname)       ! 2004 11 05
!    putini('Autonet Local Directory',x,lq:longname & ' ' & lq:size & ' ' & format(lq:date,@d6) & ' ' & format(lq:time,@t4),'Autonet.log')
    LQ:UName = upper(LQ:LongName)
    put(localqueue)
  end
  loop x = 1 to records(netqueue)
    get(netqueue,x)
    NQ:UName = upper(NQ:LongName)
    put(netqueue)
  end
  putini ('Error','DBG4','Loop 1 complete...',SettingsFile)
  loop x = 1 to records(netqueue)
    get(netqueue,x)
 !   nq:longname = upper(nq:longname)       ! 2004 11 05
    if instring('AUTONET.EXE',upper(nq:longname),1,1) then cycle.
!    putini('Autonet Net Directory',x,nq:longname & ' ' & nq:size & ' ' & format(nq:date,@d6) & ' ' & format(nq:time,@t4),'Autonet.log')
!    lq:longname = upper(nq:longname)       ! 2004 11 05
!    lq:longname = nq:longname
!    get(localqueue,lq:longname)
    LQ:UName = NQ:UName
    get(localqueue,LQ:UName)
    if targetos = 16 then lq:time = nq:time.   ! times are unreliable if prog is 16 bit ...
    if errorcode() or lq:size <> nq:size or lq:date <> nq:date or abs(lq:time-nq:time) > 1000 ! allow for 10 second flexibility in the times
      if ~winopened
        open(win)
        ?Progress{prop:rangehigh} = delaytime
        0{prop:text} = WindowTitle
        display()
        winopened = 1
        unhide(?progress)
        dispmess3 = mess3
        loop
          yield
          if wait > clock() then wait = clock().
          if clock() - wait > delaytime then break.
          progress = clock()-wait
          display()
        .
        hide(?progress)
        dispmess3 = mess4
      .
      info = nq:longname
      display()
      !copyfilename = shortpath(clip(netpath) & nq:longname)
      !copy(copyfile,shortpath(clip(localpath) & clip(nq:longname)))
      copy(shortpath(clip(netpath) & nq:longname),shortpath(clip(localpath) & clip(nq:longname)))

      if errorcode()
        err = 'Error copying file : ' & clip(netpath) & clip(nq:longname) & ' to ' &|
        clip(localpath) & clip(nq:longname) & ' Error : ' & error() & |
        ' Local Attribute : ' & lq:attrib & ' Network Attribute : ' & nq:attrib
        putini ('Error','Copying',err,SettingsFile)
        stop(err)
      else
        !Touch(clip(netpath) & clip(nq:longname),clip(localpath) & clip(nq:longname))
      .
    .
  .
  putini ('Error','DBG5','Loop 2 complete...',SettingsFile)

  if winopened
    close(win)      
    putini ('Error','DBG6','About to call RUN...',SettingsFile)
    run(clip(shortpath(backtoprog)) & ' ' & clip(backtocmd))
    if runcode() or errorcode()
      err = 'Unable to restart the original program ( ' & clip(backtoprog) & ' ' & clip(backtocmd) & ' ). The reason was : ' & clip(runcode()) & '; ' & clip(errorcode()) & '; ' & clip(error()) & '. If you restart the program yourself it may work.'
      putini ('Error','Restarting',err,SettingsFile)
      stop(err)
    end
    if runcode() = -4
      err = 'Unable to restart the original program ( ' & clip(backtoprog) & ' ' & clip(backtocmd) & ' ). The reason was : ' & clip(error()) & '. If you restart the program yourself it may work.'
      putini ('Error','Restarting',err,SettingsFile)
      stop(err)
    .
    putini ('Error','DBG7','After RUN...',SettingsFile)
  else
    err='FM2/3 DLL and AutoNet.Exe are out of Sync. Try running your program again. If you get this message again then consult your application supplier.'
    putini ('Error','Starting',err,SettingsFile)
    stop(err)
  .

 omit('***')
!===============================================================
Touch FUNCTION(STRING psFileSrc, STRING psFileDest)

inregs    LIKE(REGS)  ! for int86 call
outregs   LIKE(REGS)  ! for int86 call

hFileSrc  HFILE
hFileDest HFILE

sFileSrc  CSTRING(255)
sFileDest CSTRING(255)

lRetCode  LONG

  CODE
  sFileSrc  = psFileSrc
  sFileDest = psFileDest
  LOOP
    lRetCode = TRUE

    !open the files
    hFileSrc  = _lopen(sFileSrc, OF_READ)
    IF hFileSrc = -1
      lRetCode = FALSE
      BREAK
    END
    hFileDest = _lopen(sFileDest, OF_WRITE)
    IF hFileDest = -1
      lRetCode = FALSE
      BREAK
    END
    ! get the source file date/time
    ! setup int 21H function 5700H  Get file date and time
    inregs.ax = 5700H
    inregs.bx = hFileSrc
    ix# = int86(21H, inregs, outregs)
    IF outregs.cflag <> 0    ! carry flag set on error
      lRetCode = FALSE
      BREAK
    END
    ! copy file attributes to destination file
    ! int 21h function 5701H Set file date and time
    CLEAR(inregs)
    inregs.ax = 5701H
    inregs.bx = hFileDest
    inregs.cx = outregs.cx   ! cx = time in ms-dos format
    inregs.dx = outregs.dx   ! dx = date in ms-dos format
    ix# = int86(21H, inregs, outregs)
    IF outregs.cflag <> 0    ! carry flag set on error
      lRetCode = FALSE
      BREAK
    END
    BREAK
  END

  ! close file handles
  IF hFileSrc <> -1
    _lclose(hFileSrc)
  END
  IF hFileDest <> -1
    _lclose(hFileDest)
  END
  RETURN(lRetCode)
  ***
!===============================================================

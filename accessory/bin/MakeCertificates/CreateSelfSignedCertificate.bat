@rem --- Creates a New Demo Self-Signed Certificate -----
@rem --- Copyright (c) 2006 CapeSoft Software (Pty) Ltd -----

@echo This batch file will create a new Demo Self-Signed Certificate 
@echo.
@echo Press Ctrl-C to abort if you do not want to continue.
@echo.
@pause

@cls

@echo --- Create Private Key (no password)
@echo.
@..\openssl genrsa -out .\YourCARoot\private\Demo.key -rand .\YourCARoot\private\YourRandom.rnd 2048

@rem ---- Not used ----: @echo --- Create Private Key - with Password
@rem ---- Not used ----: @echo (please supply a password when asked to do so)
@rem ---- Not used ----: @echo.
@rem ---- Not used ----: @..\openssl genrsa -out .\YourCARoot\private\Demo.key -rand .\YourCARoot\private\YourRandom.rnd -des3 2048

@echo.
@echo.
@echo --- Create Certificate using Private Key 
@echo (Please enter the same password you used earlier when asked to do so)
@echo.
@..\openssl req -new -x509 -days 3650 -key .\YourCARoot\private\Demo.key -out .\YourCARoot\certs\Demo.crt -config .\YourCARoot\config\OpenSSL.conf

@echo.
@echo.
@echo --- Sign Certificate
@echo.
@..\openssl ca -ss_cert .\YourCARoot\certs\Demo.crt -config .\YourCARoot\config\OpenSSL.conf -policy policy_anything -out .\YourCARoot\certs\SignedDemo.crt


@echo.
@echo.
@echo --- Display Certificate
@echo.
@..\openssl x509 -in .\YourCARoot\certs\SignedDemo.crt -noout -subject -issuer -email -dates


@echo.
@echo.
@pause

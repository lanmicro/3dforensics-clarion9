@rem --- Edit the Random File -----
@rem --- Copyright (c) 2006 CapeSoft Software (Pty) Ltd -----

@echo This batch file will allow you to edit and modify the Random File
@echo.
@echo Press Ctrl-C to abort if you do not want to continue.
@echo.
@pause

@rem --- Edit Random File
edit .\YourCARoot\private\YourRandom.rnd

@pause

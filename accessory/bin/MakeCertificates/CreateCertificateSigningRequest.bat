@rem --- Creates a New Demo Certificate Signing Request -----
@rem --- Copyright (c) 2006 CapeSoft Software (Pty) Ltd -----

@echo This batch file will create a new Demo Certificate Signing Request (called "ForReal")
@echo.
@echo Press Ctrl-C to abort if you do not want to continue.
@echo.
@pause

@cls

@echo --- Create Private Key (no password)
@echo.
@..\openssl genrsa -out .\YourCARoot\private\ForReal.key -rand .\YourCARoot\private\YourRandom.rnd 2048

@rem ---- Not used ----: @echo --- Create Private Key - with Password
@rem ---- Not used ----: @echo (please supply a password when asked to do so)
@rem ---- Not used ----: @echo.
@rem ---- Not used ----: @..\openssl genrsa -out .\YourCARoot\private\ForReal.key -rand .\YourCARoot\private\YourRandom.rnd -des3 2048

@echo.
@echo.
@echo --- Create Certificate using Private Key 
@echo (Please enter the same password you used earlier when asked to do so)
@echo.
@..\openssl req -new -days 3650 -key .\YourCARoot\private\ForReal.key -out .\YourCARoot\certs\ForRealCSR.crt -config .\YourCARoot\config\OpenSSL.conf

@rem ---- Not used ----: @echo.
@rem ---- Not used ----: @echo.
@rem ---- Not used ----: @echo --- To Sign a Certificate Signing Request (CSR) 
@rem ---- Not used ----: @echo.
@rem ---- Not used ----: @..\openssl ca -in .\YourCARoot\certs\ForRealCSR.crt -config .\YourCARoot\config\OpenSSL.conf -policy policy_anything -out .\YourCARoot\certs\ForRealSigned.crt

@echo.
@echo.
@echo --- You can copy and paste the text in .\YourCARoot\certs\ForRealCSR.crt 
@echo.
@type .\YourCARoot\certs\ForRealCSR.crt


@echo.
@echo.
@pause

@rem --- Creates a New CA (Certificate Authority) -----
@rem --- Copyright (c) 2006 CapeSoft Software (Pty) Ltd -----

@echo This batch file will create a new CA Key and Certificate 
@echo.
@echo Press Ctrl-C to abort if you do not want to continue.
@echo.
@pause

@cls

@rem ---- Not used ----: echo --- Create Private Key (with no password)
@rem ---- Not used ----: ..\openssl genrsa -out .\YourCARoot\private\YourCA.key -rand .\YourCARoot\private\YourRandom.rnd 2048

@echo --- Create Private Key - with Password
@echo (please supply a password when asked to do so)
@echo.
@..\openssl genrsa -out .\YourCARoot\private\YourCA.key -rand .\YourCARoot\private\YourRandom.rnd -des3 2048 (option 2 - with Password)

@echo.
@echo.
@echo --- Create Certificate using Private Key 
@echo (Please enter the same password you used earlier when asked to do so)
@echo.
@..\openssl req -new -x509 -days 3650 -key .\YourCARoot\private\YourCA.key -out .\YourCARoot\cacert\YourCA.crt -config .\YourCARoot\config\OpenSSL.conf

@echo.
@echo.
@echo --- Display Certificate
@echo.
@..\openssl x509 -in .\YourCARoot\cacert\YourCA.crt -noout -subject -issuer -email -dates

@echo.
@echo.
@pause
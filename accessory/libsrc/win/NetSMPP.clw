! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software  - www.capesoft.com

  Member()

  Omit('EndOmit',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
  EndOmit

    include('Equates.CLW'), once
    include('Keycodes.CLW'), once
    include('Errors.CLW'), once

    map

    end

    include('NetMap.inc'), once            ! NetMap Module Map
    include('NetSMPP.inc'), once


!-----------------------------------------------------------
!
! SmppConnection - the base class for handling connections
!
!-----------------------------------------------------------

SmppConnection.Construct Procedure()
  code
    self.address_range &= new SmppAddress


SmppConnection.Destruct Procedure()
  code
    Dispose(self.address_range)


!!! <summary>Initialize the object for use. Must be called before the object is used</summary>
SmppConnection.Init Procedure (uLong Mode=NET:SimpleClient)
  code
    ! --- Set up for using Asynchronous Open
    self.AsyncOpenUse = 1
    self.AsyncOpenTimeOut = NET:ASYNC_OPEN_TIMEOUT
    self.InActiveTimeout = NET:INACTIVE_TIMEOUT

    parent.Init(mode)


!!! <summary>Opens the connection to the specified server on the passed port</summary>
SmppConnection.Open Procedure (string Server,uShort Port=0)
  code
    if port = 0
        port = 1775
    end
    self.server = Clip(server)
    self.port = Clip(port)

    parent.open (server, port)

    ! self.Process() will be called with packetType of
    ! NET:SimpleAsyncOpenSuccessful on successful open


!!! <summary>
!!!     Converts the string address to an SmppAddress and calls the DoBind virtual method
!!!     which will ve overridden by the child classes to perform a specific bind operation for
!!!     the particular type of session. A Bind operation to registers the ESME with
!!!     the MC system and request an SMPP session
!!! </summary>
!!! <param name="sysid">Identifies the ESME system requesting the bind. The "user name".</params>
!!! <param name="passwprd">The password to use for authentication</param>
!!! <param name="systype">Identifies the type of ESME system. Can be an empty string.</param>
!!! <param name="addr_range">The ESME address, or an empty string if not known</param>
SmppConnection.DoBind Procedure (string sysid, string password, string systype, string addr_range, ushort npi =0, ushort ton =0)
addr            SmppAddress
pn              equate('SmppConnection.DoBind')
  code
        self.ErrorTrap('Error, the NetTalk SMPP functionality requires Capesoft StringTheory (add the StringTheory global extension)', pn)
        return

    addr.ton = ton
    addr.npi = npi
    addr.addr = Clip(addr_range)

    self.DoBind(sysid, password, systype, addr)         ! Call the virtual Bind method implemented by the child class


!!! <summary>A virtual Bind method which is overridden by each child class to provide Bind functionality.
!!!     Must be overridden by the child class!
!!! </summary>
SmppConnection.DoBind Procedure (string sysid, string password, string systype, *SmppAddress addr_range)
  code
    self.Log('SmppConnection.DoBind', 'Should never be called - there is an error in the child class. This method MUST be overridden!')


SmppConnection.DoUnbind Procedure ()
pak             CUnbind
  code
    self.SendPacket(pak)


SmppConnection.EnquireLink Procedure ()
pak             EnquireLink
  code
    self.SendPacket(pak)


!!! <summary>
!!!     Send all data in the passed string. If the length is greater
!!!     than the maximum size of a single packet then it is split
!!!     and sent a packet at a time.
!!! </summary>
!!! <param name="data">A string which contains the data to send (may be binary)</param>
!!! <param name="dataLen">The length of the data in the string to send. If less than
!!!     or equal to zero, or greater than the length of the passed data string, then
!!!     the length of the data string is used</param>
!!! <returns>Nothing</returns>
!!! <remarks>The passed data string is treated as binary (as it may well contain
!!!     binary data)</remarks>
SmppConnection.Send Procedure(*string data, long dataLen=0)
s               long
  code
    if not dataLen
        dataLen = Len(data)
    end

    if dataLen <= NET:MaxBinData                            ! Single packet sent
        self.packet.binDataLen = dataLen
        self.packet.binData = data
        self.Send()
    else                                                    ! Send multiple packets
        loop s = 1 to dataLen by NET:MaxBinData
            if s + NET:MaxBinData > dataLen
                self.packet.binDataLen = dataLen - s + 1
                self.packet.binData = data[s : dataLen]
            else
                self.packet.binDataLen = NET:MaxBinData
                self.packet.binData = data[s : s + NET:MaxBinData-1]
            end
            self.Send()
        end
    end


!!! <summary>Encodes the passed Packet object and sends the resultant
!!!     data</summary>
!!! <param name="pak">A packet object which represents the packet to
!!!     send. Can be any of the objects derived from the PacketBase class</param>
!!! <returns>Nothing</returns>
SmppConnection.SendPacket Procedure (*PacketBase pak)
encPacket       StringTheory
pn              equate('SmppConnection.SendPacket')
  code
    self.Log(pn, '')
    pak.EncodeBody(encPacket)
    self.Send(encPacket.value)

!!! <summary>Processes incoming data and calls the appropriate method to
!!!     handle the data based on its type</summary>
!!! <returns>Nothing</returns>
SmppConnection.Process Procedure ()
pn              equate('SmppConnection.Process')
st              StringTheory                                ! csDEBUG
bi              BInt
packetSize      long
  code

    case self.packet.packetType
    of NET:SimpleIdleConnection
        self.Log(pn, '. The connection is idle, close it.')
        self._CloseIdleConnection()
    of NET:SimpleAsyncOpenSuccessful
        self.Log (pn, 'Connection to ' & self._Connection.Server |
               & ':' & self._Connection.Port & ' has been opened.')
    of NET:SimplePartialDataPacket
        ! csDEBUG
        self.Log(pn, '')
        st.SetValue(self.packet.binData[1 : self.packet.binDataLen])
        st.ToHex()
        self.Log(pn, '. Received packet data from the server: ' & st.GetValue())
        !---

        ! csTODO: Handle partial data:
        !   1. When the first packet arrives if there are 4 bytes or more read the size and then keep reading until
        !       that amount of data has been received and then process it.
        !   2. For less than 4 bytes buffer it and wait until the 4 length bytes have been received
        !   3. For data not in the current packet buffer it and wait for the next full packet (read the
        !       the packet size from the first 4 bytes of this packet.

        ! csTODO: Process the received packet based on the type

        ! Read the first 4 bytes for the data size
        packetSize = bi.ReadInt(self.packet.binData)
        self.Log(pn, 'Data length field: ' & packetSize & '. Bytes received from server: ' & self.packet.binDataLen)
        self._ParsePacket(self.packet.binData[5 : packetSize], packetSize)
    else
        self.Log(pn, 'Unhandled packet type: ' & self.packet.packetType)
    end

!!! <summary>Virtual method for parsing received data and loading it into
!!! the correct packet type.
!!! Must be derived by all child classes</summary>
SmppConnection._ParsePacket Procedure (*string pby, long nsz)
  code
    self.Log('SmppConnection._ParsePacket', 'Should never be called - there is an error in the child class. This method MUST be overridden!')


!!! <summary>
!!!     Virtual Callback method for handling received data.
!!!     Implemented by each instance of the class (object) to process
!!!     incoming packets
!!! </summary>
!!! <param name="pak">The packet object to process, should be cast to the correct
!!!     type of object after checking the command_id property for the type of command</param>
SmppConnection.ProcessPacket Procedure (*PacketBase pak)
  code
    self.Log('SmppConnection.ProcessPacket', 'Should never be called - there is an error in the child class. This method MUST be overridden!')




SmppConnection._CloseIdleConnection Procedure()
pn              equate('SmppConnection._CloseIdleConnection')
  code
    self.Log (pn, 'The connection to ' & self.server & ' on port ' & self.port & 'was idle and will be aborted.')
    self.Error = ERROR:IdleTimeOut
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Idle Timeout - Warning connection has been idle for more than ' & |
                  self._Connection.InActiveTimeout/100 & ' seconds. Connection will be closed.', pn)
    self.Abort()



!!! <summary>Returns the error message associated with the passed command_status code</summary>
!!! <param name="command_status">The Command Status value to get the associated message for</param>
!!! <returns>A string which contains the message associated with the passed status code</returns>
SmppConnection.ErrorText Procedure(long command_status)
  code
    case command_status
    of ESME_ROK
            return 'No Error'
    of ESME_RINVMSGLEN
        return 'Message Length is invalid'
    of ESME_RINVCMDLEN
        return 'Command length is invalid'
    of ESME_RINVCMDID
        return 'Invalid command id'
    of ESME_RINVBNDSTS
        return 'Incorrect BIND status for given command'
    of ESME_RALYBND
        return 'ESME already in bound state'
    of ESME_RINVPRTFLG
        return 'Invalid priority flag'
    of ESME_RINVREGDLVFLG
        return 'Invalid registered delivery flag'
    of ESME_RSYSERR
        return 'System error'
    of ESME_RINVSRCADR
        return 'Invalid source address'
    of ESME_RINVDSTADR
        return 'Invalid destination address'
    of ESME_RINVMSGID
        return 'Invalid message id'
    of ESME_RBINDFAIL
        return 'Bind failed'
    of ESME_RINVPASWD
        return 'Invalid password'
    of ESME_RINVSYSID
        return 'Invalid system id'
    of ESME_RCANCELFAIL
        return 'Cancel SM failed'
    of ESME_RREPLACEFAIL
        return 'Replace SM failed'
    of ESME_RMSGQFUL
        return 'Message queue full'
    of ESME_RINVSERTYP
        return 'Invalid service type'
    of ESME_RINVNUMDESTS
        return 'Invalid number of destinations'
    of ESME_RINVDLNAME
        return 'Invalid distribution list name'
    of ESME_RINVDESTFLAG
        return 'Destination flag is invalid'
    of ESME_RINVSUBREP
        return 'Invalid "submit_with_replace" request'
    of ESME_RINVESMCLASS
        return 'Invalid "esm_class" field data'
    of ESME_RCNTSUBDL
        return 'Cannot submit to distribution list'
    of ESME_RSUBMITFAIL
        return '"submit_sm" or "submit_multi" failed'
    of ESME_RINVSRCTON
        return 'Invalid source address TON'
    of ESME_RINVSRCNPI
        return 'Invalid source address NPI'
    of ESME_RINVDSTTON
        return 'Invalid destination address TON'
    of ESME_RINVDSTNPI
        return 'Invalid destination address NPI'
    of ESME_RINVSYSTYP
        return 'Invalid "system_type" field'
    of ESME_RINVREPFLAG
        return 'Invalid "replace_if_present" flag'
    of ESME_RINVNUMMSGS
        return 'Invalid number of messages'
    of ESME_RTHROTTLED
        return 'Throttling error'
    of ESME_RINVSCHED
        return 'Invalid Scheduled Delivery Time'
    of ESME_RINVEXPIRY
        return 'Invalid Message Validity Period (Expiry time)'
    of ESME_RINVDFTMSGID
        return 'Predefined Message Invalid or Not Found'
    of ESME_RX_T_APPN
        return 'ESME Receiver Temporary App Error Code'
    of ESME_RX_P_APPN
        return 'ESME Receiver Permanent App Error Code'
    of ESME_RX_R_APPN
        return 'ESME Receiver Reject Message Error Code'
    of ESME_RQUERYFAIL
        return '"query_sm" request failed'
    of ESME_RINVOPTPARSTREAM
        return 'Error in the optional part of the PDU Body'
    of ESME_RINVPARLEN
        return 'Invalid Parameter Length'
    of ESME_RMISSINGOPTPARAM
        return 'Expected Optional Parameter missing'
    of ESME_ROPTPARNOTALLWD
        return 'Invalid Optional Parameter Value'
    of ESME_RDELIVERYFAILURE
        return 'Delivery Failure (used for "data_sm_resp")'
    of ESME_RUNKNOWNERR
        return 'Unknown Error'
    of ESME_RTRANSPORTERR
        return 'Transport Error'
    of ESME_RGATEWAYFAILURE
        return 'Gateway Failure (Unable to reach internal gateway)'
    of ESME_RPERMANENTFAILURE
        return 'Permanent Network Failure (To external gateway)'
    of ESME_RTEMPORARYFAILURE
        return 'Temporary Network Failure (To external gateway)'
    of ESME_RINVSUBSCRIBER
        return 'Invalid Subscriber'
    of ESME_RINVMSG
        return 'Invalid Message'
    of ESME_RPROTOCOLERR
        return 'Gateway Protocol Error'
    of ESME_RDUPLICATEDMSG
        return 'Duplicated Messages'
    of ESME_RBARREDBYUSER
        return 'Barred by User'
    of ESME_RCANCELLEDBYSYS
        return 'Cancelled by System'
    of ESME_REXPIRED
        return 'Message Expired'
    else
        return 'Unknown Error'
    end


!!! <summary>Returns a string which describes the type of the packet when passed the
!!!     SMPP command ID</summary>
!!! <param name="pt">The SMPP command ID</summary>
!!! <returns>A string which describes the packet type</returns>
SmppConnection.PacketType Procedure(long command_id)
  code
    case command_id
    of SMPP_GENERIC_NACK
        return 'SMPP_GENERIC_NACK'
    of SMPP_BIND_RECEIVER
        return 'SMPP_BIND_RECEIVER'
    of SMPP_BIND_RECEIVER_RESP
        return 'SMPP_BIND_RECEIVER_RESP'
    of SMPP_BIND_TRANSMITTER
        return 'SMPP_BIND_TRANSMITTER'
    of SMPP_BIND_TRANSMITTER_RESP
        return 'SMPP_BIND_TRANSMITTER_RESP'
    of SMPP_QUERY_SM
        return 'SMPP_QUERY_SM'
    of SMPP_QUERY_SM_RESP
        return 'SMPP_QUERY_SM_RESP'
    of SMPP_SUBMIT_SM
        return 'SMPP_SUBMIT_SM'
    of SMPP_SUBMIT_SM_RESP
        return 'SMPP_SUBMIT_SM_RESP'
    of SMPP_DELIVER_SM
        return 'SMPP_DELIVER_SM'
    of SMPP_DELIVER_SM_RESP
        return 'SMPP_DELIVER_SM_RESP'
    of SMPP_UNBIND
        return 'SMPP_UNBIND'
    of SMPP_UNBIND_RESP
        return 'SMPP_UNBIND_RESP'
    of SMPP_REPLACE_SM
        return 'SMPP_REPLACE_SM'
    of SMPP_REPLACE_SM_RESP
        return 'SMPP_REPLACE_SM_RESP'
    of SMPP_CANCEL_SM
        return 'SMPP_CANCEL_SM'
    of SMPP_CANCEL_SM_RESP
        return 'SMPP_CANCEL_SM_RESP'
    of SMPP_BIND_TRANSCEIVER
        return 'SMPP_BIND_TRANSCEIVER'
    of SMPP_BIND_TRANSCEIVER_RESP
        return 'SMPP_BIND_TRANSCEIVER_RESP'
    of SMPP_OUTBIND
        return 'SMPP_OUTBIND'
    of SMPP_ENQUIRE_LINK
        return 'SMPP_ENQUIRE_LINK'
    of SMPP_ENQUIRE_LINK_RESP
        return 'SMPP_ENQUIRE_LINK_RESP'
    of SMPP_SUBMIT_MULTI
        return 'SMPP_SUBMIT_MULTI'
    of SMPP_SUBMIT_MULTI_RESP
        return 'SMPP_SUBMIT_MULTI_RESP'
    of SMPP_ALERT_NOTIFICATION
        return 'SMPP_ALERT_NOTIFICATION'
    of SMPP_DATA_SM
        return 'SMPP_DATA_SM'
    of SMPP_DATA_SM_RESP
        return 'SMPP_DATA_SM_RESP'
    of SMPP_SPECIAL_LINKCLOSE
        return 'SMPP_SPECIAL_LINKCLOSE'
    else
        return 'Unknown (' & command_id & ')'
    end

!-----------------------------------------------------------
!
! NetSmppReceiver ESME Receiver Class
!
!-----------------------------------------------------------

!!! <summary>
!!!     Bind to the MS as a Receiver. The purpose of the SMPP bind operation is to register an
!!!     instance of an ESME with the MC system and request a SMPP session over this network
!!!     connection for the submission or delivery of messages. Thus, the Bind operation
!!!     may be viewed as a "login" request.
!!! <summary>
!!! <param name="sysid">Identifies the ESME system requesting to bind as a receiver with the MC</param>
!!! <param name="password">The password may be used to authenticate the ESME requesting to bind</param>
!!! <param name="systype">Identifies the type of ESME system (may be blank)</param>
!!! <param name="src_range">A single ESME address or a range of ESME addresses served via this
!!!     SMPP receiver session. May be a null address (SmppAddress object that has
!!!     not had an address assigned to it) if not known</param>
!!! <returns>Nothing</returns>
NetSmppReceiver.DoBind Procedure (string sysid, string password, string systype, *SmppAddress src_range)
pak             BindReceiver
pn              equate('NetSmppReceiver.DoBind')
  code
    self.Log(pn, '')

    self.system_id = Clip(sysid)
    self.password = Clip(password)
    self.system_type = Clip(systype)

    self.address_range.Equals(src_range)

    pak.system_id = self.system_id
    pak.password = self.password
    pak.system_type = self.system_type

    pak.SetSourceRange(src_range)

    self.SendPacket(pak)


!!! <summary>Open a listening connection for Outbind. Not supported in this release!</summary>
!!! <returns>Nothing</summary>
NetSmppReceiver.LinkListen Procedure ()
  code
    !parent.LinkListen()
    ! csTODO: Need to do this - requires a server object and handling of incoming
    ! requests. The receiver ESME would then bind to the MC.
    return false


NetSmppReceiver._ParsePacket Procedure (*string pby, long nsz)
pn              equate('NetSmppReceiver._ParsePacket')
cmdId           long        ! Clarion equates are longs
cmdStatus       long
seqNum          long
packet          &PacketBase
bi              BInt
st              StringTheory
  code
    self.Log(pn, '')

    if nsz < 16
        self.ErrorTrap('Could not parse the packet, the passed length was less than 16 bytes', pn)
        return
    end

    cmdId = bi.ReadInt(pby)
    cmdStatus = bi.ReadInt(pby, 4)
    seqNum = bi.ReadInt(pby, 8)
                                                            self.Log(pn, 'cmdId: ' & cmdId & '(' & st.LongToHex(cmdId) & '), cmdStatus: ' & st.LongToHex(cmdStatus) & ', seqNum: ' & st.LongToHex(seqNum))
    case cmdId
    of SMPP_GENERIC_NACK
        packet &= new GenericNack
    of SMPP_SPECIAL_LINKCLOSE                               ! special packet generated internally when the link is closing
        packet &= new LinkClose
    of SMPP_BIND_RECEIVER_RESP
        packet &= new BindReceiverResp
    of SMPP_OUTBIND
        ! csTODO: Outbind functionality is NOT currently supported
        packet &= new Outbind
    of SMPP_ENQUIRE_LINK
        packet &= new EnquireLink
    of SMPP_ENQUIRE_LINK_RESP
        packet &= new EnquireLinkResp
    of SMPP_UNBIND_RESP
        packet &= new UnbindResp
    of SMPP_DELIVER_SM
        packet &= new DeliverSM
    else                                                    ! Unknown packet type
        self.ErrorTrap('Unknown packet type with command id: ' & st.LongToHex(cmdId), pn)
        return
    end
                                                            self.Log(pn, 'Load the packet')
    packet.LoadPacket(pby, nsz)
                                                            self.Log(pn, 'Call ProcessPacket')
    self.ProcessPacket(packet)                              ! Process the packet
    Dispose(packet)                                         ! Dispose once processing is complete

!!! <summary>
!!!     Virtual Callback method for handling received data.
!!!     Implemented by each instance of the class (object) to process
!!!     incoming packets
!!! </summary>
NetSmppReceiver.ProcessPacket Procedure (*PacketBase pak)
  code



!-----------------------------------------------------------
!
! NetSmppTransmitter ESME Transmitter Class
!
!-----------------------------------------------------------

!!! <summary>
!!!     Bind to the MS as a Transmitter (sender). The purpose of the SMPP bind operation is to
!!!     register an instance of an ESME with the MC system and request a SMPP session over this
!!!     network connection for the submission or delivery of messages. Thus, the Bind operation
!!!     may be viewed as a "login" request.
!!! <summary>
!!! <param name="sysid">Identifies the ESME system requesting to bind as a receiver with the MC</param>
!!! <param name="password">The password may be used to authenticate the ESME requesting to bind</param>
!!! <param name="systype">Identifies the type of ESME system (may be blank)</param>
!!! <param name="src_range">A single ESME address or a range of ESME addresses served via this
!!!     SMPP receiver session. May be a null address (SmppAddress object that has
!!!     not had an address assigned to it) if not known</param>
!!! <returns>Nothing</returns>
NetSmppTransmitter.DoBind Procedure (string sysid, string password, string systype, *SmppAddress src_range)
pak             BindTransmitter
  code
    self.system_id = Clip(sysid)
    self.password = Clip(password)
    self.system_type = Clip(systype)
    self.address_range.Equals(src_range)

    pak.system_id = self.system_id
    pak.password = self.password
    pak.system_type = self.system_type

    pak.SetSourceRange(src_range)

    self.SendPacket(pak)


NetSmppTransmitter.SubmitMessage Procedure (*SubmitSM pak)
  code
    self.SendPacket(pak)

    return 1


NetSmppTransmitter.SubmitMessage Procedure (*cstring msg, string dst, long ton, long npi)
dst_addr        SmppAddress
  code
    dst_addr.Init(ton, npi, dst)

    return self.SubmitMessage(msg, dst_addr)


NetSmppTransmitter.SubmitMessage Procedure (*cstring msg, *SmppAddress dst)
s               SubmitSM
  code
    s.message.SetValue(msg)

    s.destination.Equals(dst)
    s.source.Equals(self.address_range)

    return self.submitMessage(s)


NetSmppTransmitter.SubmitMessage Procedure (*string msg, long msglen, long enc, *SmppAddress dest, long esm=0)
s               SubmitSM
  code
    s.SetMessage(msg, msgLen)
    s.SetDestination(dest)
    s.SetSource(self.address_range)
    s.SetDataCoding(enc)
    s.SetEsmClass(esm)

    return self.SubmitMessage(s)


NetSmppTransmitter._ParsePacket Procedure (*string pby, long nsz)
pn              equate('NetSmppTransmitter._ParsePacket')
cmdId           long
cmdStatus       long
seqNum          long
packet          &PacketBase
bi              BInt
st              StringTheory
  code
    self.Log(pn, '')
    if nsz < 16
        self.ErrorTrap('Cannot parse the packet as it was too short (less than 16 bytes)', pn)
    end

    cmdId = bi.ReadInt(pby)
    cmdStatus = bi.ReadInt(pby, 4)
    seqNum = bi.ReadInt(pby, 8)
                                                            self.Log(pn, 'cmdId: ' & cmdId & '(' & st.LongToHex(cmdId) & '), cmdStatus: ' & st.LongToHex(cmdStatus) & ', seqNum: ' & st.LongToHex(seqNum))
    case cmdId
    of SMPP_GENERIC_NACK
        packet &= new GenericNack
    of SMPP_SPECIAL_LINKCLOSE
        packet &= new LinkClose
    of SMPP_BIND_TRANSMITTER_RESP
        packet &= new BindTransmitterResp
    of SMPP_SUBMIT_SM_RESP
        packet &= new SubmitSMResp
    of SMPP_QUERY_SM_RESP
        packet &= new QuerySMResp
    of SMPP_ENQUIRE_LINK
        packet &= new EnquireLink
    of SMPP_ENQUIRE_LINK_RESP
        packet &= new EnquireLinkResp
    of SMPP_UNBIND_RESP
        packet &= new UnbindResp
    else                                                    ! Unknown packet type
        self.ErrorTrap('Unknown packet type with command id: ' & st.LongToHex(cmdId), pn)
        return
    end
                                                            self.Log(pn, 'Load the packet')
    packet.LoadPacket(pby, nsz)
                                                            self.Log(pn, 'Call ProcessPacket')
    self.ProcessPacket(packet)
    Dispose(packet)

!!! <summary>
!!!     Virtual Callback method for handling received data.
!!!     Implemented by each instance of the class (object) to process
!!!     incoming packets
!!! </summary>
!!! <param name="pak">The packet object to process, should be cast to the correct
!!!     type of object after checking the command_id property for the type of command</param>
NetSmppTransmitter.ProcessPacket Procedure(*Packetbase pak)
  code



!-----------------------------------------------------------
!
! NetSmppTransceiver ESME Tranceiver Class
!
!-----------------------------------------------------------

!!! <summary>
!!!     Bind to the MS as a Tranceiver (for sending and receiving). The purpose of the SMPP bind operation
!!!     is to register an instance of an ESME with the MC system and request a SMPP session over this network
!!!     connection for the submission or delivery of messages. Thus, the Bind operation
!!!     may be viewed as a "login" request.
!!! <summary>
!!! <param name="sysid">Identifies the ESME system requesting to bind as a receiver with the MC</param>
!!! <param name="password">The password may be used to authenticate the ESME requesting to bind</param>
!!! <param name="systype">Identifies the type of ESME system (may be blank)</param>
!!! <param name="src_range">A single ESME address or a range of ESME addresses served via this
!!!     SMPP receiver session. May be a null address (SmppAddress object that has
!!!     not had an address assigned to it) if not known</param>
NetSmppTransceiver.DoBind Procedure (string sysid, string password, string systype, *SmppAddress addr_range)
pak             BindTransceiver
  code
    self.system_id = Clip(sysid)
    self.password = Clip(password)
    self.system_type = Clip(systype)
    self.address_range.Equals(addr_range)

    pak.system_id = self.system_id
    pak.password = self.password
    pak.system_type = self.system_type

    pak.SetSourceRange(addr_range)

    self.SendPacket(pak)


!!! <summary>Send a message<summary>
!!! <param name="pak">The SubmitSM object which contains the message to send</param>
!!! <remarks></remarks>
NetSmppTransceiver.SubmitMessage Procedure (*SubmitSM pak)
wholeMessage        StringTheory
udh                 UdhMessage
pn                  equate('NetSmppTransceiver.SubmitMessage ')
i                   long
  code
    self.Log(pn, '')
    ! csTODO: This handles 8-bit encoded messages, there is no handling for 7-bit encoded
    ! message currently.

    if pak.message.Len() > SMPP_MAX_SMS_8                   ! Message is too long for a single PDU
        self.refNumber += 1
        if self.refNumber > 255
            self.refNumber = 1
        end
                                                            self.Log(pn, '. Split message and add a UDH (' & pak.message.Len() & ' bytes is too large for single PDU). Reference Number for this set: ' & self.refNumber)
        wholeMessage.SetValue(pak.message.GetValue())       ! Store the whole message so that it can be split up
                                                            self.Log(pn, '. Split the message into parts no longer than ' & SMPP_MAX_SMS_8 - SMPP_UDH_LENGTH & ' bytes')
        wholeMessage.SplitEvery(SMPP_MAX_SMS_8 - SMPP_UDH_LENGTH)  ! Split into strings of no more than 140 octets including the UDH header (5 bytes/octets)
                                                            self.Log(pn, 'Split into ' & wholeMessage.Records() & ' parts')

        udh.refNumber = self.refNumber
        udh.totalParts = wholeMessage.Records()
        udh.iei = SMPP_IE_CONCATENATED                      ! This is a concatenated SMS Information Element
        udh.iel = SMPP_IE_CONCATENATED_LEN

        loop i = 1 to udh.totalParts
            udh.message_text.SetValue(wholeMessage.GetLine(i))
            udh.currentPart = i
            pak.SetMessage(udh.GetUdhMessage())             ! Set the message to the current UDH message part
                                                             self.Log(pn, '. Send part ' & udh.currentPart & ' of ' & udh.totalParts & ': ' & udh.ToString())
            self.SendPacket(pak)                            ! Send this packet
        end
                                                            self.Log(pn, '. Restore the original pack message value')
        pak.SetMessage(wholeMessage.GetValue())             ! Restore the original message contents
    else
                                                            self.Log(pn, '. Single packet send, the message is short enough for a single PDU')
        self.SendPacket(pak)
    end
    return 1

NetSmppTransceiver.SubmitMessage Procedure (*cstring msg, string dst, long ton, long npi)
dst_addr        SmppAddress
  code
    dst_addr.Init(ton, npi, dst)
    return self.SubmitMessage(msg, dst_addr)


NetSmppTransceiver.SubmitMessage Procedure (*cstring msg, *SmppAddress dst)
s               SubmitSM
  code
    s.SetMessage(msg, Len(msg))
    s.SetDestination(dst)
    s.SetSource(self.address_range);

    return self.SubmitMessage(s)


NetSmppTransceiver.SubmitMessage Procedure (*string msg, long msglen=0, long enc, *SmppAddress dest, long esm=0)
s               SubmitSM
  code
    if msglen =0
        msglen = Len(Clip(msg))
    end
    s.SetMessage(msg, msglen)
    s.SetDestination(dest)
    s.SetSource(self.address_range)
    s.SetDataCoding(enc)
    s.SetEsmClass(esm)

    return self.SubmitMessage(s)


!!! <summary>Parse the received packet data and store the result in a packet object. This is then
!!!     pass to the ProcessPacket callback method to allow it to be processed and handled</summary>
!!! <param name="pby">A string which contains the packet data</param>
!!! <param name="nsz">Length of the the data in the passed string</param>
!!! <returns>Nothing</returns>
NetSmppTransceiver._ParsePacket Procedure (*string pby, long nsz)
pn              equate('NetSmppTransceiver._ParsePacket')
cmdId           long
cmdStatus       long
seqNum          long
packet          &PacketBase
bi              BInt
st              StringTheory
  code
    self.Log(pn, '')
    if nsz < 16
        self.ErrorTrap('Cannot parse the packet as it was too short (less than 16 bytes)', pn)
    end

    cmdId = bi.ReadInt(pby)
    cmdStatus = bi.ReadInt(pby, 4)
    seqNum = bi.ReadInt(pby, 8)

    case cmdId
    of SMPP_GENERIC_NACK
        packet &= new GenericNack
    of SMPP_SPECIAL_LINKCLOSE                               ! Special internal packet indicating that the link has closed.
        packet &= new LinkClose
    of SMPP_BIND_TRANSCEIVER_RESP
        packet &= new BindTransceiverResp
    of SMPP_SUBMIT_SM_RESP
        packet &= new SubmitSMResp
    of SMPP_QUERY_SM_RESP
        packet &= new QuerySMResp
    of SMPP_UNBIND_RESP
        packet &= new UnbindResp
    of SMPP_DELIVER_SM
        packet &= new DeliverSm
    of SMPP_ENQUIRE_LINK
        packet &= new EnquireLink
    of SMPP_ENQUIRE_LINK_RESP
        packet &= new EnquireLinkResp
    else                                                    ! Unknown packet type
        self.ErrorTrap('Unknown packet type with command id: ' & st.LongToHex(cmdId), pn)
        return
    end
                                                            self.Log(pn, 'Load the packet')
    packet.LoadPacket(pby, nsz)
                                                            self.Log(pn, 'Call ProcessPacket')
    self.ProcessPacket(packet)
    Dispose(packet)

!!! <summary>
!!!     Virtual Callback method for handling received data.
!!!     Implemented by each instance of the class (object) to process
!!!     incoming packets
!!! </summary>
!!! <param name="pak">The packet object to process, should be cast to the correct
!!!     type of object after checking the command_id property for the type of command</param>
NetSmppTransceiver.ProcessPacket Procedure(*Packetbase pak)
  code



!-----------------------------------------------------------
!
!                   Packet Classes
!
!-----------------------------------------------------------


!-----------------------------------------------------------
!
! LinkClose class
!
!-----------------------------------------------------------

LinkClose.Construct Procedure ()
  code
    self.command_id = SMPP_SPECIAL_LINKCLOSE


LinkClose.EncodeBody Procedure (*StringTheory pby)
  code
    parent.encodeBody(pby)
    pby.SetValue(self.buffer.GetValue())

!-----------------------------------------------------------
!
! BindTransmitter class
!
!-----------------------------------------------------------

BindTransmitter.Construct Procedure ()
  code
    self.command_id = SMPP_BIND_TRANSMITTER


!-----------------------------------------------------------
!
! BindReceiver class
!
!-----------------------------------------------------------

BindReceiver.Construct Procedure ()
  code
    self.command_id = SMPP_BIND_RECEIVER


!-----------------------------------------------------------
!
! BindTransceiver class
!
!-----------------------------------------------------------

BindTransceiver.Construct Procedure ()
  code
    self.command_id = SMPP_BIND_TRANSCEIVER


!-----------------------------------------------------------
!
! OutBind class
!
!-----------------------------------------------------------

OutBind.Construct Procedure ()
  code
    self.command_id = SMPP_OUTBIND


OutBind.EncodeBody Procedure (*StringTheory pby)
  code
    parent.EncodeBody(pby)

    self.buffer.AppendA(self.system_id)
    self.WriteNull()

    self.buffer.AppendA(self.password)
    self.WriteNull()

    pby.SetValue(self.buffer.GetValue())

    Assert(self.GetCommandLength() = pby.LengthA(), 'OutBind.EncodeBody - Error, the length of the packet created is not equal to the length of the data stored!')


OutBind.LoadPacket Procedure (*string pby, long nsz)
pos             long, auto
  code
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        return false
    end

    pos = 12
    self.ReadString(pby, pos, self.system_id)
    self.ReadString(pby, pos, self.password)

    return true


OutBind.GetCommandLength Procedure ()
  code
    return parent.GetCommandLength() |
         + Len(self.system_id) |
         + Len(self.password) |
         + 2


!-----------------------------------------------------------
!
! QuerySM class
!
!-----------------------------------------------------------
QuerySM.Construct Procedure()
  code
    self.command_id = SMPP_QUERY_SM
    self.source &= new SmppAddress


QuerySM.Destruct Procedure()
  code
    Dispose(self.source)


QuerySM.EncodeBody Procedure (*StringTheory pby)!, virtual
pn              equate('QuerySM.EncodeBody')
  code
    self.Log(pn & ': ' & self.ToString())
    parent.EncodeBody(pby)

    self.buffer.AppendA(self.message_id)
    self.WriteNull
    self.WriteAddress(self.source)

    pby.SetValue(self.buffer.GetValue())

    Assert(self.GetCommandLength() = pby.LengthA(), pn & ' - Error, the length of the packet created is not equal to the length of the data stored!')


QuerySM.LoadPacket Procedure (*string pby, long nsz)!, bool, virtual
pn              equate('QuerySM.LoadPacket')
pos             long, auto
  code
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        return false
    end

    pos = 12

    self.ReadString(pby, pos, self.message_id)
    self.ReadAddress(pby, pos, self.source)

    return true


!!! <summary>Retrieve the length of the SMPP packet data</summary>
!!! <returns>The packet length in bytes</summary>
QuerySM.GetCommandLength Procedure ()
  code
    return parent.GetCommandLength() |
         + Len(self.message_id) |
         + self.source.GetLength() |
         + 1

!!! <summary>
!!!     Creates a string which contains the properties and their values for the object.
!!! </summary>
!!! <returns>A string which contains each of the object properties and their values</string>
QuerySM.ToString Procedure ()
  code
    return 'QuerySM [<13,10>' |
         & parent.Tostring() & '<13,10>' |
         & '  message_id: ' & self.message_id & '<13,10>' |
         & '  source: ' & self.source.addr& ' (TON: ' & self.source.ton & ', NPI: ' & self.source.npi & ')<13,10>' |
         & ']'

!-----------------------------------------------------------
!
! CUnbind class
!
!-----------------------------------------------------------

CUnbind.Construct Procedure()
  code
    self.command_id = SMPP_UNBIND


CUnbind.EncodeBody Procedure (*StringTheory pby)!, virtual
  code
    parent.EncodeBody(pby)
    pby.SetValue(self.buffer.GetValue())

!-----------------------------------------------------------
!
! SubmitSM class
!
!-----------------------------------------------------------

SubmitSM.Construct Procedure()
  code
    self.command_id = SMPP_SUBMIT_SM


!-----------------------------------------------------------
!
! DeliverSM class
!
!-----------------------------------------------------------

DeliverSM.Construct Procedure()
  code
    self.command_id = SMPP_DELIVER_SM


!-----------------------------------------------------------
!
! DataSM class
!
!-----------------------------------------------------------

DataSM.Construct Procedure()
  code
    self.command_id = SMPP_DATA_SM



!-----------------------------------------------------------
!
! DataSMResp class
!
!-----------------------------------------------------------

DataSMResp.Construct Procedure()
  code
    self.command_id = SMPP_DATA_SM_RESP


DataSMResp.Init Procedure(*DataSM pak)
  code
    self.sequence_number = pak.sequence_number


!-----------------------------------------------------------
!
! BindTransmitterResp class
!
!-----------------------------------------------------------

BindTransmitterResp.Construct Procedure()
  code
    self.command_id = SMPP_BIND_TRANSMITTER_RESP



BindTransmitterResp.Init Procedure(*BindTransmitter pak)
  code
    self.sequence_number = pak.sequence_number



!-----------------------------------------------------------
!
! BindReceiverResp class
!
!-----------------------------------------------------------

BindReceiverResp.Construct Procedure()
  code
    self.command_id = SMPP_BIND_RECEIVER_RESP



BindReceiverResp.Init Procedure(*BindReceiver pak)
  code
    self.sequence_number = pak.sequence_number


!-----------------------------------------------------------
!
! BindTransceiverResp class
!
!-----------------------------------------------------------

BindTransceiverResp.Construct Procedure()
  code
    self.command_id = SMPP_BIND_TRANSCEIVER_RESP


BindTransceiverResp.Init Procedure(*BindTransceiver pak)
  code
    self.sequence_number = pak.sequence_number



!-----------------------------------------------------------
!
! QuerySMResp class
!
!-----------------------------------------------------------

QuerySMResp.Construct Procedure ()
  code
    self.command_id = SMPP_QUERY_SM_RESP



QuerySMResp.Init Procedure(*QuerySM pak)
  code
    self.sequence_number = pak.sequence_number


!!! <summary>
!!!     Convert the data stored in the object into a packet and store
!!!     the packet data in the passed string.
!!! </summary>
!!! <param name="pby">A pointer to a string to store the passed packet data in. The
!!!     data should be treated as binary</param>
!!! <param name="nsz">A pointer to an integer to store the length of the packet created in (in bytes)</param>
QuerySMResp.EncodeBody Procedure (*StringTheory pby)
  code
    self.buffer.AppendA(self.message_id)
    self.WriteNull

    self.buffer.AppendA(self.final_date.ToString())
    self.WriteNull

    self.WriteByte(self.message_state)
    self.WriteByte(self.error_code)

    !nsz = self.buffer.LengthA()
    pby.SetValue(self.buffer.GetValue())

    Assert(self.GetCommandLength() = pby.LengthA(), 'QuerySMResp.EncodeBody - Error, the length of the packet created is not equal to the length of the data stored!')


!!! <summary>
!!!     Loads from the passed string into this packet object
!!! </summary>
!!! <param name="pby">A string that contains the packet data</param>
!!! <param name="nsz">The number of bytes of data that the packet contains</param>
!!! <returns>True if successful or false otherwise (if the passed length was invalid).</returns>
QuerySMResp.LoadPacket Procedure (*string pby, long nsz)
pos             long, auto
finalDate       cstring(17)
  code
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        return false
    end

    pos = 12

    self.ReadString(pby, pos, self.message_id)
    self.ReadString(pby, pos, finalDate)
    if finalDate <> ''
        self.final_date.SetDate(finalDate)
    end

    self.ReadByte(pby, pos, self.message_state)
    self.ReadByte(pby, pos, self.error_code)

    return true


QuerySMResp.GetCommandLength Procedure ()
  code
    return parent.GetCommandLength() |
         + Len(self.message_id) |
         + self.final_date.GetLength() |
         + 2 + 2


!!! <summary>
!!!     Creates a string which contains the properties and their values for the object.
!!! </summary>
!!! <returns>A string which contains each of the object properties and their values</string>
QuerySMResp.ToString Procedure ()
  code
    return 'QuerySMResp [<13,10>' |
         & parent.Tostring() & '<13,10>' |
         & '  message_id: ' & self.message_id & '<13,10>' |
         & '  final_date: ' & self.final_date.ToString() & '<13,10>' |
         & '  message_state: ' & self.message_state & '<13,10>' |
         & '  error_code: ' & self.error_code & '<13,10>' |
         & ']'



!-----------------------------------------------------------
!
! SubmitSMResp class
!
!-----------------------------------------------------------

SubmitSMResp.Construct Procedure ()
  code
    self.command_id = SMPP_SUBMIT_SM_RESP


SubmitSMResp.Init Procedure(*SubmitSM pak)
  code
    self.sequence_number = pak.sequence_number


!-----------------------------------------------------------
!
! DeliverSMResp class
!
!-----------------------------------------------------------

DeliverSMResp.Construct Procedure ()
  code
    self.command_id = SMPP_DELIVER_SM_RESP


DeliverSMResp.Init Procedure(*DeliverSM pak)
  code
    self.sequence_number = pak.sequence_number


!DeliverSMResp

!-----------------------------------------------------------
!
! UnbindResp class
!
!-----------------------------------------------------------

UnbindResp.Construct Procedure ()
  code
    self.command_id = SMPP_UNBIND_RESP


UnbindResp.Init Procedure(*CUnbind pak)
  code
    self.sequence_number = pak.sequence_number


UnbindResp.EncodeBody Procedure (*StringTheory pby)!, virtual
  code
    parent.EncodeBody(pby)
    pby.SetValue(self.buffer.GetValue())

!-----------------------------------------------------------
!
! EnquireLink class
!
!-----------------------------------------------------------

EnquireLink.Construct Procedure ()
  code
    self.command_id = SMPP_ENQUIRE_LINK



EnquireLink.EncodeBody Procedure (*StringTheory pby)!, virtual
  code
    parent.EncodeBody(pby)
    pby.SetValue(self.buffer.GetValue())

!-----------------------------------------------------------
!
! EnquireLinkResp class
!
!-----------------------------------------------------------

EnquireLinkResp.Construct Procedure ()
  code
    self.command_id = SMPP_ENQUIRE_LINK_RESP



EnquireLinkResp.Init Procedure(long seqNo)
  code
    self.sequence_number = seqNo


EnquireLinkResp.EncodeBody Procedure (*StringTheory pby)!, virtual
  code
    parent.EncodeBody(pby)
    pby.SetValue(self.buffer.GetValue())

!-----------------------------------------------------------
!
! GenericNack class
!
!-----------------------------------------------------------

GenericNack.Construct Procedure ()
  code
    self.command_id = SMPP_GENERIC_NACK


GenericNack.EncodeBody Procedure (*StringTheory pby)!, virtual
  code
    parent.EncodeBody(pby)
    pby.SetValue(self.buffer.GetValue())

!-----------------------------------------------------------
!
! AlertNotification class
!
!-----------------------------------------------------------

AlertNotification.Construct Procedure()
  code
    self.command_id = SMPP_ALERT_NOTIFICATION
    self.source &= new SmppAddress
    self.esme &= new SmppAddress


AlertNotification.Destruct Procedure()
  code
    Dispose(self.source)
    Dispose(self.esme)


AlertNotification.EncodeBody Procedure (*StringTheory pby)!, virtual
pos             long
  code
    parent.EncodeBody(pby)

    self.WriteAddress(self.source)
    self.WriteAddress(self.esme)

    pby.SetValue(self.buffer.GetValue())

    Assert(self.GetCommandLength() = pby.LengthA(), 'AlertNotification.EncodeBody - Error, the length of the packet created is not equal to the length of the data stored!')

AlertNotification.LoadPacket Procedure (*string pby, long nsz)
pos             long, auto
  code
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        return false
    end

    pos = 12

    self.ReadAddress(pby, pos, self.source)
    self.ReadAddress(pby, pos, self.esme)

    return true


AlertNotification.GetCommandLength Procedure ()
  code
    return parent.GetCommandLength() |
         + self.source.GetLength() |
         + self.esme.GetLength() |
         + 1


!-----------------------------------------------------------
!
! SmppAddress Class
!
!-----------------------------------------------------------


!!! <summary>Gets the length required to store an Address</summary>
!!! <returns>The number of bytes required to store the address</returns>
SmppAddress.GetLength Procedure()
  code
    return 3 + Len(self.addr)                               ! Length of the address cstring plus the TON and NPI bytes


!!! <summary>
!!!     Equality operator. Copies the passed rhs SmppAddress
!!!     properties into the current object
!!! </summary>
!!! <returns>Nothing</returns>
SmppAddress.Equals Procedure(*SmppAddress rhs)
  code
    self.ton = rhs.ton
    self.npi = rhs.npi
    self.addr = rhs.addr


!!! <summary>Initializes the object using the passed values</summary>
!!! <param name="adrton">The TON byte value</param>
!!! <param name="adrnpi">The NPI byte value</param>
!!! <param name="adraddr">A string which contains the address</param>
!!! <returns>Nothing</returns>
SmppAddress.Init Procedure(long adrton, long adrnpi, string addr)
  code
    self.ton = adrton
    self.npi = adrnpi
    self.addr = Clip(addr)


!-----------------------------------------------------------
!
! BInt Class - provides Big Endian byte order handling
!
!-----------------------------------------------------------


!!! <summary>
!!!    Write an integer to the passed string after the passed offset in Big Endian
!!!    byte order.
!!! <summary>
!!! <remarks>The data is written starting at the byte AFTER the offset, so an offset of 4
!!!    would write from byte 5 onwards. The bytes are written in Big Endian order (the most
!!!    significant byte first). Use the ReadInt method to read the bytes from the string).
BInt.WriteInt Procedure (*string pby, long ival, long offset=0)
bvals           byte, dim(4), over(ival)
pn              equate('')
w_msg           cstring(128)
  code
    if Len(pby) < offSet + 4
        NetDebugTrace('[NetSmpp] BInt.WriteInt - Cannot store the passed integer, ' |
                    & 'the passed offset would write past the end of the string.')

        return false
    end

    ! Not: The data is written AFTER the offset, not start at the offset.
    pby[offset + 1] = Chr(bvals[4])
    pby[offset + 2] = Chr(bvals[3])
    pby[offset + 3] = Chr(bvals[2])
    pby[offset + 4] = Chr(bvals[1])

    return true


!!! <summary>Reads 4 bytes from the passed string and converts them to an integer. Uses
!!!     Big Endian byte order when reading the bytes</summary>
!!! <param name="pby">A pointer to the string to store the bytes in</param>
!!! <param name="offset">Bytes are read starting at the byte after the passed offset. An
!!!     offset of zero starts at the first byte of the string.
!!! <returns>The integer value as a LONG</returns>
!!! <remarks>Note that data is read starting at the byte AFTER the offset, so an offset
!!!     of 0 returns the first 4 bytes (pby[1:4]), an offset of 4 returns the next 4, offset 8 returns
!!!     the 4 bytes thereafter and so on. Use WriteInt to write an integer to a string
!!!     in Big Endian byte order.</remarks>
BInt.ReadInt Procedure (*string pby, long offset=0)
bvals           byte, dim(4)
ival            long, over(bvals)
pn              equate('PacketBase.ReadInt')
  code
    if Len(pby) < offset + 4
        NetDebugTrace('[NetSmpp] BInt.ReadInt - Cannot read the integer from the string, ' |
                    & 'the passed offset would read past the end of the string.')

        return 0
    end

    bvals[1] = Val(pby[offset + 4])
    bvals[2] = Val(pby[offset + 3])
    bvals[3] = Val(pby[offset + 2])
    bvals[4] = Val(pby[offset + 1])
    return ival


!-----------------------------------------------------------
!
! SmppDate class
!
!-----------------------------------------------------------

SmppDate.Construct Procedure()
  code
    OS_GetSystemTime(self.m_time)
    self.isNull = true



!!! <summary>
!!!     Convert an Smpp time of the form 'YYMMDDhhmmsstnn+' to SYSTEMTIME
!!!     and stores it in the m_time property of the class.
!!! </summary>
!!! <param name="sd">A null terminate string which contains the data in the SMPP
!!!     format: 'YYMMDDhhmmsstnn+'</param>
!!! <returns>Nothing</returns>
SmppDate.SetDate Procedure (*cstring sd)
smpp_time   group(NET_SMPP_TIME), over(sd)
            end
milSec      long
tz          long
yr          long
  code
    if sd = ''
        self.isNull = true
    else
        self.m_time.wYear = smpp_time.yy + 2000             ! Convert from YY to a four digit year
        self.m_time.wMonth = smpp_time.mm
        self.m_time.wDay = smpp_time.dd
        self.m_time.wHour = smpp_time.hh
        self.m_time.wMinute = smpp_time.min
        self.m_time.wSecond = smpp_time.sec
        self.m_time.wMilliseconds = smpp_time.t * 100        ! Convert most significant digit back to milliseconds

        self.isNull = false
    end


!!! <summary>Sets the stored data to the passed SYSTEMTIME</summary>
!!! <param name="sd">The SYSTEMTIME to set the stored time to</param>
!!! <returns>Nothing</returns>
SmppDate.SetDate Procedure (*NET_SYSTEMTIME sd)!, virtual
  code
    self.m_time = sd
    self.isNull = false



!!! <summary>
!!!     Converts the stored SYSTEMTIME to a C octet string
!!!     which contains the UTC time in SMPP format (16 bytes)
!!! </summary>
!!! <returns>
!!!     A 16 byte string which contains the date and time
!!!     in the SMPP format. Note the the class uses UTC time, not
!!!     local time and hence the UTC offset is always zero
!!! </returns>
SmppDate.ToString Procedure ()
ts          string(16)
smpp_time   group(NET_SMPP_TIME), over(ts)
            end
  code
    if self.isNull
        return ''
    else
        smpp_time.yy = Sub(self.m_time.wYear, Len(self.m_time.wYear)-1, 2)
        smpp_time.mm = Format(self.m_time.wMonth, @n02)
        smpp_time.dd = Format(self.m_time.wDay, @n02)
        smpp_time.hh = Format(self.m_time.wHour, @n02)
        smpp_time.min = Format(self.m_time.wMinute, @n02)
        smpp_time.sec = Format(self.m_time.wSecond, @n02)
        smpp_time.t  = Sub(self.m_time.wMilliseconds, 1, 1)     ! Only the most significant digit stored
        smpp_time.nn = '00'
        smpp_time.p  = '+'
        return ts
    end


!!! <summary>
!!!     Equality operator - copies the value stored in the passed SmppDate
!!!     object into this one
!!! </summary>
!!! <param name="rhs">The SmppDate object to copy the value from</param>
!!! <returns>Nothing</returns>
SmppDate.Equals Procedure(*SmppDate rhs)
  code
    self.m_time = rhs.m_time
    self.isNull = rhs.isNull


!!! <summary>Sets the isNull property to indicate a null date</summary>
!!! <returns>Nothing</returns>
SmppDate.SetToNull Procedure()
  code
    self.isNull = true


!!! <summary>Returns the byte length of an SMPP date</summary>
!!! <returns>The number of bytes used by the SMPP data format</summary>
SmppDate.GetLength Procedure()
  code
    if self.isNull
        return 0
    else
        return 16                                                   ! Length is always 16 octets (excludes any null terminator)
    end



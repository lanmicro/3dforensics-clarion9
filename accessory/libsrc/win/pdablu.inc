!ABCIncludeFile(PDLU)
  OMIT('_EndOfInclude_',_pdLookupPresent_)
_pdLookupPresent_  EQUATE(1)

 include('pdlu.inc'),once
 include('abwindow.inc'),once

PDLookupCT                CLASS(PDLUCT),IMPLEMENTS(WindowComponent),MODULE('PDABLU.CLW'),TYPE,LINK('PDABLU.CLW',_PDLULinkMode_),DLL(_PDLUDllMode_)
!-- Public properties
AskProcedure                BYTE                                     !-- Number of lookup browse.
AddEditAskProcedure         BYTE,PROTECTED                           !-- Number of AddEdit Ask Procedure
!-- Protected Properties
WM                          &WindowManager,PROTECTED                 !-- Window Manager class
LookupMgr                   &FileManager,PROTECTED                   !-- File manager class
!-- Methods: Virtaul
AddItem                     PROCEDURE(SIGNED pAddEditFeq,BYTE pAddEditAskProcedure),VIRTUAL
CallAddEdit                 PROCEDURE(BYTE pAction),BYTE,VIRTUAL
TakeEvent                   PROCEDURE,BYTE,PROC,DERIVED
TakeAddEditEvent            PROCEDURE,VIRTUAL
!-- Methods: Derived
CallLookup                  PROCEDURE,BYTE,DERIVED
FileTryFetch                PROCEDURE,BYTE,PROC,DERIVED
FileReset                   PROCEDURE,DERIVED
Init                        PROCEDURE(WindowManager pWM,<*?  P:ScreenField>,*? P:Lookup,VIEW P_LookupView,FileManager pLookupFile,KEY pLookupKey,<*? P:IDToLookup>,<*? P:IDToSave>,<KEY pIDKey>,<STRING P:Filter>,<ULONG P:Flags>,<SIGNED P:ButtonFEQ>,SIGNED P:Feq,BYTE pAskProcedure=0) !,EXTENDS
Reset                       PROCEDURE(BYTE pForce),DERIVED
WindowReset                 PROCEDURE,DERIVED
WindowUpdate                PROCEDURE,DERIVED
                          END
_EndOfInclude_
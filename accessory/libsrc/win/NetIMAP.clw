! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software  - www.capesoft.com

      Member()

      map
      end

      include('NetImap.inc'), once

!-----------------------------------------------------------
!   NetImap Class
!   Implements an IMAP client
!-----------------------------------------------------------
!-----------------------------------------------------------
NetImap.Construct Procedure()
  code
  self.prefix = 'a001' ! Command prefix - should be modified per object for simultaneous connections!
  self.pending &= new ImapPending()
  self.buffer &= new string(010000h)        ! Allocate a 64KB initial buffer
  self.responses &= new ImapResponseLines()
  self.mailBoxes &= new ImapMailboxes
  self.fields &= new ImapFields
  self.commands &= new ImapCommands
  self._commandBuffer &= new StringTheory
  self._state = NET:IMAP_NOT_CONNECTED
  self.port = NET:IMAP_PORT

!-----------------------------------------------------------
NetImap.Destruct Procedure()
  code
  Dispose(self.buffer)
  ! Queues which should be freed before disposing
  if not self.fields &= null
    self.fields.Free()
  End
  Dispose(self.fields)
  if not self.mailBoxes &= null
    Free(self.mailboxes)
  End
  Dispose(self.mailboxes)
  if not self.commands &= null
    Free(self.commands)
  End
  Dispose(self.commands)
  Dispose(self._commandBuffer)

  ! The following properties contain references within the queue
  ! and need to have the correct method called to dipose them before the queue is freed.
  if not self.pending &= null
    self.FreePending()
  end
  Dispose(self.pending)
  if not self.responses &= null
    self.FreeResponses()
  end
  Dispose(self.responses)

!-----------------------------------------------------------
! Create a new connection to an IMAP server. Once the connection
! is established the TakeConnected method will be called, which
! will call the Login method if a user name and password has been
! set.
NetImap.Connect Procedure(<string server>, <string user>, <string password>, <ushort port>)
  code
  if not Omitted(2) and server <> ''
    self.server = server
  end
  if not Omitted(3)
    self.user = user
  end
  if not Omitted(4) and password <> ''
    self.password = password
  end
  if not Omitted(5) and port <> 0
    self.port = port
  end
  if self.port = 0
    self.port = NET:IMAP_PORT
  end
  self._TryOpen()

!-----------------------------------------------------------
NetImap.FreePending Procedure()
pn              equate('NetImap.FreePending')
i               long
  code
  self.Log(pn, '')
    if self.pending &= null
        return
    end
    loop i = 1 to Records(self.pending)
        Get(self.pending, i)
        if not ErrorCode()
            if not self.pending.data &= null
                Dispose(self.pending.data)
            end
        end
    end
    Free(self.pending)


!-----------------------------------------------------------
NetImap.FreeResponses Procedure()
pn              equate('NetImap.FreeResponses')
i               long
  code
  self.Log(pn, '')
  if self.responses &= null or Records(self.responses) = 0
    return
  end
  loop i = 1 to Records(self.responses)
    Get(self.responses, i)
    if not ErrorCode()
      Dispose(self.responses.text)
    end
  end
  Free (self.responses)

!-----------------------------------------------------------
NetImap.Init Procedure (uLong mode=NET:SimpleClient)
pn              equate('NetImap.Init')
  code
    self.Log (pn, '')

    if self._serverDesc_Set = 0
        self._objectType = NET:IMAP_CLIENT
        self._serverDesc = 'IMAP (Email)'
        self._serverDesc_Set = 1
        if self.Port = 0
            self.Port = NET:IMAP_PORT
        end
    end

    parent.Init(NET:SimpleClient)        ! This is a client

    self.InActiveTimeout = NET:IMAP_IDLE_TIMEOUT            ! IMAP specific timeout (30 minutes - IMAP clients typically remain connected).
    self._state = NET:IMAP_NOT_CONNECTED


!-----------------------------------------------------------
NetImap.OK Procedure ()
  code
    return self.prefix & ' ' & IMAP_OK

!-----------------------------------------------------------
NetImap.NO Procedure()
  code
    return self.prefix & ' ' & IMAP_NO

!-----------------------------------------------------------
NetImap.BAD Procedure()
  code
    return self.prefix & ' ' & IMAP_BAD

!-----------------------------------------------------------
! Handle errors - store the error code and call ErrorTrap
! with the error information.
NetImap.ThrowError Procedure(long errcode, string methodName, <string info>)
  code
  self.lastError = errcode
  self.errorFunc = methodName
  self.cErr = Error()
  self.cErrCode = ErrorCode()
  if Omitted(4)
    self.ErrorTrap('Error ' & errcode & ': ' & self.ErrorMessage(self.lastError), methodName)
  else
    self.ErrorTrap('Error ' & errcode & ': ' & self.ErrorMessage(self.lastError) & ' [' & Clip(info) & ']', methodName)
  end
  return False  ! Always returns False to allow it to be called on return: return self.ThrowError(...)


!-----------------------------------------------------------
NetImap.Process Procedure ()
foundEnd        bool
pn              equate('NetImap.Process')
lineLen         long
s               long
e               long
  code
    self.Log(pn, '')
    case (self.packet.packetType)
    of NET:SimpleIdleConnection
        self.Log(pn, '. NET:SimpleIdleConnection')
        self._CloseIdleConnection()
    of NET:SimpleAsyncOpenSuccessful                        ! Wait for the welcome message from the server.
        self.Log(pn, '. NET:SimpleAsyncOpenSuccessful')
        self._state = NET:IMAP_CONNECTED
        self.FreeCommands()
    of NET:SimplePartialDataPacket
                                                            self.Log(pn, 'Received ' & self.packet.binDataLen & ' bytes from the remote ' & self._ServerDesc & ' Server')
                                                            !self.Log(pn, 'Server Response: ' & self.GetData())
        self.BufferData()
        if self.packet.binDataLen < 2
            return
        end

        ! Process currently received data (line based)
        s = 1
        e = Instring(IMAP_EOL, self.buffer, 1, 1)
        loop while e
            e += 1                                          ! Keep the EOL (otherwise blank lines will be removed which should not be)
            lineLen = e - s + 1
                                                            ! self.Log(pn, 'Process from ' & s & ' to ' & e & '(' & lineLen & ' bytes)')
            if lineLen                                      ! The line contains data
                self.responses.text &= new string(lineLen)
                self.responses.text = self.buffer[s : e]
                Add(self.responses)
            end
            s = e + 1                                       ! Move to after the EOL (start of the next line)
                                                            !self.Log(pn, 'Next line, start at ' & s)
            e = Instring(IMAP_EOL, self.buffer, 1, s)
        end
        if s > 1                                            ! At least one full response received. Remove processed data from the buffer.
                                                            !self.Log(pn, 'Removed Processed data from the buffer. Unprocessed data is ' & s & ' to ' & self.bufferLen & ', total buffer size: ' & Len(self.buffer))
            if s >= self.bufferLen                          ! All data processed
                                                            !self.Log(pn, 'All data processed, set bufferLen to 0')
                Clear(self.buffer)
                self.bufferLen = 0
            else
                                                            !self.Log(pn, 'Keep bytes ' & s & ' to ' & self.bufferLen)
                self.buffer = self.buffer[s : self.bufferLen]
                self.bufferLen = self.bufferLen - s + 1
                                                            !self.Log(pn, 'New buffer len is: ' & self.bufferLen)
            end
            self.ProcessResponses()                         ! Process all parse responses
        end

    end


!-----------------------------------------------------------
! Process all lines in the responses queue
NetImap.ProcessResponses Procedure()
pn              equate('NetImap.ProcessResponses')
i               long
  code
  self.Log(pn, '')
  if not self.CountCommands()                             ! No pending commands, the server just send an informational response
    self.Log(pn, 'No commands were issued, the response was informational')
    if self._state = NET:IMAP_CONNECTED
      self.Log(pn, 'Was a welcome response, check for "* OK"')
      Get(self.responses, 1)
      if ErrorCode() or self.responses.text &= null
        self.ThrowError(NET:IMAP_NO_RESPONSE, pn)
        self.Log(pn, 'complete (a)')
        return
      end
      if Upper(Sub(self.responses.text, 1, Len(IMAP_SERVER_OK))) = IMAP_SERVER_OK
        self._state = NET:IMAP_NO_AUTH      ! Move to the connected but not authenticated state
        self.TakeConnected()
        self.Log(pn, 'complete (b)')
        return
      end
      self.Log(pn, 'Invalid server greeting: ' & self.responses.text)
      self.ThrowError(NET:IMAP_INVALID_GREETING, pn)
    end
    self.TakeResponse()
    self.Log(pn, 'complete (c)')
    return
  end

  loop i = 1 to self.CountCommands()
    self.Log(pn, '  Pending Command: ' & self.GetCommand(i))
  end

  loop i = 1 to Records(self.responses)
    Get(self.responses, i)
    if ErrorCode()
      self.ThrowError(NET:IMAP_NO_RESPONSE, pn)
      cycle
    end
    !self.Log(pn, '  response: ' & self.responses.text)
        ! Check for response-done or response-fatal:
        !response-done   = response-tagged / response-fatal
        !response-fatal  = "*" SP BYE text CRLF
        !                    ; Server closes connection immediately
        !response-tagged = tag SP resp-cond-state CRLF

    if Sub(self.responses.text, 1, 2) = '* '
      !self.Log(pn, 'Untagged response')
      if Upper(Sub(self.packet.binData, 3, 3)) = 'BYE' ! Check for a fatal response
        self.Log(pn, 'response-fatal. Close the connection')
        self.TakeFatal()
        self.Log(pn, 'complete (d)')
        return
      else
        !self.Log(pn, 'Wait for the completion response before processing this line...')
        self.TakeResponseData()                     ! Don't need to do anything here
      end
    elsif Sub(self.responses.text, 1, 2) = '+ '
      self.Log(pn, 'Continuation request: ' & self.responses.text)
      if self.FirstCommand() = IMAP_IDLE              ! IDLE command issues a continuation request when it successful and then waits for the DONE command to termination
        self.TakeCompletion()
      else
        self.TakeContinuation()
      end
      self.Log(pn, 'complete (e)')
      return
    elsif Sub(self.responses.text, 1, Len(self.prefix) + 1) = self.prefix & ' '
      self.Log(pn, 'Tagged response.')
      self.TakeCompletion()
      self.Log(pn, 'complete (f)')
      return
    end
  end
  self.Log(pn, 'complete (g)')
  return

!-----------------------------------------------------------
! Called when the connection to the server has succeeded, prior to
! any authentication/login. The connected was opened and the server
! sent a valid response.
!
NetImap.TakeConnected Procedure()
pn              equate('NetImap.TakeConnected')
  code
  self.Log(pn, '')
  self.FreeResponses()                                    ! Responses processed, clear them.
  self.FreeCommands()
  if self.user and self.password                          ! Login now if credentials have been supplied
    self.Login()
  end

!-----------------------------------------------------------
! Handle responses not related to commands issued etc.
NetImap.TakeResponse Procedure()
pn              equate('NetImap.TakeResponse')
  code
  self.Log(pn, '')
  self.FreeResponses()

!-----------------------------------------------------------
! A completion response
! Response of the form:
! TAG OK text

! Parse out the tag and get the text. This may be a command name, or informational text such as:
! a001 OK Success
! a001 BAD Failure
! a001 NO Something Here

! Nice servers response according to the RFC:
! a001 OK CAPABILITY complete
! a001 OK STATUS complete

! Not so nice servers (Gmail) don't include the command name. Presumably becuase they
! assume that commands will be sent one at time and the responses will be sent in the same
! order, which is not terribly RFC compliant.
NetImap.TakeCompletion Procedure ()
pn              equate('NetImap.TakeCompletion')
i               long
sPos            long
ePos            long
res             ImapFields
  code
  self.Log(pn, 'Called (first command issued was: ' & self.FirstCommand() & ')')

  ! Process all of the response data for this command
  Get(self.responses, Records(self.responses))            ! The response for the command
  if ErrorCode() or self.responses.text &= null
    self.ThrowError(NET:IMAP_NO_RESPONSE, pn)
    return
  end

  self.Log(pn, 'Completion response: ' & self.responses.text)

  ! csTODO: Select the command based on the return result using the split response here. Assuming that that responses are
  ! returned in the order they are issued is reasonable, but not ideal.
  self.Log(pn, 'Split the response')
  self.SplitResponse(self.responses.text, res)
  if self.FirstCommand() = IMAP_IDLE                      ! The IDLE command is "complete" when it issues a completion request
    self.Log(pn, 'The IDLE command was accepted successfully')
    self.DoneExIdle()
    self.FreeResponses()
    self.FreeCommands()
    return
  end
  self.Log(pn, 'Split the command response up. Response length: ' & Len(self.responses.text))
  !self.Log(pn, 'response: ' & self.responses.text)
  sPos = Len(self.prefix) + 2                             ! Start of the Result section of the response after the Tag
  ePos = Instring(' ', self.responses.text, 1, sPos)
  if not ePos
    ePos = Len(self.responses.text) - 2
  else
    ePos -=1
  end
  if ePos < sPos                                          ! Invalid response, missing the result!
    self.ThrowError(NET:IMAP_INVALID_RESPONSE, pn)
    self.FreeResponses()
    self.DeleteCommand(1)
    return
  end

  self.result = self.responses.text[sPos : ePos]
  self.Log(pn, 'Result for the command is: ' & self.result)
  case self.result
  of IMAP_OK
    self.Log(pn, 'OK received, command (' & self.FirstCommand() & ') complete successfully')
  of IMAP_BAD
    self.Log(pn, 'BAD received, the server rejected the command')
  of IMAP_NO
    self.Log(pn, 'NO received, the server refused the command')
  else                                                    ! Invalid response from the server!
    self.Log(pn, 'Invalid server response!')
    self.ThrowError(NET:IMAP_INVALID_RESULT, pn)
    self.FreeResponses()
    self.DeleteCommand(1)
    return
  end

  case self.FirstCommand()
  of IMAP_CAPABILITY
    self.DoneCapability()
  of IMAP_LOGIN
    self.DoneLogin()
  of IMAP_LOGOUT
    self.DoneQuit()
  of IMAP_SELECT
    self.DoneSelect()
  of IMAP_EXAMINE
    self.DoneExamine
  of IMAP_CREATE
    self.DoneCreateMailbox()
  of IMAP_DELETE
    self.DoneDelete()
  of IMAP_RENAME
    self.DoneRename()
  of IMAP_SUBSCRIBE
    self.DoneSubscribe()
  of IMAP_UNSUBSCRIBE
    self.DoneUnsubscribe()
  of IMAP_LIST
    self.DoneList()
  of IMAP_LSUB
    self.DoneLSub()
  of IMAP_STATUS
    self.DoneStatus()
  of IMAP_APPEND
    self.DoneAppend()
  of IMAP_CHECK
    self.DoneCheck()
  of IMAP_CLOSE
    self.DoneCloseMailbox()
  of IMAP_EXPUNGE
    self.DoneExpunge()
  of IMAP_SEARCH
    self.DoneSearch()
  of IMAP_FETCH
    self.DoneFetch()
  of IMAP_STORE
    self.DoneStore()
  of IMAP_UID
    self.DoneUID()
  of IMAP_COPY
    self.DoneCopy()
  of IMAP_DONE
    self.DoneExDone()
  of IMAP_XLIST
    self.DoneXList
  end
  self.FreeResponses()
  self.DeleteCommand(1)
  self.Log(pn, 'complete')
  return

!-----------------------------------------------------------
! Virtual notification method - response data received
NetImap.TakeResponseData Procedure ()
pn              equate('NetImap.TakeResponseData')
  code



!-----------------------------------------------------------
! A continuation request was received
NetImap.TakeContinuation Procedure ()
pn              equate('NetImap.TakeContinuation')
  code
    self.Log(pn, 'Send any pending data')
    self.SendPending()


!-----------------------------------------------------------
! Handle the server response: '* BYE<13,10> followed by a disconnection
! This callback allows any data to be checked or used before it
NetImap.TakeFatal Procedure()
  code
    self.Abort()                                            ! Just close the connection
    self.FreePending()                                      ! Free any pending data
    self.FreeResponses()                                    ! Clean up
    self.FreeCommands()


!-----------------------------------------------------------
! Delete the currently selected record from the responses queue property
NetImap.DeleteResponse Procedure()
pn              equate('NetImap.DeleteResponse')
  code
    self.Log(pn, '')
    if self.responses &= null
        return
    end
    if not self.responses.text &= null
        Dispose(self.responses.text)
    end
    Delete(self.responses)


NetImap.DeleteLastResponse procedure()
  code
    Get(self.responses, Records(self.responses))
    if not ErrorCode()
        self.DeleteResponse()
    end



!-----------------------------------------------------------
! Store the received data in the response.text property. Allocate
! additional memory as required
NetImap.BufferData Procedure()
reqLen          long
oldData         &string
  code
    if self.packet.binDataLen <= 0
        return
    end
    reqLen = self.bufferLen + self.packet.binDataLen     ! Required buffer size
    if reqLen > Len(self.buffer)                         ! Allocate additional memory
        oldData &= self.buffer
        self.buffer &= new string(reqLen + 040000h)      ! Allocate a new string that is large enough, plus 256KB
        self.buffer = oldData
        Dispose(oldData)
    end

    self.buffer[self.bufferLen + 1: reqLen] = self.packet.binData [1 : self.packet.binDataLen]
    self.bufferLen = reqLen


!-----------------------------------------------------------
NetImap.GetBuffer Procedure()
  code
  if self.bufferLen > 0 and not self.buffer &= null
    return self.buffer[1 : self.bufferLen]
  else
    return ''
  end

!-----------------------------------------------------------
NetImap.LastCommand Procedure()!, string, virtual
  code
  Get(self.commands, 1)
  if not ErrorCode()
    return clip(self.commands.cmd)
  else
    return ''
  end

!-----------------------------------------------------------
NetImap.FirstCommand Procedure()!, string, virtual
  code
  Get(self.commands, Records(self.commands))
  if not ErrorCode()
    return clip(self.commands.cmd)
  else
   return ''
  end

!-----------------------------------------------------------
NetImap.CountCommands Procedure()!, string, virtual
  code
  return Records(self.commands)

NetImap.FreeCommands Procedure()
  code
  Free(self.commands)

!-----------------------------------------------------------
! Send a command (or buffer it if BufferCommands has been called).
! Adds the command to the commands queue to allow it to be check
! and processed when a response is received.
! Returns True for success or False for failure.
NetImap.SendCommand Procedure(string command, <string arguments>, bool noprefix = false)
pn              equate('NetImap.SendCommand')
prefix          like(self.prefix)
  code
  self.Log(pn, 'command: ' & Clip(command) & ' (' & Clip(arguments) & ')')
  self.AddCommand(command, arguments) ! Store the command for response processing, along with the first 256 bytes of the argument.
  if self._bufferCommands                                 ! Buffer the command for later sending
    if Omitted(3)
      self._commandBuffer.AppendA(self.prefix & ' ' & Clip(command) & IMAP_EOL)
    else
      self._commandBuffer.AppendA(self.prefix & ' ' & Clip(command) & ' ' & Clip(arguments) & IMAP_EOL)
    end
  else
    if Omitted(3)
      self.SetPacketData(self.prefix & ' ' & Clip(command) & IMAP_EOL)
    else
      self.SetPacketData(self.prefix & ' ' & Clip(command) & ' ' & Clip(arguments) & IMAP_EOL)
    end
    self.Send()
  end
  return true

!-----------------------------------------------------------
! Add a command to the command queue. Allows for later processing and checking
! returned data against the list of command issued.
NetImap.AddCommand Procedure(string command, string arguments)
  code
  self.commands.cmd = command
  self.commands.args = arguments
  self.commands.issued = Clock()                          ! Timestamp each command
  Add(self.commands)

! Remove a command for the command queue, typically once it
! has received  response.
NetImap.DeleteCommand Procedure(long pos = 0)
  code
  if pos
    Get(self.commands, pos)
    if not ErrorCode()
      Delete(self.commands)
    end
  else
    Delete(self.commands)
  end

NetImap.GetCommand Procedure(long pos)
  code
  Get(self.commands, pos)
  if not ErrorCode()
    return clip(self.commands.cmd)
  end

! Buffer the commands created by SendCommand so that they can be sent in
! a single block. When issuing a number of commands rapidly this is more
! efficient and reliable than simply looping and sending a large number of
! commands. For example when using STATUS to fetch information about a number
! of folders.
NetImap.BufferCommands Procedure()
pn          equate('NetImap.BufferCommands')
  code
  self.Log(pn, '')
  self._bufferCommands = true

! Flush all pending commands
NetImap.FlushCommands Procedure()
pn          equate('NetImap.FlushCommands')
  code
  self.Log(pn, '')
  self._bufferCommands = false                      ! Turn off buffering
  self.SendAllData(self._commandBuffer.value)       ! Send all in buffer (in as many packets as needed)
  self._commandBuffer.Free()                        ! Release memory

!-----------------------------------------------------------
!
!           Basic methods, simply wrap up the sending
!                of core IMAP commands
!-----------------------------------------------------------

!-----------------------------------------------------------
! Client Commands - Any State:
! CAPABILITY, NOOP, LOGOUT
!-----------------------------------------------------------


!-----------------------------------------------------------
NetImap.Capability Procedure ()
  code
    return self.SendCommand(IMAP_CAPABILITY)


!-----------------------------------------------------------
NetImap.Noop Procedure ()
  code
    return self.SendCommand(IMAP_NOOP)


!-----------------------------------------------------------
! Logout is a reserved word in Clarion so the method is simply
! called Quit.
! "$ LOGOUT"
NetImap.Quit Procedure ()
  code
    return self.SendCommand(IMAP_LOGOUT)

!-----------------------------------------------------------
! Client Commands - Not Authenticated State
! STARTTLS, AUTHENTICATE, LOGIN
!-----------------------------------------------------------

!-----------------------------------------------------------
NetImap.StartTLS Procedure ()
  code
    return self.SendCommand(IMAP_STARTTLS)


!-----------------------------------------------------------
NetImap.Authenticate Procedure (string authMechanism)
  code
    return self.SendCommand(IMAP_AUTHENTICATE, authMechanism)


!-----------------------------------------------------------
! "$ LOGIN username password"
NetImap.Login Procedure (<string user>, <string password>)
  code
    if not Omitted(2)
        self.user = user
    end
    if not Omitted(3)
        self.password = password
    end
    return self.SendCommand(IMAP_LOGIN, Clip(self.user) & ' ' & Clip(self.password))


!-----------------------------------------------------------
! Client Commands - Authenticated State
! SELECT, EXAMINE, CREATE, DELETE, RENAME, SUBSCRIBE,
! UNSUBSCRIBE, LIST, LSUB, STATUS, and APPEND.
!-----------------------------------------------------------

!-----------------------------------------------------------
NetImap.Select Procedure (string mailbox)
  code
    return self.SendCommand(IMAP_SELECT, '"' & Clip(mailbox) & '"')


!-----------------------------------------------------------
NetImap.Examine Procedure (string mailbox)
  code
    return self.SendCommand(IMAP_EXAMINE,'"' &  Clip(mailbox) & '"')


!-----------------------------------------------------------
NetImap.CreateMailbox Procedure (string mailbox)
  code
    return self.SendCommand(IMAP_CREATE, '"' & Clip(mailbox) & '"')


!-----------------------------------------------------------
NetImap.Delete Procedure (string mailbox)
  code
    return self.SendCommand(IMAP_DELETE, '"' & Clip(mailbox) & '"')


!-----------------------------------------------------------
NetImap.Rename Procedure (string mailbox, string newName)
  code
    return self.SendCommand(IMAP_RENAME, '"' & Clip(mailbox) & '"')


!-----------------------------------------------------------
NetImap.Subscribe Procedure (string mailbox)
  code
    return self.SendCommand(IMAP_SUBSCRIBE, '"' & Clip(mailbox) & '"')


!-----------------------------------------------------------
NetImap.Unsubscribe Procedure (string mailbox)
  code
    return self.SendCommand(IMAP_UNSUBSCRIBE, '"' & Clip(mailbox) & '"')


!-----------------------------------------------------------
NetImap.List Procedure (string reference, string mailbox)
  code
    return self.SendCommand(IMAP_LIST, '"' & Clip(reference) & '" "' & Clip(mailbox) & '"')


!-----------------------------------------------------------
NetImap.LSub Procedure (string reference, string mailbox)
  code
  return self.SendCommand(IMAP_LIST, '"' & Clip(reference) & '" "' & Clip(mailbox) & '"')

!-----------------------------------------------------------
NetImap.Status Procedure (string mailbox, string statusItems)
  code
  return self.SendCommand(IMAP_STATUS, '"' & Clip(mailbox) & '" (' & Clip(statusItems) & ')')

!-----------------------------------------------------------
! Parameters
! mailbox name
! OPTIONAL flag parenthesized list
! OPTIONAL date/time string
! message literal - number of bytes that will be sent for the message
!
! Note: this method is asynchronous - the server will send a continuation
! request when it is ready to receive the data (at which point the actual
! message data will be sent).
NetImap.Append Procedure (string mailbox, <string flags>, <string dateTime>, *string msgData)
pn              equate('NetImap.Append')
  code
  self.Log(pn, '')
  if not self.StorePending(msgData)                       ! store the message data
    return false
  end
  self.SendCommand(IMAP_APPEND,  '"' & Clip(mailbox) & '"')
  return false

!-----------------------------------------------------------
! Store the pending data. Data is clipped (all data being sent can be
! treated as text)
!
! Return value:
!   True is successful and False if an error occurs.
NetImap.StorePending Procedure(*string pData)!, bool, virtual
pn              equate('NetImap.StorePending')
dataLen         long
  code
  self.Log(pn, '')
  dataLen = Len(Clip(pData))
  if dataLen
    self.pending.data &= new string(dataLen)
    self.pending.data = Clip(pData)
    Add(self.pending)
  else
    self.ThrowError(NET:IMAP_NODATA_PASSED, pn)
    return false
  end

!-----------------------------------------------------------
! Send the first record in the pending queue
! Return value:
!   True is successful and False if an error occurs.
NetImap.SendPending Procedure()!, bool, virtual
pn              equate('NetImap.SendPending')
dataLen         long
  code
  self.Log(pn, '')
  Get(self.pending, 1)
  if ErrorCode()
    self.ThrowError(NET:IMAP_NODATA_TO_SEND, pn)
    return false
  end
  self.SendAllData(self.pending.data)
  return true

!-----------------------------------------------------------
! Client Commands - Selected State
! CHECK, CLOSE, EXPUNGE, SEARCH, FETCH, STORE, COPY, and UID.
!-----------------------------------------------------------
NetImap.Check Procedure ()
  code
  return self.SendCommand(IMAP_CHECK)

!-----------------------------------------------------------
NetImap.CloseMailbox Procedure ()
  code
  return self.SendCommand(IMAP_CLOSE)

!-----------------------------------------------------------
NetImap.Expunge    Procedure ()
  code
  return self.SendCommand(IMAP_EXPUNGE)

!-----------------------------------------------------------
NetImap.Search Procedure (<string charset>, string search)
  code
  if Omitted(2)
    return self.SendCommand(IMAP_SEARCH, Clip(search))
  else
    return self.SendCommand(IMAP_SEARCH, 'CHARSET ' & Clip(charset) & ' ' & Clip(search))
  end

!-----------------------------------------------------------
NetImap.Fetch Procedure (string sequenceSet, string itemNames)
  code
  !return self.SendCommand(IMAP_FETCH, Clip(sequenceSet) & ' (' & Clip(itemNames) & ')')
  !bruce removed the brackets after testing with MT's server. 6.32
  return self.SendCommand(IMAP_FETCH, Clip(sequenceSet) & ' ' & Clip(itemNames) & '')

!-----------------------------------------------------------
NetImap.Store Procedure (string sequenceSet, string itemName, string itemValue)
  code
  return self.SendCommand(IMAP_STORE, Clip(sequenceSet) & '(' & Clip(itemName) & ')')
  !return false

!-----------------------------------------------------------
NetImap.Copy Procedure (string sequenceSet, string mailbox)
  code
  !return self.SendCommand(IMAP_COPY, Clip(sequenceSet) & '(' & Clip(itemNames) & ')')
  return false

!-----------------------------------------------------------
NetImap.Uid Procedure (string commandName, string commandArguments)
  code
    !return self.SendCommand(IMAP_UID, Clip(sequenceSet) & '(' & Clip(itemNames) & ')')
    return false


!-----------------------------------------------------------
! eXperimental commands
!-----------------------------------------------------------


!-----------------------------------------------------------
NetImap.xList Procedure (string reference, string mailbox)
  code
    return self.SendCommand(IMAP_XLIST, Clip(reference) & ' ' & Clip(mailbox))

!-----------------------------------------------------------
!
!         Extensions to the IMAPv4rev1 protocol
!
!-----------------------------------------------------------

!-----------------------------------------------------------
NetImap.exIdle Procedure ()
  code
  return false

!-----------------------------------------------------------
NetImap.exDone Procedure ()
  code
  return false

!-----------------------------------------------------------
! Completion event methods
!-----------------------------------------------------------

!-----------------------------------------------------------
NetImap.DoneCapability          Procedure ()
pn              equate('NetImap.DoneCapability')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneQuit                Procedure ()
pn              equate('NetImap.DoneQuit')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneStartTLS            Procedure ()
pn              equate('NetImap.DoneStartTLS')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneAuthenticate        Procedure ()
pn              equate('NetImap.DoneAuthenticate')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneLogin               Procedure ()
pn              equate('NetImap.DoneLogin')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneSelect              Procedure ()
pn              equate('NetImap.DoneSelect')
  code
    self.Log(pn, '')
    self._state = IMAP_SELECT


!-----------------------------------------------------------
NetImap.DoneExamine             Procedure ()
pn              equate('NetImap.DoneExamine')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneCreateMailbox       Procedure ()
pn              equate('NetImap.DoneCreateMailbox')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneDelete              Procedure ()
pn              equate('NetImap.DoneDelete')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneRename              Procedure ()
pn              equate('NetImap.DoneRename')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneSubscribe           Procedure ()
pn              equate('NetImap.DoneSubscribe')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneUnsubscribe         Procedure ()
pn              equate('NetImap.DoneUnsubscribe')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
! csTODO: Can use the SplitResponse method to parse this
! without the string handling within this method.
NetImap.DoneList                Procedure ()
pn              equate('NetImap.DoneList')
i               long
recs            long
sPos            long
ePos            long

eLen            long                                        ! Expected length - when pased a literal in a response
curLen          long
  code
    self.Log(pn, '')

    i = Records(self.responses)
                                                            self.Log(pn, 'Process ' & i & ' reponse records')
    Get(self.responses, i)
                                                            self.Log(pn, 'Remove the command completion response: ' & self.responses.text)
    self.DeleteResponse()

    ! List reponses contains one or more attributes in brackets
    ! * LIST (/attribute) "ref" "name"
    ! * LIST (/attribute) "ref" {7}
    Free(self.mailboxes)

    ! Process all other responses for this command
    loop i = 1 to Records(self.responses)
        Clear(self.mailboxes)
        Get(self.responses, i)
        if self.responses.text &= null or Len(Clip(self.responses.text)) < 8
            self.Log(pn, 'response skipped, it was null or too short')
            cycle
        end

        ! Get the attributes
        ! csTODO: Support any number of attributes
        sPos = Instring('(', self.responses.text, 1, 1)
        ePos = Instring(')', self.responses.text, 1, sPos + 1)
        if not sPos or not ePos
            self.Log(pn, 'Invalid response skipped, it did not contain an Attributes section: ' & Clip(self.responses.text))
            cycle
        end
        sPos += 1
        ePos -=1
        if ePos >= sPos
            self.mailboxes.attributes = self.responses.text[sPos : ePos]
        end

        ! Get the reference
        sPos = Instring('"', self.responses.text, 1, ePos + 2)
        ePos = Instring('"', self.responses.text, 1, sPos + 1)
        if not sPos or not ePos
            self.Log(pn, 'Invalid response skipped, it did not contain a Reference section: ' & Clip(self.responses.text))
            cycle
        end
        sPos += 1
        ePos -=1
        if ePos >= sPos
            self.mailboxes.reference = self.responses.text[sPos : ePos]
        end

        ! Get the name
        sPos = ePos + 2                                     ! Find the next non whitespace char...
        loop while self.responses.text[sPos] = ' ' and sPos < Len(self.responses.text)
            sPos += 1
        end
        case self.responses.text[sPos]
        of '"'                                              ! Quoted name (the generally expected value)
            ePos = Instring('"', self.responses.text, 1, sPos + 1)
            if not ePos
                self.Log(pn, 'Invalid responses, the quote name did not have a terminating quote!')
                cycle
            end
            sPos += 1
            ePos -= 1

            if ePos >= sPos
                self.mailboxes.name = self.responses.text[sPos : ePos]
            end
        of '{{'                                             ! Handle literal field
                                                            self.Log(pn, 'The name field is a literal, get the value')
            sPos += 1
            ePos = Instring('}', self.responses.text)
            if not ePos
                self.Log(pn, 'Invalid responses, the string literal was unterminated!')
                cycle
            end

            eLen = self.responses.text[sPos : ePos - 1]
                                                            self.Log(pn, 'Literal length received is: ' & eLen)
            if eLen > Size(self.mailboxes.name)
                self.Log(pn, 'The server indicated that the mailbox name is: ' & eLen & ', bytes long, which is greater than the maximum supported length of ' & Size(self.mailboxes.name))
                eLen = Size(self.mailboxes.name)
            end
                                                            self.Log(pn, 'Fetch ' & eLen & ' bytes of data for the name field')
            ! Need to loop and get the specified number of bytes of data
            loop while curLen < eLen
                i += 1
                                                            self.Log(pn, 'Get the next response record at position: ' & i)
                Get(self.mailboxes, i)
                if ErrorCode()
                                                            self.Log(pn, 'Error fetching the response record: ' & Error())
                    break
                end
                curLen += Len(self.responses.text)
                self.mailboxes.name = Clip(self.mailboxes.name) & Clip(self.responses.text)
            end
            if not self.mailboxes.name
                self.ThrowError(NET:IMAP_INVALID_LIST, pn)
                cycle
            end
        else                                                ! Either INBOX, blank, or an unquoted string
                                                            self.Log(pn, 'Unquoted name')
            !ePos = Len(Clip(self.responses.text))
            ePos = Instring('<13,10>', self.responses.text, 1, sPos + 1) - 1 !bj
            if epos <= 0
              ePos = Len(Clip(self.responses.text))
            end                                                          !bj
            if ePos >= sPos
                self.mailboxes.name = Left(self.responses.text[sPos : ePos])
            else
                self.ThrowError(NET:IMAP_INVALID_LIST, pn)
                cycle
            end
        end

        self.mailboxes.nocase = Upper(self.mailboxes.name)
        Add(self.mailboxes)
        if Errorcode()
            self.ThrowError(NET:IMAP_CANNOT_ADD_MAILBOX, pn)
        end
    end


!-----------------------------------------------------------
! Tokenize (split) an individual response
! A space is used as a delimiter between fields, except where
! the space occurs wrapped in brackets, curly braces or quotation marks
NetImap.SplitResponse Procedure(string str, *ImapFields vals, bool recurse=false)
pn              equate('NetImap.SplitResponse')
i               long
sPos            long
ePos            long
strLen          long
delim           string(1)
tc              string(1)
cDelim          long          ! Number of opening delimeter characters (handling of nested sections)
!field           &string
lPos            long
lEnd            long
  code

  self._trace('=====================')
  self.trace(clip(str))
  self._trace('=====================')


    self.Log(pn, '')
    strLen = Len(Clip(str))
    sPos = 1
    ePos = 1
    loop i = 1 to strLen
        case str[i]
        of '['                                              ! A reponse within pair of square brackets (can contain parameter lists etc.): [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen \*)]
            tc = ']'
            vals.type = NET:IMAP_RESP
            Do GetDelim
        of '('                                              ! A parameter list - a list of one or more values seperated by spaces. Indicated by a pair of brackets: (\Answered \Flagged \Draft \Deleted \Seen)
            tc = ')'
            vals.type = NET:IMAP_PARAM                      ! The SplitParams method can be used to split this up
            Do GetDelim
        of '{{'                                             ! A literal - numeric values in curly braces such as {223}
            tc = '}'
            vals.type = NET:IMAP_LITERAL
            Do GetDelim

        of '"'                                              ! A quoted string
            tc = '"'
            vals.type = NET:IMAP_QUOTE
            Do GetDelim
        of ' '                                              ! A simple string
            if i <= sPos                                    ! Just the space after the last processed item
                cycle
            elsif str[sPos : i - 1] = ''                    ! Ignore empty strings (they should not occur)
                                                            !self.Log(pn, 'Ignore empty strings')
                cycle
            end
                                                            !self.Log(pn, 'Found a standard string')
            vals.Insert(str[sPos : i - 1], NET:IMAP_STRING)
                                                            self.Log(pn, 'Add part (type ' & vals.type & '): ' & vals.value)
            sPos = i + 1
        end
    end
    if sPos <= strLen                                       ! The last block may be a simple string (if anything has yet to be processed)
                                                            !self.Log(pn, 'Add the last block from ' & sPos & 'to ' & strLen)
        vals.Insert(str[sPos : strLen], NET:IMAP_STRING)
                                                            self.Log(pn, 'Add part (type ' & vals.type & '): ' & vals.value)
    end

    return true


! Look for tc (the terminating char) in the string. For each character that
! matching opening char (delim) increment the count. This handles nesting
! of the delimiters - for example brackets within quotation marks within
! a bracketed section:
! FETCH (ENVELOPE ("Mon, 27 Sep 2004 03:38:16 -0700 (PDT)" "Hello World"))

! Note that there is one exception - in the case of a literal being specified for the server
! return data the section will be unterminated, as the terminator will only
! be sent at the end of the data, for example:
! FETCH (RC822 {14448}
GetDelim routine
    delim = str[i]                                          ! Store the delimiter
    i += 1
    cDelim = 1

    ePos = i
    loop while ePos <= strLen
        if str[ePos] = tc                                   ! Found a terminator
            cDelim -= 1
            if cDelim = 0                                   ! Found the end of the section (same number of opening and closing chars)
                break
            end
        elsif str[ePos] = delim                             ! Nested opening character
            cDelim += 1
        end
        ePos += 1
    end

    if ePos > strLen
                                                            self.Log(pn, 'Unterminated string, check for literal')
        ! Check for a literal in this section
        lPos = Instring('{{', str, 1, i)
        if lPos
                                                            self.Log(pn, 'Section contains a literal, treat it like a normal parameter section')
            vals.Insert(Clip(str[i : strLen]), NET:IMAP_PARAM)
                                                            self.Log(pn, 'Add part (type ' & vals.type & '): ' & vals.value)
            return true
        end
        return self.ThrowError(NET:IMAP_INVALID_RESPONSE, pn, 'Unterminated section')
    end

    ePos -= 1
    if ePos > i
        if vals.type = NET:IMAP_PARAM and recurse
                                                            self.Log(pn, 'Recurse and split the parameter list up')
            self.SplitResponse(vals.value, vals, true)
        else
            vals.Insert(str[i : ePos], NET:IMAP_PARAM)
                                                            self.Log(pn, 'Add part (type ' & vals.type & '): ' & vals.value)
        end
    end
    i = ePos + 1
    sPos = i + 1


!-----------------------------------------------------------
! Splits a basic parameter list, which is seperated by spaces.
! The "pairs" parameter indicates that they should be processed
! as NAME VALUE pairs rather than just seperate parameters
NetImap.SplitParams Procedure(*string str, *ImapParams params, bool pairs = false)
pn              equate('NetImap.SplitParams')
st              StringTheory
cur             long
  code
    Free(params)
    st.SetValue(str)
    st.Split(' ')

    ! Split into pairs
    loop cur = 1 to Records(st.lines)
        Get(st.lines, cur)                                  ! Get the first part of the pair, the "name"
        if ErrorCode()
            break
        end
        Clear(params)
        params.name = st.lines.line

        if pairs
            cur += 1
            Get(st.lines, cur)                              ! Get the value
            if not ErrorCode()
                params.value = st.lines.line
            end
        end
        Add(params)
    end
    st.FreeLines()
    st.Free()

!-----------------------------------------------------------
! Split the response of the STATUS command.
! Format is:
! * STATUS "MailBoxName" (ATTRIBUTES X...)
! Where _ATTRIBUTE_ can be "MESSAGES", "UNSEEN" or "RECENT" etc.
! If mStatus is passed then then the values are stored in it,
! otherwise self.status is updated.
!
! If there is a mailboxes record with a matching name, then that
! mailbox is updated with the statistics
NetImap.ParseStatus Procedure(string sr, <*ImapStatus mStatus>)
pn              equate('NetImap.ParseStatus')
vals            ImapFields
params          queue(ImapParams)        ! For splitting up parameters
                end
p               long
stat            &ImapStatus
haveMailbox     bool
  code
    self.Log(pn, '')
    if Omitted(3)
        stat &= self.status
    else
        stat &= mStatus
    end

    Clear(stat)

    self.SplitResponse(sr, vals)
                                                            self.Log(pn, 'Status response split into: ' & vals.Count() & ' items')
    if vals.Count() < 4
        self.ThrowError(NET:IMAP_INVALID_STATUS, pn, 'Too short or incomplete')
        return False
    end

    vals.Fetch(2)
    if Upper(vals.value) <> 'STATUS'
        self.ThrowError(NET:IMAP_INVALID_STATUS, pn, 'Not a STATUS response')
        return False
    end

    vals.Fetch(3)                                           ! Mailbox name
    stat.mailbox = vals.value
    self.mailboxes.name = stat.mailbox
    Get(self.mailboxes, self.mailboxes.name)
    if not ErrorCode()
        haveMailbox = true
    end

    vals.Fetch(4)
    self.SplitParams(vals.value, params, true)              ! Split into name/value pairs
    loop p = 1 to Records(params)
        Get(params, p)
        case Upper(params.name)
        of IMAP_MESSAGES
            stat.messages = params.value
            if haveMailbox
                self.mailboxes.messages = stat.messages
            end
        of IMAP_RECENT
            stat.recent = params.value
            if haveMailbox
                self.mailboxes.recent = stat.recent
            end
        of IMAP_UIDNEXT
            stat.uidnext = params.value
            if haveMailbox
                self.mailboxes.uidnext = stat.uidnext
            end
        of IMAP_UIDVALIDITY
            stat.uidvalidity = params.value
            if haveMailbox
                self.mailboxes.uidvalidity = stat.uidvalidity
            end
        of IMAP_UNSEEN
            stat.unseen = params.value
            if haveMailbox
                self.mailboxes.unseen = stat.unseen
            end
        end
    end

    if haveMailbox
        Put(self.mailboxes)
    end

    return True


!-----------------------------------------------------------
NetImap.DoneLSub Procedure ()
pn              equate('NetImap.DoneLSub')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneStatus Procedure ()
pn              equate('NetImap.DoneStatus')
  code
    self.Log(pn, '')
                                                            self.Log(pn, 'Response queue contains: ' & Records(self.responses) & ' response records')
    if Records(self.responses) < 2
        return self.ThrowError(NET:IMAP_INVALID_RESPONSE, pn, 'Too few Response entries')
    end
    ! Queue will contain (at least) two items: the actual status followed by the response state (OK in this case)
    self.DeleteLastResponse()
    Get(self.responses, Records(self.responses))
    if ErrorCode() or self.responses.text &= null
        return self.ThrowError(NET:IMAP_NO_RESPONSE, pn)
    end
                                                            self.Log(pn, 'Status to parse: ' & self.responses.text)
    self.ParseStatus(self.responses.text)



!-----------------------------------------------------------
NetImap.DoneAppend              Procedure ()
pn              equate('NetImap.DoneAppend')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneCheck               Procedure ()
pn              equate('NetImap.DoneCheck')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneCloseMailbox        Procedure ()
pn              equate('NetImap.DoneCloseMailbox')
  code
    self.Log(pn, '')


!-----------------------------------------------------------
NetImap.DoneExpunge             Procedure ()
pn              equate('NetImap.DoneExpunge')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.DoneSearch              Procedure ()
pn              equate('NetImap.DoneSearch')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.DoneFetch               Procedure ()
pn              equate('NetImap.DoneFetch')
i               long
fnum            long
!messageNumber   long
fetchType       cstring(32)                                 ! Type of the FETCH data (the argument that this is in response to), such as BODY, ENVELOPE, TEXT etc.
fetchResponses  ImapFields                                  ! The fetch split into an argument (the type of fetch) and the response data for that argument
fetchFields     ImapFields                                  ! Each fetch argument response split into the individual fields
messageNumber   long
r               long
  code
  self.Log(pn, '')
  self.Log(pn, 'Response queue contains: ' & Records(self.responses) & ' response records')
  if Records(self.responses) < 2
      return self.ThrowError(NET:IMAP_INVALID_RESPONSE, pn, 'Too few Response entries')
  end
  self.DeleteLastResponse()                               ! Remove the OK completion response
  self.Log(pn, 'Process all responses')
  r = Records(self.responses)
  loop i = 1 to r                   ! Loop through the rest of the responses
    Get(self.responses, i)
    if ErrorCode() or self.responses.text &= null
      !self.ThrowError(NET:IMAP_NO_RESPONSE, pn, 'r=' & r & ' i=' & i)
      cycle
    end
    self.Log(pn, 'Parse FETCH response: ' & self.responses.text)

    ! FETCH can return multiple sections based on the arguments passed.
    ! For example it might return ENVELOPE () FLAGS "" INTERNALDATE ""...
    ! Which needs to be split into three parts and then processed.
    self.fields.Free()
    self.SplitResponse(self.responses.text, self.fields)! Non recursive split

    if not self.fields.Fetch(2)                         ! The message number
      self.ThrowError(NET:IMAP_INVALID_FETCH,pn, 'Missing message number')
      cycle
    end
    messageNumber = self.fields.value

    if not self.fields.Fetch(4)
      return self.ThrowError(NET:IMAP_INVALID_FETCH,pn, 'No data after the FETCH response')
    end
    self.SplitResponse(self.fields.value, fetchResponses)

                ! fetchResponses now contains fetch arguments (such as ENVELOPE,
                ! INTERNALDATE and SIZE) followed by the data for that argument
                self.log(pn,'Bruce: fetchResponses.Count()=' & fetchResponses.Count())
    loop fnum = 1 to fetchResponses.Count() by 2
      fetchResponses.Fetch(fnum)
      fetchType = Left(fetchResponses.value)
      if FetchType = '' or FetchType = '<13,10>' then cycle. ! bruce 17/7/2013
      self.Log(pn,'FETCH type: [' & clip(fetchType) &']')
      if not fetchResponses.Fetch(fNum + 1)
        self.ThrowError(NET:IMAP_INVALID_FETCH,pn, 'The FETCH for argument "' & fetchType & '" did not contain a value and was skipped')
        cycle
      end

      case fetchType                                  ! Split the fetch data up for data which returns parenthesized lists.
      of IMAP_ENVELOPE orof IMAP_FLAGS orof IMAP_INTERNALDATE orof IMAP_UID |
      orof IMAP_BODYSTRUCTURE orof IMAP_MIME orof IMAP_RFC822_SIZE
        self.Log(pn, 'FETCH type returns a paramatized list, parse the response')
        fetchFields.Free()
        self.SplitResponse(fetchResponses.value, fetchFields)
      else
        self.Log(pn, 'FETCH type is not paramatized. Check for a literal size.')
        ! If the response contains a literal, then the next response is the one that contains the data.
        if fetchResponses.FetchType(4)
          self.Log(pn, 'A literal size was specified: ' & fetchResponses.value)
          i += 1
          Get(self.responses, i)
          if ErrorCode() or self.responses.text &= null
            return self.ThrowError(NET:IMAP_NO_RESPONSE, pn, 'Literal size was specified, but no data was received')
          end
        end
      end
      case fetchType
      of IMAP_HEADER      !
        self.TakeFetchHeader(fetchFields, messageNumber)
      of IMAP_MIME
        self.TakeFetchMime(fetchFields, messageNumber)
      of IMAP_BODYSTRUCTURE
        self.TakeFetchBodyStructure(fetchFields, messageNumber)
      of IMAP_ENVELOPE
        self.TakeFetchEnvelope(fetchFields, messageNumber)
      of IMAP_FLAGS
        self.TakeFetchFlags(fetchFields, messageNumber)
      of IMAP_INTERNALDATE
        self.TakeFetchInternalDate(fetchFields, messageNumber)
      of IMAP_RFC822_SIZE
        self.TakeFetchMailSize(fetchFields, messageNumber)
      of IMAP_UID
        self.TakeFetchUid(fetchFields, messageNumber)
      of IMAP_BODY        ! And IMAP_BODY_PEEK
        self.TakeFetchBody(messageNumber)
      of IMAP_TEXT
        self.TakeFetchText(messageNumber)
      of IMAP_RFC822
        self.TakeFetchMail(messageNumber)
      of IMAP_RFC822_HEADER
        self.TakeFetchMailHeader(messageNumber)
      of IMAP_RFC822_TEXT
        self.TakeFetchMailText(messageNumber)
      else                                            ! Unknown type, may be handled by the user
        self.TakeFetchOther(messageNumber)
      end
    end
  end
  self.log(pn,'Complete')

NetImap.GetFieldValue Procedure(*ImapFields fields, long pos, *string pv, bool isImapAddress = false, long numFields=1)
i               long
fVal            StringTheory
pn              equate('NetImap.GetFieldValue')
  code
    Clear(pv)
    if isImapAddress                                    ! An ImapAddress, which is four sections
        if fields.Fetch(pos)
            pv = self.AddressToEmail(fields.value)
        end
    elsif numFields > 1                                 ! Multi field fetch
        loop i = pos to pos + numFields - 1
            if fields.Fetch(i)
                fVal.AppendA(fields.value)
            end
        end
        pv = fVal.value
    else                                                ! Fetch a single value
        if fields.Fetch(pos)
            pv = fields.value
        end
    end

! Convert an ImapAddress string to a standard email address of the form:
! "Person Name" <name@domain.com>
! ("Support" NIL "support" "capesoft.com")
NetImap.AddressToEmail Procedure(*string imapAddr)
st          StringTheory
pn          equate('NetImap.AddressToEmail')
  code
    self.Log(pn, ' Parse address: ' & imapAddr)
    if imapAddr[1] = '('                                    ! Remove brackets
        st.SetValue(Sub(imapAddr, 2, Len(imapAddr) - 2))
    else
        st.Assign(imapAddr)
    end
                                                            self.Log(pn, ' Split: ' & st.value)
    st.Split(' ', '"', '"', true)                           ! Split and strip quotes
    if st.Records() < 4
        return ''
    end

    if st.GetLine(1) <> 'NIL'                               ! Contains a name as well as the email address
        return '"' & st.GetLine(1) & '" <<' & st.GetLine(3) & '@' & st.GetLine(4) & '>'
    else
        return st.GetLine(3) & '@' & st.GetLine(4)
    end
  return ''
!-----------------------------------------------------------
NetImap.TakeFetchBody Procedure(long messageNumber)
pn              equate('Imap.TakeFetchBody')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchHeader Procedure(*ImapFields fields, long messageNumber)
pn              equate('Imap.TakeFetchHeader')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchMime Procedure(*ImapFields fields, long messageNumber)
pn              equate('Imap.TakeFetchMime')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchText Procedure(long messageNumber)
pn              equate('Imap.TakeFetchText')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchBodyStructure Procedure(*ImapFields fields, long messageNumber)
pn              equate('Imap.TakeFetchBodyStructure')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchEnvelope Procedure(*ImapFields fields, long messageNumber)
pn              equate('Imap.TakeFetchEnvelope')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchFlags Procedure(*ImapFields fields, long messageNumber)
pn              equate('Imap.TakeFetchFlags')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchInternalDate Procedure(*ImapFields fields, long messageNumber)
pn              equate('Imap.TakeFetchInternalDate')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchMail Procedure(long messageNumber)
pn              equate('Imap.TakeFetchMail')
i               long
mail            StringTheory
  code
  self.Log(pn, 'Process ' & Records(self.responses) & ' response records')
  loop i = Pointer(self.responses) to Records(self.responses)
    Get(self.responses, i)
    !self.Log(pn, self.responses.text)
    mail.AppendA(self.responses.text)
  end
  if mail.Length()
    Dispose(self.wholeMessage)
    self.wholeMessage &= mail.value
    self.wholeMessageLen = mail.Length()
    self.messageTextLen = 0
    self.messageHtmlLen = 0
    self._ProcessSplitWholeMessage()        ! Split the message so that all the normal properties are available
    mail.value &= null
  else
    self.ThrowError(NET:IMAP_INVALID_EMAIL, pn)
  end
  self.FreeResponses()                        ! Remove processed entries from the queue
  self.Log(pn,'Complete')

!-----------------------------------------------------------
NetImap.TakeFetchMailHeader Procedure(long messageNumber)
pn              equate('Imap.TakeFetchMailHeader')
  code

!-----------------------------------------------------------
NetImap.TakeFetchMailSize Procedure(*ImapFields fields, long messageNumber)
pn              equate('Imap.TakeFetchMailSize')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchMailText Procedure(long messageNumber)
pn              equate('Imap.TakeFetchMailText')
  code

!-----------------------------------------------------------
NetImap.TakeFetchUid Procedure(*ImapFields fields, long messageNumber)
pn              equate('Imap.TakeFetchOther')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.TakeFetchOther Procedure(long messageNumber)
pn              equate('Imap.TakeFetchOther')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.DoneStore               Procedure ()
pn              equate('NetImap.DoneStore')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.DoneCopy                Procedure ()
pn              equate('NetImap.DoneCopy')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.DoneUid                 Procedure ()
pn              equate('NetImap.DoneUid')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.DonexList               Procedure ()
pn              equate('NetImap.DonexList')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.DoneExIdle              Procedure ()
pn              equate('NetImap.DoneExIdle')
  code
  self.Log(pn, '')

!-----------------------------------------------------------
NetImap.DoneExDone              Procedure ()
pn              equate('NetImap.DoneExDone')
  code
    self.Log(pn, '')



!-----------------------------------------------------------
NetImap.GetMailboxes Procedure(*ImapMailboxes mbq)
pn              equate('NetImap.GetMailboxes')
i               long
  code
    self.Log(pn, '')
    Free(mbq)
    if self.mailboxes &= null
        self.ThrowError(NET:IMAP_MAILBOXES_NULL, pn)
    end
    loop i = 1 to Records(self.Mailboxes)
        Get(self.Mailboxes, i)
        if ErrorCode()
            self.ThrowError(NET:IMAP_CANNOT_GET_MAILBOX, pn)
            cycle
        end
        mbq = self.mailboxes
        Add(mbq)
        if ErrorCode()
            self.ThrowError(NET:IMAP_ADD_FAILED, pn)
            cycle
        end
    end
    return true


!-----------------------------------------------------------
! For each mailbox in the list call STATUS to get additional
! information. The DoneStatus method will then update the mailbox
!
! If the mailbox parameter is passed then just that mailbox is updated
! If the flags string is passed then those flags are used, otherwise
! the method defaults to fetching all flags
!
NetImap.GetFoldersInfo Procedure (<string mailbox>, <string flags>)
pn              equate('NetImap.GetFoldersInfo')
i               long
flagList        Stringtheory
  code
  self.Log(pn, '')
  if not Omitted(3)
    flagList.SetValue(flags,st:clip)
  else
    flagList.SetValue(self.Flags(IMAP_UNSEEN, IMAP_RECENT, IMAP_MESSAGES))
  end
  if not Omitted(2)
    self.Status(mailbox, flagList.GetValue())
  else
    self.BufferCommands()                               ! Buffer these commands rather than flooding the server with requests
    loop i = 1 to Records(self.mailboxes)
      Get(self.mailboxes, i)
      if not Instring('\noselect', Lower(self.mailboxes.attributes), 1, 1)    ! Skip noselect mailboxes
        self.Status(self.mailboxes.name, flagList.GetValue())
      end
    end
    self.FlushCommands()                                ! Send the commands in one go
  end

!-----------------------------------------------------------
! Synchronise one or all records from the internal mailboxes
! queue with the passed queue
NetImap.SyncFolder Procedure (*ImapMailboxes mbq, bool all = false)
pn          equate('NetImap.SyncFolder')
i           long
  code
    self.Log(pn, '')
    if all                                                  ! Sync entire queue (copy from self.mailboxes)
        Free(mbq)
        loop i = 1 to Records(self.mailboxes)
            Get(self.mailboxes, i)
            if ErrorCode()
                break
            end
            mbq :=: self.mailboxes
            Add(mbq)
        end
    else                                                    ! Update just the current record
                                                            self.Log(pn, 'Single record update - mailbox: ' & mbq.name)
        self.mailboxes.name = mbq.name
        Get(self.mailboxes, self.mailboxes.name)
        if not ErrorCode()
            self.Log(pn, 'Found Mailbox')
            mbq :=: self.mailboxes
            self.Log(pn, 'Update unread to: ' & mbq.messages)
            Put(mbq)
            if ErrorCode()
                self.Log(pn, 'Updating the queue failed. Error ' & ErrorCode() & ': ' & Error())
            end
        else
                                                            self.Log(pn, 'Could not update the mailbox, couldn''t find a matching name in the internal mailboxes queue')
            return false
        end
    end
    return true

!-----------------------------------------------------------
! Concatenate up to 10 flags. Condenses the ugliness into one method.
NetImap.Flags Procedure(<string f1>, <string f2>, <string f3>, <string f4>, <string f5>,<string f6>, <string f7>, <string f8>, <string f9>, <string f10>)
flags       StringTheory
  code
  if not Omitted (2) then flags.appendA(f1,st:clip,' ').
  if not Omitted (3) then flags.appendA(f2,st:clip,' ').
  if not Omitted (4) then flags.appendA(f3,st:clip,' ').
  if not Omitted (5) then flags.appendA(f4,st:clip,' ').
  if not Omitted (6) then flags.appendA(f5,st:clip,' ').
  if not Omitted (7) then flags.appendA(f6,st:clip,' ').
  if not Omitted (8) then flags.appendA(f7,st:clip,' ').
  if not Omitted (9) then flags.appendA(f8,st:clip,' ').
  if not Omitted (10) then flags.appendA(f9,st:clip,' ').
  if not Omitted (11) then flags.appendA(f10,st:clip,' ').
  return flags.GetValue()

!-----------------------------------------------------------
NetImap.ErrorMessage Procedure(long errorNum)
  code
    case errorNum
    of NET:IMAP_ERR_NODATA
        return 'No response to parse, the packet was empty.'
    of NET:IMAP_INVALID_RESPONSE
        return 'The response was invalid or incomplete'
    of NET:IMAP_MAILBOXES_NULL
        return 'The Mailboxes property was null'
    of NET:IMAP_NO_RESPONSE
        return 'Error, failed to retrieve a response, or the response text was null'
    of NET:IMAP_INVALID_GREETING
        return 'Invalid server greeting'
    of NET:IMAP_INVALID_RESULT
        return 'Invalid server RESULT in the reponse, was not OK, BAD or NO'
    of NET:IMAP_NODATA_PASSED
        return 'No data was passed.'
    of NET:IMAP_NODATA_TO_SEND
        return 'There is no data available to send'
    of NET:IMAP_INVALID_LIST
        return 'The LIST entry was invalid. It did not contain a mailbox name'
    of NET:IMAP_CANNOT_ADD_MAILBOX
        return 'Failed adding a Mailbox entry to the mailboxes queue'
    of NET:IMAP_CANNOT_GET_MAILBOX
        return 'Failed to get the Mailbox'
    of NET:IMAP_ADD_FAILED
        return 'Could not add to the queue'
    of NET:IMAP_INVALID_STATUS
        return 'Invalid or malformed STATUS response'
    of NET:IMAP_INVALID_FETCH
        return 'Invalid or malformed FETCH response'
    of NET:IMAP_INVALID_EMAIL
        return 'Invalid or empty email BODY'
    else
        return 'Unknown error'
    end
!-----------------------------------------------------------
NetImap.GetFlags    Procedure(String sequenceSet)
  Code
  return self.SendCommand(IMAP_FETCH, Clip(sequenceSet) & ' FLAGS')

NetImap.SetFlags    Procedure(String sequenceSet,string flags)
  Code
  return self.SendCommand(IMAP_STORE, Clip(sequenceSet) & ' +FLAGS ' & '(' & clip(flags) & ')')
  !return false

!-----------------------------------------------------------
! ImapFields class. Provides storage for IMAP responses
! and dynamic memory allocation.

ImapFields.Construct Procedure()
  code
  self.fields &= new ImapFieldsQ

ImapFields.Destruct Procedure()
  code
  Dispose(self.fields)

ImapFields.Fetch Procedure(long pos)
  code
  Get(self.fields, pos)
  self.lastErrorCode = ErrorCode()
  if self.lastErrorCode
    self.lastError = Error()
    return false
  else
    self.value &= self.fields.value
    self.type = self.fields.type
    return True
  end

ImapFields.Remove Procedure()
  code
  Delete(self.fields)
  self.lastErrorCode = ErrorCode()
  if self.lastErrorCode
    self.lastError = Error()
    return false
  else
    self.type = -1
    self.value &= null
    return True
  end

ImapFields.Insert Procedure(string val, long type=0)
  code
  self.fields.value &= new string(Len(Clip(val)))
  self.fields.value = val
  self.fields.type  = type

  Add(self.fields)
  self.lastErrorCode = ErrorCode()
  if self.lastErrorCode
    self.lastError = Error()
    return false
  else
    self.value &= self.fields.value
    self.type = type
    return True
  end

! Delete all entries in the queue
ImapFields.Free Procedure()
i           long
  code
  loop i = 1 to Records(self.fields)
    Get(self.fields, i)
    Dispose(self.fields.value)
  end
  Free(self.fields)
  self.type = -1
  self.value &= null

! Returns the number of records in the queue
ImapFields.Count Procedure()
  code
  return Records(self.fields)

! Resets the position to zero to allow Next to be called to loop through
! the records in the queue
ImapFields.Set Procedure()
  code
  self.pos = 0

! Returns 0 (False) if successful and True (1) if
! an error occurs (for example there are no more records).
! Used for looping through records in the normal "ABC" style:
! loop until Imap.fields.Next()
!     ! Do something here with Imap.fields.value
! end
ImapFields.Next Procedure()
  code
  self.pos += 1
  if self.Fetch(self.pos)         ! Fetch was successful, return 0
    return 0
  else                            ! Fetch failed, return 1 to indicate the end of the records
    return 1
  end

ImapFields.FetchType Procedure(long type)
  code
  self.fields.type = type
  Get(self.fields, self.fields.type)
  self.lastErrorCode = ErrorCode()
  if self.lastErrorCode
    self.lastError = Error()
    return false
  else
    self.value &= self.fields.value
    self.type = self.fields.type
    return True
  end

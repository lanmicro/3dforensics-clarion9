! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software  - www.capesoft.com


      Member()
         omit('***$***',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
         ***$***
      Include('Equates.CLW'),ONCE
      Include('Keycodes.CLW'),ONCE
      Include('Errors.CLW'),ONCE
      Map
      End ! map
      Include('NetWww.inc'),ONCE
   ! includes - NetMap Module Map
   include('NetMap.inc'),once

!------------------------------------------------------------------------------
NetWebClient.Construct  Procedure
  Code
  self._FormFields &= new(FormFieldsQueueType)
  self.MultipartType = 'multipart/form-data'

!------------------------------------------------------------------------------
NetWebClient.Destruct  Procedure
  Code
  if not self._FormFields &= Null
    self.FreeFieldsQueue()
    dispose(self._FormFields)
  End
  if not self._poststring &= Null
    dispose(self._poststring)
  end

!------------------------------------------------------------------------------
NetWebClient.MakePostString  Procedure()
fq          Queue(FILE:queue),pre(fq)
            End
x           long
y           long
len         long
bdary       string('---------------------------20485309314343')
  code
  loop x = 1 to records(self._FormFields)
    get(self._FormFields,x)
    if self._FormFields.FieldIsFileName
      self.FormEncodeType = self.MultipartType
      break
    end
    if self._FormFields.value &= null
      len += len(clip(self._FormFields.Name)) + 1 + 1
      cycle
    end
    if not self._FormFields.FieldIsBlank
      len += len(clip(self._FormFields.Name)) + 1 + self.EncodeWebStringLength(self._FormFields.value,net:BigPlus,len(self._FormFields.value)) + 1
    else
      len += len(clip(self._FormFields.Name)) + 1 + 1
    end
  end
  if len > 0 then len -= 1.  ! allow for removal of trailing &
  if self.FormEncodeType = ''
    if not self._poststring &= Null
      dispose(self._poststring)
    end
    self._poststring &= new(string(len))
    loop x = 1 to records(self._FormFields)
      get(self._FormFields,x)
      if self._FormFields.value &= null or self._FormFields.FieldIsBlank
        self._poststring = clip(self._poststring) & clip(self._FormFields.name) & '=' & '&'
      else
        self._poststring = clip(self._poststring) & clip(self._FormFields.name) & '=' & self.EncodeWebString(self._FormFields.value,net:BigPlus,len(self._FormFields.value)) & '&'
      end
    end

  elsif self.FormEncodeType = self.MultipartType
    len = 0
    loop x = 1 to records(self._FormFields)
      get(self._FormFields,x)
      if self._FormFields.FieldIsFileName
        Directory(fq,self._FormFields.value,ff_:NORMAL + ff_:Hidden + ff_:system + ff_:readonly + ff_:archive)
        get(fq,1)
        len += 1000 + fq.size
      else
        len += 1000 + len(clip(self._FormFields.value)) + len(clip(self._FormFields.name))
      end
    end
    if not self._poststring &= Null
      dispose(self._poststring)
    end
    self._poststring &= new(string(len))

    self.ContentType = clip(self.MultipartType) & '; boundary=' & bdary
    loop x = 1 to records(self._FormFields)
      get(self._FormFields,x)
      if self._FormFields.FieldIsFileName = 0
        if self._FormFields.CustomHeader = ''
          self._postString = clip(self._poststring) & '--' & bdary & |
          '<13,10>Content-Disposition: form-data; name="'&clip(self._FormFields.name)&|
          '"<13,10><13,10>'&clip(self._FormFields.value)&'<13,10>'
        else
          self._postString = clip(self._poststring) & '--' & bdary & |
          '<13,10>' & clip(self._FormFields.CustomHeader)&|
          '<13,10><13,10>'&clip(self._FormFields.value)&'<13,10>'
        end
      else
        if self._FormFields.CustomHeader = ''
          Directory(fq,self._FormFields.value,ff_:NORMAL + ff_:Hidden + ff_:system + ff_:readonly + ff_:archive)
          get(fq,1)
          self._postString = clip(self._poststring) & '--' & bdary & |
          '<13,10>Content-Disposition: form-data; name="'&clip(self._FormFields.name)&|
          '"; filename="'&clip(fq.name)&'"<13,10>Content-Type: '&clip(self._FormFields.ContentType)&'<13,10,13,10>'
        else
          self._postString = clip(self._poststring) & '--' & bdary & |
          '<13,10>' & clip(self._FormFields.CustomHeader) &'<13,10,13,10>'
        end
        do AddFile
      end
    end
    self._postString = clip(self._poststring) & '--' & clip(bdary) & '--<13,10>'
  end

addFile  routine
  data
hFile     long
FileSize  long
SizeHigh  long
result    long
Buffer      &string
BufferSize  long
BytesRead   long
cFileName                 cstring(260)

  code
  cFileName = self._FormFields.value
  hFile = OS_CreateFile(cFileName, NET:OS:GENERIC_READ, NET:OS:FILE_SHARE_READ, 0, NET:OS:OPEN_EXISTING, NET:OS:FILE_ATTRIBUTE_NORMAL, 0)
  if hFile <= 0
    exit
  end

  FileSize = OS_GetFileSize (hFile,SizeHigh) ! Read up on function if filesize > 2^32 bytes
  if FileSize = 0
    exit
  end

  buffer &= new(string(FileSize))

  result = OS_ReadFile(hFile, buffer, FileSize, BytesRead, 0)
  if result = 0
    ! failed
  else
    self._postString = clip(self._poststring) & buffer & '<13,10>'
  end
  result = OS_CloseHandle(hFile)
  dispose(buffer)

!------------------------------------------------------------------------------
NetWebClient.FreeCookies            PROCEDURE ()
  code
  free(self._CookieQueue)

!------------------------------------------------------------------------------
NetWebClient._ReadCookies            PROCEDURE ()
st  long
y   long
z   long
  code
!  free(self._CookieQueue)
  st = 1
  loop
    st = instring('Set-Cookie:',self.page,1,st)
    if st
      st += 12
      y = instring('=',self.page,1,st)
      if y
        self._CookieQueue.name = self.page[st : y-1]
        get(self._CookieQueue,self._CookieQueue.name)
        if errorcode()
          add(self._CookieQueue)
        end
        st=y+1
        y = instring(';',self.page,1,st)
        z = instring('<13,10>',self.page,1,st)
        if z < y or y = 0 then y = z.
        if y
          self._CookieQueue.value = self.page[st : y-1]
          put(self._CookieQueue)
        end
      end
    else
      break
    end
  end
!------------------------------------------------------------------------------
NetWebClient._MakeCookies           PROCEDURE () ! returns a semi-colon separated string of cookies.
x  long
  code
  self.cookie = ''
  loop x = 1 to records(self._CookieQueue)
    get(self._CookieQueue,x)
    self.cookie = clip(self.cookie) & clip(self._CookieQueue.Name) & '=' & clip(self._CookieQueue.Value) & ';'
  end

!------------------------------------------------------------------------------
NetWebClient.DeleteCookie           PROCEDURE (String p_Name)
  code
  If p_name = '' then return.
  clear(self._CookieQueue)
  self._CookieQueue.Name = Upper(p_name)
  get(self._CookieQueue,self._CookieQueue.Name)
  if Errorcode() = 0
    Delete(self._CookieQueue)
  End

!------------------------------------------------------------------------------
NetWebClient.SetValue        Procedure(String pName,String pValue, Long pFile=0,<String pContentType>,<String pCustomHeader>)
y         long
l         long
  code
  if pName = '' then return.
  self._FormFields.name = pName
  get(self._FormFields,self._FormFields.name)
  If Errorcode()
    clear(self._FormFields)
    self._FormFields.name = pName
    do sv
    add(self._FormFields)
  Else
    dispose(self._FormFields.value)
    do sv
    put(self._FormFields)
  end

sv  routine
  l = len(pValue)
  if l = 0
    self._FormFields.value &= new string(1)
    self._FormFields.value = ''
    self._FormFields.FieldIsBlank = true
  else
    self._FormFields.value &= new string(l)
    self._FormFields.value = pValue
    self._FormFields.FieldIsBlank = false
  end
  self._FormFields.FieldIsFileName=pFile
  if not omitted(6)
    self._FormFields.CustomHeader = pCustomHeader
  end
  if pFile
    if omitted(5) or pContentType = ''
     self._FormFields.ContentType = self._GetContentType(pValue)
    else
      self._FormFields.ContentType = pContentType
    end
  end

!------------------------------------------------------------------------------
NetWebClient.DeleteValue        Procedure(String pName)
  code
  self._FormFields.Name = pName
  get(self._FormFields,self._FormFields.Name)
  If Errorcode() = 0
    if not self._FormFields.value &= Null
      dispose(self._FormFields.value)
    end
    delete(self._FormFields)
  End

!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
NetWebClient.FreeFieldsQueue  Procedure()
  code
  if not self._FormFields &= Null
    loop while records(self._FormFields)
      get(self._FormFields,1)
      if not self._FormFields.value &= Null
        dispose(self._FormFields.value)
      end
      delete(self._FormFields)
    end
  end

!------------------------------------------------------------------------------
NetWebClient.ANSIToUnicode PROCEDURE  ()                   ! Declare Procedure 3
MyUnicode           Class (_NetUnicode)
                    End

PageSize            long
TempString          &String
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.AnsiToUnicode', 'Converting Page from ANSI to Unicode (UTF-16)')
    end
  ****
  PageSize = self.PageLen - self.HeaderLen
  if PageSize = 0
    return
  end
  MyUnicode.AnsiCString &= New (CString (PageSize+1))
  MyUnicode.AnsiCString = self.Page[self.HeaderLen + 1 : self.PageLen] & '<0>'
  MyUnicode.AnsiCSTringToUnicodeCString()
  if MyUnicode.UnicodeCStringLen = 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.AnsiToUnicode', 'Warning. Could not convert from Ansi To Unicode.')
      end
    ****
    dispose (MyUnicode.AnsiCString)
    return
  end
  if self.HeaderLen + MyUnicode.UnicodeCStringLen > size(self.Page)
    TempString &= self.Page
    self.Page &= New (String(self.HeaderLen + MyUnicode.UnicodeCStringLen))
    if self.HeaderLen > 0
      self.Page [1 : self.HeaderLen] = TempString [1 : self.HeaderLen]
    end
    dispose (TempString)
  end
  self.Page[self.HeaderLen + 1 : self.HeaderLen+MyUnicode.UnicodeCStringLen] = MyUnicode.UnicodeCString[1 : MyUnicode.UnicodeCStringLen]
  self.PageLen = self.HeaderLen + MyUnicode.UnicodeCStringLen

  dispose (MyUnicode.UnicodeCString)
  dispose (MyUnicode.AnsiCString)

!------------------------------------------------------------------------------
NetWebClient.ConnectionClosed PROCEDURE  ()                ! Declare Procedure 3
  CODE
  if self.busy = 1
    Do Finished
  end

! ---------------------------------------------------------------------------------

Finished Routine
  ! Changes to this routine must also be made in Process()
  self.busy = 0
  if self.PageLen = 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.ConnectionClosed', 'Warning. About to call ErrorTrap as Connection closed, but PageLen = 0 (ERROR:NoDataReceived)')
      end
    ****
    self.Error = ERROR:NoDataReceived
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap('Connection closed before any data was received', 'NetWebClient.ConnectionClosed')
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.ConnectionClosed', 'About to call self._CallPageReceived(). PageLen = ' & self.PageLen)
      end
    ****
    self._CallPageReceived()
  end

!------------------------------------------------------------------------------
NetWebClient.DecodeWebString PROCEDURE  (string p_PostString) ! Declare Procedure 3
loc:x         long
loc:y         long
  CODE
  ! turn + into space
  loc:x = instring('+',p_PostString,1,1)
  loop until loc:x = 0
    p_PostString[loc:x] = ' '
    loc:x = instring('+',p_PostString,1,1)
  end
  ! turn %ab into a single char.
  loc:x = instring('%',p_PostString,1,1)
  loop until loc:x = 0
    case p_PostString[loc:x+1]
    of '0' to '9'
      loc:y = p_PostString[loc:x+1] * 16
    of 'A' to 'F'
      loc:y = (val(p_PostString[loc:x+1])-55) * 16
    of 'a' to 'f'
      loc:y = (val(p_PostString[loc:x+1])-87) * 16
    end
    case p_PostString[loc:x+2]
    of '0' to '9'
      loc:y += p_PostString[loc:x+2]
    of 'A' to 'F'
      loc:y += val(p_PostString[loc:x+2])-55
    of 'a' to 'f'
      loc:y += val(p_PostString[loc:x+2])-87
    end
    !loc:ans = sub(loc:ans,1,loc:x-1) & chr(loc:y) & sub(loc:ans,loc:x+3,255)
    if loc:x <= 1
      p_PostString = chr(loc:y) & p_PostString[loc:x+3 : SIZE(p_PostString)]
    elsif loc:x+3 < SIZE(p_PostString)
      p_PostString = p_PostString[1 : loc:x-1] & chr(loc:y) & p_PostString[loc:x+3 : SIZE(p_PostString)]
    else
      p_PostString = p_PostString[1 : loc:x-1] & chr(loc:y)
    end
    loc:x = instring('%',p_PostString,1,(loc:x+1))
  end
  return p_PostString

! >> I also found that the (*) asterisk, (_) underscore, @ symbol, (-) dash, and
!    (.) period do not need to be converted to ascii hex along with 0-9, A-Z, and a-z.



!!------------------------------------------------------------------------------
!NetWebClient.EncodeWebString PROCEDURE  (string p_PostString, Long p_Len=0)
!local   group, pre (loc)
!x         long
!y         long
!t1        byte
!t2        byte
!c1        string(1)
!c2        string(1)
!l         long
!v         byte
!        end
!
!loc:table    string('0123456789ABCDEF')
!
!   omit('***',_VER_C60)
!loc:ans      string(NET:MAXBinData)
!   ***
!   compile('***',_VER_C60)
!loc:ans      STRING(SIZE(p_PostString) * 3)
!   ***
!
!  CODE
!! >> I also found that the (*) asterisk, (_) underscore, (@) at symbol, (-) dash, and
!!    (.) period do not need to be converted to ascii hex along with 0-9, A-Z, and a-z.
!
!  loc:ans = p_PostString
!  ! turn single chars into %ab
!  if p_len = 0
!    loc:l = len (clip (loc:ans))
!  else
!    loc:l = p_len
!  end
!  loc:x = 0
!  loop until loc:x >= loc:l
!    loc:x += 1
!    case loc:ans[loc:x]
!    of 'a' to 'z'
!    orof 'A' to 'Z'
!    orof '0' to '9'
!    orof '*'
!    orof '_'
!    orof '@'
!    orof '-'
!    orof '.'
!      ! do nothing
!    else
!      loc:v = val (loc:ans[loc:x])
!      loc:t1 = (loc:v / 16)
!      loc:c1 = loc:table[loc:t1 + 1]
!      loc:t2 = loc:v - (loc:t1 * 16)
!      loc:c2 = loc:table[loc:t2 + 1]
!      loc:ans = sub (loc:ans,1,loc:x-1) & '%' & loc:c1 & loc:c2 & sub (loc:ans,loc:x+1,(loc:l - loc:x))
!      loc:x += 2
!      loc:l += 2
!    end
!  end
!  return clip(loc:ans)

!------------------------------------------------------------------------------
NetWebClient.Fetch   PROCEDURE  (String p_Url)             ! Declare Procedure 3
! ----------------------------------------------------------------------
! This function sets self.error = 0 on success,
!     or to ERROR:CouldNotConnectToHost (-39) if it can't connect,
!     or to the error reported in self.Open
! ----------------------------------------------------------------------

local           group, pre (loc)
HostName          string(256)     ! e.g if URL = www.capesoft.com/index.htm HostName = www.capesoft.com
HostPort          NET:PortType
_Spacer1          NET:PortType
StoreSuppress     long
                end

  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.Fetch', 'Fetch called on ' & clip (p_Url))
    end
  ****
  self.error = 0                              ! Clear error flag
  self.WinSockError = 0
  self.SSLError = 0

  self._TempRedirectURL = p_Url
  self._CommandURL = self.GetURL(p_Url)                  ! Add 'http://' or trailing '/' to it if neccessary

  self._CheckForHTTPS(self._CommandURL)                  ! New 26/4/2004 - turns on SSL if HTTPS is being used

  if self._CommandURL = ''
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Fetch', 'No Server specified')
      end
    ****
    self.error = ERROR:NoServers
    self.ErrorTrap ('The URL for the Fetch command was blank', 'NetWebClient.Fetch')
    return
  end

  self.ContentLength = 0

  loc:HostName = self.getURLHostName (self._CommandURL, loc:HostPort)  ! Just get the hostname and Port of the URL

  if self.AsyncOpenUse = 0
    Do Old_SynchronousOpen_Open
  else
    if self.openflag = 1
      self.abort()
    end
  end

  self.Host = loc:HostName                    ! Populate the Host Property with the Hostname (HTTP 1.0 Protocol)

  clear(self.Packet)

  self._Command = ''
  self._CommandType = Net:WebClientFetch

  if (self.AsyncOpenUse = 0)
    Do Old_SynchronousOpen_Command
  else
    self._Command = clip (self._Command) & self._CreateRequestHeader(loc:HostPort)
  end

  self._UseCommandLarge = false

  self.Packet.BinData = clip(self._Command)
  self.Packet.BinDataLen = len(clip(self.Packet.BinData))

  self._ResetPage()

  if self.AsyncOpenUse = 0
    Do Old_SynchronousOpen_Send
  else
    Do AsyncOpen
  end

  return


! -------------------------------------
AsyncOpen Routine
  if self.AsyncOpenBusy
    self.abort()
  end
  if self.AsyncOpenTimeout = 0
    self.AsyncOpenTimeout = 900 ! 9 seconds
  end
  ! Open using Async Open
  self.Open (loc:HostName, loc:HostPort)
  ! Packet will be sent in


! ------------------------------------------------------
! ------------------------------------------------------
! ------------------------------------------------------
Old_SynchronousOpen_Open Routine
  loc:StoreSuppress = self.SuppressErrorMsg   ! Store current error suppression option
  self.SuppressErrorMsg = 1                   ! Suppress errors
  self.Open(loc:HostName, loc:HostPort)       ! Try and open the site (may try Proxy as well)
  self.SuppressErrorMsg = loc:StoreSuppress   ! Restore error suppression option
  if self.OpenFlag = 0                        ! Site could not be connected to
    if self.error = 0
      self.error = ERROR:CouldNotConnectToHost
    end
    self.ErrorTrap ('Could not connect synchronously to the website.', 'NetWebClient.Fetch')
    return
  end

! ------------------------------------------------------
Old_SynchronousOpen_Command Routine
  ! OLD (Outdated Synchronous Usage)
  if self.HeaderOnly
    self._Command = self._CreateRequestURI('HEAD ') & self._CreateRequestHeader(loc:HostPort)
  else
    self._Command = self._CreateRequestURI('GET ') & self._CreateRequestHeader(loc:HostPort)
  end

! ------------------------------------------------------
Old_SynchronousOpen_Send Routine
  self.Send()                                   ! Try and send data
  if self.error = ERROR:ClientNotConnected      ! The connection was closed before before this object found out.
    if self.AsyncOpenUse = 0
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetWebClient.Fetch', 'Connection is not open - will try and open again')
        end
      ****
      loc:StoreSuppress = self.SuppressErrorMsg   ! Store current error suppression option
      self.SuppressErrorMsg = 1                   ! Suppress errors
      self.Open(loc:HostName, loc:HostPort)       ! Try and open the site (may try Proxy as well)
      self.SuppressErrorMsg = loc:StoreSuppress   ! Restore error suppression option
      if self.OpenFlag = 0                        ! Site could not be connected to
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetWebClient.Fetch', 'Could not open connection')
          end
        ****
        if self.error = 0
          self.error = ERROR:CouldNotConnectToHost
        end
        self.ErrorTrap ('Could not connect synchronously to the website.', 'NetWebClient.Fetch')
        return
      end
      self.error = 0                              ! Clear error flag
      self.send()                                 ! Try and send again
      if self.error <> 0
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetWebClient.Fetch', '2nd attempt to fetch the page failed.')
          end
        ****
      end
    else
      ! Async Open
      Do AsyncOpen
      return
    end
  end
! ------------------------------------------------------


!------------------------------------------------------------------------------
NetWebClient.Free    PROCEDURE  ()                         ! Declare Procedure 3
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.Free', 'Called')
    end
  ****

  if ~self.page&=NULL
    dispose(self.page)
  end
  self.PageLen = 0
  self.HeaderLen = 0
  self._AllocatedPageLen = 0
  self._ExpectedPageLen = 0

!------------------------------------------------------------------------------
NetWebClient.GetHeaderField PROCEDURE  (string p_Header,long p_count=1) ! Declare Procedure 3
local      group, pre (loc)
StartPos1    long
HeaderEndPos long
EndPos       long
Pos3         long
Len          long
ret          string (2048)
           end

  CODE
  loc:ret = ''
  if self.HeaderLen < 2 or self.HeaderLen > size (self.Page)
    return (clip(loc:ret)) ! return nothing
  end
  loop p_Count times
    loc:StartPos1 = InString (upper(clip(p_header)), upper (self.Page [1 : self.HeaderLen]),1,(loc:StartPos1 + 1))
    if loc:StartPos1 = 0
      break
    end
  end
  if (loc:StartPos1 > 0) and (loc:StartPos1 <= self.HeaderLen)
    loc:Len = len (clip(p_Header))
    loc:StartPos1 += loc:Len
    loc:EndPos = loc:StartPos1
    loop
      loc:EndPos = InString ('<13,10>',self.Page [1 : self.HeaderLen],1,loc:EndPos)
      if loc:EndPos = 0
        return (clip (loc:ret))  ! return nothing
      end
      if (loc:EndPos - 1) >= loc:StartPos1
        loc:ret = clip(left(self.Page [loc:StartPos1 : (loc:EndPos-1)]))
        return (clip(loc:ret)) ! Return the field
      end
    end
  end

  return (clip (loc:ret))

!------------------------------------------------------------------------------
NetWebClient.GetURL  PROCEDURE  (String p_Url)             ! Declare Procedure 3
local             group, pre (loc)
ourURL              String(16384)
x                   long
y                   long
z                   long
ourURLSize          long
slashes             long
pos1                long
res                 long
                  end

  CODE
  ! -----------------------------------------------------------------------
  ! This function processes the URL and converts it into a format that the
  ! Web Server will accept.
  ! -----------------------------------------------------------------------

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.GetURL', 'Called p_Url = [' & clip (p_Url) & ']')
    end
  ****

  ! Convert spaces to %20
  loc:x = len (clip(p_Url))               ! Max length we can handle
  loc:z = 1                               ! position in p_Url
  loc:y = 1                               ! position in loc:ourURL
  loc:ourURLSize = size (loc:ourURL)
  loop while (loc:y <= loc:x) and (loc:z <= loc:ourURLSize) and loc:y < size(loc:ourURL)
    if p_Url[loc:y] = ' '
      if loc:z < size(loc:ourURL) - 1
        loc:ourURL[loc:z : (loc:z + 2)] = '%20'
        loc:z += 3
      end
    else
      if loc:z < size(loc:ourURL)
        loc:ourURL[loc:z] = p_URL[loc:y]
        loc:z += 1
      end
    end
    loc:y += 1
  end

  if clip(loc:ourURL) = ''
    Return ''
  end

  ! Convert any '\' to '/'
  loc:Pos1 = 1
  loop
    loc:res = instring('\',loc:ourURL,1, loc:Pos1)
    if loc:res = 0
      break
    end
    loc:ourURL[loc:res] = '/'
    loc:Pos1 = loc:res + 1
  end

  if sub(lower(loc:ourURL),1,7) <> 'http://' and sub(lower(loc:ourURL),1,8) <> 'https://'
    loc:ourURL = 'http://' & clip(loc:ourURL)             ! Add 'http://'
  end

  if sub (loc:ourURL, len (clip(loc:ourURL)), 1) <> '/'   ! If last character not '/'
    ! Start counting slashes
    loc:slashes = 0
    loc:Pos1 = 1
    loop
      loc:res = instring('/',loc:ourURL,1, loc:Pos1)
      if loc:res = 0
        break
      end
      loc:Slashes += 1
      loc:Pos1 = loc:res + 1
    end
    ! End counting slashes

    if loc:slashes < 3
      loc:ourURL = clip (loc:ourURL) & '/'                ! Convert 'http://www.capesoft.co.za' to 'http://www.capesoft.co.za/'
    end
  end

  if self.LoggingOn
    self.Log ('NetWebClient.GetURL', 'Returning ourURL = [' & clip (loc:ourURL) & ']')
  end

  Return(loc:ourURL)

!------------------------------------------------------------------------------
NetWebClient.GetURLHostName PROCEDURE  (string p_Url,*ushort p_Port) ! Declare Procedure 3
local    group, pre(loc)
temp        long
ret         string (URLLENGTH)
l           long
         end

  CODE
  loc:ret = p_Url

  loc:l = len(clip(loc:ret))
  if loc:l >= 7
    if lower (sub (loc:ret,1, 7)) = 'http://'
      loc:ret = sub(loc:ret,8,loc:l)
      loc:l -= 7
    else
      if loc:l >= 8
        if lower (sub (loc:ret,1, 8)) = 'https://'
          loc:ret = sub(loc:ret,9,loc:l)
          loc:l -= 8
        end
      end
    end
  end
  loc:temp = instring('/',loc:ret,1,1)
  if loc:temp <> 0
    loc:ret = sub(loc:ret, 1, (loc:temp-1))
    loc:l = (loc:temp-1)
  end
  ! Get the Port
  loc:temp = instring (':', loc:ret, 1, 1)
  if (loc:temp = 0) or (loc:temp + 1 > loc:l)
    if self.SSL = 0
      p_Port = 80  ! Default HTTP port
    else
      p_Port = 443 ! Default HTTPS (SSL) port
    end
  else
    p_Port = 0 + sub (loc:ret, loc:temp + 1, loc:l - loc:temp)
    loc:ret = sub (loc:ret, 1,  loc:temp - 1)
  end
  return (loc:ret)

!------------------------------------------------------------------------------
NetWebClient.GetValueFromWebString PROCEDURE  (string p_VarName,string p_PostString) ! Declare Procedure 3
s     string(8192)
ans   string(1024)
x     long
y     long
  CODE
  s = p_poststring
  ans = ''
  x = instring(clip(lower(p_VarName))&'=',lower(s),1,1)
  if x > 0
    y = instring('&',s,1,x)
    if y = 0 then y = instring('<13>',s,1,x).
    if y = 0 then y = instring('<10>',s,1,x).
    if y = 0 then y = len(clip(s)) + 1.
    x = x + len(clip(p_VarName)) + 1
    ans = sub(s,x,y-x)
    ans = self.DecodeWebString (ans)
  end
  return(ans)
!------------------------------------------------------------------------------
NetWebClient.GetVariableNameFromWebString PROCEDURE  (long p_number,string p_string) ! Declare Procedure 3
x       long
y       long
ans     string(255)
  CODE
  ans = ''
  if p_number = 1
    x = 1
  else
    loop p_number-1 times
      x = instring('&',p_string,1,1) + 1
    end
  end
  if x > 0
    y = instring('=',p_string,1,x)
    if y > 0
      ans = self.DecodeWebString(sub(p_string,x,y-x))
    end
  end
  return(ans)

!------------------------------------------------------------------------------
NetWebClient.Init    PROCEDURE  (uLong Mode=NET:SimpleClient) ! Declare Procedure 3
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.Init', 'Called')
    end
  ****

  ! Set default values
  self.CanUseProxy = 1
  self._MinPageLen = 65536 ! 64K
  self.HeaderOnly = 0
  self._HTTPVersion = 'HTTP/1.0' ! Not set in SetAllHeadersDefault ! Set this to 'HTTP/1.1' if you want HTTP/1.1

  ! The following defaults are set in SetAllHeadersDefault
  self._Accept_ = 'image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, */*' ! '*/*'
  self._AcceptLanguage = 'en'           ! 'en-za'
 !self._UserAgent = 'Mozilla/4.0 (compatible; MSIE 5.0; Windows 98; DigExt)' ! Windows 98
 !self._UserAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)' ! Windows XP
 !self._UserAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)' ! Windows XP SP2 (with Security Version 1)
 !self._UserAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)' ! Windows XP SP2 (with Security Version 1) & Dot Net 1.1 and 2.0
 !self._UserAgent = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.204 Safari/534.16' ! chrome
  self._UserAgent = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16 ( .NET CLR 3.5.30729; .NET4.0C)' ! firefox 3.6
  self.SetAllHeadersDefault()
  parent.Init()

!------------------------------------------------------------------------------
NetWebClient.Kill    PROCEDURE  ()                         ! Declare Procedure 3
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.Kill', 'Called')
    end
  ****

  if ~(self._CommandLarge &= NULL)
    dispose (self._CommandLarge)
  end

  parent.kill()

  self.free()                                     ! Free memory in self.Page

!------------------------------------------------------------------------------
NetWebClient.MoreMemoryForPage PROCEDURE  ()               ! Declare Procedure 3
TempStringPointer          &String
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.MoreMemoryForPage', 'Called')
    end
  ****

  if self._ExpectedPageLen < self._MinPageLen
    self._ExpectedPageLen = self._MinPageLen           ! Smallest block = self._MinPageLen (64K default)
  end

  if (self._ExpectedPageLen - self._AllocatedPageLen) < self._MinPageLen
    self._ExpectedPageLen += self._MinPageLen          ! Make sure we are going up by at least the value in self._MinPageLen (64K default)
  end

  TempStringPointer &= self.Page                       ! Store old pointer
  self.Page &= new(String(self._ExpectedPageLen))
  if ~TempStringPointer&=NULL
    if self._FirstPacket = 0
      self.Page = TempStringPointer                    ! Only Copy Old Page, if not first packet
    end
    dispose(TempStringPointer)
  else
    ! Not copying
  end
  self._AllocatedPageLen = self._ExpectedPageLen

!------------------------------------------------------------------------------
NetWebClient.Open    PROCEDURE  (string Server,uShort Port=0) ! Declare Procedure 3
TryServer                  string (1024)
TryPort                    long

ProxyEnabled               long
ProxyFound                 byte
RegFound                   byte
_Spacer1                   byte
_Spacer2                   byte
tempPos                    long


  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.Open', 'Called')
    end
  ****

  if Port = 0
    Port = 80                                                          ! Default
  end

  TryPort = 80                                                         ! Default - keep this here, prevents LastPort = TryPort

  Do GetProxySettings

  if self.OpenFlag or self.AsyncOpenBusy                           ! Changed 12/3/2004
    ! Connection is open, or is already opening
    if (self._CurrentServer = Server and self._CurrentPort = Port) or |
       (self._CurrentServer = TryServer and self._CurrentPort = TryPort)
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetWebClient.Open', 'Already connected. No need to reconnect. Server = ' & clip (self._CurrentServer) & ':' & self._CurrentPort)
        end
      ****
      return                                                       ! We are already connected, so that's fine
    else
      self.abort()                                                 ! Close this connection first
    end
  end

  if ProxyFound
    self._ProxyUsed = 1
    self._ProxyServer = TryServer
    self._ProxyPort = TryPort
    self._HostServer = Server
    self._HostPort = Port
  else
    self._ProxyUsed = 0
    self._ProxyServer = ''
    self._ProxyPort = 0
    self._HostServer = Server
    self._HostPort = Port
  end

  self._TryOpen()                                                  ! Try open

  if (self.OpenFlag = 0) and (self.AsyncOpenBusy = 0)
    ! This will only be called in the Synchronous Open Mode
    ! The connection did not open
    Do HandleSyncOpenFailed
  else
    ! Connecting fine (asynchronous open) or Connected fine (synchronous open) or
    if ProxyFound and self._ProxyUsed
      self.ProxyServer = self._ProxyServer         ! Store Proxy Settings to save loading from Registry in future
      self.ProxyPort = self._ProxyPort
    end
    self._ConnectingOkay()
  end


! ------------------------------------------------------------------------------------------
HandleSyncOpenFailed Routine
  if self._ProxyUsed
    ! It was the proxy that did not open. We will try and connect directly to the host
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Open', 'Could not open proxy, will try open connection directly.')
      end
    ****
    self._ProxyUsed = 0 ! Try and connect direct to the Web Server
    self._TryOpen()
    if self.openFlag = 1 or self.AsyncOpenBusy = 1
      ! Connecting directly okay
      self._ConnectingOkay()
    else
      ! Neither the proxy or connecting directly worked
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetWebClient.Open', '(2) Could not connect via Proxy or directly to Web Server ' & clip (self._HostServer) & ':' & self._HostPort)
        end
      ****
    end
  else
    ! The site could not be opened
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Open', 'Could not connect directly to Web Server ' & clip (self._HostServer) & ':' & self._HostPort)
      end
    ****
  end

! ------------------------------------------------------------------------------------------
GetProxySettings Routine
  if self.CanUseProxy = 1
    ! Sean Feb 08 - readded support for allowing the proxy server to be set
    if self.ProxyServer <> ''
      ! Proxy specified
      ProxyFound = 1
      TryServer = self.ProxyServer
      if self.ProxyPort <> 0
        TryPort = self.ProxyPort
      else
        TryPort = 80
      end
    else
      ! Get Proxy Settings out the registry
      ProxyEnabled = 0
      ProxyFound = false
      RegFound = 0
      ProxyEnabled = NetGetRegistry(NET:HKEY_CURRENT_USER,'Software\Microsoft\Windows\CurrentVersion\Internet Settings', 'ProxyEnable', NET:REG_DWORD, RegFound)
      if (RegFound = true) and (ProxyEnabled = 1)
        TryServer = NetGetRegistry(NET:HKEY_CURRENT_USER,'Software\Microsoft\Windows\CurrentVersion\Internet Settings', 'ProxyServer', NET:REG_SZ, ProxyFound)
      end
      if ProxyFound = false
        ! Not found in registry
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            if ProxyEnabled = 0
              self.Log ('NetWebClient.Open', 'Internet Explorer Settings are not setup to use a Proxy server. Will try open connection directly.')
            else
              self.Log ('NetWebClient.Open', 'Proxy settings not found in registry. Will try open connection directly.')
            end
          end
        ****
      else
        ! Proxy found in Registry
        if self.SSL = 1
          if self.OptionsDontUseSSLOverHTTPProxy = 0
            self._UseSSLOverHTTPProxy = 1 ! Automatically turn this on
          end
          tempPos = instring ('https=', lower (TryServer),1,1)
          if tempPos
            ! The Registry entry is something like this:
            ! ftp=http://earth22:8022;gopher=http://earth33:8033;http=earth:80;https=http://earth11:11
            TryServer = sub(TryServer, (tempPos+6), len (clip (TryServer)) - (tempPos+6) + 1)
            tempPos = instring (';', TryServer,1,1)
            if tempPos
              ! There is a ;
              TryServer = sub (TryServer, 1, (tempPos-1))
            end
          end

          if lower (sub(TryServer, 1, 7)) = 'http://'
            TryServer = sub(TryServer, 8, len (clip (TryServer)) - 7 + 1)
          end
          tempPos = instring(':',TryServer,1,1)
          if tempPos = 0
            compile ('****', NETTALKLOG=1)
              if self.LoggingOn
                self.Log ('NetWebClient.Open', 'Having to guess Port of the Secure Proxy (80)')
              end
            ****
            ! Leave TryServer
            TryPort = 80  ! Default
          else
            if len (clip (TryServer)) > tempPos
              TryPort = TryServer [(tempPos + 1) : len (clip(TryServer))]
            else
              compile ('****', NETTALKLOG=1)
                if self.LoggingOn
                  self.Log ('NetWebClient.Open', '(2)Having to guess Port of the Secure Proxy (80)')
                end
              ****
              TryPort = 80
            end
            if len (clip(Tryserver)) > tempPos
              TryServer = TryServer[1 : (tempPos - 1)]
            end
          end
        else ! SSL = 0
          tempPos = instring ('http=', lower (TryServer),1,1)
          if tempPos
            ! The Registry entry is something like this:
            ! ftp=http://earth22:8022;gopher=http://earth33:8033;http=earth:80;https=http://earth11:11
            TryServer = sub(TryServer, (tempPos+5), len (clip (TryServer)) - (tempPos+5) + 1)
            tempPos = instring (';', TryServer,1,1)
            if tempPos
              ! There is a ;
              TryServer = sub (TryServer, 1, (tempPos-1))
            end
          end

          if lower (sub(TryServer, 1, 7)) = 'http://'
            TryServer = sub(TryServer, 8, len (clip (TryServer)) - 7 + 1)
          end
          tempPos = instring(':',TryServer,1,1)
          if tempPos = 0
            compile ('****', NETTALKLOG=1)
              if self.LoggingOn
                self.Log ('NetWebClient.Open', 'Having to guess Port of the Proxy (80)')
              end
            ****
            ! Leave TryServer
            TryPort = 80  ! Default
          else
            if len (clip (TryServer)) > tempPos
              TryPort = TryServer [(tempPos + 1) : len (clip(TryServer))]
            else
              compile ('****', NETTALKLOG=1)
                if self.LoggingOn
                  self.Log ('NetWebClient.Open', '(2)Having to guess Port of the Proxy (80)')
                end
              ****
              TryPort = 80
            end
            if len (clip(Tryserver)) > tempPos
              TryServer = TryServer[1 : (tempPos - 1)]
            end
          end
        end

        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetWebClient.Open', 'Proxy is '& clip (TryServer) & ':' & TryPort)
          end
        ****
      end
    end
  end

  if (TryServer = '') or (ProxyFound = 0)                              ! No Proxy found, try connection directly
    TryServer = Server
    TryPort = Port
    ProxyFound = false
  end

!------------------------------------------------------------------------------
NetWebClient.PageReceived PROCEDURE  ()                    ! Declare Procedure 3
  CODE
  if self.OptionAutoCookie
    self._ReadCookies()
    self._MakeCookies() ! primes self.cookie
  end

!------------------------------------------------------------------------------
NetWebClient.Post    PROCEDURE  (String p_Url,String p_PostString) ! Declare Procedure 3
local           group, pre (loc)
HostName          string(256)     ! e.g if URL = www.capesoft.com/index.htm HostName = www.capesoft.com
Hostport          NET:PortType
_Spacer1          NET:PortType
StoreSuppress     long
ourURL            string (URLLENGTH)
                end


  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.Post', 'Post called on ' & clip (p_Url))
    end
  ****

  if p_PostString = ''
    self.MakePostString()
  else
    if not self._postString &= Null
      dispose(self._postString)
    End
    self._postString &= new(string(len(clip(p_PostString))))
    self._postString = clip(p_PostString)
  end

  self.error = 0                               ! Clear error flag
  self.WinSockError = 0
  self.SSLError = 0

  self._TempRedirectURL = p_Url
  self._CommandURL = self.GetURL(p_Url)        ! Add 'http://' or trailing '/' to it if neccessary

  self._CheckForHTTPS(self._CommandURL)        ! New 26/4/2004 - turns on SSL if HTTPS is being used

  if p_Url = ''
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Post', 'No Server specified')
      end
    ****
    self.error = ERROR:NoServers
    self.ErrorTrap ('The URL for the Fetch command was blank', 'NetWebClient.Fetch')
    return
  end

  loc:HostName = self.getURLHostName (p_Url, loc:HostPort)  ! Just get the hostname of the URL

  if self.AsyncOpenUse = 0
    Do Old_SynchronousOpen_Open
  else
    ! AsynOpen
    if self.openflag = 1
      self.abort()
    end
  end

  self.Host = loc:HostName                    ! Populate the Host Property with the Hostname (HTTP 1.0 Protocol)

  clear(self.Packet)

  self._Command = ''
  self._CommandType = Net:WebClientPost

  if self._postString &= Null
    self.ContentLength = 0
  else
    self.ContentLength = len(clip(self._postString)) ! bj 4.44 13/1/2010 - explicit override
  end
  if (self.AsyncOpenUse = 0)
    Do Old_SynchronousOpen_Command
  else
    self._Command = self._CreateRequestHeader(loc:HostPort)
  end

  if self.ContentLength + len(clip(self._Command)) + 4 + size (self._CommandURL) > Net:MaxBinData
    ! Data too large to fit in one packet
    if ~(self._CommandLarge &= NULL)
      dispose (self._CommandLarge)
    end
    self._CommandLarge &= new(string(self.ContentLength + len(clip(self._Command)) + 0)) ! Nov09: Changed to +4. Adding 8 bytes creates an unused 4 bytes, which means that the content-length will be incorrect, breaking POSTS for servers that enforce it. ! bj changed from 4 to 0
    if self._CommandLarge &= NULL
      self.error = ERROR:CouldNotAllocMemory
      self.errortrap('Error. Could not allocate memory for self._CommandLarge', 'NetWebClient.Post')
      return
    end
    self._UseCommandLarge = true
    self._CommandLarge = clip(self._Command) & self._postString !& '<13,10>' & '<13,10>' ! bj commented 4.44 13/1/2010
  else
    self._UseCommandLarge = false
    if not self._postString &= NULL
      self._Command = clip(self._Command) & self._postString !& '<13,10>' & '<13,10>' ! bj commented 4.44 13/1/2010
    end
    if len (clip (self._Command)) > NET:MaxBinData
      self.error = ERROR:InvalidStructureSize
      self.errortrap('Trying to send too much data', 'NetWebClient.Post')
      return
    end
  end

  self._ResetPage()

  if (self.AsyncOpenUse = 0)! or (loc:AsyncOpen_ConnectionOpen = 1)
    Do Old_SynchronousOpen_Send
  else
    ! Async Open
    Do AsyncOpen
  end

  return


! ------------------------------------------------
AsyncOpen Routine
  if self.AsyncOpenBusy
    self.abort()
  end
  if self.AsyncOpenTimeout = 0
    self.AsyncOpenTimeout = 900 ! 9 seconds
  end
  ! Open using Async Open
  self.Open (loc:HostName, loc:HostPort)
  ! Packet will be sent in .Process()


! -------------------------------------------------------
! -------------------------------------------------------
! -------------------------------------------------------

Old_SynchronousOpen_Open Routine
  loc:StoreSuppress = self.SuppressErrorMsg   ! Store current error suppression option
  self.SuppressErrorMsg = 1                   ! Suppress errors
  self.Open(loc:HostName, loc:HostPort)       ! Try and open the site (may try Proxy as well)
  self.SuppressErrorMsg = loc:StoreSuppress   ! Restore error suppression option
  if self.OpenFlag = 0                        ! Site could not be connected to
    if self.error = 0
      self.error = ERROR:CouldNotConnectToHost
    end
    self.ErrorTrap ('Could not connect to the website.', 'NetWebClient.Fetch')
    return
  end


! -------------------------------------------------------
Old_SynchronousOpen_Command Routine
  ! Old Synchronous Open
  self._Command = self._CreateRequestURI('POST ') & self._CreateRequestHeader(loc:HostPort)

! -------------------------------------------------------
Old_SynchronousOpen_Send Routine
  self._SendCommand()
  if self.error = ERROR:ClientNotConnected        ! The connection was closed before before this object found out.
    if self.AsyncOpenUse = 0
      loc:StoreSuppress = self.SuppressErrorMsg   ! Store current error suppression option
      self.SuppressErrorMsg = 1                   ! Suppress errors
      self.Open(loc:HostName, loc:HostPort)       ! Try and open the site (may try Proxy as well)
      self.SuppressErrorMsg = loc:StoreSuppress   ! Restore error suppression option
      if self.OpenFlag = 0                        ! Site could not be connected to
        if self.error = 0
          self.error = ERROR:CouldNotConnectToHost
        end
        self.ErrorTrap ('Could not connect to the website.', 'NetWebClient.Fetch')
        return
      end
      self.error = 0                              ! Clear error flag
      self.send()                                 ! Try and send again
    else
      DO AsyncOpen
    end
  end


! -------------------------------------------------------

!------------------------------------------------------------------------------
NetWebClient.Process PROCEDURE  ()                         ! Declare Procedure 3
local          group, pre(loc)
startPos         Long
endPos           Long
endHeaderPos     long
str7             string(7)
               end

  CODE
  case self.Packet.PacketType
  !-------------
  of NET:SimpleAsyncOpenSuccessful
  !-------------
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Process', 'Async Open Successful')
      end
    ****
    self._SendCommand()

  !-------------
  of NET:SimpleIdleConnection
  !-------------
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Process', 'Idle Connection')
      end
    ****
    self.Abort() ! Close connection
    self.error = ERROR:IdleTimeOut
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap('Connection Timed Out', 'NetWebClient.Process')

  !-------------
  of NET:SimplePartialDataPacket
  !-------------
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Process', 'Packet Received')
      end
    ****

    if self._PageContentLen = 0
      Do CheckForContentLength
    end

    if (self._NoHeaderYet = 1) and (self._FirstPacket = 1)
      if self.packet.binDataLen >= 5
        if lower (self.packet.binData[1:5]) <> 'http/'        ! First characters received are not a valid header
          self._NoHeaderYet = 0                               ! There is no header
          self.HeaderLen = 0                                  ! Header ends at character 0
        end
      end
    end


    if self._NoHeaderYet = 1
      loc:endHeaderPos = instring ('<13,10><13,10>', self.packet.binData [1 : self.packet.binDataLen],1,1)
      if loc:endHeaderPos = 0                                 ! End of header not found
        self.HeaderLen += self.packet.binDataLen              ! Header size so far
      else                                                    ! End of Header found
        self.HeaderLen += loc:endHeaderPos + 3                ! Total header size
        self._NoHeaderYet = 0
      end
    end

    Do ResizeMemory

    Do StoreData

    self._FirstPacket = false

    Do WorkOutIfFinished
  end


! ---------------------------------------------------------------------------------
WorkOutIfFinished Routine
  if self._ProxyUsed and self.SSL and self._UseSSLOverHTTPProxy and self._SSLDontConnectOnOpen and self._UseSSLOverHTTPProxyConnectSent and self._UseSSLOverHTTPProxyFirstPacketDone = 0
    if self._NoHeaderYet = 0
      Do Finished
      self._UseSSLOverHTTPProxyFirstPacketDone = 1 ! Must be set after the Do Finished !
    end
  elsif self.HeaderOnly and self._NoHeaderYet = 0
    Do Finished
  elsif self._PageContentLen < 0  ! 5.39
    Do Finished                   ! 5.39
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Process', |
                  '_PageContentLen = ' & self._PageContentLen & |
                  ' PageLen = ' & self.PageLen & |
                  ' HeaderLen = ' & self.HeaderLen & |
                   ' [' & clip (loc:str7) & ']')
      end
    ****
    if self._PageContentLen = 0
      if self.OptionFinishOnEndHTMLTag = 0
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetWebClient.Process', |
                      'Expected _PageContentLen = ' & self._PageContentLen & |
                      ' PageLen (so far) = ' & self.PageLen & |
                      ' HeaderLen = ' & self.HeaderLen & |
                      ' Will need a close connection to determine if the page has finished.')
          end
        ****
      else
        if self.PageLen >= 7
          loc:str7 = lower(self.Page[(self.PageLen-7+1):self.PageLen])
        else
          loc:str7 = ''
        end
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetWebClient.Process', |
                      'Expected _PageContentLen = ' & self._PageContentLen & |
                      ' PageLen (so far) = ' & self.PageLen & |
                      ' HeaderLen = ' & self.HeaderLen & |
                       ' Last 7 chars = [' & clip (loc:str7) & ']')
          end
        ****
        if clip (loc:str7) = '<</html>' ! note does not check for </html><13,10> - But I don't know why this would help anyway.
          Do Finished
        end
      end
    else
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetWebClient.Process', |
                    'Expected _PageContentLen = ' & self._PageContentLen & |
                    ' PageLen (so far) = ' & self.PageLen & |
                    ' HeaderLen = ' & self.HeaderLen)
        end
      ****
      if self.PageLen >= (self._PageContentLen + self.HeaderLen)
        Do Finished
      end
    end
  end

! ---------------------------------------------------------------------------------

Finished Routine
  ! Changes to this routine must also be made in ConnectionClose()
  if self.busy = 1
    self.busy = 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Process', 'About to call self._CallPageReceived(). PageLen = ' & self.PageLen)
      end
    ****
    self._CallPageReceived()
  end

! ---------------------------------------------------------------------------------

StoreData Routine
  if (self.PageLen + self.packet.binDataLen) > self._AllocatedPageLen
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.Process', 'Error. Insufficient memory to store web page.', NET:LogError)
      end
    ****
    exit
  end
  self.page [(self.pagelen + 1) : (self.PageLen + self.packet.BinDataLen)] = self.packet.binData [1 : self.packet.binDataLen]
  self.PageLen += self.packet.binDataLen

! ---------------------------------------------------------------------------------

ResizeMemory Routine
  if (self._PageContentLen > 0) and (self.HeaderOnly = 0)
    if self._AllocatedPageLen < (self._PageContentLen + self.HeaderLen)
      self._ExpectedPageLen = self._PageContentLen + self.HeaderLen
      self.MoreMemoryForPage()
    end
  end
  if (self.PageLen + self.packet.binDataLen) > self._AllocatedPageLen
    self._ExpectedPageLen = (self.PageLen + self.packet.binDataLen)
    self.MoreMemoryForPage()
  end

! ---------------------------------------------------------------------------------

CheckForContentLength Routine
  loc:startPos = instring('content-length: ',lower (self.packet.binData [1 : self.packet.binDataLen]),1,1)
  if loc:startPos <> 0
    loc:StartPos += 16
    loc:endPos = instring('<13,10>',self.packet.binData [1 : self.packet.binDataLen],1,loc:startPos)
    self._PageContentLen = sub(self.packet.binData [1 : self.packet.binDataLen],loc:startPos,(loc:endPos-loc:startPos))
    if self._PageContentLen = 0 then self._PageContentLen = -1.  ! 5.39
  end

!------------------------------------------------------------------------------
NetWebClient.SavePage PROCEDURE  (string p_FileName)       ! Declare Procedure 3
Local     group, pre (loc)
lSize       long
cStr        CString(FILE:MAXFILENAME)
hFile       long ! 20/7/2005 - was ulong  ! was previously HANDLE      !UNSIGNED
Result      long
Result2     long
Written     long ! 20/7/2005 - was ulong !DWORD        !ULONG
          end
  CODE
  !Get size of actual data buffer (without header)
  loc:lSize = self.PageLen - self.HeaderLen

  if loc:lSize <= 0
    self.error = ERROR:AcceptedDataTooShort  ! The data is too small to save (ie 0 bytes or less)
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('There is no data to save', 'NetWebClient.SavePage')
    return
  end

  !Trim header from data buffer

  !Prepare filename
  loc:cStr = clip(p_FileName)

  !Create the file
  loc:hFile = OS_CreateFile(loc:cStr, NET:OS:GENERIC_WRITE, NET:OS:FILE_SHARE_READ, 0, NET:OS:CREATE_ALWAYS, 0, 0)
  !If the file was created and we have a handle for it

  !Assign "0" Bytes Written to our object property
  self.BytesWritten = 0

  if loc:hFile
    !Write the data to the file using the Windows API
    if OS_WriteFile(loc:hFile, SELF.Page[self.HeaderLen + 1], loc:lSize, loc:Written,0)
      !Assign the Bytes Written to our object property
      self.BytesWritten = loc:Written
    end
    !Close the newly created file
    self.error = 0
    self.WinSockError = 0
    self.SSLError = 0
    loc:result = OS_FlushFileBuffers(loc:hFile)  ! 0 return result = Failure
    loc:result2 = OS_CloseHandle(loc:hFile)      ! 0 return result = Failure
    if loc:result = 0 or loc:result2 = 0
      self.error = ERROR:FileAccessError
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Could not save the page to disk', 'NetWebClient.SavePage')
    end
  else
    self.error = ERROR:FileAccessError
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Could not save the page to disk', 'NetWebClient.SavePage')
  end


!------------------------------------------------------------------------------
NetWebClient.SetAllHeadersDefault PROCEDURE  ()            ! Declare Procedure 3
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.SetAllHeadersDefault', 'Called')
    end
  ****
  self.DontErrorTrapInSendIfConnectionClosed = 1    ! New 31/1/2002 - we want to handle these errors ourself. This gets rid of the 10038 errors

  self.Accept_ = self._Accept_
  self.Referer = ''
  self.AcceptLanguage = self._AcceptLanguage
  self.ContentType = ''
  self.AcceptEncoding = ''             ! can be set to things like 'gzip, deflate'
  self.IfModifiedSince = ''
  self.IfNoneMatch = ''
  self.UserAgent = self._UserAgent

  self.ContentLength = ''
  self.Pragma_ = ''
  self.CacheControl = ''
  self.Cookie = ''
  self.ProxyConnectionKeepAlive = 1
  self.ConnectionKeepAlive = 1

  self.SSL = 0 ! By Default SSL is off - it's turned on automatically if needed (e.g. if there's a https in the web url)

  self.SSLCertificateOptions.DontVerifyRemoteCertificateCommonName = 1
  self.SSLCertificateOptions.DontVerifyRemoteCertificateWithCARoot = 1
  self.SSLCertificateOptions.CARootFile = 'ca_roots.pem'


!------------------------------------------------------------------------------
NetWebClient.TextOnly PROCEDURE  ()                        ! Declare Procedure 3
CASE_INSENSITIVE equate (1)
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.TextOnly', 'Called')
    end
  ****

  if self.PageLen > 0
    ! Strip off Header
    if (self.HeaderLen > 0) and ((self.HeaderLen + 1) <= self.PageLen)
      self.Page = self.Page [self.HeaderLen + 1 : self.PageLen]
      self.PageLen -= self.HeaderLen
      self.HeaderLen = 0
    end

    self._StripOut (self.Page, self.PageLen, '<<style', '<</style>', CASE_INSENSITIVE)
    self._StripOut (self.Page, self.PageLen, '<<script', '<</script>', CASE_INSENSITIVE)
    self._StripOut (self.Page, self.PageLen, '<<', '>')

    self._StripOutWhiteSpace (self.Page, self.PageLen)

    self._Expand_nbsp (self.Page, self.PageLen)
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.TextOnly', 'Called')
    end
  ****

!------------------------------------------------------------------------------
NetWebClient.UnicodeToAnsi PROCEDURE  ()                   ! Declare Procedure 3
MyUnicode           Class (_NetUnicode)
                    End

PageSize            long
TempString          &String

  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient.UnicodeToAnsi', 'Converting Page from Unicode to ANSI')
    end
  ****
  PageSize = self.PageLen - self.HeaderLen
  if PageSize = 0
    return
  end
  MyUnicode.UnicodeCString &= New (CString (PageSize+2))
  MyUnicode.UnicodeCString = self.Page[self.HeaderLen + 1 : self.PageLen] & '<0><0>'
  MyUnicode.UnicodeCSTringToAnsiCString()
  if MyUnicode.AnsiCStringLen = 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient.UnicodeToAnsi', 'Warning. Could not convert from UnicodeCString To CString.')
      end
    ****
    dispose (MyUnicode.UnicodeCString)
    return
  end
  if self.HeaderLen + MyUnicode.AnsiCStringLen > size(self.Page)
    TempString &= self.Page
    self.Page &= New (String(self.HeaderLen + MyUnicode.AnsiCStringLen))
    if self.HeaderLen > 0
      self.Page [1 : self.HeaderLen] = TempString [1 : self.HeaderLen]
    end
    dispose (TempString)
  end
  self.Page[self.HeaderLen + 1 : self.HeaderLen+MyUnicode.AnsiCStringLen] = MyUnicode.AnsiCString[1 : MyUnicode.AnsiCStringLen]
  self.PageLen = self.HeaderLen + MyUnicode.AnsiCStringLen

  dispose (MyUnicode.UnicodeCString)
  dispose (MyUnicode.AnsiCString)
!------------------------------------------------------------------------------
NetWebClient._CallErrorTrap PROCEDURE  (string errorStr,string functionName) ! Declare Procedure 3
  CODE
  if self.packet.PacketType = NET:SimpleAsyncOpenFailed
    if self._HandleAsyncOpenFailed() = 1
      return ! Don't call Parent _CallErrorTrap
    end
  end
  parent._CallErrorTrap (errorStr, FunctionName)

!------------------------------------------------------------------------------
NetWebClient._CallPageReceived PROCEDURE  ()               ! Declare Procedure 3
local           group, pre(loc)
x                 long
y                 long
CRPos             long
tempLen           long
                end

  CODE
  ! ------------------------
  ! Get Server Number from the bit that looks like this
  ! HTTP/1.1 200 OK
  self.ServerResponse = 0
  if self.HeaderLen > 0 and self.HeaderLen <= size (self.Page)
    loc:CRPos = instring ('<13,10>', self.Page [1 : self.HeaderLen],1,1)
    if loc:CRPos >= 2
      loc:x = instring ('HTTP/1.', upper (self.Page[1 : loc:CRPos]),1,1)
      if loc:x > 0
        loc:x = instring (' ', self.Page[1 : loc:CRPos],1,1)         ! First Space
        if loc:x > 0
          loc:y = instring (' ', self.Page[1 : loc:CRPos],1,loc:x+1) ! Second Space
          ! new 1  April 2005 (April Fools!) -
          ! Fix if the server returns "HTTP/1.1 302" instead of "HTTP/1.1 302 Moved Temporarily"
          if loc:y = 0
            if (loc:CRpos-1) >= (loc:x+1)
              loc:y = loc:CRPos
            end
          end
          ! end 1 April 2005
          if loc:y > 0
            if (loc:y-1) >= (loc:x+1) and (loc:y-1) <= self.HeaderLen
              self.ServerResponse = self.Page [(loc:x+1) : (loc:y-1)]
            end
          end
        end
      end
    end
  end
  ! ------------------------

  if self._ProxyUsed and self.SSL and self._UseSSLOverHTTPProxy and self._SSLDontConnectOnOpen and self._UseSSLOverHTTPProxyFirstPacketDone = 0
    if self.ServerResponse >= 200 and self.ServerResponse <= 299
      self._ResetPage()  ! Reset the page, so that the next response becomes the received page, not the 200 OK that we just received to tell us we can enter secure mode
      self._SSLSwitchToSSL() ! This will call .Process with AsyncOpenSuccessful when/if it succeeds
    end
  end


  ! ------------------------
  if self.ServerResponse > 300 and self.ServerResponse <= 399
    if self.OptionDontRedirect = 0 and self.HeaderOnly = 0 and self._CommandType = Net:WebClientFetch
      ! Only do this for a GET as long as the Options hasn't been turned off
      self._TempRedirectURL = self.GetHeaderField ('<13,10>LOCATION: ')

      !--- Sean August 2006: IIS seems to sometimes incorrectly set the location header to a relative URL.
      !    This is not RFC compliant, but we'll handle it anyway.
      if not Instring('http://', Lower(self._TempRedirectURL), 1, 1) and not Instring('https://', Lower(self._TempRedirectURL), 1, 1)
        ! Need to append Location to the original URL
        loc:tempLen = Len(Clip(self._LastURLRequested))

        if self._LastURLRequested[loc:tempLen] = '/' and self._TempRedirectURL[1] = '/'         ! both have a slash, only one is needed
            self._TempRedirectURL = Clip(self._LastURLRequested) & self._TempRedirectURL[2 : Len(Clip(self._TempRedirectURL))]
        elsif self._LastURLRequested[loc:tempLen] = '/' or self._TempRedirectURL[1] = '/'       ! only 1 has a slash
            self._TempRedirectURL = Clip(self._LastURLRequested) & Clip(self._TempRedirectURL)
        else                                                                                    ! neither has a slash, so insert one to seperate the sections of the URL
            self._TempRedirectURL = Clip(self._LastURLRequested) & '/' & Clip(self._TempRedirectURL)
        end
      end
      !----

      if self.OptionDontSetCookieOnRedirect = 0
        self.Cookie = clip (self.Cookie) & self.GetHeaderField ('<13,10>SET-COOKIE: ')
        loc:tempLen = len (clip(self.Cookie))
        if lower(sub (self.Cookie, loc:tempLen-5, 6)) = 'path=/'
          self.Cookie = sub (self.Cookie, 1, loc:tempLen-6)
        end
      end
      if self._TempRedirectURL <> '' and self._TempRedirectURL <> self._LastURLRequested
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetWebClient._CallPageReceived', 'About to redirect (' & self.ServerResponse & ') to ' & clip (self._TempRedirectURL))
          end
        ****
        if self.OpenFlag or self.AsyncOpenBusy ! Prevent recursive calls to .ErrorTrap if Abort fails
          self.abort()
        end
        self.Fetch (self._TempRedirectURL)
        return
      end
    end
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebClient._CallPageReceived', 'About to call self.PageReceived(). PageLen = ' & self.PageLen & ' ServerResponse = ' & self.ServerResponse)
    end
  ****
  self.PageReceived()
!------------------------------------------------------------------------------
NetWebClient._CheckForHTTPS PROCEDURE  (string p_URL)      ! Declare Procedure 3
  CODE
  if sub(lower(p_URL),1,8) = 'https://'
    self.SSL = 1 ! turn on SSL mode      ! Is set back to 0 in NetWebClient.SetAllHeadersDefault()
  end
!------------------------------------------------------------------------------
NetWebClient._ConnectingOkay PROCEDURE  ()                 ! Declare Procedure 3
  CODE
  if self._ProxyUsed = 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        if self.AsyncOpenUse
          self.Log ('NetWebClient._ConnectingOkay', 'Connecting (asynchronously) to directly to Web Server ('& clip (self._HostServer) & ':' & self._HostPort & ')')
        else
          self.Log ('NetWebClient._ConnectingOkay', 'Connected to directly to Web Server ('& clip (self._HostServer) & ':' & self._HostPort & ')')
        end
      end
    ****
  else
    self._ProxyUsed = 1
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        if self.AsyncOpenUse
          self.Log ('NetWebClient._ConnectingOkay', 'Connecting (asynchronously) to Proxy Server ('& clip (self._ProxyServer) & ':' & self._ProxyPort & ')')
        else
          self.Log ('NetWebClient._ConnectingOkay', 'Connected to Proxy Server ('& clip (self._ProxyServer) & ':' & self._ProxyPort & ')')
        end
      end
    ****
  end


!------------------------------------------------------------------------------
NetWebClient._CreateRequestHeader PROCEDURE  (long p_HostPort) ! Declare Procedure 3
loc:tempCString       cstring (NET:MaxBinData)
  CODE
  if self.Accept_ <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Accept: ' & clip (self.Accept_)
  end

  if self.Referer <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Referer: ' & clip (self.Referer)
  end

  if self.AcceptLanguage <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Accept-Language: ' & clip (self.AcceptLanguage)
  end

  if self.Authorization <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Authorization: ' & clip (self.Authorization)
  end

  if self.ProxyAuthorization <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Proxy-Authorization: ' & clip (self.ProxyAuthorization)
  end

  if self.ContentType <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Content-Type: ' & clip (self.ContentType)
  end

  if self.AcceptEncoding <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Accept-Encoding: ' & clip (self.AcceptEncoding)
  end

  if self.IfModifiedSince <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'If-Modified-Since: ' & clip (self.IfModifiedSince)
  end

  if self.IfNoneMatch <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'If-None-Match: ' & clip (self.IfNoneMatch)
  end

  if self.UserAgent <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'User-Agent: ' & clip (self.UserAgent)
  end

  if self.Host <> ''
    if p_HostPort = 80 or p_HostPort = 0 or p_HostPort = 443
      loc:tempCString = loc:tempCString & '<13,10>' & 'Host: ' & clip(self.Host)
    else
      loc:tempCString = loc:tempCString & '<13,10>' & 'Host: ' & clip(self.Host) & ':' & p_HostPort
    end
  end

  if self.ContentLength <> 0
    loc:tempCString = loc:tempCString & '<13,10>' & 'Content-Length: ' & self.ContentLength
  end

  if self.Pragma_ <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Pragma: ' & clip (self.Pragma_)
  end

  if self.CacheControl <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Cache-Control: ' & clip (self.CacheControl)
  end

  if records(self._cookieQueue)
    self._MakeCookies()
  end
  if self.Cookie <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & 'Cookie: ' & clip(self.Cookie)
  end

  if self._ProxyUsed
    if self.ProxyConnectionKeepAlive
      loc:tempCString = loc:tempCString & '<13,10>' & 'Proxy-Connection: Keep-Alive'
    else
      loc:tempCString = loc:tempCString & '<13,10>' & 'Proxy-Connection: Close'
    end
  else
    if self.ConnectionKeepAlive
      loc:tempCString = loc:tempCString & '<13,10>' & 'Connection: Keep-Alive'
    else
      loc:tempCString = loc:tempCString & '<13,10>' & 'Connection: Close'
    end
  end

  if self.CustomHeader <> ''
    loc:tempCString = loc:tempCString & '<13,10>' & clip(self.CustomHeader)
  end

  loc:tempCString = loc:tempCString & '<13,10>' & '<13,10>'  ! Header needs two NewLines

  return (loc:tempCString)

!------------------------------------------------------------------------------
NetWebClient._CreateRequestURI PROCEDURE  (STRING p_Method) ! Declare Procedure 3
loc:tempString        like (self._CommandURL)
loc:RemoveHTTP        long
x                     long
sl                    long
  CODE
  ! When you connect directly (ie via no Proxy) strip off the "http://domain"

  !if self._ProxyUsed = 0    ! Changed Jono 31/8/2004 - was if self._ProxyUsed = 1
  if self._ProxyUsed = 0 or (self._ProxyUsed and self.SSL and self._UseSSLOverHTTPProxy and self._SSLDontConnectOnOpen)   ! Changed Jono 15/03/2006
    if lower (sub(self._CommandURL,1,7)) = 'http://'
      loc:RemoveHTTP = 1
      x = instring ('/',self._CommandURL,1,8)
      if x = 0
        x = 8
      end
    end
    if lower (sub(self._CommandURL,1,8)) = 'https://'
      loc:RemoveHTTP = 1
      x = instring ('/',self._CommandURL,1,9)
      if x = 0
        x = 9
      end
    end
  end
  if loc:RemoveHTTP
    sl = len(clip (self._CommandURL))
    if sl >= x
      loc:tempString = p_Method & self._CommandURL [x : sl] & ' ' & clip (self._HTTPVersion)
    else
      ! This should never happen
      loc:tempString = p_Method & '/ ' & clip (self._HTTPVersion)
    end
  else
    loc:tempString = p_Method & clip(self._CommandURL) & ' ' & clip (self._HTTPVersion)
  end

  return (clip (loc:tempString))

!------------------------------------------------------------------------------
NetWebClient._Expand_nbsp PROCEDURE  (*string p_String, *long p_StringLen) ! Declare Procedure 3
local       group, pre(loc)
Pos1          long
Pos2          long
LastPos1      long
Biggest       long
            end

  CODE
  if p_StringLen > 0
    loc:Pos1 = 1
    loop
      loc:LastPos1 = loc:Pos1
      if loc:Pos1 > p_StringLen ! Jan 2005 Index Range Fix
        break
      end
      loc:Pos1 = instring ('&nbsp;', p_String [loc:Pos1 : p_StringLen], 1, 1)
      if (loc:Pos1 = 0)
        break
      end
      loc:Pos1 += (loc:LastPos1 - 1)
      if (1 > (loc:Pos1 - 1)) or ((loc:Pos1 + 6) > p_StringLen) ! Jan 2005 Index Range Fix
        break
      end
      p_String = p_String [1 : (loc:Pos1 - 1)] & ' ' & p_String [(loc:Pos1 + 6) : p_StringLen]
      p_StringLen -= 5
    end
  end

!------------------------------------------------------------------------------
NetWebClient._HandleAsyncOpenFailed PROCEDURE  ()          ! Declare Procedure 3
  CODE
  if self._ProxyUsed
    ! It was the proxy that did not open. We will try and connect directly to the host
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient._HandleAsyncOpenFailed', 'Could not open proxy, will try open connection directly.')
      end
    ****
    self._ProxyUsed = 0 ! Try and connect direct to the Web Server
    self._TryOpen()
    if self.openFlag = 1 or self.AsyncOpenBusy = 1
      ! Connecting directly okay
      self._ConnectingOkay()
      return (1) ! Don't call ._CallErrorTrap()
    else
      ! Neither the proxy or connecting directly worked
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetWebClient._HandleAsyncOpenFailed', '(2) Could not connect via Proxy or directly to Web Server ' & clip (self._HostServer) & ':' & self._HostPort)
        end
      ****
      Do HandleError
    end
  else
    ! The site could not be opened
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient._HandleAsyncOpenFailed', 'Could not connect directly to Web Server ' & clip (self._HostServer) & ':' & self._HostPort)
      end
    ****
    Do HandleError
  end
  return (0) ! Call ._CallErrorTrap


! ---------------------------------------------------------------------------------
HandleError Routine
  self._CurrentServer = ''
  self._CurrentPort = ''
  ! Commented out 7/7/2004
  ! As this function returns to _CallErrorTrap which calls Parent._CallErrorTrap
  ! which in turn calls .ErrorTrap
  !self.ErrorTrap ('The requested connection could not be opened.', 'NetWebClient._HandleAsyncOpenFailed')


!------------------------------------------------------------------------------
NetWebClient._SendCommand PROCEDURE  ()                    ! Declare Procedure 3
local           group, pre(loc)
SizeLarge         long
CurrentPos        long
tempString        like (self._CommandURL)
tempStringLen     long
tempCString       cstring(Net:MaxBinData)
                end
  CODE
  if self._ProxyUsed and self.SSL and self._UseSSLOverHTTPProxy and self._SSLDontConnectOnOpen
    if self._UseSSLOverHTTPProxyConnectSent = 0
      ! Build up a CONNECT request
      loc:tempCString = 'CONNECT ' & clip(self.Host) & ':' & self._HostPort & ' ' & clip (self._HTTPVersion)
      if self.UserAgent <> ''
        loc:tempCString = loc:tempCString & '<13,10>' & 'User-Agent: ' & clip (self.UserAgent)
      end
      if self.Host <> ''
        if self._HostPort = 80 or self._HostPort = 0
          loc:tempCString = loc:tempCString & '<13,10>' & 'Host: ' & clip(self.Host)
        else
          loc:tempCString = loc:tempCString & '<13,10>' & 'Host: ' & clip(self.Host) & ':' & self._HostPort
        end
      end
      if self.ProxyAuthorization <> ''
        loc:tempCString = loc:tempCString & '<13,10>' & 'Proxy-Authorization: ' & clip (self.ProxyAuthorization)
      end
      loc:tempCString = loc:tempCString & '<13,10>' & 'Content-Length: 0'
      loc:tempCString = loc:tempCString & '<13,10>' & 'Proxy-Connection: Keep-Alive'
      !if self.Pragma_ <> ''
        !loc:tempCString = loc:tempCString & '<13,10>' & 'Pragma: ' & clip (self.Pragma_)
        loc:tempCString = loc:tempCString & '<13,10>' & 'Pragma: no-cache'
      !end
      if self.CacheControl <> ''
        loc:tempCString = loc:tempCString & '<13,10>' & 'Cache-Control: ' & clip (self.CacheControl)
      end
      loc:tempString = loc:tempCString & '<13,10>' & '<13,10>'  ! Header needs two NewLines
      self._UseSSLOverHTTPProxyConnectSent = 1
      self.packet.BinData = clip (loc:tempString)
      self.packet.BinDataLen = len(clip(self.packet.BinData))
      Do Send16
      return
    end
  end


  case self._CommandType
  ! --------------
  of Net:WebClientFetch
  ! --------------
    if self.HeaderOnly
      loc:tempString = self._CreateRequestURI('HEAD ')
    else
      loc:tempString = self._CreateRequestURI('GET ')
    end
  ! --------------
  of Net:WebClientPost
  ! --------------
    loc:tempString = self._CreateRequestURI('POST ')
  end

  loc:tempStringLen = len (clip(loc:tempString))

  if loc:tempStringLen = 0
    if self.OpenFlag or self.AsyncOpenBusy ! Prevent recursive calls to .ErrorTrap if Abort fails
      self.Abort() ! Close connection
    end
    self.ErrorTrap('Invalid Data to Send (in tempString)', 'NetWebClient._SendCommand')
    return
  end


  if self._UseCommandLarge = false
    self.packet.BinData = clip (loc:tempString) & self._Command
    self.packet.BinDataLen = len(clip(self.packet.BinData))
    Do Send16
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetWebClient._SendCommand', 'Sending Large Multi-Packet command')
      end
    ****
    self._UseCommandLarge = false ! reset for next time
    loc:CurrentPos = 0
    loc:SizeLarge = size(self._CommandLarge)
    if loc:SizeLarge = 0
      if self.OpenFlag or self.AsyncOpenBusy ! Prevent recursive calls to .ErrorTrap if Abort fails
        self.Abort() ! Close connection
      end
      self.ErrorTrap('Invalid Data to Send', 'NetWebClient._SendCommand')
      return
    end
    loop
      if loc:CurrentPos >= loc:SizeLarge
        break
      elsif (loc:CurrentPos + Net:MaxBinData - loc:tempStringLen) <= loc:SizeLarge
        self.packet.BinData = self._CommandLarge [(loc:CurrentPos+1) : (loc:CurrentPos + Net:MaxBinData - loc:tempStringLen)] ! Change 2/2/2005
        self.packet.BinDataLen = Net:MaxBinData - loc:tempStringLen
      else
        self.packet.BinData = self._CommandLarge [(loc:CurrentPos+1) : loc:SizeLarge]
        self.packet.BinDataLen = loc:SizeLarge - loc:CurrentPos
      end
      loc:CurrentPos += self.packet.BinDataLen
      if loc:tempStringLen > 0
        self.packet.binData = loc:tempString[1 : loc:tempStringLen] & self.packet.binData
        self.packet.binDataLen += loc:tempStringLen
        loc:tempStringLen = 0 ! Clear this now, as our data is being sent out in binData now.
      end
      self.send()
    end
    if ~(self._CommandLarge &= NULL)
      dispose (self._CommandLarge)
    end
  end


!---------------------------
Send16 Routine
  if self.packet.BinDataLen = 0
    if self.OpenFlag or self.AsyncOpenBusy ! Prevent recursive calls to .ErrorTrap if Abort fails
      self.Abort() ! Close connection
    end
    self.ErrorTrap('Invalid Data to Send', 'NetWebClient._SendCommand')
  else
    self.send()
  end

!------------------------------------------------------------------------------
NetWebClient._StripOut PROCEDURE  (*string p_String, *long p_StringLen, string p_First, string p_Second, long p_CaseInSensitive=0) ! Declare Procedure 3
!-----------------------------------------------------------------------
! Note:   p_First and p_Second must be clipped before sending.
!         In CaseInSensitive mode p_First and p_Second must be lowercase
!-----------------------------------------------------------------------


local       group, pre(loc)
Pos1          long
Pos2          long
LastPos1      long
SecondLen     long
            end

  CODE
  if p_StringLen > 0
    loc:SecondLen = len (p_Second)
    ! Strip out everything between p_First and p_Second
    loc:Pos1 = 1
    loop
      loc:LastPos1 = loc:Pos1
      if loc:Pos1 > p_StringLen ! Jan 2005 Index Range Fix
        break
      end
      if p_CaseInSensitive
        loc:Pos1 = instring (p_First, lower(p_String [loc:Pos1 : p_StringLen]), 1, 1)
      else
        loc:Pos1 = instring (p_First, p_String [loc:Pos1 : p_StringLen], 1, 1)
      end
      if (loc:Pos1 = 0) or (loc:Pos1 = p_StringLen)
        break
      end
      loc:Pos1 += (loc:LastPos1 - 1)
      if p_CaseInSensitive
        loc:Pos2 = instring (p_Second, lower(p_String [loc:Pos1 + 1 : p_StringLen]), 1, 1)
      else
        loc:Pos2 = instring (p_Second, p_String [loc:Pos1 + 1 : p_StringLen], 1, 1)
      end
      if loc:Pos2 = 0
        break
      end
      loc:Pos2 += loc:Pos1 + (loc:SecondLen - 1)
      if loc:Pos1 = 1
        if loc:Pos2 = p_StringLen
          p_String = ''
          p_StringLen = 0
        else
          p_String = p_String [(loc:Pos2 + 1) : p_StringLen]
          p_StringLen -= loc:Pos2
        end
      else
        if loc:Pos2 = p_StringLen
          p_String = p_String [1 : (loc:Pos1 - 1)]
          p_StringLen = (loc:Pos1 - 1)
        else
          p_String = p_String [1 : (loc:Pos1 - 1)] & p_String [(loc:Pos2 + 1) : p_StringLen]
          p_StringLen = (p_StringLen - loc:Pos2) + (loc:Pos1 - 1)
        end
      end
    end
  end

!------------------------------------------------------------------------------
NetWebClient._StripOutWhiteSpace PROCEDURE  (*string p_String, *long p_StringLen) ! Declare Procedure 3
local       group, pre(loc)
Pos1          long
Pos2          long
LastPos1      long
Biggest       long
            end

  CODE
  ! Strip out whitespace

  if p_StringLen <= 0
    return
  end

  loc:Biggest = 0
  loc:Pos1 = 0
  loop
    loc:Pos1 += 1
    if loc:Pos1 >= (p_StringLen - 1)
      break
    end
    if (p_String [loc:Pos1] = ' ') or (p_String [loc:Pos1] = '<13>') or (p_String [loc:Pos1] = '<10>') or (p_String [loc:Pos1] = '<9>')
      loc:Biggest = 0
      loc:Pos2 = loc:Pos1
      loop
        if (p_String [loc:Pos2] = ' ')
          if loc:Biggest < 1
            loc:Biggest = 1
          end
        elsif (p_String [loc:Pos2] = '<9>')
          if loc:Biggest < 1
            loc:Biggest = 2
          end
        elsif (p_String [loc:Pos2] = '<10>') or (p_String [loc:Pos2] = '<13>')
          if loc:Biggest < 1
            loc:Biggest = 3
          end
        else
          break ! Not whitespace
        end
        loc:Pos2 += 1
        if loc:Pos2 > (p_StringLen - 1)
          loc:Pos2 -= 1
          break
        end
      end
      if loc:Biggest = 1 ! Space
        if loc:Pos2 > (loc:Pos1+1)
          if 1 <= (loc:Pos1 - 1) ! Jan 2005 Index Range Fix
            p_String = p_String [1 : (loc:Pos1 - 1)] & ' ' & p_String [loc:Pos2 : p_StringLen]
          else
            p_String = ' ' & p_String [loc:Pos2 : p_StringLen]
          end
          p_StringLen = (loc:Pos1 - 1) + 1 + (p_StringLen - loc:Pos2 + 1)
        end
      elsif loc:Biggest = 2 ! Tab
        if loc:Pos2 > (loc:Pos1+1) ! Jan 2005 Index Range Fix
          if 1 <= (loc:Pos1 - 1)
            p_String = p_String [1 : (loc:Pos1 - 1)] & '<9>' & p_String [loc:Pos2 : p_StringLen]
          else
            p_String = '<9>' & p_String [loc:Pos2 : p_StringLen]
          end
          p_StringLen = (loc:Pos1 - 1) + 1 + (p_StringLen - loc:Pos2 + 1)
        end
      elsif loc:Biggest = 3 ! CR or LF
        if 1 <= (loc:Pos1 - 1) ! Jan 2005 Index Range Fix
          p_String = p_String [1 : (loc:Pos1 - 1)] & '<13,10>' & p_String [loc:Pos2 : p_StringLen]
        else
          p_String = '<13,10>' & p_String [loc:Pos2 : p_StringLen]
        end
        p_StringLen = (loc:Pos1 - 1) + 2 + (p_StringLen - loc:Pos2 + 1)
        loc:Pos1 += 1
      end
    end
  end

!------------------------------------------------------------------------------
NetWebClient._TryOpen PROCEDURE  ()                        ! Declare Procedure 3
loc:StoreSuppress          long
  CODE
  loc:StoreSuppress = self.SuppressErrorMsg   ! Don't show errors
  self._SSLDontConnectOnOpen = 0
  !self.SuppressErrorMsg = 1 ! bruce comented this out in 5.30
  if self._ProxyUsed
    if self._ProxyUsed and self.SSL and self._UseSSLOverHTTPProxy
      self._SSLDontConnectOnOpen = 1
      self._UseSSLOverHTTPProxyConnectSent = 0
      self._UseSSLOverHTTPProxyFirstPacketDone = 0
    end
    parent.Open(self._ProxyServer, self._ProxyPort) ! Will probably open asynchronously depening on self.AsyncOpen and self.AsynOpenTimeout
    self._CurrentServer = self._ProxyServer
    self._CurrentPort = self._ProxyPort
  else
    parent.Open(self._HostServer, self._HostPort)
    self._CurrentServer = self._HostServer
    self._CurrentPort = self._HostPort
  end
  self.SuppressErrorMsg = loc:StoreSuppress   ! Restore status
  if self.AsyncOpenUse = 0
    if self.error
      self._CurrentServer = ''
      self._CurrentPort = ''
    end
  end

!------------------------------------------------------------------------------
NetWebClient.CreateAuthorizationString PROCEDURE  (string p_UserName, string p_Password, long p_AuthorizationMethod, long p_Options=0) ! Declare Procedure 3
! This method creates a authorization string using the provided UserName and Password
! The AuthorizationMethod will be set to one of the following:
!   Net:WebBasicAuthentication - default
!   Net:WebDigestAuthentication -

RetString         string (1024),auto
TempString1       string (1024),auto
TempString1Len    long,auto
TempString2       string (1024),auto


uri               string(256),auto
realm             string(128),auto    ! Authentication realm
qop               string(32),auto     ! Quality of protection (auth|auth-int|token)
nonce             string(32),auto     ! Server nonce
opaque            string(32),auto     ! Server opaque hash
cnonce            string(32),auto     ! Client nonce


  CODE
  if p_UserName = '' and p_Password = ''
    return ('') ! Blank
  end

  RetString = ''

  case (p_AuthorizationMethod)
  ! -----------------------
  of Net:WebBasicAuthentication
  ! -----------------------
    TempString1 = clip(p_UserName) & ':' & clip(p_Password)
    TempString1Len = len (clip(TempString1)) ! Compute the length of the string to be encoded
    RetString = 'Basic ' & NetBase64Encode (clip(TempString1), TempString1Len)

  ! -----------------------
  of Net:WebDigestAuthentication
  ! -----------------------
    case p_Options
    of 1
      uri = self.AuthProxyMD5Uri
      realm = self.AuthProxyMD5Realm
      nonce = self.AuthProxyMD5Nonce
      opaque = self.AuthProxyMD5Opaque
      qop = self.AuthProxyMD5Qop
      cnonce = self.AuthProxyMD5CNonce
    else
      uri = self.AuthMD5Uri
      realm = self.AuthMD5Realm
      nonce = self.AuthMD5Nonce
      opaque = self.AuthMD5Opaque
      qop = self.AuthMD5Qop
      cnonce = self.AuthMD5CNonce
    end
    TempString1 = clip(p_UserName) & ':' & clip(realm) & ':' & clip(p_Password)
    NetMD5(TempString1, len(clip(TempString1)), TempString1)
    TempString2 = 'GET:/' & clip(uri)
    NetMD5(TempString2, len(clip(TempString2)), TempString2)
    TempString2 = clip(TempString1) & ':' & clip(nonce) & ':00000001:' & clip(cnonce) & ':auth:' & clip (TempString2)
    NetMD5(TempString2, len(clip(TempString2)), TempString2)
    RetString = 'Digest username="' & clip(p_UserName) &      |
                          '", realm="' & clip(realm) &        |
                          '", qop="auth"' &                   |
                          ', algorithm="MD5"' &               |
                          ', uri="/' & clip(uri) &            |
                          '", nonce="' & clip(nonce) &        |
                          '", nc=00000001' &                  |
                          ', cnonce="' & clip(cnonce) &       |
                          '", opaque="' & clip(opaque) &      |
                          '", response="' & clip(TempString2) & '"'
  end

  return (RetString)

!------------------------------------------------------------------------------
NetWebClient._ResetPage PROCEDURE  ()                      ! Declare Procedure 3
  CODE
  self.PageLen = 0
  self.Page = ''
  self.HeaderLen = 0
  self._NoHeaderYet = 1
  self._FirstPacket = 1
  self.Busy = 1
  self._PageContentLen = 0

!------------------------------------------------------------------------------

! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software  - www.capesoft.com


      Member()
         omit('***$***',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
         ***$***
      include('Equates.CLW'), once
      include('Keycodes.CLW'), once
      include('Errors.CLW'), once
      map
      end ! map
      include('NetEmail.inc'), once
      include ('nethttp.inc'), once
      include('NetMap.inc'), once            ! NetMap Module Map

! csTODO: Replace this with API file reading and writing.
NETEmailReadFileNameA         string(256),thread
NETEmailReadFileA             file,driver('dos'),thread,name(NETEmailReadFileNameA)
r                              record
d                                string(NET:Base64MaxDecodedLength)
                               end
                             end

NETEmailWriteFileNameA        string(256),thread
NETEmailWriteFileA            file,driver('dos'),thread,name(NETEmailWriteFileNameA),create
r                              record
d                                string(NET:MailFileDataSize)
                               end
                             end


!-----------------------------------------------------------
!                       NetEmail class
!      Base class for NetEmailReceive (POP) and NetIMAP
!     (IMAP) email clients. Also currently includes MIME
!       processing, (may be in a base class in future)
!-----------------------------------------------------------

! Returns a string indicating the type of object
NetEmail.IsType Procedure()
  code
    case self._objectType
    of NET:MAIL_CLIENT
        return 'Mail Client'
    of NET:POP_CLIENT
        return 'POP Client'
    of NET:SMTP_CLIENT
        return 'SMTP Client'
    of NET:NNTP_CLIENT
        return 'NNTP Client'
    of NET:IMAP_CLIENT
        return 'IMAP Client'
    else
        return 'Unknown Type'
    end


!-----------------------------------------------------------
! Returns the current data in in self.packet, or an empty string
! if there is no data
NetEmail.GetData Procedure()
pn          equate('NetEmail.GetData')
  code
    if self.packet.binDataLen > 0
        return self.packet.binData[1 : self.packet.binDataLen]
    else
        return ''
    end


!-----------------------------------------------------------
NetEmail.DataLen Procedure()
  code
    return self.packet.binDataLen



!-----------------------------------------------------------
! Send all data in the passed string. If the length is greater
! than the maximum size of a single packet then it is split
! and sent a packet at a time.
NetEmail.SendAllData Procedure(*string data, long dataLen=0)
s           long
  code
    if not dataLen
        dataLen = Len(data)
    end

    if dataLen <= NET:MaxBinData                            ! Single packet sent
        self.packet.binDataLen = dataLen
        self.packet.binData = data
        self.Send()
    else                                                    ! Send multiple packets
        loop s = 1 to dataLen by NET:MaxBinData
            if s + NET:MaxBinData > dataLen
                self.packet.binDataLen = dataLen - s + 1
                self.packet.binData = data[s : dataLen]
            else
                self.packet.binDataLen = NET:MaxBinData
                self.packet.binData = data[s : s + NET:MaxBinData-1]
            end
            self.Send()
        end
    end


!-----------------------------------------------------------
! Stores the passed data in the self.packet and sets the packet
! length to the passed dataLen, or the clipped length of the passed
! data if dataLen is zero (the default). Returns the length
! of the data stored.
! IMPORTANT:
!   * Handles a single packets only. Excess data is truncated.
NetEmail.SetPacketData Procedure(string data, long dataLen = 0)
pn          equate('NetEmail.SetPacketData')
maxLen      long
  code
    maxLen = Len(self.packet)
    if dataLen > 0
       self.packet.binDataLen = dataLen
    else
        self.packet.binDataLen = Len(Clip(data))
    end
    if self.packet.binDataLen > maxLen
        self.ErrorTrap(pn, 'The data passed was too large for a single packet and was truncated! ')
        self.packet.binDataLen = maxLen
    end

    self.packet.binData = data[1 : self.packet.binDataLen]
    return self.packet.binDataLen

!-----------------------------------------------------------
! The connection details (self.server, self.port etc.) must be set before
! the open method is call.
NetEmail.Open Procedure (string Server,uShort Port=0)
  code
    self.Log ('NetEmail.Open' & ' (' & self._serverDesc & ')', 'Attempting to open connection to remote ' & self._serverDesc & ' Server')
    if self.openFlag = 0                                    ! Check it is not already open
        parent.Open (server, port)                          ! Opens the actual port
    end


!-----------------------------------------------------------
NetEmail.Close Procedure()
  CODE
  self.Log ('NetEmail.Close' & ' (' & self._serverDesc & ')', 'Attempting to close the connection to the remote ' & self._serverDesc & ' Server')

  parent.close()
  self._State = NET:ER_NOTCONNECTED


!-----------------------------------------------------------
NetEmail._CloseIdleConnection Procedure()
  code
    self.Log ('NetEmail._CloseIdleConnection' & ' (' & self._serverDesc & ')', 'The connection was idle and will be aborted.')
    self.Error = ERROR:IdleTimeOut
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Idle Timeout - Warning connection has been idle for more than ' & |
                  self._Connection.InActiveTimeout/100 & ' seconds. Connection will be closed. State = ' &  self._state, 'NetEmail._CloseIdleConnection')
    self.abort()


!-----------------------------------------------------------
NetEmail._TryOpen Procedure ()
  code
    self.Log ('NetEmail._TryOpen' & ' (' & self._serverDesc & ')', 'Will try to establish a connection to ' & clip (self.Server) & ':' & self.Port)

    Free (self.AttachmentListQ)
    Free (self.EmbedListQ)

    if self.openFlag = 0 and self.asyncOpenBusy = 0
        do OpenConnection
    else
        if (self._state <> NET:ER_NOTCONNECTED) and (self._state <> NET:ER_WAITING)
            self.abort()                                    ! Abort connection and start again
            do OpenConnection
        else
            self.Log ('NetEmail._TryOpen' & ' (' & self._serverDesc & ')', 'The connection was already open, trying to re-use connection with a NOOP command')
            self.packet.binData = 'NOOP<13,10>'
            self.packet.bindatalen = Len(Clip(self.packet.bindata))
            self.Send()
        end
    end

! Open and then wait for Asynchronous connection to open and
! the Server to send a welcome message
OpenConnection routine
  self._state = NET:ER_CONNECTING
  self.open (self.Server,self.Port)


!-----------------------------------------------------------
! reads an eml file from the disk, rather than receiving it through TCP. After that
! parses it into the component parts as a normal receive object.
NetEmail.ProcessEML Procedure (String pFileName)
eml  StringTheory
  CODE
  eml.LoadFile(pFileName)
  self._ProcessResizeWholeMessage(eml.Length())
  self.wholeMessage = eml.GetValue()
  self.wholeMessageLen = eml.Length()
  if self._ProcessSplitWholeMessage().

!-----------------------------------------------------------
NetEmail._ProcessSplitWholeMessage Procedure ()

result              long
startPos            long
endPos              long
bodyPos             long
pn                  equate('NetEmail._ProcessSplitWholeMessage')
  CODE
  if self.optionsDontProcessWholeMessage
    self.Log (pn & ' (' & self._serverDesc & ')','Ignoring function as .optionsDontProcessWholeMessage is set.')
    return (0)
  end
  self.log(pn,'start')
  Free(self.AttachmentListQ)
  Free(self.EmbedListQ)
  if self.wholeMessageLen = 0
    self.Log (pn & ' (' & self._serverDesc & ') Warning self.wholeMessageLen = 0. Nothing to do. Returning from function', NET:LogError)
    return 0                                              ! No data to work on!
  end
  bodyPos = InString ('<13,10><13,10>', self.wholeMessage [1 : self.wholeMessageLen],1,1)
  if bodyPos = 0
    bodyPos = self.wholeMessageLen-5 ! To be safe - although it may cause the last line in the header to loose 5 characters, but it won't GPF on the string copies
  end
  self._ProcessHeader(startpos,bodyPos)
  StartPos = endPos + 2
  if self.OptionsOnlyProcessHeader
    self.Log (pn & ' (' & self._serverDesc & ')',' Wont process email body as .OptionsOnlyProcessHeader is set.')
    return 0
  end
  result = self._ProcessBody()
  return result

! ---------------------------------------------------------------------------------------
NetEmail._ProcessBody    Procedure()
Decode1             long
Decode2             long
x                   long
len1                long
TempEndPos1         long
pos1                long
pos3                long
length              long

fStart              long
fEnd                long
fLen                long
bodypos             long
result              long
pn                  equate('NetEmail._ProcessBody')
  code
  self.log(pn,'self.MimeVersion=' & clip(self.MimeVersion) & ' self.ContentType=' & clip(self.ContentType) & ' self.MimeBoundary=' & clip(self.MimeBoundary))
  if self.MimeVersion or self.ContentType
    if self.MimeBoundary
      result = self._ProcessParts(self.wholemessage,self.MimeBoundary,bodypos)
    else
      result = self._ProcessPart(self.wholemessage)
    end
  else
    result = self._ProcessBodyNoContentType(bodypos)
  end
  return result
!-----------------------------------------------------------
NetEmail._ProcessParts  Procedure(String pParts,String pBoundary,Long pStart)
pn        equate('NetEmail._ProcessParts')
!startpos  long
!endpos    long
bl        long
x         long
y         long
result    long
  code
  self.log(pn,'start')
  bl = len(clip(pBoundary)) + 2
  x = pstart - 1
  if x < 1 then x = 1.
  loop
    x = instring(clip(pBoundary), pParts,1,x+1)
    y = instring(clip(pBoundary), pParts,1,x+1)
    if x and y and y > x
      result = self._ProcessPart(pParts[x + bl : y-4])
    else
      break
    end
  end
  return result
!-----------------------------------------------------------
NetEmail._ProcessPart  Procedure(String pPart)
! each part has a header section, and a body section. Each part can contain multiple more parts.
bodypos                     long
contenttransfer             string (NET:StdEmailContentTransferEncodingSize)
contentid                   string(Net:StdEmailMessageIDSize)
contenttype                 string (NET:StdEmailContentTypeSize)
contentname                 string(File:MaxFileName)
contentcharset              string (NET:StdEmailCharSetSize)
contentboundary             string(NET:StdEmailMimeBoundarySize)
contentdisposition          string (NET:StdEmailContentTypeSize)
contentfilename             string(File:MaxFileName)
result                      long
pn                          equate('NetEmail._ProcessPart')
  code
  self.log(pn,'start pPart=' & sub(pPart,1,200))
  if pPart = '' then return 0.
  bodypos = instring ('<13,10,13,10>',pPart,1,1) + 4
  if bodypos = 4 then bodypos = 1.
  if bodypos < 1 or bodypos > len(clip(pPart)) then bodypos = len(clip(pPart)).
  contenttransfer = self._extractHeader('content-transfer-encoding',pPart[1 : bodypos])
  contentid = self._extractHeader('content-id',pPart[1 : bodypos])
  contenttype = self._extractHeader('content-type',pPart[1 : bodypos])
  self._ProcessHeaderParts(contenttype,contentboundary,contentname,contentcharset)
  contentdisposition = self._extractHeader('content-disposition',pPart[1 : bodypos],false)
  self._ProcessHeaderParts(contentdisposition,,,,contentfilename)
  if sub(contenttype,1,10) = 'multipart/'
    result = self._ProcessParts(pPart,contentboundary,bodypos)
  else
    !self.log(pn,'contentdisposition=' & clip(contentdisposition) & ' contenttype=' & clip(contenttype))
    case contentdisposition
    of 'inline' orof ''
      case contenttype
      of 'text/html'
        result = self._ProcessStoreHTML(sub(pPart,bodypos,len(clip(pPart))),contenttransfer,contentcharset)
      of 'text/plain' orof ''
        result = self._ProcessStoreText(sub(pPart,bodypos,len(clip(pPart))),contenttransfer,contentcharset)
      of 'message/rfc822'
        result = self._ProcessStoreAttachment(sub(pPart,bodypos,len(clip(pPart))),contenttransfer,contentfilename,contentid,contentname,contenttype,contentdisposition)
      else ! attached to news was application/ZIP
        result = self._ProcessStoreAttachment(sub(pPart,bodypos,len(clip(pPart))),contenttransfer,contentfilename,contentid,contentname,contenttype,contentdisposition)
      end
    of 'attachment'
      result = self._ProcessStoreAttachment(sub(pPart,bodypos,len(clip(pPart))),contenttransfer,contentfilename,contentid,contentname,contenttype,contentdisposition)
    end
  end
  return result

!-----------------------------------------------------------
NetEmail._ProcessBodyNoContentType  Procedure(Long BodyPos)
pn                  equate('NetEmail._ProcessBodyNoContentType')
TempEndPos1         long
EndBeforeUUEncode   long
  code
  self.Log (pn & ' (' & self._serverDesc & ')','No Mime or ContentType - (may have UUDecode)')
  !EndBeforeUUEncode = self._ProcessUUDecode ((bodyPos + 4), self.wholeMessageLen)
  EndBeforeUUEncode = self._ProcessUUDecode (sub(self.wholeMessage,bodypos+4,self.wholeMessageLen))
  if EndBeforeUUEncode = 0
    TempEndPos1 = self.wholeMessageLen
  else
    TempEndPos1 = EndBeforeUUEncode
  end
  self.MessageTextLen = TempEndPos1 - (bodyPos + 4) + 1
  if self.MessageTextLen < 0     ! Make sure we don't have a negative length - this can happen when we are looking at headers as we asume their will be <13,10,13,10> at the end, but there isn't
    self.MessageTextLen = 0
  else
    if self._ProcessSetSizes (self.MessageTextLen, -1, -1) ! Don't change WholeMessage or HTMLMessage
      return 1
    end
    if self.MessageTextLen > 0
      self.MessageText = self.wholeMessage [(bodyPos + 4) : TempEndPos1]
    else
      self.MessageText = ''
    end
  end
  return 0


!-----------------------------------------------------------
NetEmail._ExtractHeader Procedure (String pWhat,String pHeader, Long pClean=true)
loc:what   string(255)
answer     string(4096)
x          long
y          long
pn                  equate('NetEmail._ExtractHeader')
  code
  loc:what = '<13,10>' & clip(upper(pWhat)) & ':'
  x = instring (clip(loc:what),upper(pHeader),1,1)
  if x = 0
    loc:what = clip(upper(pWhat)) & ':'
    if upper(sub(pHeader,1,len(clip(loc:what)))) = loc:what
      x = 1
    end
  end
  ! x contains the start of the header
  if x
    y = x + 1
    loop
      y = instring('<13,10>',pHeader,1,y)
      if y and y+2 <= len(pHeader)
        case pHeader[y+2]
        of '<9>' orof ' '
          y = y + 1
          cycle
        end
      elsif y = 0
        y = len(clip(pHeader)) + 1
      end
      break
    end
    ! y is the end of the header, the 13 part of the end 13,10
    if y > x
      x += len(clip(loc:what))
      if pClean
        answer = self._processCleanString(left(pHeader[x : y-1]),true)
      else
        answer = left(pHeader[x : y-1])
      end
    end
  end
  return clip(answer)

!-----------------------------------------------------------
NetEmail._ExtractHeaderPart Procedure (String pWhat,String pStr)
answer   string(4096)
x1        long
x2        long
x3        long
pn       equate('NetEmail._ExtractHeaderPart')
  code
  if pWhat = ''
    ! get first value
    x1 = instring(';',pStr,1,1)
    x2 = instring('<13>',pStr,1,1)
    if x1 = 0  or (x1 > x2 and x2 <> 0) then x1 = x2.
    if x1 = 0
      answer = pStr
    else
      answer = sub(pStr,1,x1-1)
    end
  Else
    x3 = instring(clip(upper(pWhat))&'=',upper(pStr),1,1)
    if x3
      x3 += len(clip(pWhat))+1
      x1 = instring(';',pStr,1,x3)
      x2 = instring('<13>',pStr,1,x3)
      if x1 = 0 or (x1 > x2 and x2 <> 0) then x1 = x2.
      if x1 = 0 then x1 = len(clip(pStr))+1.
      answer = sub(pStr,x3,x1-x3)
    end
  end
  if left(answer,1) = '"'
    answer = sub(answer,2,len(clip(answer))-1)
  end
  if right(answer,1) = '"'
    answer = sub(answer,1,len(clip(answer))-1)
  end
  return answer

!-----------------------------------------------------------
NetEmail._ProcessHeaderParts   Procedure(*string pOriginal,<*String pBoundary>, <*String pName>, <*String pCharset>, <*String pFileName>)
pn                  equate('NetEmail._ProcessContentType')
  code
  if not omitted(3) then pBoundary = ''.
  if not omitted(4) then pName  = ''.
  if not omitted(5) then pCharset = ''.
  if not omitted(6) then pFileName = ''.
  if pOriginal = '' then return.
  if not omitted(3) then pBoundary = self._extractHeaderPart('boundary',pOriginal).
  if not omitted(4) then pName = self._extractHeaderPart('name',pOriginal) .
  if not omitted(5) then pCharset = self._extractHeaderPart('charset',pOriginal) .
  if not omitted(6) then pFileName = self._extractHeaderPart('filename',pOriginal) .
  pOriginal = self._processCleanString(self._extractHeaderPart('',pOriginal),true)
  return

!-----------------------------------------------------------
NetEmail._ProcessHeader Procedure (Long Startpos, Long BodyPos)
pn                  equate('NetEmail._ProcessHeader')
  code
  self.log(pn,'start')
  if bodypos = 0 then bodypos = len(clip(self.WholeMessage)).
  if bodypos > len(clip(self.WholeMessage)) then bodypos = len(clip(self.WholeMessage)).
  self.ToList = self._extractHeader('to',self.wholemessage[1 : bodypos])
  self.CCList = self._extractHeader('cc',self.wholemessage[1 : bodypos])
  self.From =   self._extractHeader('from',self.wholemessage[1 : bodypos])
  self.Subject = self._extractHeader('subject',self.wholemessage[1 : bodypos])
  self.ReplyTo = self._extractHeader('reply-to',self.wholemessage[1 : bodypos])
  self.Organization = self._extractHeader('organization',self.wholemessage[1 : bodypos])
  self.DeliveryReceiptTo = self._extractHeader('delivery-receipt-to',self.wholemessage[1 : bodypos])
  self.DispositionNotificationTo = self._extractHeader('dispositionnotificationto',self.wholemessage[1 : bodypos])
  if self.DispositionNotificationTo = '' then self.DispositionNotificationTo = self._extractHeader('disposition-notification-to',self.wholemessage[1 : bodypos]).
  self.SentDate = self._extractHeader('date',self.wholemessage[1 : bodypos])
  self.MessageID = self._extractHeader('message-id',self.wholemessage[1 : bodypos])
  self.References = self._extractHeader('references',self.wholemessage[1 : bodypos])
  self.MimeVersion = self._extractHeader('mime-version',self.wholemessage[1 : bodypos])
  self.ContentTransferEncoding = self._extractHeader('content-transfer-encoding',self.wholemessage[1 : bodypos])
  self._MDContentTransfer = self.ContentTransferEncoding
  self.UserFilterContent = self._extractHeader(self.OptionsUserFilter,self.wholemessage[1 : bodypos])
  self.ContentDisposition = self._extractHeader('Content-Disposition',self.wholemessage[1 : bodypos],false) ! don't clean it.
  self._ProcessHeaderParts (self.ContentDisposition, , self._MDFileName, , self._MDFileName)
  self.ContentType = self._extractHeader('Content-Type',self.wholemessage[1 : bodypos],false) ! don't clean it.
  self.MimeMixedBoundary = ''
  self.MimeAltBoundary = ''
  self.MimeRelBoundary = ''
  self._ProcessHeaderParts(self.ContentType,self.MimeBoundary,self._MDName)
  case lower (clip (self.ContentType))
  of 'multipart/mixed' orof 'multipart/report'
     self.MimeMixedBoundary = self.MimeBoundary
  of 'multipart/alternative'
     self.MimeAltBoundary = self.MimeBoundary
  of 'multipart/related'
     self.MimeRelBoundary = self.MimeBoundary
  end

!-----------------------------------------------------------
NetEmail._SplitParam Procedure (*string p_String, long p_Number, byte p_Separator=32)
  code
    return (self._SplitParam2 (p_String, p_Number, p_Separator, 0))

!-----------------------------------------------------------
! Thanks to Carl Barnes for optimizing this method
NetEmail._SplitParam2 Procedure (*string p_String,long p_Number,byte p_Separator,byte p_IgnoreRepeatedSeparators)
strlen         long                  ! p_string Length
x              long
BegPos         long                  ! BegPos = 1  required to work right
endPos         long                  ! endPos = 0  required to work right, actually is Ending Separator, return endPos-1
ParamIndex     long
  CODE
    BegPos = 1                         ! Required
    endPos = 0                         ! Required
    strlen = len(p_String)             ! could clip() w/o any trouble, but clip takes time too
    loop x = 1 to (strlen + 1)         ! Go 1 past end of string and assume end is final separator
      if x <= strlen
        if val(p_String[x]) <> p_Separator
          cycle                        ! Not a separator, and < end of p_String cycle back for more
        end
      end
      if (x > BegPos) or (p_IgnoreRepeatedSeparators = 0) or (x = 1)   ! BegPos = x when there are duplicate separators, p_IgnoreRepatedSeparators = 0 when every separator counts, also trap if the first character in the string is the separator as BegPos = x, but a first character separator counts as a separator
        ParamIndex += 1                ! Found a separator, so got next Parm
        if ParamIndex = p_Number       ! Is this the number of the Parm I wanted
          endPos = x                   ! Save end position of the last separator
          break                        ! and Stop looking as we are at the number we want
        end
      end
      BegPos = x + 1                   ! New data begins after previous p_Separator
    end
    if endPos > BegPos                 ! Endpos only set if parm found, If sequential separators then End-1<Beg and so slice is not valid, so fall thru to return empty
      return p_String[BegPos : (endPos - 1)]
    end
    return ''                          ! Instead of ELSE per Gordon insure the last thing is always a return


!-----------------------------------------------------------
NetEmail._ProcessResizeWholeMessage Procedure (long p_newRequiredSize)
currentSize2     long
NewWhole2        byte
StringRef        &String
  CODE
  currentSize2 = (size (self.wholeMessage))
  NewWhole2 = false
  if p_newRequiredSize <= currentSize2
    ! Okay there is enough memory allocated.
  else
    NewWhole2 = True
  end

  if NewWhole2
    StringRef &= self.wholeMessage
    self.wholeMessage &= new (String (p_newRequiredSize))
    if size (self.wholeMessage) < p_newRequiredSize
      self.error = ERROR:CouldNotAllocMemory
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Unable to allocate memory required for sending email.', 'NetEmailReceive._ProcessResizeWholeMessage')
      self.close()
      return (1)
    end
    if not StringRef &= NULL
      self.wholeMessage = clip (StringRef)
      dispose (StringRef)
    else
      self.wholeMessage = ''
    end
  end
  return (0)

!-----------------------------------------------------------
! Set any parameter = -1 to ignore changing size
NetEmail._ProcessSetSizes Procedure (long p_TextLen, long p_HTMLLen, long p_WholeLen)
local               group, pre (loc)
NewText               byte
NewHTML               byte
NewWhole              byte
currentSize           long
                    end
  CODE
  self.Error = 0
  self.WinSockError = 0
  self.SSLError = 0
  loc:NewText = false
  loc:NewHtml = false
  loc:NewWhole = false

  if p_TextLen >= 0
    loc:currentSize = (size (self.MessageText))
    if p_TextLen <= loc:currentSize
      ! Okay there is enough memory allocated.
      ! Just check there is not more than 64K extra memory allocated.
      if (loc:currentSize - p_TextLen) > NET:EmailNewDifference
        loc:NewText = True
      end
    else
      loc:NewText = True
    end

    if loc:NewText
      ! Alloc new Text
      if loc:currentSize > 0
        dispose (self.MessageText)
      end
      self.MessageText &= new (String (p_TextLen))
      if size (self.MessageText) < p_TextLen
        self.error = ERROR:CouldNotAllocMemory
        self.ErrorTrap ('Unable to allocate memory required for receiving email.', 'NetEmail._ProcessSetSizes')
        self.close()
        return (1)
      end
    end
  end

  if p_HTMLLen >= 0
    loc:currentSize = (size (self.MessageHtml))
    if p_HTMLLen <= loc:currentSize
      ! Okay there is enough memory allocated.
      ! Just check there is not more than 64K extra memory allocated.
      if (loc:currentSize - p_HTMLLen) > NET:EmailNewDifference
        loc:NewHtml = True
      end
    else
      loc:NewHtml = True
    end

    if loc:NewHtml
      ! Alloc new HTML
      if loc:currentSize > 0
        dispose (self.MessageHtml)
      end
      self.MessageHtml &= new (String (p_HTMLLen))
      if size (self.MessageHtml) < p_HTMLLen
        self.error = ERROR:CouldNotAllocMemory
        self.ErrorTrap ('Unable to allocate memory required for receiving email.', 'NetEmail.SetRequiredMessageSize')
        self.close()
        return(1)
      end
    end
  end

  if p_WholeLen > 0
    loc:currentSize = (size (self.wholeMessage))
    if p_WholeLen <= loc:currentSize                        ! Sufficient space
      if (loc:currentSize - p_WholeLen) > NET:EmailNewDifference    ! Max 64K extra memory (otherwise free and allocate smaller block)
        loc:NewWhole = True
      end
    else
      loc:NewWhole = True
    end

    if loc:NewWhole                                         ! Alloc new Whole Message
      if loc:currentSize > 0
        dispose (self.wholeMessage)
      end
      self.wholeMessage &= new (String (p_WholeLen))
      if size (self.wholeMessage) < p_WholeLen
        self.error = ERROR:CouldNotAllocMemory
        self.ErrorTrap ('Unable to allocate memory required for email.', 'NetEmail.SetRequiredMessageSize')
        self.close()
        return(1)
      end
    end
  end

  return (0)


!-----------------------------------------------------------
NetEmail.ErrorTrap Procedure (string errorStr,string functionName)
ourErrorStr        string (512)
  CODE
  case self.error
  of 10061
    ourErrorStr = 'Could not open a connection to the remote ' & self._serverDesc & ' Server'
  else
    ourErrorStr = clip (errorStr)
  end
  parent.errortrap (ourErrorStr, functionName)



!-----------------------------------------------------------
NetEmail._ProcessClearData Procedure()
  CODE
  self.MsgNumber = 0
  self.Bytes = 0
  self.UID = 0
  self.ToList = ''
  self.CcList = ''
  self.From = ''
  self.Subject = ''
  self.ReplyTo = ''
  self.Organization = ''
  self.SentDate = ''
  self.MessageID = ''
  self.References = ''
  self.UserFilterContent = ''
  self.AttachmentList = ''
  self.EmbedList = ''
  self.MessageText = ''
  self.MessageHtml = ''
  self.wholeMessage = ''
  self.MessageTextLen = 0
  self.MessageHtmlLen = 0
  self.wholeMessageLen = 0

  self.MimeVersion = ''
  self.MimeAltBoundary = ''
  self.MimeMixedBoundary = ''
  self.MimeRelBoundary = ''
  self.ContentType = ''
  self.TextCharSet = ''
  self.HTMLCharSet = ''
  self.ContentTransferEncoding = ''
  self.DeliveryStatusOriginalRecipient = ''
  self.DeliveryStatusFinalRecipient = ''
  self.DeliveryStatusAction = ''
  self.DeliveryStatusStatus  = ''
  self.DeliveryStatusRemoteMTA  = ''
  self.DeliveryStatusDiagnosticCode = ''
  self.DeliveryStatusLastAttemptDate = ''
  self.DeliveryReceiptTo = ''
  self.DispositionNotificationTo = ''
  self.ContentDisposition = ''

  self._MDContentType = ''
  self._MDCharSet = ''
  self._MDContentTransfer = ''
  self._MDContentBoundary = ''
  self._MDContentDisposition = ''
  self._MDContentID = ''
  self._MDFileName = ''
  self._MDName = ''

!-----------------------------------------------------------
NetEmail._ProcessStoreText Procedure (string pStr,string pContentTransfer, string pCharset)
len1                long
result              long
pn                  equate('NetEmail._ProcessStoreText')
  CODE
  if self.MessageTextLen > 0
    !return 0 ! up to 7.12 only the first text part was stored in MessageText.
    !         ! however from 7.13 it seems better to store the last text part in MessageText.
  end
  self.TextCharSet = pCharset
  len1 = len(clip(pStr))
  self.MessageTextLen = len1
  result = self._ProcessSetSizes(self.MessageTextLen,-1,-1)
  case Lower(pContentTransfer)
  of 'quoted-printable'
    self.MessageText = NetQuotedPrintableDecode(pStr, len1)
  of 'base64'
    NetBase64DecodeEx(pStr,self.MessageText,len1)
  else
    self.MessageText = pStr
  end
  return 0
!-----------------------------------------------------------
NetEmail._ProcessStoreHTML Procedure (string pStr,string pContentTransfer, string pCharset)
len1                long
result              long
  CODE
  if self.MessageHTMLLen > 0                              ! Ignore HTML - we've already got HTML code in for this message
    return 0
  end
  self.HTMLCharSet = pCharset
  len1 = len(clip(pStr))
  self.MessageHTMLLen = len1
  result = self._ProcessSetSizes(-1,self.MessageHTMLLen,-1)
  case Lower(pContentTransfer)
  of 'quoted-printable'
    self.MessageHtml = NetQuotedPrintableDecode(pStr, len1)
  of 'base64'
    NetBase64DecodeEx(pStr,self.MessageHtml,len1)
  else
    self.MessageHtml = pStr
  end
  return 0

!-----------------------------------------------------------
NetEmail._AddToEmbedListQueue Procedure (*String pFileName,String pContentType,String pContentTransfer)
TempFileName          string (255)
x                     long
  code
  TempFileName = pFileName
  loop x = 1 to records (self.EmbedListQ)
    get (self.EmbedListQ, x)
    if errorcode() then break.
    if self.EmbedListQ.Name = TempFileName
      TempFileName = 'Copy (' & records(self.EmbedListQ) & ') ' & pFileName
    end
  end
  pFileName = TempFileName
  if self.EmbedList = ''
    self.EmbedList = pFileName
  else
    self.EmbedList = clip (self.EmbedList) & ', ' & pFileName
  end
  Clear (self.EmbedListQ)
  self.EmbedListQ.Name = pFileName
  self.EmbedListQ.ContentType = pContentType
  self.EmbedListQ.ContentEncoding = pContentTransfer
  self.EmbedListQ.ContentID  = pContentType
  Add (self.EmbedListQ)
!-----------------------------------------------------------
NetEmail._AddToAttachmentListQueue Procedure (*String pFileName)
TempFileName          string (255)
x                     long
  code
  TempFileName = pFileName
  loop x = 1 to records (self.AttachmentListQ)
    get (self.AttachmentListQ, x)
    if errorcode() <> 0 then break.
    if self.AttachmentListQ.Name = TempFileName
      TempFileName = 'Copy (' & records(self.AttachmentListQ) & ') ' & pFileName
    end
  end
  pFileName = TempFileName
  if self.AttachmentList = ''
    self.AttachmentList = pFileName
  else
    self.AttachmentList = clip (self.AttachmentList) & ', ' & pFileName
  end
  clear (self.AttachmentListQ)
  self.AttachmentListQ.Name = pFileName
  add (self.AttachmentListQ)

!-----------------------------------------------------------
NetEmail._ProcessStoreAttachment Procedure (string pStr,String pContentTransfer,String pFileName,String pContentId,String pName,String pContentType, String pContentDisposition)
len1                  long
ThisFileName          string (255)
Static:NextFileNumber long,static
result                long
pn                  equate('NetEmail._ProcessStoreAttachment')
  CODE
  self.log(pn,'start pContentTransfer=' & clip(pContentTransfer) & ' pFileName=' & clip(pFileName) & ' pContentTransfer=' & clip(pContentTransfer) & ' pContentId=' & clip(pContentId))
  if lower (clip(pContentTransfer)) = 'x-uuencode'
    result = self._ProcessUUDecode (pStr)
    return result
  end

  if pFileName <> ''
    ThisFileName = pFileName
  elsif pContentId <> ''
    ThisFileName = pContentId
  else
    ThisFileName = pName
  end
  if ThisFileName = ''
    Static:NextFileNumber += 1
    ThisFileName = 'EmailFile' & format (Static:NextFileNumber, @N09)
    if lower(pContentType) = 'message/rfc822'
      ThisFileName = clip (ThisFileName) & '.eml'
    end
  end
  ThisFileName = self._CleanFileName (ThisFileName)

  if pContentId <> '' and pContentDisposition <> 'attachment'
    self._AddToEmbedListQueue(ThisFileName,pContentType,pContentTransfer)
    if self.OptionsDontSaveEmbeds
      self.log(pn,'Not Saving: OptionsDontSaveEmbeds=' & self.OptionsDontSaveEmbeds)
      return 0
    end
  else
    self._AddToAttachmentListQueue(ThisFileName)
    if self.OptionsDontSaveAttachments
      self.log(pn,'Not Saving: OptionsDontSaveAttachments=' & self.OptionsDontSaveAttachments)
      return 0
    end
  end
  len1 = len(clip(pStr))
  case lower (pContentTransfer)
  of 'base64'
    NetBase64DecodeEx(pStr,pStr,len1)
    result = self._SaveFile (pStr, clip (self.AttachmentPath) & ThisFileName,len1)
    self.log(pn,'After call to _SaveFile ' & clip(clip (self.AttachmentPath) & ThisFileName) & ' len=' & len1 & ' result=' & result)
  of 'quoted-printable'
    len1 -= 1
    pStr = NetQuotedPrintableDecode(pStr,len1)
    result = self._SaveFile (pStr, clip (self.AttachmentPath) & ThisFileName,len1)
  else
    result = self._SaveFile (pStr, clip (self.AttachmentPath) & ThisFileName,len1)
  end
  return 0

!-----------------------------------------------------------
NetEmail._SaveFile Procedure (*string writeString, string fileName, long dataLen=0)
hFile                   long, auto
lpFileName              cstring(255), auto
bytesWritten            long
writeSize               long, auto
FNULL                   &long
result                  long
pn                  equate('NetEmail._SaveFile')
  code
  if fileName = ''
    return 0
  end
  lpFileName = Clip(fileName)
  hFile = OS_CreateFile(lpFileName, NET:OS:GENERIC_WRITE, NET:OS:FILE_SHARE_READ, 0, NET:OS:CREATE_ALWAYS, 0, 0)
  if hFile = NET:OS:INVALID_HANDLE_VALUE
    return 0
  end
  if dataLen <= 0
    writeSize = Size(writeString)
  else
    writeSize = dataLen
  end
  if not OS_WriteFile(hFile, writeString, writeSize, bytesWritten, 0) ! (handle, data, size, *bytesWritten, overlapped)
    result = OS_CloseHandle(hFile)
    return 0
  end
  result = OS_CloseHandle(hFile)
  self.log(pn,' Saved File ' & clip(lpFileName) & ' length=' & bytesWritten)
  return 1
!-----------------------------------------------------------
!-----------------------------------------------------------
!-----------------------------------------------------------
NetEmail._ProcessRaiseError Procedure (string p_ErrorStr)
  CODE
  self.ServerErrorDesc = clip (p_ErrorStr)
  self.ErrorTrap ('The ' & self._serverDesc & ' Server returned error = ' & clip (p_ErrorStr), 'NetEmail._ProcessRaiseError')
  self.ServerErrorDesc = ''
  self.Close()
  return 1


!-----------------------------------------------------------
NetEmail._ProcessSendIt Procedure
  CODE
    self.Log ('NetEmail._ProcessSendIt' & ' (' & self._serverDesc & ')', 'Sending [' & Clip(self.packet.binData) & ']')
    self.packet.bindatalen = Len(Clip(self.packet.bindata))
    self.Send()


!-----------------------------------------------------------
! ----------------------------------------------------------
! String1 = 'Description1: Cool<13,10>Description2: Hot<13,10>Description3: Awesome<13,10>'
! Message (GetLineEntry ('description2:', String1, 1, len (clip(String1))))
!
! Displays 'Hot'
! ----------------------------------------------------------
NetEmail._GetLineEntry Procedure (string p_SearchString,*string p_MainString,long p_StartPos,long p_EndPos,long p_Offset,long p_CaseInSensitive)
local                 group, pre(loc)
pos1                    long
pos3                    long
tempString              string (1024)
                      end
  CODE
  if p_startPos > p_endPos
    return ('')
  end

  if p_CaseInSensitive
    loc:pos1 = InString (lower (p_searchString),lower (p_MainString [(p_startPos) : p_endPos]),1,1)
  else
    loc:pos1 = InString (p_searchString, p_MainString [(p_startPos) : p_endPos], 1, 1)
  end
  if loc:pos1 <> 0
    loc:pos1 += p_startPos - 1
    loc:pos3 = InString ('<13,10>',p_MainString [loc:pos1 : p_endPos],1,1)
    if loc:pos3 = 0
      loc:pos3 = p_endPos
    else
      loc:pos3 += loc:pos1 - 1
    end
    return (p_MainString [(loc:pos1 + len (clip (p_searchString)) + p_Offset) : (loc:pos3 - 1)])
  end
  return ('')



!-----------------------------------------------------------
! if p_mode = 1 : tabs converted to spaces, all 13,10's removed
! if p_mode <> 1 then passes through NetCharSet function.
NetEmail._ProcessCleanString Procedure (string p_string,long p_mode)
pos1            long
!lastPos1        long
answer          string (NET:StdEmailListSize)
len             long
!spos            long
!epos            long
!charSet         string(32)      ! utf-8, iso-8859-1 etc.
!encoding        string(1)
  CODE
  answer = p_string
  len = len (clip(answer))
  if len <= 0
    return ('')
  end
  if p_mode = 1
    pos1 = instring ('<9>', answer[1 : Len],1,1)
    loop while pos1 > 0
      answer [Pos1] = ' ' ! Convert TAB to SPACE
      Pos1 = instring ('<9>', answer[1 : Len], 1, pos1)
    end
    pos1 = instring('<13,10>',answer,1,1)
    loop while pos1 > 0
      if pos1 = 1
        answer = answer[3 : len]
      elsif pos1 = len-1
        answer = answer[1 : len-1]
      else
        answer = answer[1 : pos1-1] & answer[ pos1+2 : len]
      end
      len -= 2
      pos1 = instring('<13,10>',answer, 1, 1)
    end
  end
  return (NetCharSet(answer))

!-----------------------------------------------------------
NetEmail._ProcessUUDecode Procedure (String pStr)
local         group, pre (loc)
StartPos        long
endPos          long
TextEnd         long
Pos1            long
Pos2            long
Pos3            long
Pos4            long
Pos5            long
FileOpen        byte
ThisFileName    string (255)
length          long
DecodedString   string (255)
              end
!p_StartPos  long
!p_EndPos    long
  CODE
  loc:StartPos = 1
  loc:endPos = len(clip(pStr))
  if pStr = '' then return 0.


  loop  ! through each attachment
    loc:FileOpen = false
    loc:ThisFileName = ''

    loc:Pos1 = instring ('<13,10>begin ', pStr [loc:StartPos : loc:endPos],1,1)
    if loc:Pos1 > 0
      loc:Pos1 += loc:StartPos - 1
      loc:Pos5 = instring ('<13,10>', pStr [(loc:Pos1 + 1) : loc:endPos],1,1)
      if loc:Pos5 = 0
        break
      end
      loc:ThisFileName = sub (pStr, (loc:Pos1 + 12), (loc:Pos5 - 12))

      if clip (self.AttachmentList) = ''
        self.AttachmentList = clip (self.AttachmentList) & clip (loc:ThisFileName)
      else
        self.AttachmentList = clip (self.AttachmentList) & ', ' & clip (loc:ThisFileName)
      end

      loc:Pos2 =  instring ('<13,10>end', pStr [loc:Pos1 : loc:endPos],1,1)
      if loc:Pos2 > 0
        loc:Pos2 += (loc:Pos1 - 1) + 2
        if loc:TextEnd = 0
          loc:TextEnd = loc:Pos1 - 1
        end
        if self.OptionsDontSaveAttachments
          loc:StartPos = loc:Pos2 + 1
          cycle
        end
        if (loc:Pos1 + 1) < loc:Pos2
          loc:Pos3 = instring ('<13,10>', pStr [loc:Pos1+1 : loc:Pos2],1,1)
          if loc:Pos3 <> 0
            loc:Pos3 += ((loc:Pos1 + 1) - 1) + 2
            loop
              if (loc:Pos3) >= loc:Pos2
                break
              end
              loc:Pos4 = instring ('<13,10>', pStr [loc:Pos3 : loc:Pos2],1,1)
              if (loc:Pos4 = 0)
                break
              end
              loc:Pos4 += loc:Pos3 - 1

              if loc:FileOpen = false
                Do CreateFile
              end
              Do WriteLineToFile
              loc:Pos3 = loc:Pos4 + 2
            end
          end
        end
      else
        break ! no end found for attachment  ! New 24/6/2003
      end
      if loc:FileOpen
        Do SaveFile
      end
    else
      break  ! no more attachments
    end
    loc:StartPos = loc:Pos2 + 1
  end ! loop through each attachment

  return (loc:TextEnd)


CreateFile Routine
  loc:FileOpen = true
  NETEmailWriteFileNameA = clip (self.AttachmentPath) & clip (loc:ThisFileName) ! Need to loop
  create(NETEmailWriteFileA)
  if errorcode()
    self.error = ERROR:FileAccessError
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error creating attached files from email (' & clip (NETEmailWriteFileNameA) & ') for sending via email. Error = ' & errorcode() & ' ' & clip (error()) , 'NetEmail._ProcessUUDecode')
    return (0)
  end

  open(NETEmailWriteFileA,22h)  ! Read/Write, Deny Write
  if errorcode()
    self.error = ERROR:FileAccessError
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error creating (and opening) attached files from email (' & clip (NETEmailWriteFileNameA) & ') for sending via email. Error = ' & errorcode() & ' ' & clip (error()) , 'NetEmail._ProcessUUDecode')
    self.close()
    return (0)
  end


WriteLineToFile Routine
  loc:length = 0
  loc:DecodedString = NetUUDecode(pStr [loc:Pos3 : loc:Pos4],loc:length)
  if loc:length > 0
    NETEmailWriteFileA.r.d = loc:DecodedString[1 : loc:length]
    add(NETEmailWriteFileA, loc:length)
    if errorcode()
      self.error = ERROR:FileAccessError
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Error writing attached files from email (' & clip (NETEmailWriteFileNameA) & ') for sending via email. Error = ' & errorcode() & ' ' & clip (error()) , 'NetEmail._ProcessUUDecode')
      self.close()
      return (0)
    end
  end


SaveFile Routine
  close (NETEmailWriteFileA)


!-----------------------------------------------------------
NetEmail._CleanFileName Procedure (string p_InString)
local      group, pre (loc)
x            long
l            long
ans          string (1024)
pos1         long
           end
  CODE
  loc:ans = NetCharSet(p_InString)

  ! Get rid of everything before (and including) the last \
  loc:pos1 = instring ('\',loc:ans,1,1)
  loop while loc:pos1 <> 0
    loc:ans = sub (loc:ans, loc:pos1 + 1, len (loc:ans) - loc:pos1)
    loc:pos1 = instring ('\',loc:ans,1,1)
  end

  ! Get rid of everything before (and including) the last /
  loc:pos1 = instring ('/',loc:ans,1,1)
  loop while loc:pos1 <> 0
    loc:ans = sub (loc:ans, loc:pos1 + 1, len (loc:ans) - loc:pos1)
    loc:pos1 = instring ('/',loc:ans,1,1)
  end


  loc:l = len (clip (loc:ans))
  loop loc:x = 1 to loc:l

    ! Is the file name legal?
    ! Filenames must be less than 255 characters
    ! and must NOT include any of the following characters:
    ! forward slash (/),
    ! backslash (\),
    ! greater-than sign (>),
    ! less-than sign (<),
    ! asterisk (*),
    ! question mark (?),
    ! quotation mark ("),
    ! pipe symbol (|),
    ! colon (:), or
    ! semicolon (;).


    case loc:ans[loc:x]
    of '/'
    orof '\'
    orof '>'
    orof '<<'
    orof '*'
    orof '?'
    orof '"'
    orof '|'
    orof ':'
    orof ';'
      ! Remove character
      loc:ans = sub (loc:ans, 1, (loc:x - 1)) & sub (loc:ans, (loc:x + 1), (loc:l - loc:x))
      loc:l -= 1
      loc:x -= 1
    end

  end

  if loc:l = 0
    loc:ans = 'file'
  end
  return (clip(loc:ans))


!-----------------------------------------------------------
NetEmail._ProcessDeliveryStatus Procedure (long p_endMDPos,long p_NextBoundaryPos)
local                 group, pre(loc)
StartPos                long
endPos                  long
Length                  long
                      end

  CODE
  loc:StartPos = (p_endMDPos + 4)
  loc:endPos = ((p_NextBoundaryPos) - 5)
  loc:Length = (loc:endPos - loc:StartPos + 1)

  if loc:Length <= 0
    return (0)
    ! Nothing to process
  end

  self.DeliveryStatusOriginalRecipient = self._GetLineEntry ('Original-recipient: rfc822;', self.wholeMessage, loc:StartPos, loc:endPos, 0, 1)
  self.DeliveryStatusFinalRecipient = self._GetLineEntry ('Final-recipient: rfc822;', self.wholeMessage, loc:StartPos, loc:endPos, 0, 1)
  self.DeliveryStatusAction = self._GetLineEntry ('Action:', self.wholeMessage, loc:StartPos, loc:endPos, 1, 1)
  self.DeliveryStatusStatus  = self._GetLineEntry ('Status:', self.wholeMessage, loc:StartPos, loc:endPos, 1, 1)
  self.DeliveryStatusRemoteMTA  = self._GetLineEntry ('Remote-MTA:', self.wholeMessage, loc:StartPos, loc:endPos, 1, 1)
  self.DeliveryStatusDiagnosticCode = self._GetLineEntry ('Diagnostic-Code:', self.wholeMessage, loc:StartPos, loc:endPos, 1, 1)
  self.DeliveryStatusLastAttemptDate = self._GetLineEntry ('Last-Attempt-Date:', self.wholeMessage, loc:StartPos, loc:endPos, 1, 1)

  return (0)


!-----------------------------------------------------------
NetEmail.Init       PROCEDURE  (uLong Mode=NET:SimpleClient)
  code
    self.Log ('NetEmail.Init', 'Init Called')

    self._AttachmentListQ &= new (Net:EmailAttachmentQType)
    self.AttachmentListQ &= self._AttachmentListQ

    self._EmbedListQ &= new (Net:EmailAttachmentQType)
    self.EmbedListQ &= self._EmbedListQ

    self._EmailsListQ &= new (Net:EmailsListQType)
    self.EmailsListQ &= self._EmailsListQ

    self._EmailsDoneBeforeQ &= new (Net:EmailsDoneBeforeQType)
    self.EmailsDoneBeforeQ &= self._EmailsDoneBeforeQ

    ! --- Set up for using Asynchronous Open
    self.AsyncOpenUse = 1
    self.AsyncOpenTimeOut = NET:ASYNC_OPEN_TIMEOUT
    self.InActiveTimeout = NET:INACTIVE_TIMEOUT

    parent.Init(mode)


!-----------------------------------------------------------
NetEmail.Free Procedure ()
  CODE
  compile ('EndCompile', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetEmail.Free' & ' (' & self._serverDesc & ')', 'Freeing extra memory used by the object.')
    end
  EndCompile

  if ~self.MessageText &= NULL
    dispose (self.MessageText)
  end
  if ~self.MessageHtml &= NULL
    dispose (self.MessageHtml)
  end
  if ~self.wholeMessage &= NULL
    dispose (self.wholeMessage)
  end

  compile ('EndCompile', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetEmail.Free' & ' (' & self._serverDesc & ')', 'End of free()')
    end
  EndCompile


!-----------------------------------------------------------
NetEmail.Kill Procedure ()
  CODE
  compile ('EndCompile', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetEmail.Kill' & ' (' & self._serverDesc & ')', 'Kill called')
    end
  EndCompile

  parent.kill()

  if not self._AttachmentListQ &= NULL
    free (self._AttachmentListQ)
    dispose (self._AttachmentListQ)                  ! Free the allocated Memory of AttachmentListQ
  end

  if not self._EmbedListQ &= NULL
    free (self._EmbedListQ)
    dispose (self._EmbedListQ)                       ! Free the allocated Memory of EmbedListQ
  end

  if not self._EmailsListQ &= NULL
    free (self._EmailsListQ)
    dispose (self._EmailsListQ)
  end

  if not self._EmailsDoneBeforeQ &= NULL
    free (self._EmailsDoneBeforeQ)
    dispose (self._EmailsDoneBeforeQ)
  end

  self.free()

  compile ('EndCompile', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetEmail.Kill' & ' (' & self._serverDesc & ')', 'End of Kill()')
    end
  EndCompile



! Buffer a partial reply
NetEmail._BufferReply Procedure()
  code
    if self._ServerReplyBufferLen = 0
        self._ServerReplyBuffer = self.packet.binData [1 : self.packet.binDataLen]
        self._ServerReplyBufferLen = self.packet.binDataLen
    else
        self._ServerReplyBuffer = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen] & self.packet.binData [1 : self.packet.binDataLen]
        self._ServerReplyBufferLen += self.packet.binDataLen
        if self._ServerReplyBufferLen > Size (self._ServerReplyBuffer)
            self._ServerReplyBufferLen = Size (self._ServerReplyBuffer)
        end
    end


! Right join the buffered data and new data
NetEmail._GetBufferedData Procedure()
  code
    if self.packet.binDataLen = 0
      self.packet.binData = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen]
      self.packet.binDataLen = self._ServerReplyBufferLen
    else
      self.packet.binData = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen] & self.packet.binData[1 : self.packet.binDataLen]
      self.packet.binDataLen += self._ServerReplyBufferLen
      if self.packet.binDataLen > size (self.packet.binData)
        self.packet.binDataLen = size (self.packet.binData)
      end
    end
    self.Log ('NetEmail._GetBufferedData' & ' (' & self._serverDesc & ')', |
              'Okay we put the buffered lines together and got [' & self.packet.binData [ 1 : self.packet.BinDataLen] & ']')
    self._ServerReplyBufferLen = 0


!-----------------------------------------------------------
! Pure Virtual Methods - to be implemented by child classes
NetEmail.Ask Procedure (long p_command)
  code


NetEmail.Decide                 Procedure (long mode)!, virtual
  code


NetEmail.Done                   Procedure (long command, long finished)
  code



NetEmail.ConnectionClosed       Procedure ()
  code



NetEmail.Process                Procedure ()
  code




!-----------------------------------------------------------
!               NetEmailRecieve class
!              Implements a POP client
!-----------------------------------------------------------

NetEmailReceive.Ask  Procedure (long p_command)
x                long
  code
  ! Command is set to one of NET:EmailCount, NET:EmailDownload
  if self.OptionsUseDoneBeforeQ                             ! New November 2005
    self.Log ('NetEmailReceive.Ask' & ' (' & self._serverDesc & ')', 'Ask Called with OptionUseDoneBeforeQ')
    !self.OptionsUsePreviousEmailsListQ = false
    !self.OptionsJustGetEmailsListQ = true
    self._UseList = 1
    p_command = NET:EmailDownload
  end

  self.Log ('NetEmailReceive.Ask' & ' (' & self._serverDesc & ')', 'Ask called with command = ' & p_command)

  self.LoadEmailsDoneBeforeQ()
  self._command = p_Command
  self._StoppedAtMsgNumber = 0

  self.TotalBytesCount = 0
  self.TotalBytesToGo = 0
  self.Progress1000 = 0
  self.TotalBytesReceived = 0

  self._PositionInNextBatch = 0
  self._BatchResume = 0
  self._BatchListHasChanged = 0

  self._ListCacheLen = 0

  if self.OptionsUsePreviousEmailsListQ = false
    free (self.EmailsListQ)
  else
    loop x = 1 to records (self.EmailsListQ)
      get (self.EmailsListQ, x)
      if errorcode() = 0
        if self.EmailsListQ._ToDo = 0
          if self.EmailsListQ.ToDelete or self.EmailsListQ.ToDownload
            self.EmailsListQ._ToDo = 1
            put (self.EmailsListQ)
          end
        end
      end
    end
  end

  if self.OptionsNewBatchEveryXMessages > 0 or self.OptionsNewBatchEveryXBytes > 0
    self._UseUIDL = 1 ! Turn this on if Batches are on
  end

  if self.progresscontrol                                   ! Update the Progress Control Control if there is one
    self.progresscontrol{PROP:RangeLow} = 0                 ! zero to 1000
    self.progresscontrol{PROP:RangeHigh} = 1000             ! zero to 1000
    self.progresscontrol{prop:hide} = 0
    self.progresscontrol{prop:progress} = 0
    display(self.progressControl)
  end

  self._TryOpen()                                           ! Will open or re-use a connection


!-----------------------------------------------------------
NetEmailReceive.Process Procedure
splitStr              string (1024)
tempMessageLen        long
newRequiredSize       long
FullLineReceived      byte
InActiveSeconds       long
ID                    long
x                     long
y                     long
  code
  case (self.packet.packetType)
  of NET:SimpleIdleConnection
    self._CloseIdleConnection()
  of NET:SimpleAsyncOpenSuccessful                          ! Wait for the welcome message from the server.
    self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'Connection to ' & self._Connection.Server |
             & ':' & self._Connection.Port & ' has been opened. Will wait for the Welcome message from the Server.')
  of NET:SimplePartialDataPacket
    self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'Received ' & self.packet.binDataLen & ' bytes from the remote ' & self._serverDesc & ' Server')

    if (self._state <> NET:ER_DOWNLOAD) and (self._State <> NET:ER_LISTALL) and (self._State <> NET:ER_UIDLALL)
      fullLineReceived = false
      if self.packet.binDataLen >= 2
        if self.packet.binData[(self.packet.binDataLen-1) : self.packet.binDataLen] = '<13,10>'
          fullLineReceived = true
        end
      end

      if fullLineReceived = false
        self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', |
                  'Warning. We Received Data from the remote ' & self._serverDesc & ' Server - but did not receive a whole line. Will store the line in the buffer')
        self._BufferReply()
        return                                            ! Waiting for additional data
      end

      if self._ServerReplyBufferLen > 0
        self._GetBufferedData()
      end
    end

    if self.Error <> 0
      self.ErrorTrap ('Error reading data from the remote ' & self._serverDesc & ' Server. State = ' & self._state, 'NetEmailReceive.Process')
      self.close()
      return
    end

    Case self._state
    of NET:ER_NOTCONNECTED
      self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'Received data while in NOT CONNECTED state. Will ignore the data.')
      self.close()
      return
    of NET:ER_WAITING
      self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'Waiting State')
      if self._ProcessSplitReturned(splitStr) <> NET:RET_OKAY  ! Spilt the returned string into a return code and return text
        Do RaiseError
      else                                                  ! Start doing next state
        self._ProcessDecideWhereToGo
      end
    of NET:ER_CONNECTING
      self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'Connecting State. Will send USER ' & clip (self.User))
      if self._ProcessSplitReturned(splitStr) <> NET:RET_OKAY
        do RaiseError
      else                                                  ! Start doing next state
        self._state = NET:ER_USER
        self.packet.bindata = 'USER ' & clip (self.User) & '<13,10>'
        self._ProcessSendIt
      end
    of NET:ER_USER
      self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'User State. Will send PASS **' & '**')
      if self._ProcessSplitReturned(splitStr) <> NET:RET_OKAY
        do RaiseError
      else                                                  ! Start doing next state
        self._state = NET:ER_PASS
        self.packet.bindata = 'PASS ' & clip (self.Password) & '<13,10>'
        self._ProcessSendIt
      end
    of NET:ER_PASS
      self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'Pass State. Will decide what to do and action it.')
      if self._ProcessSplitReturned(splitStr) <> NET:RET_OKAY
        do RaiseError
      else                                                  ! Start doing next state
        self._LoginSuccessful() ! Notification method called, when login works
        self._ProcessDecideWhereToGo
      end
    of NET:ER_STAT
        self._ProcessStat()
    of NET:ER_LISTALL
        self._ProcessList()
    of NET:ER_UIDLALL
      self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'UIDL State.')
      loop                                                  ! once only
        if self._FirstFlagList = true and self._ProcessSplitReturned(splitStr) <> NET:RET_OKAY
          Do RaiseError
          break
        end
        self._FirstFlagList = false
        if self._ProcessUIDLAll() = 0
          ! List data takes more than one packet - wait for next packet.
        else
          self.DoneUIDLAll()                                ! Virtual notification method  ! Changed 11 Nov 2005 was self.DoneListAll()
          if self.OptionsJustGetEmailsListQ                 ! Stop processing - only the EmailsList was required
            self._ProcessDoQuit()
          else
            self._FirstFlag = true
            self._state = NET:ER_DOWNLOAD
            if self._ProcessGetData()
              return
            end
          end
        end
        break
      end
    of NET:ER_UIDLONE
      ! This option will not work if you have selectively deleted some of the emails in the last batch
      ! you must either delete emails 1 to x in the last batch or none at all.
      ! e.g. If you deleted email 1 and email 3 then this method (self.OptionsUIDOneOnly) will not work
      self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'UIDL One State.')

      if self._ProcessSplitReturned(splitStr) <> NET:RET_OKAY
        do RaiseError
      else
        Clear (self.EmailsListQ)
        self.EmailsListQ.UID = self._SplitParam2 (splitStr, 2, 32, true) ! ! true = IgnoreRepeatSeparators as in ComCasts UID list "3   20040625130210r1600n4cqte000003" note duplicate spaces
        if Sub (self.EmailsListQ.UID, len(clip(self.EmailsListQ.UID))-1,2) = '<13,10>'
          self.EmailsListQ.UID = sub (self.EmailsListQ.UID, 1, len(clip(self.EmailsListQ.UID))-2)
        end
        get (self.EmailsListQ, self.EmailsListQ.UID)
        if errorcode() = 0
          ID = self._SplitParam (splitStr, 1) ! Should be 1
          if ID <> self.EmailsListQ._BatchID
            self.EmailsListQ._BatchID = ID
            put (self.EmailsListQ)
            y = ID + 1 ! Should be 2
            ID = self.EmailsListQ.ID
            loop x = 1 to records (self.EmailsListQ)
              get (self.EmailsListQ, x)
              if errorcode() <> 0
                break
              end
              if self.EmailsListQ._BatchID = 0
                cycle
              end
              if self.EmailsListQ.ID < ID ! Don't change the entry we just changed, or any before that
                self.EmailsListQ._BatchID = 0
                put (self.EmailsListQ)
                cycle
              elsif self.EmailsListQ.ID <> ID
                self.EmailsListQ._BatchID = y ! Set all BatchIDs to 0
                put (self.EmailsListQ)
                y += 1
              end
            end
          end
        else
          self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', |
                    'Warning could not find Email UID (One) [' & self.EmailsListQ.UID & '] in the EmailsListQ. This could be a new email.', NET:LogError)
        end

        self.DoneUIDLAll()                                  ! Virtual notification method  ! Changed 11 Nov 2005 was self.DoneListAll()
        if self.OptionsJustGetEmailsListQ                   ! Stop processing - only the EmailsList was required
          self._ProcessDoQuit()
        else
          self._FirstFlag = true
          self._state = NET:ER_DOWNLOAD
          if self._ProcessGetData()
            return
          end
        end
      end
    of NET:ER_DOWNLOAD
      if self._ProcessGetData()
        return
      end
    of NET:ER_QUIT
      self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'Quit State.')
      if self._ProcessSplitReturned(SplitStr) <> NET:RET_OKAY
        Do RaiseError
      else
        ! Start doing next state
        if self.OpenFlag = 1
          self._state = NET:ER_WAITING
        else
          self._state = NET:ER_NOTCONNECTED
        end
        self.Log ('NetEmailReceive.Process' & ' (' & self._serverDesc & ')', 'Calling Done(command, true)')
        if self.progresscontrol                          ! Update the Progress Control Control if there is one
          self.progresscontrol{prop:progress} = 1000
          display(self.progressControl)
        end
        self._CallDone(self._command, true)
        if self.progresscontrol                          ! Update the Progress Control Control if there is one
          self.progresscontrol{prop:hide} = 1
          self.progresscontrol{prop:progress} = 0
          display(self.progressControl)
        end
      end

    of NET:ER_QUITTHENRESUME orof NET:ER_RESUME     ! Nothing to do

    else
      self.ErrorTrap ('Error - no state found. State = ' &  self._state, 'NetEmailReceive.Process')
      self.close()
    end
    return
  end


RaiseError Routine
  if self._ProcessRaiseError (SplitStr)
    return
  end


!-----------------------------------------------------------
NetEmailReceive._ProcessList Procedure()
splitStr            string(1024)
  code
    self.Log ('NetEmailReceive._ProcessListAll' & ' (' & self._serverDesc & ')', 'List State.')

    loop                                                  ! Once only
        if self._FirstFlagList = true and self._ProcessSplitReturned(splitStr) <> NET:RET_OKAY
            self._ProcessRaiseError (SplitStr)
            return
        end
        self._FirstFlagList = false
        if self._ProcessListAll() = 0
            ! List data takes more than one packet - wait for next packet.
        else
            self.DoneListAll()                              ! Virtual notification method
            if self._UseUIDL
                self._GetUidlList()
            else
                self._FirstFlag = true
                self._state = NET:ER_DOWNLOAD
                if self._ProcessGetData()
                    return
                end
            end
        end
        break
    end


!-----------------------------------------------------------
NetEmailReceive._ProcessStat Procedure()
splitStr            string(1024)
  code
    self.Log ('NetEmailReceive._ProcessStat' & ' (' & self._serverDesc & ')', 'Stat State.')
    if self._ProcessSplitReturned(splitStr) <> NET:RET_OKAY
        self._ProcessRaiseError (SplitStr)
        return
    end

    if self._BatchResume = 0                                ! Only do this if this is not a second (or subsequent) batch
      self.TotalMessagesCount = 0
      self.TotalBytesCount = 0
      self.TotalMessagesCount = self._SplitParam (splitStr, 1)
      self.TotalBytesCount = self._SplitParam (splitStr, 2)
    end

    if self._command = NET:EmailCount
      self._ProcessDoQuit()
      return
    end

    if self._UseList                                        ! After STAT call LIST
        if self._SplitParam (splitStr, 1) = 0
            self._ProcessDoQuit()                           ! Finished now emails
        else
            if (self._BatchResume = 0) and (self.OptionsUsePreviousEmailsListQ = false)
                free (self.EmailsListQ)
                self._FirstFlagList = true
                self._state = NET:ER_LISTALL
                self.packet.bindata = 'LIST' & '<13,10>'
                self._ListCacheLen = 0
                self._ProcessSendIt
            else                                            ! This is the second (or subsequent) Batch
                if self.OptionsUsePreviousEmailsListQ
                    self._BatchListHasChanged = true        ! force this
                end
                if self._BatchListHasChanged
                    if self.OptionsUIDLOneOnly = 1 and self._BatchResume = 1  ! Get the UIDL One - New 17/1/2005
                        self._FirstFlagList = true
                        self._state = NET:ER_UIDLONE
                        self.packet.bindata = 'UIDL 1' & '<13,10>'
                        self._ProcessSendIt
                    else                                    ! Get the UIDL list again
                        self._GetUidlList()
                    end
                else                                        ! Otherwise, start getting the next batch
                  self._FirstFlag = true
                  self._state = NET:ER_DOWNLOAD
                  if self._ProcessGetData()
                        return
                  end
                end
            end
        end
    else                                                    ! Don't Call LIST, start doing next state
        if self._command = NET:EmailDownload
            self._FirstFlag = true
            self._state = NET:ER_DOWNLOAD
            if self._ProcessGetData()
                return
            end
        end
    end

NetEmailReceive._GetUidlList Procedure()
  code
    self._FirstFlagList = true
    self._state = NET:ER_UIDLALL
    self._FirstUIDLUpdate = true
    self._ListCacheLen = 0
    self.packet.bindata = 'UIDL' & '<13,10>'
    self._ListCacheLen = 0
    self._ProcessSendIt


!-----------------------------------------------------------
NetEmailReceive.Init Procedure (ulong mode=NET:SimpleClient)
  CODE
  self.Log ('NetEmailReceive.Init', 'Init Called')

  if self._serverDesc_Set = 0
    self._ServerDesc = 'POP (Email)'
    self._serverDesc_Set = 1
    self._ObjectType = NET:POP_CLIENT
    if self.Port = 0
      self.Port = NET:POP_PORT
    end
  end

  self._UseList = 1
  self._UseUIDL = 1

  parent.Init(NET:SimpleClient)                             ! This is a client


!-----------------------------------------------------------
NetEmailReceive.ConnectionClosed Procedure()
  CODE
  compile ('EndCompile', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetEmailReceive.ConnectionClosed' & ' (' & self._serverDesc & ')', |
         'The remote ' & self._serverDesc & ' Server closed the connection or a dial-up connection closed.')
    end
  EndCompile

  ! Called when the connection is closed
  case self._state
  ! ------------------
  of NET:ER_QUITTHENRESUME
  ! ------------------
    ! No worries - we are doing a quit for a resume of a new batch
    self._State = NET:ER_RESUME
    self._TryOpen() ! Try open a new connection again, to get the next batch


  ! ------------------
  of   NET:ER_NOTCONNECTED
  orof NET:ER_WAITING
  ! ------------------
    ! No worries
    self._state = NET:ER_WAITING

  ! ------------------
  of   NET:ER_QUIT
  ! ------------------
    ! No worries
    self._state = NET:ER_WAITING
    if self._CallDoneIfConnectionCloses
      self._CallDone (self._command, true)   ! The connect closed just after sending the QUIT command, this is fine, we just need to call Done
    end

  ! ------------------
  else
  ! ------------------
    self.ErrorTrap ('Connection to the remote ' & self._serverDesc & ' Server closed by Server or a dial-up connection closed.', |
      'EmailReceive.ConnectionClosed')
    self._state = NET:ER_NOTCONNECTED
  end



!-----------------------------------------------------------
NetEmailReceive.Kill Procedure ()
  CODE
  compile ('EndCompile', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetEmailReceive.Kill' & ' (' & self._serverDesc & ')', 'Kill called')
    end
  EndCompile

  parent.kill()



!-----------------------------------------------------------
NetEmailReceive.Done Procedure (long command, long finished)
  CODE
! --------------------------------------------------------------
! Virtual Method called, when processing of Ask() has been completed
!
! This method is called when an email has been downloaded (finished = false),
! and also when all the emails have been processed (finished = true)
!
! --------------------------------------------------------------
! command will be set to one of the following:
!   NET:EmailCount
!   NET:EmailDownload
!
! finished will be set to one of the following:
!   false - when an email has been downloaded
!   true - when all emails have been downloaded.
!   e.g. If there are 2 emails to download. finished will be false twice and true once.
!
! --------------------------------------------------------------
!
!  Suggested use:
!
!  if Finished
!    if command = NET:EmailCount
!      Message ('Total Number of Messages = ' & self.TotalMessagesCount & |
!               '|Total Bytes = ' & self.TotalBytesCount, '', ICON:ASTERISK)
!    else
!      Message ('Proceessing Completed', '', ICON:ASTERISK)
!    end
!  else
!    ! Display message
!    message (clip (self.wholeMessage))     ! Done() will only be called with finished = false,
!                                           ! after a download
!  end
!
! --------------------------------------------------------------
!-----------------------------------------------------------
NetEmailReceive.Decide Procedure (long mode)
  CODE
! Virtual Method
! --------------
! Mode is one of:
! NET:EmailBasics - self.MsgNumber and self.Bytes are presented
! NET:EmailDetails - Header info available for looking at

! Set self.DecideAction to one of the following
! NET:EmailMoreInfo  - Requests more info
! NET:EmailDownload  - Download message
! NET:EmailSkip      - Skip message
! NET:EmailStop      - Stop processing
!
! The default action is NET:EmailDownload



!-----------------------------------------------------------
NetEmailReceive.CalcProgress Procedure
local       group, pre (loc)
x             long
BytesToSend   long
tempPointer   long
temp100a      long
temp100b      long
            end

  CODE
  if self.TotalBytesCount > 0
    if (self.TotalBytesReceived > self.TotalBytesCount)
      self.TotalBytesCount = self.TotalBytesReceived
    end
    self.TotalBytesToGo = self.TotalBytesCount - self.TotalBytesReceived
    self.Progress1000 = (self.TotalBytesReceived / self.TotalBytesCount) * 1000
    if self.progresscontrol                          ! Update the Progress Control
      loc:temp100a = self.progresscontrol{prop:progress} / 10
      loc:temp100b = self.Progress1000 / 10
      if loc:temp100a <> loc:temp100b
        self.progresscontrol{prop:progress} = self.Progress1000
      end
    end
  else
    self.Progress1000 = 0 ! All done
    if self.progresscontrol                          ! Update the Progress Control
      if self.progresscontrol{prop:progress} <> self.Progress1000
        self.progresscontrol{prop:progress} = self.Progress1000
        display(self.progressControl)
      end
    end
  end


!-----------------------------------------------------------
NetEmailReceive.DecideToDelete Procedure()
  code


!-----------------------------------------------------------
NetEmailReceive.DoneListAll Procedure ()
  code


!-----------------------------------------------------------
NetEmailReceive.DoneUIDLAll Procedure ()
  code


!-----------------------------------------------------------




!-----------------------------------------------------------
NetEmailReceive._ProcessDecideWhereToGo Procedure
  CODE
  case self._command
  of NET:EmailCount
  orof NET:EmailDownload
    self._state = NET:ER_STAT
    self.packet.bindata = 'STAT<13,10>'
    self._ProcessSendIt
  else
    self.ErrorTrap ('Illegal State. State = ' & self._state, 'NetEmailReceive._ProcessDecideWhereToGo')
    self.Close()
  end


!-----------------------------------------------------------
NetEmailReceive._ProcessDoQuit Procedure
  CODE
  if self.OptionsKeepOpen and (self._command = NET:EmailCount or Not(self.OptionsDelete))
    self._state = NET:ER_WAITING  ! Wait for next command - it may timeout waiting
    compile ('EndCompile', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetEmailReceive._ProcessDoQuit' & ' (' & self._serverDesc & ')', 'Calling Done(command, true)')
      end
    EndCompile
    if self.progresscontrol                          ! Update the Progress Control Control if there is one
      self.progresscontrol{prop:progress} = 1000
      display(self.progressControl)
    end
    self._CallDone(self._command, true)
    if self.progresscontrol                          ! Update the Progress Control Control if there is one
      self.progresscontrol{prop:hide} = 1
      self.progresscontrol{prop:progress} = 0
      display(self.progressControl)
    end

  else                           ! Must issue QUIT if Delete option is on
    self._CallDoneIfConnectionCloses = true
    self._state = NET:ER_QUIT
    self.packet.bindata = 'QUIT<13,10>'
    self._ProcessSendIt()
  end

!-----------------------------------------------------------
NetEmailReceive._ProcessSplitReturned Procedure (*String p_Str)
local          group, pre (loc)
ret              long
               end
  CODE
  loc:ret = NET:RET_ERROR
  p_Str = ''
  if upper (self.packet.binData [1 : 3]) = '+OK'
    loc:ret = NET:RET_OKAY
  else
    loc:ret = NET:RET_ERROR
  end
  if loc:ret = NET:RET_OKAY
    if self.packet.binDataLen >= 5
      p_Str = self.packet.binData [5 : self.packet.binDataLen]
    end
  else
    if self.packet.binDataLen >= 6
      p_Str = self.packet.binData [6 : self.packet.binDataLen]
    end
  end
  return (loc:ret)


!-----------------------------------------------------------
NetEmailReceive._ProcessCopyBinData Procedure (string p_Correct)
local               group, pre (loc)
startpos              long         ! Start Position
tempMessageLen        long
tempEndFound          byte
s                     long
e                     long
                    end

  CODE
  ! Copies Data in Packet into Data in Message

  compile ('EndCompile', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetEmailReceive._ProcessCopyBinData' & ' (' & self._serverDesc & ')', 'Start of function')
    end
  EndCompile

  if self.packet.binDataLen >= 5
    self._Last5Bytes = self.packet.binData [(self.packet.binDataLen - 4) : self.packet.binDataLen]
    self._Last5BytesLen = 5
  else
    if self.packet.binDataLen >= 1
      loc:s = (self._Last5BytesLen-(5-self.packet.binDataLen)+1)
      loc:e = loc:s + (5-self.packet.binDataLen) - 1
      if loc:s < 1
        loc:s = 1
      end
      if loc:e > self._Last5BytesLen
        loc:e = self._Last5BytesLen
      end
      if (loc:s >= 1) and (loc:e >= loc:s)
        self._Last5Bytes = self._Last5Bytes [loc:s : loc:e] & self.packet.binData [1 : self.packet.binDataLen]
        self._Last5BytesLen = loc:e - loc:s + 1 + self.packet.binDataLen
      else
        self._Last5Bytes = self.packet.binData [1 : self.packet.binDataLen]
        self._Last5BytesLen = self.packet.binDataLen
      end
      if self._Last5BytesLen > 5
        self._Last5BytesLen = 5
      end
    end
  end

  ! Check if this is the end
  if (self.bytes = 0) and (self._ZeroByteFixDisable=0)
    compile ('EndCompile', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetEmailReceive._ProcessCopyBinData' & ' (' & self._serverDesc & ')', 'Warning. Email has 0 bytes. Applied the 0 byte fix to it.')
      end
    EndCompile
    loc:TempEndFound = true
    ! If the remote server has a 0 byte email, then it will not send '<CR>.<CR>',
    ! it just sends '+OK 0 octets<CR>'
    ! so we set the TempEndFound = true
  else
    if self._Last5BytesLen >= 5
      if self._Last5Bytes [1 : self._Last5BytesLen] = '<13,10>.<13,10>'
        loc:TempEndFound = true
      else
        loc:TempEndFound = false
      end
    else
      loc:TempEndFound = false
    end
  end

  ! Start by converting .. to .
  loc:startpos = 1
  loop
    loc:startpos = Instring ('<13,10>..', self.packet.binData [1 : self.packet.bindatalen], 1, loc:startpos)
    if loc:startpos = 0
      break ! out of loop
    end
    if (loc:startpos + 4) <= self.packet.binDatalen
      self.packet.binData = self.packet.binData [1 : (loc:startpos + 2)] & self.packet.binData [(loc:startpos + 4) : self.packet.binDatalen]
      self.packet.binDataLen += -1
    else
      loc:startpos += 1  ! Added to prevent infinit loop
    end
  end


  if self._FirstPacket1 = true
    self._FirstPacket1 = false
    ! First packet of data for WholeMessage
    ! Strip off first line (+OK ???????<13,10>)
    loc:startpos = 0
    if self.packet.BinDataLen < len (clip (p_Correct))
      self.ErrorTrap ('Too little data retured from ' & self._serverDesc & ' Server. Less than 3 bytes. State = ' & self._state, 'NetEmailReceive._ProcessCopyBinData')
      self.close()
      return(1)
    else
      if upper (self.packet.binData [1 : len (clip (p_Correct))]) <> p_Correct
        if self._DownloadStage = 4  ! Download
          self.ErrorTrap ('The ' & self._serverDesc & ' Server returned error = ' & clip (left (self.packet.binData,160)), 'NetEmailReceive._ProcessCopyBinData')
          self.close()
          return(1)
        elsif self._DownloadStage = 3 ! TOP
          self._endNotFound = false ! An error occurred (probably TOP is not supported) - Ignore error
          return (1)
        end
      else
        loc:startpos = InString ('<13,10>', self.packet.binData [1 : self.packet.binDataLen], 1, 1) ! Strip the rest of the line off
      end
    end
    if loc:startpos = 0
      self.ErrorTrap ('Too little data retured from ' & self._serverDesc & ' Server. No CR found.', 'NetEmailReceive._ProcessCopyBinData')
      self.close()
      return(1)
    else
      self.wholeMessageLen = self.Bytes + 5     ! Bytes required for entire message + 5 terminating bytes
      if self._ProcessSetSizes (self.MessageTextLen, self.MessageHTMLLen, self.wholeMessageLen)
        return (1)
      end
      self.wholeMessageLen = self.packet.binDataLen - (loc:startpos + 2) + 1  ! How much we have already
      if self.wholeMessageLen > size (self.wholeMessage)
        compile ('EndCompile', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetEmailReceive._ProcessCopyBinData' & ' (' & self._serverDesc & ')',|
                      'Attention. (1) Resizing self.wholeMessage because ' & self._serverDesc & ' Server sent more bytes than it initially indicated it would send!' & |
                      'Old size was = ' & size (self.wholeMessage) & ' new size = ' & self.wholeMessageLen)
          end
        EndCompile
        if self._ProcessResizeWholeMessage (self.wholeMessageLen)
          return (1)
        end
      end
      if (loc:startpos + 2 <= self.packet.binDataLen) and (loc:startpos + 2 > 0) ! Jan 2005 Index Range Fix
        self.wholeMessage = self.packet.bindata [loc:startpos + 2 : self.packet.binDataLen]
      else
        self.wholeMessage = ''
      end
    end
  else
    ! Subsequent packet for the message
    loc:tempMessageLen = self.wholeMessageLen + self.packet.binDataLen
    if loc:tempMessageLen > size (self.wholeMessage)
      if self.LoggingOn
        self.Log ('NetEmailReceive._ProcessCopyBinData' & ' (' & self._serverDesc & ')', |
                    'Attention. (2) Resizing self.wholeMessage because ' & self._serverDesc & ' Server sent more bytes than it initially indicated it would send!' & |
                    'Old size was = ' & size (self.wholeMessage) & ' new size = ' & loc:tempMessageLen)
      end
      if self._ProcessResizeWholeMessage (loc:tempMessageLen)
        if self.LoggingOn
          self.Log ('NetEmailReceive._ProcessCopyBinData' & ' (' & self._serverDesc & ')', |
                      'Error - Resizing WholeMessagefailed!')
        end
        return (1)
      end
    end
    self.wholeMessage [(self.wholeMessageLen + 1) : (self.wholeMessageLen + self.packet.binDataLen)] = self.packet.bindata [1 : self.packet.binDataLen]
    self.wholeMessageLen = loc:tempMessageLen
  end

  if loc:TempEndFound = true
    compile ('EndCompile', NETTALKLOG=1)
      if self.LoggingOn
        if self.wholeMessageLen > 0
          self.Log ('NetEmailReceive._ProcessCopyBinData' & ' (' & self._serverDesc & ')', 'Download of message (or Header) complete : {{' & clip (left (self.wholeMessage[1 : self.wholeMessageLen], 160)) & '}')
        else
          self.Log ('NetEmailReceive._ProcessCopyBinData' & ' (' & self._serverDesc & ')', 'Download of message (or Header) complete : [0 byte header]')
        end
      end
    EndCompile
    self._endNotFound = false
  else
    self._endNotFound = true
    ! Keep receiving packets
  end
  return (0)


!-----------------------------------------------------------
NetEmailReceive._ProcessGetData Procedure ()
local               group, pre (loc)
SplitStr              string (1024)
DecideParam           long
                    end
  CODE
  self._endNotFound = false
  if self._FirstFlag
    self._ProcessClearData()
    if self._UseList
      if self._NextMessageFromList() <> 0
        self._ProcessDoQuit()
        return (1)
      end
      compile ('EndCompile', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetEmailReceive._ProcessGetData' & ' (' & self._serverDesc & ')', |
                    'Starting at message number ' & self._CurrentMessageNum)
        end
      EndCompile
    else
      if self._PositionInNextBatch <> 0
        self._CurrentMessageNum = self._PositionInNextBatch  ! The Message ID we are processing
        compile ('EndCompile', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetEmailReceive._ProcessGetData' & ' (' & self._serverDesc & ')', |
                      'Resuming the download on the new connection. Starting at message number ' & |
                      self._CurrentMessageNum)
          end
        EndCompile
      else
        self._CurrentMessageNum = 1         ! The Message ID we are processing
      end
    end
    self._MessagesThisBatch = 0
    self._BytesThisBatch = 0
    self._PositionInNextBatch = self._CurrentMessageNum
    Do SetStartingDownloadStage
  else
    case self._DownloadStage
    of 1                                  ! List
      if self._ProcessSplitReturned (loc:SplitStr) <> NET:RET_OKAY ! Spilt the returned string into a return code and return text
        if self._ProcessRaiseError (loc:SplitStr)
          return (1)
        end
      end
      self.MsgNumber = self._SplitParam (loc:SplitStr, 1)
      self.Bytes = self._SplitParam (loc:SplitStr, 2)
    of 2                                  ! UIDL
      if self._ProcessSplitReturned (loc:SplitStr) <> NET:RET_OKAY
        if self._ProcessRaiseError (loc:SplitStr)
          return (1)
        end
      end
      self.UID = self._SplitParam2 (loc:SplitStr, 2, 32, true) ! true = IgnoreRepeatSeparators as in ComCasts UID list "3   20040625130210r1600n4cqte000003" note duplicate spaces
      if len (clip(self.UID)) > 2
        if self.UID [len (clip(self.UID)) - 1 : len (clip(self.UID))] = '<13,10>'
          self.UID = self.UID [1 : len (clip(self.UID)) - 2]
        end
      end
    of 3                                  ! TOP
      if self._ProcessCopyBinData('+OK')                 ! Don't check for OK, it's checked in Do CopyBinData
        return (1)
      end
    of 4                                  ! RETR
      if self._ProcessCopyBinData('+OK')                 ! Don't check for OK, it's checked in Do CopyBinData
        return (1)
      end
    of 5                                  ! DELE
      if self._ProcessSplitReturned (loc:SplitStr) <> NET:RET_OKAY
        if self._ProcessRaiseError (loc:SplitStr)
          return (1)
        end
      end
      ! All cool
    end ! Case
    ! Accept data
  end

  if not (self._FirstFlag)
    case self._DownloadStage
    !---
    of 1                                          ! LIST
    !---
      loc:DecideParam = NET:EmailBasics
      Do CallDecideMethod
    !---
    of 2                                          ! UIDL
    !---
      self._DownloadStage = 3                     ! Get More Info (Header Info - TOP)
    !---
    of 3
    !---                                          ! TOP
      compile ('EndCompile', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetEmailReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'TOP')
        end
      EndCompile
      if self._endNotFound = true
        ! Keep receiving packets.
      else
        self.DecideAction = NET:EmailDownload
        if self._ProcessSplitWholeMessage()
          self.ErrorTrap ('Error. Could not Split apart WholeMessage. Probably because Memory could not be allocated. Either free memory or use .optionsDontProcessWholeMessage=1', 'NetEmailReceive._ProcessGetData')
          self.close()
          return (1)
        end
        loc:DecideParam = NET:EmailDetails
        Do CallDecideMethod
      end
    !---
    of 4                                          ! Download
    !---
      compile ('EndCompile', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetEmailReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Download')
        end
      EndCompile
      if self._endNotFound = true
                                                  ! Keep receiving packets till we get the last packet for the message
      else  ! End Found
        self.DecideToDelete()                     ! New method added. This method is called when the email has been downloaded.
                                                  ! You can set self.OptionsDelete = true if you want to delete the email at this point.
                                                  ! 30/8/2001
        ! Changes 1 March 2004 ---------------------------
        if self._ProcessReceivedMessage(self.TotalMessagesCount)
          return (1)
        end
        if self.OptionsDelete = true
          ! Always delete if .OptionsDelete = on
          self._DownloadStage = 5                 ! Delete
        else
          if self._UseList and self._GetToDelete()
            ! Delete because self.EmailsListQ.ToDelete is set
            self._DownloadStage = 5               ! Delete
          else
            if self._UseList
              if self._NextMessageFromList() <> 0
                self._ProcessDoQuit()
                return (1)
              end
            else
              if self._CurrentMessageNum >= self.TotalMessagesCount
                self._ProcessDoQuit()                                   ! All done
                return (1) ! was exit 10/10/2000
              end
              self._CurrentMessageNum += 1
            end
            self._PositionInNextBatch += 1
            Do SetStartingDownloadStage

            if self._CheckIfWeNeedNewBatch()
              return(1) ! Start a new batch
            end
          end
        end
      end
    !---
    of 5                                          ! Delete
    !---
      compile ('EndCompile', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetEmailReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Delete')
        end
      EndCompile
      if self._UseList
        self._MarkMessageDoneInList()
        if self._NextMessageFromList() <> 0
          self._ProcessDoQuit()
          return (1)
        end
      else
        if self._CurrentMessageNum >= self.TotalMessagesCount
          self._ProcessDoQuit()                             ! All done
          return (1)
        end
        self._CurrentMessageNum += 1
      end
      self._PositionInNextBatch += 1
      Do SetStartingDownloadStage

      if self._CheckIfWeNeedNewBatch()
        return(1) ! Start a new batch
      end

    !---
    else
    !---
      self.ErrorTrap ('Illegal value for self.DownloadStage', 'NetEmailReceive._ProcessGetData')
      self.Close()
      return (1)
    end
  else
    self._FirstFlag = false
  end


  if self._UseList = false
    ! Double check that we haven't finished
    if self._CurrentMessageNum > self.TotalMessagesCount
      self._ProcessDoQuit()                                    ! All done
      return (1)
    end
  end

  if self._state = NET:ER_NOTCONNECTED                   ! We got disconnected
    compile ('EndCompile', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetEmailReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Got Disconnected')
      end
    EndCompile
    return (1)
  end

  case self._DownloadStage
  of 1
    self._FirstPacket1 = true
    self._Last5BytesLen = 0
    self.packet.bindata = 'LIST ' & self._CurrentMessageNum & '<13,10>'
    self._ProcessSendIt
  of 2
    self._FirstPacket1 = true
    self._Last5BytesLen = 0
    self.packet.bindata = 'UIDL ' & self._CurrentMessageNum & '<13,10>'
    self._ProcessSendIt
  of 3
    if self._endNotFound = false
      self._FirstPacket1 = true
    self._Last5BytesLen = 0
      self.packet.bindata = 'TOP ' & self._CurrentMessageNum & ' 0<13,10>'
      self._ProcessSendIt
    else
      ! NOT Sending TOP
      ! Keep reading data
    end
  of 4
    if self._endNotFound = false
      self._FirstPacket1 = true
      self._Last5BytesLen = 0
      self.packet.bindata = 'RETR ' & self._CurrentMessageNum & '<13,10>'
      self._ProcessSendIt
    else
      ! Waiting for next packet
    end
  of 5
    self._FirstPacket1 = true
    self._Last5BytesLen = 0
    self.packet.bindata = 'DELE ' & self._CurrentMessageNum & '<13,10>'
    self._BatchListHasChanged = 1
    self._PositionInNextBatch -= 1
    self._ProcessSendIt
  end

  return (0)

! -------------------------------------------------------------------------------------

SetStartingDownloadStage Routine
  if self._UseList = false
    self._DownloadStage = 1 ! List x
  else
    self._FirstPacket1 = true
    self._Last5BytesLen = 0
    self._endNotFound = 0
    loc:DecideParam = NET:EmailBasics
    Do CallDecideMethod
  end


! -------------------------------------------------------------------------------------

CallDecideMethod Routine
  if (self._GetToDownload() = false) and (self._GetToDelete() = true)
    self.DecideAction = NET:EmailDelete
  else
    self.DecideAction = NET:EmailDownload
  end
  compile ('EndCompile', NETTALKLOG=1)
    if self.LoggingOn
      if loc:DecideParam = NET:EmailBasics
        self.Log ('NetEmailReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Calling Decide(NET:EmailBasics)')
      elsif loc:DecideParam = NET:EmailDetails
        self.Log ('NetEmailReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Calling Decide(NET:EmailDetails)')
      else
        self.Log ('NetEmailReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Calling Decide(' & loc:DecideParam & ')')
      end
    end
  EndCompile
  self.Decide(loc:DecideParam)                 ! Find out what the guy wants to do
  case (self.DecideAction)
  of NET:EmailMoreInfo
    if self._UseList = false
      self._DownloadStage = 2                  ! Get More Info (UIDL)
    else
      self._DownloadStage = 3                  ! Get More Info TOP
    end
  of NET:EmailDownload
    self.wholeMessageLen = 0
    self.MessageText = 0
    self.MessageHtml = 0
    self._DownloadStage = 4
  of NET:EmailSkip
    self.TotalBytesCount -= self.Bytes
    self._ProcessClearData()
    if self._UseList
      self._MarkMessageDoneInList()
      if self._NextMessageFromList() <> 0
        self._ProcessDoQuit()
        return (1)
      end
    else
      self._CurrentMessageNum += 1             ! Go to next one
    end
    self._PositionInNextBatch += 1
    Do SetStartingDownloadStage
  of NET:EmailStop
    self._StoppedAtMsgNumber = self.MsgNumber
    !Stop ('_ProcessGetData Stopped _StoppedAtMsgNumber = ' & self._StoppedAtMsgNumber)
    self._ProcessClearData()
    self._ProcessDoQuit()                                ! Finish
    return (1)
  of NET:EmailDelete
    self._ProcessClearData()
    self.wholeMessageLen = 0
    self.MessageText = 0
    self.MessageHtml = 0
    self._DownloadStage = 5
    self.TotalBytesCount -= self.Bytes
  else
    self.ErrorTrap ('Illegal value for self.DecideAction', 'NetEmailReceive._ProcessGetData')
    self.Close()
    return (1)
  end

!-----------------------------------------------------------
NetEmailReceive._ProcessReceivedMessage Procedure (long p_MaxMessageNum)
  CODE
  if self._ProcessSplitWholeMessage()
    self.ErrorTrap ('Error. Could not Split apart WholeMessage. ' & |
                    'Probably because Memory could not be allocated. ' & |
                    'Either free memory or use .optionsDontProcessWholeMessage=1', |
                    'NetEmailReceive._ProcessReceivedMessage')
    self.close()
    return (1)
  end

  if self._UseList
    self._MarkMessageDoneInList()
  end

  if self.wholeMessageLen > 0
    self.TotalBytesReceived += self.wholeMessageLen
    self.CalcProgress
    self._MessagesThisBatch += 1
    self._BytesThisBatch += self.wholeMessageLen
    self._CallDone (self._command, false)                  ! Let the user know we've downloaded a file
  end

  self._ProcessClearData()

  return (0)




!-----------------------------------------------------------
NetEmailReceive._CheckIfWeNeedNewBatch Procedure ()
! returns 1 if we will be starting a new batch
  CODE
  if self.OptionsNewBatchEveryXMessages = 0 and self.OptionsNewBatchEveryXBytes = 0
    return (0) ! Don't split into batches
  end

  if self._UseList
    clear (self.EmailsListQ)
    self.EmailsListQ._ToDo = 1
    get (self.EmailsListQ, self.EmailsListQ._ToDo)
    if errorcode() <> 0
      return (0) ! No more undownloaded emails - so finish up normally
    end
  else
    if self._CurrentMessageNum > self.TotalMessagesCount
      return (0) ! This is the last message, so just finish up normally
    end
  end

  if self._MessagesThisBatch >= self.OptionsNewBatchEveryXMessages or |
     self._BytesThisBatch >= self.OptionsNewBatchEveryXBytes
    ! Okay split into new batch
    compile ('EndCompile', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetEmailReceive._CheckIfWeNeedNewBatch' & ' (' & self._serverDesc & ')', |
                  'Will restart the connection, as we have downloaded ' & self._MessagesThisBatch & |
                  ' messages (' & self._BytesThisBatch & ' bytes) this batch')
      end
    EndCompile
    self._BatchResume = 1
    self.packet.bindata = 'QUIT<13,10>'
    self._ProcessSendIt()
    self._state = NET:ER_QUITTHENRESUME
    return(1)
  end

  return(0)                                                 ! No batch needed


!-----------------------------------------------------------
NetEmailReceive._CallDone Procedure (long command, long finished)
x        long
  CODE
  self._CallDoneIfConnectionCloses = false

  if Finished
    !if self.OptionsUseDoneBeforeQ ! Always do
      if self.OptionsJustGetEmailsListQ = 0 ! only do this if you aren't just getting the list
        free (self.EmailsDoneBeforeQ)
        loop x = 1 to records (self.EmailsListQ)
          get (self.EmailsListQ, x)
          if errorcode() <> 0
            break
          end
          if (self._StoppedAtMsgNumber = 0) or (self.EmailsListQ.ID < self._StoppedAtMsgNumber)
            clear (self.EmailsDoneBeforeQ)
            self.EmailsDoneBeforeQ.Size = self.EmailsListQ.Size
            self.EmailsDoneBeforeQ.UID = self.EmailsListQ.UID
            self.EmailsDoneBeforeQ.DownloadDate = self.EmailsListQ.DownloadDate
            self.EmailsDoneBeforeQ.DownloadTime = self.EmailsListQ.DownloadTime
            if self.EmailsDoneBeforeQ.DownloadDate = 0
              self.EmailsDoneBeforeQ.DownloadDate = Today()
              self.EmailsDoneBeforeQ.DownloadTime = Clock()
            end
            add (self.EmailsDoneBeforeQ)
          !else
          !  Stop ('NetEmailReceive._CallDone Will not write in ID = ' & self.EmailsListQ.ID & ' (' & self._StoppedAtMsgNumber & ')')
          end
        end
        self.SaveEmailsDoneBeforeQ()
      end
      compile ('EndCompile', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetEmailReceive._CallDone' & ' (' & self._serverDesc & ')', |
                    'There are now ' & records(self.EmailsDoneBeforeQ) & ' in self.EmailsDoneBeforeQ')
        end
      EndCompile
    !end
  end

  self.Done(Command, Finished)

!-----------------------------------------------------------
NetEmailReceive._ProcessListAll Procedure ()
! return 1 when the whole data set has been received
local             group, pre(loc)
ret                 long
FinishDot           long
StartPos            long
CRPos               long
                  end
  CODE
  loc:ret = 0
  if self.packet.binDataLen <= 0
    return (loc:ret) ! Nothing to do
  end

  if self._ListCacheLen > 0
    self._ListCache [(self._ListCacheLen+1) : (self._ListCacheLen+self.packet.binDataLen)] = self.packet.binData[1 : self.packet.binDataLen]
    self._ListCacheLen += self.packet.binDataLen
  else
    self._ListCache [1 : self.packet.binDataLen] = self.packet.binData[1 : self.packet.binDataLen]
    self._ListCacheLen = self.packet.binDataLen
  end

  if self._ListCacheLen >= 3
    if self._ListCache[1 : 3] = '.<13,10>'
      loc:ret = 1
    else
      loc:FinishDot = instring ('<13,10>.<13,10>', self._ListCache[1 : self._ListCacheLen],1,1)
      if loc:FinishDot
        loc:ret = 1
      end
    end
  end

  loc:StartPos = 1

  loop
    if loc:StartPos > self._ListCacheLen
      self._ListCacheLen = 0
      break
    end
    loc:CRPos = instring ('<13,10>', self._ListCache[1 : self._ListCacheLen],1,loc:StartPos)
    if loc:CRPos = 0
      if loc:StartPos = 1
        return (loc:ret) ! Keep whole packet
      end
      if loc:StartPos <= self._ListCacheLen
        self._ListCache [1 : (self._ListCacheLen-loc:StartPos+1)] = self._ListCache [loc:StartPos : self._ListCacheLen]
        self._ListCacheLen = (self._ListCacheLen-loc:StartPos+1)
      end
      return (loc:ret)
    end
    if (loc:StartPos+2) <= self._ListCacheLen
      if self._ListCache[loc:StartPos : loc:StartPos+2] = '+OK'
        loc:StartPos = loc:CRPos+2
        cycle ! skip first line
      end
    end
    if self._ListCache[loc:StartPos : loc:CRPos+1] = '.<13,10>'
      loc:StartPos = loc:CRPos+2
      cycle ! skip last line
    end
    clear (self.EmailsListQ)
    self.EmailsListQ.ID = self._SplitParam (self._ListCache[loc:StartPos : loc:CRPos], 1)
    self.EmailsListQ._BatchID = self.EmailsListQ.ID
    self.EmailsListQ.Size = self._SplitParam (self._ListCache[loc:StartPos : loc:CRPos], 2)
    self.EmailsListQ._ToDo = 1      ! Default
    self.EmailsListQ.ToDownload = 1 ! Default
    self.EmailsListQ.ToDelete = 0   ! Default - if OptionsDelete is on, then Delete will still happen
    self.EmailsListQ.DownloadDate = Today()
    self.EmailsListQ.DownloadTime = Clock()
    if self.EmailsListQ.ID > 0
      add (self.EmailsListQ)
    else
      compile ('EndCompile', NETTALKLOG=1)
        self.Log ('NetEmailReceive._ProcessListAll' & ' (' & self._serverDesc & ')', |
                  'Warning received invalid data or ID numbers of 0 from the server in the LIST command. ' & clip (self.Server) & ':' & self.Port, NET:LogError)
      EndCompile
    end
    loc:StartPos = loc:CRPos+2
  end

  return (loc:ret)
!-----------------------------------------------------------
NetEmailReceive._NextMessageFromList Procedure ()
  CODE
  loop
    clear (self.EmailsListQ)
    self.EmailsListQ._ToDo = 1
    get (self.EmailsListQ, self.EmailsListQ._ToDo)
    if errorcode() <> 0
      return (1)
    end
    if self.EmailsListQ.ToDelete or self.EmailsListQ.ToDownload
      break ! found some work
    end
    self.EmailsListQ._ToDo = 0 ! nothing to do on this one
    put (self.EmailsListQ)
  end
  self._CurrentMessageNum = self.EmailsListQ._BatchID
  self.MsgNumber = self.EmailsListQ.ID
  self.Bytes = self.EmailsListQ.Size
  self.UID = self.EmailsListQ.UID
  return (0)

!-----------------------------------------------------------
NetEmailReceive._MarkMessageDoneInList Procedure ()
  CODE
  clear (self.EmailsListQ)
  self.EmailsListQ._BatchID = self._CurrentMessageNum
  get (self.EmailsListQ, self.EmailsListQ._BatchID)
  if errorcode() <> 0
    compile ('EndCompile', NETTALKLOG=1)
      self.Log ('NetEmailReceive._MarkMessageDoneInList' & ' (' & self._serverDesc & ')', |
                'Error. Could not mark email _BatchID ' & self.EmailsListQ._BatchID & |
                ' as Downloaded in self.EmailsListQ', NET:LogError)
    EndCompile
  else
    if self.EmailsListQ._ToDo <> 0
      self.EmailsListQ._ToDo = 0 ! Done
      put (self.EmailsListQ)
    end
  end

!-----------------------------------------------------------
! return 1 when the whole data set has been received
NetEmailReceive._ProcessUIDLAll Procedure ()
local             group, pre(loc)
ret                 long
FinishDot           long
StartPos            long
CRPos               long
ID                  long
x                   long
MyCount             long
Hours1              long
Hours2              long
                  end
  CODE
  loc:ret = 0
  if self.packet.binDataLen <= 0
    return (loc:ret) ! Nothing to do
  end

  if self._ListCacheLen > 0
    self._ListCache [(self._ListCacheLen+1) : (self._ListCacheLen+self.packet.binDataLen)] = self.packet.binData[1 : self.packet.binDataLen]
    self._ListCacheLen += self.packet.binDataLen
  else
    self._ListCache [1 : self.packet.binDataLen] = self.packet.binData[1 : self.packet.binDataLen]
    self._ListCacheLen = self.packet.binDataLen
  end

  if self._ListCacheLen >= 3
    if self._ListCache[1 : 3] = '.<13,10>'
      loc:ret = 1
    else
      loc:FinishDot = instring ('<13,10>.<13,10>', self._ListCache[1 : self._ListCacheLen],1,1)
      if loc:FinishDot
        loc:ret = 1
      end
    end
  end

  loc:StartPos = 1

  loop
    if loc:StartPos > self._ListCacheLen
      self._ListCacheLen = 0
      break
    end
    loc:CRPos = instring ('<13,10>', self._ListCache[1 : self._ListCacheLen],1,loc:StartPos)
    if loc:CRPos = 0
      if loc:StartPos = 1
        return (loc:ret) ! Keep whole packet
      end
      if loc:StartPos <= self._ListCacheLen
        self._ListCache [1 : (self._ListCacheLen-loc:StartPos+1)] = self._ListCache [loc:StartPos : self._ListCacheLen]
        self._ListCacheLen = (self._ListCacheLen-loc:StartPos+1)
      end
      return (loc:ret)
    end
    if (loc:StartPos+2) <= self._ListCacheLen
      if self._ListCache[loc:StartPos : loc:StartPos+2] = '+OK'
        loc:StartPos = loc:CRPos+2
        cycle ! skip first line
      end
    end
    if self._ListCache[loc:StartPos : loc:CRPos+1] = '.<13,10>'
      loc:StartPos = loc:CRPos+2
      cycle ! skip last line
    end
    if loc:StartPos <= (loc:CRPos-1)
      if (self._BatchResume) = 0 and (self.OptionsUsePreviousEmailsListQ = false)
        clear (self.EmailsListQ)
        self.EmailsListQ._BatchID = self._SplitParam (self._ListCache[loc:StartPos : (loc:CRPos-1)], 1)
        get (self.EmailsListQ, self.EmailsListQ._BatchID)
        if errorcode() = 0
          self.EmailsListQ.UID = self._SplitParam2 (self._ListCache[loc:StartPos : (loc:CRPos-1)], 2, 32, true) ! true = IgnoreRepeatSeparators as in ComCasts UID list "3   20040625130210r1600n4cqte000003" note duplicate spaces
          put (self.EmailsListQ)
        else
          compile ('EndCompile', NETTALKLOG=1)
            self.Log ('NetEmailReceive._ProcessUIDLAll' & ' (' & self._serverDesc & ')', |
                      'Warning could not find Email ID [' & self.EmailsListQ._BatchID & '] in the EmailsListQ', NET:LogError)
          EndCompile
        end
      else
        clear (self.EmailsListQ)
        self.EmailsListQ.UID = self._SplitParam2 (self._ListCache[loc:StartPos : (loc:CRPos-1)], 2, 32, true) ! ! true = IgnoreRepeatSeparators as in ComCasts UID list "3   20040625130210r1600n4cqte000003" note duplicate spaces
        get (self.EmailsListQ, self.EmailsListQ.UID)
        if errorcode() = 0
          loc:ID = self._SplitParam (self._ListCache[loc:StartPos : (loc:CRPos-1)], 1)
          if loc:ID <> self.EmailsListQ._BatchID
            self.EmailsListQ._BatchID = loc:ID
            put (self.EmailsListQ)
            if self._FirstUIDLUpdate
              self._FirstUIDLUpdate = false
              loc:ID = self.EmailsListQ.ID
              loop loc:x = 1 to records (self.EmailsListQ)
                get (self.EmailsListQ, loc:x)
                if errorcode() <> 0
                  break
                end
                if self.EmailsListQ.ID <> loc:ID ! Don't change the entry we just changed
                  if self.EmailsListQ._BatchID <> 0
                    self.EmailsListQ._BatchID = 0 ! Set all BatchIDs to 0
                    put (self.EmailsListQ)
                  end
                end
              end
            end
          end
        else
          compile ('EndCompile', NETTALKLOG=1)
            self.Log ('NetEmailReceive._ProcessUIDLAll' & ' (' & self._serverDesc & ')', |
                      'Warning could not find Email UID [' & self.EmailsListQ.UID & '] in the EmailsListQ. This could be a new email.', NET:LogError)
          EndCompile
        end
      end
    end
    loc:StartPos = loc:CRPos+2
  end

  if self.OptionsUseDoneBeforeQ
    if self.OptionsDeleteAfterHours
      loc:Hours2 = Today()*24 + (Clock()/360000)
    end
    loop loc:x = 1 to records (self.EmailsDoneBeforeQ)
      get (self.EmailsDoneBeforeQ, loc:x)
      if errorcode() <> 0
        break
      end
      clear (self.EmailsListQ)
      self.EmailsListQ.Size = self.EmailsDoneBeforeQ.Size
      self.EmailsListQ.UID = self.EmailsDoneBeforeQ.UID
      get (self.EmailsListQ, self.EmailsListQ.Size, self.EmailsListQ.UID)
      if errorcode() = 0
        self.EmailsListQ.ToDownload = 0 ! Skip it
        self.EmailsListQ.DownloadDate = self.EmailsDoneBeforeQ.DownloadDate
        self.EmailsListQ.DownloadTime = self.EmailsDoneBeforeQ.DownloadTime
        if self.OptionsDeleteAfterHours
          loc:Hours1 = loc:Hours2 - (self.EmailsListQ.DownloadDate*24 + self.EmailsListQ.DownloadTime/360000)
          if loc:Hours1 >= self.OptionsDeleteAfterHours
            compile ('EndCompile', NETTALKLOG=1)
              if self.LoggingOn
                self.Log ('NetEmailReceive._ProcessUIDLAll' & ' (' & self._serverDesc & ')', |
                          'Need to Delete UID = ' & clip (self.EmailsListQ.UID) & ' as it is ' & loc:Hours1 & ' old.')
              end
            EndCompile
            self.EmailsListQ.ToDelete = 1 ! Delete It
          end
        end
        put (self.EmailsListQ)
        loc:MyCount += 1
      end
    end
    compile ('EndCompile', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetEmailReceive._ProcessUIDLAll' & ' (' & self._serverDesc & ')', |
                  'Processed OptionsUseDoneBeforeQ - ' & loc:MyCount & ' records done before (and will not be downloaded)')
      end
    EndCompile
  end

  return (loc:ret)


!-----------------------------------------------------------
NetEmailReceive._GetToDelete Procedure ()
  CODE
  ! Looks at the current emails ToDelete value in self.EmailsListQ
  if self._UseList
    clear (self.EmailsListQ)
    self.EmailsListQ._BatchID = self._CurrentMessageNum
    get (self.EmailsListQ, self.EmailsListQ._BatchID)
    if errorcode() <> 0
      compile ('EndCompile', NETTALKLOG=1)
        self.Log ('NetEmailReceive._GetToDelete' & ' (' & self._serverDesc & ')', |
                  'Warning could not find Email ID [' & self.EmailsListQ._BatchID & '] in the EmailsListQ', NET:LogError)
      EndCompile
      return (0) ! no delete - could not find item in queue
    end
    return (self.EmailsListQ.ToDelete)
  end
  return (0) ! false - no delete


!-----------------------------------------------------------
NetEmailReceive._GetToDownload Procedure ()
  CODE
  ! Looks at the current emails ToDelete value in self.EmailsListQ
  if self._UseList
    clear (self.EmailsListQ)
    self.EmailsListQ._BatchID = self._CurrentMessageNum
    get (self.EmailsListQ, self.EmailsListQ._BatchID)
    if errorcode() <> 0
      compile ('EndCompile', NETTALKLOG=1)
        self.Log ('NetEmailReceive._GetToDownload' & ' (' & self._serverDesc & ')', |
                  'Warning could not find Email ID [' & self.EmailsListQ._BatchID & '] in the EmailsListQ', NET:LogError)
      EndCompile
      return (0)                                            ! no download - could not find item in queue
    end
    return (self.EmailsListQ.ToDownload)
  end
  return (1)                                                ! false - yes download


!-----------------------------------------------------------
NetEmailReceive._LoginSuccessful Procedure ()
  code




!-----------------------------------------------------------
NetEmailSend.PostError Procedure(long err, long errStatus, long errAction, string msg, string functionName)
  code
    self.error = Err
    self._ErrorStatus = errStatus
    self._AfterErrorAction = errAction
    self.ErrorTrap (Clip(msg), Clip(functionName))
    return 0

!-----------------------------------------------------------
NetEmailSend._CloseIdleConnection Procedure()
  code
    self.Log ('NetEmailSend._CloseIdleConnection' & ' (' & self._serverDesc & ')', 'The connection was idle and will be aborted.')
    self.Error = ERROR:IdleTimeOut
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Idle Timeout - Warning connection has been idle for more than ' & |
                  self._Connection.InActiveTimeout/100 & ' seconds. Connection will be closed. State = ' &  self._state, 'NetEmailSend._CloseIdleConnection')
    self.abort()


NetEmailSend.ResetProgressControl Procedure()
  code
    if self.progresscontrol                          ! Update the Progress Control Control if there is one
        self.progresscontrol{PROP:RangeLow} = 0        ! zero to 1000
        self.progresscontrol{PROP:RangeHigh} = 1000    ! zero to 1000
        self.progresscontrol{prop:progress} = 0
        self.progresscontrol{prop:hide} = 0
        Display(self.progressControl)
    end


!-----------------------------------------------------------
NetEmailSend.SendMail Procedure (long mode)
myEmail           string (80)
myDesc            string (80)
currentSize       long
NewWhole          byte
UseMime           byte
MimeBoundary1     string (20)
MimeBoundary2     string (80)
AttachmentsExist  byte
EmbedsExist       byte
embedsQ           queue(Net:EmbedsQType), pre(embedsQ)
                  end
StringRef         &String
FilePath          string (NET:StdEmailPathSize)
FileName          string (NET:StdEmailPathSize) ! Was 80
filecount         long
startpos          long
endpos            long
x                 long
len1              long
encode1           long
encode2           long
QPlinelength      long
Base64EncodedStr  string (NET:Base64MaxEncodedLength)
QPEncodedStr      string (NET:QPMaxEncodedLength)
tempTime          string (10)
tempFileType      string (32)                               ! Sep09: Increased this from 10
len2              long
len3              long
UsesQuotes        byte
StopNow           byte
tempWholeLen      long
myListIndex       long
i                 long
pn                equate('NetEmailSend.SendMail')
  CODE
    if self._mx = 1
        self.Log (pn & ' (' & self._serverDesc & ')', 'SendMail called. Will load email into Queue, to be sent shortly via MX direct email')
    else
        self.Log (pn & ' (' & self._serverDesc & ')', 'SendMail called. Will try and send ' & self._serverDesc & ' to ' & clip (self.Server) & ' on port ' & self.port)
    end

  ! Mode must be set to one of the following:
  !  NET:EmailWholeMessageMode uses the WholeMessageProperty
  !  NET:EMailMadeFromPartsMode uses the Properties like To & From etc.

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if self.progresscontrol                          ! Update the Progress Control Control if there is one
    self.progresscontrol{PROP:RangeLow} = 0        ! zero to 1000
    self.progresscontrol{PROP:RangeHigh} = 1000    ! zero to 1000
    self.progresscontrol{prop:progress} = 0
    self.progresscontrol{prop:hide} = 0
    display(self.progressControl)
  end

  self._SSLDontConnectOnOpen = 0
  if self.SecureEmailStartTLS = 1 ! Use Secure Email (SSL or TLS) SMTP - new 25/5/2004
    self.SSL = 1
    !self.SSLMethod = NET:SSLMethodTLSv1
    !self.SSLMethod = NET:SSLMethodSSLv23 ! Tries TLS v1.0, then SSL v3.0, then SSL v2.0
    self._SSLDontConnectOnOpen = 1
  end

  ! Load data into the queue
  Clear (self.DataQueue)
  if mode = NET:EMailMadeFromPartsMode
    case self._objectType
    of NET:POP_CLIENT orof NET:SMTP_CLIENT
      if self.ToList = '' and self.CClist = '' and self.BccList = ''
        return self.PostError(ERROR:ObjectRequiresMoreProperties, NET:EINVALIDDATA, NET:ANONE, |
                              'No To, CC or BCC field specified when sending ' & self._serverDesc, pn)
      end
    of NET:NNTP_CLIENT
      if self.newsGroup = ''
        return self.PostError(ERROR:ObjectRequiresMoreProperties, NET:EINVALIDDATA, NET:ANONE, |
                              'No Newsgroup field specified when sending ' & self._serverDesc, pn)
      end
    end
    if self.From = ''
        return self.PostError(ERROR:ObjectRequiresMoreProperties, NET:EINVALIDDATA, NET:ANONE, |
                              'No From Field specified when sending ' & self._serverDesc, pn)
    end
    Do BuildWholeMessage
    self.DataQueue.WholeMessageLen = self.wholeMessageLen
    self.DataQueue.WholeMessage &= new (String(self.wholeMessageLen))
    self.DataQueue.WholeMessage = self.wholeMessage
    self.DataQueue.ToList = self.ToList
    self.DataQueue.CCList = self.CcList
    self.DataQueue.BccList = self.BccList
    self.DataQueue.ReplyTo = self.ReplyTo
    self.DataQueue.Organization = self.Organization
    self.DataQueue.DeliveryReceiptTo = self.DeliveryReceiptTo
    self.DataQueue.DispositionNotificationTo = self.DispositionNotificationTo
    self.DataQueue.Newsgroup = self.Newsgroup
    self.DataQueue.From = self.From
    self.DataQueue.Subject = self.Subject
    self.DataQueue.ExtraHeader = self.ExtraHeader
    self.DataQueue.MessageTextLen = self.MessageTextLen
    self.DataQueue.MessageText &= new (String(self.MessageTextLen))
    self.DataQueue.MessageText = self.MessageText
    if self.MessageHtmlLen > 0
      self.DataQueue.MessageHtmlLen = self.MessageHtmlLen
      self.DataQueue.MessageHtml &= new (String(self.MessageHtmlLen))
      self.DataQueue.MessageHtml = self.MessageHtml
    else
      self.DataQueue.MessageHtmlLen = 0
    end
    self.DataQueue.AttachmentList = self.AttachmentList
    self.DataQueue.EmbedList = self.EmbedList
  elsif mode = NET:EmailWholeMessageMode
    self.DataQueue.WholeMessageLen = self.wholeMessageLen
    self.DataQueue.WholeMessage &= new (String(self.wholeMessageLen))
    self.DataQueue.WholeMessage = self.wholeMessage
    self.DataQueue.Newsgroup = ''
    self.DataQueue.Subject = ''
    self.DataQueue.ReplyTo = ''
    self.DataQueue.Organization = ''
    self.DataQueue.DeliveryReceiptTo = ''
    self.DataQueue.DispositionNotificationTo = ''
    self.DataQueue.ExtraHeader = ''
    self.DataQueue.MessageTextLen = 0
    self.DataQueue.MessageHtmlLen = 0
    self.DataQueue.ReplyTo = ''
    self.DataQueue.Organization = ''
    self.DataQueue.DeliveryReceiptTo = ''

    Do BuildPartsFromWholeMessage

    self.DataQueue.ToList = self.ToList
    self.DataQueue.CCList = self.CcList
    self.DataQueue.BccList = self.BccList
    self.DataQueue.Newsgroup = self.Newsgroup
    self.DataQueue.From = self.From

  else
    return self.PostError(ERROR:BadOptions, NET:EINVALIDDATA, NET:ANONE, |
                          'Incorrect Mode parameter passed to function.', pn)
  end

  ! Oct11: Changed to use the machine name. Using the domain is only
  ! a good choice for servers with correctly configured DNS records
  if self.helo = ''
    NetOptions(NET:GETHOSTNAME, self.Helo)
    if self.Helo = ''
      ! Set it to something
      self.Helo = 'helo_is_blank'
    end
  end

  Add (self.DataQueue)
  if Errorcode()
    Clear (self.DataQueue)
    Get (self.DataQueue, 1)  ! return to the first item in the queue
    return self.PostError(ERROR:QueueAccessError, NET:EINVALIDDATA, NET:ANONE, 'Could not add ' & self._serverDesc & ' to the queue', pn)
  end

  self.ResetProgressControl()

  Get (self.DataQueue, 1)  ! return to the first item in the queue
  if self.openFlag = 0
    self._TryOpen()
    ! Now wait for Async Open, or for the response from the server
  else
    if self._State = NET:ES_NOTCONNECTED
      if self.progresscontrol                          ! Update the Progress Control Control if there is one
        self.progresscontrol{prop:hide} = 1
        self.progresscontrol{prop:progress} = 0
        display(self.progressControl)
      end
      return self.PostError(self.error, NET:ESERVER, NET:ADELETE, 'Illegal State. NET:ES_NOTCONNECTED in section of SendMail.', pn)
    elsif self._State = NET:ES_CONNECTEDANDIDLE
      if self._objectType = NET:POP_CLIENT or self._objectType = NET:SMTP_CLIENT
        self._State = NET:ES_READY              ! Added by Jono 20/3/2001
        self.packet.bindata = 'RSET<13,10>'
      end
      if self._objectType = NET:NNTP_ClIENT
        self._State = NET:NS_READY              ! Added by Jono 21/3/2001
        self.packet.bindata = 'DATE<13,10>'     ! Any arb command
      end
      self.packet.bindatalen = len (clip (self.packet.bindata))
      if self.packet.binDataLen > 0
        self.Log (pn & ' (' & self._serverDesc & ')', 'Sending --> [' & self.packet.binData [1 : self.packet.bindatalen] & ']')
      end
      self.send()
    else
      ! It will get processed automatically in Process()
    end
  end
  return 1


BuildPartsFromWholeMessage Routine
    !Message ('[' & self.wholeMessage [loc:StartPos : loc:endPos] & ']')
  self.ToList = self._GetHeaderField ('To:',1)
  self.CCList = self._GetHeaderField ('CC:',1)
  self.BccList = self._GetHeaderField ('Bcc:',1)  ! There probably isn't a BCC field !!
  self.From = self._GetHeaderField ('From:',1)
  self.Newsgroup = self._GetHeaderField ('Newsgroups:',1)
  self.AfterBuildFromWhole()               ! New 30/8/2001 allow programers to change the To, CC, BCC From etc.


BuildWholeMessage Routine
    ! ---------- Decide size of Whole Message
    self.wholeMessageLen = NET:EmailEstimatedExtraWholeMessageSize + Len (clip(self.ToList)) + |
                           Len (clip(self.CCList)) + Len (clip(self.From)) + Len(clip(self.Subject)) + |
                           Len (clip(self.ReplyTo)) + len (clip(self.organization)) + |
                           len (clip(self.DispositionNotificationTo)) + len (clip (self.DeliveryReceiptTo)) + |
                           len(clip(self.ExtraHeader)) + self.MessageTextLen + self.MessageHTMLLen

    NewWhole = false
    currentSize = (size (self.wholeMessage))
    if self.wholeMessageLen <= currentSize
      ! Okay there is enough memory allocated.
      ! Just check there is not more than 64K extra memory allocated.
      if (currentSize - self.wholeMessageLen) > NET:EmailNewDifference
        NewWhole = True
      end
    else
      NewWhole = True
    end

    if NewWhole
      if currentSize > 0
        dispose (self.wholeMessage)
      end
      self.wholeMessage &= new (String (self.wholeMessageLen))
      if size (self.wholeMessage) < self.wholeMessageLen
        return self.PostError(ERROR:CouldNotAllocMemory, NET:EINVALIDDATA, NET:ANONE, 'Unable to allocate memory required for sending ' & self._serverDesc & '.', pn)
      end
    end

    ! ================= Header ==================
    MimeBoundary1 = '--=_NextPart'  ! Good to have a _ in it
    self._IDCount += 1
    MimeBoundary2 = 'd' & clip (format(today(),@D12)) & |
                    't' & clip (left (format (clock(),@T5))) & |
                    '.' & clock()%100 & |
                    'c' & self._IDCount & |
                    'r' & random (1, 1000000)

    if self._GotHostName = 0
      NetOptions (NET:GETHOSTNAME, self._HostName)
      if self._HostName = ''
        self._HostName = 'UnknownHost' & Random(1,100000)
      else
        self._GotHostName = 1
      end
    end
    self.wholeMessage = 'Message-ID: <<' & clip (MimeBoundary2) & '.00@' & |
                        clip (self._HostName) & '><13,10>'

    ! ---------- Delivery-receipt-to  -------------
    if self.DeliveryReceiptTo <> ''
      self.wholeMessage = clip(self.wholeMessage) & |
                          'Delivery-receipt-to: ' & clip(self.DeliveryReceiptTo) & '<13,10>'
    end

    ! ---------- Disposition-Notification-To  -------------
    if self.DispositionNotificationTo <> ''
      self.wholeMessage = clip(self.wholeMessage) & |
                          'Disposition-Notification-To: ' & clip(self.DispositionNotificationTo) & '<13,10>'
    end

    ! ---------- ReplyTo  -------------
    if self.replyTo <> ''
      self.wholeMessage = clip(self.wholeMessage) & |
                          'Reply-To: ' & clip(self.ReplyTo) & '<13,10>'
    end

    ! ---------- From -------------
    if self.CalcEmailName (self.From, myDesc, myEmail, 1)
      self.wholeMessage = clip(self.wholeMessage) & |
                          'From: "' & clip(MyDesc) & '" <<' & clip(MyEmail) & '><13,10>'
    end
    ! ---------- Newsgroup -------------
    if self._objectType = NET:NNTP_CLIENT
      self.wholeMessage = clip(self.wholeMessage) & |
                          'Newsgroups: ' & clip(self.Newsgroup) & '<13,10>'
    end

    ! ---------- To -------------
    !self._ListIndex = 0
    myListIndex = 0
    if self._objectType = NET:POP_CLIENT or self._objectType = NET:SMTP_CLIENT
      if len (clip(self.ToList)) > 0
        loop
          !self._ListIndex += 1
          myListIndex += 1
          if self.CalcEmailName (self.ToList, myDesc, myEmail, myListIndex) !self._ListIndex)
            if myListIndex = 1
               self.wholeMessage = clip(self.wholeMessage) & 'To:'
            else
               self.wholeMessage = clip(self.wholeMessage) & ','
            end
            self.wholeMessage = clip(self.wholeMessage) & |
                               ' "' & clip(myDesc) & '" <<' & clip(myEmail) & '>'
          else
            if myListIndex > 1 ! New If statement 23/7/2003
              self.wholeMessage = clip(self.wholeMessage) & '<13,10>'
            end
            break  ! out of loop
          end
        end
      end
    end
    ! ---------- CC Field -------------
    myListIndex = 0
    if self._objectType = NET:POP_CLIENT or self._objectType = NET:SMTP_CLIENT
      if len (clip(self.CCList)) > 0
        loop
          myListIndex += 1
          if self.CalcEmailName (self.CCList, myDesc, myEmail, myListIndex) !self._ListIndex)
            if myListIndex = 1
               self.wholeMessage = clip(self.wholeMessage) & 'CC:'
            else
               self.wholeMessage = clip(self.wholeMessage) & ','
            end
            self.wholeMessage = clip(self.wholeMessage) & |
                               ' "' & clip(myDesc) & '" <<' & clip(myEmail) & '>'
          else
            if myListIndex > 1 ! New If statement 23/7/2003
              self.wholeMessage = clip(self.wholeMessage) & '<13,10>'
            end
            break  ! out of loop
          end
        end
      end
    end
    ! ---------- References (for replies)  -------------

    if self.References <> ''
      self.wholeMessage = clip(self.wholeMessage) & |
                          'References: ' & clip(self.references) & '<13,10>'
    end


    ! ---------- Subject & Date  -------------
    tempTime = format(clock(),@T4)

    self.wholeMessage = clip(self.wholeMessage) & |
                          'Subject: ' & clip(self.subject) & '<13,10>' & |
                          'Date: ' & NET:DayOfWeek [(Today() % 7) + 1] & |
                          ', ' & day(today()) & ' ' & NET:Months[Month(today())] & ' ' & |
                          year(today()) & ' ' & |
                          format(tempTime[1:2],@N02) & ':' & format(tempTime[4:5],@N02) & |
                          ':' & format(tempTime[7:8],@N02) & |
                          ' ' & clip (NetGetTimeZone()) & '<13,10>'
                          ! Mon, 5 Jun 2000 16:54:48 +0200
                          ! clip (format(clock(),@T4)) & |


    ! ---------- Organization  -------------

    if self.Organization <> ''
      self.wholeMessage = clip(self.wholeMessage) & |
                          'Organization: ' & clip(self.organization) & '<13,10>'
    end

    ! -----------------------------

    if clip (self.AttachmentList) <> ''
        AttachmentsExist = true
    end
    if (clip (self.EmbedList) <> '') and (self.MessageHTMLLen > 0)  ! Embeds only work if MessageHTMLlen > 0
        EmbedsExist = true
        ! Sep09: New code to process the embeds list and create a queue
        self.ProcessEmbedList(self.embedList, EmbedsQ)

        ! Sep09: Check for files names that could cause problems and modify them when embedding.
        self.ProcessEmbedNames(embedsQ, self.messageHtml)
    end

    ! ------- Mime --------------

    ! Sean June 2007: New OptionsContentType property allows the default to be overridden and a custom type specified
    ! The new OptionsMultiPartContent property treats the custom Content-Type as a multi-part message and adds the MIME
    ! boundary etc.
    if self.OptionsContentType <> ''                      ! Option to override the content-type for none MIME or Multi-part messages
        if self.OptionsMultiPartContent                   ! Add a MIME boudary to the custom content type (multi part message)
          self.wholeMessage = clip (self.wholeMessage) & |
                              'MIME-version: 1.0<13,10>' & |
                              'Content-Type: ' & Clip(self.OptionsContentType) & ';<13,10>' & |
                              '<9>boundary="' & clip (MimeBoundary1) & '_001_' & clip(MimeBoundary2) & '"<13,10>'

        else                                                ! A plain text message with a custom type
          self.wholeMessage = clip (self.wholeMessage) & |
                              'MIME-version: 1.0<13,10>' & |
                              'Content-Type: ' & Clip(self.OptionsContentType) & ';<13,10>' & |
                              '<9>charset=' & clip (self.OptionsMimeTextCharset) & '<13,10>' & |
                              'Content-Transfer-Encoding: ' & clip (self.OptionsMimeTextTransferEncoding) & '<13,10>'

        end
    else
      if AttachmentsExist
        self.wholeMessage = clip (self.wholeMessage) & |
                            'MIME-version: 1.0<13,10>' & |
                            'Content-Type: multipart/mixed;<13,10>' & |
                            '<9>boundary="' & clip (MimeBoundary1) & '_000_' & clip(MimeBoundary2) & '"<13,10>'
      elsif EmbedsExist ! no attachments  ! Jono 6/3/2001
        self.wholeMessage = clip (self.wholeMessage) & |
                            'MIME-version: 1.0<13,10>' & |
                            'Content-Type: multipart/related; type="multipart/alternative";<13,10>' & |
                            '<9>boundary="' & clip (MimeBoundary1) & '_002_' & clip(MimeBoundary2) & '"<13,10>'
      else
        if self.MessageHTMLLen > 0
          self.wholeMessage = clip (self.wholeMessage) & |
                              'MIME-version: 1.0<13,10>' & |
                              'Content-Type: multipart/alternative;<13,10>' & |
                              '<9>boundary="' & clip (MimeBoundary1) & '_001_' & clip(MimeBoundary2) & '"<13,10>'
        else
          self.wholeMessage = clip (self.wholeMessage) & |
                              'MIME-version: 1.0<13,10>' & |
                              'Content-Type: text/plain;<13,10>' & |
                              '<9>charset=' & clip (self.OptionsMimeTextCharset) & '<13,10>' & |
                              'Content-Transfer-Encoding: ' & clip (self.OptionsMimeTextTransferEncoding) & '<13,10>'
        end
      end
    end

    ! ------- X-Mailer --------------
    case self._objectType
    of NET:POP_CLIENT orof NET:SMTP_CLIENT
      self.wholeMessage = Clip(self.wholeMessage) |
                        & 'X-Mailer: ' & clip (self.XMailer) & '<13,10>' |
                        & 'X-NetTalk: NetTalk Email ' & NETTALK:EmailVersion & '<13,10>'

    of NET:NNTP_CLIENT
      self.wholeMessage = clip (self.wholeMessage) |
                        & 'X-Newsreader: ' & clip (self.XMailer) & '<13,10>' |
                        & 'X-NetTalk: NetTalk News ' & NETTALK:EmailVersion & '<13,10>'
    end

    ! ------- Extra User Specified Header --------------
    if self.ExtraHeader <> ''
      Len2 = len (clip(self.ExtraHeader))
      if len2 >= 2
        if sub (self.ExtraHeader,len2-1,2) <> '<13,10>'
          self.ExtraHeader = clip (self.ExtraHeader) & '<13,10>'
        end
      end
      self.wholeMessage = clip (self.wholeMessage) & clip (self.ExtraHeader)
    end
    self.wholeMessage = clip (self.wholeMessage) & '<13,10>'

    ! ===========================================
    ! ================= BODY ====================
    ! ===========================================

    ! ------------------- Mime For Body Part --------------------------
    if self.MessageHTMLLen > 0
      ! ------------------- Mime For Non Mime compatible email clients -----------------
      self.wholeMessage = clip (self.wholeMessage) & |
                          'This is a multi-part message in MIME format.<13,10>' & |
                          'This email requires a MIME compatible email reader.<13,10>' & |
                          '<13,10>'

      if EmbedsExist   ! Jono 6/3/2001
        ! ------------------- Mime For Mixed Mime -----------------
        if AttachmentsExist
          self.wholeMessage = clip (self.wholeMessage) & |
                              '--' & clip (MimeBoundary1) & '_000_' & clip(MimeBoundary2) & '<13,10>' & |
                              'Content-Type: multipart/related;<13,10>' & |
                              '<9>type="multipart/related";<13,10>' & |
                              '<9>boundary="' & clip (MimeBoundary1) & '_002_' & clip(MimeBoundary2) & '"<13,10>' & |
                              '<13,10>' & |
                              '<13,10>'
        end
        self.wholeMessage = clip (self.wholeMessage) & |
                            '--' & clip (MimeBoundary1) & '_002_' & clip(MimeBoundary2) & '<13,10>' & |
                            'Content-Type: multipart/alternative;<13,10>' & |
                            '<9>boundary="' & clip (MimeBoundary1) & '_001_' & clip(MimeBoundary2) & '"<13,10>' & |
                            '<13,10>'
      elsif AttachmentsExist
        ! ------------------- Mime For Mixed Mime -----------------
        self.wholeMessage = clip (self.wholeMessage) & |
                            '--' & clip (MimeBoundary1) & '_000_' & clip(MimeBoundary2) & '<13,10>' & |
                            'Content-Type: multipart/alternative;<13,10>' & |
                            '<9>boundary="' & clip (MimeBoundary1) & '_001_' & clip(MimeBoundary2) & '"<13,10>' & |
                            '<13,10>'
      end

      ! ------------------- Mime For Text Part --------------------------
      self.wholeMessage = clip (self.wholeMessage) & |
                          '--' & clip (MimeBoundary1) & '_001_' & clip(MimeBoundary2) & '<13,10>' & |
                          'Content-Type: text/plain;<13,10>' & |
                          '<9>charset=' & clip (self.OptionsMimeTextCharset) & '<13,10>' & |
                          'Content-Transfer-Encoding: ' & clip (self.OptionsMimeTextTransferEncoding) & '<13,10>' & |
                          '<13,10>'
    elsif AttachmentsExist          ! added by Jono 16/8/2000
      ! No point in handling Embeds here - as you need to use HTML to enable Embeds.
      ! ------------------- Mime For Non Mime compatible email clients -----------------
      self.wholeMessage = clip (self.wholeMessage) & |
                          'This is a multi-part message in MIME format.<13,10>' & |
                          'This email requires a MIME compatible email reader.<13,10>' & |
                          '<13,10>'

      ! ------------------- Mime For Text Part --------------------------
      self.wholeMessage = clip (self.wholeMessage) & |
                          '--' & clip (MimeBoundary1) & '_000_' & clip(MimeBoundary2) & '<13,10>' & |
                          'Content-Type: text/plain;<13,10>' & |
                          '<9>charset=' & clip (self.OptionsMimeTextCharset) & '<13,10>' & |
                          'Content-Transfer-Encoding: ' & clip (self.OptionsMimeTextTransferEncoding) & '<13,10>' & |
                          '<13,10>'

    end

    ! ------------------- Text Part --------------------------
    if self.OptionsMimeTextDontEncode = true
      if self.MessageTextLen > 0
        self.wholeMessage = clip (self.wholeMessage) & |
                            self.MessageText [1 : self.MessageTextLen]
      else
        self.Log (pn & ' (' & self._serverDesc & ')', 'Attention. TextLen = 0')
      end
    else
      case lower (self.OptionsMimeTextTransferEncoding)
      of 'quoted-printable'
        encode1 = 1
        QPlinelength = 0        ! QP Line Length
        tempWholeLen = len (clip (self.wholeMessage))
        loop while encode1 <= (self.MessageTextLen)
          if (encode1 + NET:QPMaxDecodedLength - 1) <= (self.MessageTextLen)
            encode2 = (encode1 + NET:QPMaxDecodedLength) - 1
          else
            encode2 = self.MessageTextLen
          end
          len1 = encode2 - encode1 + 1

          if encode2 < encode1
            self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. Encode2 << Encode1 !!! May cause Index error or GPF')
          end

          QPEncodedStr = NetQuotedPrintableEncode (self.MessageText [encode1 : encode2], len1, QPlinelength)
          if self.wholeMessageLen < (tempWholeLen + len1 + NET:EmailEstimatedExtraWholeMessageSize)
            self.wholeMessageLen = (tempWholeLen + len1 + NET:EmailEstimatedExtraWholeMessageSize)
            if self._ProcessResizeWholeMessage(self.wholeMessageLen)
              return 1
            end
          end
          if len1 = 0
            self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. len1 = 0 !!! May cause Index error or GPF')
          end

          if len1 >= 1
            self.wholeMessage [(tempWholeLen+1) : (tempWholeLen+len1)] = QPEncodedStr [1 : len1]
            tempWholeLen += len1
          end
          encode1 = encode2 + 1
        end
      else
        if self.MessageTextLen > 0
          self.wholeMessage = clip (self.wholeMessage) & |
                              self.MessageText [1 : self.MessageTextLen]
        else
          compile ('EndCompile', NETTALKLOG=1)
            if self.LoggingOn
              self.Log (pn & ' (' & self._serverDesc & ')', 'Attention. (b) TextLen = 0')
            end
          EndCompile
        end
      end
    end

    ! ------------------- End Mime for Text Part ----------------------
    if self.MessageHTMLLen = 0 and AttachmentsExist
      self.wholeMessage = clip (self.wholeMessage) & '<13,10>'
    end

    ! ------------------- Mime For HTML Part --------------------------
    if self.MessageHTMLLen > 0
      ! MIME Support
      self.wholeMessage = clip (self.wholeMessage) & |
                          '<13,10>' & |
                          '<13,10>' & |
                          '--' & clip (MimeBoundary1) & '_001_' & clip(MimeBoundary2) & '<13,10>' & |
                          'Content-Type: text/html;<13,10>' & |
                          '<9>charset=' & clip (self.OptionsMimeHtmlCharset) & '<13,10>' & |
                          'Content-Transfer-Encoding: ' & clip (self.OptionsMimeHtmlTransferEncoding) & '<13,10>' & |
                          '<13,10>'
      if self.OptionsMimeHtmlDontEncode = true
        self.wholeMessage = clip (self.wholeMessage) & |
                            self.MessageHtml [1 : self.MessageHtmlLen]
      else
        case lower (self.OptionsMimeHtmlTransferEncoding)
        of 'quoted-printable'
          encode1 = 1
          QPlinelength = 0
          tempWholeLen = len (clip (self.wholeMessage))
          loop while encode1 <= (self.MessageHtmlLen)
            if (encode1 + NET:QPMaxDecodedLength - 1) <= (self.MessageHtmlLen)
              encode2 = (encode1 + NET:QPMaxDecodedLength) - 1
            else
              encode2 = self.MessageHtmlLen
            end
            len1 = encode2 - encode1 + 1

            if encode2 < encode1
              self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. case 2. Encode2 << Encode1 !!! May cause Index error or GPF')
            end

            QPEncodedStr = NetQuotedPrintableEncode (self.MessageHtml [encode1 : encode2], len1, QPlinelength)
            if self.wholeMessageLen < (len (clip (self.wholeMessage)) + len1 + NET:EmailEstimatedExtraWholeMessageSize)
              self.wholeMessageLen = (len (clip (self.wholeMessage)) + len1 + NET:EmailEstimatedExtraWholeMessageSize)
              if self._ProcessResizeWholeMessage(self.wholeMessageLen)
                return 1
              end
            end
            if self.wholeMessageLen < (tempWholeLen + len1 + NET:EmailEstimatedExtraWholeMessageSize)
              self.wholeMessageLen = (tempWholeLen + len1 + NET:EmailEstimatedExtraWholeMessageSize)
              if self._ProcessResizeWholeMessage(self.wholeMessageLen)
                return 1
              end
            end
            ! End New changes 4/4/2002

            compile ('EndCompile', NETTALKLOG=1)
              if self.LoggingOn
                if len1 = 0
                  self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. case 2. len1 = 0 !!! May cause Index error or GPF')
                end
              end
            EndCompile

            ! Changes 6/12/2002
            ! was self.wholeMessage = clip (self.wholeMessage) & QPEncodedStr [1 : len1]
            if len1 >= 1
              self.wholeMessage [(tempWholeLen+1) : (tempWholeLen+len1)] = QPEncodedStr [1 : len1]
              tempWholeLen += len1
            end
            ! End Changes 6/12/2002

            encode1 = encode2 + 1
          end
        else
          self.wholeMessage = clip (self.wholeMessage) & |
                              self.MessageHtml [1 : self.MessageHtmlLen]
        end
      end
      self.wholeMessage = clip (self.wholeMessage) & |
                          '<13,10>' & |
                          '<13,10>' & |
                          '--' & clip (MimeBoundary1) & '_001_' & clip(MimeBoundary2) & '--<13,10>'  ! End Mime Boundary
    end

    ! ------------------- Attachments & Embeds --------------------------
    Do ProcessEmbeds
    Do ProcessAttachments

    self.wholeMessageLen = len(clip (self.wholeMessage))
    self.AfterBuildFromParts()               ! New 26/7/2002 allow programers to change the To, CC, BCC, From etc. or their own fields in DataQueue

! ===========================================================================================

!ReSizeWholeMessage Routine                                  !
!  if self._ProcessResizeWholeMessage(self.wholeMessageLen)
!    return (1)
!  end
! exit

!    currentSize = (size (self.wholeMessage))
!    NewWhole = false
!    if self.wholeMessageLen <= currentSize
!      ! Okay there is enough memory allocated.
!    else
!      NewWhole = True
!    end
!
!    if NewWhole
!      StringRef &= self.wholeMessage
!      self.wholeMessage &= new (String (self.wholeMessageLen))
!      if size (self.wholeMessage) < self.wholeMessageLen
!        return self.PostError(ERROR:CouldNotAllocMemory, NET:EINVALIDDATA, NET:ANONE, 'Unable to allocate memory required for sending email.', pn)
!      end
!      if not self.wholeMessage &= null                        ! Only dispose if WholeMessage <> NULL
!        self.wholeMessage = Clip(StringRef)
!        Dispose (StringRef)
!      else
!        self.wholeMessage = ''
!      end
!    end

ProcessAttachments Routine
    if AttachmentsExist
      self._ConvertList (self.AttachmentList)  ! Converts from the File Browse format
      !was Do ConvertAttachmentList
      if instring ('"', self.attachmentlist, 1, 1)
        UsesQuotes = true
      else
        UsesQuotes = false
      end
      filecount = 0
      loop
        filecount += 1                                        ! Get FilePath for each file
        startpos = 0
        StopNow = false
        loop x = 1 to (filecount - 1)
          if UsesQuotes
            startpos = instring ('"', self.attachmentlist, 1, (startpos + 1))
            startpos = instring ('"', self.attachmentlist, 1, (startpos + 1))
            startpos = instring (',', self.attachmentlist, 1, (startpos + 1))
          else
            startpos = instring (',', self.attachmentlist, 1, (startpos + 1))
          end
          if startpos = 0
            StopNow = true
            break
          end
        end
        if StopNow = true
          break
        end

        if UsesQuotes
          startpos = instring ('"', self.attachmentlist, 1, (startpos + 1))
        end

        if startpos = 0 and (filecount <> 1)
          break
        end

        if UsesQuotes
          endpos = instring ('"', self.attachmentlist, 1, (startpos + 1))
        else
          endpos = instring (',', self.attachmentlist, 1, (startpos + 1))
        end

        if endpos = 0
          if self.LoggingOn
            if ((Len (Clip (self.attachmentlist))) < 1)
              self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 1a) out of range !!! May cause Index error or GPF')
            elsif  ((startpos + 1) > (len (clip (self.attachmentlist))))
              self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 1b) out of range !!! May cause Index error or GPF')
            end
          end
          filepath = clip(left(self.attachmentlist [(startpos + 1): (len (clip (self.attachmentlist)))]))
        else
          if self.LoggingOn
            if ((endpos-1) < 1)
              self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 2a) out of range !!! May cause Index error or GPF')
            elsif ((startpos + 1) > (endpos - 1))
              self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 2b) out of range !!! May cause Index error or GPF')
            end
          end
          filepath = clip (left(self.attachmentlist [(startpos + 1) : (endpos - 1)]))
        end

        ! Begin strip out "
        len3 = len (clip(filepath))
        if len3 > 0
          if filepath[1] = '"'
            filepath = sub (filepath, 2, len3-1)
            len3 = len (clip(filepath))
          end
          if filepath[len3] = '"'
            filepath = sub (filepath, 1, len3-1)
            len3 = len (clip(filepath))
          end
        end
        ! End strip out "

        if filepath = ''
          cycle
        end

        startpos = 0                                          ! Just work out the filename
        loop while instring('\', filepath, 1, startpos + 1) <> 0
          startpos = instring('\', filePath,1,startpos + 1)
        end
        if startpos = 0
          filename = filepath
        else
          if self.LoggingOn
            if (len (clip(filepath)) < 1)
              self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 3a) out of range !!! May cause Index error or GPF')
            elsif ((startpos + 1) > len (clip(filepath)))
              self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 3b) out of range !!! May cause Index error or GPF')
            end
          end
          filename = filePath [ (startpos+1) : len (clip(filepath)) ]
        end
        if self.AddAttachmentFromFile(FilePath,FileName,false,fileCount,MimeBoundary1,MimeBoundary2)            ! 6.44
          return 1
        end
      end  ! looping through attachments

      self.wholeMessage = clip (self.wholeMessage) & |
                          '<13,10>' & |
                          '<13,10>' & |
                          '--' & clip (MimeBoundary1) & '_000_' & clip(MimeBoundary2) & '--<13,10>'  ! End Mime Boundary
    end


! Sep09: New routine uses the embedsQ created by the ProcessEmbedList method
ProcessEmbeds Routine
  loop x = 1 to Records(embedsQ)
    Get(embedsQ, x)
    if self.AddAttachmentFromFile(embedsQ.filepath,embedsQ.FileName,true,FileCount,MimeBoundary1,MimeBoundary2)
      return 1
    end
  end                                                   ! looping through Embeds
  if Records(embedsQ)
    self.wholeMessage = clip (self.wholeMessage) & |
                        '<13,10>' & |
                        '<13,10>' & |
                        '--' & clip (MimeBoundary1) & '_002_' & clip(MimeBoundary2) & '--<13,10>'  ! End Mime Boundary
  End
  Free(embedsQ)       ! Done with queue, free it

!--------------------------------------------------------------------------------------------------
NetEmailSend.AddAttachmentFromFile  Procedure(string FilePath,String FileName,Long pEmbed,Long fileCount, String MimeBoundary1, String MimeBoundary2)
contentType like(self.OptionsAttachmentContentType)
pn          equate('NetEmailSend.AddAttachmentFromFile')
fileLen     long

  code
  self.log(pn,clip(filePath) & ' ' & clip(FileName))
  fileLen = self.OpenAttachmentFile(filepath,fileName)
  self.SetAttachmentContentType(fileName, contentType)
  self.wholeMessageLen += (filelen * 1.37) + 300   ! FileSize + Overhead
  if self._ProcessResizeWholeMessage(self.wholeMessageLen)
    return (1)
  end

  if filecount > 1
      self.wholeMessage = clip (self.wholeMessage) & '<13,10>'
  end

  if pEmbed
    self.wholeMessage = clip (self.wholeMessage) & '<13,10>' & |
                      '--' & clip (MimeBoundary1) & '_002_' & clip(MimeBoundary2) & '<13,10>' & |
                      'Content-Type: ' & clip(contentType) & ';<13,10>' & |
                      ' name="' & clip (filename) & '"<13,10>' & |
                      'Content-Transfer-Encoding: base64<13,10>' &|
                      'Content-ID: <<' & clip (filename) & '><13,10>' & |
                      '<13,10>'
  else ! attachment
    self.wholeMessage = clip (self.wholeMessage) & '<13,10>' & |
                      '--' & clip (MimeBoundary1) & '_000_' & clip(MimeBoundary2) & '<13,10>' & |
                      'Content-Type: ' & Clip (contentType) & ';<13,10>' & |
                      ' name="' & clip (filename) & '"<13,10>' & |
                      'Content-Transfer-Encoding: base64<13,10>' & |
                      'Content-Disposition: attachment;<13,10>' & |
                      ' filename="' & clip (filename) & '"<13,10>' & |
                      '<13,10>'
  end

  self.ReadAttachmentFile(filepath,fileName)
  self.CloseAttachmentFile(filepath,fileName)
  return 0
!--------------------------------------------------------------------------------------------------
NetEmailSend.OpenAttachmentFile   Procedure(string filepath,string fileName)
pn   equate('NetEmailSend.OpenAttachmentFile')
  code
  NetEmailReadFileNameA = filepath
  Open(NetEmailReadFileA, 20h)                          ! Read Only, Deny Write
  if ErrorCode()
      return self.PostError(ERROR:FileAccessError, NET:EINVALIDDATA, NET:ANONE, 'Error opening attached file (' & clip (filepath) & ') for sending via ' & self._serverDesc & '. Error = ' & errorcode() & ' ' & clip (error()) , pn)
  end
  return Bytes(NetEmailReadFileA)
!--------------------------------------------------------------------------------------------------
NetEmailSend.ReadAttachmentfile   Procedure(string filepath,string fileName)
tempAttPos  long
lenRead     long
Base64EncodedStr   string (NET:Base64MaxEncodedLength)
  code
  Set(NetEmailReadFileA)
  tempAttPos = len (clip (self.wholeMessage))
  loop
    Next(NetEmailReadFileA)
    if ErrorCode()
      break
    end
    lenRead = bytes(NetEmailReadFileA)  ! bytes read this go
    Base64EncodedStr = NetBase64Encode (NetEmailReadFileA.r.d, lenRead)
    self.wholeMessage [(tempAttPos + 1) : (tempAttPos + lenRead) ] = Base64EncodedStr [1 : lenRead]  ! Speed improvement
    tempAttPos += lenRead
  end
!--------------------------------------------------------------------------------------------------
NetEmailSend.CloseAttachmentfile   Procedure(string filepath,string fileName)
  code
  Close (NetEmailReadFileA)
!--------------------------------------------------------------------------------------------------
NetEmailSend.SetAttachmentContentType Procedure(string fileName, *string contentType)
nw              NetWebHttp
  code
  contentType = nw._GetContentType(filename)
  if not contentType
    contentType = self.OptionsAttachmentContentType
  end

!-----------------------------------------------------------
! Sep09: If there are embeds, then the file names for the embeds need to be cleaned to remove spaces
! (Outlook does not support space in embed file names for example).
! An queue is created with the actual path to the file on disk, and the name to use for the embed.
!-----------------------------------------------------------
NetEmailSend.ProcessEmbedList Procedure(*string embedList, *Net:EmbedsQType EmbedsQ)
usesQuotes          long
filecount           long
startpos            long
endpos              long
stopNow             long
x                   long
filename            string(File:MaxFileName)
filepath            string(File:MaxFileName)
pn                  equate('NetEmailSend.ProcessEmbedList')
  code
    if self.EmbedList = '' or self.MessageHTMLLen = 0  ! Embeds only work if MessageHTMLlen > 0
        return
    end

    self._ConvertList (self.EmbedList)     ! Converts the FileDialog multi-file selection style list to a standard comma seperated list
    if instring ('"', self.EmbedList, 1, 1)
        usesQuotes = true
    else
        usesQuotes = false
    end
    filecount = 0

    loop
        filecount += 1                                        ! Get FilePath for each file in the self.embedList string
        startpos = 0
        stopNow = false
        loop x = 1 to filecount - 1
            if UsesQuotes
              startpos = instring ('"', self.EmbedList, 1, (startpos + 1))
              startpos = instring ('"', self.EmbedList, 1, (startpos + 1))
              startpos = instring (',', self.EmbedList, 1, (startpos + 1))
            else
              startpos = instring (',', self.EmbedList, 1, (startpos + 1))
            end
            if startpos = 0
                stopNow = true
                break
            end
        end
        if stopNow = true
            break
        end
        if UsesQuotes
            startpos = instring ('"', self.EmbedList, 1, (startpos + 1))
        end

        if startpos = 0 and (filecount <> 1)
            break
        end
        if UsesQuotes
            endpos = instring ('"', self.EmbedList, 1, (startpos + 1))
        else
            endpos = instring (',', self.EmbedList, 1, (startpos + 1))
        end

        if endpos = 0
            if Len(Clip(self.EmbedList)) < 1
                self.Log (pn & ' (' & self._serverDesc & ')', |
                          'Warning. startPos Len (case 1a (embeds)) out of range !!! May cause Index error or GPF')
            elsif  (startpos + 1) > (Len(clip (self.EmbedList)))
                self.Log (pn & ' (' & self._serverDesc & ')', |
                          'Warning. startPos Len (case 1b (embeds)) out of range !!! May cause Index error or GPF')
            end
            filepath = Clip(Left(self.EmbedList [(startpos + 1): Len(Clip(self.EmbedList))]))
        else
            if endpos-1 < 1
                self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 2a) out of range !!! May cause Index error or GPF')
            elsif startpos + 1 > endpos - 1
                self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 2b) out of range !!! May cause Index error or GPF')
            end
            filepath = clip (left(self.EmbedList [(startpos + 1) : (endpos - 1)]))
        end

        ! Begin strip out "
        x = Len(Clip(filepath))
        if x > 0
            if filepath[1] = '"'
                filepath = sub (filepath, 2, x-1)
                x = len (clip(filepath))
            end
            if filepath[x] = '"'
                filepath = sub (filepath, 1, x-1)
                x = len (clip(filepath))
            end
        end
        ! End strip out "

        if filepath = ''
            cycle
        end

        startpos = 0                                        ! Just work out the filename
        loop x = Len(Clip(filePath)) to 1 by -1
            if filePath[x] = '\'                            !'
                startPos = x
                break
            end
        end
        if startpos = 0
            filename = filepath
        else
            if Len(Clip(filepath)) < 1
                self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 5a) out of range !!! May cause Index error or GPF')
            elsif startpos + 1 > Len(Clip(filepath))
                self.Log (pn & ' (' & self._serverDesc & ')', 'Warning. startPos Len (case 5b) out of range !!! May cause Index error or GPF')
            end
            filename = filePath [ (startpos+1) : len (clip(filepath)) ]
        end

        ! Filepath contains the name of the file to open and Filename contains just the name
        EmbedsQ.filePath = Clip(filePath)
        EmbedsQ.fileName = Clip(fileName)
        self.Log (pn & ' (' & self._serverDesc & ')', 'Add an embed: ' & Clip(EmbedsQ.fileName))
        Add(EmbedsQ)
    end


!-----------------------------------------------------------
! Sep09: Processes the embeds queue, fixes filenames that contain spaces, and replaces
! All references to those embeds in the HTML with the corrected file names.
!-----------------------------------------------------------
NetEmailSend.ProcessEmbedNames Procedure(*Net:EmbedsQType embedsQ, *string messageHtml)
oldName             string(File:MaxFileName)
oldNameLen          long
fixName             long
cpos                long
i                   long
spos                long
  code
    loop i = 1 to Records(embedsQ)
        Get(embedsQ, i)
        fixname = 0
        oldname = Clip(embedsQ.fileName)

        loop cpos = 1 to Len(Clip(embedsQ.fileName))
            if embedsQ.fileName[cpos] = ' '             ! replace spaces with hyphens
                embedsQ.fileName[cpos] = '-'
                fixName = 1
            end
        end
        if fixname                                      ! name was changed, need to find all occurances and fix the embed names
            Put(embedsQ)
            oldName = 'cid:' & Clip(oldName)
            oldNameLen = Len(Clip(oldName))
            spos = Instring(Clip(oldName), self.messageHtml, 1, 1)
            loop while spos
                self.MessageHtml[sPos : sPos + oldNameLen-1] = 'cid:' & Clip(embedsQ.filename)  ! The length won't change
                spos = Instring(Clip(oldName), self.messageHtml, 1, sPos + oldNameLen)
            end
        end
    end


!-----------------------------------------------------------
! Sep09: New procedure returns a list of images to embed, and modifies all the links to
! local images in the passed HTML to use the correct CID format for embedding.
! NetEmailSend.EmbedList = NetEmailSend.EmbedImages(messageHtml, 1, '')
!-----------------------------------------------------------
NetEmailSend.EmbedImages Procedure(*string htmlSource, long embedImages = 1, string rootFolder)
sPos                long
ePos                long
tagStart            long
tagEnd              long
fileName            cstring(File:MaxFileName)
tagContents         cstring(1024)
graphicsList        string(8192)        ! size from NetTalk's EmbedList String
j                   long
x                   long
foundBody           long
  code
    ! Supports IMG, BODY and INPUT tags. Examples:
    !     <IMG alt="" hspace=0 src="capesoft.gif" align=baseline border=0>
    !     <BODY background="c:\My Documents\MailBackground.bmp">
    !     <INPUT type=image height=18 hspace=0 width=66 src="C:\My Documents\My Webs\search.gif" border=0 ?>
    ! Ignores images where the src is not local (begins with http://)

    ! TODO: Add support for:
    ! background='imagepath'
    ! style="background: url('imagepath')

    if rootFolder = ''                                      ! For images with a relative path
        rootFolder = LongPath()
    end

    ePos = Instring('>', htmlSource, -1, Len(htmlSource))
    loop while ePos
          !GT changed - go from the back - so we can change without missing stuff out.
        sPos = Instring('<', htmlSource, -1, ePos)
        if sPos = 0
            break
        end

        tagContents = htmlSource[sPos : ePos]

        if Instring('<img', Lower(tagContents), 1, 1)
            tagStart = Instring('src', Lower(tagContents), 1, 1)
            Do SetSrc
        elsif Instring('<body', Lower(tagContents), 1, 1)
            foundBody = true
            tagStart = Instring('background', Lower(tagContents), 1, 1)
            Do SetSrc
        elsif Instring('<input type=image', Lower(tagContents), 1, 1) or Instring('<input type="image"', Lower(tagContents), 1, 1)
            tagStart = Instring('src', Lower(tagContents), 1, 1)
            Do SetSrc
        end

        ePos = Instring('>', htmlSource, -1, sPos)
    end

    Return(graphicsList)


SetSrc routine
    if tagStart
        tagStart = Instring('"', tagContents, 1, tagStart)
        if tagStart
            tagEnd = Instring('"', tagContents, 1, tagStart + 1)
            if tagEnd
                fileName = tagContents[tagStart + 1 : tagEnd - 1]
                if Instring('http://', Lower(fileName), 1, 1)           ! Ignore linked files
                    exit
                elsif Instring('cid:', Lower(fileName), 1, 1)           ! Ignore embeds
                    exit
                elsif fileName = ''                                     ! Ignore blank file name
                    exit
                else

                    if not Instring(':', fileName, 1, 1) and not fileName[1:2] = '\\'  ! Doesn't contain the full path or UNC path (GT added UNC Path 5 Aug 2008)
                        fileName = Clip(rootFolder) & '\' & Clip(fileName)   !'
                    end

                    Do ReverseFileNameSlashes

                    if not Instring(Clip(fileName), graphicsList, 1, 1)  ! Doesn't already exist in the list
                        if graphicsList = ''
                            graphicsList = Clip(fileName)
                        else
                            graphicsList = Clip(graphicsList) & ',' & Clip(fileName)
                        end
                    end

                    if embedImages
                        j = Instring('\', fileName, -1, Len(Clip(fileName)))         ! Remove the path from the fileName'
                        if j
                            fileName = fileName[j + 1 : Len(fileName)]
                        end

                        htmlSource = htmlSource[1 : sPos+tagStart-1] & 'cid:' & Clip(fileName) & htmlSource[sPos + tagEnd-1 : Len(htmlSource)]
                    end
                end
            end
        end
    end

ReverseFileNameSlashes routine                              ! Reverse any "/" to "\" '
    x = instring ('/', FileName, 1, 1)                      !'
    loop while x
        FileName[x] = '\'
        x = Instring ('/', FileName, 1, x)                  !'
    end


!-----------------------------------------------------------
! This Method is automatically called for your when you call the SendMail() Method
! Make sure you have set self.Server to the Email Server Host Name or IP Address
! self.Port Defaults to 25 (SMTP Port Default), but it can be set to something else
NetEmailSend.Open    Procedure (string Server,uShort Port=0)
  code
    self.Log ('NetEmailSend.Open' & ' (' & self._serverDesc & ')', 'Trying to open connection to remote ' & self._serverDesc & ' Server')

    self._MessagesSentOnCurrentConnection = 0
    self._ServerReplyBufferLen = 0

    Parent.Open(Server, Port)


!-----------------------------------------------------------
NetEmailSend.Close Procedure()
  CODE
    self.Log ('NetEmailSend.Close' & ' (' & self._serverDesc & ')', 'Trying to close connection to the remote ' & self._serverDesc & ' Server')
    parent.close()
    self._MessagesSentOnCurrentConnection = 0
    self._State = NET:ES_NOTCONNECTED


!-----------------------------------------------------------
NetEmailSend.Init Procedure (uLong Mode=NET:SimpleClient)
  CODE
    self.Log ('NetEmailSend.Init' & ' (' & self._serverDesc & ')', 'Init called')

    if self._serverDesc_Set = 0
        self._serverDesc_Set = 1
        self._objectType = NET:SMTP_CLIENT
        self._ServerDesc = 'Email (SMTP)'
        if self.XMailer = ''
            self.XMailer = 'NetTalk Email ' & NETTALK:EmailVersion
        end
        if self.Port = 0
            self.Port = NET:SMTP_PORT
        end
    end

    if self._Inited = 0                 ! Set Default Settings
        self.Port = 25                    ! Default SMTP Port
        self.OptionsMimeTextCharset = 'us-ascii'
        self.OptionsMimeTextTransferEncoding = '7bit'
        self.OptionsMimeHtmlCharset = 'us-ascii'
        self.OptionsMimeHtmlTransferEncoding = 'quoted-printable'
        self.OptionsAttachmentContentType = 'application/x-unknown'
    end

    ! -------- Set up for using Asynchronous Open
    self.AsyncOpenUse = 1
    self.AsyncOpenTimeOut = 1200 ! 12 sec
    self.InActiveTimeout = 9000  ! 90 sec
    ! --------

    self._DataQueue &= new (Net:EmailDataQType)
    self.DataQueue &= self._DataQueue

    Parent.init(NET:SimpleClient)       ! Ignore Mode parameter. This is an email Client


!-----------------------------------------------------------
NetEmailSend.Process Procedure()
ReturnedStr       string(255)
ReturnedNum       long
ReturnedStrLen    long
myEmail           string (80)
myDesc            string (80)
DataPos           long
endPos            long
DebugBase64Len    long
FullLineReceived  byte
tempStore         long
loc:CheckPos      long
loc:LastCRPos     long
loc:InActiveSeconds long
LastByte          string(1)
SecondLastByte    string(1)
_Pad11            string(1)
_Pad12            string(1)
  code
  case (self.packet.packetType)
  ! ------------------------------
  of NET:SimpleIdleConnection
    self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', 'Received Idle Warning. Will abort connection.')
    returnedNum = 0
    loc:InActiveSeconds = self._Connection.InActiveTimeout/100
    returnedStr = 'Idle Timeout - Warning connection has been idle for more than ' & |
                  loc:InActiveSeconds & ' seconds. Connection will be closed'
    self.Error = ERROR:IdleTimeOut
    self.WinSockError = 0
    self.SSLError = 0
    do RaiseErrorAndTryNextMessage

  of NET:SimpleAsyncOpenSuccessful
    self._ProcessOpen()

  of NET:SimplePartialDataPacket
    self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', 'Received Data from the remote ' & self._serverDesc & ' Server')

    FullLineReceived = false
    if self.packet.binDataLen >= 2
      if self.packet.binData[(self.packet.binDataLen-1) : self.packet.binDataLen] = '<13,10>'
        FullLineReceived = true
      end
    end

    if FullLineReceived = false
        self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', |
                  'Warning. We Received Data from the remote ' & self._serverDesc |
                & ' Server - but did not receive a whole line. Will store the line in the buffer', NET:LogError)
      if self._ServerReplyBufferLen = 0
        self._ServerReplyBuffer = self.packet.binData [1 : self.packet.binDataLen]
        self._ServerReplyBufferLen = self.packet.binDataLen
      else
        self._ServerReplyBuffer = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen] & self.packet.binData [1 : self.packet.binDataLen]
        self._ServerReplyBufferLen += self.packet.binDataLen
        if self._ServerReplyBufferLen > size (self._ServerReplyBuffer)
          self._ServerReplyBufferLen = size (self._ServerReplyBuffer)
        end
      end
      return ! more data is coming.
    end

    if self._ServerReplyBufferLen > 0
      ! Right join the buffered data and this new data
      if self.packet.binDataLen = 0
        self.packet.binData = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen]
        self.packet.binDataLen = self._ServerReplyBufferLen
      else
        self.packet.binData = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen] & self.packet.binData[1 : self.packet.binDataLen]
        self.packet.binDataLen += self._ServerReplyBufferLen
        if self.packet.binDataLen > size (self.packet.binData)
          self.packet.binDataLen = size (self.packet.binData)
        end
      end
      self._ServerReplyBufferLen = 0
      self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', |
                'Okay we put the buffered lines together and got [' & self.packet.binData [ 1 : self.packet.BinDataLen] & ']')
    end

    if FullLineReceived = true and self.packet.binDataLen > 0
      if self.packet.binData [4] = '-'  ! Okay we got a line like '220-XXX' - needs another line line '220 XXX'
        loc:LastCRPos = 0
        loc:CheckPos = 4
        loop
          if loc:CheckPos > self.packet.binDataLen
            FullLineReceived = false
            break
          end
          if self.packet.binData [loc:CheckPos] <> '-'
            ! Cool we have the last line
            FullLineReceived = true
            break
          end
          loc:LastCRPos = InString ('<13,10>', self.packet.binData [1 : self.packet.binDataLen], 1, loc:LastCRPos + 1)
          if loc:LastCRPos = 0
            FullLineReceived = false
            break
          else
            loc:CheckPos = loc:LastCRPos + 5
          end
        end
        if FullLineReceived = false
          self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', |
                    'Warning. We Received **MultiLine** Data from the remote ' & self._serverDesc & ' Server - but did not receive a whole set of lines. Will store the data in the buffer', NET:LogError)
          self._ServerReplyBuffer = self.packet.binData [1 : self.packet.binDataLen]
          self._ServerReplyBufferLen = self.packet.binDataLen
          return ! more data is coming.
        end
      end
    end

    if self.Error
      self._ErrorStatus = NET:ERROR_STATUS_SEND_ERROR
      self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
      self.ErrorTrap ('Error reading data from the remote ' & self._serverDesc & ' Server', 'NetEmailSend.Process')
      if self._mx
        self.abort()
        self._mxOpenRequest()
        return
      end
      if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
        Do DoNextEmail
      end
      return
    end

    Do SplitReturned         ! Spilt the returned string into a return code and return text

    if self._state = NET:ES_CONNECTEDANDIDLE
      if records (self.DataQueue) > 0
        self._state = NET:ES_READY
      else
        self._state = NET:ES_CONNECTEDANDIDLE  ! nothing to do, but we are connected.
        return
      end
    end


    Case self._state
      !--------------------
      of NET:ES_NOTCONNECTED
      !--------------------
         if ReturnedNum <> 221  ! New 21/11/2003 (Clarion 6 somtimes gets a 221 (closing down message) after everything has closed down - don't error here, it's fine.
           do RaiseErrorAndAbortAllSends       ! We received a message from the mail server, but weren't expeciting it?
         end
      !--------------------
      of NET:ES_CONNECTING
      !--------------------
        if ReturnedNum <> 220
          Do RaiseErrorAndAbortAllSends
        else
          ! Start doing next state
          if self.SecureEmailStartTLS = 1
            self._state = NET:ES_TLS1
            self.packet.bindata = 'EHLO ' & clip (self.helo) & '<13,10>'
            Do SendIt
          elsif (self.AuthUser <> '') or (self.AuthPassword <> '')
            ! AUTH LOGIN
            self._state = NET:ES_LOGIN1
            self.packet.bindata = 'EHLO ' & clip (self.helo) & '<13,10>'
            Do SendIt
          else
            self._state = NET:ES_READY
            self.packet.bindata = 'HELO ' & clip (self.helo) & '<13,10>'
            Do SendIt
          end
        end

      !--------------------
      of NET:ES_CONNECTEDANDIDLE
      !--------------------
         self._ErrorStatus = NET:ERROR_STATUS_SEND_ERROR
         self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
         self.ErrorTrap ('Invalid State. Should never be in the NET:ES_CONNECTEDANDIDLE at this point.', 'NetEmailSend.Process')
         if self._mx
           self.abort()
           self._mxOpenRequest()
           return
         end
         if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
           Do DoNextEmail
         end
         return

      !--------------------
      of NET:ES_TLS1
      !--------------------
        if ReturnedNum <> 250
          Do RaiseErrorAndAbortAllSends
        else
          ! Start doing next state
          self._state = NET:ES_TLS2
          self.packet.bindata = 'STARTTLS<13,10>'
          Do SendIt
        end

      !--------------------
      of NET:ES_TLS2
      !--------------------
        if ReturnedNum <> 220
          Do RaiseErrorAndAbortAllSends
        else
          ! Start doing next state
          self._State = NET:ES_TLS3
          self._SSLSwitchToSSL()    ! Will send us back a AsyncOpenSuccessful packet type - see in ._ProcessOpen()
        end


      !--------------------
      of NET:ES_LOGIN1
      !--------------------
        if ReturnedNum <> 250
          Do RaiseErrorAndAbortAllSends
        else
          ! Start doing next state
          self._state = NET:ES_LOGIN2
          self.packet.bindata = 'AUTH LOGIN<13,10>'
          Do SendIt
        end

      !--------------------
      of NET:ES_LOGIN2
      !--------------------
        if ReturnedNum <> 334
          Do RaiseErrorAndAbortAllSends
        else
          ! Start doing next state
          self._state = NET:ES_LOGIN3
          DebugBase64Len = len (clip (self.AuthUser))
          self.packet.bindata = clip (NetBase64Encode (clip(self.AuthUser), DebugBase64Len)) & '<13,10>'
          Do SendIt
        end


      !--------------------
      of NET:ES_LOGIN3
      !--------------------
        if ReturnedNum <> 334
          Do RaiseErrorAndAbortAllSends
        else
          ! Start doing next state
          self._state = NET:ES_READY
          DebugBase64Len = len (clip (self.AuthPassword))
          self.packet.bindata = clip (NetBase64Encode (clip(self.AuthPassword), DebugBase64Len)) & '<13,10>'
          Do SendIt
        end

      !--------------------
      of NET:ES_READY
      !--------------------
        if ((self.AuthUser <> '') or (self.AuthPassword <> '')) and |
           ((ReturnedNum <> 235) and (ReturnedNum <> 250))  ! Login Successful ! Added 250 check 24/11/2003
          Do RaiseErrorAndAbortAllSends
        elsif ((self.AuthUser = '') or (self.AuthPassword = '')) and (ReturnedNum <> 250)
          Do RaiseErrorAndAbortAllSends
        else
          if records (self.DataQueue) = 0
            ! Nothing to do
            self._state = NET:ES_CONNECTEDANDIDLE  ! nothing to do, but we are connected.
            return
          else
            ! Start doing next state
            self._state = NET:ES_MAILFROM
            self._ListIndex2 = 0
            self._ListWhich2 = 0
            if self.CalcEmailName (self.DataQueue.From, myDesc, myEmail, 1)
              self.packet.bindata = 'MAIL FROM: <<' & clip (myEmail) & '><13,10>'
              Do SendIt
            else
              !self._AfterErrorTryNextMessage = true  ! Indicate that the message must not be removed from the queue after the ErrorTrap() method call. We do it ourselves
              self._ErrorStatus = NET:ERROR_STATUS_SEND_ERROR
              self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
              self.ErrorTrap ('Invalid from address found in data queue', 'NetEmailSend.Process')
              if self._mx
                self.abort()
                self._mxOpenRequest()
                return
              end
              if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
                Do DoNextEmail
              end
            end
          end
        end

      !--------------------
      of NET:ES_MAILFROM orof NET:ES_RCPTTO
      !--------------------
        if ReturnedNum <> 250 and ReturnedNum <> 251   ! 251 returned if address will be forwarded.
          Do RaiseErrorAndTryNextMessage
        else
          if self._ListWhich2 = 0  ! To Addresses
            self._ListIndex2 += 1
            if self.CalcEmailName (self.DataQueue.ToList, myDesc, myEmail, self._ListIndex2)
              self._state = NET:ES_RCPTTO
              self.packet.bindata = 'RCPT TO: <<' & clip (myEmail) & '><13,10>'
              Do SendIt
            else
              self._ListIndex2 = 0
              self._ListWhich2 = 1
            end
          end
          if self._ListWhich2 = 1  ! CC Addresses
            self._ListIndex2 += 1
            if self.CalcEmailName (self.DataQueue.CCList, myDesc, myEmail, self._ListIndex2)
              self._state = NET:ES_RCPTTO
              self.packet.bindata = 'RCPT TO: <<' & clip (myEmail) & '><13,10>'
              Do SendIt
            else
              self._ListIndex2 = 0
              self._ListWhich2 = 2
            end
          end
          if self._ListWhich2 = 2  ! BCC Addresses
            self._ListIndex2 += 1
            if self.CalcEmailName (self.DataQueue.BCCList, myDesc, myEmail, self._ListIndex2)
              self._state = NET:ES_RCPTTO
              self.packet.bindata = 'RCPT TO: <<' & clip (myEmail) & '><13,10>'
              Do SendIt
            else
              ! Start doing next state
              self._state = NET:ES_DATA
              self.packet.bindata = 'DATA<13,10>'
              Do SendIt
            end
          end
        end

      !--------------------
      of NET:ES_DATA
      !--------------------
        if ReturnedNum <> 354
          Do RaiseErrorAndTryNextMessage
        else
          self._state = NET:ES_DATAEND

          ! Send WholeMessage and Break it up into small bits to send
          DataPos = 0
          LastByte = ''
          SecondLastByte = ''
          loop
            if DataPos + NET:EMAIL_MaxMailData > (self.DataQueue.WholeMessageLen)
              endPos = self.DataQueue.WholeMessageLen
            else
              endPos = DataPos + NET:EMAIL_MaxMailData
            end
            self.packet.bindata = self.DataQueue.WholeMessage [(DataPos + 1) : endPos]
            self.packet.bindatalen = endPos - DataPos                ! Set this ourselves so we don't loose any spaces in SendIt Routine due to clip function
            self._ConvertDots(SecondLastByte, LastByte)   ! Makes Dots transparent 26/7/2002
            compile ('EndCompile', NETTALKLOG=1)
              if self.LoggingOn
                if self.packet.BinDataLen > 0
                  self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', |
                            'Sending Message DataLen = ' & self.packet.BinDataLen)
                            !'Sending Message Data = [' & self.packet.BinData [1 : self.packet.binDataLen] & ']')
                end
              end
            EndCompile
            self.send()

            DataPos = endPos
            if endPos >= self.DataQueue.WholeMessageLen
              break ! out of loop - We are finished
            end
            if self.packet.binDataLen >= 2
              secondlastbyte = self.packet.binData [self.packet.binDataLen - 1]
            else
              if self.packet.binDataLen >= 1
                secondlastbyte = lastbyte
              else
                !keep the same
              end
            end
            if self.packet.binDataLen >= 1
              lastbyte = self.packet.binData [self.packet.binDataLen]
            else
              !keep the same
            end
          end
          self._MessageBytesSent += self.DataQueue.WholeMessageLen

          ! Send End Sequence
          self.packet.bindata = '<13,10>.<13,10>'
          Do SendIt
        end

      !--------------------
      of NET:ES_DATAEND
      !--------------------
        if ReturnedNum <> 250
          Do RaiseErrorAndTryNextMessage
        else
          do SentOkay
        end

    else
      self._ErrorStatus = NET:ERROR_STATUS_SEND_ERROR
      self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
      self.ErrorTrap ('Error. NetEmailSend - no state found. State = ' & self._state, 'NetEmailSend.Process')
      if self._mx
        self.abort()
        self._mxOpenRequest()
        return
      end
      if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
        Do DoNextEmail
      end
    end
  end

!-------------------------------------------------------------------
SentOkay Routine
  compile ('EndCompile', NETTALKLOG=1)
    if self.LoggingOn
      if self._mx = 0
        self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', 'Message sent to ' & clip (self.DataQueue.ToList) & '. Will call MessageSent()')
      else
        self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', 'Message sent to ' & clip (self.DataQueue.BCCList) & '. Will call MessageSent()')
      end
    end
  EndCompile
  self._MessagesSentOnCurrentConnection += 1
  self.MessageSent()              ! Call Virtual Method - with message still in top of queue
  Do DoNextEmail

!-------------------------------------------------------------------
! This routine is always called after either an ErrorTrap() or a MessageSent() call.
! It deletes the top message from the queue and then issues a RSET if there are
! more emails to send. That way it trys to send the remaining emails.
DoNextEmail Routine
  Do DeleteOneMessageFromQueue
  if records (self.DataQueue) > 0
    if self._mx
      self.packet.bindata = 'QUIT' & '<13,10>'
      self.packet.binDataLen = len (clip(self.packet.bindata))
      tempStore = self.DontErrorTrapInSendIfConnectionClosed
      self.DontErrorTrapInSendIfConnectionClosed = 1
      self.Send()
      self.DontErrorTrapInSendIfConnectionClosed = tempStore
      self.close()
      self._mxOpenRequest()

    else
      if (self.ReconnectAfterXMsgs > 0) and (self._MessagesSentOnCurrentConnection >= self.ReconnectAfterXMsgs)
        ! Need to restart connection
        self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', |
                  self._MessagesSentOnCurrentConnection & ' messages have been sent on this connection. Connection will be restarted')
        self.packet.bindata = 'QUIT' & '<13,10>'
        Do SendIt
        self.close()
        Do StartConnection
      else
        self._state = NET:ES_READY           ! Jono 20/3/2001 Was NET:ES_CONNECTEDANDIDLE
        self.packet.bindata = 'RSET<13,10>'  ! Send RSET to get the next packet out.
        Do SendIt
      end
    end
  else
    ! No more emails to send
    if (self.OptionsKeepConnectionOpen = false) or ((self.ReconnectAfterXMsgs > 0) and (self._MessagesSentOnCurrentConnection >= self.ReconnectAfterXMsgs))
      ! Need to restart connection
      if self.LoggingOn and ((self.ReconnectAfterXMsgs > 0) and (self._MessagesSentOnCurrentConnection >= self.ReconnectAfterXMsgs))
        self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', |
                  self._MessagesSentOnCurrentConnection & ' messages have been sent on this connection. (2) Connection will be restarted')
      else
        self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', |
                  'This object has been configured to close when there are no more messages to be sent.')
      end
      ! Close the connection - the object has already had either ErrorTrap() or MessageSent() called
      ! so we don't need to notify the object at all.
      self.packet.bindata = 'QUIT' & '<13,10>'
      Do SendIt
      self.close()
      self._state = NET:ES_NOTCONNECTED
    else
      self._state = NET:ES_READY
      self.packet.bindata = 'RSET<13,10>'  ! Send RSET
      Do SendIt
    end
    if self.progresscontrol                          ! Update the Progress Control Control if there is one
      self.progresscontrol{prop:hide} = 1
      self.progresscontrol{prop:progress} = 0
      display(self.progressControl)
    end
  end

!-------------------------------------------------------------------

SendIt Routine
  self.packet.bindatalen = len (clip (self.packet.bindata))
  if self.packet.binDataLen > 0
    self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', 'Sending --> [' & self.packet.binData [1 : self.packet.bindatalen] & ']')
  end
  self.send()

!-------------------------------------------------------------------

SplitReturned Routine
  ReturnedNum = 0
  ReturnedStr = ''
  if self.packet.binDataLen => 3
    ReturnedNum = self.packet.binData [1 : 3]
    if self.packet.binDataLen > 3
      if self.packet.binDataLen >= (3 + len (ReturnedStr))
        ReturnedStrLen = (3 + len (ReturnedStr))
      else
        ReturnedStrLen = self.packet.binDataLen
      end
      ReturnedStr = self.packet.binData [4 : ReturnedStrLen]  ! Jono reckons this may need to be 5! - 11/10/2000
    end
  end
  self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', '<<-- Data : ' & ReturnedNum & ' [' & clip (ReturnedStr) & ']')


!-------------------------------------------------------------------

RaiseErrorAndAbortAllSends Routine
  self.ServerErrorNum = ReturnedNum
  self.ServerErrorDesc = ReturnedStr
  self._ErrorStatus = NET:ESERVER
  self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_DELETE_ALL
  self.ErrorTrap ('Fatal Error. The remote ' & self._serverDesc & ' server sent error = ' & ReturnedNum & ' ' & clip (left (ReturnedStr)) & ' State = ' & self._state & '. Will abort all messages in the DataQueue', 'NetEmailSend.Process')
  self.ServerErrorNum = 0
  self.ServerErrorDesc = ''
  if self._mx
    self.abort()
    self._mxOpenRequest()
    return
  end
  if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_DELETE_ALL
    self._DeleteAllMessagesInDataQueue()
    self.Abort()
  end

!-------------------------------------------------------------------
RaiseErrorAndTryNextMessage Routine
  self.ServerErrorNum = ReturnedNum
  self.ServerErrorDesc = ReturnedStr
  self._ErrorStatus = NET:ERROR_STATUS_SEND_ERROR
  self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
  self.ErrorTrap ('Error in this Message. The remote ' & self._serverDesc & ' server sent error = ' & ReturnedNum & ' ' & clip (left (ReturnedStr)) & ' State = ' & self._state & '. Will try and send next email', 'NetEmailSend.Process')
  self.ServerErrorNum = 0
  self.ServerErrorDesc = ''
  if self._mx
    self.abort()
    self._mxOpenRequest()
    return
  end

  if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
    Do DeleteOneMessageFromQueue
    if records (self.DataQueue) > 0
      self.abort()  ! Abort connection
      Do StartConnection
    else
      self.abort()
    end
  end

! ------------------------------------------------------------------
StartConnection Routine
  self._TryOpen()
  ! Now wait for Async/Sync Open and Response from Server

!-------------------------------------------------------------------
DeleteOneMessageFromQueue Routine
  if records (self.DataQueue) = 0
    clear (self.DataQueue)
    exit
  end
  if size (self.DataQueue.MessageText) > 0
    dispose (self.DataQueue.MessageText)
  end
  if size (self.DataQueue.MessageHtml) > 0
    dispose (self.DataQueue.MessageHtml)
  end
  if size (self.DataQueue.WholeMessage) > 0
    dispose (self.DataQueue.WholeMessage)
  end
  delete (self.DataQueue)
  clear (self.DataQueue)
  if records (self.DataQueue) > 0
    get (self.DataQueue, 1)   ! Load top of queue if it exists
  end


!-----------------------------------------------------------
! This is a virtual method handled by derivative classes.
! This method will be called when an error occurs
!
! There are two properties that are useful when your virtual ErrorTrap is called
! self._AfterErrorAction - which tells this object how to handle the error. Don't change this.
! self._ErrorStatus - which tells you what kind of error occurred.
!
!--------------------------------------------------------------
! Table 2.1 SMTP Reply Codes
! The first digit of the SMTP reply code basically tells whether the response is positive or negative.
!
! Code  Text of Response
! 211  system status, or system help reply
! 214  help message
! 220  <domain> service ready
! 221  <domain> service closing transmission channel
! 250  request mail action okay, completed
! 251  user not local, will forward to <forward-path >
! 354  start mail input; and with <CRLF>.<CRLF>
! 421  <domain> servers not available, closing transmission channel
! 450  requested mail action not taken: mailbox unavailable
! 451  requested action aborted: local error in processing
! 452  requested action not taken: insufficient system storage
! 500  syntax error, command unrecognized
! 501  syntax error in parameters or arguments
! 502  command not implemented
! 503  bad sequence of commands
! 504  command parameter not implemented
! 550  requested action not taken: mailbox unavailable
! 551  user not local; please try <forward-path>
! 552  requested mail action aborted: exceeded storage allocation
! 553  requested action not taken: mailbox name not allowed
! 554  transaction failed
NetEmailSend.ErrorTrap Procedure (string errorStr,string functionName)
ourErrorStr        String (512)
  code
  case self.error
  of 10061                                                  ! Overwrite this message with a more user friendly version
    ourErrorStr = 'Could not open a connection to the remote ' & self._serverDesc & ' Server'
  else
    ourErrorStr = clip (errorStr)
  end

  parent.errortrap (ourErrorStr, functionName)             ! Call parent ErrorTrap method.

  case self._AfterErrorAction
  of NET:ADELETE
    if self._mx
      self.Log ('NetEmailSend.ErrorTrap' & ' (' & self._serverDesc & ')', 'This error was a server error, but because this is mx we will attempt to continue.', NET:LogError)
      if self.OpenFlag or self.AsyncOpenBusy
        self.abort()
      end
      self._mxOpenRequest()
      return
    else
      self.Log ('NetEmailSend.ErrorTrap' & ' (' & self._serverDesc & ')', 'This error was so bad we need to delete all records from DataQueue and abort the connection', NET:LogError)
      self._DeleteAllMessagesInDataQueue()                   ! Delete all messages in DataQueue
      if self.OpenFlag or self.AsyncOpenBusy
        self.abort()
      end
      self._State = NET:ES_NOTCONNECTED
    end
  of NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
    self.Log ('NetEmailSend.ErrorTrap' & ' (' & self._serverDesc & ')', 'Attempting to send next message', NET:LogError)
  of NET:ANONE
    self.Log ('NetEmailSend.ErrorTrap' & ' (' & self._serverDesc & ')', 'The email was not added to the DataQueue, so no further actions are necessary.', NET:LogError)
  of NET:ERROR_ACTION_AFTER_ERRORTRAP_DELETE_ALL
    self.Log ('NetEmailSend.ErrorTrap' & ' (' & self._serverDesc & ')', 'This error was so bad we need to delete all records from DataQueue and abort the connection, but this will be done after ErrorTrap.', NET:LogError)
  else
    self.Log ('NetEmailSend.ErrorTrap' & ' (' & self._serverDesc & ')', 'ERROR. Invalid Error Status in NetEmailSend.ErrorTrap..', NET:LogError)
  end

  do UpdateProgressControl

  self._AfterErrorSavedAction = self._AfterErrorAction  ! Store this

  self._ErrorStatus = 0        ! Reset
  self._AfterErrorAction = 0   ! Reset

UpdateProgressControl Routine
  if self._mx = 0
    if self.progresscontrol                          ! Update the Progress Control Control if there is one
      if records (self.DataQueue) <= 1
        self.progresscontrol{prop:hide} = 1
        self.progresscontrol{prop:progress} = 0
        display()
      end
    end
  end


!-----------------------------------------------------------
! Called when the connection is closed
NetEmailSend.ConnectionClosed Procedure()
  code
  self.Log ('NetEmailSend.ConnectionClosed' & ' (' & self._serverDesc & ')', |
     'The remote ' & self._serverDesc & ' server closed the connection or a dial-up connection closed.')
  self._MessagesSentOnCurrentConnection = 0
  if self._state = NET:ES_NOTCONNECTED
    ! No worries
  else
    self._ErrorStatus = NET:ESERVER
    self._AfterErrorAction = NET:ADELETE
    self.ErrorTrap ('The remote ' & self._serverDesc & |
                    ' server closed the connection or a dial-up connection closed', |
                    'NetEmailSend.ConnectionClosed')
    self._state = NET:ES_NOTCONNECTED
  end


!-----------------------------------------------------------
! This is a virtual method handled by derivative classes.
! This method everytime a message is sent successfully
!-----------------------------------------------------------
NetEmailSend.MessageSent Procedure()
  code


!-----------------------------------------------------------
NetEmailSend.CalcEmailName Procedure (string myList,*string myDesc,*string myEmail,long myIndex)
local      group, pre (loc)
x            long
startpos     long
tempA        long
tempB        long
endpos       long
add1pos      long
add2pos      long
len          long
len2         long
InQuotes     long
ourIndexCount long
           end
  code
  loc:startpos = 1
  myEmail = ''
  myDesc = ''

  loc:len2 = len (clip(myList))

  if myIndex <= 0
    return false ! Nothing to do
  end

  if myIndex = 1
    if loc:len2 = 0
      return false                                          ! Nothing to do
    end
    loc:startpos = 1
  else
    loc:startpos = 0
  end
  loop loc:x = 1 to loc:len2
    if myList[loc:x] = '"'
      if loc:InQuotes
        loc:InQuotes = false
        cycle
      else
        loc:InQuotes = true
        cycle
      end
    end
    if loc:InQuotes
      cycle
    end
    case myList[loc:x]
    of   ','
    orof ';'
      loc:ourIndexCount += 1
      if loc:ourIndexCount = (myIndex - 1)                  ! We've got the start position
        loc:startpos = loc:x + 1
      end
      if loc:ourIndexCount = (myIndex)                      ! We've got the end position
        loc:endpos = loc:x - 1
        break
      end
    end
  end

  if loc:startpos = 0
    return false ! didn't find enough , or ;
  end
  if loc:startpos > loc:len2
    return false
  end
  if loc:endPos = 0
    loc:endpos = loc:len2
  end
  if loc:endpos > loc:len2
    loc:endpos = loc:len2
  end
  if loc:endpos < loc:startpos
    loc:endpos = loc:startpos
  end

  loc:add1pos = Instring ('<<', myList, 1, loc:startpos)  ! Search for '<' sign
  if (loc:add1pos = 0) or (loc:add1pos > loc:endpos)
    Do ReturnSame                                 ! There is no <
  end

  loc:add2pos = Instring ('>', myList, 1, loc:add1pos)
  if (loc:add2pos = 0) or (loc:add2pos > loc:endpos)
    Do ReturnSame
  end

  if (loc:add1pos+1) <= (loc:add2pos-1)
    myEmail = clip (left (myList [(loc:add1pos + 1) : (loc:add2pos - 1)]))
  else
    myEmail = ''
  end

  if loc:startpos <= (loc:add1pos-1)
    myDesc = clip (left (myList [loc:startpos : (loc:add1pos - 1)]))
  else
    myDesc = ''
  end

  ! Strip " from Descriptions
  loc:len = len (clip (myDesc))
  loc:startPos = instring ('"', myDesc,1,1)
  loop while loc:startPos > 0
    if (loc:startPos = 1) and (loc:len = 1)
      MyDesc = ''
    elsif (loc:startpos = 1)
      if 2 <= loc:len
        MyDesc = MyDesc [2 : loc:len]
      else
        MyDesc = ''
      end
    elsif (loc:startPos+2) > loc:len
      if 1 <= (loc:StartPos-1)
        MyDesc = MyDesc [1 : (loc:startPos-1)]
      else
        MyDesc = ''
      end
    else
      if (1 <= (loc:startPos-1)) and ((loc:startPos+2)<=loc:len)
        MyDesc = MyDesc [1 : (loc:startPos-1)] & MyDesc [(loc:startPos+2) : loc:len]
      else
        MyDesc = ''
      end
    end
    loc:len -= 1
    loc:startPos = instring ('"', myDesc,1,1)
  end

  return true

ReturnSame Routine ! Make the Email Address and the description the same
  if loc:startpos <= (loc:endpos-1)
    myEmail = clip (left (myList [loc:startpos : (loc:endpos)]))
  else
    myEmail = ''
  end
  myDesc = clip (left (myEmail))
  if myEmail = ''
    ! no data
    return false
  end
  return true


!-----------------------------------------------------------
NetEmailSend.SetRequiredMessageSize Procedure (long p_WholeSize,long p_TextSize,long p_HtmlSize)
currentSize         long
NewText             byte
NewHTML             byte
NewWhole            byte
pn                  equate('NetEmailSend.SetRequiredMessageSize')
  code
  self.Log ('NetEmailSend.SetRequiredMessageSize' & ' (' & self._serverDesc & ')', 'Setting Sizes:' & |
            ' WholeSize = ' & p_WholeSize & |
            ' TextSize = ' & p_TextSize & |
            ' HtmlSize = ' & p_HtmlSize)
  self.Error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if (p_WholeSize < 0) or (p_TextSize < 0) or (p_HtmlSize < 0)
    self.PostError(ERROR:InvalidStringSize, NET:EINVALIDDATA, NET:ANONE, 'Invalid String Size (less than zero) passed to function', pn)
    return (self.error)
  end

  self.MessageTextLen = p_TextSize
  self.MessageHTMLLen = p_HTMLSize
  self.wholeMessageLen = p_WholeSize

  currentSize = (size (self.MessageText))
  if p_TextSize <= currentSize      ! Sufficient memory allocated (max 64K extra)
    if (currentSize - p_TextSize) > NET:EmailNewDifference
      NewText = True
    end
  else
    NewText = True
  end

  if NewText
    if currentSize > 0
      dispose (self.MessageText)
    end
    self.MessageText &= new (String (p_TextSize))
    if size (self.MessageText) < p_TextSize
      self.PostError(ERROR:CouldNotAllocMemory, NET:EINVALIDDATA, NET:ANONE, 'Unable to allocate memory required for sending ' & self._serverDesc & '.', pn)
    end
  end


  currentSize = (size (self.MessageHtml))
  if p_HtmlSize <= currentSize          ! Sufficient memory allocated (check that there is not more than 64K extra)
    if (currentSize - p_HtmlSize) > NET:EmailNewDifference
      NewHtml = True
    end
  else
    NewHtml = True
  end

  if NewHtml
    if currentSize > 0
      dispose (self.MessageHtml)
    end
    self.MessageHtml &= new (String (p_HtmlSize))
    if size (self.MessageHtml) < p_HtmlSize
      self.PostError(ERROR:CouldNotAllocMemory, NET:EINVALIDDATA, NET:ANONE, 'Unable to allocate memory required for sending ' & self._serverDesc & '.', pn)
    end
  end


  currentSize = (size (self.wholeMessage))
  if p_WholeSize <= currentSize
    ! Okay there is enough memory allocated.
    ! Just check there is not more than 64K extra memory allocated.
    if (currentSize - p_WholeSize) > NET:EmailNewDifference
      NewWhole = True
    end
  else
    NewWhole = True
  end

  if NewWhole
    if currentSize > 0
      dispose (self.wholeMessage)
    end
    self.wholeMessage &= new (String (p_WholeSize))
    if size (self.wholeMessage) < p_WholeSize
      self.PostError(ERROR:CouldNotAllocMemory, NET:EINVALIDDATA, NET:ANONE, 'Unable to allocate memory required for sending ' & self._serverDesc & '.', pn)
    end
  end


!-----------------------------------------------------------
NetEmailSend.Free Procedure ()
  CODE
  self.Log ('NetEmailSend.Free' & ' (' & self._serverDesc & ')', 'Freeing memory used in sending ' & self._serverDesc & '.')
  if not self.MessageText &= NULL
    dispose (self.MessageText)
  end
  if not self.MessageHtml &= NULL
    dispose (self.MessageHtml)
  end
  if not self.WholeMessage &= NULL
    dispose (self.WholeMessage)
  end



!-----------------------------------------------------------
NetEmailSend.Kill    Procedure ()
x     long
  code
    self.Log ('NetEmailSend.Kill' & ' (' & self._serverDesc & ')', 'Kill called')

    parent.kill()

    self._DeleteAllMessagesInDataQueue                       ! Delete the contents of DataQueue

    if not self._DataQueue &= NULL
        free (self._DataQueue)
        dispose (self._DataQueue)                              ! Free the allocated Memory of DataQueue
    end

    self.free()
    self.Log ('NetEmailSend.Kill' & ' (' & self._serverDesc & ')', 'Kill complete')


!-----------------------------------------------------------
NetEmailSend.Abort   Procedure
  code
    self.Log ('NetEmailSend.Abort' & ' (' & self._serverDesc & ')', 'Aborting connection to ' & self._serverDesc & ' Server')
    parent.abort()
    self._MessagesSentOnCurrentConnection = 0
    self._State = NET:ES_NOTCONNECTED


!-----------------------------------------------------------
NetEmailSend.CalcProgress Procedure ()
local       group, pre (loc)
x             long
BytesToSend   long
tempPointer   long
temp100a      long
temp100b      long
            end
  CODE
  if records (self.DataQueue) = 0
    self.Progress1000 = 1000
    self.ProgressBytesToGo = 0
    self.ProgressBytesMax = 0
  else
    loc:BytesToSend = 0
    loc:tempPointer = pointer (self.DataQueue)
    loop loc:x = 1 to records (self.DataQueue)
      get (self.DataQueue,loc:x)
      loc:BytesToSend += self.DataQueue.WholeMessageLen
    end

    if loc:BytesToSend > self.ProgressBytesMax
      self.ProgressBytesMax = loc:BytesToSend
    end

    if records (self.DataQueue)
      if (self._state = NET:ES_DATAEND) or (self._state = NET:NS_POSTEND)
        get (self.DataQueue,1)
        loc:BytesToSend -= self.DataQueue.WholeMessageLen
        self.GetInfo(2)  ! GetExtended Info
        loc:BytesToSend += self.OutgoingDLLPacketsBytes
      end
      get (self.DataQueue, loc:tempPointer)
      if errorcode()
      end
    end

    self.ProgressBytesToGo = loc:BytesToSend
    if self.ProgressBytesMax > 0
      self.Progress1000 = ((self.ProgressBytesMax - self.ProgressBytesToGo) / self.ProgressBytesMax) * 1000
    else
      self.Progress1000 = 1000 ! All done
    end
    if self.progresscontrol                          ! Update the Progress Control
      loc:temp100a = self.progresscontrol{prop:progress} / 10
      loc:temp100b = self.Progress1000 / 10
      if loc:temp100a <> loc:temp100b
        self.progresscontrol{prop:progress} = self.Progress1000
      end
    end
  end


!-----------------------------------------------------------
NetEmailSend.AfterBuildFromWhole Procedure
  CODE


!-----------------------------------------------------------
NetEmailSend.AfterBuildFromParts Procedure
  CODE


!-----------------------------------------------------------
! This method goes through the DataQueue and frees all the allocated variables and then
! frees (deletes all) records in the DataQueue.
NetEmailSend._DeleteAllMessagesInDataQueue Procedure ()
x     long
  CODE
  if records (self.DataQueue) > 0
    self.Log ('NetEmailSend._DeleteAllMessagesInDataQueue' & ' (' & self._serverDesc & ')', 'Attention: All messages in the queue will be deleted now.')
  end

  ! Loop through queue and free memory
  loop x = 1 to records (self.DataQueue)
    get (self.DataQueue, x)
    if ~self.DataQueue.MessageText&=NULL
      Dispose (self.DataQueue.MessageText)
    end
    if ~self.DataQueue.MessageHTML&=NULL
      Dispose (self.DataQueue.MessageHtml)
    end
    if ~self.DataQueue.WholeMessage&=NULL
      Dispose (self.DataQueue.WholeMessage)
    end
  end
  free (self.DataQueue)


!-----------------------------------------------------------
! Finds a header field in the self.wholeMessage string
NetEmailSend._GetHeaderField Procedure (string p_Header,long p_count)
local      group, pre (loc)
StartPos1    long
HeaderEndPos long
endPos       long
Pos3         long
Len          long
ret          string (NET:StdEmailListSize)
           end

  CODE
  loc:ret = ''
  if self.wholeMessageLen > 0
    loc:HeaderEndPos = Instring ('<13,10><13,10>', self.wholeMessage[1 : self.wholeMessageLen],1,1)
    if (loc:HeaderEndPos = 0) and self.wholeMessageLen > 2
      loc:HeaderEndPos = self.wholeMessageLen - 2 ! Then scan through the whole email.
    end
    if loc:HeaderEndPos
      loc:HeaderEndPos += 2  ! Compensate for <13,10>
      loop p_count times
        loc:StartPos1 = InString (upper(clip(p_header)), upper (self.wholeMessage [1 : loc:HeaderEndPos]),1,(loc:Startpos1 + 1))
        if loc:StartPos1 = 0
          break
        end
      end
      if (loc:StartPos1 > 0) and (loc:StartPos1 <= self.wholeMessageLen)
        loc:Len = len (clip(p_Header))
        loc:StartPos1 += loc:Len
        loc:endPos = loc:StartPos1
        loop
          loc:endPos = InString ('<13,10>',self.wholeMessage,1,loc:endPos)
          if loc:endPos = 0
            return (clip (loc:ret))  ! return nothing
          end
          if loc:endPos + 2 > self.wholeMessageLen
            !ReturnAnswer Routine
            loc:ret = clip(left(sub (self.wholeMessage, loc:StartPos1, (loc:endPos-loc:StartPos1))))
            loc:Pos3 = 1
            loop
              loc:Pos3 = Instring ('<13,10>',loc:ret,1,loc:Pos3)
              if loc:Pos3 = 0
                break
              end
              loc:ret = clip(left(sub (loc:ret, 1, loc:Pos3-1))) & clip(left(sub (loc:ret, loc:Pos3+2, len (clip (loc:ret)))))
            end
            return (loc:ret)
          end

          ! Handle code for messages like this (note To line is brocken over two lines!):
          !To: "hbci@example.com" <hbci@example.com>,
          !    "jono@example.com"  <jono@example.com>
          if (self.wholeMessage[loc:endPos+2] <> ' ') and (self.wholeMessage[loc:endPos+2] <> '<9>')
            ! Okay that's it
            !ReturnAnswer Routine
            loc:ret = clip(left(sub (self.wholeMessage, loc:StartPos1, (loc:endPos-loc:StartPos1))))
            loc:Pos3 = 1
            loop
              loc:Pos3 = Instring ('<13,10>',loc:ret,1,loc:Pos3)
              if loc:Pos3 = 0
                break
              end
              loc:ret = clip(left(sub (loc:ret, 1, loc:Pos3-1))) & ' ' & clip(left(sub (loc:ret, loc:Pos3+2, len (clip (loc:ret)))))
            end
            return (loc:ret)
          end
          loc:endPos += 1 ! Otherwise keep searching
        end
      end
    end
  end
  return (clip (loc:ret))


!-----------------------------------------------------------
NetEmailSend._ConvertList Procedure (*string p_List)
local      group, pre (loc)
attPos1      long
attPath      string (NET:StdEmailPathSize)
attGotPath   long
attFileCount long
           end
  CODE
  loc:attPos1 = 0
  loc:attPath = ''
  loc:attGotPath = false
  loc:attFileCount = 0
  loop
    loc:attPos1 = InString ('|', p_List, 1, loc:attPos1 + 1)
    if loc:attPos1 > 0
      if loc:attGotPath = false
        if loc:attPos1 > 1
          loc:attPath = p_List [1 : (loc:attPos1 - 1)]
          if loc:attPath [(loc:attPos1 - 1)] <> '\'
            loc:attPath [loc:attPos1] = '\'
          end
        end
        loc:attGotPath = true
        p_List = '"' & clip (loc:attPath) & p_List[loc:attPos1 + 1 : len (clip (p_List))]
        loc:AttFileCount += 1
      else                                                  ! Insert ", " between files
        p_List = p_List [1 : (loc:attPos1-1)] & '", "' & |
                 clip (loc:attPath) & p_List[loc:attPos1 + 1 : len (clip (p_List))]
        loc:AttFileCount += 1
      end
    else
      break
    end
  end
  if loc:AttFileCount > 0
    ! Insert last "
    p_List = clip (p_List) & '"'
  end

!-----------------------------------------------------------
NetEmailSend._ConvertDots Procedure (string p_secondlastchar, string p_lastchar)
local        group, pre (loc)
x              long
             end
  CODE
  loc:x = 0
  if self.packet.binDataLen <= 0
    return
  end

  ! Start Jono Change 16 Feb 2006
  ! Start by converting . to ..
  if p_secondlastchar = '<13>' and self.packet.binData >= 1
    if p_lastchar = '<10>'
      if self.packet.binData[1] = '.'
        if self.packet.binDataLen < NET:MAXBINDATA
          self.packet.binData = '.' & self.packet.BinData [1 : self.packet.binDataLen] ! Transparency of lines beginning in .
          ! This above line will change something like '.Hello' to '..Hello'
          self.packet.binDataLen += 1
        else
          if self.packet.binDataLen >= 3
            self.packet.binData = '..' & self.packet.BinData [3 : self.packet.binDataLen]
            ! This above line will change something like '.Hello' to '..ello'
            ! Notice the character after the . is overwritten as the packet size is full
            ! Packet size remains the same
          end
          if self.packet.BinDataLen > 0
            self.Log ('NetEmailSend._ConvertDots' & ' (' & self._serverDesc & ')', |
                      'Error. Buffer full. Lost character after dot (to prevent GPF)')
          end
        end
      end
    end
  end

  if p_lastchar = '<13>' and self.packet.binData >= 2
    if self.packet.binData[1:2] = '<10>.'
      if self.packet.binData[1:2] = '<10>.'
        if self.packet.binDataLen < NET:MAXBINDATA
          self.packet.binData = '<10>.' & self.packet.BinData [2 : self.packet.binDataLen] ! Transparency of lines beginning in .
          ! This above line will change something like '.Hello' to '..Hello'
          self.packet.binDataLen += 1
        else
          if self.packet.binDataLen >= 3
            self.packet.binData = '..' & self.packet.BinData [3 : self.packet.binDataLen]
            ! This above line will change something like '.Hello' to '..ello'
            ! Notice the character after the . is overwritten as the packet size is full
            ! Packet size remains the same
          end
          if self.packet.BinDataLen > 0
            self.Log ('NetEmailSend._ConvertDots' & ' (' & self._serverDesc & ')', |
                      'Error. Buffer full. Lost character after dot (to prevent GPF)')
          end
        end
      end
    end
  end
  ! End Jono Change 16 Feb 2006

  loop
    loc:x = InString ('<13,10>.', self.packet.BinData[1 : self.packet.BinDataLen],1,(loc:x+1))
    if (loc:x = 0) or (loc:x > self.packet.binDataLen) or (loc:x > NET:MAXBINDATA)
      return
    end
    if self.packet.binDataLen < NET:MAXBinData
      self.packet.binData[(loc:x+2) : (self.packet.binDataLen+1)] = '.' & self.packet.binData[(loc:x+2) : (self.packet.binDataLen)]
      ! This above line will change something like '.Hello' to '..Hello'
      self.packet.binDataLen += 1
    else
      self.packet.binData[(loc:x+2) : (self.packet.binDataLen)] = '..' & self.packet.binData[(loc:x+4) : (self.packet.binDataLen)]
        ! This above line will change something like '.Hello' to '..ello'
        ! Notice the character after the . is overwritten as the packet size is full
        ! Packet size remains the same
      if self.packet.BinDataLen > 0
        self.Log ('NetEmailSend._ConvertDots' & ' (' & self._serverDesc & ')', |
                  'Error. Buffer full (2). Lost character after dot (to prevent GPF)')
      end
    end
  end



!-----------------------------------------------------------
NetEmailSend._mxOpenRequest Procedure
  CODE
!-----------------------------------------------------------
NetEmailSend._TryOpen Procedure
  CODE
  if self.AsyncOpenBusy
    ! The connection is already being opened (asynchronously), just wait around
    self.Log ('NetEmailSend._TryOpen' & ' (' & self._serverDesc & ')', 'The connection is currently being opened asynchronously, we will just wait for it to work or fail.')
  else
    ! Not connected
    if self._mx
      self._mxOpenRequest()
    else
      self.Open(self.Server, self.Port)
      if self.AsyncOpenUse
        ! Asynchronous Open
        ! Just wait for it to open
      else
        ! Synchronous Open
        if self.OpenFlag = false
          self.error = ERROR:CouldNotConnectToHost
          self.WinSockError = 0
          self.SSLError = 0
          ! ErrorTrap has already been called in self.open, so we don't need to call it here.
          self._State = NET:ES_NOTCONNECTED
          if self.progresscontrol                          ! Update the Progress Control Control if there is one
            self.progresscontrol{prop:hide} = 1
            self.progresscontrol{prop:progress} = 0
            display(self.progressControl)
          end
          return
        end
        case self._objectType
        of NET:POP_CLIENT orof NET:SMTP_CLIENT
          self._State = NET:ES_CONNECTING
        of NET:NNTP_CLIENT
          self._State = NET:NS_CONNECTING
        end
      end
    end
  end

!-----------------------------------------------------------
NetEmailSend._ProcessOpen Procedure
  CODE
  if self._State = NET:ES_TLS3 and self.SSL = 1
    ! SSL Mode
    ! Connection has just switched from normal to SSL
    self.Log ('NetEmailSend._ProcessOpen' & ' (' & self._serverDesc & ')', 'The connection successfully switched to SSL mode (' & self._Connection.Server & ':' & self._Connection.Port & ')')
    if (self.AuthUser <> '') or (self.AuthPassword <> '')
      ! AUTH LOGIN
      self._state = NET:ES_LOGIN1
      self.packet.bindata = 'EHLO ' & clip (self.helo) & '<13,10>'
      Do SendIt
    else
      self._state = NET:ES_READY
      self.packet.bindata = 'HELO ' & clip (self.helo) & '<13,10>'
      Do SendIt
    end
  else
    ! Non-SSL (Normal) Mode
    self.Log ('NetEmailSend._ProcessOpen' & ' (' & self._serverDesc & ')', 'The connection opened to ' & self._Connection.Server & ':' & self._Connection.Port & ' The EmailSend will wait for the Welcome message from the Server.')
    case self._objectType
    of NET:POP_CLIENT orof NET:SMTP_CLIENT
      self._State = NET:ES_CONNECTING
    of NET:NNTP_CLIENT
      self._State = NET:NS_CONNECTING
    end
    ! Now wait for 220 message from the Email Server
  end

!-------------------------------------------------------------------
SendIt Routine
  self.packet.bindatalen = len (clip (self.packet.bindata))
  if self.packet.binDataLen > 0
    self.Log ('NetEmailSend._ProcessOpen' & ' (' & self._serverDesc & ')', 'Sending --> [' & self.packet.binData [1 : self.packet.bindatalen] & ']')
  end
  self.send()

!-----------------------------------------------------------
NetNewsReceive.Process Procedure
local               group, pre(loc)
SplitStr              string (1024)
tempMessageLen        long
newRequiredSize       long
FullLineReceived      byte
                    end
  CODE
  case (self.packet.packetType)
  of NET:SimpleIdleConnection
    self._CloseIdleConnection()
  of NET:SimpleAsyncOpenSuccessful          ! Wait for packet data to arrive
    self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Connection opened asynchronously.')
  of NET:SimplePartialDataPacket
    self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Received data from the ' & self._serverDesc & ' Server')
    self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Data (' & self.packet.binDataLen & ' bytes) = [' & clip (self.packet.binData) & ']')
    if (self._state <> NET:ER_DOWNLOAD) and (self._state <> NET:NR_XOVER) and (self._State <> NET:NR_LIST)
      loc:FullLineReceived = false
      if self.packet.binDataLen >= 2
        if self.packet.binData[(self.packet.binDataLen-1) : self.packet.binDataLen] = '<13,10>'
          loc:FullLineReceived = true
        end
      end

      if loc:FullLineReceived = false
        self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', |
                  'Warning. We Received Data from ' & self._serverDesc & ' Server - but did not receive a whole line. Will store the line in the buffer')
        if self._ServerReplyBufferLen = 0
          self._ServerReplyBuffer = self.packet.binData [1 : self.packet.binDataLen]
          self._ServerReplyBufferLen = self.packet.binDataLen
        else
          self._ServerReplyBuffer = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen] & self.packet.binData [1 : self.packet.binDataLen]
          self._ServerReplyBufferLen += self.packet.binDataLen
          if self._ServerReplyBufferLen > size (self._ServerReplyBuffer)
            self._ServerReplyBufferLen = size (self._ServerReplyBuffer)
          end
        end
        return ! more data is coming.
      end

      if self._ServerReplyBufferLen > 0
        ! Right join the buffered data and this new data
        if self.packet.binDataLen = 0
          self.packet.binData = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen]
          self.packet.binDataLen = self._ServerReplyBufferLen
        else
          self.packet.binData = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen] & self.packet.binData[1 : self.packet.binDataLen]
          self.packet.binDataLen += self._ServerReplyBufferLen
          if self.packet.binDataLen > size (self.packet.binData)
            self.packet.binDataLen = size (self.packet.binData)
          end
        end
        self.Log ('NetEmailSend.Process' & ' (' & self._serverDesc & ')', |
                  'Okay we put the buffered lines together and got [' & self.packet.binData [ 1 : self.packet.BinDataLen] & ']')
      end
    end

    if self.Error <> 0
      self.ErrorTrap ('Error reading data from the ' & self._serverDesc & ' Server. State = ' & self._state, 'NetNewsReceive.Process')
      self.close()
      return
    end

    Case self._state

    !--------------------
    of NET:NR_CONNECTING
    !--------------------
      self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Connecting State. Will send MODE READER or USER NAME')
      if (self._ProcessSplitReturned(loc:SplitStr) <> 200) and (self._ProcessSplitReturned(loc:SplitStr) <> 201)
        Do RaiseError
      else
        if self._ProcessSplitReturned(loc:SplitStr) = 201
          self.NoPosting = 1
        else
          self.NoPosting = 1
        end
        ! Start doing next state
        if self.password = '' and self.user = ''
          self._state = NET:NR_MODE
          self.packet.bindata = 'MODE READER<13,10>'
          self._ProcessSendIt
        else
          self._state = NET:NR_USER
          self.packet.bindata = 'AUTHINFO USER ' & clip (self.User) & '<13,10>'
          self._ProcessSendIt
        end
      end

    !--------------------
    of NET:NR_USER
    !--------------------
      self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'User State. Will send PASSWORD')
      if self._ProcessSplitReturned(loc:SplitStr) <> 381
        Do RaiseError
      else
        ! Start doing next state
        self._state = NET:NR_PASSWORD
        self.packet.bindata = 'AUTHINFO PASS ' & clip (self.Password) & '<13,10>'
        self._ProcessSendIt
      end

    !--------------------
    of NET:NR_PASSWORD
    !--------------------
      self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Password State. Will send MODE READER')
      if self._ProcessSplitReturned(loc:SplitStr) <> 281
        Do RaiseError
      else
        ! Start doing next state
        self._state = NET:NR_MODE
        self.packet.bindata = 'MODE READER<13,10>'
        self._ProcessSendIt
      end

    !--------------------
    of NET:NR_MODE
    !--------------------
      self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Mode State. Will send GROUP ' & clip (self.Newsgroup))
      if self._ProcessSplitReturned(loc:SplitStr) = 201
        self.NoPosting = 1
      end
      ! Start doing next state
      if (self._command = NET:NewsCount) or    |
         (self._command = NET:NewsDownload) or |
         (self._command = Net:NewsHeadersOnly)
        self._state = NET:NR_GROUP
        self.packet.bindata = 'GROUP ' & clip (self.Newsgroup) & '<13,10>'
        self._ProcessSendIt
      elsif (self._command = NET:NewsListGroups)
        self._state = NET:NR_LIST
        self._TempData = ''
        self._TempDataLen = 0
        self._SubPacket = 0
        self.packet.bindata = 'LIST<13,10>'
        self._ProcessSendIt
      else
        self.ErrorTrap ('Invalid self._command. Set to ' & self._command, 'NetNewsReceive.Process')
        self.close()
      end

    !--------------------
    of NET:NR_LIST
    !--------------------
      self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'LIST State.')
      if self._ProcessList()
        return
      end
    !--------------------
    of NET:NR_GROUP
    !--------------------
      self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Group State. Will decide what to do and action it.')
      if self._ProcessSplitReturned(loc:SplitStr) <> 211
        Do RaiseError
      else
        self.TotalMessagesCount = 0
        self.TotalBytesCount = 0

        self.TotalMessagesCount = self._SplitParam (loc:SplitStr, 1)
        self._CurrentMessageNum = self._SplitParam (loc:SplitStr, 2)  ! The Message ID we start processing
        if (self._CurrentMessageNum < self.DownloadRangeStart)
          self._CurrentMessageNum = self.DownloadRangeStart
        end
        self._MaxMessageNum = self._SplitParam (loc:SplitStr, 3)      ! Max Message ID
        if (self.DownloadRangeEnd <> 0) and (self._MaxMessageNum > self.DownloadRangeEnd)
          self._MaxMessageNum = self.DownloadRangeEnd
        end

        ! Start doing next state
        if self._command = Net:NewsCount
          self._ProcessDoQuit()
        elsif (self._command = Net:NewsDownload) or (self._command = Net:NewsHeadersOnly)
          if self.TotalMessagesCount <= 0
            self._ProcessDoQuit()
          else
            self._state = NET:NR_XOVER
            self._SubPacket = 0                  ! Reset
            if (self.DownloadRangeStart = 0) and (self.DownloadRangeEnd = 0)
              !self.packet.bindata = 'XOVER ' & '-' & '<13,10>'                                                 ! Sean March 2007: WRONG WRONG WRONG, this is NOT how the RFC specifies it.
              self.packet.bindata = 'XOVER<13,10>'
            elsif (self.DownloadRangeStart = 0)
              self.packet.bindata = 'XOVER ' & self._CurrentMessageNum & '-' & self.DownloadRangeEnd & '<13,10>'
            elsif (self.DownloadRangeEnd = 0)
              !self.packet.bindata = 'XOVER ' & self.DownloadRangeStart & '-' & self._MaxMessageNum & '<13,10>' ! Sean March 2007: Arghhh! Again, NOT how the RFC specifies it!
              self.packet.bindata = 'XOVER ' & self.DownloadRangeStart & '-<13,10>'
            else
              self.packet.bindata = 'XOVER ' & self.DownloadRangeStart & '-' & self.DownloadRangeEnd & '<13,10>'
            end
            self._ProcessSendIt
          end
        end
      end

    !--------------------
    of NET:NR_XOVER
    !--------------------
      self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'XOVER State. Will decide what to do and action it.')
      self._ProcessXOver()

    !--------------------
    of NET:ER_DOWNLOAD
    !--------------------
      self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Download State.')
      if self._ProcessGetData()
        return
      end
    !--------------------
    of NET:NR_QUIT
    !--------------------
      self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Quit State.')

      if self._ProcessSplitReturned(loc:SplitStr) <> 205
        Do RaiseError
      else
        ! Start doing next state
        if self.OpenFlag = 1
          self._state = NET:ER_WAITING
        else
          self._state = NET:ER_NOTCONNECTED
        end
        self.Log ('NetNewsReceive.Process' & ' (' & self._serverDesc & ')', 'Calling Done(command, true)')
        if self.progresscontrol                          ! Update the Progress Control Control if there is one
          self.progresscontrol{prop:progress} = 1000
          display(self.progressControl)
        end
        self._CallDone(self._command, true)
        if self.progresscontrol                          ! Update the Progress Control Control if there is one
          self.progresscontrol{prop:hide} = 1
          self.progresscontrol{prop:progress} = 0
          display(self.progressControl)
        end
      end
    !---------------------
    else
      self.ErrorTrap ('Error - no state found. State = ' &  self._state, 'NetNewsReceive.Process')
      self.close()
    end
    return
  end


!-------------------------------------------------------------------------
RaiseError Routine
  if self._ProcessRaiseError (loc:SplitStr)
    return
  end


!-----------------------------------------------------------
NetNewsReceive.Ask   Procedure (long p_command)
  CODE
! Command is set to one of Net:NewsCount, Net:NewsDownload, Net:NewsListGroups, Net:NewsHeadersOnly
  self.Log ('NetNewsReceive.Ask' & ' (' & self._serverDesc & ')', 'Ask called with command = ' & p_command)

  self._command = p_Command

  self.TotalBytesCount = 0
  self.TotalBytesToGo = 0
  self.Progress1000 = 0
  self.TotalBytesReceived = 0


  if self.openFlag = 0 and self.AsyncOpenBusy = 0
    Do OpenConnection
  else
    if (self._State <> NET:ER_NOTCONNECTED) and (self._State <> NET:ER_WAITING)
      if self.OpenFlag or self.AsyncOpenBusy ! New 7/7/2004 - Prevent recursive calls to .ErrorTrap if Abort fails
        self.abort()  ! Nuke connection and start again
      end
      Do OpenConnection
    else
      self.packet.binData = 'STAT<13,10>'    !XXX ???
      self.packet.bindatalen = len (clip (self.packet.bindata))
      self.send()
    end
  end

  if self.progresscontrol                          ! Update the Progress Control Control if there is one
    self.progresscontrol{PROP:RangeLow} = 0        ! zero to 1000
    self.progresscontrol{PROP:RangeHigh} = 1000    ! zero to 1000
    self.progresscontrol{prop:hide} = 0
    self.progresscontrol{prop:progress} = 0
    display(self.progressControl)
  end


!----------------------------------------------------------------

OpenConnection Routine
  self._state = NET:NR_CONNECTING
  self.open(self.Server,self.Port)

!-----------------------------------------------------------
NetNewsReceive._ProcessSplitReturned Procedure (*String p_Str)
local          group, pre (loc)
ret              long
len              long
               end

  CODE
  p_Str = ''
  if self.packet.binDataLen => 3
    loc:ret = self.packet.binData [1 : 3]
    if self.packet.binDataLen >= 5
      if self.packet.binDataLen >= (3 + len (p_Str))
        loc:Len = (3 + len (p_Str))
      else
        loc:Len = self.packet.binDataLen
      end
      p_Str = self.packet.binData [5 : Loc:Len]
    end
  end
  self.Log ('NetNewReceive._ProcessSplitReturned' & ' (' & self._serverDesc & ')', '--> Data : ' & loc:ret & ' [' & clip (p_Str) & ']')
  return (loc:ret)

!-----------------------------------------------------------
NetNewsReceive._ProcessGetData Procedure ()
local               group, pre (loc)
SplitStr              string (1024)
                    end
  CODE
  self.TotalBytesReceived += self.packet.binDataLen

  self._endNotFound = false

  if self._FirstFlag
    self._ProcessClearData
    self._CurrentHeader = 0                ! It will increment to 1 on first use
    Do GetNextMessageNumber
    self._DownloadStage = 4                ! The stage we are processing (set high so we will rap to next stage)
  else
    ! ===============================================================================
    ! Check repsonses from Server
    ! ===============================================================================
    case self._DownloadStage
    !-----
    of 1                                  ! XOVER
    !-----
      if self._ProcessSplitReturned (loc:SplitStr) <> 224
        if self._ProcessRaiseError (loc:SplitStr)
          return (1)
        end
      end
      self.MsgNumber = self._CurrentMessageNum
      self.Subject = self._SplitParam (loc:SplitStr, 2, 9)
      self.From = self._SplitParam (loc:SplitStr, 3, 9)
      self.SentDate = self._SplitParam (loc:SplitStr, 4, 9)
      self.MessageID = self._SplitParam (loc:SplitStr, 5, 9)
      self.References = self._SplitParam (loc:SplitStr, 6, 9)
      self.Bytes = self._SplitParam (loc:SplitStr, 7, 9)
      self.Lines = self._SplitParam (loc:SplitStr, 8, 9)
      self.XRef = self._SplitParam (loc:SplitStr, 9, 9)
      self.Log ('NetNewsReceive._ProcessGetData' & ' (' & self._serverDesc & ')', |
                ' MsgNumber = ' & clip (self.MsgNumber) & ' Subject = ' & clip (self.Subject) & |
                ' From = ' & clip (self.From) & ' SentDate = ' & clip (self.SentDate) & |
                ' MessageID = ' & clip (self.MessageID) & ' References = ' & clip (self.References) & |
                ' Bytes = ' & clip (self.Bytes) & ' Lines = ' & clip (self.Lines) & |
                ' XRef = ' & clip (self.XRef))
    !-----
    of 4                                  ! RETR
    !-----
      if self._ProcessCopyBinData('220')                  ! Don't check for OK, it's checked in Do CopyBinData
        return (1)
      end
    end ! Case
    ! Accept data
  end

  self.Log ('NetNewsReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Download stage = ' & self._DownloadStage)

  if not (self._FirstFlag)                                  ! Not first time
    ! ===============================================================================
    ! Decide what DownloadStage you should be doing next
    ! ===============================================================================
    case self._DownloadStage
    of 1                                                    ! LIST
      self.DecideAction = Net:NewsDownload
      self.Log ('NetNewsReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Calling Decide()')

      self.Decide(Net:NewsBasics)                           ! Find out what the guy wants to do
      case (self.DecideAction)
      of Net:NewsDownload
        self._DownloadStage = 4
      of Net:NewsSkip
        self._ProcessClearData()
        Do GetNextMessageNumber
        self._DownloadStage = 4
      of Net:NewsStop
        self._ProcessClearData()
        self._ProcessDoQuit()                                ! Finish
        return 1
      else
        self.ErrorTrap ('Illegal value for self.DecideAction', 'NetNewsReceive._ProcessGetData')
        self.Close()
        return 1
      end
    of 4                                          ! Download
      if self._endNotFound = true
                                                  ! Keep receiving packets till we get the last packet for the message
      else  ! End Found
        if self._ProcessSplitWholeMessage()
          return (1)
        end
        self.Log ('NetNewsReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Calling Done(command, false)')
        self._CallDone (self._command, false)                  ! Let the user know we've downloaded a message
        self._ProcessClearData
        Do GetNextMessageNumber
        self._DownloadStage = 4
      end
    else
      self.ErrorTrap ('Illegal value for self.DownloadStage', 'NetNewsReceive._ProcessGetData')
      self.Close()
      return (1)
    end
  else
    self._FirstFlag = false
  end

  ! Double check that we haven't finished
  if self._CurrentMessageNum > self._MaxMessageNum
    self._ProcessDoQuit()                                    ! All done
    return (1)
  end

  if self._state = NET:ER_NOTCONNECTED                   ! We got disconnected
    return (1)
  end


  ! ===============================================================================
  ! Send the next command to the server. Determined by DownloadStage
  ! ===============================================================================
  case self._DownloadStage
  of 1
    self._FirstPacket1 = true
    self._Last5BytesLen = 0
    self.packet.bindata = 'XOVER ' & self._CurrentMessageNum & '<13,10>'
    self._ProcessSendIt
  of 4
    if self._endNotFound = false
      self._FirstPacket1 = true
      self._Last5BytesLen = 0
      self.packet.bindata = 'ARTICLE ' & self._CurrentMessageNum & '<13,10>'
      self._ProcessSendIt
    else
      ! Waiting for next packet
    end
  end

  return (0)


! =============================================================================
GetNextMessageNumber Routine
    if records (self.qNewsHeaders) = 0
      self._ProcessDoQuit()                                    ! All done
      return (1)
    end
    loop
      self._CurrentHeader += 1
      if self._CurrentHeader > records (self.qNewsHeaders)
        self._ProcessDoQuit()                                    ! All done
        return (1)
      end
      get (self.qNewsHeaders, self._CurrentHeader)
      if errorcode()
        self.Log ('NetNewsReceive._ProcessGetData' & ' (' & self._serverDesc & ')', 'Could not read qNewsHeader Queue. Failing.')
        return (1)
      else
        self._CurrentMessageNum = self.qNewsHeaders.Number
      end
      if self._CurrentMessageNum < self.DownloadRangeStart
        ! Keep looping
      else
        if self.qNewsHeaders.DownloadFlag = 1
          break
        else
          ! Keep looping
        end
      end
    end
    if self._CurrentMessageNum > self._MaxMessageNum
      self._ProcessDoQuit()                                    ! All done
      return (1)
    end
    self.MsgNumber = self.qNewsHeaders.Number
    self.Subject = self.qNewsHeaders.Subject
    self.From = self.qNewsHeaders.From
    self.SentDate = self.qNewsHeaders.Date
    self.MessageID = self.qNewsHeaders.MessageID
    self.References = self.qNewsHeaders.References
    self.Bytes = self.qNewsHeaders.Bytes
    self.Lines = self.qNewsHeaders.Lines
    self.XRef = self.qNewsHeaders.XRef
    self.Log ('NetNewsReceive._ProcessGetData' & ' (' & self._serverDesc & ')', |
              ' MsgNumber = ' & clip (self.MsgNumber) & |
              ' Subject = ' & clip (self.Subject) & |
              ' From = ' & clip (self.From) & |
              ' SentDate = ' & clip (self.SentDate) & |
              ' MessageID = ' & clip (self.MessageID) & |
              ' References = ' & clip (self.References) & |
              ' Bytes = ' & clip (self.Bytes) & |
              ' Lines = ' & clip (self.Lines) & |
              ' XRef = ' & clip (self.XRef))

!-----------------------------------------------------------
NetNewsReceive._ProcessList Procedure ()
local               group, pre(loc)
SplitStr              string (1024)
startpos              long         ! Start Position
endpos                long
TempStartPos          long
                    end
  CODE
  self.Log ('NetNewsReceive._ProcessList' & ' (' & self._serverDesc & ')', 'Called')

  ! Fill TempData as data returned may be larger than one packet
  self._TempData [(self._TempDataLen + 1) : (self._TempDataLen + self.packet.binDataLen)] = self.packet.binData [1 : self.packet.binDataLen]
  self._TempDataLen += self.packet.binDataLen

  if self._SubPacket = false ! ie FirstPacket
    self._SubPacket = true
    if self._ProcessSplitReturned(loc:SplitStr) <> 215
      if self._ProcessRaiseError (loc:SplitStr)
        return (1)
      end
    end
    free (self.qNewsgroups)
    loc:startpos = 0
  else
    loc:startpos = 0
  end

  loop
    loc:tempStartPos = loc:startPos
    loc:startpos = InString ('<13,10>', self._TempData [1 : self._TempDataLen], 1, (loc:startpos + 1)) ! Strip the rest of the line off
    if loc:startpos = 0
      Do WaitForNextPacket ! End not found wait for next packet.
      break
    else
      if loc:startpos > (self._TempDataLen - 4)
        Do WaitForNextPacket ! End not found wait for next packet.
        break
      else
        if self._TempData [loc:startpos : (loc:startpos + 4)] = '<13,10>.<13,10>'
          Do AllDone
          break
        else
          loc:endpos = InString ('<13,10>', self._TempData [(loc:startpos + 2) : self._TempDataLen], 1, 1) ! Strip the rest of the line off
          if loc:endpos = 0
            Do WaitForNextPacket ! End not found wait for next packet.
            break
          else
            !  ===================== Got a whole line of data ===============================
            loc:endpos += (loc:startpos + 1)
            if (loc:startpos + 2) < loc:endpos
              clear (self.qNewsgroups)
              self.qNewsgroups.Name =     self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 1)
              self.qNewsgroups.StartID =  self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 3)
              self.qNewsgroups.EndID =    self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 2)
              self.qNewsgroups.EstimatedCount = self.qNewsgroups.EndID - self.qNewsgroups.StartID + 1
              if self.qNewsgroups.EstimatedCount < 0
                self.qNewsgroups.EstimatedCount = 0
              end
              add (self.qNewsgroups)
            end
          end
        end
      end
    end
  end
  return (0)

AllDone Routine
  self._TempData = ''
  self._TempDataLen = 0
  self._SubPacket = 0
  sort (self.qNewsgroups, +self.qNewsgroups.Name)
  self._ProcessDoQuit()

WaitForNextPacket Routine
  self._TempData2 = ''
  self._TempData2 [1 : (self._TempDataLen - loc:TempStartPos)] = self._TempData [(loc:TempStartPos + 1) : self._TempDataLen]
  self._TempData  [1 : (self._TempDataLen - loc:TempStartPos)] = self._TempData2 [1 : (self._TempDataLen - loc:TempStartPos)]
  self._TempDataLen = (self._TempDataLen - loc:TempStartPos)


!-----------------------------------------------------------
NetNewsReceive.Init  Procedure (uLong Mode=NET:SimpleClient)
  CODE
  self._qIntNewsgroups &= new (Net:NewsGroupsQType)
  self.qNewsgroups &= self._qIntNewsgroups

  self._qIntNewsHeaders &= new (Net:NewsHeaderQType)
  self.qNewsHeaders &= self._qIntNewsHeaders


  self.AsyncOpenTimeOut = 1200 ! 12 seconds  (To use Async Open set .AsyncOpenUse = 1)
  self.AsyncOpenUse = 1
  self.InActiveTimeout = 9000  ! 90 seconds


  if self._serverDesc_Set = 0
    self._ServerDesc = 'NetNews'
    self._serverDesc_Set = 1

    self.DefaultDownloadFlag = 1  ! Default to downloading messages (you can set this - it controls all messages, or individually set it in the self.decide() method).
    self._ObjectType = NET:NNTP_CLIENT

    if self.Port = 0
      self.Port = NET:NNTP_PORT    ! Default Port number if none specified
    end
  end

  self._ZeroByteFixDisable = 1  ! 24/6/2004 - turn this on by default, as this fix only applies to emails, and can cause errors with NewServers, that don't always return the size of the news article.

  parent.init()




!-----------------------------------------------------------
NetNewsReceive.Kill  Procedure
  CODE
  self.Log ('NetNewsReceive.Kill' & ' (' & self._serverDesc & ')', 'Kill called')

  parent.kill()

  ! Moved after kill by Jono 15/3/2001 - To prevent GPF if any code in the kill method access these allocated data items.
  free (self.qNewsgroups)
  free (self.qNewsHeaders)
  if ~self._qIntNewsgroups&=NULL
    dispose (self._qIntNewsgroups)
  end
  if ~self._qIntNewsHeaders&=NULL
    dispose (self._qIntNewsHeaders)
  end


  self.Log ('NetNewsReceive.Kill' & ' (' & self._serverDesc & ')', 'End of Kill()')


!-----------------------------------------------------------
NetNewsReceive._ProcessXOver Procedure ()
local               group, pre(loc)
SplitStr              string (1024)
startpos              long         ! Start Position
endpos                long
TempStartPos          long
                    end
  CODE
  self.Log ('NetNewsReceive._ProcessXOver' & ' (' & self._serverDesc & ')', 'Start Process XOVER' & |
            ' binDataLen = ' & self.packet.binDataLen & |
            ' _TempDataLen = ' & self._TempDataLen & |
            ' XXX = ' & (self._TempDataLen + self.packet.binDataLen) - (self._TempDataLen + 1) + 1 & |
            ' Max = ' & (self._TempDataLen + self.packet.binDataLen))

  ! Fill TempData as data returned may be larger than one packet
  self._TempData [(self._TempDataLen + 1) : (self._TempDataLen + self.packet.binDataLen)] = self.packet.binData [1 : self.packet.binDataLen]
  self._TempDataLen += self.packet.binDataLen

  if self._SubPacket = false ! ie FirstPacket
    self._SubPacket = true
    if self._ProcessSplitReturned(loc:SplitStr) <> 224
      if self._ProcessRaiseError (loc:SplitStr)
        return (1)
      end
    end
    free (self.qNewsHeaders)
    loc:startpos = 0
  else
    loc:startpos = 0
  end

  ! ===================================================================================

  loop
    loc:tempStartPos = loc:startPos
    loc:startpos = InString ('<13,10>', self._TempData [1 : self._TempDataLen], 1, (loc:startpos + 1)) ! Strip the rest of the line off
    if loc:startpos = 0
      Do WaitForNextPacket ! End not found wait for next packet.
      break
    else
      if loc:startpos > (self._TempDataLen - 4)
        Do WaitForNextPacket ! End not found wait for next packet.
        break
      else
        if self._TempData [loc:startpos : (loc:startpos + 4)] = '<13,10>.<13,10>'
          Do XOverDone
          break
        else
          loc:endpos = InString ('<13,10>', self._TempData [(loc:startpos + 2) : self._TempDataLen], 1, 1) ! Strip the rest of the line off
          if loc:endpos = 0
            Do WaitForNextPacket ! End not found wait for next packet.
            break
          else
            ! ============  Got a whole line of data ===============================
            loc:endpos += (loc:startpos + 1)
            if (loc:startpos + 2) < loc:endpos
              clear (self.qNewsHeaders)
              self.qNewsHeaders.Number =     self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 1, 9)
              self.qNewsHeaders.Subject =    self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 2, 9)
              self.qNewsHeaders.From =       self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 3, 9)
              self.qNewsHeaders.Date =       self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 4, 9)
              self.qNewsHeaders.MessageID =  self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 5, 9)
              self.qNewsHeaders.References = self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 6, 9)
              self.qNewsHeaders.Bytes =      self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 7, 9)
              self.qNewsHeaders.Lines =      self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 8, 9)
              self.qNewsHeaders.XRef =       self._SplitParam (self._TempData [loc:startpos + 2 : (loc:endpos - 1)], 9, 9)
              self.qNewsHeaders.DownLoadFlag = self.DefaultDownloadFlag
              add (self.qNewsHeaders)
              self.TotalBytesCount += self.qNewsHeaders.Bytes
              !Message ('HEY 1 = ' & self.TotalBytesCount)
            end
          end
        end
      end
    end
  end
  return (0)

! =============================================================================
XOverDone Routine
  self._TempData = ''
  self._TempDataLen = 0
  self._SubPacket = 0

  self._FirstFlag = true
  self._state = NET:ER_DOWNLOAD
  if (self._command = Net:NewsDownload)
    self.Decide(NET:NewsDetails)             ! Give the programmer a chance to decide what needs downloading
    if self._ProcessGetData()                ! Move on to downloading the stuff
      return(0)
    end
  elsif (self._command = Net:NewsHeadersOnly)
    self._ProcessDoQuit()
  else
    self.Log ('NetNewsReceive._ProcessXOver' & ' (' & self._serverDesc & ')', 'Warning. Invalid State')
  end

! =============================================================================
WaitForNextPacket Routine
  self._TempData2 = ''
  self._TempData2 [1 : (self._TempDataLen - loc:TempStartPos)] = self._TempData [(loc:TempStartPos + 1) : self._TempDataLen]
  self._TempData  [1 : (self._TempDataLen - loc:TempStartPos)] = self._TempData2 [1 : (self._TempDataLen - loc:TempStartPos)]
  self._TempDataLen = (self._TempDataLen - loc:TempStartPos)


!-----------------------------------------------------------
NetNewsSend.Init     Procedure (uLong Mode=NET:SimpleClient)
  code
    self.asyncOpenUse = 1
    self.asyncOpenTimeOut = NET:ASYNC_OPEN_TIMEOUT
    self.inActiveTimeout = NET:INACTIVE_TIMEOUT


    if self._serverDesc_Set = 0
      self._serverDesc_Set = 1
      self._objectType = NET:NNTP_CLIENT
      self._serverDesc = 'NetNews'
      if self.xMailer = ''
        self.xMailer = 'NetTalk News ' & NETTALK:EmailVersion
      end
      if self.port = 0
        self.port = NET:NNTP_PORT                             ! Default Port number if none specified
      end
    end

    parent.Init()

!-----------------------------------------------------------
NetNewsSend.Process  Procedure
ReturnedStr         string(256)
ReturnedNum         long
ReturnedStrLen      long
DataPos             long
endPos              long
SecondLastByte      string(1)
LastByte            string(1)
_Pad12              string(1)
_Pad13              string(1)

local               group, pre(loc)
SplitStr              string (1024)
FullLineReceived      byte
SplitNumber           long
InActiveSeconds       long
                    end


  CODE
  case (self.packet.packetType)
  ! ------------------------------
  of NET:SimpleIdleConnection
    self._CloseIdleConnection()
  ! ------------------------------
  of NET:SimpleAsyncOpenSuccessful
  ! ------------------------------
    self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', 'Connection opened asynchronously.')
    self._ProcessOpen()
    ! Wait for packet data to arrive

  ! ------------------------------
  of NET:SimplePartialDataPacket
    self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', 'Received Data from ' & self._serverDesc & ' Server')

    loc:FullLineReceived = false
    if self.packet.binDataLen >= 2
      if self.packet.binData[(self.packet.binDataLen-1) : self.packet.binDataLen] = '<13,10>'
        loc:FullLineReceived = true
      end
    end

    if loc:FullLineReceived = false
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', |
                'Warning. We Received Data from ' & self._serverDesc & ' Server - but did not receive a whole line. Will store the line in the buffer')
      if self._ServerReplyBufferLen = 0
        self._ServerReplyBuffer = self.packet.binData [1 : self.packet.binDataLen]
        self._ServerReplyBufferLen = self.packet.binDataLen
      else
        self._ServerReplyBuffer = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen] & self.packet.binData [1 : self.packet.binDataLen]
        self._ServerReplyBufferLen += self.packet.binDataLen
        if self._ServerReplyBufferLen > size (self._ServerReplyBuffer)
          self._ServerReplyBufferLen = size (self._ServerReplyBuffer)
        end
      end
      return ! more data is coming.
    end

    if self._ServerReplyBufferLen > 0
      ! Right join the buffered data and this new data
      if self.packet.binDataLen = 0
        self.packet.binData = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen]
        self.packet.binDataLen = self._ServerReplyBufferLen
      else
        self.packet.binData = self._ServerReplyBuffer[1 : self._ServerReplyBufferLen] & self.packet.binData[1 : self.packet.binDataLen]
        self.packet.binDataLen += self._ServerReplyBufferLen
        if self.packet.binDataLen > size (self.packet.binData)
          self.packet.binDataLen = size (self.packet.binData)
        end
      end
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', |
                'Okay we put the buffered lines together and got [' & self.packet.binData [ 1 : self.packet.BinDataLen] & ']')
    end

    if self.Error
      self._ErrorStatus = NET:ERROR_STATUS_SEND_ERROR
      self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
      self.ErrorTrap ('Error reading data from the ' & self._serverDesc & ' Server', 'NetNewsSend.Process')
      if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
        Do DoNextEmail
      end
      return
    end

    Do SplitReturned         ! Spilt the returned string into a return code and return text

    if self._state = NET:NS_CONNECTEDANDIDLE  ! added by Jono 13/8/2000
      if records (self.DataQueue) > 0
        self._state = NET:NS_READY
      else
        self._state = NET:NS_CONNECTEDANDIDLE  ! nothing to do, but we are connected.
        return
      end
    end

    case self._state
    !--------------------
    of NET:NS_NOTCONNECTED
    !--------------------
      if ReturnedNum <> 205  ! New 21/11/2003 (Clarion 6 somtimes gets a 221 (closing down message) after everything has closed down - don't error here, it's fine.
        Do RaiseErrorAndAbortAllSends       ! We received a message from the mail server, but weren't expeciting it?
      end

    !--------------------
    of NET:NS_READY
    !--------------------
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', 'Ready State. Will send MODE READER')
      ! Start doing next state
      if self.password = '' and self.user = ''
        self._state = NET:NS_MODE
        self.packet.bindata = 'MODE READER<13,10>'
        Do SendIt
      else
        self._state = NET:NS_USER
        self.packet.bindata = 'AUTHINFO USER ' & clip (self.User) & '<13,10>'
        Do SendIt
      end

    !--------------------
    of NET:NS_CONNECTING
    !--------------------
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', 'Connecting State. Will send MODE READER')
      loc:SplitNumber = self._ProcessSplitReturned(loc:SplitStr)

      if (loc:SplitNumber = 201)
        self.abort()
        self.Error = ERROR:NoPostingAllowed
        self.WinSockError = 0
        self.SSLError = 0
        Do RaiseErrorAndAbortAllSends
      elsif (loc:SplitNumber <> 200)
        Do RaiseErrorAndAbortAllSends
      else
        ! Start doing next state
        if self.password = '' and self.user = ''
          self._state = NET:NS_MODE
          self.packet.bindata = 'MODE READER<13,10>'
          Do SendIt
        else
          self._state = NET:NS_USER
          self.packet.bindata = 'AUTHINFO USER ' & clip (self.User) & '<13,10>'
          Do SendIt
        end
      end

    !--------------------
    of NET:NS_USER
    !--------------------
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', 'User State. Will send PASSWORD')
      if self._ProcessSplitReturned(loc:SplitStr) <> 381
        Do RaiseErrorAndAbortAllSends
      else
        ! Start doing next state
        self._state = NET:NS_PASSWORD
        self.packet.bindata = 'AUTHINFO PASS ' & clip (self.Password) & '<13,10>'
        Do SendIt
      end

    !--------------------
    of NET:NS_PASSWORD
    !--------------------
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', 'Password State. Will send MODE READER')
      if self._ProcessSplitReturned(loc:SplitStr) <> 281
        Do RaiseErrorAndAbortAllSends
      else
        ! Start doing next state
        self._state = NET:NS_MODE
        self.packet.bindata = 'MODE READER<13,10>'
        Do SendIt
      end


    !--------------------
    of NET:NS_MODE
    !--------------------
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', 'MODE State. Will send POST')
      case (self._ProcessSplitReturned(loc:SplitStr))
      of   200 ! Cool
      orof 500 ! Command not recognised - just keep going.
        ! Start doing next state
        self._state = NET:NS_POST
        self.packet.bindata = 'POST<13,10>'
        Do SendIt
      else
        Do RaiseErrorAndAbortAllSends
      end


    !--------------------
    of NET:NS_POST
    !--------------------
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', 'MODE State. Will send POST')

      if ReturnedNum = 480  ! Logon Required
        Do RaiseErrorAndAbortAllSends
      elsif ReturnedNum <> 340
        Do RaiseErrorAndTryNextMessage
      else
        self._state = NET:NS_POSTEND

        ! Send WholeMessage and Break it up into small bits to send
        DataPos = 0
        SecondLastByte = ''
        LastByte = ''
        loop
          if DataPos + NET:EMAIL_MaxMailData > (self.DataQueue.WholeMessageLen)
            endPos = self.DataQueue.WholeMessageLen
          else
            endPos = DataPos + NET:EMAIL_MaxMailData
          end
          self.packet.bindata = self.DataQueue.WholeMessage [(DataPos + 1) : endPos]
          self.packet.bindatalen = endPos - DataPos                ! Set this ourselves so we don't loose any spaces in SendIt Routine due to clip function
          self._ConvertDots(SecondLastByte, LastByte)   ! Makes Dots transparent  26/7/2002
          self.send()

          DataPos = endPos
          if endPos >= self.DataQueue.WholeMessageLen
            break ! out of loop - We are finished
          end
          if self.packet.binDataLen >= 2
            secondlastbyte = self.packet.binData [self.packet.binDataLen - 1]
          else
            if self.packet.binDataLen >= 1
              secondlastbyte = lastbyte
            else
              !keep the same
            end
          end
          if self.packet.binDataLen >= 1
            lastbyte = self.packet.binData [self.packet.binDataLen]
          else
            !keep the same
          end
        end

        ! Send End Sequence
        self.packet.bindata = '<13,10>.<13,10>'
        Do SendIt
      end


    !--------------------
    of NET:NS_POSTEND
    !--------------------
      if ReturnedNum <> 240
        Do RaiseErrorAndTryNextMessage
      else
        do SentOkay
      end

    !--------------------
    else
    !--------------------
      self._ErrorStatus = NET:ERROR_STATUS_SEND_ERROR
      self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
      self.ErrorTrap ('Error. NetNewsSend - no state found. State = ' & self._state, 'NetNewsSend.Process')
      if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
        Do DoNextEmail
      end
    end
  end


!-------------------------------------------------------------------

SentOkay Routine
  self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', 'Message sent to ' & clip (self.DataQueue.ToList) & '. Will call MessageSent()')
  self._MessagesSentOnCurrentConnection += 1
  self.MessageSent()              ! Call Virtual Method - with message still in top of queue
  Do DoNextEmail

!-------------------------------------------------------------------

DoNextEmail Routine
  Do DeleteOneMessageFromQueue
  if records (self.DataQueue) > 0
    if (self.ReconnectAfterXMsgs > 0) and (self._MessagesSentOnCurrentConnection >= self.ReconnectAfterXMsgs)
      ! Need to restart connection
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', |
                self._MessagesSentOnCurrentConnection & ' messages have been sent on this connection. Connection will be restarted')
      self.packet.bindata = 'QUIT' & '<13,10>'
      Do SendIt
      self.close()
      Do StartConnection
    else
      self._state = NET:NS_POST
      self.packet.bindata = 'POST<13,10>'  ! Send POST to get the next packet out.
      Do SendIt
    end
  else
    if (self.OptionsKeepConnectionOpen = false) or ((self.ReconnectAfterXMsgs > 0) and (self._MessagesSentOnCurrentConnection >= self.ReconnectAfterXMsgs))
      ! Close the connection
      if (self.ReconnectAfterXMsgs > 0) and (self._MessagesSentOnCurrentConnection >= self.ReconnectAfterXMsgs)
        self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', |
                  self._MessagesSentOnCurrentConnection & ' messages have been sent on this connection. (2) Connection will be restarted')
      else
        self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', |
                  'This object has been configured to close when there are no more messages to be sent.')
      end
      ! Close the connection
      self.packet.bindata = 'QUIT' & '<13,10>'
      Do SendIt
      self.close()
      self._state = NET:NS_NOTCONNECTED
    else
      ! Need stuff here - KeepConnectionOpen still not tested!
      self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', |
                'All messages in the Data Queue have been sent. Will keep the connection to the server alive though.')
      self._state = NET:NS_CONNECTEDANDIDLE
    end
    if self.progresscontrol                          ! Update the Progress Control Control if there is one
      self.progresscontrol{prop:hide} = 1
      self.progresscontrol{prop:progress} = 0
      display()
    end
  end


! ------------------------------------------------------------------

StartConnection Routine
  self.Open(self.Server, self.Port)
  if self.OpenFlag = false
    self.error = ERROR:CouldNotConnectToHost
    self.WinSockError = 0
    self.SSLError = 0
    ! self.errorTrap has already been called in self.open, so we don't need to call it here.
    self._State = NET:ES_NOTCONNECTED
    return
  end
  case self._objectType
  of NET:POP_CLIENT orof NET:SMTP_CLIENT
    self._State = NET:ES_CONNECTING
  of NET:NNTP_CLIENT
    self._State = NET:NS_CONNECTING
  end


!-------------------------------------------------------------------

SendIt Routine
  self.packet.bindatalen = len (clip (self.packet.bindata))
  self.send()

!-------------------------------------------------------------------

RaiseErrorAndAbortAllSends Routine
  self._ErrorStatus = NET:ESERVER
  self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_DELETE_ALL
  self.ServerErrorNum = ReturnedNum
  self.ServerErrorDesc = ReturnedStr
  self.ErrorTrap ('Fatal Error ' & self._serverDesc & ' server sending error = ' & ReturnedNum & ' ' & clip (left (ReturnedStr)) & ' State = ' & self._state, 'NetNewsSend.Process')
  self.ServerErrorNum = 0
  self.ServerErrorDesc = ''
  if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_DELETE_ALL
    self._DeleteAllMessagesInDataQueue()
    self.Abort()
  end

!-------------------------------------------------------------------
RaiseErrorAndTryNextMessage Routine
  self._ErrorStatus = NET:ERROR_STATUS_SEND_ERROR
  self._AfterErrorAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
  self.ServerErrorNum = ReturnedNum
  self.ServerErrorDesc = ReturnedStr
  self.ErrorTrap ('Error in this Message. ' & self._serverDesc & ' server sent error = ' & ReturnedNum & ' ' & clip (left (ReturnedStr)) & ' State = ' & self._state & '. Will try and send next email', 'NetNewsSend.Process')
  self.ServerErrorNum = 0
  self.ServerErrorDesc = ''
  if self._AfterErrorSavedAction = NET:ERROR_ACTION_AFTER_ERRORTRAP_TRY_NEXT
    Do DeleteOneMessageFromQueue
    if records (self.DataQueue) > 0
      !self._state = NET:ES_READY           ! Jono 20 March 2001 Was NET:ES_CONNECTEDANDIDLE
      self.abort()  ! Abort connection
      Do StartConnection
    else
      self.abort()
    end
  end

!-------------------------------------------------------------------
SplitReturned Routine
  ReturnedNum = 0
  ReturnedStr = ''
  if self.packet.binDataLen => 3
    ReturnedNum = self.packet.binData [1 : 3]
    if self.packet.binDataLen > 3
      if self.packet.binDataLen >= (3 + len (ReturnedStr))
        ReturnedStrLen = (3 + len (ReturnedStr))
      else
        ReturnedStrLen = self.packet.binDataLen
      end
      ReturnedStr = self.packet.binData [4 : ReturnedStrLen]  ! This may need to be 5! - 11/10/2000
    end
  end
  self.Log ('NetNewsSend.Process' & ' (' & self._serverDesc & ')', '--> Data : ' & ReturnedNum & ' [' & clip (ReturnedStr) & ']')

!-------------------------------------------------------------------
DeleteOneMessageFromQueue Routine
  if records (self.DataQueue) = 0
    clear (self.DataQueue)
    exit
  end
  if size (self.DataQueue.MessageText) > 0
    dispose (self.DataQueue.MessageText)
  end
  if size (self.DataQueue.MessageHtml) > 0
    dispose (self.DataQueue.MessageHtml)
  end
  if size (self.DataQueue.WholeMessage) > 0
    dispose (self.DataQueue.WholeMessage)
  end
  delete (self.DataQueue)
  clear (self.DataQueue)
  if records (self.DataQueue) > 0
    get (self.DataQueue, 1)   ! Load top of queue if it exists
  end

!-----------------------------------------------------------
NetNewsSend._ProcessSplitReturned Procedure (*String p_Str)
local          group, pre (loc)
ret              long
len              long
               end

  CODE
  p_Str = ''
  if self.packet.binDataLen => 3
    loc:ret = self.packet.binData [1 : 3]
    if self.packet.binDataLen >= 5
      if self.packet.binDataLen >= (3 + len (p_Str))
        loc:Len = (3 + len (p_Str))
      else
        loc:Len = self.packet.binDataLen
      end
      p_Str = self.packet.binData [5 : Loc:Len]
    end
  end
  self.Log ('NetNewsSend._ProcessSplitReturned' & ' (' & self._serverDesc & ')', '[' & loc:ret & ']--> Data : ' & loc:ret & ' [' & clip (p_Str) & ']')
  return (loc:ret)

!-----------------------------------------------------------
NetEmailReceive.LoadEmailsDoneBeforeQ Procedure ()
  CODE


!-----------------------------------------------------------
NetEmailReceive.SaveEmailsDoneBeforeQ Procedure ()
  code


!-----------------------------------------------------------

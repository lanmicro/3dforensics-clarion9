! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software - www.capesoft.com
      Member()
         omit('***$***',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
         ***$***
    Include('Equates.CLW'),ONCE
    Include('Keycodes.CLW'),ONCE
    Include('Errors.CLW'),ONCE
    Map
    End ! map
    include('NetMap.inc'),once
    include('NetHttp.inc'),once

!------------------------------------------------------------------------------
NetWebRoot._UnChunk  Procedure(*String pData,*Long pDataLen)
temp       &string
pos        long
ypos       long
chunklen   long
headerpos  long
templen    long
footerpos  long
  code
  temp &= new(string(pDataLen))
  pos = instring ('<13,10><13,10>', pData[1 : pDataLen], 1, 1)
  if pos = 0
    return
  end
  headerpos = pos+1 ! we add the header bit on later
  loop
    do FindNextChunkLenStart
    ypos = instring('<13,10>',pData,1,pos)
    if ypos = 0 then ypos = pos.
    chunklen = self.HexToLong(pData[pos : ypos-1])
    pos = ypos + 2
    if chunklen = 0 or pos > pDataLen ! finished with chunks, so handle footers.
      footerpos =  pDataLen
      break
    end
    if templen
      temp = temp[1 : templen] & pData[pos : pos + chunklen-1]
    else
      temp = pData[pos : pos + chunklen-1]
    end
    templen += chunklen
    pos = pos + chunklen
  end

  if footerpos - pos < 2
    pData = pData[1 : headerpos] & '<13,10>' & temp
    pDataLen = headerpos + 2 + templen
  else
    pData = pData[1 : headerpos] & pData[pos : footerpos] & temp
    pDataLen = headerpos + (footerpos-pos+1) + templen
  end
  dispose(temp)

FindNextChunkLenStart  routine
  loop while pos <= pDataLen
    case pData[pos]
    of '<13>' orof '<10>'
      pos += 1
      cycle
    else
      break
    end
  end
!------------------------------------------------------------------------------
NetWebRoot.NormalizeURL  PROCEDURE(String pURL)
x    long
y    long
  code
  loop
    x = instring('?',pURL,1,1)
    if x = 0 then break.
    pURL[x] = '&'
  end
  x = instring('&',pURL,1,1)
  if x
    pURL[x] = '?'
  end
  x = 0
  y = instring('?',pURL,1,1)
  if y = 0
    y = len(clip(pURL))
  elsif y = len(clip(pURL))
    pURL[y]=' '
  end
  loop
    x = instring('\',pURL,1,x+1)
    if x = 0 or x > y then break.
    pURL[x] = '/'
  end
  return(clip(pURL))

!------------------------------------------------------------------------------
NetWebHttp.Construct               PROCEDURE
  Code
  self._CookieQueue &= new (NetWebServerCookieQueueType)

!------------------------------------------------------------------------------
NetWebHttp.Destruct                PROCEDURE
  Code
  if Not (self._CookieQueue &= Null)
    free (self._CookieQueue)
    dispose (self._CookieQueue)
    self._CookieQueue &= null
  end

!------------------------------------------------------------------------------
NetWebHttp.SetCookie  PROCEDURE (String p_name,String p_Value,<Long p_Date>,<Long p_Time>,<Long p_Secure>,<String p_Path>,<String p_Domain>,Long p_HttpOnly=0)
  Code
  If p_name = '' then return.
  clear(self._CookieQueue)
  self._CookieQueue.Name = Upper(p_name)
  get(self._CookieQueue,self._CookieQueue.Name)
  if Errorcode()
    Add(self._CookieQueue)
  End
  self._CookieQueue.Value = p_Value
  If not omitted(4)!(p_Date)
    self._CookieQueue.ExpiresDate = p_date
  end
  If not omitted(5)!(p_Time)
    self._CookieQueue.ExpiresTime = p_time
  end
  If not omitted(6)!(p_Secure)
    self._CookieQueue.Secure = p_secure
  end
  If not omitted(7)!(p_Path)
    self._CookieQueue.Path = p_path
  else
    self._CookieQueue.Path = '/'
  end
  If not omitted(8)!(p_Domain)
    self._CookieQueue.Domain = p_domain
  end
  self._CookieQueue.HttpOnly = p_HttpOnly
  !self._trace('Creating cookie: ' & clip(self._CookieQueue.Name) & '=' & clip(self._CookieQueue.Value) & ' path=' & clip(self._CookieQueue.Path) & ' domain=' & clip(self._CookieQueue.Domain))
  put(self._CookieQueue)

!------------------------------------------------------------------------------
NetWebHttp.EncodeWebString PROCEDURE  (string p_Text, Long p_Flags=0, Long p_Len=0)
mapping               string('0123456789ABCDEF')
x                     long,auto
y                     long,auto
   omit('***',_VER_C60)
returnValue      string(NET:MAXBinData)
   ***
   compile('***',_VER_C60)
returnValue      STRING(SIZE(p_text) * 3)
   ***

  CODE
  y = 1
  if p_len = 0
    p_len = len(clip(p_text))
  end
  loop x = 1 to p_len
    case val(p_text[x])
    of val('/') orof val('\') orof val('.') orof val('$') !PR32
      if band(p_flags,net:dos)
        returnvalue[y] = p_text[x]
        y += 1
      else
        returnvalue[y] = '%'
        returnvalue[y+1] = mapping[ bshift(band(val(p_text[x]),0F0h),-4)+1 ]
        returnvalue[y+2] = mapping[ band(val(p_text[x]),0Fh)+1 ]
        y += 3
      end

    of 48 to 57 ! 0 to 9
    orof 65 to 90 ! A to Z
    orof 97 to 122 ! a to z
      returnvalue[y] = p_text[x]
      y += 1
    of 32 ! space
      if band(p_flags,net:NoHex + net:NoPlus)
        returnvalue[y] = p_text[x]
      elsif band(p_flags,net:BigPlus)
        returnvalue[y : y+2] = '%20'
        y += 2
      else
        returnvalue[y] = '+'
      end
      y += 1
    of 58 ! :
      if band(p_flags,net:NoColon)
        returnvalue[y] = p_text[x]
        y += 1
      else
        returnvalue[y] = '%'
        returnvalue[y+1] = mapping[ bshift(band(val(p_text[x]),0F0h),-4)+1 ]
        returnvalue[y+2] = mapping[ band(val(p_text[x]),0Fh)+1 ]
        y += 3
      end
    else
      returnvalue[y] = '%'
      returnvalue[y+1] = mapping[ bshift(band(val(p_text[x]),0F0h),-4)+1 ]
      returnvalue[y+2] = mapping[ band(val(p_text[x]),0Fh)+1 ]
      y += 3
    end
  end
  return clip(returnValue)

!------------------------------------------------------------------------------
NetWebHttp.EncodeWebStringLength PROCEDURE  (string p_Text, Long p_Flags=0, Long p_Len=0)
y  long
x  long
  code
  if p_len = 0
    p_len = len(clip(p_text))
  end
  loop x = 1 to p_len
    case val(p_text[x])
    of 48 to 57 ! 0 to 9
    orof 65 to 90 ! A to Z
    orof 97 to 122 ! a to z
      y += 1
    of 32 ! space
      if band(p_flags,net:BigPlus)
        y += 2
      end
      y += 1
    else
      y += 3
    end
  end
  return y
!------------------------------------------------------------------------------
NetWebHttp.Base64Encode  PROCEDURE (String p_Text)
x   Long
ReturnValue String(NET:Base64MaxEncodedLength)
  CODE
  x = len(clip(p_Text))
  ReturnValue = NetBase64Encode(p_Text,x)
  return clip(ReturnValue)
!------------------------------------------------------------------------------
NetWebHttp.Base64Decode  PROCEDURE (String p_Text)
x Long
ReturnValue String(NET:Base64MaxDecodedLength)
  CODE
  x = len(clip(p_Text))
  ReturnValue = NetBase64Decode(p_Text,x)
  return sub(ReturnValue,1,x)

!------------------------------------------------------------------------------
NetWebHttp._GetContentType PROCEDURE  (string p_FileName)
local             group, pre(loc)
l                   long
Ext                 string (40)
x                   long
                  end
  CODE
  loc:l = len (clip(p_FileName))
  loop loc:x = loc:l to 1 by -1
    case p_FileName [loc:x]
    of '.'
      if loc:x < loc:l
        loc:Ext = p_FileName [(loc:x+1) : loc:l]
      end
      break
    end
  end
  loc:l = instring('?',loc:Ext,1,1)
  if loc:l
    loc:Ext = sub(loc:Ext,1,loc:l-1)
  end
  loc:l = instring('&',loc:Ext,1,1)
  if loc:l
    loc:Ext = sub(loc:Ext,1,loc:l-1)
  end

  case lower(clip (loc:Ext))
  of 'ai'
    return ('application/postscript')
  of 'aif'
    return ('audio/x-aiff')
  of 'aifc'
    return ('audio/x-aiff')
  of 'aiff'
    return ('audio/x-aiff')
  of 'asc'
    return ('text/plain')
  of 'au'
    return ('audio/basic')
  of 'avi'
    return ('video/x-msvideo')
  of 'bcpio'
    return ('application/x-bcpio')
  of 'bin'
    return ('application/octet-stream')
  of 'c'
    return ('text/plain')
  of 'cc'
    return ('text/plain')
  of 'ccad'
    return ('application/clariscad')
  of 'cdf'
    return ('application/x-netcdf')
  of 'class'
    return ('application/octet-stream')
  of 'cpio'
    return ('application/x-cpio')
  of 'cpt'
    return ('application/mac-compactpro')
  of 'csh'
    return ('application/x-csh')
  of 'css'
    return ('text/css')
  of 'dcr'
    return ('application/x-director')
  of 'dir'
    return ('application/x-director')
  of 'dms'
    return ('application/octet-stream')
  of 'doc'
  orof 'docx'
    return ('application/msword')
  of 'drw'
    return ('application/drafting')
  of 'dvi'
    return ('application/x-dvi')
  of 'dwg'
    return ('application/acad')
  of 'dxf'
    return ('application/dxf')
  of 'dxr'
    return ('application/x-director')
  of 'eps'
    return ('application/postscript')
  of 'etx'
    return ('text/x-setext')
  of 'exe'
    return ('application/octet-stream')
  of 'ez'
    return ('application/andrew-inset')
  of 'f'
    return ('text/plain')
  of 'f90'
    return ('text/plain')
  of 'fli'
    return ('video/x-fli')
  of 'gif'
    return ('image/gif')
  of 'gtar'
    return ('application/x-gtar')
  of 'gz'
    return ('application/x-gzip')
  of 'h'
    return ('text/plain')
  of 'hdf'
    return ('application/x-hdf')
  of 'hh'
    return ('text/plain')
  of 'hqx'
    return ('application/mac-binhex40')
  of 'htc'
    return ('text/x-component')
  of 'htm'
    return ('text/html')
  of 'html'
    return ('text/html')
  of 'ice'
    return ('x-conference/x-cooltalk')
  of 'ief'
    return ('image/ief')
  of 'iges'
    return ('model/iges')
  of 'igs'
    return ('model/iges')
  of 'ips'
    return ('application/x-ipscript')
  of 'ipx'
    return ('application/x-ipix')
  of 'jpe'
    return ('image/jpeg')
  of 'jpeg'
    return ('image/jpeg')
  of 'jpg'
    return ('image/jpeg')
  of 'js'
    return ('application/x-javascript')
  of 'kar'
    return ('audio/midi')
  of 'latex'
    return ('application/x-latex')
  of 'lha'
    return ('application/octet-stream')
  of 'lsp'
    return ('application/x-lisp')
  of 'lzh'
    return ('application/octet-stream')
  of 'm'
    return ('text/plain')
  of 'man'
    return ('application/x-troff-man')
  of 'me'
    return ('application/x-troff-me')
  of 'mesh'
    return ('model/mesh')
  of 'mid'
    return ('audio/midi')
  of 'midi'
    return ('audio/midi')
  of 'mif'
    return ('application/vnd.mif')
  of 'mime'
    return ('www/mime')
  of 'mov'
    return ('video/quicktime')
  of 'movie'
    return ('video/x-sgi-movie')
  of 'mp2'
    return ('audio/mpeg')
  of 'mp3'
    return ('audio/mpeg')
  of 'mpe'
    return ('video/mpeg')
  of 'mpeg'
    return ('video/mpeg')
  of 'mpg'
    return ('video/mpeg')
  of 'mpga'
    return ('audio/mpeg')
  of 'ms'
    return ('application/x-troff-ms')
  of 'msh'
    return ('model/mesh')
  of 'nc'
    return ('application/x-netcdf')
  of 'oda'
    return ('application/oda')
  of 'pbm'
    return ('image/x-portable-bitmap')
  of 'pdb'
    return ('chemical/x-pdb')
  of 'pdf'
    return ('application/pdf')
  of 'msi'
    return ('application/msi')
  of 'pgm'
    return ('image/x-portable-graymap')
  of 'pgn'
    return ('application/x-chess-pgn')
  of 'png'
    return ('image/png')
  of 'pnm'
    return ('image/x-portable-anymap')
  of 'pot'
    return ('application/mspowerpoint')
  of 'ppm'
    return ('image/x-portable-pixmap')
  of 'pps'
    return ('application/mspowerpoint')
  of 'ppt'
    return ('application/mspowerpoint')
  of 'ppz'
    return ('application/mspowerpoint')
  of 'pre'
    return ('application/x-freelance')
  of 'prt'
    return ('application/pro_eng')
  of 'ps'
    return ('application/postscript')
  of 'qt'
    return ('video/quicktime')
  of 'ra'
    return ('audio/x-realaudio')
  of 'ram'
    return ('audio/x-pn-realaudio')
  of 'ras'
    return ('image/cmu-raster')
  of 'rgb'
    return ('image/x-rgb')
  of 'rm'
    return ('audio/x-pn-realaudio')
  of 'roff'
    return ('application/x-troff')
  of 'rpm'
    return ('audio/x-pn-realaudio-plugin')
  of 'rtf'
    return ('text/rtf')
  of 'rtx'
    return ('text/richtext')
  of 'saf'
    return ('application/safereader')
  of 'scm'
    return ('application/x-lotusscreencam')
  of 'set'
    return ('application/set')
  of 'sgm'
    return ('text/sgml')
  of 'sgml'
    return ('text/sgml')
  of 'sh'
    return ('application/x-sh')
  of 'shar'
    return ('application/x-shar')
  of 'silo'
    return ('model/mesh')
  of 'sit'
    return ('application/x-stuffit')
  of 'skd'
    return ('application/x-koan')
  of 'skm'
    return ('application/x-koan')
  of 'skp'
    return ('application/x-koan')
  of 'skt'
    return ('application/x-koan')
  of 'smi'
    return ('application/smil')
  of 'smil'
    return ('application/smil')
  of 'snd'
    return ('audio/basic')
  of 'sol'
    return ('application/solids')
  of 'spl'
    return ('application/x-futuresplash')
  of 'src'
    return ('application/x-wais-source')
  of 'step'
    return ('application/STEP')
  of 'stl'
    return ('application/SLA')
  of 'stp'
    return ('application/STEP')
  of 'sv4cpio'
    return ('application/x-sv4cpio')
  of 'sv4crc'
    return ('application/x-sv4crc')
  of 'svg'
  orof 'svgz'
    return ('image/svg+xml')
  of 'swf'
    return ('application/x-shockwave-flash')
  of 't'
    return ('application/x-troff')
  of 'tar'
    return ('application/x-tar')
  of 'tcl'
    return ('application/x-tcl')
  of 'tex'
    return ('application/x-tex')
  of 'texi'
    return ('application/x-texinfo')
  of 'texinfo'
    return ('application/x-texinfo')
  of 'tif'
    return ('image/tiff')
  of 'tiff'
    return ('image/tiff')
  of 'tr'
    return ('application/x-troff')
  of 'tsi'
    return ('audio/TSP-audio')
  of 'tsp'
    return ('application/dsptype')
  of 'tsv'
    return ('text/tab-separated-values')
  of 'txt'
    return ('text/plain')
  of 'unv'
    return ('application/i-deas')
  of 'ustar'
    return ('application/x-ustar')
  of 'vcd'
    return ('application/x-cdlink')
  of 'vda'
    return ('application/vda')
  of 'viv'
    return ('video/vnd.vivo')
  of 'vivo'
    return ('video/vnd.vivo')
  of 'vrml'
    return ('model/vrml')
  of 'wav'
    return ('audio/x-wav')
  of 'wrl'
    return ('model/vrml')
  of 'xbm'
    return ('image/x-xbitmap')
  of 'xlc'
  orof 'xll'
  orof 'xlm'
  orof 'xls'
  orof 'xlw'
  orof 'csv'
  orof 'xlsx'
    return ('application/vnd.ms-excel')
  of 'xml'
  orof 'xsl'
    return ('text/xml')
  of 'xpm'
    return ('image/x-xpixmap')
  of 'xwd'
    return ('image/x-xwindowdump')
  of 'xyz'
    return ('chemical/x-pdb')
  of 'zip'
    return ('application/zip')
  end
  return ''
!------------------------------------------------------------------------------

! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2012 CapeSoft Software  - www.capesoft.com


      Member()
         omit('***$***',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
         ***$***
      Include('Equates.CLW'),ONCE
      Include('Keycodes.CLW'),ONCE
      Include('Errors.CLW'),ONCE
      Map
      End ! map
      Include('NetWebSms.inc'),ONCE
   ! includes - NetMap Module Map
   include('NetMap.inc'),once

! Equates for the .status property
net:SmsIdle             equate(0)                           ! No command issued, nothing in progress
net:SmsSuccess          equate(1)                           ! A Message was sent successfully
net:SmsSending          equate(2)                           ! Busy sending
net:SmsNoData           equate(-1)                          ! A message was sent, but the reply contained no data
net:SmsError            equate(-2)                          ! An error occured, the statusText will contain the error message if there was one
net:SmsUnknown          equate(-3)                          ! The server replied with an invalid or unknown response
NetWebSms.BuildSms   PROCEDURE  (string apiId, string userName, string password, string sendTo, string text, string from) ! Declare Procedure 3
  CODE
  ! BaseUrl is something like: 'http://api.clickatell.com/http/sendmsg?api_id='
  self.Url = Clip(self.baseUrl) & Clip(apiId) |
           & '&user=' & self.EncodeWebString(userName) |
           & '&password=' & self.EncodeWebString(password) |
           & '&to=' & self.EncodeWebString(sendTo) |
           & '&text=' & self.EncodeWebString(text) |
           & '&from=' & self.EncodeWebString(from)
!------------------------------------------------------------------------------
NetWebSms.SendSMS    PROCEDURE  ()                         ! Declare Procedure 3
  CODE
    self.SetAllHeadersDefault()
    self.HeaderOnly = 0
    self.Fetch(self.Url)
!------------------------------------------------------------------------------
NetWebSms.PageReceived PROCEDURE  ()                       ! Declare Procedure 3
webResult               string(4096)
  CODE
    ! Page Received
    if self.PageLen <= 0
        self.status = net:SmsNoData
        self.statusText = 'No data was returned'
        return
    else
        webResult = self.Page[self.HeaderLen : self.PageLen]
    end

    case Sub(webResult, 2, 2)
    of 'ID'                                                 ! Successful
        self.status = net:SmsSuccess
        self.statusText = 'Message Sent'
    of 'ER'                                                 ! Error
        self.status = net:SmsError
        self.statusText =  'Error (' & Clip(webResult) & ')'
    else
        self.status = net:SmsUnknown
        self.statusText = 'Unknown or invalid server reply'
    end
!------------------------------------------------------------------------------

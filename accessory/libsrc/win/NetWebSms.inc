!ABCIncludeFile(NETTALK)
! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2012 CapeSoft Software  - www.capesoft.com
OMIT('_EndOfInclude_',_NetWebSms_)
_NetWebSms_ EQUATE(1)
! Generated by CapeSoft's Object Writer
  ! Includes (NetAll.inc & NetSimp.inc)
  include('NetAll.inc'),once
  include('NetSimp.inc'),once
  include('NetWww.inc'),once

!--------------------------------------------------------------------------------
!Class NetWebSms
!
!--------------------------------------------------------------------------------
NetWebSms            Class(NetWebClient),Type,Module('NetWebSms.Clw'),LINK('NetWebSms.Clw',_ABCLinkMode_),DLL(_ABCDllMode_)
! Properties
! Class Properties
url             string(8192)
baseUrl         string(8192)

status          long
statusText      string(256)
! Methods
BuildSms               PROCEDURE (string apiId, string userName, string password, string sendTo, string text, string from) ,VIRTUAL 
SendSMS                PROCEDURE () ,VIRTUAL 
PageReceived           PROCEDURE () ,VIRTUAL 
                     END ! Class Definition
!_EndOfInclude_

                MEMBER()
! (C) copyright 1996-2010 ProDomus, Inc.
! ALL RIGHTS RESERVED

Version         EQUATE('6.2-001')
                INCLUDE('PDLU.INC'),ONCE
                INCLUDE('KEYCODES.CLW'),ONCE
                INCLUDE('TPLEQU.CLW'),ONCE
                INCLUDE('PDLU.TRN'),ONCE

  MAP
    SLICE(*? StrToSlice,LONG StartPtr,LONG EndPtr=0),STRING
    COMPILE('@@',ClarioNETUsed)
      INCLUDE('CLARIONET.CLW'),ONCE
    ! @@

    !-- <<>> 9/27/04 - Add debug code.
    COMPILE('@@',_pdluDebug_)
    MODULE('Winapi')
      pdDebugBreak(),pascal,raw,name('debugbreak')
      pdOutputDebugString(*cstring),pascal,raw,name('OutputDebugStringA')
    END
    !@@

  END

!------------------------------------------------------------------------------
PDLuCT.Construct PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.PC &= NEW luPairsCT
  SELF.PC.Init
  SELF.PC.FillIfBlank &= SELF.FillIfBlank                            !-- <<>> 07/06/05 pw - Add
  SELF.PC.IsBlank &= SELF.IsBlank                                    !-- <<>> 07/06/05 pw - "
!------------------------------------------------------------------------------
PDLuCT.Destruct PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.Kill
  IF NOT SELF.PC &= NULL
    SELF.PC.Kill
    DISPOSE(SELF.PC)
    SELF.PC &= NULL
  END
!------------------------------------------------------------------------------
PDLuCT.Init PROCEDURE(? pRequest,<*?  pScreenField>,*? pLookup,VIEW pLookupView,FILE pLookupFile,KEY pLookupKey,<*? pIDToLookup>,<*? pIDToSave>,<KEY pIDKey>,<STRING pFilter>,<ULONG pFlags>,SIGNED pButtonFEQ=0,SIGNED pFeq)
!------------------------------------------------------------------------------
Parm ITEMIZE(2)
eWM                   EQUATE
eScreenField          EQUATE
eLookup               EQUATE
eLookupView           EQUATE
eLookupFile           EQUATE
eLookupKey            EQUATE
eIDToLookup           EQUATE
eIDToSave             EQUATE
eIDKey                EQUATE
eFilter               EQUATE
eFlags                EQUATE
eButtonFEQ            EQUATE
eFeq                  EQUATE
     END
  CODE
  SELF.InitServer(pRequest,pLookup,pLookupView,pLookupKey,pFeq)
  SELF.ButtonFEQ=pButtonFEQ
  SELF.File &= pLookupFile
  IF NOT OMITTED(eScreenField)
    SELF.ScreenField &= pScreenField
  END
  IF OMITTED(eFlags)
    SELF.IsRequired=1
    SELF.SetFlags(0)                                                 
  ELSE
!    SELF.IsRequired =CHOOSE(~BAND(pFlags,LU_NotRequired),1,0)       !-- <<>> 09/25/04 pw - Move to setflags
    SELF.SetFlags(pFlags)
  END
  IF OMITTED(eIDToLookup) OR OMITTED(eIDToSave) OR OMITTED(eIDKey)   !-- <<>> 06/28/03 - Moved from before to after setflags
    SELF.StuffSysId=FALSE
  ELSE
    SELF.InitID(pIDToLookup,pIDToSave,pIDKey)
  END
  IF NOT OMITTED(eFilter)
    SELF.SetFilter(pFilter)
  END
!------------------------------------------------------------------------------
PDLuCT.InitServer PROCEDURE(? pRequest,*? pLookup,VIEW pLookupView,KEY pLookupKey,SIGNED pFeq)
!------------------------------------------------------------------------------
  CODE
  IF COMMAND('/pdLuDebug')
    SELF.DebugOn=TRUE
  END
  SELF.Request=pRequest
  SELF.LookupField &= pLookup
  SELF.Feq=pFeq
  SELF.LookupView &= pLookupView
  SELF.LookupKey &= pLookupKey
!  SELF.SetAlerts
!------------------------------------------------------------------------------
PDLuCT.InitID PROCEDURE(*? pIDToLookup,*? pIDToSave,KEY pIDKey)
!------------------------------------------------------------------------------
! Added to allow PC to be private.
  CODE
  SELF.ID &= pIDToLookup                                             !-- <<>> 01/25/05 - Added to track value.
  SELF.SavedID &= pIdToSave
  SELF.StuffSysId=TRUE
  SELF.PC.AddPair(pIDToLookup,pIdToSave,SELF.DisableOnEdit)          !-- <<>> 06/28/03 - Add SELF.DisableOnEdit
  SELF.IdKey &= pIDKey
!------------------------------------------------------------------------------
PDLuCT.Open PROCEDURE
!------------------------------------------------------------------------------
SavLookup ASTRING
FilterOk  STRING(1)
  CODE
  SELF.KeyToOrder()
  OPEN(SELF.LookupView)                                              !-- <<>> 07/6/03 - Make sure it starts opened.
  SELF.SetOrder()
  SELF.SavRequest=SELF.Request                                       !-- <<>> 04/30/03 pw - To handle vcr request
  IF SELF.Request<>InsertRecord
!    IF SELF.StuffSysID OR NOT (SELF.Request=ChangeRecord AND SELF.DisableOnEdit) !-- <<>> 06/28/03 - Add disable on edit. <<>> 07/27/03 - Add sysid
    !-- <<>> 02/20/05 pw - Add view record
    IF SELF.StuffSysID OR NOT ((SELF.Request=ChangeRecord OR SELF.Request=ViewRecord) AND SELF.DisableOnEdit) !-- <<>> 06/28/03 - Add disable on edit. <<>> 07/27/03 - Add sysid
      IF NOT SELF.ScreenField &= NULL
        SELF.LookupField=SELF.ScreenField
        SELF.DebugOut('ScreenField Found','Open 1')
      END
      SavLookup=SELF.LookupField                                     !-- <<>> 04/30/03 pw - "
      SELF.SetFile
      SELF.DebugOut('Before AssignRighttoLeft','Open 1a')
      SELF.PC.AssignRighttoLeft                                      !-- Assign current values to lookups
      SELF.DebugOut('After AssignRighttoLeft','Open 1b')
      IF SELF.StuffSysID
        IF SELF.FileTryFetch()                                       !-- If error getting record.
          SELF.AssignRecord(1)                                       !-- ClearAll
          IF NOT SELF.IsRequired                                     !-- <<>> 06/28/03 - Restore field if not required.
            IF NOT SELF.ScreenField &=NULl
              SELF.ScreenField=SavLookup
              IF ~SELF.ScreenField
                SELF.IsBlank=TRUE                                    !-- <<>> 07/06/05 pw - Add property.
              END
            END
          ELSE
            SELF.IsBlank=TRUE                                        !-- <<>> 07/06/05 pw - Add property.
          END                                                        !-- <<>> 06/28/03 - " end restore field
          SELF.DebugOut('FileTryFetch Failed','Open 2')
        ELSE                                                         !-- No error getting record
          SELF.DebugOut('FileTryFetch Succeeded','Open 3')
           ! <<>> 09/02/04 pw - Remove evaluation.
!          IF NOT (SELF.Request=ChangeRecord AND SELF.DisableOnEdit)  !-- <<>> 07/27/03 - Add condtion
!            SELF.EvaluateID()
!          ELSE
            SELF.AssignRecord(0)                                     !-- <<>> 07/27/03 - Assign record.
!          END
        END
!      ELSIF NOT (SELF.Request=ChangeRecord AND SELF.DisableOnEdit)   !-- <<>> 07/27/03 - Add condtion.
      !-- <<>> 02/20/05 pw - Add viewrecord
      ELSIF NOT ((SELF.Request=ChangeRecord OR SELF.Request=ViewRecord) AND SELF.DisableOnEdit)   !-- <<>> 07/27/03 - Add condtion.
        IF SELF.LookupField<>''                                      !-- <<>> 05/23/03 - Check for lookup field value
          SELF.DebugOut('No SysID. Before Next','Open 4')
          SELF.SetFile()                                             !-- <<>> 07/9/03 - Set the file again.
!          NEXT(SELF.LookupView)                                     !-- <<>> 04/02/05 - Add validate Record
!          IF NOT ERRORCODE() AND SELF.LookupField=SavLookup         !-- <<>> 04/02/05 - "
          IF NOT SELF.Next() AND SELF.LookupField=SavLookup          !-- <<>> 04/02/05 - "
            SELF.AssignRecord(0)                                     !-- assignAll
          ELSE
            SELF.LookupField=SavLookup
            SELF.AssignRecord(1)                                     !-- <<>> 05/23/03 - Clear All
            IF NOT SELF.IsRequired                                   !-- <<>> 08/25/04 - Add to restore not required entry
              IF NOT SELF.ScreenField &=NULl                         !-- <<>> 08/25/04 - "
                SELF.ScreenField=SavLookup                           !-- <<>> 08/25/04 - "
                IF ~SELF.Screenfield                                 !-- <<>> 07/06/05 pw - added
                  SELF.IsBlank=TRUE                                  !-- <<>> 07/06/05 pw - "
                END                                                  !-- <<>> 07/06/05 pw - "
              ELSE                                                   !-- <<>> 07/06/05 pw - "
                SELF.IsBlank=TRUE                                    !-- <<>> 07/06/05 pw - "
              END                                                    !-- <<>> 08/25/04 - "
            END                                                      !-- <<>> 06/28/03 - " end restore field
          END
          SELF.DebugOut('No SysID. After Next','Open 5')
        ELSE                                                         !-- <<>> 05/23/03 - Clear All
          SELF.IsBlank=TRUE                                          !-- <<>> 07/06/05 pw - Add property
          SELF.DebugOut('Lookup blank, clear all','Open 6')
          SELF.AssignRecord(1)                                       !-- <<>> 05/23/03 - "
        END
      END
    ELSE
      IF SELF.ScreenField &= NULL                                      !-- <<>> 07/06/05 pw - Add check for blank entry.
        IF ~SELF.LookupField
          SELF.IsBlank=TRUE
        END
      ELSE
        IF ~SELF.ScreenField
          SELF.IsBlank=TRUE
        END
      END                                                              !-- <<>> 07/06/05 pw - End addition.
    END ! IF NOT DisableOnEdit
  ELSE ! Request <> Insert
    SELF.PC.AssignRighttoLeft                                        !-- <<>> 10/19/02
    SELF.PrimeRecord
    SELF.DebugOut('After PrimeRecord','Open 7')
  END
  SELF.Opened=TRUE
  SELF.DebugOut('End','Open 8')
!------------------------------------------------------------------------------
PDLuCT.KeyToOrder PROCEDURE !(KEY ThisKey,*CSTRING pOrder)           !-- <<>> 07/11/03 - Add method
!------------------------------------------------------------------------------
lField  SIGNED
I       SIGNED
  CODE
  IF NOT SELF.IsSql THEN RETURN.
  CLEAR(SELF.LookupOrder)
  LOOP I = 1 TO SELF.LookupKey{PROP:Components}
    lField = SELF.LookupKey{PROP:Field,I}
    SELF.LookupOrder = SELF.LookupOrder & CHOOSE(SELF.LookupOrder='','',',') & CHOOSE(SELF.LookupKey{PROP:Ascending,I}=TRUE,'+','-')
    lField = SELF.LookupKey{PROP:Field,I}
    SELF.LookupOrder = SELF.LookupOrder & SELF.File{PROP:Label,lField}
  END
!------------------------------------------------------------------------------
PDLuCT.FileTryFetch PROCEDURE !,BYTE,PROC,VIRTUAL                    !-- File Manager holder
!------------------------------------------------------------------------------
RV BYTE
  CODE
  SELF.DebugOut('Begin','FileTryFetch')
  ASSERT(NOT SELF.File &= NULL,'File not Assigned')
  GET(SELF.File,SELF.IDKey)
  IF ERRORCODE()
    RV=1
  END
  SELF.DebugOut('End','FileTryFetch')
  RETURN RV
!------------------------------------------------------------------------------
PDLuCT.EvaluateID PROCEDURE
!------------------------------------------------------------------------------
SavLookup ASTRING                                                    !-- <<>> 09/11/04 - Add required check.
  CODE
  SELF.DebugOut('Begin','EvaluateID')
  IF NOT BAND(SELF.Flags,LU_EvalOnReset) THEN RETURN.                !-- <<>> 11/03/04 - Add evaluation flag.
  IF SELF.LookupFilter
    CASE EVALUATE(SELF.LookupFilter)
?   OF ''
?     ASSERT(0,'Filter Error: '&SELF.LookupFilter)
    OF '0'
      DO r_Clear                                                     !-- <<>> 04/02/05 - Move to routine
    OF '1'
      DO r_Assign                                                    !-- <<>> 04/02/05 - Move to rotine
    END
  ELSE
    DO r_Assign                                                      !-- <<>> 04/02/05 - Move to routine
  END
  SELF.DebugOut('End','EvaluateID')
r_Clear ROUTINE
  IF NOT (SELF.Request=ChangeRecord AND SELF.DisableOnEdit)          !-- <<>> 11/02/04 - Add condition
    IF NOT SELF.IsRequired AND NOT SELF.ScreenField &= NULL          !-- <<>> 09/11/04 - Add required field check
      SavLookup=SELF.ScreenField                                     !-- <<>> 09/11/04 - "
    END
    SELF.AssignRecord(1)                                             !-- clearAll
    IF NOT SELF.IsRequired AND NOT SELF.ScreenField &= NULL          !-- <<>> 09/11/04 - "
      SavLookup=SELF.ScreenField                                     !-- <<>> 09/11/04 - "
    END                                                              !-- <<>> 09/11/04 - "
  END                                                                !-- <<>> 11/02/04 - Add condition
r_Assign ROUTINE
  IF SELF.ValidateRecord()                                           !-- <<>> 04/02/05 - Add validateRecord
    DO r_Clear                                                       !-- <<>> 04/02/05 - "
  ELSE                                                               !-- <<>> 04/02/05 - "
    SELF.AssignRecord(0)                                             !-- assingAll
  END                                                                !-- <<>> 04/02/05 - "
!------------------------------------------------------------------------------
PDLuCT.FileReset PROCEDURE !,VIRTUAL                                 !-- File Manager holder
!------------------------------------------------------------------------------
  CODE
  SELF.DebugOut('Begin','FileReset')
  ASSERT(NOT SELF.File &= NULL,'File not Assigned')
  ASSERT(NOT SELF.LookupView &= NULL,'View not Assigned')
  RESET(SELF.LookupView,SELF.File)
  ASSERT(NOT ERRORCODE(),'View Reset Error: '&ERROR())
  SELF.DebugOut('End','FileReset')
!------------------------------------------------------------------------------------
PDLuCT.Select PROCEDURE(SIGNED pFEQ=0,SIGNED p1=0,SIGNED p2=0)!,VIRTUAL
!------------------------------------------------------------------------------------
! Add to allow for ClarioNET override.
  CODE
  SELECT(pFEQ,p1,p2)
!------------------------------------------------------------------------------
PDLuCT.PrimeRecord PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.DebugOut('Begin','PrimeRecord')
  IF BAND(SELF.Flags,LU_ClearOnInsert) 
    SELF.AssignRecord(1) !ClearAll
  ELSE
    SELF.PC.AssignRightToLeft
    IF SELF.StuffSysID
      IF SELF.FileTryFetch()
        SELF.AssignRecord(1)                                         !-- clearAll
      ELSE
        SELF.FileReset()
        SELF.AssignRecord(0)                                         !-- assignAll
      END
    END
  END
  SELF.DebugOut('End','PrimeRecord')
!------------------------------------------------------------------------------
PDLuCT.AssignRecord PROCEDURE(BYTE pClearAll=0)
!------------------------------------------------------------------------------
  CODE
  SELF.DebugOut('Begin '&CHOOSE(~pClearAll,'(Assign)','(Clear)'),'AssignRecord')
  CASE pClearAll
  OF 0 ! AssignAll
    SELF.PC.AssignLeftToRight(SELF.Request)                          !-- Assign lookups to local fields.
    IF NOT SELF.ScreenField &= NULL
      IF SELF.ScreenField='' OR NOT (SELF.Request<>InsertRecord AND SELF.DisableOnEdit) !-- <<>> 06/27/03 - disable screenfield change flag.  <<>> 07/27/03 - Add Screenfield='' condition.
        IF SELF.LookupField='' AND NOT SELF.IsRequired               !-- <<>> 12/10/04 pw - Added
          !-- Leave entry value.                                     !-- <<>> 12/10/04 pw - "
        ELSE                                                         !-- <<>> 12/10/04 pw - "
          SELF.ScreenField=SELF.LookupField
        END                                                          !-- <<>> 12/10/04 pw - "
      END
    END
  OF 1 ! ClearAll
    SELF.PC.ClearLeft
    SELF.PC.ClearRight(SELF.Request)
    SELF.Lookupfield=''
    IF NOT SELF.ScreenField &= NULL
      IF NOT (SELF.Request<>InsertRecord AND SELF.DisableOnEdit)     !-- <<>> 06/27/03 - disable screenfield change flag
        IF NOT SELF.IsRequired                                       !-- <<>> 06/28/03 - Add condition
          SELF.ScreenField=''
        END
      END
    END
    SELF.Locator=''                                                  !-- <<>> 09/25/04 pw - Clear locator
  END
  SELF.DebugOut('End','AssignRecord')
  DISPLAY()
!------------------------------------------------------------------------------
PDLuCT.SetAlerts PROCEDURE
!------------------------------------------------------------------------------
l:Index  BYTE
  CODE
  SELF.DebugOut('Begin feq: '&SELF.ButtonFeq&' Width: '&SELF.ButtonFEQ{PROP:Width}&' Height: '&SELF.ButtonFeq{PROP:Height},'SetAlerts')
  IF NOT BAND(SELF.Flags,LU_ValidateOnAccept)                        !-- For ClarionNet Support
    ! Put here as the wrong keycode is currently transmitted by Clarionet
    SELF.Feq{PROP:Alrt,255}=AltDown
    SELF.Feq{PROP:Alrt,255}=UpKey
    SELF.Feq{PROP:Alrt,255}=DownKey
    SELF.Feq{PROP:Alrt,255}=CtrlUp                                   !-- <<>> 04/20/07 - Added
    SELF.Feq{PROP:Alrt,255}=CtrlDown                                 !-- <<>> 04/20/07 - Added
    SELF.Feq{PROP:Alrt,255}=CtrlV
    SELF.Feq{PROP:IMM}=TRUE
    COMPILE('@@',ClarioNETUsed)     !When ClarioNET used, do not use immediate, too much traffic is generated
      IF ClarioNETServer:Active()   !Only call this if we are running in the appbroker
        SELF.Feq{PROP:IMM}=FALSE    !No ClarioNET support for SelStart and SelEnd.  Would need to manual track these values
      END
    ! @@
  ELSE
    SELF.Feq{PROP:Alrt,255}=AltDown
    SELF.Feq{PROP:IMM}=FALSE
    SELF.ButtonFEQ{PROP:Skip}=FALSE
  END
  IF pdlu:Btn                                                        !-- <<>> 04/02/05 - Flag set in pdlu.trn file
    SELF.ButtonFeq{PROP:Icon}=pdlu:BtnIcon                           !-- "
    SELF.ButtonFeq{PROP:Text}=pdlu:BtnText                           !-- "
    SELF.ButtonFeq{PROP:Flat}=pdlu:BtnFlat                           !-- "
    SELF.ButtonFeq{PROP:Skip}=pdlu:BtnSkip                           !-- "
    SELF.ButtonFeq{PROP:Tip}=pdlu:BtnTip                             !-- "
    SELF.ButtonFeq{PROP:Msg}=pdlu:BtnMsg                             !-- "
    SELF.ButtonFeq{PROP:Width}=pdlu:BtnWidth                         !-- "
    SELF.ButtonFeq{PROP:Height}=pdlu:BtnHeight                       !-- "
    DISPLAY(SELF.ButtonFeq)                                          !-- "
  END
  SELF.DebugOut('End feq: '&SELF.ButtonFeq&' Width: '&SELF.ButtonFEQ{PROP:Width}&' Height: '&SELF.ButtonFeq{PROP:Height},'SetAlerts')
!------------------------------------------------------------------------------
PDLuCT.SetFlags PROCEDURE(ULONG pFlags=0)
!------------------------------------------------------------------------------
eFlags       EQUATE(1)
  CODE
  SELF.Initialized=TRUE
  IF pFlags                                                         !-- Set up beep
    SELF.isBeep = TRUE
    IF BAND(pFlags,LU_DefaultBeep)
      SELF.BeepChoice=BEEP:SystemDefault
    ELSIF BAND(pFlags,LU_HandBeep)
      SELF.BeepChoice=BEEP:SystemHand
    ELSIF BAND(pFlags,LU_QuestionBeep)
      SELF.BeepChoice=BEEP:SystemQuestion
    ELSIF BAND(pFlags,LU_ExclamationBeep)
      SELF.BeepChoice=BEEP:SystemExclamation
    ELSIF BAND(pFlags,LU_AsteriskBeep)
      SELF.BeepChoice=BEEP:SystemAsterisk
    ELSE
      SELF.IsBeep=FALSE
    END
    IF BAND(pFlags,LU_FillIfBlank)                                   !-- <<>> 07/06/05 pw - Add fill if blank.
      SELF.FillIfBlank=TRUE
    END
    IF BAND(pFlags,LU_PopUpOnError)
      SELF.PopUpOnError=TRUE
    ELSE
      SELF.PopUpOnError=FALSE
    END
    IF BAND(pFlags,LU_FeqReadOnly)
      SELF.FeqReadOnly=TRUE
    ELSIF BAND(pFlags,LU_FeqDisable)
      SELF.FeqDisable=TRUE
    END
    SELF.DisableOnEdit=CHOOSE(~BAND(pFlags,LU_DisableOnEdit),0,1)   !-- <<>> 06/10/03 - Add global flag
    SELF.IsSql=CHOOSE(~BAND(pFlags,LU_IsSql),0,1)                   !-- <<>> 07/14/03 - Add sql flag
    SELF.InactiveInvisible=CHOOSE(~BAND(pFlags,LU_InActiveInvisible),0,1) !-- <<>> 03/11/05 - Add sql flag
  ELSE
    SELF.PopUpOnError=FALSE
  END
  SELF.IsRequired =CHOOSE(~BAND(pFlags,LU_NotRequired),1,0)       !-- <<>> 09/25/04 pw - Move from init.
  SELF.Flags=pFlags
!-----------------------------------------------------------------------------
PDLuCT.TakeEvent  PROCEDURE
!------------------------------------------------------------------------------
RV BYTE
  CODE
  CASE EVENT()
  OF EVENT:OpenWindow
    SELF.DebugOut('Openwindow 1','TakeEvent')
    SELF.SetAlerts                                                   !- <<>> 05/7/03 pw - Moved from init.
    SELF.HideButton                                                  !- <<>> 06/27/03 - Move from open
    SELF.DebugOut('Openwindow 2','TakeEvent')
  END
  IF SELF.DisableOnEdit AND SELF.Request=ChangeRecord THEN RETURN RV.!-- <<>> 07/21/03 - Disable all
  CASE FIELD()
  OF SELF.Feq
    SELF.DebugOut('case Field 1','TakeEvent')
    RV=SELF.TakeFieldEvent()
    SELF.HideButton()                                                !-- <<>> 04/30/03 pw - change from reset
    SELF.DebugOut('case Field 2','TakeEvent')
  OF SELF.ButtonFEQ
    SELF.DebugOut('Button 1','TakeEvent')
    RV=SELF.TakeBTNFieldEvent()
    SELF.HideButton()                                                !-- <<>> 04/30/03 pw - "
    SELF.DebugOut('Button 2','TakeEvent')
  ELSE
    SELF.DebugOut('*****Other Field','TakeEvent')
    SELF.HideButton()                                                !-- <<>> 04/30/03 pw - added
    IF FIELD() AND SELF.HasFocus                                     !-- <<>> 10/28/02 Added IF FIELD()
      SELF.HasFocus=FALSE
      IF SELF.Feq{PROP:Imm}                                          !-- <<>> 11/2/02 IF Not immediate, takeaccepted is called from event accepted.
        IF CONTENTS(SELF.Feq)<>SELF.LastEntry
          SELF.LastEntry=CONTENTS(SELF.Feq)
          SELF.TakeAccepted                                          !-- <<>> 10/28/02 Added
        END
      END
    END
  END
  RETURN RV
!------------------------------------------------------------------------------
PDLuCT.TakeBtnFieldEvent PROCEDURE
!------------------------------------------------------------------------------
RV BYTE
  CODE
  CASE EVENT()
  OF EVENT:Accepted
    SELF.TakeBtnAccepted
  END
  RETURN RV
!------------------------------------------------------------------------------
PDLuCT.TakeFieldEvent PROCEDURE
!------------------------------------------------------------------------------
RV BYTE
  CODE
  IF NOT SELF.Feq{PROP:ReadOnly}                                     !-- <<>> 06/05/03 - Add readonly property (IF)
    CASE EVENT()                                 
    OF EVENT:Selected
      RV=SELF.TakeSelected()
    OF EVENT:NewSelection OROF EVENT:AlertKey                        !-- Treat both the same
      RV=SELF.TakeNewSelection()
    OF EVENT:Accepted
      SELF.TakeAccepted()
    END
    DISPLAY()
  END                                                                !-- <<>> 06/05/03 - end readonly mod.
  RETURN RV

!------------------------------------------------------------------------------
PDLuCT.TakeNewSelection PROCEDURE
!------------------------------------------------------------------------------
RV BYTE
  CODE
  SELF.DebugOut('Begin','TakeNewSelection')
  IF BAND(SELF.Flags,LU_ValidateOnAccept)
    IF EVENT()=EVENT:AlertKey
      POST(EVENT:Accepted,SELF.ButtonFEQ)
      RETURN RV
    ELSE
      RETURN RV
    END
  END
  SELF.BeforeLookup
  SELF.IsError=FALSE
  UPDATE(?)                                                        !-- Move screen to LookupField
  IF SELF.ScreenField &= NULL
    SELF.Locator = SELF.LookupField
  ELSE
    SELF.Locator = SELF.ScreenField                                !-- Save it
  END
  IF SELF.Feq{PROP:SelStart} <= LEN(CLIP(SELF.Locator))+SELF.Spaces !-- See if mid-word
    SELF.DebugOut('Midword','TakeNewSelection','Locator: '&CLIP(SELF.Locator)&' Start: '&SELF.Feq{PROP:SelStart})
    SELF.MidWord = True
    SELF.Spaces = SELF.FEQ{PROP:SelStart}-LEN(CLIP(SELF.Locator))
  ELSE
    SELF.MidWord=FALSE                                               !-- <<>> 10/06/04 - Clear midword.
  END
  IF SELF.TakeKey()
    ! Returns if left, home, or end key.  No change in entry.
    DISPLAY()
    SELF.DebugOut('TakeKey (Return)','TakeNewSelection')
    RETURN RV
  END
  IF NOT SELF.IsError
    SELF.SetSelects
    SELF.AssignRecord(0)                                           !-- AssignAll
    SELF.DebugOut('Is Not Error','TakeNewSelection')
  ELSE ! Is Error
!      SELF.TakeEntryError
    IF NOT SELF.IsRequired AND NOT SELF.ScreenField &= NULL        !-- <<>> 6/28/03 - Moved from old take entry error
      CLEAR(SELF.LookupField)
    END
    SELF.PC.ClearRight(SELF.Request)
    DISPLAY()
    SELF.DebugOut('Is Error','TakeNewSelection')
  END
  SELF.AfterLookup
  IF BAND(SELF.Flags,LU_ResetOnNewSelection)
    SELF.WindowReset()
  END
  SELF.DebugOut('End','TakeNewSelection')
  RETURN RV
!------------------------------------------------------------------------------
PDLuCT.WindowReset PROCEDURE
!------------------------------------------------------------------------------
! ABC calls window manager Reset(1)
  CODE
!------------------------------------------------------------------------------
PDLuCT.TakeSelected PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.DebugOut('Begin','TakeSeleced')
  SELF.HasFocus=TRUE
  SELF.LastEntry=CONTENTS(SELF.Feq)
  SELF.AtEnd = FALSE                                                 !-- Not at end of word
  SELF.FileIsSet=FALSE
  CLEAR(SELF.Spaces)
  RETURN 0
!------------------------------------------------------------------------------
PDLuCT.TakeAccepted PROCEDURE
!------------------------------------------------------------------------------
RV BYTE
SavLookup ASTRING
  CODE
  SELF.DebugOut('Begin','TakeAccepted')
  IF 0{PROP:AcceptAll}=TRUE THEN RETURN 0.                           !-- <<>> 07/27/03 - change <> to =
  ! For Clarionet support
  IF BAND(SELF.Flags,LU_ValidateOnAccept) AND 0{PROP:AcceptAll}<>TRUE AND SELF.IsRequired
    IF NOT SELF.ScreenField &= NULL
      SELF.LookupField=SELF.ScreenField
    END
    SavLookup=SELF.LookupField
    SELF.SetFile
!    NEXT(SELF.LookupView)
!    IF ERRORCODE()
    IF SELF.Next()                                                   !-- <<>> 04/02/05 - Add validate Record
      POST(EVENT:Accepted,SELF.ButtonFEQ)
    ELSE
      IF SELF.LookupField<>SavLookup
        POST(EVENT:Accepted,SELF.ButtonFEQ)
      ELSE
        SELF.AssignRecord(0)                                         !-- AssignAll
        SELF.AfterLookup()                                           !-- <<>> 03/2/03 Added
      END
    END
  ELSIF 0{PROP:AcceptAll}<>TRUE ! AND SELF.IsRequired   ! <<>> 11/2/02 FOR CLARIONET.  Remove code from template
    COMPILE('@@',ClarioNetUsed)
      IF ClarionetServer:Active()
        SELF.WindowUpdate()
        RV=SELF.TakeClarioNETAccepted()
      END
    !@@
  END
  ! End Clarionet support
  IF (BAND(SELF.Flags,LU_ResetOnAccepted) OR BAND(SELF.Flags,LU_ResetOnNewSelection)) AND 0{PROP:AcceptAll}<>TRUE  ! <<>> 11/2/02 Add New Seleciton
    SELF.WindowReset()
  END
  SELF.DebugOut('End','TakeAccepted')
  RETURN RV
!------------------------------------------------------------------------------
PDLuCT.WindowUpdate PROCEDURE
!------------------------------------------------------------------------------
! Place holder.  ABC calls thiswindow.update.
  CODE
!------------------------------------------------------------------------------
PDLuCT.TakeClarioNETAccepted PROCEDURE
!------------------------------------------------------------------------------
RV BYTE
SavLookup ASTRING      
  CODE
  SELF.DebugOut('Begin','TakeClarioNETAccepted')
  IF NOT SELF.ScreenField &= NULL
    SELF.LookupField=SELF.ScreenField
  END
  SavLookup=SELF.LookupField
  SELF.SetFile
!  NEXT(SELF.LookupView)                                             !-- <<>> 04/02/05 - Add validate Record
!  IF ERRORCODE()                                                    !-- <<>> 04/02/05 - "
  IF SELF.Next()                                                     !-- <<>> 04/02/05 - "
    IF SELF.IsRequired                                               !-- <<>> 11/04/02 Only if match is required.
      POST(EVENT:Accepted,SELF.ButtonFEQ)
    END
  ELSE
    IF SELF.IsRequired AND (INSTRING( UPPER(CLIP(SavLookup)), UPPER(CLIP(SELF.LookupField)), 1, 1 ) = 1 AND SELF.LookupField <> SavLookup)
      SELF.AssignRecord(0)                                           !-- <<>> 06/05/03 - Add
      SELF.Select(SELF.Feq)                          
      RV = 1 !Level:Notify                                           !-- notify the calling procedure to cycle the ACCEPT loop
    ELSIF UPPER(SELF.LookupField)<>UPPER(SavLookup)                  !-- <<>> 11/04/02 Add UPPER
      IF SELF.IsRequired                                             !-- <<>> 11/04/02 Add IF
        POST(EVENT:Accepted,SELF.ButtonFEQ)
      ELSE                                                           !-- <<>> 11/04/02 Add ELSE
        SELF.AssignRecord(1)                                         !-- <<>> 11/04/02 Add Clear 1=Clear
        SELF.LookupField=SavLookup                                   !-- <<>> 11/04/02 Save Values
        SELF.AfterLookup()                                           !-- <<>> 03/2/03 Added
        DISPLAY(SELF.Feq)                                            !-- <<>> 11/04/02 Save Values
      END
    ELSE
      SELF.AssignRecord(0)                                           !-- 0=AssignAll
      SELF.AfterLookup()                                             !-- <<>> 03/2/03 Added
    END
  END
  ! End Clarionet support
  ! Do a reset for ClarioNET on NewSelection or Accepted
  IF (BAND(SELF.Flags,LU_ResetOnNewSelection) OR BAND(SELF.Flags,LU_ResetOnAccepted)) AND 0{PROP:AcceptAll}<>TRUE
    SELF.WindowReset
  END
  SELF.DebugOut('End','TakeClarioNETAccepted')
  RETURN RV
!------------------------------------------------------------------------------
PDLuCT.TakeKey PROCEDURE
!------------------------------------------------------------------------------
RV BYTE
SavLookup ASTRING
  CODE
    SELF.DebugOut('Begin','TakeKey')
    CASE KEYCODE()                                                   !-- Case Keycode (First trap everything that is not a char)
    OF EnterKey                                                      !-- <<>> 07/16/09 - Will handle EnterKey when used as tab key.
      RV=1                                                           !-- <<>> 07/16/09 - 
    OF AltDown                                                       !--
      RV=1                                                           !-- <<>> 06/25/03 - Should cause calling procedure to cycle.
      IF ~SELF.ScreenField &= NULL                                   !-- <<>> 06/08/06 - Added.
        SELF.LookupField=SELF.ScreenField                            !-- <<>> 06/08/06 - "
      END                                                            !-- <<>> 06/08/06 - "
      CLEAR(SELF.Spaces)                                             !-- <<>> 06/08/06 - "
      SELF.CursorPos=LEN(CLIP(SELF.LookupField))+2                   !-- <<>> 06/08/06 - "
      POST(EVENT:Accepted,SELF.ButtonFEQ)
    OF HomeKey OROF LeftKey 
      SELF.AtEnd = False    
      IF SELF.Spaces
        IF KEYCODE()=LeftKey
          SELF.Spaces -= 1
        ELSE
          CLEAR(SELF.Spaces)
        END
      END
      RV=1
    OF EndKey          
      SELF.AtEnd = True
      RV=1
    OF RightKey     
      IF ?{PROP:SelStart} = LEN(CLIP(SELF.Locator)) + 1 + SELF.Spaces
        SELF.AtEnd = True
      ELSE
        SELF.AtEnd = False
      END
      RV=1
    OF UpKey OROF CtrlUp                                             !-- <<>> 04/20/07 - Added CtrlUp
      SELF.TakeUpKey
    OF DownKey OROF CtrlDown                                         !-- <<>> 04/20/07 - Added CtrlDown
      SELF.TakeDownKey
    OF MouseLeft2                               
      SELF.TakeMouseLeft2
    OF MouseLeft
      SELF.TakeMouseLeft
      RV=1
    OF CTrlV                                                         !-- <<>> 06/28/03 - Add clipboard function
      IF CLIPBOARD()<>''
        IF NOT SELF.ScreenField &= NULL
           SELF.ScreenField=CLIPBOARD()
        ELSE
          SELF.LookupField=CLIPBOARD()
        END
        DISPLAY(SELF.Feq)
        IF NOT (SELF.Request=ChangeRecord AND SELF.DisableOnEdit)
          IF NOT SELF.ScreenField &= NULL
            SELF.LookupField=SELF.ScreenField
          END
          SavLookup=SELF.LookupField
          SELF.PC.AssignRighttoLeft
          SELF.SetFile
          IF SELF.LookupField<>''
!            NEXT(SELF.LookupView)
!            IF NOT ERRORCODE() AND SELF.LookupField=SavLookup
            IF NOT SELF.Next()  AND SELF.LookupField=SavLookup       !-- <<>> 04/02/05 - Add validat record
              SELF.AssignRecord(0)                                   !-- assignAll
            ELSE
              !-- Lookup field not found
              SELF.LookupField=SavLookup
              IF SELF.IsRequired                                     !-- Clear in case lookup cancelled.
                IF NOT SELF.ScreenField &= NULL
                   CLEAR(SELF.ScreenField)
                ELSE
                  CLEAR(SELF.LookupField)
                END
              ELSE
                !-- dont clear if record is being changed and disable on edit is true
                IF NOT (SELF.DisableOnEdit AND SELF.Request=ChangeRecord) !-- <<>> 03/13/05 pw = Clear display fields.
                  SELF.AssignRecord(1)                               !-- <<>> 03/13/05 pw = Clear display fields.
                  IF NOT SELF.ScreenField &= NULL                    !-- <<>> 03/13/05 pw = "
                    SELF.ScreenField=SavLookup                       !-- <<>> 03/13/05 pw = Reassign screenfield as no match is required.
                  END                                                !-- <<>> 03/13/05 pw = "
                END                                                  !-- <<>> 03/13/05 pw = "
              END
              SELF.TakeEntryError()
            END
          ELSE
            SELF.AssignRecord(1)                                     !-- Clear all
          END
        END
      END
      RV=1
    ELSE
      SELF.TakeCharacter()
    END
    SELF.DebugOut('End','TakeKey')
    RETURN RV
!------------------------------------------------------------------------------
PDLuCT.TakeMouseLeft PROCEDURE
!------------------------------------------------------------------------------
  CODE
  CLEAR(SELF.Spaces)
!------------------------------------------------------------------------------
PDLuCT.TakeMouseLeft2 PROCEDURE
!------------------------------------------------------------------------------
  CODE
  IF ~SELF.ScreenField &= NULL                                       !-- <<>> 06/08/06 - Added
    SELF.LookupField=SELF.ScreenField                                !-- <<>> 06/08/06 - "
  END                                                                !-- <<>> 06/08/06 - "
  CLEAR(SELF.Spaces)                                                 !-- <<>> 06/08/06 - "
  SELF.CursorPos=LEN(CLIP(SELF.LookupField))+2                       !-- <<>> 06/08/06 - "
  POST(EVENT:Accepted,SELF.ButtonFEQ)
!------------------------------------------------------------------------------
PDLuCT.SetSelects PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.DebugOut('Begin','SetSelects')
  IF NOT SELF.ScreenField &= NULL
    SELF.Feq{PROP:SelEnd} = LEN(CLIP(SELF.ScreenField))+SELF.spaces  
    IF SELF.Feq{PROP:SelStart} = LEN(CLIP(SELF.ScreenField)) + 1 + SELF.spaces
      SELF.AtEnd = TRUE
    ELSE
      SELF.AtEnd = FALSE
    END
  ELSE
    SELF.Feq{PROP:SelEnd} = LEN(CLIP(SELF.LookupField))+SELF.Spaces
    IF SELF.Feq{PROP:SelStart} = LEN(CLIP(SELF.LookupField)) + 1 + SELF.spaces
      SELF.AtEnd = TRUE
    ELSE
      SELF.AtEnd = FALSE
    END
  END
  SELF.DebugOut('End','SetSelects')
!------------------------------------------------------------------------------
PDLuCT.TakeCharacter PROCEDURE
!------------------------------------------------------------------------------
lHoldEntry  ASTRING                                                  !-- <<>> 10/06/04 - To save positions for not isrequired entry
lHoldCursor SHORT
  CODE
  SELF.DebugOut('Begin','TakeCharacter')
  SELF.CursorPos = SELF.Feq{PROP:SelStart}

  IF NOT SELF.IsRequired
    IF NOT SELF.ScreenField &= NULL                                  !-- <<>> 10/06/04 - Add for non required entry
      lHoldEntry=SELF.ScreenField
    ELSE
      lHoldEntry=SELF.LookupField
    END
    lHoldCursor=SELF.CursorPos
  END

  IF SELF.CursorPos > 1
    IF KEYCODE() = BSKey                   
      SELF.DebugOut('Begin BSKey','TakeCharacter')
      IF SELF.Spaces
        SELF.Spaces -= 1
      END
      IF SELF.MidWord or SELF.AtEnd
        SELF.DebugOut('BS 1','TakeCharacter')
        IF NOT SELF.AtEnd
          SELF.CursorPos -= 1
        END
        IF SELF.CursorPos > 1  
          !SELF.Locator = SELF.Locator[1: SELF.CursorPos - 1]
          IF SELF.MidWord
            SELF.DebugOut('BS 2','TakeCharacter')
            SELF.Locator = SELF.Locator[1: SELF.CursorPos]  !& SELF.Locator[SELF.CursorPos+1 : LEN(CLIP(SELF.Locator))]  !-- <<>> 10/06/04 - Contatintate
          ELSE
            SELF.DebugOut('BS 3','TakeCharacter')
            SELF.Locator = SELF.Locator[1: SELF.CursorPos]             !-- <<>> 10/06/04 - double counted.
          END
        END                    
        SELF.DebugOut('BS 4','TakeCharacter')
      ELSE
        SELF.DebugOut('BS 5','TakeCharacter')
        SELF.CursorPos -= 1
        IF SELF.CursorPos > 1 THEN
          !SELF.Locator = SELF.Locator[1: SELF.CursorPos - 1]
          SELF.Locator = SELF.Locator[1: SELF.CursorPos]             !-- <<>> 10/06/04 - double counted.
        END                                                          
        SELF.DebugOut('BS 6','TakeCharacter')
      END
      SELF.DebugOut('End BSKey','TakeCharacter')
    END
    IF KEYCODE() = DeleteKey AND SELF.MidWord    
      !SELF.Locator = SELF.Locator[1: SELF.CursorPos - 1]            !-- <<>> 10/06/04 - Concatintate
      SELF.Locator =  SELF.Locator[1: SELF.CursorPos - 1] & SELF.Locator[SELF.CursorPos : LEN(CLIP(SELF.Locator))]  !-- <<>> 10/06/04 - Contatintate
      SELF.Spaces =   SELF.CursorPos - 1 -LEN(CLIP(SELF.Locator))
    END
    IF KEYCODE() = SpaceKey
      SELF.Spaces += 1
      IF NOT SELF.IsRequired
        ! SELF.IsError = TRUE                                        !-- <<>> 10/06/04 - Causes to not select
      END
    END
    SELF.LookupField=SELF.Locator
    IF NOT SELF.ScreenField &= NULL
      SELF.ScreenField = SELF.LookupField                            !-- Refresh if changed
    END
    SELF.DebugOut('After Assign Locator','TakeCharacter')
    IF SLICE(SELF.LookupField,LEN(SELF.LookupField))=CHR(0)          !-- <<>> 07/6/03 - Change to 1
      SELF.SetFile
      PREVIOUS(SELF.LookupView)                                      !-- Read record again
    ELSE
      SELF.LookupField=CLIP(SELF.LookupField)&CHR(0)                 !-- Set back a level
      SELF.SetFile
    END                                                              !-- Decrement last chr by 1
    SELF.SetFile                                                     !-- <<>> 07/6/03 - try
    SELF.DebugOut('Before Next','TakeCharacter')

    IF SELF.Next() OR |                                              !-- Handles required field fill with space.
    (UPPER(SLICE(SELF.LookupField,1,SELF.CursorPos-1)) ~= |
    UPPER(SELF.Locator[1: SELF.CursorPos - 1]) |
    AND SELF.Locator[1: SELF.CursorPos - 1])

      SELF.DebugOut('Before TakeEntryError','TakeCharacter')
      IF NOT SELF.IsRequired                                         !-- <<>> 10/06/04 - Add holdfield
        IF NOT SELF.ScreenField&=NULL
          SELF.ScreenField=lHoldEntry
        END
      ELSE
        IF SELF.LastLookup<>''
          SELF.LookupField=SELF.LastLookup
          SELF.SetFile
          NEXT(SELF.LookupView)                                      !-- Reget record
        END
      END
      SELF.TakeEntryError()                                          !-- <<>> 06/28/03 - Move code to new method.
      IF NOT SELF.IsRequired                                         !-- <<>> 10/06/04 - Add Holdfield
        SELF.CursorPos=lHoldCursor
      END
      SELF.DebugOut('Next Err','TakeCharacter')
    ELSE
      SELF.LastLookup=SELF.LookupField                               !-- <<>> 10/06/04 - record last successful lookup.
      SELF.DebugOut('Next No Err','TakeCharacter')
    END

  ELSE                                                               !-- Cursor at first position
    CLEAR(SELF.LookupField)                                          !-- Clear 
    SELF.PC.ClearLeft
  END

  SELF.PC.AssignLeftToRight(SELF.Request)
  IF NOT SELF.ScreenField &= NULL
    SELF.ScreenField=SELF.LookupField
    CHANGE(SELF.Feq,SELF.ScreenField)                                !-- Update screen to new record
  ELSE
    CHANGE(SELF.Feq,SELF.LookupField)
  END
  SELF.Feq{PROP:SelStart} = SELF.CursorPos                           !-- Start highlight here
  SELF.DebugOut('End','TakeCharacter')
!------------------------------------------------------------------------------
PDLuCT.TakeEntryError PROCEDURE                                      !-- <<>> 06/28/03 - New method
!------------------------------------------------------------------------------
SavScreen ASTRING                                                    !-- <<>> 03/13/05 pw = "
SavLocator ASTRING                                                   !-- <<>> 04/01/05 pw = Save if cleared.
  CODE
  SELF.DebugOut('Begin','TakeEntryError')
  IF SELF.IsRequired
    IF SELF.PopUpOnError
      IF SELF.IsBeep
        BEEP(SELF.BeepChoice)
      END
      POST(EVENT:Accepted,SELF.ButtonFEQ)
    ELSE
      IF SELF.isBeep
        BEEP(SELF.BeepChoice)
      END
      SELF.CursorPos -= 1                                            !-- Go back to last good char
      IF SELF.CursorPos = 1                                          !-- Fix if wrong first char
        CLEAR(SELF.LookupField)                                      !-- Clear name
        SELF.PC.ClearLeft
      ELSE                                                           !-- Back out char
        SELF.LookupField = SELF.Locator[1: SELF.CursorPos - 1]
        SELF.SetFile                                                 !-- <<>> 10/06/04 - Get the full entry
        NEXT(SELF.LookupView)                                        !-- <<>> 10/06/04 - "
        SELF.SetSelects
      END
    END
  ELSE
    SELF.IsError = TRUE
    IF NOT SELF.ScreenField &= NULL
      SavScreen=SELF.ScreenField                                     !-- <<>> 03/13/05 pw = Add to clear other fields.
      SavLocator=SELF.Locator                                        !-- <<>> 04/01/05 pw = "
!      !-- dont clear if record is being changed and disable on edit is true
      IF NOT (SELF.DisableOnEdit AND (SELF.Request=ChangeRecord OR SELF.Request=ViewRecord)) !-- <<>> 04/01/05 pw = Add viewrecord
        !-- <<>> 07/06/05 pw - The following line was commented out ????
        SELF.PC.ClearLeft()                                          !-- <<>> 07/06/05 pw - Change to clear left.
!        SELF.AssignRecord(1)                                         !-- <<>> 03/13/05 pw = Clear fields
      END
      SELF.ScreenField=SavScreen                                     !-- <<>> 03/13/05 pw = "
      SELF.LookupField=SELF.ScreenField
      SELF.Locator=SavLocator                                        !-- <<>> 04/01/05 pw = Add because cleared by self.Assignrecord(1)
      SELF.SetSelects                                                !-- <<>> 04/01/05 pw = "
    END
  END
  SELF.DebugOut('End','TakeEntryError')
!------------------------------------------------------------------------------
PDLuCT.TakeDownKey PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.DebugOut('Begin','TakeDownKey')
  CLEAR(SELF.Spaces)
  IF NOT SELF.ScreenField &= NULL
    SELF.LookupField=SELF.ScreenField
  END
  IF SELF.LookupField                                                !-- If something in field
    SELF.AfterCurrent                                                !-- Move so SET finds next
  ELSE                                                               !-- Field is empty
    CLEAR(SELF.LookupField,-1)
    SELF.SetFile
  END
  ! Test
!  NEXT(SELF.LookupView)                                             !-- <<>> 04/02/05 - Add validateRecord
!  IF ERRORCODE()                                                    !-- <<>> 04/02/05 - "
  IF SELF.Next()                                                     !-- <<>> 04/02/05 - "
    SELF.AssignRecord(1) !ClearALl
  ELSE                          ! record found
    SELF.AssignRecord(0) !AssignAll
  END
  IF NOT SELF.ScreenField &= NULL                                    !-- <<>> 04/20/07 - Added
    SELF.Locator=SELF.ScreenField                                    !-- <<>> 04/20/07 - "
  ELSE                                                               !-- <<>> 04/20/07 - "
    SELF.Locator=SELF.LookupField                                    !-- <<>> 04/20/07 - "
  END
  SELF.Feq{PROP:SelStart} = 1                                        !-- Start Selection at beginning
  SELF.DebugOut('End','TakeDownKey')
!------------------------------------------------------------------------------
PDLuCT.TakeUpKey PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.DebugOut('Begin','TakeUpeKey')
  CLEAR(SELF.Spaces)
  IF SELF.LookupField                                                !-- If something in field
    SELF.BeforeCurrent                                               !-- Move so SET finds previous
  ELSE                                                               !-- Field is empty
    CLEAR(SELF.LookupField,1)                                        !-- Set past last record
    SELF.SetFile
    SET(SELF.LookupView)                                             !-- <<>> 09/25/04 pw - set again
  END
!  PREVIOUS(SELF.LookupView)                                          !-- Read record
!  IF ERRORCODE()
  IF SELF.Previous()                                                 !-- <<>> 04/02/05 - Add validateRecord
    SELF.DebugOut('Error','TakeUpeKey')
    SELF.AssignRecord(1) !ClearAll
  ELSE                                                               !-- Record found
    SELF.AssignRecord(0) !AssignAll
  END
  IF NOT SELF.ScreenField &= NULL                                    !-- <<>> 04/20/07 - Added
    SELF.Locator=SELF.ScreenField                                    !-- <<>> 04/20/07 - "
  ELSE                                                               !-- <<>> 04/20/07 - "
    SELF.Locator=SELF.LookupField                                    !-- <<>> 04/20/07 - "
  END
  SELF.Feq{PROP:SelStart} = 1                                        !-- Start selection at beginning
  SELF.DebugOut('End','TakeUpeKey')
!------------------------------------------------------------------------------
PDLuCT.BeforeCurrent PROCEDURE
!------------------------------------------------------------------------------
FilePosition  STRING(1024)
  CODE
  SELF.DebugOut('Begin','BeforeCurrent')
  IF NOT SELF.ScreenField &= NULL
    SELF.LookupField=SELF.ScreenField                                !-- Current value
  END
  IF NOT SELF.FileIsSet AND NOT BAND(SELF.Flags,LU_SkipDuplicates)
    SELF.SetFile
    NEXT(SELF.LookupView)
  END
  FilePosition=POSITION(SELF.LookupView)
  IF FilePosition AND SELF.FileIsSet AND NOT BAND(SELF.Flags,LU_SkipDuplicates)
    REGET(SELF.LookupView,FilePosition)
  ELSE
    IF SLICE(SELF.LookupField,LEN(SELF.LookupField))=CHR(0)          !-- <<>> 07/6/03 - Change from 0
      SELF.SetFile
      PREVIOUS(SELF.LookupView)                                      !-- Read record again
    ELSE
      SELF.LookupField =SLICE(SELF.LookupField,1,LEN(SELF.LookupField)-1) & CHR(VAL(SLICE(SELF.LookupField,LEN(SELF.LookupField))) - 1)
      SELF.SetFile
    END                                                              !-- Decrement last chr by 1
  END
  SELF.DebugOut('End','BeforeCurrent')
!------------------------------------------------------------------------------
PDLuCT.AfterCurrent  PROCEDURE
!------------------------------------------------------------------------------
FilePosition STRING(1024)
SavNameToFind ASTRING
  CODE
  SELF.DebugOut('Begin','AfterCurrent')
  IF NOT SELF.FileIsSet AND NOT BAND(SELF.Flags,LU_SkipDuplicates)
    IF NOT SELF.ScreenField &= NULL
      SELF.LookupField=SELF.ScreenField                              !-- Current value
    END
    SELF.SetFile
    NEXT(SELF.LookupView)                                            !-- Set to beginning of group
    SavNameToFind=SELF.LookupField
    NEXT(SELF.LookupView)
    IF SELF.LookupField=SavNameToFind
      LOOP 2 TIMES
        PREVIOUS(SELF.LookupView)
      END
    ELSE
      PREVIOUS(SELF.LookupView)
    END
  END
  FilePosition=POSITION(SELF.LookupView)
  IF FilePosition AND SELF.FileIsSet AND NOT BAND(SELF.Flags,LU_SkipDuplicates)
    REGET(SELF.LookupView,FilePosition)
  ELSE
    IF SLICE(SELF.LookupField,LEN(SELF.LookupField)) = CHR(255)      !-- If can't go higher
    ELSE                                                             !-- Otherwise
      SELF.LookupField = SLICE(SELF.LookupField,1,LEN(SELF.LookupField)-1) & (CHR(VAL(SLICE(SELF.LookupField,LEN(SELF.LookupField))) + 1))
    END                                                              !-- Increment last chr by 1
    SELF.SetFile
  END
  SELF.DebugOut('End','AfterCurrent')
!------------------------------------------------------------------------------
PDLuCT.SetFile PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.DebugOut('Begin','SetFile')
  CLOSE(SELF.LookupView)
  SELF.SetFilter(SELF.LookupFilter,SELF.LookupSqlFilter)
  IF NOT SELF.IsSql
    SET(SELF.LookupKey,SELF.LookupKey)                               !-- <<>> 07/09/03 - Comment out - use setorder.
  END
  OPEN(SELF.LookupView)
  SELF.SetOrder()                                                    !-- Set SQL Order
  SELF.FileIsSet=TRUE
  SELF.DebugOut('End','SetFile')
!------------------------------------------------------------------------------
PDLuCT.SetOrder PROCEDURE                                            !-- <<>> 07/09/03 - New method.
!------------------------------------------------------------------------------
  CODE
  IF SELF.IsSql
    SELF.LookupView{PROP:Order}=SELF.LookupOrder
    SELF.SetSqlOrder()
    SET(SELF.LookupView,SELF.LookupKey{PROP:Components})
  END
!------------------------------------------------------------------------------
PDLuCT.SetSqlOrder PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.LookupView{PROP:SqlOrder}=SELF.LookupSqlOrder
!------------------------------------------------------------------------------
PDLuCT.SetSqlOrder PROCEDURE(STRING pSqlOrder)
!------------------------------------------------------------------------------
  CODE
  SELF.LookupSqlOrder=pSqlOrder
!------------------------------------------------------------------------------
PDLuCT.SetFilter PROCEDURE(STRING pFilter,STRING pSqlFilter)
!------------------------------------------------------------------------------
  CODE
  ASSERT(NOT (pFilter<>'' AND pSqlFilter<>''),'Only one entry should be made.')
  IF SELF.IsSql AND pFilter=''                                      !-- <<>> 05/03/05 - Added pFilter='' !-- <<>> 10/03/03 - Added
    SELF.LookupSqlFilter=pSqlFilter
    SELF.LookupView{PROP:SqlFilter}=SELF.LookupSqlFilter
    IF NOT SELF.LookupSqlFilter OR SELF.LookupView{PROP:SqlFilter}=''''''                          !-- <<>> 07/13/03/10/03/03 - SQL Workaround.  Add blank condition
      SELF.LookupView{PROP:SqlFilter}='(1=1)'                        !-- <<>> 07/13/03 - "
    END
  END                                                                !-- <<>> 10/03/03 - "
  SELF.SetFilter(pFilter)
  SELF.DebugOut('SqlFilter: '&SELF.LookupView{PROP:SqlFilter},'SetFilter')
  SELF.DebugOut('Filter: '&SELF.LookupView{PROP:SqlFilter},'SetFilter')
!------------------------------------------------------------------------------------
PDLuCT.SetFilter PROCEDURE(STRING pFilter)
!------------------------------------------------------------------------------------
  CODE
  SELF.LookupFilter=pFilter
  SELF.LookupView{PROP:Filter}=SELF.LookupFilter
!------------------------------------------------------------------------------------
PDLuCT.TakeBtnAccepted PROCEDURE    
!------------------------------------------------------------------------------------
SavePointer     STRING(256),auto
  CODE
  SELF.DebugOut('Begin','TakeBtnAccepted')
  SavePointer = POSITION(SELF.LookupView)
  SELF.BeforeLookup
  IF NOT SELF.ScreenField &= NULL
    SELF.LookupField=SELF.ScreenField                               !-- <<>> 10/06/04 - Use Locator.
    SELF.DebugOut('CursorPos: '&SELF.CursorPos,'TakeBtnAcccepted')
    IF SELF.Locator AND SELF.CursorPos                               !-- <<>> 04/20/07 - Add SELF.CursorPos
!      SELF.LookupField=SELF.Locator[1 : CHOOSE(SELF.CursorPos>2,SELF.CursorPos-2,SELF.CursorPos)] !-- <<>> 11/11/04 - Add CHOOSE.
      SELF.LookupField=SELF.Locator[1 : CHOOSE(SELF.CursorPos>2,SELF.CursorPos-1,SELF.CursorPos)] !-- <<>> 03/02/10 - from Bruce Cohen fix excape key.
    END

  END
  SELF.DebugOut('Before Assign','TakeBtnAccepted')
  SELF.PC.AssignRightToLeft                                          !-- <<>> 06/25/03 - Assign Lookup Fields
  SELF.DebugOut('After Assign','TakeBtnAccepted')
  IF SELF.CallLookup() = RequestCompleted                            !-- template generated
    SELF.AssignRecord(0) !AssignAll                                  !-- <<>> 07/13/03 - Move to before filereset
    SELF.FileReset()
    SELF.PC.AssignRightToLeft                                        !-- <<>> 07/13/03 - SQL workaround (reset changes value).
    SELF.AfterLookup
    SELF.FileIsSet=FALSE
  ELSE
    IF SavePointer<>''                                               !-- <<>> 02/15/06 - to avoid sql gpf
      REGET(SELF.LookupView,SavePointer)
      SELF.FileReset()                                                 !-- <<>> 06/25/03 - Reset pointer.
      NEXT(SELF.LookupView)                                            !-- <<>> 06/25/03 - Reget the record.
    END
    DISPLAY()
  END
  CLEAR(SELF.LastLookup)                                             !-- <<>> 10/06/04 - Added
  SELF.Select(SELF.FEQ)
  IF (BAND(SELF.Flags,LU_ResetOnNewSelection) OR BAND(SELF.Flags,LU_ResetOnAccepted)) AND 0{PROP:AcceptAll}<>TRUE
    SELF.WindowReset()
  END
  SELF.DebugOut('End','TakeBtnAccepted')
!------------------------------------------------------------------------------------
PDLuCT.CallLookup PROCEDURE
!------------------------------------------------------------------------------------
! Response returned set by templates in legacy.  ABC calls window manager run.
RV    BYTE
  CODE
  RETURN RV
!------------------------------------------------------------------------------------
PDLuCT.BeforeLookup PROCEDURE !,VIRTUAL
!------------------------------------------------------------------------------------
  CODE
!------------------------------------------------------------------------------------
PDLuCT.AfterLookup PROCEDURE !,VIRTUAL
!------------------------------------------------------------------------------------
  CODE
!------------------------------------------------------------------------------------
PDLuCT.Reset PROCEDURE(BYTE pForce=0) 
!------------------------------------------------------------------------------------
savLookup ASTRING                                                    !-- 09/11/04 - Added
  CODE
  SELF.DebugOut('Begin (Force='&pForce&')','Reset')
  !-- Handle recursive add
  IF SELF.SavRequest<>SELF.Request ! OR SELF.PositionChanged()
    SELF.Open()
    RETURN
  END
  !-- Reset button
  SELF.HideButton()
  !-- Reset lookup
  IF NOT (SELF.InactiveInvisible AND ~SELF.Feq{PROP:Visible})          !-- <<>> 03/10/05 pw - Added
    IF pForce
      IF SELF.StuffSysID
        SELF.PC.AssignRightToLeft
        IF SELF.FileTryFetch()
          IF NOT SELF.IsRequired AND NOT SELF.Screenfield &= NULL      !-- 09/11/04 - "
            SavLookup=SELF.Screenfield                                 !-- 09/11/04 - "
          END                                                          !-- 09/11/04 - "
          SELF.AssignRecord(1) !ClearAll                               !-- 09/11/04 - "
          CLEAR(SELF.Locator)
          IF NOT SELF.IsRequired AND NOT SELF.Screenfield &= NULL      !-- 09/11/04 - "
            SELF.Screenfield=SavLookup                                 !-- 09/11/04 - "
          END                                                          !-- 09/11/04 - "
        ELSE
          SELF.EvaluateID()                                            !-- <<>> 07/13/03 - Code moved to new method.
        END
        SELF.AfterLookup()                                             !-- <<>> 03/2/03 Added
      ELSE
      END
    END
  END
  SELF.DebugOut('End (Force='&pForce&')','Reset')
!------------------------------------------------------------------------------------
PDLuCT.HideButton PROCEDURE
!------------------------------------------------------------------------------------
  CODE
  IF SELF.Opened                                                     !-- <<>> 06/28/03 - Return if window not opened
    SELF.DebugOut('Start','Hidebutton')
    IF SELF.feqReadOnly AND SELF.Request=ChangeRecord AND NOT SELF.Feq{PROP:ReadOnly}
      SELF.Feq{PROP:ReadOnly}=TRUE                                   !-- <<>> 06/05/03 - Add readonly
    END
    IF SELF.feqDisable AND SELF.Request=ChangeRecord AND NOT SELF.Feq{PROP:Disable}
      SELF.Feq{PROP:Disable}=TRUE                                    !-- <<>> 06/05/03 - Add disable feq on edit
    END
    IF BAND(SELF.Flags,LU_HideButton)                                !-- <<>> 03/28/06 - Add hide button if flag is set to always hide.
      IF FIELD()=SELF.Feq                                            !-- <<>> 06/08/06 - JMS Suggestion to hide/unhide button.
        IF ~SELF.ButtonFEQ{PROP:Hide}=SELF.FEQ{PROP:Hide}            !--
          SELF.ButtonFEQ{PROP:Hide}=SELF.FEQ{PROP:Hide}                !-- <<>> 06/08/06 - "
        END
      ELSIF ~SELF.ButtonFEQ{PROP:Hide} AND ~SELF.ButtonFEQ{PROP:Hide}=TRUE                               !-- <<>> 06/08/06 - " (Change IF to ELSIF)
        SELF.ButtonFEQ{PROP:Hide}=TRUE                               !-- <<>> 03/23/06 pw - "
      END
    ELSE                                                             !-- <<>> 03/23/06 pw - "
      IF SELF.ButtonFEQ{PROP:Hide}<>SELF.Feq{PROP:Hide}              !-- <<>> 06/09/08 Add if statement.
        SELF.ButtonFEQ{PROP:Hide}=SELF.Feq{PROP:Hide}
      END
    END                                                              !-- <<>> 03/23/06 pw - "
    IF SELF.ButtonFEQ{PROP:Disable}<>SELF.Feq{PROP:Disable}
      !-- <<>> 06/09/08 Add if clause for viewing record while button is disabled.
      IF SELF.Feq{PROP:ReadOnly} AND ~SELF.ButtonFeq{PROP:Disable}=TRUE
        SELF.ButtonFEQ{PROP:Disable}=SELF.Feq{PROP:Disable}
      END
    END
    IF (SELF.Request=ChangeRecord AND SELF.DisableOnEdit) AND NOT SELF.ButtonFeq{PROP:Disable} !-- <<>> 06/28/03 - Disable button if editing disabled.
      DISABLE(SELF.ButtonFeq)                                        !-- <<>> 06/28/03 - "
    END
    IF SELF.Feq{PROP:ReadOnly} AND NOT SELF.ButtonFeq{PROP:Disable}  !-- <<>> 06/05/03 - Add readonly.
      SELF.ButtonFeq{PROP:Disable}=TRUE                              !-- <<>> 06/05/03 - "
    END                                                              !-- <<>> 06/05/03 - "
    IF SELF.AddEditFeq
      IF SELF.ButtonFeq{PROP:Hide}
        IF ~SELF.AddEditFeq{PROP:Hide}
          SELF.AddEditFeq{PROP:Hide}=TRUE
        END
      END
      IF SELF.ButtonFeq{PROP:Disable}
        IF ~SELF.AddEditFeq{PROP:Disable}
          SELF.AddEditFeq{PROP:Disable}=TRUE
        END
      END
    END
    SELF.DebugOut('Start','Hidebutton')
  END
!------------------------------------------------------------------------------------
PDLuCT.AddDisplay PROCEDURE(*? pLookup,<*pDisplay>,BYTE pDisableOnEdit=0,BYTE pFillIfBlank=0)
!------------------------------------------------------------------------------------
  CODE
  ASSERT(NOT SELF.PC &= NULL,'Display Q not initialized')
  pDisableOnEdit=CHOOSE(~BAND(SELF.Flags,LU_DisableOnEdit),pDisableOnEdit,1)  !-- Set to disable if global value set to disableOnEdit.
  IF NOT OMITTED(3)                                                  
    SELF.PC.AddPair(pLookup,pDisplay,pDisableOnEdit,pFillIfBlank)    !-- <<>> 07/17/05 - Add FillIfblank
  ELSE
    SELF.PC.AddItem(pLookup,pDisableOnEdit)
  END
!------------------------------------------------------------------------------------
PDLuCT.Kill PROCEDURE
!------------------------------------------------------------------------------------
  CODE
!------------------------------------------------------------------------------------
PDLuCT.Version PROCEDURE
!------------------------------------------------------------------------------------
  CODE
  RETURN(Version)
!------------------------------------------------------------------------------------
PDLuCT.DebugOut PROCEDURE(STRING pMessage,STRING pProc,<STRING pData>)
!------------------------------------------------------------------------------------
  COMPILE('@@',_pdluDebug_)
lMessage CSTRING(255)
lBase    STRING(40)
eDataParm EQUATE(4)
  !@@
  CODE
  COMPILE('@@',_pdluDebug_)
    IF NOT SELF.DebugOn THEN RETURN.
    lBase='`'&CLIP(pProc)&'-'&pMessage
    lMessage = lBase&' LU-'&SELF.LookupField
    IF NOT SELF.ScreenField &= NULl
      lMessage=CLIP(lMessage)&' SF-'&SELF.ScreenField
    END
    IF SELF.Locator
      lMessage=CLIP(lMessage)&' LO-'&SELF.Locator
    END
    IF NOT OMITTED(eDataParm)
      lMessage=CLIP(lMessage) & pData
    END
    lMessage=CLIP(lMessage)&' W-'&SELF.ButtonFEQ{PROP:Width}&' C-'&SELF.CursorPos&' SP-'&SELF.Spaces&' MID-'&SELF.MidWord&' ERR-'&ERROR()&CHOOSE(NOT SELF.ID &= NULL,' ID-'&SELF.ID&' Saved-'&SELF.SavedID,'')
    pdOutputDebugString(lMessage)
  !@@
!------------------------------------------------------------------------------------
PDLuCT.Next PROCEDURE
!------------------------------------------------------------------------------------
! Returns true if error or record out of range.
! Goes to next record if filtered.
  CODE
  LOOP
    NEXT(SELF.LookupView)
    IF ERRORCODE()
      RETURN 1
      BREAK
    ELSE
      CASE SELF.ValidateRecord()
      OF 0
        RETURN Record:Ok
      OF Record:Filtered
        CYCLE
      OF Record:OutOfrange
        RETURN 1
      END
    END
  END
!------------------------------------------------------------------------------------
PDLuCT.Previous PROCEDURE
!------------------------------------------------------------------------------------
! Returns true if error or record out of range.
! Goes to next record if filtered.
  CODE
  LOOP
    PREVIOUS(SELF.LookupView)
    IF ERRORCODE()
      RETURN 1
      BREAK
    ELSE
      CASE SELF.ValidateRecord()
      OF 0
        RETURN Record:Ok
      OF Record:Filtered
        CYCLE
      OF Record:OutOfrange
        RETURN 1
      END
    END
  END
!------------------------------------------------------------------------------------
PDLuCt.ValidateRecord PROCEDURE !Byte
!------------------------------------------------------------------------------------
  CODE
  RETURN Record:Ok
!------------------------------------------------------------------------------------
SLICE FUNCTION(*? StrToSlice,LONG StartPtr,LONG EndPtr=0)
!------------------------------------------------------------------------------------
S     STRING(1000)                                                   !-- <<>> 07/6/03 - Increase size from 256
  CODE
  IF StartPtr < 1
    StartPtr = 1
  END
  IF EndPtr < StartPtr ! = 0
    EndPtr=StartPtr
  ELSIF EndPtr > 1000
    EndPtr = 1000
  END
  S=StrToSlice
  RETURN S[StartPtr : EndPtr]
!------------------------------------------------------------------------------------
luPairsCT.AddItem PROCEDURE(*? Left,BYTE pDisableOnEdit=0)
!------------------------------------------------------------------------------------
  CODE
  ASSERT(~(SELF.List &= NULL))
  CLEAR(SELF.List)
  SELF.List.Left &= Left
  SELF.List.Right = Left
  SELF.List.DisableOnEdit=pDisableOnEdit
  ADD(SELF.List)
!------------------------------------------------------------------------------------
luPairsCT.AddPair PROCEDURE(*? Left,*? Right,BYTE pDisableOnEdit=0,BYTE pFillIfBlank=0)
!------------------------------------------------------------------------------------
! Left is lookup
! Right is entry/display field
  CODE
  ASSERT(~(SELF.List &= NULL))
  CLEAR(SELF.List)
  SELF.List.Left &= Left
  SELF.List.Right &= Right
  SELF.List.DisableOnEdit=pDisableOnEdit
  SELF.List.FillIfBlank=pFillIfBlank
  IF SELF.FillIfBlank
    SELF.List.IsBlank=CHOOSE(~SELF.List.Right,1,0)                   !-- <<>> 07/17/05 - True if field is blank.
  END
  ADD(SELF.List)
!------------------------------------------------------------------------------------
luPairsCT.AssignLeftToRight PROCEDURE(BYTE pRequest)
!------------------------------------------------------------------------------------
 !-- Assign lookup to fields to Display/Update Fields
I UNSIGNED,AUTO
  CODE
  LOOP I = 1 TO RECORDS(SELF.List)
    GET(SELF.List,I)
    IF SELF.List.DisableOnEdit AND pRequest<>InsertRecord
      IF ~(SELF.IsBlank and SELF.FillIfBlank)                      !-- <<>> 07/06/05 pw - Add
        IF ~(SELF.List.FillIfBlank AND SELF.List.IsBlank)             !-- <<>> 07/17/05 - Added.
          CYCLE
        END
      END
    END
    SELF.List.Right = SELF.List.Left
    PUT(SELF.List)
  END
!------------------------------------------------------------------------------------
luPairsCT.AssignRightToLeft PROCEDURE
!------------------------------------------------------------------------------------
!-- Assign entries/display fields to lookups.
I UNSIGNED,AUTO
  CODE
  LOOP I = 1 TO RECORDS(SELF.List)
    GET(SELF.List,I)
    SELF.List.Left = SELF.List.Right
    PUT(SELF.List)
  END
!------------------------------------------------------------------------------------
luPairsCT.ClearLeft PROCEDURE
!------------------------------------------------------------------------------------
!-- Clear lookup fields
I UNSIGNED,AUTO
  CODE
  LOOP I = 1 TO RECORDS(SELF.List)
    GET(SELF.List,I)
    CLEAR(SELF.List.Left)
  END
!------------------------------------------------------------------------------------
luPairsCT.ClearRight PROCEDURE(BYTE pRequest)
!------------------------------------------------------------------------------------
!-- Clear entry/display fields.
I UNSIGNED,AUTO
  CODE
    LOOP I = 1 TO RECORDS(SELF.List)
      GET(SELF.List,I)
      IF SELF.List.DisableOnEdit and pRequest<>InsertRecord
        IF ~(SELF.IsBlank and SELF.FillIfBlank)                      !-- <<>> 07/06/05 pw - Add
          IF ~(SELF.List.FillIfBlank AND SELF.List.IsBlank)          !-- <<>> 07/17/05 - Added.
            CYCLE
          END
        END
      END
      CLEAR(SELF.List.Right)
    END
!------------------------------------------------------------------------------------
luPairsCT.Kill PROCEDURE
!------------------------------------------------------------------------------------
  CODE
  IF ~SELF.List &= NULL
    GET(SELF.List,1)
    LOOP WHILE ~ERRORCODE()
      SELF.List.Left &= NULL
      SELF.List.Right &= NULL
      GET(SELF.List,POINTER(SELF.List)+1)
    END
    DISPOSE(SELF.List)
  END
!------------------------------------------------------------------------------------
luPairsCT.Init PROCEDURE
!------------------------------------------------------------------------------------
  CODE
  SELF.Kill
  SELF.List &= NEW luPairsQT

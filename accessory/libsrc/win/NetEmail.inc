!ABCIncludeFile(NETTALK)
! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software  - www.capesoft.com
Omit('_EndOfInclude_',_NetEmail_)
_NetEmail_ EQUATE(1)
  include('NetAll.inc'),once
  include('NetSimp.inc'),once



! Sep09: Embeds queue for processing embeds when sending mail
Net:EmbedsQType     queue, type
filePath                string(File:MaxFileName)
fileName                string(File:MaxFileName)
                    end

! _objectType for Email and New objects
                    itemize(), pre(NET)
MAIL_CLIENT             equate()            ! Not set, an instance of the base class
POP_CLIENT              equate()
SMTP_CLIENT             equate()
NNTP_CLIENT             equate()
IMAP_CLIENT             equate()
                    end

! Default port values
NET:IMAP_PORT       equate(143)
NET:POP_PORT        equate(11)
NET:SMTP_PORT       equate(25)
NET:NNTP_PORT       equate(119)

!-----------------------------------------------------------
!       Base class for NetEmailReceive (POP Client)
!               and NetIMAP (IMAP client)
!-----------------------------------------------------------
NetEmail            Class(NetSimple),Type,Module('NetEmail.Clw'),LINK('NetEmail.Clw',_ABCLinkMode_),DLL(_ABCDllMode_)
! Object Properties
user                   string(NET:StdAUTHLoginSize)
password               string(NET:StdAUTHLoginSize)
server                 string(252)
port                   NET:PortType
_Spacer1               NET:PortType

! Message Properties
msgNumber              long                          ! These are returned by NET:EmailDownload mode
bytes                  long
UID                    string(NET:StdEmailUIDSize)   ! Unique ID
toList                 string(NET:StdEmailListSize)  ! Comma separated e.g. Jono Woodhouse <jono@capesoft.com>, Bruce Johnson <bruce@capesoft.com>
ccList                 string(NET:StdEmailListSize)
from                   string(NET:StdEmailFromSize)
subject                string(NET:StdEmailSubjectSize)
replyTo                string(Net:StdEmailFromSize)
organization           string(Net:StdEmailFromSize)
deliveryReceiptTo      string(Net:StdEmailFromSize)
dispositionNotificationTo string(Net:StdEmailFromSize)
sentDate               string(NET:StdEmailSentDateSize)
messageID              string(Net:StdEmailMessageIDSize)  ! Message-ID
references             string(Net:StdEmailReferencesSize) ! References
userFilterContent      string(NET:StdEmailListSize)
attachmentList         string(NET:StdEmailAttachmentListSize)
attachmentPath         string(NET:StdEmailPathSize)
attachmentListQ        &NET:EmailAttachmentQType               ! New 25/4/2002
embedList              string(NET:StdEmailAttachmentListSize)  ! A list of files (images) that are embedded into the HTML code
embedListQ             &NET:EmailAttachmentQType               ! New 25/4/2002
messageText            &string                       ! Use free() to free this
messageHtml            &string
wholeMessage           &string
messageTextLen         long
messageHtmlLen         long
wholeMessageLen        long

textCharSet            string (NET:StdEmailCharSetSize)
htmlCharSet            string (NET:StdEmailCharSetSize)
mimeVersion            string (NET:StdEmailMimeVersionSize)
contentType            string (NET:StdEmailContentTypeSize)
contentDisposition     string (NET:StdEmailContentTypeSize)
contentTransferEncoding string (NET:StdEmailContentTransferEncodingSize)
mimeBoundary            string (NET:StdEmailMimeBoundarySize)
mimeAltBoundary        string (NET:StdEmailMimeBoundarySize)
mimeMixedBoundary      string (NET:StdEmailMimeBoundarySize)
mimeRelBoundary        string (NET:StdEmailMimeBoundarySize)
deliveryStatusOriginalRecipient string (NET:StdEmailStringSize)
deliveryStatusFinalRecipient string (NET:StdEmailStringSize)
deliveryStatusAction   string (NET:StdEmailStringSize)      ! "failed" / "delayed" / "delivered" / "relayed" / "expanded"
deliveryStatusStatus   string (NET:StdEmailStringSize)
deliveryStatusRemoteMTA string (NET:StdEmailStringSize)
deliveryStatusDiagnosticCode string (NET:StdEmailStringSize)
deliveryStatusLastAttemptDate string (NET:StdEmailStringSize)

emailsListQ            &Net:EmailsListQType
emailsDoneBeforeQ      &Net:EmailsDoneBeforeQType

serverErrorDesc         string (252)

! Options
optionsDontProcessWholeMessage  long                        ! If set only .wholeMessage will be populated (it will not be split)
optionsUserFilter      cstring (80)                         ! A user defined header filter (e.g. 'X-VARIABLE') - this is put in self.UserFilterContent after asking for more options in Decide() - Must be in capitals
optionsDontSaveAttachments long
optionsDontSaveEmbeds  long
optionsOnlyProcessHeader long                               ! Only the header part of the email will be processed ie split into things like toList, From, Subject etc.
optionsIncludeTextAttachmentsInText long                    ! Set this to 1 if you want the text attachments added to the text portion of the email


!--- Internal Properties
_state                 long                                 ! Do not change any of these variables starting with _
_command               long
_firstFlag             long
_firstFlagList         long
_currentMessageNum     long
_downloadStage         long
_firstPacket1          long

_serverDesc            cstring(80)
_serverDesc_Set        long
_objectType            long(0)                              ! NET:MAIL_CLIENT, NET:POP_CLIENT, NET:SMTP_CLIENT, NET:NNTP_CLIENT, NET:IMAP_CLIENT

_endNotFound           long

_attachmentListQ       &NET:EmailAttachmentQType            ! Original Queue
_embedListQ            &NET:EmailAttachmentQType            ! Original Queue
_emailsListQ           &Net:EmailsListQType                 ! Original Queue
_emailsDoneBeforeQ     &Net:EmailsDoneBeforeQType           ! Original Queue

_listCache             string(Net:MaxBinData*2)             ! Cache for the incoming List (All) data
_listCacheLen          long                                 ! Cache for the incoming List (All) data

_MDContentType         string(NET:StdEmailContentTypeSize)  ! MD = MimeDescription
_MDCharSet             string(NET:StdEmailCharSetSize)
_MDContentTransfer     string(NET:StdEmailContentTypeSize)
_MDContentBoundary     string(NET:StdEmailMimeBoundarySize)
_MDContentDisposition  string(80)
_MDContentID           string(252)
_MDFileName            string(252)
_MDName                string(252)

_serverReplyBuffer     string(NET:EmailReplyBufferSize)
_serverReplyBufferLen  long

_last5Bytes            string(5)
_spacer2               string(3)
_last5BytesLen         long

! --------------------------------------------------------------------------------------------------------
!--- Methods
Ask                    Procedure (long p_command), virtual
Decide                 Procedure (long mode), virtual
Done                   Procedure (long command, long finished), virtual

ConnectionClosed       Procedure (), virtual
Init                   Procedure (uLong Mode=NET:SimpleClient), virtual
Open                   Procedure (string Server,uShort Port=0), virtual
Close                  Procedure (), virtual
Process                Procedure (), virtual

ErrorTrap              Procedure (string errorStr, string functionName), virtual
Free                   Procedure (), virtual
Kill                   Procedure (), virtual

IsType                 Procedure (), string, virtual
GetData                Procedure (), string, virtual
SetPacketData          Procedure (string data, long dataLen = 0), long, proc, virtual
DataLen                Procedure (), long, virtual
ProcessEML             Procedure (String pFileName),Virtual

SendAllData            Procedure(*string data, long dataLen=0), virtual

!--- Internal Methods
_TryOpen               Procedure (), virtual
_CloseIdleConnection   Procedure (), virtual
_BufferReply           Procedure (), virtual
_GetBufferedData       Procedure (), virtual

! MIME and text processing
_SplitParam            Procedure (*string p_String, long p_Number,byte p_Separator=32), string, virtual
_SplitParam2           Procedure (*string p_String, long p_Number,byte p_Separator,byte p_IgnoreRepeatedSeparators), string, virtual
_ProcessResizeWholeMessage Procedure (long p_newRequiredSize), long, virtual
_ProcessSetSizes           Procedure (long p_TextLen, long p_HTMLLen, long p_WholeLen), long, virtual
_ProcessSplitWholeMessage  Procedure (), long, proc, virtual
_ProcessHeader             Procedure (Long Startpos, Long BodyPos), virtual
_ExtractHeader             Procedure (String pWhat,String pHeader, Long pClean=true),String, virtual
_ExtractHeaderPart         Procedure (String pWhat,String pStr),String, virtual
_ProcessHeaderParts        Procedure(*string pOriginal,<*String pBoundary>, <*String pName>, <*String pCharset>, <*String pFileName>) ,virtual

_ProcessBody               Procedure(),Long, virtual
_ProcessParts              Procedure(String pParts,String pBoundary,Long pStart),Long, virtual
_ProcessPart               Procedure(String pPart),Long, virtual
_ProcessBodyNoContentType  Procedure(Long BodyPos),Long, virtual
_AddToEmbedListQueue       Procedure(*String pFileName,String pContentType,String pContentTransfer), virtual
_AddToAttachmentListQueue  Procedure(*String pFileName), virtual

_ProcessClearData      Procedure (), virtual
_ProcessStoreText      Procedure (string pStr,string pContentTransfer, string pCharset), long, virtual
_ProcessStoreHTML      Procedure (string pStr,string pContentTransfer, string pCharset), long, virtual
_ProcessStoreAttachment Procedure (string pStr,String pContentTransfer,String pFileName,String pContentId,String pName,String pContentType, String pContentDisposition), long, virtual
_ProcessRaiseError     Procedure (string p_ErrorStr), long, proc, virtual
_ProcessSendIt         Procedure (), virtual
_GetLineEntry          Procedure (string p_SearchString,*string p_MainString, long p_StartPos, long p_EndPos, long p_Offset, long p_CaseInSensitive), string, virtual
_ProcessCleanString    Procedure (string p_string, long p_mode), string, virtual
_ProcessUUDecode       Procedure (String pStr), long, virtual
_SaveFile              Procedure (*string writeString, string fileName, long dataLen=0),long, virtual
_CleanFileName         Procedure (string p_InString), string, virtual

_ProcessDeliveryStatus Procedure (long p_endMDPos, long p_NextBoundaryPos), long, virtual
                    end


!--------------------------------------------------------------------------------
!Class NetEmailReceive
!
!--------------------------------------------------------------------------------
NetEmailReceive      Class(NetEmail),Type,Module('NetEmail.Clw'),LINK('NetEmail.Clw',_ABCLinkMode_),DLL(_ABCDllMode_)

! --- Properties
totalMessagesCount      long                                ! Returned after NET:EmailCountAll mode
totalBytesCount         long                                ! Returned after NET:EmailCountAll mode
totalBytesReceived      long
totalBytesToGo          long
progress1000            long
progressControl         long

! --- Options for modifying behaviour
optionsDelete           long                                ! Delete messages from POP server after download
optionsDeleteAfterHours long                                ! Must be used in conjunction with self.OptionsUseDoneBeforeQ
optionsKeepOpen         long                                ! Keep connection open after ask method called
decideAction            long                                ! Set in Decide() method
optionsUseDoneBeforeQ   long                                ! Set when you want to be able to not delete from the server, but then not get the emails you've downloaded before.

optionsNewBatchEveryXMessages long                          ! Only get x number of messages at a time.
optionsNewBatchEveryXBytes long                             ! Only get x number of bytes at a time, after the x number of bytes have been downloaded and the current email downloaded, the connection to the POP3 Server is resumed.
optionsUIDLOneOnly      long                                ! Batch Download Optimization

optionsJustGetEmailsListQ long                              ! Just get the EmailsListQ - needs Ask(NET:EmailDownload)
optionsUsePreviousEmailsListQ long                          ! This will use the ToDelete and ToDownload information in the previous EmailsListQ

!--- Internal Properties
_zeroByteFixDisable     long
_messagesThisBatch      long
_bytesThisBatch         long
_positionInNextBatch    long
_batchResume            long                                ! indicates if a batch resume is happening
_batchListHasChanged    long                                ! set if you do a DELE and the Batch list will need updating
_callDoneIfConnectionCloses long
_firstUIDLUpdate        long                                ! set automatically, do not change this!
_useList                long
_useUIDL                long
_stoppedAtMsgNumber     long

!--- Methods
Ask                     Procedure (long p_command), virtual
Decide                  Procedure (long mode), virtual
Done                    Procedure (long command, long finished), virtual
Init                    Procedure (uLong Mode=NET:SimpleClient), virtual
ConnectionClosed        Procedure (), virtual
Process                 Procedure (), virtual
Kill                    Procedure (), virtual

CalcProgress            Procedure (), virtual
DecideToDelete          Procedure (), virtual

DoneListAll             Procedure (), virtual
DoneUIDLAll             Procedure (), virtual

_ProcessStat            Procedure (), virtual
_ProcessList            Procedure (), virtual
_GetUidlList            Procedure (), virtual

_ProcessDecideWhereToGo Procedure (), virtual
_ProcessDoQuit          Procedure (), virtual
_ProcessSplitReturned   Procedure (*String p_Str), long, virtual
_ProcessCopyBinData     Procedure (string p_Correct), long, virtual
_ProcessGetData         Procedure (), long, virtual
_ProcessReceivedMessage Procedure (long p_MaxMessageNum), long, virtual
_ProcessUIDLAll         Procedure (), long, virtual

_CheckIfWeNeedNewBatch  Procedure (), long, virtual
_CallDone               Procedure (long Command, long Finished), virtual
_ProcessListAll         Procedure (), long, virtual
_NextMessageFromList    Procedure (), long, virtual
_MarkMessageDoneInList  Procedure (), virtual
_GetToDelete            Procedure (),byte, virtual
_GetToDownload          Procedure (),byte, virtual
_LoginSuccessful        Procedure (), virtual

LoadEmailsDoneBeforeQ   Procedure (), virtual
SaveEmailsDoneBeforeQ   Procedure (), virtual
                     end


!--------------------------------------------------------------------------------
!Class NetEmailSend
!
!--------------------------------------------------------------------------------
NetEmailSend         Class(NetEmail),Type,Module('NetEmail.Clw'),LINK('NetEmail.Clw',_ABCLinkMode_),DLL(_ABCDllMode_)
! Properties
! NetSimple Object

! Object Properties
! ------------ SMTP Server Settings -----------
!Server                 string(252)
!Port                   NET:PortType
!_Spacer1               NET:PortType
SecureEmailStartTLS    long                            ! switch to SSL (TLS) after issuing STARTTLS call
                                                       ! If you wish to use SSL from the start of the connection
                                                       ! then set self.SSL = 1
! -------------- Message Details --------------
Helo                   string(NET:StdEmailListSize)
!ToList                 string(NET:StdEmailListSize)    ! Comma separated e.g. Jono Woodhouse <jono@capesoft.com>, Bruce Johnson <bruce@capesoft.com>
!CcList                 string(NET:StdEmailListSize)
BccList                string(NET:StdEmailListSize)
!From                   string(NET:StdEmailFromSize)
!ReplyTo                string(Net:StdEmailFromSize)
!Organization           string(Net:StdEmailFromSize)
!DeliveryReceiptTo      string(Net:StdEmailFromSize)
!DispositionNotificationTo string(Net:StdEmailFromSize)
NewsGroup              string(NET:StdNewsNewsgroupSize)
!Subject                string(NET:StdEmailSubjectSize)
!References             string(Net:StdEmailReferencesSize) ! References
!AttachmentList         string(NET:StdEmailAttachmentListSize)
!EmbedList              string(NET:StdEmailAttachmentListSize)  ! New 6/3/2001! A list of files that are embedded into the HTML code
ExtraHeader            string(1024)
XMailer                string(60)                      ! New 12/6/2001
!MessageText            &string                         ! Use SetRequiredMessageSize() Method to set the size of this
!MessageHtml            &string
!WholeMessage           &string
!MessageTextLen         long
!MessageHtmlLen         long
!WholeMessageLen        long
Progress1000           long
ProgressBytesToGo      long
ProgressBytesMax       long
ProgressControl        long
ServerErrorNum         long
!ServerErrorDesc        string (252)

! -------------- Message Queue ---------------
DataQueue              &Net:EmailDataQType

! ----------------- Options ------------------
ReconnectAfterXMsgs         long
OptionsKeepConnectionOpen   long
OptionsAttachmentContentType string (40)                    ! e.g. 'application/x-unknown'
OptionsMimeTextCharset      string (40)                     ! e.g. 'us-ascii' or 'iso-8859-1'
OptionsMimeTextTransferEncoding string (40)                 ! e.g. '7bit' (no encoding), 'quoted-printable' or 'base64'
OptionsMimeTextDontEncode   long                            ! Set the transfer encoding to be of type OptionsTransferEncoding, but don't perform any encoding algorithms. ie the text is already encoded
OptionsMimeHtmlCharset      string (40)                     ! e.g. 'us-ascii' or 'iso-8859-1'
OptionsMimeHtmlTransferEncoding string (40)                 ! e.g. '7bit' (no encoding), 'quoted-printable' or 'base64'
OptionsMimeHtmlDontEncode   long                            ! Set the transfer encoding to be of type OptionsTransferEncoding, but don't perform any encoding algorithms. ie the text is already encoded
OptionsContentType          string(40)                      ! e.g. 'text/plain' or 'multipart/alternative'. By default this is blank and the type is set automatically.
OptionsMultiPartContent     long                            ! Used with the above property and generates a custom multi-part header rather than a custom plain text message header

! ------------ AUTH LOGIN Properties ---------
AuthUser                string (NET:StdAUTHLoginSize)
AuthPassword            string (NET:StdAUTHLoginSize)

! ------------ Internal Properties -----------
!_objectType             long
_DataQueue              &Net:EmailDataQType
!_ServerDesc             cstring(80)
!_ServerDesc_Set         long
_ListIndex2             long
_ListWhich2             long
_SentOnThisConnection   long
!_State                  long
_MessagesSentOnCurrentConnection  long
!_ServerReplyBuffer      string (NET:EmailReplyBufferSize)
!_ServerReplyBufferLen   long

_MX                     long                                ! 28/7/2002 For compatability with CapeSoft Email Server
_MessageBytesSent       long                                ! 15/4/2003 Jono for ES

_GotHostName            long
_HostName               string (128)
_IDCount                long

_ErrorStatus            long                                ! See nettalk.inc for possible values like NET:ERROR_STATUS_SERVER_ERROR
_AfterErrorAction       long                                ! See nettalk.inc for possible values like NET:AFTER_ERROR_ACTION_DELETE_ALL
_AfterErrorSavedAction  long                                ! This is the value of _AfterErrorAction in ErrorTrap, just before _AfterErrorAction was reset


! Methods
Init                    Procedure (uLong Mode=NET:SimpleClient), virtual
SetRequiredMessageSize  Procedure (long p_WholeSize, long p_TextSize, long p_HtmlSize), long, proc, virtual
Free                    Procedure (), virtual
SendMail                Procedure (long mode), long, proc, virtual
AddAttachmentFromFile   Procedure(string FilePath,String FileName,Long pEmbed,Long fileCount, String MimeBoundary1, String MimeBoundary2),long,proc, virtual
OpenAttachmentFile      Procedure(string filepath,string fileName),Long,Virtual
ReadAttachmentFile      Procedure(string filepath,string fileName),Virtual
CloseAttachmentFile     Procedure(string filepath,string fileName),Virtual
ErrorTrap               Procedure (string errorStr, string functionName), virtual
Open                    Procedure (string Server,uShort Port=0), virtual
Close                   Procedure (), virtual
Process                 Procedure (), virtual
ConnectionClosed        Procedure (), virtual
MessageSent             Procedure (), virtual
CalcEmailName           Procedure (string myList,*string myDesc,*string myEmail, long myIndex), long, virtual
Kill                    Procedure (), virtual
Abort                   Procedure (), virtual
CalcProgress            Procedure (), virtual
AfterBuildFromWhole     Procedure (), virtual
AfterBuildFromParts     Procedure (), virtual

PostError               Procedure (long err, long errStatus, long errAction, string msg, string functionName), long, proc, virtual
ResetProgressControl    Procedure (), virtual

_CloseIdleConnection    Procedure (), virtual
_DeleteAllMessagesInDataQueue Procedure (), virtual
_GetHeaderField         Procedure (string p_Header, long p_count), string, virtual
_ConvertList            Procedure (*string p_List), virtual
_ConvertDots            Procedure (string p_secondlastchar, string p_lastchar), virtual
_mxOpenRequest          Procedure (), virtual
_TryOpen                Procedure (), virtual
_ProcessOpen            Procedure (), virtual


! Sep09: New
EmbedImages             Procedure (*string htmlSource, long embedImages = 1, string rootFolder), string, virtual
ProcessEmbedList        Procedure (*string embedList, *Net:EmbedsQType EmbedsQ), virtual
ProcessEmbedNames       Procedure (*Net:EmbedsQType embedsQ, *string messageHtml), virtual
SetAttachmentContentType Procedure(string fileName, *string contentType), virtual
                     end


!--------------------------------------------------------------------------------
!Class NetNewsReceive
!
!--------------------------------------------------------------------------------
NetNewsReceive       Class(NetEmailReceive),Type,Module('NetEmail.Clw'),LINK('NetEmail.Clw',_ABCLinkMode_),DLL(_ABCDllMode_)
!--- Properties
qNewsGroups             &Net:NewsGroupsQType               ! Queue of Newsgroups
qNewsHeaders            &Net:NewsHeaderQType               ! Queue of Newsgroups

Newsgroup               string (NET:StdNewsNewsgroupSize)  ! Newsgroup name of the group to read
DownloadRangeStart      long                               ! 0 = don't use ie Starting Message ID
DownloadRangeEnd        long                               ! 0 = don't use ie Last Messages ID
Lines                   long
XRef                    string (NET:StdEmailReferencesSize)
DefaultDownloadFlag     long !(1)                           ! Default to downloading messages (you can set this - it controls all messages, or individually set it in the self.decide() method).
NoPosting               long

!--- "Private" (internal) Properties
_MaxMessageNum          long
_CurrentHeader          long                               ! Current header number in the qNewsHeader that is being read in full (ie whole message)
_qIntNewsgroups         &Net:NewsGroupsQType               ! Do not use
_qIntNewsHeaders        &Net:NewsHeaderQType               ! Do not use
_TempData               string (Net:MAXBinData * 2)
_TempDataLen            long
_TempData2              string (Net:MAXBinData * 2)
_SubPacket              long                               ! Subsequent Packet Flag (ie not FirstPacket)

!--- Methods
Process                 Procedure (), virtual
Ask                     Procedure (long p_command), virtual
_ProcessSplitReturned   Procedure (*String p_Str), long, virtual
_ProcessGetData         Procedure (), long, virtual
_ProcessList            Procedure (), long, virtual
Init                    Procedure (uLong Mode=NET:SimpleClient), virtual
Kill                    Procedure (), virtual
_ProcessXOver           Procedure (), long, proc, virtual
                     end


!--------------------------------------------------------------------------------
!Class NetNewsSend
!
!--------------------------------------------------------------------------------
NetNewsSend          Class(NetEmailSend),Type,Module('NetEmail.Clw'),LINK('NetEmail.Clw',_ABCLinkMode_),DLL(_ABCDllMode_)
!--- Properties
!User                    string(80)
!Password                string(80)
NoPosting               long

!--- Methods
Init                    Procedure (uLong Mode=NET:SimpleClient), virtual
Process                 Procedure (), virtual
_ProcessSplitReturned   Procedure (*String p_Str), long, virtual
                     end
!_EndOfInclude_

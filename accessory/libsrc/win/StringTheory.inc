!===================================================================!
! CapeSoft StringTheory, a generic string handling class
! Handles dynamic allocation of string, parsing and splitting
! of string, conversion to and from Unicode, Base64 encoding
! etc.
!
! Copyright (c) 2013 by CapeSoft Software, All Rights Reserved
!   Based on a StringClass by Rick Martin as published in
!   www.clarionmag.com.
!===================================================================!

st:NoHex            Equate(1)
st:NoPlus           Equate(2)
st:NoColon          Equate(8)
st:Plus             Equate(4)
st:BigPlus          Equate(16)
st:dos              Equate(32)

st:windows          equate(1) ! CRLF
st:mac              equate(2) ! CR
st:unix             equate(3) ! LF

st:SortNoCase       equate(1)
st:SortCase         equate(2)
st:SortLength       equate(4)

st:spaces           equate(1)
st:tabs             equate(2)
st:cr               equate(4)
st:lf               equate(8)

st:First            Equate(1)
st:Last             Equate(-1)

st:Upper            equate(1)
st:Lower            equate(2)
st:Number           equate(4)
st:AlphaFirst       equate(8)
st:Punctuation      equate(16)
st:Hex              equate(32)
st:DNA              equate(64)

st:Append           equate(1)
st:Overwrite        equate(0)
st:Clip             equate(1)
st:NoBlanks         equate(2)
st:NoClip           equate(0)
st:nested           equate(1)

st:EncodeAnsi       equate(0)
st:EncodeUtf8       equate(1)
st:EncodeUtf16      equate(2)

ST:TEXT             equate(0)
ST:HTML             equate(1)
ST:NOPUNCTUATION    equate(2)

st:Forwards         equate(1)
st:Backwards        equate(-1)

st:Base64           equate(1)
st:QuotedPrintable  equate(2)

!------------- API Equates -------------!

st:MAX_PATH                   equate(260)

st:GENERIC_READ               equate(80000000h)
st:GENERIC_WRITE              equate(40000000h)
st:FILE_SHARE_READ            equate(00000001h)
st:FILE_SHARE_WRITE           equate(00000002h)
st:FILE_SHARE_DELETE          equate(00000002h)
st:CREATE_NEW                 equate(1)
st:CREATE_ALWAYS              equate(2)
st:CREATE_EXISTING            equate(3)
st:OPEN_ALWAYS                equate(4)
st:OPEN_EXISTING              equate(3)
st:TRUNCATE_EXISTING          equate(5)
st:INVALID_SET_FILE_POINTER   equate(-1)
st:FILE_BEGIN                 equate(0)
st:FILE_CURRENT               equate(1)
st:FILE_END                   equate(2)
st:FILE_ATTRIBUTE_NORMAL      equate(080h)

st:INVALID_HANDLE_VALUE       equate(-1)

! equate for passing the the FormatMessage() function
st:FORMAT_MESSAGE_ALLOCATE_BUFFER equate(000100h)
st:FORMAT_MESSAGE_IGNORE_INSERTS  equate(000200h)
st:FORMAT_MESSAGE_FROM_STRING     equate(000400h)
st:FORMAT_MESSAGE_FROM_HMODULE    equate(000800h)
st:FORMAT_MESSAGE_FROM_SYSTEM     equate(001000h)
st:FORMAT_MESSAGE_ARGUMENT_ARRAY  equate(002000h)
st:FORMAT_MESSAGE_MAX_WIDTH_MASK  equate(0000FFh)

! Code Page Values.
st:CP_ACP               equate(0)               ! ANSI code page
st:CP_OEMCP             equate(1)               ! OEM  code page
st:CP_MACCP             equate(2)               ! MAC  code page
st:CP_THREAD_ACP        equate(3)               ! current thread's ANSI code page
st:CP_SYMBOL            equate(42)              ! SYMBOL translations

st:CP_US_ASCII          equate(20127)

st:CP_WINDOWS_1250       equate(1250)
st:CP_WINDOWS_1251       equate(1251)
st:CP_WINDOWS_1252       equate(1252)
st:CP_WINDOWS_1253       equate(1253)
st:CP_WINDOWS_1254       equate(1254)
st:CP_WINDOWS_1255       equate(1255)
st:CP_WINDOWS_1256       equate(1256)
st:CP_WINDOWS_1257       equate(1257)
st:CP_WINDOWS_1258       equate(1258)

st:CP_WINDOWS1250       equate(1250)
st:CP_WINDOWS1251       equate(1251)
st:CP_WINDOWS1252       equate(1252)
st:CP_WINDOWS1253       equate(1253)
st:CP_WINDOWS1254       equate(1254)
st:CP_WINDOWS1255       equate(1255)
st:CP_WINDOWS1256       equate(1256)
st:CP_WINDOWS1257       equate(1257)
st:CP_WINDOWS1258       equate(1258)

st:CP_ISO_8859_1        equate(28591)
st:CP_ISO_8859_2        equate(28592)
st:CP_ISO_8859_3        equate(28593)
st:CP_ISO_8859_4        equate(28594)
st:CP_ISO_8859_5        equate(28595)
st:CP_ISO_8859_6        equate(28596)
st:CP_ISO_8859_7        equate(28597)
st:CP_ISO_8859_8        equate(28598)
st:CP_ISO_8859_9        equate(28599)
st:CP_ISO_8859_13       equate(28603)
st:CP_ISO_8859_15       equate(28605)

st:CP_ISO88591        equate(28591)
st:CP_ISO88592        equate(28592)
st:CP_ISO88593        equate(28593)
st:CP_ISO88594        equate(28594)
st:CP_ISO88595        equate(28595)
st:CP_ISO88596        equate(28596)
st:CP_ISO88597        equate(28597)
st:CP_ISO88598        equate(28598)
st:CP_ISO88599        equate(28599)
st:CP_ISO885913       equate(28603)
st:CP_ISO885915       equate(28605)

st:CP_UTF7              equate(65000)           ! UTF-7 translation
st:CP_UTF8              equate(65001)           ! UTF-8 translation

StringTheory:Version                 equate('1.93')


!  MBCS and Unicode Translation Flags.
st:MB_PRECOMPOSED       equate(000000001h)      ! use precomposed chars
st:MB_COMPOSITE         equate(000000002h)      ! use composite chars
st:MB_USEGLYPHCHARS     equate(000000004h)      ! use glyph chars, not ctrl chars
st:MB_ERR_INVALID_CHARS equate(000000008h)      ! error for invalid chars

st:WC_COMPOSITECHECK    equate(000000200h)      ! convert composite to precomposed
st:WC_DISCARDNS         equate(000000010h)      ! discard non-spacing chars
st:WC_SEPCHARS          equate(000000020h)      ! generate separate chars
st:WC_DEFAULTCHAR       equate(000000040h)      ! replace w/ default char

st:WC_NO_BEST_FIT_CHARS equate(000000400h)      ! do not use best fit chars

! Stores a string that has been split into parts based on a token (seperator)
LinesQType          queue, type
line                  &string
                    end

LinesGroupType      group,type
line                  &string
                    end

st:Z_BINARY   Equate(0)
st:Z_TEXT     Equate(1)
st:Z_UNKNOWN  Equate(2)
st:Z_DEFLATED Equate(8)

st:Z_FILTERED         Equate(1)
st:Z_HUFFMAN_ONLY     Equate(2)
st:Z_RLE              Equate(3)
st:Z_FIXED            Equate(4)
st:Z_DEFAULT_STRATEGY Equate(0)

st:MAX_WBITS       Equate(15)
st:Z_NO_FLUSH      Equate(0)
st:Z_PARTIAL_FLUSH Equate(1)
st:Z_SYNC_FLUSH    Equate(2)
st:Z_FULL_FLUSH    Equate(3)
st:Z_FINISH        Equate(4)
st:Z_BLOCK         Equate(5)
st:Z_TREES         Equate(6)

st:Z_OK            Equate(0)
st:Z_STREAM_END    Equate(1)
st:Z_NEED_DICT     Equate(2)
st:Z_ERRNO         Equate(-1)
st:Z_STREAM_ERROR  Equate(-2)
st:Z_DATA_ERROR    Equate(-3)
st:Z_MEM_ERROR     Equate(-4)
st:Z_BUF_ERROR     Equate(-5)
st:Z_VERSION_ERROR Equate(-6)
st:Z_DLL_ERROR     Equate(-99)

z_stream_s   Group,Type
next_in        ulong !&string    !  /* next input byte */
avail_in       ulong      !  /* number of bytes available at next_in */
total_in       ulong      !  /* total number of input bytes read so far */

next_out       ulong !&string    !  /* next output byte should be put there */
avail_out      ulong      !  /* remaining free space at next_out */
total_out      ulong      !  /* total number of bytes output so far */

msg            ulong !&cstring   !  /* last error message, NULL if no error */
state          long      !  /* not visible by applications */

zalloc         ulong      !  /* used to allocate the internal state */
zfree          ulong      !  /* used to free the internal state */
opaque         ulong      !  /* private data object passed to zalloc and zfree */

data_type      long       !  /* best guess about the data type: binary or text */
adler          ulong      !  /* adler32 value of the uncompressed data */
reserved       ulong      !  /* reserved for future use */
             End



!===================================================================!
StringTheory        Class(), type, Module('StringTheory.clw'), Link('StringTheory.clw',StringTheoryLinkMode),DLL(StringTheoryDLLMode)
! These properties should be treated as private in most cases
value                 &string                               ! Use GetVal() to retrieve and Assign() to set. However can be used directly when a *String is required.
!_length               long                                  ! Should not be accessed directly. Will not contain the length of the string!
lines                 &LinesQType

! Public properties
winErrorCode          long
logErrors             long
gzipped               long
base64                long
base64NoWrap          long                                  ! If set, the base64 encoding doesn't break the line every 76 characters.
encoding              long                                  ! Oct10: Text encoding - Ansi, UTf-8 or Utf16
wideChars             long                                  ! When a string is converted to UTF-16, this is set to the number of wide chars the string contains
Bytes                 long
LastError             string(252)

Destruct              Procedure ()
Construct             Procedure ()
AppendA               Procedure (string newValue, long pClip = false, <string pSep>), virtual   ! Append to the string (The C55 compiler won't allow Append as a method name)
AppendBinary          Procedure(long pValue,Long pLength=4),Virtual
  omit ('****', _OLD_)    ! not allowed in 6.2, but allowed in 6.3. Unfortunately _C63_ not working in 6.3
  ! if you are running Clarion 6.2 or earlier, and getting a compile error here, then add _OLD_=>1 to your project settings.
Append                Procedure (string newValue, long pClip = false, <string pSep>), virtual   ! The Append method name is available in C6.3
  !****
SetValue              Procedure (string newValue,long pClip=false), virtual
Assign                Procedure (*string newValue), virtual
SetValue              Procedure (*string newValue,long pClip=false), virtual
Base64Encode          Procedure (),Virtual
Base64Encode          Procedure (*string pText, *long pLen), virtual
Base64Decode          Procedure (), virtual
Base64Decode          Procedure (*string pText, *long pLen), virtual
EncodedWordDecode     Procedure (), virtual
EncodedWordEncode     Procedure (<String pCharset>,long pEncoding=2),virtual
Free                  Procedure (Long pLines=False), virtual
FreeLines             Procedure (), virtual
ClipLen               Procedure (), long, virtual
ClipLength            Procedure (), long, virtual
Crop                  Procedure (ulong pStart=1, ulong pEnd=0), Virtual
Instring              Procedure (string pSearchValue, long pStep=1, long pStart=1, long pEnd=0, long pNocase=0, long pWholeWord=0), long, virtual
!Find                  Procedure (*string pFind, *string pVal, long pStart=1, long pEnd=0, bool noCase=False, bool clipLen=true), long, virtual
Count                 Procedure (string pSearchValue, long pStep=1, long pStart=1, long pEnd=0, long pNoCase=0, bool softClip=true, long pOverlap=true), long, virtual
ErrorTrap             Procedure (string methodName, string errorMessage), virtual
ExtensionOnly         Procedure (<string fPath>),String, virtual
FormatMessage         Function (long err), string, virtual
GetValue              Procedure (), string, virtual
GetValue              Procedure (long maxLen), string, virtual
GetVal                Procedure (), string, virtual
GetLine               Procedure (long pLineNumber), string, virtual
InLine                Procedure (string pSearchValue, long pStep=1, long pStart=1, long pEnd=0, long pNocase=0, long pWholeLine=0),Long,virtual
SetLine               Procedure (long pLineNumber,String pValue),virtual
AddLine               Procedure (long pLineNumber,String pValue),virtual
DeleteLine            Procedure(long pLineNumber), long, proc, virtual
RemoveLines           Procedure(<String pAlphabet>), long, proc, virtual
Join                  Procedure (string pBoundary,<string pQuotestart>,<string pQuoteEnd>),virtual
  Compile ('****', _C60_)
Length                Procedure (), long, virtual                ! Method name only allowed in C6 and above
  ****
LengthA               Procedure (), long, virtual
Len                   Procedure (), long, virtual
MD5                   Procedure (), string, virtual
Prepend               Procedure (string newValue, long pClip = false, <string pSep>), virtual
Random                Procedure (long pLength=16, long pFlags=0, <String pAlphabet>),String, Proc, virtual
Records               Procedure (), long, virtual
Replace               Procedure (string pOldValue, string pNewValue, long pCount=0, long pStart=1, long pEnd=0, long pNoCase=0, bool pRecursive=false), Long, Proc, virtual
SetLength             Procedure (Long strLen), virtual
Slice                 Procedure (ulong pStart=1, ulong pEnd=0),String, virtual
Split                 Procedure (string pSplitStr,<string pQuotestart>,<string pQuoteEnd>, bool removeQuotes = false, bool pClip = false, bool pLeft=false, <string pSeparator>,Long pNested=false), virtual
  omit ('****', _OLD_)                                          ! not allowed in 6.2, but allowed in 6.3. Unfortunately _C63_ not working in 6.3
    ! if you are running Clarion 6.2 or earlier, and getting a compile error here, then add _OLD_=>1 to your project settings.
Sort                  Procedure (Long pSortType,string pSplitStr,<string pQuotestart>,<string pQuoteEnd>, bool pClip = false, bool pLeft=false),Virtual
Sort                  Procedure (Long pSortType),Virtual
          ****
SplitEvery            Procedure (long numChars), virtual
Filter                Procedure(string pInclude, string pExclude),virtual
Sub                   Procedure (ulong pStart=1, ulong pLength=1), string, virtual
ToCstring             Procedure (), *cstring, virtual
Trace                 Procedure (<string errMsg>), virtual
Upper                 Procedure (<String pQuote>, <String pQuoteEnd>), virtual
Lower                 Procedure (<String pQuote>, <String pQuoteEnd>), virtual
FileNameOnly          Procedure (<string fPath>,Long pIncludeExtension=true), string, virtual
PathOnly              Procedure (<string fPath>), string, virtual
LoadFile              Procedure (string fileName,Long pOffset=0, Long pLength=0), long, proc, virtual
SaveFile              Procedure (string fileName, bool pAppendFlag=false), long, proc, virtual
SaveFile              Procedure (*string WriteString, string fileName, bool pAppendFlag, long dataLen=0), long, proc, virtual
SaveFileA             Procedure (string WriteString, string fileName, bool pAppendFlag=false), long, proc, virtual

! Feb2010
Before                Procedure (string pSearchValue, long pStart=1, long pEnd=0, long pNoCase=0), string, virtual
Between               Procedure (string pLeft, string pRight,long pStart=1, long pEnd=0, long pNoCase=0, long pExclusive=true), string, virtual
FindBetween           Procedure (string pLeft, string pRight, *long pStart, *long pEnd, bool pNoCase=false, long pExclusive=true), string, virtual
After                 Procedure (string pSearchValue, long pStart=1, long pEnd=0, long pNoCase=0), string, virtual

! Mar 2010
ToBlob                Procedure (*blob blobField), long, proc, virtual
FromBlob              Procedure (*blob blobField), long, proc, virtual

!--- Unicode handling
! Conversion between ANSI and UTF-16
AnsiToUtf16           Procedure (*string strAnsi, *long unicodeChars, ulong CodePage=st:CP_US_ASCII), *string, virtual
Utf16ToAnsi           Procedure (*string unicodeString, *long ansiLen, long unicodeChars=-1, ulong CodePage=st:CP_US_ASCII), *string, virtual

! Conversion between UTF-8 and UTF-16
Utf8To16              Procedure (*string strUtf8, *long unicodeChars), *string, virtual
Utf16To8              Procedure (*string unicodeString, *long utf8Len, long unicodeChars=-1), *string, virtual

! Conversion between ANSI and UTF-8
Utf8ToAnsi            Procedure (*string strUtf8, *long ansiLen, ulong pCodePage=st:CP_US_ASCII), *string, virtual
AnsiToUtf8            Procedure (*string strAnsi, *long utf8Len, ulong pCodePage=st:CP_US_ASCII), *string, virtual

! Old method names, backward compatibility only
apiUtf8ToAnsi         Procedure (*string strUtf8, *long ansiLen), *string, virtual
apiAnsiToUtf8         Procedure (*string strAnsi, *long utf8Len), *string, virtual

ToAnsi                Procedure (long encoding=0, ulong pCodePage=st:CP_US_ASCII), long, proc, virtual
ToUnicode             Procedure (long encoding=st:EncodeUtf8, ulong pCodePage=st:CP_US_ASCII), long, proc, virtual

! Base conversion
DecToBase             Procedure (long num, long base=16, long lowerCase=1), string, virtual
BaseToDec             Procedure (string num, long base=16), long, virtual

! Byte based conversion to hex
ByteToHex             Procedure (byte pByte), string, virtual
HexToByte             Procedure (string hexVal), byte, virtual
LongToHex             Procedure (long pLong), string, virtual
StringToHex           Procedure (string binData,Long pLen=0,Long pCase=0), *string, virtual
HexToString           Procedure (string hexData), *string, virtual

ToHex                 Procedure (Long pCase=0), virtual     ! convert the stored string to hex
FromHex               Procedure (), virtual

WrapText              Procedure(long wrapAt=80), virtual
StrCpy                Procedure(*string pIn, *string pOut, bool pClip = true), long, proc, virtual

ToBytes               Procedure (*byte pVal, *string bVals), virtual
ToBytes               Procedure (*short pVal, *string bVals), virtual
ToBytes               Procedure (*ushort pVal, *string bVals), virtual
ToBytes               Procedure (*long pVal, *string bVals), virtual
ToBytes               Procedure (*ulong pVal, *string bVals), virtual
ToBytes               Procedure (*sreal pVal, *string bVals), virtual
ToBytes               Procedure (*real pVal, *string bVals), virtual
ToBytes               Procedure (*decimal pVal, *string bVals), virtual
ToBytes               Procedure (*cstring pVal, *string bVals), virtual
ToBytes               Procedure (*string pVal, *string bVals), virtual

FromBytes             Procedure (*string bVals, *byte pVal), virtual
FromBytes             Procedure (*string bVals, *short pVal), virtual
FromBytes             Procedure (*string bVals, *ushort pVal), virtual
FromBytes             Procedure (*string bVals, *long pVal), virtual
FromBytes             Procedure (*string bVals, *ulong pVal), virtual
FromBytes             Procedure (*string bVals, *sreal pVal), virtual
FromBytes             Procedure (*string bVals, *real pVal), virtual
FromBytes             Procedure (*string bVals, *decimal pVal), virtual
FromBytes             Procedure (*string bVals, *cstring pVal), virtual
FromBytes             Procedure (*string bVals, *string pVal), virtual

! Store the passed data in the StringTheory object as bytes
! The bytes are stored exactly as they are in the passed parameter
SetBytes              Procedure (*byte pVal), virtual
SetBytes              Procedure (*short pVal), virtual
SetBytes              Procedure (*ushort pVal), virtual
SetBytes              Procedure (*long pVal), virtual
SetBytes              Procedure (*ulong pVal), virtual
SetBytes              Procedure (*sreal pVal), virtual
SetBytes              Procedure (*real pVal), virtual
SetBytes              Procedure (*decimal pVal), virtual
SetBytes              Procedure (*cstring pVal), virtual
SetBytes              Procedure (*string pVal), virtual

! Retrieve the value stored in the StringTheory object and
! push the bytes into the passed parmeter.
GetBytes              Procedure (*byte pVal), virtual
GetBytes              Procedure (*short pVal), virtual
GetBytes              Procedure (*ushort pVal), virtual
GetBytes              Procedure (*long pVal), virtual
GetBytes              Procedure (*ulong pVal), virtual
GetBytes              Procedure (*sreal pVal), virtual
GetBytes              Procedure (*real pVal), virtual
GetBytes              Procedure (*decimal pVal), virtual
GetBytes              Procedure (*cstring pVal), virtual
GetBytes              Procedure (*string pVal), virtual

SetBytes              Procedure (*? pVal, string pType), bool, proc, virtual
GetBytes              Procedure (*? pVal, string pType), bool, proc, virtual

FindWord              Procedure(long pWordNumber,long startPos = -1, bool htmlEnc=false,*Long pStart, *Long pEnd), string, virtual
GetWord               Procedure (long pWordNumber,long startPos = -1, bool htmlEnc=false), string, virtual
Word                  Procedure (long pWordNumber,long startPos = -1, bool htmlEnc=false), string, virtual
WordStart             Procedure (long pStartPos=1, long textType=0,Long pDir=st:Forwards), long, virtual
WordEnd               Procedure (long pStartPos=1, long textType=0), long, virtual
CountWords            Procedure (long startPos = 1, bool htmlEnc=false), long, virtual
RemoveAttributes      Procedure (String pTag), virtual
Remove                Procedure(string pLeft,<string pRight>,long pNoCase=0,long pContentsOnly=0, long pCount=0), Long,Proc,virtual

Clip                  Procedure (), virtual
Trim                  Procedure (), virtual
Left                  Procedure(Long pLength=0,Long pwhat=1,<String pPad>),String, virtual
SetLeft               Procedure(Long pLength=0,Long pwhat=1,<String pPad>), virtual
Right                 Procedure(Long pLength=0,Long pwhat=1,<String pPad>),String, virtual
SetRight              Procedure(Long pLength=0,Long pwhat=1,<String pPad>), virtual
All                   Procedure(Long pLength=255),String, virtual
SetAll                Procedure(Long pLength=255), virtual
Squeeze               Procedure (long textType=ST:TEXT), virtual
ContainsADigit        Procedure (), bool, virtual
IsAllDigits           Procedure (), bool, virtual
ContainsA             Procedure (String pDigits,<String pTestString>), bool, virtual
IsAll                 Procedure (String pDigits,<String pTestString>), bool, virtual

SwitchEndian          Procedure (ulong x), long, virtual
BigEndian             Procedure (ulong x), long, virtual
LittleEndian          Procedure (ulong x), long, virtual

ReverseByteOrder      Procedure (), virtual

Str                   Procedure (), string, virtual
Str                   Procedure (string newValue),  string, proc, virtual
Str                   Procedure (*string newValue), string, proc, virtual
SetSlice              Procedure (ulong pStart=1, ulong pEnd=0, string newValue), virtual
Insert                Procedure (long pStart, string insertValue), virtual
Quote                 Procedure (<string pQuotestart>,<string pQuoteEnd>), virtual
UnQuote               Procedure (<string pQuotestart>,<string pQuoteEnd>), virtual
FindChar              Procedure (string pSearchValue, long pStart=1, long pEnd=0), long, virtual
FindChars             Procedure (string pSearchValue, long pStart=1, long pEnd=0), long, virtual

ColorToHex            Function (long claColor, bool addHash=false), string, virtual
ColorFromHex          Function (string hexCol), long, virtual

UrlEncode             Procedure (long flags =0,<String pDelimiter>,<String pSpace>), virtual
UrlDecode             Procedure (<String pDelimiter>,<String pSpace>), virtual

CleanFileName         Procedure (<string pFileName>, <string pReplaceChar>), string, proc, virtual
PeekRam               Procedure(uLong pAdr,Long pLen),Virtual
loadlibs              Procedure(),Virtual
gzip                  Procedure(Long pLevel=5),Long,Virtual,Proc
gunzip                Procedure(),Long,Virtual,Proc
MergeXml              Procedure(String pNew, Long pWhere),Virtual
LineEndings           Procedure(Long pEndings=1), Virtual
IsTime                PROCEDURE (String pValue),Long, Virtual
IsTime                PROCEDURE (),Long, Virtual
DeformatTime          PROCEDURE (String pValue),String, Virtual
DeformatTime          PROCEDURE (),Virtual
FormatTime            PROCEDURE (String pFormat),Virtual
FormatTime            PROCEDURE (Long pValue,string pFormat),String,Virtual
StartsWith            Procedure (String pStr,Long pCase=True),Long,Virtual
EndsWith              Procedure (String pStr,Long pCase=True),Long,Virtual
                    end

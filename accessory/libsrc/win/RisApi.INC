! Globalni podaci za API

UINT                    EQUATE(UNSIGNED)
FOURCC                  EQUATE(LONG)
WCHAR                   EQUATE(BYTE)
UCHAR                   EQUATE(BYTE)
CHAR                    EQUATE(BYTE)
BOOL                    EQUATE(SIGNED)
BYTE                    EQUATE(BYTE)
WORD                    EQUATE(USHORT) ! 03.04.11
DWORD                   EQUATE(ULONG)
HANDLE                  EQUATE(UNSIGNED)
LPVOID                  EQUATE(ULONG)
PVOID                   EQUATE(ULONG)
FLOAT                   EQUATE(SREAL)
LPSTR                   EQUATE(CSTRING)    !Usage:Pass the Label of the LPSTR
LPCSTR                  EQUATE(CSTRING)    !Usage:Pass the Label of the LPCSTR
!MAX_PATH                EQUATE(260)
HWND                    EQUATE(HANDLE)
HINSTANCE               EQUATE(HANDLE)
HMODULE                 EQUATE(HINSTANCE)
HTASK                   EQUATE(HANDLE)
HDC                     EQUATE(HANDLE)
WPARAM                  EQUATE(UINT)
LPARAM                  EQUATE(LONG)
LRESULT                 EQUATE(LONG)
LPBOOL                  EQUATE(ULONG)
LPCVOID                 EQUATE(ULONG)

HDIOBJ                  EQUATE(HANDLE)
HCOLORSPACE             EQUATE(HANDLE)
HRDVR                   EQUATE(HANDLE)
HWAVEOUT                EQUATE(HANDLE)
HWAVEIN                 EQUATE(HANDLE)
HACCEL                  EQUATE(HANDLE)
HBITMAP                 EQUATE(HANDLE)
HPEN                    EQUATE(HANDLE)
HWINSTA                 EQUATE(HANDLE)
HBRUSH                  EQUATE(HANDLE)
HRGN                    EQUATE(HANDLE)
HKL                     EQUATE(HANDLE)
HPALETTE                EQUATE(HANDLE)
HFONT                   EQUATE(HANDLE)
HHOOK                   EQUATE(HANDLE)
HDRVR                   EQUATE(HANDLE)
HDWP                    EQUATE(HANDLE)
HRSRC                   EQUATE(HANDLE)
HSTR                    EQUATE(HANDLE)
HCURSOR                 EQUATE(HANDLE)
HICON                   EQUATE(HCURSOR)
HGRN                    EQUATE(HANDLE)
HMENU                   EQUATE(HANDLE)
SC_HANDLE               EQUATE(HANDLE)
HDESK                   EQUATE(HANDLE)
HENHMETAFILE            EQUATE(HANDLE)
HSZ                     EQUATE(WORD)
HMETAFILE               EQUATE(HANDLE)
HMIDIIN                 EQUATE(HANDLE)
HMIDIOUT                EQUATE(HANDLE)
HMMIO                   EQUATE(HANDLE)
HFILE                   EQUATE(SIGNED)
LUID                    EQUATE(LONG)
PLUID                   EQUATE(LONG)

ANYSIZE_ARRAY           EQUATE(1)
PHANDLE                 EQUATE(HANDLE)
SPHANDLE                EQUATE(HANDLE)
LPHANDLE                EQUATE(HANDLE)
HGLOBAL                 EQUATE(HANDLE)
HLOCAL                  EQUATE(HANDLE)
GLOBALHANDLE            EQUATE(HANDLE)
LOCALHANDLE             EQUATE(HANDLE)
ATOM                    EQUATE(UINT)

COLORREF                EQUATE(DWORD)
HDDEDATA                EQUATE(DWORD)
HCONV                   EQUATE(DWORD)
HCONVLIST               EQUATE(DWORD)
LONGREAL                EQUATE(REAL)
DWORDLONG               EQUATE(REAL)
WORDLONG                EQUATE(REAL)
LARGE_INTEGER           EQUATE(LONG)
ULARGE_INTEGER          EQUATE(ULONG)
BOOLEAN                 EQUATE(BYTE)
!VERSION                 EQUATE(UNSIGNED)

LPCTSTR                 EQUATE(STRING)
!LPTSTR                  EQUATE(LPSTR)

! Processes section

PRUN:SW_HIDE                EQUATE(0)
PRUN:SW_SHOWNORMAL          EQUATE(1)
PRUN:SW_NORMAL              EQUATE(1)
PRUN:SW_SHOWMINIMIZED       EQUATE(2)
PRUN:SW_SHOWMAXIMIZED       EQUATE(3)
PRUN:SW_MAXIMIZE            EQUATE(3)
PRUN:SW_SHOWNOACTIVATE      EQUATE(4)
PRUN:SW_SHOW                EQUATE(5)
PRUN:SW_MINIMIZE            EQUATE(6)
PRUN:SW_SHOWMINNOACTIVE     EQUATE(7)
PRUN:SW_SHOWNA              EQUATE(8)
PRUN:SW_RESTORE             EQUATE(9)
PRUN:SW_SHOWDEFAULT         EQUATE(10)
PRUN:SW_MAX                 EQUATE(10)

!INFINITE                EQUATE(0FFFFh)

STARTF_USESHOWWINDOW        EQUATE(00000001H)

IDLE_PRIORITY_CLASS         EQUATE(00000040h)
NORMAL_PRIORITY_CLASS       EQUATE(00000020h)
HIGH_PRIORITY_CLASS         EQUATE(00000080h)
REALTIME_PRIORITY_CLASS     EQUATE(00000100h)

CREATE_DEFAULT_ERROR_MODE   EQUATE(04000000h)
CREATE_NEW_CONSOLE          EQUATE(00000010h)
CREATE_NEW_PROCESS_GROUP    EQUATE(00000200h)
CREATE_SEPARATE_WOW_VDM     EQUATE(00000800h)
CREATE_SUSPENDED            EQUATE(00000004h)
CREATE_UNICODE_ENVIRONMENT  EQUATE(00000400h)
DEBUG_ONLY_THIS_PROCESS     EQUATE(00000002h)
DEBUG_PROCESS               EQUATE(00000001h)
DETACHED_PROCESS            EQUATE(00000008h)
PROCESS_TERMINATE           EQUATE(0001H)
PROCESS_QUERY_INFORMATION   EQUATE(0400H)

PROCESS_INFORMATION     GROUP,TYPE
hProcess                  HANDLE
hThread                   HANDLE
dwProcessId               DWORD
dwThreadId                DWORD
                        END

! 28.08.12
RisRECT    GROUP,TYPE
left     SIGNED
top      SIGNED
right    SIGNED
bottom   SIGNED
 END


STARTUPINFO GROUP,TYPE
cb              DWORD
lpReserved      LONG    !*PSTR
lpDesktop       LONG    !*PSTR
lpTitle         LONG    !*PSTR
dwX             DWORD
dwY             DWORD
dwXSize         DWORD
dwYSize         DWORD
dwXCountChars   DWORD
dwYCountChars   DWORD
dwFillAttribute DWORD
dwFlags         DWORD
wShowWindow     WORD
cbReserved2     WORD
lpReserved2     LONG    !*BYTE
hStdInput       HANDLE
hStdOutput      HANDLE
hStdError       HANDLE
dwHotKey        DWORD
dwShellData     DWORD
            END

!SECURITY_ATTRIBUTES GROUP,TYPE
!nLength              DWORD
!lpSecurityDescriptor LPVOID
!bInheritHandle       BOOL
!            END

! Screen section

HORZSIZE      EQUATE(4)
HORZRES       EQUATE(8)
VERTSIZE      EQUATE(6)
VERTRES       EQUATE(10)
LOGPIXELSX    EQUATE(88)
LOGPIXELSY    EQUATE(90)

! Printer section

PRINTER_DEFAULTS        GROUP,TYPE
pDatatype                LONG   !*PSTR
pDevMode                 LONG   !LPDEVMODE
DesiredAccess            DWORD
                        END

! Memory Funcions
MEMORYSTATUS    GROUP,TYPE
dwLength            DWORD
dwMemoryLoad        DWORD
dwTotalPhys         DWORD
dwAvailPhys         DWORD
dwTotalPageFile     DWORD
dwAvailPageFile     DWORD
dwTotalVirtual      DWORD
dwAvailVirtual      DWORD
                END
                
MEMSTS GROUP(MEMORYSTATUS)
 END
 
PROCESS_MEMORY_COUNTERS GROUP,TYPE
cb DWORD
PageFaultCount             DWORD 
PeakWorkingSetSize             DWORD 
WorkingSetSize                 DWORD 
QuotaPeakPagedPoolUsage        DWORD 
QuotaPagedPoolUsage            DWORD 
QuotaPeakNonPagedPoolUsage     DWORD 
QuotaNonPagedPoolUsage         DWORD 
PagefileUsage                  DWORD 
PeakPagefileUsage              DWORD 
 END

PMC GROUP(PROCESS_MEMORY_COUNTERS)
 END 
 
! Version info
OSVERSIONINFO GROUP,TYPE
dwOSVersionInfoSize DWORD 
dwMajorVersion      DWORD 
dwMinorVersion      DWORD 
dwBuildNumber       DWORD 
dwPlatformId        DWORD 
szCSDVersion        STRING(128)
 END

OSV GROUP(OSVERSIONINFO)
 END

OSVERSIONINFOEX GROUP,TYPE
dwOSVersionInfoSize DWORD 
dwMajorVersion      DWORD 
dwMinorVersion      DWORD 
dwBuildNumber       DWORD 
dwPlatformId        DWORD 
szCSDVersion        STRING(128)
wServicePackMajor   WORD
wServicePackMinor   WORD
wSuiteMask          WORD
wProductType        BYTE
wReserved           BYTE
 END
  
OSVEX GROUP(OSVERSIONINFOEX)
 END

!----------------------------------------------------------------------------------------------------------------------------
! API Functions map
!----------------------------------------------------------------------------------------------------------------------------

 MAP
 MODULE('')
    ! Processec procs
    RisWinExec(*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW,NAME('WinExec')
    RisGetLastError(),DWORD,PASCAL,NAME('GetLastError')
    RisCreateProcess (ULONG lpApplicationName, ULONG lpCommandLine, |
      ULONG lpProcessAttributes,ULONG lpThreadAttributes, |
      BOOL bInheritHandles, DWORD dwCreationFlags, |
      LPVOID lpEnvironment, ULONG lpCurrentDirectory, |
      ULONG lpStartupInfo, ULONG lpProcessInformation ),BOOL,RAW,PASCAL,NAME('CreateProcessA')
    RisTerminateProcess(HANDLE hProcess,UINT uExitCode),BOOL,PASCAL,RAW,NAME('TerminateProcess')
    RisExitProcess(UNSIGNED),PASCAL,RAW,NAME('ExitProcess')
    RisCloseHandle(HANDLE),BOOL,PASCAL,NAME('CloseHandle')
    RisOpenProcess(DWORD dwDesiredAccess,BOOL bInheritHandle,DWORD dwProcessId),HANDLE,PASCAL,NAME('OpenProcess')
    RisGetExitCodeProcess(HANDLE hProcess,ULONG lpExitCode),BOOL,PASCAL,NAME('GetExitCodeProcess')
    RisWaitForSingleObject(HANDLE,DWORD),DWORD,PASCAL,PROC,NAME('WaitForSingleObject')
    RisGetCurrentProcessId(),DWORD,PASCAL,NAME('GetCurrentProcessId')
    RisDeleteFileA(*LPCSTR),BOOL,PASCAL,RAW,NAME('DeleteFileA')
    RisGlobalMemoryStatus(*MEMORYSTATUS),PASCAL,RAW,NAME('GlobalMemoryStatus')
    RisGetVolumeInformationA(*LPCSTR,*LPSTR,DWORD,*DWORD,*DWORD,*DWORD, *LPSTR ,DWORD),BOOL,PASCAL,RAW,NAME('GetVolumeInformationA')
    ! Printer procs
    RisStartDocPrinter(HANDLE,DWORD,ULONG),DWORD,PASCAL,NAME('StartDocPrinterA')
    RisWritePrinter(HANDLE,LPVOID,DWORD,DWORD),BOOL,PASCAL,RAW,NAME('WritePrinter')
    RisStartPagePrinter(HANDLE),BOOL,PASCAL,NAME('StartPagePrinter')
    RisEndPagePrinter(HANDLE),BOOL,PASCAL,NAME('EndPagePrinter')
    RisEndDocPrinter(HANDLE),BOOL,PASCAL,NAME('EndDocPrinter')
    RisOpenPrinter(*LPSTR,*HANDLE,*PRINTER_DEFAULTS),BOOL,PASCAL,RAW,NAME('OpenPrinterA')
    RisClosePrinter(HANDLE),BOOL,PASCAL,NAME('ClosePrinter')
    !
    RisGetTempPath(UNSIGNED,*CSTRING),UNSIGNED,RAW,PASCAL,NAME('GetTempPathA')
    RisGetUserNameA(*LPSTR,*DWORD),BOOL,PASCAL,RAW,NAME('GetUserNameA')
    RisGetVersionExA(*OSVERSIONINFOEX),BOOL,PASCAL,RAW,NAME('GetVersionExA')
    RisGetOperatingComputerName(*LPSTR,*DWORD),BOOL,PASCAL,RAW,NAME('GetComputerNameA')
    ! 
    RisGetDC(HWND),HDC,PASCAL,RAW,NAME('GetDC')
    RisGetDeviceCaps(HDC, SIGNED),SIGNED,PASCAL,RAW,NAME('GetDeviceCaps')
    RisReleaseDC(HWND, HDC),SIGNED,PASCAL,RAW,NAME('ReleaseDC')
    !
    RisBringWindowToTop(UNSIGNED),SHORT,PASCAL,PROC,NAME('BringWindowToTop')
    RisGetDesktopWindow(),UNSIGNED,PASCAL,NAME('GetDesktopWindow')
    RisAttachThreadInput(DWORD,DWORD,BOOL),BOOL,PASCAL,PROC,NAME('AttachThreadInput')
    RisShowWindow(HWND, SIGNED),BOOL,PASCAL,PROC,NAME('ShowWindow')
    RisShowWindowAsync(HWND,SIGNED),BOOL,PASCAL,NAME('ShowWindowAsync')
    RisFindWindow(LONG,*LPCSTR),HWND,PASCAL,RAW,NAME('FindWindowA')
    RisGetWindowThreadProcessId(HWND,*DWORD),DWORD,PASCAL,NAME('GetWindowThreadProcessId')
    RisGetForegroundWindow(),HWND,PASCAL,NAME('GetForegroundWindow')
    RisGetCurrentThreadId(),DWORD,PASCAL ,NAME('GetCurrentThreadId')  
    RisShellExecute(UNSIGNED,*CSTRING,*CSTRING,*CSTRING,*CSTRING,SIGNED),UNSIGNED,PASCAL,RAW,PROC,NAME('ShellExecuteA')
  END
    !
 END
 
 
! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software  - www.capesoft.com


      Member()
         omit('***$***',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
         ***$***
      Include('Equates.CLW'),ONCE
      Include('Keycodes.CLW'),ONCE
      Include('Errors.CLW'),ONCE
      Map
      End ! map
      Include('NetAll.inc'),ONCE
      ! includes - NetMap Module Map
      include('NetMap.inc'),once
      include('StringTheory.Inc'),Once

_NetAll.Log          PROCEDURE  (string p_FunctionName,string p_LogString,long p_ErrorType=0) ! Declare Procedure 3
SHORT_STRING_LEN     equate (256)
ShortCStr            cstring (SHORT_STRING_LEN),auto
LongCStr             &cstring
ls                   long,auto
  CODE
  if p_ErrorType <> Net:LogAlways
    if self.LoggingOn = 0 and self.LoggingErrorsOnly = 0
      return
    end
    if self.LoggingOn = 0 and self.LoggingErrorsOnly and p_ErrorType <> NET:LogError
      return                              ! Nothing to do
    end
  end
  if gNetLogging.LoggingFirstTime = 0
    gNetLogging.LoggingFirstTime = 1
    self._LoggingFirstTime()
  end

  ls = len (p_LogString) + len (p_FunctionName) + 15
  if ls < SHORT_STRING_LEN ! < because we need 1 extra char for cstring
    ShortCStr = '[Net] [' & Thread() & '] ' & p_FunctionName & ' - ' &  p_LogString
    OS_OutputDebugString (address(ShortCStr))
    return
  else
    LongCStr &= new (cstring(ls+1))
    LongCStr = '[Net] [' & Thread() & '] ' & p_FunctionName & ' - ' &  p_LogString
    OS_OutputDebugString (address(LongCStr))
    dispose (LongCStr)
  end

  return

!------------------------------------------------------------------------------
_NetAll.InterpretError PROCEDURE  ()                       ! Declare Procedure 3
tempErrorStr   string (512)
  CODE
  case (self.error)
  of 0
    return ('General Object level error')          !
  of -1
    return('Unknown Error (was previously NetTalk DLL not actively initiated)')
  of ERROR:DllActive
    return('NetTalk DLL already active. Certain options can''t be set once the DLL is loaded.')
  of ERROR:TooManyInstances
    return('Too many NetTalk NetAuto Instances on one machine')
  of ERROR:InvalidStructureSize
    return('Invalid structure size')
  of ERROR:MainStartFailed
    return('Main start sequence faile')
  of ERROR:CallBackWindowTimeout
    return('CallBack window timeout')
  of ERROR:AcceptedDataTooShort
    return('Accepted data too short')
  of ERROR:DataIsOutOfTopic
    return('Data is out of topic')
  of ERROR:UnknownPacketType
    return('Unknown packet type received')
  of ERROR:NotAllDataSent
    return('Not all data sent')
  of ERROR:NotAllDataReceived
    return('Not all data received')
  of ERROR:ReceiverNotFound
    return('You are trying to send data to someone who is not in the receivers (servers) list.')
  of ERROR:NoRequest
    return('No request')
  of ERROR:RequestNotFound
    return('Request not found')
  of ERROR:NoServers
    return('No servers')
  of ERROR:ServerNotFound
    return('Server not found')
  of ERROR:ServiceNotSupport
    return('Service not supported')
  of ERROR:BadResponseAddress
    return('Bad response address')
  of ERROR:NewThreadFail
    return('New thread failed')
  of ERROR:AddressNotFound
    return('Address not found')
  of ERROR:IncorrectWinSockDLLVersion
    return('Incorrect WinSock DLL version')
  of ERROR:BadOptions
    return('Bad parameters passed to function')
  of ERROR:TooManyListeningSockets
    return('Too many listening sockets. The maximum has been reached')
  of ERROR:SocketDead
    return('Socket dead. Could not find socket in listening queue')
  of ERROR:IncompatiblePacketVersion
    return('An incompatible packet version was received')
  of ERROR:MachineHasNoIP
    return('This machine has no IP address')
  of ERROR:ServiceNowInactive
    return('The service is marked as inactive, because a previous error occurred')
  of ERROR:SimpleConnectNotClosed
    return('The simple connection was not closed properly')
  of ERROR:TooManySendingSockets
    return('Too many sending sockets. The maximum has been reached')
  of ERROR:QueueAccessError
    return('An error occurred while trying to access a memory queue (read or write error). The queue could be full or there is not enough memory')
  of ERROR:CouldNotLoad
    return('An error occurred while trying to load the NetTalk DLL. The DLL could not be loaded.')
  of ERROR:CouldNotAllocMemory
    return('Unable to allocate the requested memory')
  of ERROR:NoIPorSocketSpecified
    return ('Host or Socket not specified. Please set the <<object>.packet.ToIp and <<object>.packet.OnSocket before sending data.')
  of ERROR:ClientNotConnected
    return ('Could not find connected client in Simple Connect Client or Server')
  of ERROR:FileAccessError
    return ('An error occurred while trying to access a file')
  of ERROR:ConnectionNotOpen
    return ('The connection is not open')
  of ERROR:ObjectRequiresMoreProperties
    return ('The NetTalk object requires more properties to contain valid values')
  of ERROR:NoLongerSupported
    return ('This functionality is no longer supported')
  of ERROR:CouldNotConnectToHost
    return ('Could not open a connection to the specified host machine')
  of ERROR:SimpleDataNotSent
    return ('Could not send NetSimple Data')
  of ERROR:GeneralFTPError
    return ('a General FTP error occurred')
  of ERROR:InvalidStringSize               ! -42
    return ('Invalid String Size')
  of ERROR:DUNNoPhoneBookEntryCantDial     ! -43
    return ('No Phone Book Entry. You need a Phone Book Entry before you can dial.')
  of ERROR:DUNObjectNotInDLLQueue          ! -44
    return ('The Ras Object is not in the DLL queue.')
  of ERROR:DUNObjectBusy                   ! -45
    return ('The Ras Object is busy opening another RAS connection.')
  of ERROR:CouldNotLoadDUNProcedures       ! -46
    return ('Could Not Load the Dial-Up Networking Windows Procedures. DUN will be disabled.')
  of ERROR:WinSockNotActive                ! -47
    return ('Could not load the WinSock DLL. NetTalk needs this to be able to work.')
  of ERROR:CallBackWindowAlreadyClosed     ! -48
    return ('Call Back Window Already Closed')
  of ERROR:NoNewThread                     ! -49
    return ('Could not create new thread to start NetTalk DLL Callback Window')
  of ERROR:CouldNotLoadProcedures          ! -50
    return ('Could not load some run-time procedures')
  of ERROR:CorruptPacket                   ! -51
    return ('Received a corrupt packet')
  of ERROR:NoPostingAllowed                ! -52
    return ('No Posting allowing')
  of ERROR:OpenTimeOut                     ! -53 (14/10/2003 - was previously ERROR:TimeOut)
    return ('Open Timeout or Failure error')
  of ERROR:PingUnSuccessful                ! -54
    return ('Ping unsuccessful')
  of ERROR:AlreadyOpen                     ! -55
    return ('Connection already open')
  of ERROR:DllNotActive                    ! -56 (13/12/2002 - was previously (-1))
    return('NetTalk DLL not actively initiated')
  of ERROR:IdleTimeOut                     ! -57
    return('Connection was closed because it was idle (inactive)')
  of ERROR:AlreadyBusy                     ! -58
    return('Already busy with a command')
  of ERROR:NoDataReceived                  ! -59
    return('No Data was received')
  of ERROR:ObjectInitNotCalled             ! -60
    return('Object''s Init method not called before the Object was used')
  of ERROR:SSLGeneralFailure               ! -61
    return('SSL General Failure')
  of ERROR:SSLFailedToConnect              ! -62
    return('SSL Client failed to Connect')
  of ERROR:SSLFailedTolisten               ! -63
    return('SSL Server failed to Listen')
  of ERROR:SSLFailedToAccept               ! -64
    return('SSL Server failed to Accept New Connection')
  of ERROR:SSLFailedToLoadCertificate      ! -65
    return('SSL Failed to Load Certificate')
  of ERROR:SSLFailedToLoadPrivateKey       ! -66
    return('SSL Failed to Load Private Key')
  of ERROR:SSLLocalCertOrPrivateKeyInvalid ! -67
    return('SSL Local Certificate or Private Key are Invalid')
  of ERROR:SSLFailedToLoadCARootFile       ! -68
    return('SSL Failed to Load CA Root File')
  of ERROR:SSLInvalidRemoteCertificate     ! -69
    return('The SSL Remote Certificate Failed Verification')
  of ERROR:SSLInvalidCertificateDates      ! -70
    return('The SSL Remote Certificate Had Invalid Dates')
  of ERROR:SSLInvalidCertificateCommonName ! -71
    return('The SSL Remote Certificate Common Name did not match the Server Name')
  of ERROR:SSLIncorrectDLLVersion          ! -72
    return('SSL Incorrect SSL DLL Version')
  of ERROR:SSLCouldNotLoadDLL              ! -73
    return('SSL Could Not Load SSL DLLs [LIBEAY32.DLL, LIBSSL32.DLL, MSVCR71.DLL]')
  of ERROR:SSLWriteFailed                  ! -74
    return('SSL Write Failed')
  of ERROR:SSLReadFailed                   ! -75
    return('SSL Read Failed')
  of ERROR:NetTalkObjectAndDLLDontMatch    ! -90
    return('NetTalk Object and DLL versions do not match')
  of ERROR:ErrorStoredInWinSockError       ! -91
    return('NetTalk WinSock Error (' & self.WinSockError & ')')
  of ERROR:ErrorStoredInSSLError           ! -92
    return('NetTalk SSL Error (' & self.SSLError & ')')
  end


  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('_NetAll.InterpretError', 'Not an object level error - need to ask the NetTalk DLL (NetErrorStr)', NET:LogError)
    end
  ****

  tempErrorStr = clip (NetErrorStr (self.error)) & ' [listed by NetErrorStr]'

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('_NetAll.InterpretError', 'Error = ' & self.error & ' NetErrorStr returned : ' & clip(tempErrorStr), NET:LogError)
    end
  ****
  return (tempErrorStr)

! this is a simple error handler - which converts an error code into a string.
! this is handled in one place to make it easy to support different messages, or
! different languages etc...

!------------------------------------------------------------------------------
_NetAll._Wait        PROCEDURE  (long p_Count=-60)         ! Declare Procedure 3
  compile('***',_VER_C60)
loc:MemAddress             long,auto
  ***
  CODE
  compile('***',_VER_C60)  ! ---- Clarion 6 only -----
    if self._NoCritSectionPointer
      return
    end
    if self._NetCritical &= NULL
      loc:MemAddress = NetGet_NetCriticalSection() ! Get the address of the Nettalk DLL Critical Section
      if loc:MemAddress = 0
        self._NoCritSectionPointer = 1
        self.Log ('_NetAll.Wait', 'Error. Could not get address of Critical Section from the NetTalk DLL.', NET:LogError)
        return ! Bail out
      end
      self._NetCritical &= (loc:MemAddress)
    end
    self._NetCritical.Wait(p_Count)
  ***
!------------------------------------------------------------------------------
_NetAll._Release     PROCEDURE  (long p_Count=-61)         ! Declare Procedure 3
  CODE
  compile('***',_VER_C60)
    ! Clarion 6 only !
    if self._NoCritSectionPointer
      return
    end
    if self._NetCritical &= NULL
      self.Log ('_NetAll.Release', 'Error. Calling _NetAll._Release() before calling Wait().', NET:LogError)
      return ! Bail out
    end
    self._NetCritical.Release(p_Count)
  ***
!------------------------------------------------------------------------------
_NetAll._LoggingSetup PROCEDURE  ()                        ! Declare Procedure 3
DoneOnce    long,static
  CODE
  ! ----------------------- Logging options ------------------------------------------
  compile ('**123**',NETTALKLOGALWAYS)  ! If this define is set the application will always
  if DoneOnce = false                   ! produce logging. Only use when you absolutely can't
    if command ('/netnolog')            ! use the command line parameter /netall as this
      ! Do nothing                      ! approach will be slower
    else
      setcommand (clip (command('')) & ' /netdebugtrace /netdlllogall /nettalklog')
      NetTurnOnLogging() ! DLL Logging
    end
  end
  **123**

  compile ('****', NETTALKLOG=1)
  if gNetLogging.LogCommandOptionsSet = false
    gNetLogging.LogCommandOptionsSet = true
    if command ('/netnolog')
      ! Do nothing
    else
      if command ('/nettalklog') or command ('/netall')   ! Turn on NetTalk Logging
        gNetLogging.LoggingOn = 1
        self.LoggingOn = gNetLogging.LoggingOn
      end
      if command ('/nettalklogerrors') or command ('/neterrors')
        gNetLogging.LoggingErrorsOnly = 1
        self.LoggingErrorsOnly = gNetLogging.LoggingErrorsOnly
        self.Log ('', '======== Object Logging : Errors only (/nettalklogerrors) ========', NET:LogError)
      end
    end
  end
  self.LoggingOn = gNetLogging.LoggingOn
  self.LoggingErrorsOnly = gNetLogging.LoggingErrorsOnly
  ****

  compile ('**123**',NETTALKLOGALWAYS)
  if DoneOnce = false
    DoneOnce = true
    if command ('/netnolog')
      ! Do nothing
    else
      self.Log ('_NetAll._LoggingSetup', 'NETTALKLOGALWAYS=>1')
    end
  end
  **123**

  compile ('****', NETTALKLOG=1)
  if self.LoggingOn
    self.Log ('_NetAll._LoggingSetup', 'Initialisation')
  end
  ****

  ! ----------------------- End Logging options --------------------------------------

!------------------------------------------------------------------------------
_NetAll._LoggingFirstTime PROCEDURE  ()                    ! Declare Procedure 3
  CODE
  self.Log ('', 'Using NetTalk (Object) version ' & NETTALK:VERSION & ' in application ' & lower(clip(command(0))))
  self.Log ('', 'NetTalk (Object) Information:' & |
                ' Net:MaxBinData = ' & NET:MaxBinData & |
                ' Size(Net:PacketType) = ' & Size(Net:AutoPacketType) & |
                ' Size(Net:PublicServicesQType) = ' & Size(Net:AutoPublicServicesQType) & |
                ' Size(Net:ServiceType) = ' & Size(Net:AutoServiceType) & |
                ' Size(Net:RemoteType) = ' & Size(Net:AutoRemoteType))
  FoundClarionVersion_# = False
  compile ('**-**', _VER_C73)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 7.3')
    end
  **-**
  compile ('**-**', _VER_C72)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 7.2')
    end
  **-**
  compile ('**-**', _VER_C71)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 7.1')
    end
  **-**
  compile ('**-**', _VER_C70)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 7.0')
    end
  **-**
  compile ('**-**', _VER_C65)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 6.5')
    end
  **-**
  compile ('**-**', _VER_C64)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 6.4')
    end
  **-**
  compile ('**-**', _VER_C63)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 6.3')
    end
  **-**
  compile ('**-**', _VER_C62)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 6.2')
    end
  **-**
  compile ('**-**', _VER_C61)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 6.1')
    end
  **-**
  compile ('**-**', _VER_C60)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 6.0')
    end
  **-**
  compile ('**-**', _VER_C55)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 5.5')
    end
  **-**
  compile ('**-**', _VER_C5)
    if FoundClarionVersion_# = false
      FoundClarionVersion_# = true
      self.Log ('', 'NetTalk (Object) using Clarion 5.0')
    end
  **-**


!------------------------------------------------------------------------------
_NetAll.Construct    PROCEDURE  ()                         ! Declare Procedure 3
  CODE
  self._LoggingSetup()
!------------------------------------------------------------------------------
_NetAll._Trace                 Procedure (String p_String)
  Code
  self.Trace(p_String)
!------------------------------------------------------------------------------
_NetAll.Trace                  Procedure (String p_String)
ds  StringTheory
  code
  ds.SetValue('[netTalk][thread=' & thread() & '] ' & clip(p_String))
  ds.trace()

!------------------------------------------------------------------------------
NetDUN.Init          PROCEDURE                             ! Declare Procedure 4
  CODE
  self.ControlTemplateINIFile = '.\netdemo.ini'     ! You can change this in your
                                                    ! objects .init method after the parent call

  self._ConnectionsQ &= new (NetDUNConnectionsQType)
  self.ConnectionsQ &= self._ConnectionsQ
  self._DunEntriesQ &= new (NetDUNDunEntriesQType)
  self.DunEntriesQ &= self._DunEntriesQ
  self._DevicesQ &= new (NetDUNDevicesQType)
  self.DevicesQ &= self._DevicesQ

  self.prime()

  if self.UseThisThread <> 0                    ! New 6 Feb 2002
    self._Thread = self.UseThisThread
  else
    self._Thread = thread()                     ! the thread to post the message to
  end

  NetDUNObjectManager(NetDUN:ADD, self._Thread, self._Event)

!------------------------------------------------------------------------------
NetDUN.Kill          PROCEDURE                             ! Declare Procedure 4
  CODE
  free (self.ConnectionsQ)
  if ~self._ConnectionsQ &= NULL
    dispose (self._ConnectionsQ)                              ! Free the allocated Memory of Queue
  end
  free (self.DunEntriesQ)
  if ~self._DunEntriesQ &= NULL
    dispose (self._DunEntriesQ)                                ! Free the allocated Memory of Queue
  end
  free (self.DevicesQ)
  if ~self._DevicesQ &= NULL
    dispose (self._DevicesQ)                                  ! Free the allocated Memory of Queue
  end

  NetDUNObjectManager(NetDUN:Delete, self._Thread, self._Event)
  NetFreeUniqueEvent(self._Event)


!------------------------------------------------------------------------------
NetDUN.GetConnections PROCEDURE                            ! Declare Procedure 4
ErrorHappenedBefore   byte, static
  CODE
  self.error = 0

  self.error = NetDUNGetConnections(self.ConnectionsQ)
  if self.error
    if ErrorHappenedBefore = 0
      ErrorHappenedBefore = 1
      self.ErrorTrap ('Could not get the list of current connections. (This error will only be reported once)', 'NetDUN.GetConnection')
    end
  end
  if records (self.ConnectionsQ) > 0
    self.error = NetDUNGetConnectionsStatus(self.ConnectionsQ)
    if self.error
      if ErrorHappenedBefore = 0
        ErrorHappenedBefore = 1
        self.ErrorTrap ('Could not get the list of current connections. (This error will only be reported once)', 'NetDUN.GetConnection')
      end
    end
  end
!------------------------------------------------------------------------------
NetDUN.GetDunEntries PROCEDURE                             ! Declare Procedure 4
ErrorHappenedBefore  byte,static
  CODE
  self.error = 0
  self.error = NetDUNGetDUNs(self.DunEntriesQ)
  if self.error
    if ErrorHappenedBefore = 0
      ErrorHappenedBefore = 1
      self.ErrorTrap ('Could not get the list of Dialup Entries. (This error will only be reported once)', 'NetDUN.GetDunEntries')
    end
  end

!------------------------------------------------------------------------------
NetDUN.GetDevices    PROCEDURE                             ! Declare Procedure 4
ErrorHappenedBefore   byte, static
  CODE
  self.error = 0
  self.error = NetDUNGetDevices(self.DevicesQ)
  if self.error
    if ErrorHappenedBefore = 0
      ErrorHappenedBefore = 1
      self.ErrorTrap ('Could not get the list of devices. (This error will only be reported once)', 'NetDUN.GetDevices')
    end
  end


!------------------------------------------------------------------------------
NetDUN.Dial          PROCEDURE  ()                         ! Declare Procedure 3
Handle  long ! 20/7/2005 - was ulong
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetDUN.Dial', 'Will try and Dial.' & |
                ' PhoneNumber = ' & clip (self.DunEntriesQ.PhoneNumber) & |
                ' EntryName = ' & clip (self.DunEntriesQ.entryName) & |
                ' UserName = ' & clip (self.DunEntriesQ.UserName))
    end
  ****

  self.error = 0

  self.error = NetDUNDialDUN(self._Thread, self._Event, self.DunEntriesQ.entryName, self.DunEntriesQ.PhoneNumber, self.DunEntriesQ.UserName, self.DunEntriesQ.Password, self.DunEntriesQ.Domain, Handle)
  if self.error
    self.ErrorTrap ('Could not dial', 'NetDUN.Dial')
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetDUN.Dial', 'Handle returned by DLL Dial function = ' & Handle)
    end
  ****

  return (Handle)
!------------------------------------------------------------------------------
NetDUN.HangUp        PROCEDURE                             ! Declare Procedure 4
  CODE
  ! Hangs up the highlighted connection
  self.error = 0
  if records(self.connectionsQ) > 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetDUN.HangUp', 'Attempting to HangUp. Dial Handle ' & self.ConnectionsQ.handle)
      end
    ****
    self.error = NetDUNHangUp(self.ConnectionsQ.handle)
    if self.error
      self.ErrorTrap ('could not hangup dialup connection', 'NetDUN.HangUp')
    end
  end
!------------------------------------------------------------------------------
NetDUN.TakeEvent     PROCEDURE                             ! Declare Procedure 4
local             group, pre (loc)
AliveConnections    long
x                   long
                  end
  CODE
  if event() = self._Event
    self.GetConnections()    ! Update this queue first
    loc:AliveConnections = 0
    loop loc:x = 1 to records (self.ConnectionsQ)
      get (self.ConnectionsQ, loc:x)
      if errorcode() = 0
        if self.ConnectionsQ.State = 8192
          loc:AliveConnections += 1
        end
      end
    end
    clear (self.ConnectionsQ)
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetDUN.TakeEvent', 'Calling ConnectionsChanged() method. Alive Connection count = ' & loc:AliveConnections)
      end
    ****
    self.ConnectionsChanged(loc:AliveConnections)
  elsif event() = (self._Event+1)
    ! This occurs when the Dial() method has been called and the DLL detects that
    ! the connection has failed. The DLL calls the HangUp for you, but ErrorTrap will be called.
    self.GetConnections()    ! Update this queue first
    NetDUNGetLastError (self._Thread, self._Event, self.LastError)
    self.error = 0
    self.ErrorTrap(clip ('Dial-up failed. ' & clip (self.LastError.LastStateStr) & ' : ' & |
                   clip (self.LastError.LastErrorStr)), 'NetDUN.TakeEvent')
  end
!------------------------------------------------------------------------------
NetDUN.NewDunEntry   PROCEDURE                             ! Declare Procedure 4
  CODE
  self.error = 0
  self.error = NetDUNAddDUN()
  if self.error
    self.ErrorTrap ('Could not add a new entry to the dialup entries', 'NetDUN.NewDunEntry')
  end


!------------------------------------------------------------------------------
NetDUN.EditDunEntry  PROCEDURE                             ! Declare Procedure 4
  CODE
  self.error = 0
  if records (self.DunEntriesQ) > 0
    self.error = NetDUNChangeDUN(self.DunEntriesQ.EntryName)
    if self.error
      self.ErrorTrap ('Could not edit one of the dialup entries', 'NetDUN.EditDunEntry')
    end
  end
!------------------------------------------------------------------------------
NetDUN.DeleteDunEntry PROCEDURE                            ! Declare Procedure 4
  CODE
  self.error = 0
  if records (self.DunEntriesQ) > 0
    self.error = NetDUNDeleteDUN(self.DunEntriesQ.EntryName)
    if self.error
      self.ErrorTrap ('Could not delete one of the dialup entries', 'NetDUN.DeleteDunEntry')
    end
  end

!------------------------------------------------------------------------------
NetDUN.RenameDunEntry PROCEDURE  (string p_NewName)        ! Declare Procedure 3
  CODE
  self.error = 0
  if records (self.DunEntriesQ) > 0
    self.error = NetDUNRenameDUN(self.DunEntriesQ.EntryName, p_NewName)
    if self.error
      self.ErrorTrap ('Could not rename one of the dialup entries', 'NetDUN.RenameDunEntry')
    end
  end

!------------------------------------------------------------------------------
NetDUN.Prime         PROCEDURE                             ! Declare Procedure 4
  CODE
  self._Wait()
  assert (~self._initalised) ! jono 16/6/1999

  if ~self._initalised
    self._Event = NetGetUniqueEvent()
    if self._Event = 0
      compile ('****', NETTALKLOG=1)
        self.Log ('NetDUN.Prime', 'Error getting new unique event. None was available.', NET:LogError)
      ****
    end
    self._initalised = 1
  end
  self._Release()

!------------------------------------------------------------------------------
NetDUN.ConnectionsChanged PROCEDURE  (long p_AliveConnections) ! Declare Procedure 3
  CODE
  ! Notification Method
!------------------------------------------------------------------------------
NetDUN.InterpretError PROCEDURE  ()                        ! Declare Procedure 3
  CODE
! this is a simple error handler - which converts an error code into a string.
! this is handled in one place to make it easy to support different messages, or
! different languages etc...


  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetDUN.InterpretError', 'Looking up Error. self.error = ' & self.error)
    end
  ****

  if self.Error = 0 then return('').

  execute (self.error-599)
    return(' An operation is pending')
    return(' The port handle is invalid')
    return(' The port is already open')
    return(' Caller''s buffer is too small')
    return(' Wrong information specified')
    return(' Cannot set port information')
    return(' The port is not connected')
    return(' The event is invalid')
    return(' The device does not exist')
    return(' The device type does not exist')
    return(' The buffer is invalid')
    return(' The route is not available')
    return(' The route is not allocated')
    return(' Invalid compression specified')
    return(' Out of buffers')
    return(' The port was not found')
    return(' An asynchronous request is pending')
    return(' The port or device is already disconnecting')
    return(' The port is not open')
    return(' The port is disconnected')
    return(' There are no endpoints')
    return(' Cannot open the phone book file')
    return(' Cannot load the phone book file')
    return(' Cannot find the phone book entry')
    return(' Cannot write the phone book file')
    return(' Invalid information found in the phone book file')
    return(' Cannot load a string')
    return(' Cannot find key')
    return(' The port was disconnected')
    return(' The data link was terminated by the remote machine')
    return(' The port was disconnected due to hardware failure')
    return(' The port was disconnected by the user')
    return(' The structure size is incorrect')
    return(' The port is already in use or is not configured for Remote Access dial out')
    return(' Cannot register your computer on on the remote network')
    return(' Unknown error')
    return(' The wrong device is attached to the port')
    return(' The string could not be converted')
    return(' The request has timed out')
    return(' No asynchronous net available')
    return(' A NetBIOS error has occurred')
    return(' The server cannot allocate NetBIOS resources needed to support the client')
    return(' One of your NetBIOS names is already registered on the remote network')
    return(' A network adapter at the server failed')
    return(' You will not receive network message popups')
    return(' Internal authentication error')
    return(' The account is not permitted to logon at this time of day')
    return(' The account is disabled')
    return(' The password has expired')
    return(' The account does not have Remote Access permission')
    return(' The Remote Access server is not responding')
    return(' Your modem (or other connecting device) has reported an error')
    return(' Unrecognized response from the device')
    return(' A macro required by the device was not found in the device .INF file section')
    return(' A command or response in the device .INF file section refers to an undefined macro')
    return(' The <<message> macro was not found in the device .INF file secion')
    return(' The <<defaultoff> macro in the device .INF file section contains an undefined macro')
    return(' The device .INF file could not be opened')
    return(' The device name in the device .INF or media .INI file is too long')
    return(' The media .INI file refers to an unknown device name')
    return(' The device .INF file contains no responses for the command')
    return(' The device .INF file is missing a command')
    return(' Attempted to set a macro not listed in device .INF file section')
    return(' The media .INI file refers to an unknown device type')
    return(' Cannot allocate memory')
    return(' The port is not configured for Remote Access')
    return(' Your modem (or other connecting device) is not functioning')
    return(' Cannot read the media .INI file')
    return(' The connection dropped')
    return(' The usage parameter in the media .INI file is invalid')
    return(' Cannot read the section name from the media .INI file')
    return(' Cannot read the device type from the media .INI file')
    return(' Cannot read the device name from the media .INI file')
    return(' Cannot read the usage from the media .INI file')
    return(' Cannot read the maximum connection BPS rate from the media .INI file')
    return(' Cannot read the maximum carrier BPS rate from the media .INI file')
    return(' The line is busy')
    return(' A person answered instead of a modem')
    return(' There is no answer')
    return(' Cannot detect carrier')
    return(' There is no dial tone')
    return(' General error reported by device')
    return(' ERROR_WRITING_SECTIONNAME')
    return(' ERROR_WRITING_DEVICETYPE')
    return(' ERROR_WRITING_DEVICENAME')
    return(' ERROR_WRITING_MAXCONNECTBPS')
    return(' ERROR_WRITING_MAXCARRIERBPS')
    return(' ERROR_WRITING_USAGE')
    return(' ERROR_WRITING_DEFAULTOFF')
    return(' ERROR_READING_DEFAULTOFF')
    return(' ERROR_EMPTY_INI_FILE')
    return(' Access denied because username and/or password is invalid on the domain')
    return(' Hardware failure in port or attached device')
    return(' ERROR_NOT_BINARY_MACRO')
    return(' ERROR_DCB_NOT_FOUND')
    return(' ERROR_STATE_MACHINES_NOT_STARTED')
    return(' ERROR_STATE_MACHINES_ALREADY_STARTED')
    return(' ERROR_PARTIAL_RESPONSE_LOOPING')
    return(' A response keyname in the device .INF file is not in the expected format')
    return(' The device response caused buffer overflow')
    return(' The expanded command in the device .INF file is too long')
    return(' The device moved to a BPS rate not supported by the COM driver')
    return(' Device response received when none expected')
    return(' The Application does not allow user interaction. The connection requires interaction with the user to complete successfully. ')
    return(' ERROR_BAD_CALLBACK_NUMBER')
    return(' ERROR_INVALID_AUTH_STATE')
    return(' ERROR_WRITING_INITBPS')
    return(' X.25 diagnostic indication')
    return(' The account has expired')
    return(' Error changing password on domain.  The password may be too short or may match a previously used password')
    return(' Serial overrun errors were detected while communicating with your modem')
    return(' RasMan initialization failure.  Check the event log')
    return(' Biplex port initializing.  Wait a few seconds and redial')
    return(' No active ISDN lines are available')
    return(' No ISDN channels are available to make the call')
    return(' Too many errors occurred because of poor phone line quality')
    return(' The Remote Access IP configuration is unusable')
    return(' No IP addresses are available in the static pool of Remote Access IP addresses')
    return(' Timed out waiting for a valid response from the remote PPP peer')
    return(' PPP terminated by remote machine')
    return(' No PPP control protocols configured')
    return(' Remote PPP peer is not responding')
    return(' The PPP packet is invalid')
    return(' The phone number including prefix and suffix is too long')
    return(' The IPX protocol cannot dial-out on the port because the machine is an IPX router')
    return(' The IPX protocol cannot dial-in on the port because the IPX router is not installed')
    return(' The IPX protocol cannot be used for dial-out on more than one port at a time')
    return(' Cannot access TCPCFG.DLL')
    return(' Cannot find an IP adapter bound to Remote Access')
    return(' SLIP cannot be used unless the IP protocol is installed')
    return(' Computer registration is not complete')
    return(' The protocol is not configured')
    return(' The PPP negotiation is not converging')
    return(' The PPP control protocol for this network protocol is not available on the server')
    return(' The PPP link control protocol terminated')
    return(' The requested address was rejected by the server')
    return(' The remote computer terminated the control protocol')
    return(' Loopback detected')
    return(' The server did not assign an address')
    return(' The authentication protocol required by the remote server cannot use the Windows NT encrypted password.  Redial, entering the password explicitly')
  end

  if self.Error = 123 then return('Invalid Entry Name').

  case self.Error
  of ERROR:DUNNoPhoneBookEntryCantDial     ! -43
    return ('No Phone Book Entry. You need a Phone Book Entry before you can dial')
  of ERROR:DUNObjectNotInDLLQueue          ! -44
    return ('The DUN Object is not in the DLL queue')
  of ERROR:DUNObjectBusy                   ! -45
    return ('The DUN Object is busy opening another DUN connection')
  end


  compile ('****', NETTALKLOG=1)
    self.Log ('NetDUN.InterpretError', 'Unknown error. self.error = ' & self.error, NET:LogError)
  ****
  return ('unknown DUN (DialUp) error')

! this is a simple error handler - which converts an error code into a string.
! this is handled in one place to make it easy to support different messages, or
! different languages etc...

!------------------------------------------------------------------------------
NetDUN.ErrorTrap     PROCEDURE  (string p_errorStr,string p_functionName) ! Declare Procedure 3
temp2        string (512)
  CODE
  temp2 = self.InterpretError()

  if self.error = 0
    self.errorString = clip(p_errorStr) & '.'
  else
    self.errorString = clip(p_errorStr) & '.' & ' The error number was ' & |
                       self.error & ' which means ' & clip(temp2) & '.'
  end

  if not (self.SuppressErrorMsg)
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetDUN.ErrorTrap', |
                  'Error = ' & self.error & |
                  ' = ' & clip(p_errorStr) & |
                  ' : ' & clip (temp2) & |
                  ' Function = ' & clip(p_functionName) & |
                  ' NetDUN Object' |
                  , NET:LogError)
      end
    ****
    if self.error = 0
      message('A NetTalk DUN (Dial-up) error has occurred.||Error = ' & clip(p_errorStr) & '.' & |
              '||Error occurred in function ' & clip(p_functionName) & |
              ' (NetDUN Object)' |
              , clip(NET:ErrorTitle), Icon:Asterisk)
    else
      message('A NetTalk DUN (Dial-up) error has occurred.||Error = ' & clip(p_errorStr) & '.' & |
              '||The error number was ' & self.error & |
              ' which means ' & clip(temp2) & '.' & |
              '||Error occurred in function ' & clip(p_functionName) & |
              ' (NetDUN Object)' |
              , clip(NET:ErrorTitle), Icon:Asterisk)
    end
  else                                       ! Only log to file
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetDUN.ErrorTrap', '<<no display> ' & |
                  'Error = ' & self.error & |
                  ' = ' & clip(p_errorStr) & |
                  ' : ' & clip (temp2) & |
                  ' Function = ' & clip(p_functionName) & |
                  ' NetDUN Object' |
                  ,NET:LogError)
      end
    ****
  end

  ! this is a simple error handler. It means every error received interrupts your program
  ! (which you may want to override). The actual message is provided by the
  ! InterpretError method, which can also be overridden.

!------------------------------------------------------------------------------
_NetUnicode.AnsiCStringToUnicodeCString PROCEDURE  ()      ! Declare Procedure 3
LastError          long
Flags              long
UnicodeSize        long
lpDefaultChar      long
lpUsedDefaultChar  long
SizeNeeded         long
  CODE
  if self.AnsiCString &= NULL
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('_NetUnicod.AnsiCStringToUnicodeCString', 'Warning. self.AnsiCString &= NULL.')
      end
    ****
    return
  end

  if size (self.AnsiCString) <= 0
    return
  end

  Flags = 0 ! (0 or NET:MB_PRECOMPOSED)

  if self.DontAllocateNewStrings = 0
    SizeNeeded = OS_MultiByteToWideChar(NET:CP_ACP,           |! CodePage NET:CP_ACP = ANSI or NET:CP_UTF8 = UTF-8 (Win98 or higher)
                               Flags,                         |! dwFlags
                               self.AnsiCString,              |! wide-character string
                               -1,                            |! number of chars in string (-1 = auto detect)
                               self.UnicodeCString,           |! buffer for new string
                               0)                             !! size of buffer
    self.UnicodeCString &= New(CString(SizeNeeded*2))
  end

  self.UnicodeCStringLen = OS_MultiByteToWideChar(NET:CP_ACP, |! CodePage NET:CP_ACP = ANSI or NET:CP_UTF8 = UTF-8 (Win98 or higher)
                               Flags,                         |! dwFlags
                               self.AnsiCString,              |! wide-character string
                               -1,                            |! number of chars in string (-1 = auto detect)
                               self.UnicodeCString,           |! buffer for new string
                               size (self.UnicodeCString))    !! size of buffer

  if self.UnicodeCStringLen > 0
    if BAND (Flags, NET:MB_PRECOMPOSED) <> 0
      ! UTF-8 - not tested yet
      self.UnicodeCStringLen = len (self.UnicodeCStringLen)   ! UTF-8 - just measure string len
    else
      ! UTF-16
      self.UnicodeCStringLen = (self.UnicodeCStringLen-1)*2   ! UTF-16 Remove one byte of length for the <0> terminator and *2 for 16bit per character
    end
  else
    LastError = OS_GetLastError()
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('_NetUnicod.AnsiCStringToUnicodeCString', 'Error = ' & LastError)
      end
    ****
  end

  ! Programmers: Remember to dispose self.AnsiCString yourself


!------------------------------------------------------------------------------
_NetUnicode.UnicodeCStringToAnsiCString PROCEDURE  ()      ! Declare Procedure 3
! Notes: BOM (Byte Order Mark)
! Bytes Encoding Form
! 00 00 FE FF    UTF-32, big-endian
! FF FE 00 00    UTF-32, little-endian
! FE FF          UTF-16, big-endian
! FF FE          UTF-16, little-endian
! EF BB BF       UTF-8

! Note UnicodeCString must be double NULL terminated (ie <0,0>)

LastError          long
Flags              long
UnicodeSize        long
lpDefaultChar      long
lpUsedDefaultChar  long
SizeNeeded         long

  CODE
  if self.UnicodeCString &= NULL
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('_NetUnicod.UnicodeCStringToAnsiCString', 'Warning. self.UnicodeCString &= NULL.')
      end
    ****
    return
  end

  if size (self.UnicodeCString) <= 0
    return
  end

  Flags = 0
  UnicodeSize = -1  ! (-1 = auto detect)
  lpDefaultChar = 0
  lpUsedDefaultChar = 0

  if self.DontRemoveBOM = 0
    if size (self.UnicodeCString) >= 2
      if self.UnicodeCString[1] = '<255>' and self.UnicodeCString[2] = '<254>' ! UTF-16 BOM
        if size (self.UnicodeCString) > 2
          self.UnicodeCString = self.UnicodeCString[3:size(self.UnicodeCString)] & '<0,0>'
        else
          self.UnicodeCString[1:2] = '<0,0>'
        end
      end
    end
  end

  if self.DontAllocateNewStrings = 0
    SizeNeeded = OS_WideCharToMultiByte(NET:CP_ACP, |! CodePage NET:CP_ACP = ANSI or NET:CP_UTF8 = UTF-8 (Win98 or higher)
                                 Flags,                        |! dwFlags
                                 self.UnicodeCString,          |! wide-character string
                                 UnicodeSize,                  |! number of chars in string
                                 self.AnsiCString,             |! buffer for new string
                                 0,                            |! size of buffer
                                 lpDefaultChar,                |! lpDefaultChar - default for unmappable chars - fastest when NULL
                                 lpUsedDefaultChar)            !! lpUsedDefaultChar - set when default char used - fastest when NULL
    self.AnsiCString &= New(CString(SizeNeeded))
  end

  self.AnsiCStringLen = OS_WideCharToMultiByte(NET:CP_ACP, |! CodePage NET:CP_ACP = ANSI or NET:CP_UTF8 = UTF-8 (Win98 or higher)
                               Flags,                      |! dwFlags
                               self.UnicodeCString,        |! wide-character string
                               UnicodeSize,                |! number of chars in string (-1 = auto detect)
                               self.AnsiCString,           |! buffer for new string
                               size (self.AnsiCString),    |! size of buffer
                               lpDefaultChar,              |! lpDefaultChar - default for unmappable chars - fastest when NULL
                               lpUsedDefaultChar)          !! lpUsedDefaultChar - set when default char used - fastest when NULL

  if self.AnsiCStringLen > 0
    self.AnsiCStringLen -= 1 ! Remove one byte of length for the <0> terminator
  else
    LastError = OS_GetLastError()
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('_NetUnicod.UnicodeCStringToAnsiCString', 'Error = ' & LastError)
        ! 87 = ERROR_INVALID_PARAMETER
        ! 122 = ERROR_INSUFFICIENT_BUFFER
        ! 1004 = ERROR_INVALID_FLAGS
      end
    ****
  end

  ! Programmers: Remember to dispose self.AnsiCString yourself

!------------------------------------------------------------------------------

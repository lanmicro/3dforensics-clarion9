! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software - www.capesoft.com
      Member()

         omit('***$***',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
         ***$***
   Include('Equates.CLW'),ONCE
   Include('Keycodes.CLW'),ONCE
   Include('Errors.CLW'),ONCE
   Map
     MODULE('C Library')
       mkdir (*CSTRING),SIGNED,PROC,RAW,NAME('_mkdir')
       CRC32(*String Buffer, ULong Bytes, ULong crc), ULong, Raw,Name('CLA$CRC32')
     END
   End ! map
   Include('NetWeb.inc'),ONCE
   include('NetMap.inc'),once
   include('StringTheory.Inc'),Once

   Compile('***',_ODDJOB_=1)
   include('oddjob.inc'),once
   ***

!------------------------------------------------------------------------------
! to see output in debugview use NETSHOWSEND=>1
!------------------------------------------------------------------------------

NetWebServer.Construct PROCEDURE
  CODE
  self._SessionQueue &= new (NetWebServerSessionQueueType)
  self._SessionDataQueue &= new (NetWebServerSessionDataQueueType)
  self._ConnectionDataQueue &= new (NetWebServerConnectionDataQueueType)
  self._BrowseIDQueue &= new (NetBrowseIDQueueType)
  self._SitesQueue          &= new (NetWebServerSiteQueueType)
  self._SettingsQueue       &= new (NetFormStateQueueType)
  self.AppPath = longpath(command(0))
  Loop until sub(self.AppPath,len(clip(self.AppPath)),1) = '\'
    self.AppPath = sub(self.AppPath,1,len(clip(self.AppPath))-1)
  End
  self.LogPath = clip(self.AppPath) & 'log'
  Self.phpPath = clip(self.AppPath) & 'php'
  self.WebLogName = 'weblog'
  self.InActiveTimeout = 6000    ! 60 seconds idle connection timeout
  self.LogDataBytes = 1500       ! Log first 1500 bytes of each send and receive
  self.CheckGarbageCollectEvery = 500       ! Check Garbage Collector every 5 seconds
  !self._UnknownContentType = 'application/unknown' ! assume the unknown file is binary and needs to be saved. But fails for pages that have no extension (and are text).
  self._UnknownContentType = 'text/html'
  self.SetDefaultHeaderDetails()

  clear(self._SitesQueue.defaults)
  self._SitesQueue.defaults.AppPath = self.AppPath
  self._SitesQueue.defaults.PhpPath = self.PhpPath
  self._SitesQueue.defaults.WebFolderPath = clip(self._SitesQueue.defaults.AppPath) & 'web'  ! Default Web Path Folder.
  self._SitesQueue.defaults.UploadsPath = clip(self._SitesQueue.defaults.AppPath) & 'web\uploads'  ! Default Web Path Folder.
  self._SitesQueue.defaults.DataPath = self._SitesQueue.defaults.AppPath
  self._SitesQueue.defaults.DefaultPage = 'index.htm'
  self._SitesQueue.defaults.scriptsdir = 'scripts'
  self._SitesQueue.defaults.stylesdir = 'styles'
  self._SitesQueue.defaults.themesdir = 'themes'

  self._SitesQueue.defaults.securedir = 'secure'
  self._SitesQueue.defaults.loggedindir = 'loggedin'
  self._SitesQueue.defaults.HtmlCharset = 'ISO-8859-1'
  self._SitesQueue.defaults.StoreDataAs = net:StoreAsUTF8
  self._SitesQueue.defaults.SessionExpiryAfterHS = 15 * 60 * 100 ! 15 minutes
  self._SitesQueue.defaults.SessionLength = 50
  self._SitesQueue.defaults._CheckForParseHeader = 1         ! Check for Parse Header String
  self._SitesQueue.defaults._CheckForParseHeaderSize = 1000  ! Check for the Parse Header in the first x bytes
  self._SitesQueue.defaults._CheckParseHeader = '<!-- NetWebServer --><13,10>'

  self._SitesQueue.defaults.Loginpage = 'login.htm'
  Self._SitesQueue.defaults.DatePicture = '@D4' ! dd/mm/yyyy '@D17'
  Self._SitesQueue.defaults.WebFormStyle = Net:Web:Tab
  Self._SitesQueue.defaults.PageTitle = ''
  Self._SitesQueue.defaults.PreCompressed = 1
  Self._SitesQueue.defaults.LocatePromptText = 'Locate'
  Self._SitesQueue.defaults.LocatePromptTextPosition = 'Locate (Position)'
  Self._SitesQueue.defaults.LocatePromptTextBegins = 'Locate (Begins With)'
  Self._SitesQueue.defaults.LocatePromptTextContains = 'Locate (Contains)'
  Self._SitesQueue.defaults.InsertPromptText = 'Insert'
  Self._SitesQueue.defaults.CopyPromptText = 'Copy'
  Self._SitesQueue.defaults.ChangePromptText = 'Change'
  Self._SitesQueue.defaults.ViewPromptText   = 'View'
  Self._SitesQueue.defaults.DeletePromptText = 'Delete'
  Self._SitesQueue.defaults.RequiredText = 'Required'
  Self._SitesQueue.defaults.NumericText = 'A Number'
  Self._SitesQueue.defaults.MoreThanText = 'More than or equal to'
  Self._SitesQueue.defaults.LessThanText = 'Less than or equal to'
  Self._SitesQueue.defaults.NotZeroText = 'Must not be Zero or Blank'
  Self._SitesQueue.defaults.OneOfText = 'Must be one of'
  Self._SitesQueue.defaults.InListText = 'Must be one of'
  Self._SitesQueue.defaults.InFileText = 'Must be in table'
  Self._SitesQueue.defaults.DuplicateText = 'Creates Duplicate Record on'
  Self._SitesQueue.defaults.RestrictText = 'Unable to Delete, Child records exist in table'
  Self._SitesQueue.defaults.mobile.SupportMobile = 1
  self._SitesQueue.defaults.ChangeSessionOnLogInOut = true
  Add(self._SitesQueue,1)
  self.Performance.StartDate = today()
  self.Performance.StartTime = clock()
  self.MaxThreads = 100

!------------------------------------------------------------------------------

NetWebServer.Destruct PROCEDURE                       ! Declare Procedure 4
x            long
  CODE
  self._Wait()
  if self.LoggingOn
    self.Log ('NetWebServer.Destruct', 'Destructing')
  end
  if Not (self._SessionQueue &= Null)
    free (self._SessionQueue)
    dispose (self._SessionQueue)
    self._SessionQueue &= null
  end
  if not (self._BrowseIDQueue &= Null)
    free(self._BrowseIDQueue)
    dispose(self._BrowseIDQueue)
    self._BrowseIDQueue &= null
  end
  if Not (self._SessionDataQueue &= Null)
    loop x = 1 to records(self._SessionDataQueue)
      get(self._SessionDataQueue,x)
      if not self._SessionDataQueue.ExtValue &= Null
        dispose(self._SessionDataQueue.ExtValue)
      End
    end
    free (self._SessionDataQueue)
    dispose (self._SessionDataQueue)
    self._SessionDataQueue &= null
  end

  if Not (self._ConnectionDataQueue &= Null)
    self._FreeConnectionDataQueue()
    dispose (self._ConnectionDataQueue)
    self._ConnectionDataQueue &= null
  end

  if Not (self._SitesQueue &= Null)
    free(self._SitesQueue)
    dispose (self._SitesQueue)
    self._SitesQueue &= null
  end

  if Not (self._SettingsQueue &= Null)
    free(self._SettingsQueue)
    dispose (self._SettingsQueue)
    self._SettingsQueue &= null
  end

  self._Release()
!------------------------------------------------------------------------------

NetWebServer.Init    PROCEDURE  (uLong Mode=NET:SimpleClient) ! Declare Procedure 3
  CODE
  parent.Init (NET:SimpleServer)
!------------------------------------------------------------------------------

NetWebServer.Open    PROCEDURE                        ! Declare Procedure 4
! Not Thread Safe !! - Make sure you only call this method from one thread per object.
  CODE
  self._FreeConnectionDataQueue()
  ! Start - Not Thread Safe
  ! Not Thread Safe - but you should only be calling this method from one thread per object
  ! Can't wrap this function with Wait and Release as if it's the first NetTalk object, then
  ! the NetTalk DLL will deadlock, when it spawns it's new thread
  self.Open (clip(self._ServerIP), self.port)    ! self._ServerIP is normally blank - (Advanced) but could be set to a specific IP address if you wanted to bind the server only on one IP address
  ! End - Not Thread Safe

!------------------------------------------------------------------------------
NetWebServer.AddToIncoming Procedure(Long pSize,Long pContentLength=-1, Long pFlags=0)
  Code

!------------------------------------------------------------------------------
NetWebServer.RemoveFromIncoming Procedure(Long pFlag=0)
  Code

!------------------------------------------------------------------------------

NetWebServer.Process PROCEDURE
RequestData        Group(NetWebServerRequestDataType).
TempString         &String
x                  long
!y                  long
pos                long
NewSize            long
WholeRequestFound  long
TooBig             long
space1             long

  CODE
  self._Wait()
  case self.Packet.PacketType
  ! --------------------------
  of NET:SimpleNewConnection
  ! --------------------------
    if self.LoggingOn
      self.Log ('NetWebServer.Process', 'New Connection')
    end
    ! A new connection to our Server
    ! Just wait for the data to arrive
    clear (self._ConnectionDataQueue)
    self._ConnectionDataQueue.SockID = self.packet.SockID
    self._ConnectionDataQueue.Data &= NULL
    self._ConnectionDataQueue.DataLen = 0
    self._ConnectionDataQueue.BufferSize = 0
    add (self._ConnectionDataQueue, +self._ConnectionDataQueue.SockID)
    self.Performance.NumberofConnections = records(self._ConnectionDataQueue)
    If self.Performance.NumberofConnections > self.Performance.MaximumConnections
      self.Performance.MaximumConnections = self.Performance.NumberofConnections
    End
  ! --------------------------
  of NET:SimplePartialDataPacket
  ! --------------------------
    if self.LoggingOn then self.Log ('NetWebServer.Process', 'Data Packet').
    ! We received data
    self.PacketsReceived += 1

    if self.packet.binDataLen = 0
      !self.RemoveFromIncoming()
      !self.AbortServerConnection(self.packet.OnSocket, self.packet.SockID)
      self._Release()
      return ! Should never happen, but good coding anyway
    end

    clear (self._ConnectionDataQueue)
    self._ConnectionDataQueue.SockID = self.packet.SockID
    get (self._ConnectionDataQueue, self._ConnectionDataQueue.SockID)
    if errorcode() <> 0
      self.Log ('NetWebServer.Process', 'Error. Could not find data connection in the DataConnection queue', NET:LogError)
      self.RemoveFromIncoming()
      self.AbortServerConnection(self.packet.OnSocket, self.packet.SockID)
      self._Release()
      return
    end

    if self._ConnectionDataQueue.Ignore = true
      self.RemoveFromIncoming()
      self._Release()
      return
    end

    if self._ConnectionDataQueue.DataLen > 4
      pos = self._ConnectionDataQueue.DataLen - 4 ! position to start looking for the <13,10> in the Data
    else
      pos = 1                                     ! position to start looking for the <13,10> in the Data
    end

    ! Begin - Add Data to our DataConnectionQueue String
    do ResizeBuffer

    If TooBig
      self.Log ('NetWebServer.Process', 'Error. Incoming request is too big', NET:LogError)
      self.RemoveFromIncoming()
      self.error = 413
      self.AddLog(self._ConnectionDataQueue.Data[1:self._ConnectionDataQueue.DataLen],self.packet.FromIP)
      self.SendError(413,'Request entity too large','Incoming request is too big')
      self._Release()
      return
    end

    ! add the incoming data to the string
    self._ConnectionDataQueue.Data [(self._ConnectionDataQueue.DataLen+1) : (self._ConnectionDataQueue.DataLen + self.packet.binDataLen)] = self.packet.binData [1 : self.packet.binDataLen]
    self._ConnectionDataQueue.DataLen += self.packet.binDataLen
    self.AddToIncoming(self._ConnectionDataQueue.DataLen)
    ! End - Add Data to our DataConnectionQueue String

    if self._ConnectionDataQueue.RequestMethodType = NetWebServer_NOTDETERMINED ! ie this is the first packet
      if self._ConnectionDataQueue.DataLen >= 4
        space1 = instring(' ',self._ConnectionDataQueue.Data,1,1)
        if space1 > 0 and space1 < 10
          ! Check binData
          self._ConnectionDataQueue.RequestMethodType = self.SetRequestMethodType(upper(self._ConnectionDataQueue.Data[1 : space1]))
          if self._ConnectionDataQueue.RequestMethodType = NetWebServer_NOTDETERMINED
            self.RemoveFromIncoming()
            self.error = 501
            self.AddLog(self._ConnectionDataQueue.Data[1:self._ConnectionDataQueue.DataLen],self.packet.FromIP)
            self.SendError(501,'Not implemented','The requested Method is not implemented')
            self._Release()
            return
          end
        end
      end
    end
    if self._ConnectionDataQueue.expect = '' and self._ConnectionDataQueue.HeaderEndPosition = 0
      self._ConnectionDataQueue.expect = self._GetHeaderField ('Expect:', self._ConnectionDataQueue.Data, 1, self._ConnectionDataQueue.DataLen, 1, 1)
    end

    WholeRequestFound = false
    if self._ConnectionDataQueue.HeaderEndPosition = 0
      x = instring ('<13,10><13,10>', self._ConnectionDataQueue.Data[1 : self._ConnectionDataQueue.DataLen], 1, pos)  ! Look for request terminating character
      if x > 0
        ! Found Header End Marker
        self._ConnectionDataQueue.HeaderEndPosition = x + 3
        case self._ConnectionDataQueue.RequestMethodType
        of NetWebServer_POST orof NetWebServer_PUT
          self.AddLog(self._ConnectionDataQueue.Data[1:x],self.packet.FromIP)
          self._ConnectionDataQueue.ContentLength = self._GetHeaderField ('Content-Length:', self._ConnectionDataQueue.Data, 1, self._ConnectionDataQueue.DataLen, 1, 1)
          if self._ConnectionDataQueue.ContentLength > 0 and self.MaxPostSize > 0 and self._ConnectionDataQueue.ContentLength > self.MaxPostSize
            self.Log ('NetWebServer.Process', 'Error. Incoming request is too big', NET:LogError)
            self.RemoveFromIncoming()
            self.error = 413
            self.AddLog(self._ConnectionDataQueue.Data[1:self._ConnectionDataQueue.DataLen],self.packet.FromIP)
            self.SendError(413,'Request entity too large','Incoming request is too big')
            self._Release()
            return
          end
          self.AddToIncoming(self._ConnectionDataQueue.DataLen,self._ConnectionDataQueue.ContentLength)
          self._ConnectionDataQueue.chunked = Instring('chunked',lower(self._GetHeaderField ('Transfer-Encoding:', self._ConnectionDataQueue.Data, 1, self._ConnectionDataQueue.DataLen, 1, 1)),1,1)
        else
          WholeRequestFound = true
        end
      end
    end
    if WholeRequestFound = false and self._ConnectionDataQueue.HeaderEndPosition
      ! Found Header End Marker
      case self._ConnectionDataQueue.RequestMethodType
      of NetWebServer_POST orof NetWebServer_PUT
        ! Internet Explorer isn't shy to give you 2 bytes more than what you expect.
        ! But Mozilla/FireFox won't do this.
        If self._ConnectionDataQueue.chunked
          do ParseIncoming
          if WholeRequestFound
            self._UnChunk(self._ConnectionDataQueue.Data,self._ConnectionDataQueue.DataLen)
          end
        else
          if self._ConnectionDataQueue.DataLen >= (self._ConnectionDataQueue.ContentLength + self._ConnectionDataQueue.HeaderEndPosition)
            WholeRequestFound = true
          end
        end
      end
    end
    if lower(left(self._ConnectionDataQueue.expect)) = '100-continue'
       self._ConnectionDataQueue.expect = '100-continue (sent)'
       self.packet.binData = 'HTTP/1.1 100 Continue<13,10><13,10>'
       self.packet.binDataLen = len (clip(self.packet.binData))
       self.packet.ToIP = self.packet.FromIP
       self.Send()
    end
    if WholeRequestFound
      ! Okay, we've got a whole request in
      self.RemoveFromIncoming()
      do SetRequestData
      self._ConnectionDataQueue.DataLen = 0
      self._ConnectionDataQueue.Data &= NULL ! The original pointer will be disposed in _NetWebServerHandleRequest.Destruct, but we need to clear it now, so that we can receive new requests still on this connection, if the settings and the email client permit it.
      self._ConnectionDataQueue.BufferSize = 0
      put (self._ConnectionDataQueue)
      self.AddLog(RequestData.DataString,RequestData.FromIP)
      if self.LoggingOn
        self.Log ('NetWebServer.Process', 'Got a Request [' & RequestData.DataString[1 : RequestData.DataStringLen] & ']')
      end
      self.StatsRequestCount += 1
      If self._AlwaysRedirect = 0
        self.StartNewThread (RequestData)      ! Object's dervived code will open a new thread with a NetWebServerHandleRequest object on it
      Else
        !  auto-redirection from a non-secure to a secure site (on the same machine.)
        !self._Redirect(RequestData,,Net:Web:ToSecure,Net:Web:Repost,Net:Web:PermRedirect)
        self._Redirect(RequestData,,Net:Web:ToSecure,Net:Web:CleanPost,Net:Web:PermRedirect)
        self.CloseServerConnection(self.packet.OnSocket, self.packet.SockID)
        self._ConnectionDataQueue.Ignore = true
        put (self._ConnectionDataQueue)
      End
    else
      put (self._ConnectionDataQueue)
      ! And keep waiting for more of the request to come through (ie We've only received a part of it so far)
    end

  ! --------------------------
  of NET:SimpleIdleConnection
  ! --------------------------
    ! Connection has been
    ! idle for period set
    ! in .InactiveTimeout
     if self.LoggingOn
        self.Log ('NetWebServer.Process', 'Idle incoming connection will be closed. SockID = ' & self.packet.SockID)
     end
     self.abortserverconnection(self.packet.OnSocket,self.packet.SockID)
  end

  if self.LoggingOn
    self.Log ('NetWebServer.Process', 'End Process')
  end
  self._Release()

SetRequestData  Routine
      clear (RequestData)
      RequestData.FromIP = self.packet.FromIP
      RequestData.OnSocket = self.packet.OnSocket
      RequestData.SockID = self.packet.SockID
      RequestData.DataString &= self._ConnectionDataQueue.Data
      RequestData.DataStringLen = self._ConnectionDataQueue.DataLen
      RequestData.WebServer &= self          ! Pointer to ourself
      RequestData.RequestMethodType = self._ConnectionDataQueue.RequestMethodType

ResizeBuffer  Routine
    if self._ConnectionDataQueue.BufferSize = 0
      NewSize = NET:MaxBinData                    ! 16K
    elsif self._ConnectionDataQueue.BufferSize >= (self._ConnectionDataQueue.DataLen + self.packet.BinDataLen)
      NewSize = 0                                 ! No new size needed. Buffer is big enough
    else
      NewSize = bshift(bshift(self._ConnectionDataQueue.BufferSize,-20)+1,+20)  ! Increment size in 1M blocks
      if self.MaxPostSize > 0 and NewSize > self.MaxPostSize
        TooBig = 1
        Exit
      end
      if NewSize < (self._ConnectionDataQueue.DataLen + self.packet.BinDataLen)
        NewSize = (self._ConnectionDataQueue.DataLen + self.packet.BinDataLen)
      end
    end
    if NewSize > 0
      if self.LoggingOn
        self.Log ('NetWebServer.Process', 'Incrementing BufferSize to ' & NewSize)
      end
      TempString &= self._ConnectionDataQueue.Data ! Temp Copy
      self._ConnectionDataQueue.Data &= New (String(NewSize))
      if self._ConnectionDataQueue.Data &= NULL
        self.AbortServerConnection(self.packet.OnSocket, self.packet.SockID)
        self._ConnectionDataQueue.Ignore = true
        put (self._ConnectionDataQueue)
        self._Release()
        return
      end
      if size (self._ConnectionDataQueue.Data) <> NewSize
        self.AbortServerConnection(self.packet.OnSocket, self.packet.SockID)
        self._ConnectionDataQueue.Ignore = true
        put (self._ConnectionDataQueue)
        self._Release()
        return
      end
      self._ConnectionDataQueue.BufferSize = NewSize
      if self._ConnectionDataQueue.DataLen > 0
        self._ConnectionDataQueue.Data [1 : self._ConnectionDataQueue.DataLen] = TempString [1 : self._ConnectionDataQueue.DataLen]
      end
      dispose (TempString)                   ! free old memory block
    end

! check to see if the whole request has arrived, terminating in a 0 length chunk.
! this routine could be a bit more sophisticated.
ParseIncoming  routine
  if instring ('<13,10><13,10>0<13,10><13,10>', self._ConnectionDataQueue.Data[1 : self._ConnectionDataQueue.DataLen],1,1)
    WholeRequestFound = TRUE
  end
!------------------------------------------------------------------------------
NetWebServer.SetRequestMethodType  Procedure(String pVerb)
  code
  case (pverb)
  of 'GET'
    return(NetWebServer_GET)
  of 'HEAD'
    return(NetWebServer_HEAD)
  of 'OPTIONS'
    return(NetWebServer_OPTIONS)
  of 'POST'
    return(NetWebServer_POST)
  of 'PUT'
    return(NetWebServer_PUT)
  of 'DELETE'
    return(NetWebServer_DELETE)
  of 'PROPFIND'
  orof 'FIND'
    return(NetWebServer_FIND)
  of 'PROPPATCH'
  orof 'PATCH'
    return(NetWebServer_PATCH)
  of 'REPORT'
    return(NetWebServer_REPORT)
  of 'TRACE'
    return(NetWebServer_TRACE)
  of 'CONNECT'
    return(NetWebServer_CONNECT)
  ELSE
    return(NetWebServer_NOTDETERMINED)
  end

!------------------------------------------------------------------------------
NetWebServer._Redirect   PROCEDURE(NetWebServerRequestDataType RequestData, <String p_to>, Long p_ToSecure=0, Long p_Repost=1, Long p_Type=301)
host               StringTheory
toGet              StringTheory
posted             StringTheory
x                  Long
y                  Long
  CODE
  self._wait()
  host.SetValue(self._GetHeaderField ('host:', RequestData.DataString, 1, RequestData.DataStringLen, 1, 1))

  If p_repost
    Case RequestData.RequestMethodType
    of NetWebServer_POST orof NetWebServer_PUT
      x = instring('<13,10,13,10>', RequestData.DataString, 1, 1)
      if (x > 0) and (x+4 <= RequestData.DataStringLen)
        posted.SetValue('?' & RequestData.DataString[(x+4) : RequestData.DataStringLen])
      End
    End
  End
  if not omitted(3) ! p_To
    toGet.SetValue(p_To)
  end
  if toGet.GetValue() = ''
    x = Instring('<13,10>',RequestData.DataString,1,1)
    toGet.SetValue(left(sub(RequestData.DataString,5,x-5)))
    x = toGet.instring (' HTTP/')
    If x
      toGet.SetValue(toGet.sub(1,x-1))
    End
  End
  If p_repost
    toGet.trace()
    toGet.SetValue(self.NormalizeURL(toGet.GetValue() & posted.GetValue()))
    toGet.trace()
  end
  if lower(toGet.sub(1,7)) = 'http://' or lower(toGet.sub(1,8)) = 'https://'
    self.packet.binData = self._MakeErrorPacket(p_Type, toGet.GetValue(), 'redirected')
  elsif p_Tosecure
    x = host.instring(':')
    if x then host.SetValue(host.sub(1,x-1)).
    If self._RedirectToHttpsPort = 443
      self.packet.binData = self._MakeErrorPacket(p_Type, 'https://' & host.GetValue() & toGet.GetValue(), 'redirected')
    Else
      self.packet.binData = self._MakeErrorPacket(p_Type, 'https://' & host.GetValue() & ':' & self._RedirectToHttpsPort & toGet.GetValue(), 'Permanently redirected')
    End
  Else
    if host.right(1) <> '/' and toGet.left(1) <> '/'
      host.append('/')
    end
    self.packet.binData = self._MakeErrorPacket(p_Type, 'http://'&host.GetValue() & toGet.GetValue(), 'redirected')
  End
  self.packet.binDataLen = len (clip(self.packet.binData))
  self.packet.ToIP = RequestData.FromIP
  self.Send()
  self.CloseServerConnection(RequestData.OnSocket, RequestData.SockID)
  self._release()

!------------------------------------------------------------------------------
NetWebServer.Send    PROCEDURE ()
  CODE
  self._Wait()
  self.PacketsSent += 1
  if self.LoggingOn
    self.Log ('NetWebServer.Send', 'Sending ' & self.packet.binDataLen & ' bytes.')
  end
  compile ('****', NETSHOWSEND=1)
  self._trace('[LEN ' & self.packet.bindatalen & '] ' & clip(self.packet.bindata))
  ****
  parent.Send()
  self._Release()

!------------------------------------------------------------------------------
NetWebServer.Translate  PROCEDURE (<String p_String>,Long p_AllowHtml=0)
ReturnValue  string(net:MaxBinData)
  code
  if omitted(2)
    return ''
  else
    self._jsok(p_String,p_AllowHtml,ReturnValue)
    return clip(ReturnValue)
  end

!------------------------------------------------------------------------------
NetWebServer._AddSettingsQueue      PROCEDURE (String p_SessionID,String p_File, String p_Key, Long p_Action, string p_ParentPage, string p_Procedure, *string[] p_RecordID, string p_ParentState, String p_target, Long p_OriginalAction)
loc:FormState          String(Net:FormStateSize)
x  long
  code
  !self._trace('AddSettings Queue')
  loop x = 1 to Net:FormStateSize
    loc:FormState[x] = chr(random(65,90))
  End
  self._Wait()
  clear(self._SettingsQueue)
  self._SettingsQueue.SessionID = p_SessionID
  self._SettingsQueue.FormState = loc:FormState
  self._SettingsQueue.Settings.ParentState = p_ParentState
  self._SettingsQueue.Settings.File = p_File
  self._SettingsQueue.Settings.Key = p_Key
  self._SettingsQueue.Settings.Action = p_Action
  !self._SettingsQueue.Settings.LookupField = p_LookupField
  self._SettingsQueue.Settings.ParentPage = p_ParentPage
  self._SettingsQueue.Settings.Proc = p_Procedure
  self._SettingsQueue.Settings.Target = p_Target
  self._SettingsQueue.Settings.OriginalAction = p_OriginalAction
  loop x = 1 to Net:MaxKeyFields
    self._SettingsQueue.Settings.RecordID[x] = p_RecordID[x]
  end
  add(self._SettingsQueue,self._SettingsQueue.SessionID,self._SettingsQueue.FormState)
  !self._trace('ADDing FormState ' & clip(self._SettingsQueue.FormState) & ' file=' & clip(self._SettingsQueue.Settings.File)& ' action=' & clip(self._SettingsQueue.Settings.Action))
  self._Release()
  !self._trace('AddSettings Done')
  return loc:FormState
!------------------------------------------------------------------------------
NetWebServer._GetSettingsQueue      PROCEDURE (String p_SessionID,String p_FormState,*NetFormStateGroupType p_Settings)
ans  Long
  code
  !self._trace('_ GetSettings p_SessionID=' & clip(p_SessionID) & ' p_FormState=' & clip(p_FormState))
  self._Wait()
  self._SettingsQueue.SessionID = p_SessionID
  self._SettingsQueue.FormState = p_FormState
  get(self._settingsQueue,self._SettingsQueue.SessionID,self._SettingsQueue.FormState)
  If Errorcode() = 0
    p_Settings = self._SettingsQueue.Settings
    !self._trace('GetSettings FormState ' & clip(self._SettingsQueue.FormState) & ' file=' & clip(self._SettingsQueue.Settings.File)& ' action=' & clip(self._SettingsQueue.Settings.Action))
  Else
    !self._trace('Settings not found')
    clear(p_settings)
    ans = 1
  End
  self._Release()
  !self._trace('GetSettings Done')
  Return Ans
!------------------------------------------------------------------------------
NetWebServer._DeleteSettingsQueue   PROCEDURE (String p_SessionID,String p_FormState)
  code
  self._Wait()
  if p_FormState <> ''
    loop
      self._SettingsQueue.SessionID = p_SessionID
      self._SettingsQueue.FormState = p_FormState
      get(self._settingsQueue,self._SettingsQueue.SessionID,self._SettingsQueue.FormState)
      If errorcode() or self._SettingsQueue.SessionID <> p_sessionId or self._SettingsQueue.FormState <> p_FormState
        break
      End
      !self._trace('DELETEing FormState ' & clip(self._SettingsQueue.FormState) & ' file=' & clip(self._SettingsQueue.Settings.File)& ' action=' & clip(self._SettingsQueue.Settings.Action))
      delete(self._SettingsQueue)
    End
  Else
    loop
      self._SettingsQueue.SessionID = p_sessionId
      get(self._settingsQueue,self._SettingsQueue.SessionID)
      If errorcode() or self._SettingsQueue.SessionID <> p_sessionId
        break
      End
      !self._trace('DELETEing FormState ' & clip(self._SettingsQueue.FormState) & ' file=' & clip(self._SettingsQueue.Settings.File)& ' action=' & clip(self._SettingsQueue.Settings.Action))
      delete(self._SettingsQueue)
    End
  End
  self._Release()

!------------------------------------------------------------------------------

NetWebServer.TakeEvent PROCEDURE  ()
  CODE
  ! Check if Garbage Collector needs calling
  if abs(clock() - self._LastGarbageCollectTime) > self.CheckGarbageCollectEvery
    self._LastGarbageCollectTime = clock()
    self._GarbageCollector()
  end

  case event()
  !----------------
  of self._connection.NotifyEvent to (self._connection.NotifyEvent + NET:MAX_RESERVED_EVENTS - 1)
  !----------------
    ! Okay this is a NetSimple event. So do the wait and release
    ! We need the wait and release, as both this object, and the HandleRequest objects (on
    ! different threads) can write to the self.packet data
    self._Wait()
    parent.TakeEvent()
    self._Release()
  end
!------------------------------------------------------------------------------

NetWebServer.SetDefaultHeaderDetails PROCEDURE  ()    ! Declare Procedure 3
  CODE
  self._Wait()

  self.HeaderDetails.HTTP = 'HTTP/1.1'
  self.HeaderDetails.ResponseNumber = ''
  self.HeaderDetails.ResponseString = ''
  self.HeaderDetails.Date = 0
  self.HeaderDetails.Time = 0
  self.HeaderDetails.ExpiresDate = 0
  self.HeaderDetails.ExpiresTime = 0
  self.HeaderDetails.LastModifiedDate = 0
  self.HeaderDetails.LastModifiedTime = 0
  self.HeaderDetails.ETag = ''
  self.HeaderDetails.Age = ''
  self.HeaderDetails.ContentLength = ''
  self.HeaderDetails.ContentType = 'text/html'
  self.HeaderDetails.CacheControl = ''
  self.HeaderDetails._Pragma = ''
  self.HeaderDetails.Server = 'NetTalk-WebServer/' & NetTalk:Version
  self.HeaderDetails.Location = ''
  self.HeaderDetails.WWWAuthenticate = ''
  self.HeaderDetails.Cookies = ''
  self.HeaderDetails.Connection = 'close'
  self.HeaderDetails.AccessControlAllowOrigin = ''
  self.HeaderDetails.AccessControlAllowCredentials = ''
  self.HeaderDetails.AccessControlAllowMethods = ''
  self.HeaderDetails.AccessControlMaxAge = ''
  self._Release()

!------------------------------------------------------------------------------

NetWebServer.StartNewThread PROCEDURE  (NetWebServerRequestDataType p_RequestData) ! Declare Procedure 3
  CODE
  stop ('Error. Code needs to be added to the NetWebServer.StartNewThread() virtual method')
!------------------------------------------------------------------------------

NetWebServer.AbortServerConnection PROCEDURE  (ulong p_Socket,long p_SockID) ! Declare Procedure 3
  CODE
  self._Wait()
  if self.LoggingOn
    self.Log ('NetWebServer.AbortServerConnection', 'AbortServerConnection')
  end
  clear (self._ConnectionDataQueue)
  self._ConnectionDataQueue.SockID = p_SockID
  get (self._ConnectionDataQueue, self._ConnectionDataQueue.SockID)
  if errorcode() <> 0
    self.Log ('NetWebServer.AbortServerConnection', 'Error. Could not find data connection in the DataConnection queue', NET:LogError)
  else
    if Not (self._ConnectionDataQueue.Data &= NULL)
      dispose (self._ConnectionDataQueue.Data)
    end
    delete (self._ConnectionDataQueue)
  end
  parent.AbortServerConnection (p_Socket, p_SockID)
  self.Performance.NumberofConnections = records(self._ConnectionDataQueue)
  self._Release()


!------------------------------------------------------------------------------

NetWebServer.CloseServerConnection PROCEDURE  (ulong p_Socket,long p_SockID) ! Declare Procedure 3
  CODE
  self._Wait()

  if self.LoggingOn
    self.Log ('NetWebServer.CloseServerConnection', 'CloseServerConnection')
  end
  clear (self._ConnectionDataQueue)
  self._ConnectionDataQueue.SockID = p_SockID
  get (self._ConnectionDataQueue, self._ConnectionDataQueue.SockID)
  if errorcode() <> 0
    self.Log ('NetWebServer.CloseServerConnection', 'Error. Could not find data connection in the DataConnection queue', NET:LogError)
  else
    if Not (self._ConnectionDataQueue.Data &= NULL)
      dispose (self._ConnectionDataQueue.Data)
    end
    delete (self._ConnectionDataQueue)
  end
  parent.CloseServerConnection (p_Socket, p_SockID)
  self.Performance.NumberofConnections = records(self._ConnectionDataQueue)
  !self._trace('[cc] CloseServerConnection sockid ' & p_SockID & ' r=' & records(self._ConnectionDataQueue)  & ' noc=' & self.Performance.NumberofConnections)
  self._Release()

!------------------------------------------------------------------------------

NetWebServer.ConnectionClosed PROCEDURE  ()           ! Declare Procedure 3
  CODE
  self._Wait()
  if self.LoggingOn
    self.Log ('NetWebServer.ConnectionClosed', 'ConnectionClosed')
  end
  self.RemoveFromIncoming()
  clear (self._ConnectionDataQueue)
  self._ConnectionDataQueue.SockID = self.packet.SockID
  get (self._ConnectionDataQueue, self._ConnectionDataQueue.SockID)
  if errorcode() <> 0
    self.Log ('NetWebServer.ConnectionClosed', 'Error. Could not find data connection in the DataConnection queue', NET:LogError)
  else
    if Not (self._ConnectionDataQueue.Data &= NULL)
      dispose (self._ConnectionDataQueue.Data)
    end
    delete (self._ConnectionDataQueue)
  end
  self.Performance.NumberofConnections = records(self._ConnectionDataQueue)
  parent.ConnectionClosed()

  self._Release()

!------------------------------------------------------------------------------

NetWebServer.Close   PROCEDURE  ()                    ! Declare Procedure 3
  CODE
  self._Wait()

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebServer.Close', 'Close')
    end
  ****
  self._FreeConnectionDataQueue()
  parent.Close()

  self._Release()

!------------------------------------------------------------------------------

NetWebServer.Abort   PROCEDURE  ()                    ! Declare Procedure 3
  CODE
  self._Wait()

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetWebServer.Abort', 'Abort')
    end
  ****
  self._FreeConnectionDataQueue()
  parent.Abort()

  self._Release()
!------------------------------------------------------------------------------
NetWebServer._AddBrowseValue PROCEDURE (String p_SessionID,String p_proc, String p_FileName, File p_file, Key p_Key,<*String p_Field1>, <*String p_field2>,<*String p_field3>, <*String p_field4>, <*String p_field5>, <*String p_field6>, <*String p_field7>, <*String p_field8>, <*String p_field9>, <*String p_field10>,<String p_ViewPos>)
x          Long
y          Long
hash       String(Net:HashSize)
loc:any    Any
loc:group  &Group

  code
  loc:group &= p_file{prop:record}
  self._wait()
  clear(self._BrowseIDQueue)
  self._BrowseIDQueue.SessionID = p_SessionID
  self._BrowseIDQueue.Proc    = p_proc
  self._BrowseIDQueue.FileName  = p_FileName
  self._BrowseIDQueue.KeyName   = p_key{prop:label}
  loop y = 1 to p_key{prop:components}
    if y > Net:MaxKeyFields then break.
    Loc:any &= what(loc:group,p_key{prop:field,y})
    execute y
      self._BrowseIDQueue.RecordId1  = choose(omitted(7)=1,loc:any & ' ',p_Field1) ! &' ' forces it to be a string assignment.
      self._BrowseIDQueue.RecordId2  = choose(omitted(8)=1,loc:any & ' ',p_Field2)
      self._BrowseIDQueue.RecordId3  = choose(omitted(9)=1,loc:any & ' ',p_Field3)
      self._BrowseIDQueue.RecordId4  = choose(omitted(10)=1,loc:any & ' ',p_Field4)
      self._BrowseIDQueue.RecordId5  = choose(omitted(11)=1,loc:any & ' ',p_Field5)
      self._BrowseIDQueue.RecordId6  = choose(omitted(12)=1,loc:any & ' ',p_Field6)
      self._BrowseIDQueue.RecordId7  = choose(omitted(13)=1,loc:any & ' ',p_Field7)
      self._BrowseIDQueue.RecordId8  = choose(omitted(14)=1,loc:any & ' ',p_Field8)
      self._BrowseIDQueue.RecordId9  = choose(omitted(15)=1,loc:any & ' ',p_Field9)
      self._BrowseIDQueue.RecordId10  = choose(omitted(16)=1,loc:any & ' ',p_Field10)
    end
  end
  if not omitted(17)
    self._BrowseIDQueue.ViewPos = p_ViewPos
  end
  !self._trace('Get for SessionID=' & clip(self._BrowseIDQueue.SessionID) & ' : proc=' & clip(self._BrowseIDQueue.Proc) & ' : id1=' & clip(self._BrowseIDQueue.RecordId1) & ' : ' & clip(self._BrowseIDQueue.RecordId2) & ' : ' & clip(self._BrowseIDQueue.RecordId3) & ' vp=' & self._BrowseIDQueue.ViewPos)
  !get(self._BrowseIDQueue,self._BrowseIDQueue.SessionID,self._BrowseIDQueue.Proc,self._BrowseIDQueue.FileName,self._BrowseIDQueue.RecordId1,self._BrowseIDQueue.RecordId2,self._BrowseIDQueue.RecordId3,self._BrowseIDQueue.RecordId4,self._BrowseIDQueue.RecordId5,self._BrowseIDQueue.RecordId6,self._BrowseIDQueue.RecordId7,self._BrowseIDQueue.RecordId8,self._BrowseIDQueue.RecordId9,self._BrowseIDQueue.RecordId10,self._BrowseIDQueue.ViewPos)
  !viewpos can change based on how the browse is refreshing. See memory of contracted after popup Save in Multi-Row example.
  get(self._BrowseIDQueue,self._BrowseIDQueue.SessionID,self._BrowseIDQueue.Proc,self._BrowseIDQueue.FileName,self._BrowseIDQueue.RecordId1,self._BrowseIDQueue.RecordId2,self._BrowseIDQueue.RecordId3,self._BrowseIDQueue.RecordId4,self._BrowseIDQueue.RecordId5,self._BrowseIDQueue.RecordId6,self._BrowseIDQueue.RecordId7,self._BrowseIDQueue.RecordId8,self._BrowseIDQueue.RecordId9,self._BrowseIDQueue.RecordId10)
  if errorcode()
    loop x = 1 to Net:HashSize
      y = random(48,109)
      if y > 57 then y += 7.
      if y > 90 then y += 6.
      hash[x] = chr(y)
    end
    self._BrowseIDQueue.Hash = hash
    add(self._BrowseIDQueue,self._BrowseIDQueue.SessionID,self._BrowseIDQueue.Proc,self._BrowseIDQueue.FileName,self._BrowseIDQueue.RecordId1,self._BrowseIDQueue.RecordId2,self._BrowseIDQueue.RecordId3,self._BrowseIDQueue.RecordId4,self._BrowseIDQueue.RecordId5,self._BrowseIDQueue.RecordId6,self._BrowseIDQueue.RecordId7,self._BrowseIDQueue.RecordId8,self._BrowseIDQueue.RecordId9,self._BrowseIDQueue.RecordId10)!,self._BrowseIDQueue.ViewPos)
    !self._trace('Add for ' & clip(self._BrowseIDQueue.SessionID) & ' : ' &clip( self._BrowseIDQueue.Proc) & ' : ' & clip(self._BrowseIDQueue.RecordId1) & ' : ' & clip(self._BrowseIDQueue.RecordId2) & ' : ' & clip(self._BrowseIDQueue.RecordId3) & ' : ' & clip(self._BrowseIDQueue.RecordId4) & ' : ' & clip(self._BrowseIDQueue.RecordId5) & ' :: '& hash)
  else
    if not omitted(17)
      if self._BrowseIDQueue.ViewPos <> p_ViewPos
        self._BrowseIDQueue.ViewPos = p_ViewPos
        put(self._BrowseIDQueue)
      end
    end
    !self._trace('row found')
    hash = self._BrowseIDQueue.Hash
  end
  self._release()
  return hash
!------------------------------------------------------------------------------
NetWebServer._SetBrowseValueStatus PROCEDURE (String p_SessionId,String p_Hash,Long p_Contracted)
ans long
  code
  self._wait()
  self._BrowseIDQueue.SessionId = p_SessionId
  self._BrowseIDQueue.Hash = p_hash
  get(self._BrowseIDQueue,self._BrowseIDQueue.SessionId,self._BrowseIDQueue.Hash)
  if errorcode() = 0
    ans = true
    self._BrowseIDQueue.Contracted = p_Contracted
    put(self._BrowseIDQueue)
  end
  self._release()
  return ans
!------------------------------------------------------------------------------
NetWebServer._GetBrowseValueStatus PROCEDURE (String p_SessionId,String p_Hash)
ans long
  code
  self._wait()
  self._BrowseIDQueue.SessionId = p_SessionId
  self._BrowseIDQueue.Hash = p_hash
  get(self._BrowseIDQueue,self._BrowseIDQueue.SessionId,self._BrowseIDQueue.Hash)
  if errorcode() = 0
    ans = self._BrowseIDQueue.Contracted
  end
  self._release()
  return ans
!------------------------------------------------------------------------------
NetWebServer._GetBrowseValue PROCEDURE (String p_SessionId,String p_Hash, *String p_FileName, *String p_Keyname,*String p_Field1, *String p_field2,*String p_field3, *String p_field4, *String p_field5, *String p_field6, *String p_field7, *String p_field8, *String p_field9, *String p_field10, *String p_ViewPos)
ans long
  code
  !self._trace('GetBrowseValue ' & p_Hash)
  self._wait()
  self._BrowseIDQueue.SessionId = p_SessionId
  self._BrowseIDQueue.Hash = p_hash
  get(self._BrowseIDQueue,self._BrowseIDQueue.SessionId,self._BrowseIDQueue.Hash)
  if errorcode() = 0
    ans = true
    p_FileName = self._BrowseIDQueue.FileName
    p_KeyName = self._BrowseIDQueue.KeyName
    p_Field1 = self._BrowseIDQueue.RecordId1
    p_Field2 = self._BrowseIDQueue.RecordId2
    p_Field3 = self._BrowseIDQueue.RecordId3
    p_Field4 = self._BrowseIDQueue.RecordId4
    p_Field5 = self._BrowseIDQueue.RecordId5
    p_Field6 = self._BrowseIDQueue.RecordId6
    p_Field7 = self._BrowseIDQueue.RecordId7
    p_Field8 = self._BrowseIDQueue.RecordId8
    p_Field9 = self._BrowseIDQueue.RecordId9
    p_Field10 = self._BrowseIDQueue.RecordId10
    p_ViewPos = self._BrowseIDQueue.ViewPos
    !self._trace('_GetBrowseValue -found ' & clip(p_FileName) & ' ' & clip(p_Keyname) & ' ' & clip(p_Field1))
  else
    !self._trace('_GetBrowseValue - not found ' & p_hash)
  end
  self._release()
  return ans
!------------------------------------------------------------------------------
NetWebServer._ClearBrowse PROCEDURE (String p_SessionID,<String p_proc>)
  code
  self._wait()
  loop
    self._BrowseIDQueue.SessionID = p_SessionID
    if not omitted(3) and p_proc <> ''
      self._BrowseIDQueue.Proc    = p_proc
      get(self._BrowseIDQueue,self._BrowseIDQueue.SessionID,self._BrowseIDQueue.Proc)
    else
      get(self._BrowseIDQueue,self._BrowseIDQueue.SessionID)
    end
    if errorcode()
      break
    end
    delete(self._BrowseIDQueue)
  end
  self._release()

!------------------------------------------------------------------------------
NetWebServer._GenSessionId PROCEDURE  (Long p_Length)
x    long
y    long
ans  string(Net:MaxSessionLength)
  code
  !self._trace('Gen Session Id ' & p_Length)
  loop 10000 times
    if p_Length <= 1
      ans = random(1,2147483647)
    Else
      if p_Length > size(ans) then p_Length = size(ans).
      loop x = 1 to p_Length
        y = random(1,62)
        case y
        of 1 to 10
          ans[x] = chr(y+47)
        of 11 to 36
          ans[x] = chr(y+54)
        of 37 to 62
          ans[x] = chr(y+60)
        end
      end
    end
    self._SessionQueue.SessionID = ans
    get(self._SessionQueue, self._SessionQueue.SessionID)
    if errorcode() then break.
  End
  !self._trace('Generated Session Id ' & clip(ans))
  return clip(ans)

!------------------------------------------------------------------------------
NetWebServer._NewSession PROCEDURE  (String p_SessionID,Long pExpiry,String pReferer,Long pLength)
ans  string(Net:MaxSessionLength)
  CODE
  self._Wait()
  self._SessionQueue.SessionID = Choose(p_SessionID <> '',p_SessionId,self._GenSessionId(pLength))
  ans = self._SessionQueue.SessionID
  self._SessionQueue.time = clock()
  self._sessionQueue.LoggedIn = 0
  self._sessionQueue.ExpiryAfterHS = pExpiry
  self._sessionQueue.Referer = pReferer
  add(self._SessionQueue, self._SessionQueue.SessionID)
  self._Release()
  return clip(ans)

!------------------------------------------------------------------------------
NetWebServer._SetSessionIP    PROCEDURE (String p_SessionID, string p_ip)
returnValue       Long
  CODE
  self._Wait()
  self._SessionQueue.SessionID = p_SessionID
  get(self._SessionQueue,self._SessionQueue.SessionID)
  if errorcode() = 0
    self._sessionQueue.IP = p_ip
    put(self._SessionQueue)
    ReturnValue = 0
  Else
    ReturnValue = 1
  end
  self._Release()
  return ReturnValue

!------------------------------------------------------------------------------
NetWebServer._GetSessionIP    PROCEDURE (String p_SessionID)
returnValue       like (Net:SimplePacketType.FromIP)
  CODE
  self._Wait()
  self._SessionQueue.SessionID = p_SessionID
  get(self._SessionQueue,self._SessionQueue.SessionID)
  if errorcode() = 0
    ReturnValue = self._SessionQueue.IP
  else
    ReturnValue = 0
  end
  self._Release()
  return clip(ReturnValue)
!------------------------------------------------------------------------------
NetWebServer._SetSessionReferer    PROCEDURE (String p_SessionID, string p_referer)
returnValue       Long
  CODE
  self._Wait()
  self._SessionQueue.SessionID = p_SessionID
  get(self._SessionQueue,self._SessionQueue.SessionID)
  if errorcode() = 0
    self._sessionQueue.Referer = p_referer
    put(self._SessionQueue)
    ReturnValue = 0
  Else
    ReturnValue = 1
  end
  self._Release()
  return ReturnValue
!------------------------------------------------------------------------------
NetWebServer._GetSessionReferer    PROCEDURE (String p_SessionID)
returnValue   like (NetWebServerSessionQueueType.Referer)
  CODE
  self._Wait()
  self._SessionQueue.SessionID = p_SessionID
  get(self._SessionQueue,self._SessionQueue.SessionID)
  if errorcode() = 0
    ReturnValue = clip(self._SessionQueue.Referer)
  else
    ReturnValue = 0
  end
  self._Release()
  return clip(ReturnValue)
!------------------------------------------------------------------------------
NetWebServer._GetSessionLoggedIn    PROCEDURE (String p_SessionID)
returnValue       long,Auto
  CODE
  self._Wait()
  self._SessionQueue.SessionID = p_SessionID
  get(self._SessionQueue,self._SessionQueue.SessionID)
  if errorcode() = 0
    ReturnValue = self._SessionQueue.LoggedIn
  else
    ReturnValue = 0
  end
  self._Release()
  return ReturnValue

!------------------------------------------------------------------------------
! returns the New Session ID to use. (SessionID's can change on login/logout)
NetWebServer._SetSessionLoggedIn PROCEDURE  (String p_SessionID,Long p_Value,Long p_ChangeSessionOnLogInOut,Long p_DeleteSessionOnLogout,*Long p_DeleteNow)
returnValue       string(Net:MaxSessionLength)
randomx           string(Net:MaxSessionLength)
x                 long
  CODE
  !self._trace('NetWebServer._SetSessionLoggedIn ' & clip(p_SessionID) & ' ' & p_value)
  self._Wait()
  if p_ChangeSessionOnLogInOut and (p_DeleteSessionOnLogout = 0 or p_Value = 1)
    if numeric(p_SessionID)
      x = 0
    else
      x = len(clip(p_SessionID))
    end
    randomx = self._GenSessionId(x)
  end
  self._SessionQueue.SessionID = p_SessionID
  get(self._SessionQueue,self._SessionQueue.SessionID)
  if errorcode() = 0
    self._SessionQueue.LoggedIn = p_value
    if p_ChangeSessionOnLogInOut and (p_DeleteSessionOnLogout = 0 or p_Value = 1)
      !self._trace('Changing Session from [' & clip(self._SessionQueue.SessionID) & '] to [' & clip(randomx) & ']')
      self._SessionQueue.SessionID = clip(randomx)   ! changing session id
    end
    put(self._SessionQueue,self._SessionQueue.SessionID)
    if p_DeleteSessionOnLogout = 1 and p_Value = 0  ! logging out, and deleting session on logout
      p_DeleteNow = 1
    elsif p_ChangeSessionOnLogInOut
      loop ! session data
        self._SessionDataQueue.SessionID = p_SessionID
        get(self._SessionDataQueue,self._SessionDataQueue.SessionID)
        if errorcode() or self._SessionDataQueue.SessionID <> p_SessionID then break.
        self._SessionDataQueue.SessionID = randomX
        put(self._SessionDataQueue)
      end
      loop !6.52 ; form settings
        self._SettingsQueue.SessionID = p_SessionID
        get(self._SettingsQueue,self._SettingsQueue.SessionID)
        if errorcode() or self._SettingsQueue.SessionID <> p_SessionID then break.
        self._SettingsQueue.SessionID = randomX
        put(self._SettingsQueue)
      end   !6.52
      loop  !6.52 ; browse rows
        self._BrowseIDQueue.SessionId = p_SessionID
        get(self._BrowseIDQueue,self._BrowseIDQueue.SessionId)
        if errorcode() or self._BrowseIDQueue.SessionID <> p_SessionID then break.
        self._BrowseIDQueue.SessionID = randomX
        put(self._BrowseIDQueue)
      end  !6.52
    end
    ReturnValue = self._SessionQueue.SessionID
  else
!    loop x# = 1 to records(self._SessionQueue)
!      get(self._SessionQueue,x#)
!    end
    ReturnValue = ''
  end
  self._Release()
  return ReturnValue

!------------------------------------------------------------------------------
NetWebServer._GetSessionLevel    PROCEDURE (String p_SessionID)
returnValue       long,Auto
  CODE
  self._Wait()
  self._SessionQueue.SessionID = p_SessionID
  get(self._SessionQueue,self._SessionQueue.SessionID)
  if errorcode() = 0
    ReturnValue = self._SessionQueue.Level
  else
    ReturnValue = 0
  end
  self._Release()
  return ReturnValue

!------------------------------------------------------------------------------
NetWebServer._SetSessionLevel PROCEDURE  (String p_SessionID,Long p_Value)
returnValue       long
  CODE
  self._Wait()
  self._SessionQueue.SessionID = p_SessionID
  get(self._SessionQueue,self._SessionQueue.SessionID)
  if errorcode() = 0
    self._SessionQueue.Level = p_value
    put(self._SessionQueue)
    ReturnValue = 0
  else
    ReturnValue = 1
  end
  self._Release()
  return ReturnValue

!------------------------------------------------------------------------------
NetWebServer._TouchSession PROCEDURE  (String p_SessionID)
returnValue       long
  CODE
  self._Wait()
  self._SessionQueue.SessionID = clip(p_SessionID)
  get(self._SessionQueue,self._SessionQueue.SessionID)
  if errorcode() = 0
    self._SessionQueue.time = clock()
    put(self._SessionQueue)
    ReturnValue = 0
  else
    loop x# = 1 to records(self._SessionQueue)
      get(self._SessionQueue,x#)
    end
    ReturnValue = 1
  end
  self._Release()
  return ReturnValue

!------------------------------------------------------------------------------

NetWebServer._DeleteSession PROCEDURE  (String p_SessionID)
returnValue        long
  CODE
  self._Wait()
  loop
    self._SessionDataQueue.SessionID = p_SessionID
    get(self._SessionDataQueue, self._SessionDataQueue.SessionID)
    if errorcode() then break.
    If not self._SessionDataQueue.ExtValue &= Null
      dispose(self._SessionDataQueue.ExtValue)
    End
    self._DeleteSettingsQueue(p_SessionID,'')
    delete(self._SessionDataQueue)
  end
  self._SessionQueue.SessionID = p_SessionID
  get(self._SessionQueue, self._SessionQueue.SessionID)
  if errorcode() = 0
    delete(self._SessionQueue)
    returnValue = 0
  else
    returnValue = 1
  end
  self._Release()
  return returnValue

!------------------------------------------------------------------------------
NetWebServer._GetSessionValueLength PROCEDURE  (String p_SessionID, string p_Name)
ReturnValue  Long
  Code
  if p_SessionID = ''
  elsif p_name = ''
  else
    self._Wait()
    clear(self._SessionDataQueue)
    self._SessionDataQueue.SessionID = p_SessionID
    self._SessionDataQueue.Name = left(lower(p_Name))
    get(self._SessionDataQueue, self._SessionDataQueue.SessionID, self._SessionDataQueue.Name)
    if ErrorCode() = 0
      If self._SessionDataQueue.ExtValue &= Null
        ReturnValue = len(clip(self._SessionDataQueue.Value))
      Else
        ReturnValue = self._SessionDataQueue.ExtValueLen
      End
    end
    self._Release()
  end
  Return ReturnValue
!------------------------------------------------------------------------------
NetWebServer._GetSessionValue PROCEDURE  (String p_SessionID, string p_Name, *long p_GetValueError) ! Declare Procedure 3
ReturnValue   like (NetWebServerSessionDataQueueType.Value)
RValue        &String
  CODE
  RValue &= Null
  if p_SessionID = ''
    p_GetValueError = -2 ! SessionID = ''
    if self.LoggingOn
      self.Log ('NetWebServer._GetSessionValue', 'Error. SessionID = 0 while asking for Name = ' & p_Name, NET:LogError)
    end
  elsif p_name = ''

  else
    self._Wait()
    clear(self._SessionDataQueue)
    p_GetValueError = 0
    self._SessionDataQueue.SessionID = p_SessionID
    self._SessionDataQueue.Name = left(lower(p_Name))
    get(self._SessionDataQueue, self._SessionDataQueue.SessionID, self._SessionDataQueue.Name)
    if ErrorCode() = 0
      If self._SessionDataQueue.ExtValue &= Null
        ReturnValue = self._SessionDataQueue.Value
      Else
        RValue &= self._SessionDataQueue.ExtValue
      End
    else
      p_GetValueError = 1 ! Not found
    end
    self._Release()
  end
  if rValue &= Null
    return clip(ReturnValue)
  Else
    return clip(RValue)
  End
!------------------------------------------------------------------------------
NetWebServer._RestoreSessionValues PROCEDURE  (String p_SessionID, string p_Prefix)
loc:prefix       cstring(256)
loc:name         like(self._SessionDataQueue.Name)
loc:value        like(self._SessionDataQueue.Value)
loc:ExtValue     &String
siz              long
ev               long
ReturnValue      Long
xl               Long
nl               Long
p                Long
  code
  !self._trace('_RestoreSessionValues prefix=' & clip(p_prefix))
  loc:ExtValue &= Null
  if p_SessionID = ''
    ReturnValue = -2
  elsif p_Prefix = ''
    ReturnValue = 0
  else
    xl = len(clip(p_prefix)) + 2
    if xl > size(self._SessionDataQueue.Name) then xl = size(self._SessionDataQueue.Name).
    loc:Prefix = clip(lower(p_prefix)) & '**'
    nl = size(self._SessionDataQueue.Name)
    self._Wait()
    clear(self._SessionDataQueue)
    self._SessionDataQueue.SessionID = p_SessionID
    self._SessionDataQueue.Name = loc:prefix
    get(self._SessionDataQueue, self._SessionDataQueue.SessionID, self._SessionDataQueue.Name)
    if ErrorCode() = 0
      p = Pointer(self._SessionDataQueue)
      do LoadRecord
      loop
        execute ev
          self._setSessionValue(p_SessionId,clip(loc:name[xl+1 : nl]),loc:value)
          self._setSessionValue(p_SessionId,clip(loc:name[xl+1 : nl]),loc:extvalue)
        end
        !self._trace('Restoring Session Value: ' & clip(loc:name[xl+1 : nl]) & ' = ' & clip(loc:value))
        p += 1
        Get(self._SessionDataQueue,p)
        if self._SessionDataQueue.SessionID <> p_SessionID then break.
        if self._SessionDataQueue.Name[1 : xl] <> loc:prefix then break.
        do LoadRecord
      End
    End
    self._Release()
  end
  Return ReturnValue

LoadRecord Routine
      loc:name = self._SessionDataQueue.Name
      If not self._SessionDataQueue.ExtValue &= Null
        If siz < self._SessionDataQueue.ExtValueSize
          If not loc:ExtValue &= Null
            Dispose(loc:ExtValue)
          End
          siz = self._SessionDataQueue.ExtValueSize
          loc:ExtValue &= new(String(siz))
        End
        loc:ExtValue = self._SessionDataQueue.ExtValue
        ev = 2
      Else
        loc:value = self._SessionDataQueue.Value
        ev = 1
        self._SessionDataQueue.Value = ''
        put(self._SessionDataQueue)
      End

!------------------------------------------------------------------------------
NetWebServer._DeleteSessionValue PROCEDURE  (String p_SessionID, string p_Name, *long p_GetValueError) ! Declare Procedure 3
  Code
  If p_SessionID = ''
    p_GetValueError = -2 ! SessionID = 0
  Else
    Self._Wait()
    Clear(self._SessionDataQueue)
    p_GetValueError = 0
    Self._SessionDataQueue.SessionID = p_SessionID
    Self._SessionDataQueue.Name = left(lower(p_Name))
    Get(self._SessionDataQueue, self._SessionDataQueue.SessionID, self._SessionDataQueue.Name)
    If ErrorCode() = 0
      If not self._SessionDataQueue.ExtValue &= Null
        Dispose(self._SessionDataQueue.ExtValue)
      End
      Delete(self._SessionDataQueue)
    Else
      p_GetValueError = 1 ! Not found
    End
    Self._Release()
  End
  Return

!------------------------------------------------------------------------------
! Extracts Clarion Date from a Unix date/time (which is stored as Seconds since Jan 1 1970)
NetWebServer.UnixToClarionDate               Procedure(Long p_DateTime)
  code
  if p_DateTime >= 86400
    return (int(p_DateTime / 86400) + 61730) ! 61730 = Date(1,1,1970)
  else                                       ! 86400 = seconds in 1 day
    return 0
  end
!------------------------------------------------------------------------------
! Extracts Clarion Time from a Unix date/time (which is stored as Seconds since Jan 1 1970)
NetWebServer.UnixToClarionTime               Procedure(Long p_DateTime)
  code
  if p_DateTime < 0
    return 0
  end
  return(1+(p_DateTime % 86400) * 100) ! 86400 = seconds in 1 day
!------------------------------------------------------------------------------
! Converts a Clarion Date & Time to a Unix date/time (which is stored as Seconds since Jan 1 1970)
NetWebServer.ClarionToUnixDate          Procedure(Long p_Date, Long p_Time)
  code
  if p_Date > 61730                                   ! 61730 = date(1,1,1970)
    return(((p_Date-61730)*86400) + (p_Time / 100))   ! 86400 = seconds in 1 day
  else
    return((p_Time / 100))
  end
!------------------------------------------------------------------------------
NetWebServer._GetSessionValueFormat PROCEDURE  (String p_SessionID, string p_Name, *long p_GetValueError)
ReturnValue   like (NetWebServerSessionDataQueueType.Value)
rValue        &String
  Code
  rValue &= Null
  p_GetValueError = 0
  if p_SessionID = ''
    p_GetValueError = -2 ! SessionID = 0
    if self.LoggingOn
      self.Log ('NetWebServer._GetSessionValueFormat', 'Error. SessionID = 0 while asking for Name = ' & p_Name, NET:LogError)
    end
  elsif p_name = ''
  else
    self._Wait()
    clear(self._SessionDataQueue)
    self._SessionDataQueue.SessionID = p_SessionID
    self._SessionDataQueue.Name = left(lower(p_Name))
    get(self._SessionDataQueue, self._SessionDataQueue.SessionID, self._SessionDataQueue.Name)
    If ErrorCode() = 0
      If self._SessionDataQueue.Picture <> ''
        If self._SessionDataQueue.ExtValue &= Null
          case upper(self._SessionDataQueue.Picture)
          of '@L1'
          orof '@L2'
            ReturnValue = self.ColorWeb(self._SessionDataQueue.Value)
          of '@U1'
            ReturnValue = self._SessionDataQueue.Value
            p_GetValueError = Net:UnsafeHtmlOk ! unsafe values allowed
          else
            ReturnValue = self.mformat (self._SessionDataQueue.Value, self._SessionDataQueue.Picture)
          end
          !self._trace('GetSessionValueFormat value=' & clip(self._SessionDataQueue.Value) & ' pic=' & clip(self._SessionDataQueue.Picture) & ' ans=' & returnvalue)
          If lower(sub(self._SessionDataQueue.Picture,1,2))='@n'
            ReturnValue = Left(ReturnValue)
          End
        Else
          ! can't set a reference to a function result, and you can't format very large strings anyway.
          !RValue &= format (self._SessionDataQueue.ExtValue, self._SessionDataQueue.Picture)
          RValue &= self._SessionDataQueue.ExtValue
          case upper(self._SessionDataQueue.Picture)
          of '@U1'
            p_GetValueError = Net:UnsafeHtmlOk ! unsafe values allowed
          end
        End
      else
        If self._SessionDataQueue.ExtValue &= Null
          ReturnValue = self._SessionDataQueue.Value
        Else
          RValue &= self._SessionDataQueue.ExtValue
        End
      end
    else
      p_GetValueError = 1 ! Not found
    end
    self._Release()
  end
  if rValue &= Null
    return clip(ReturnValue)
  else
    return(clip(rValue))
  End
!------------------------------------------------------------------------------

NetWebServer._GetSessionPicture PROCEDURE  (String p_SessionID, string p_Name, *long p_GetValueError) ! Declare Procedure 3
ReturnValue          like (NetWebServerSessionDataQueueType.Picture)
  CODE
  p_GetValueError = 0
  if p_SessionID = ''
    p_GetValueError = -2 ! SessionID = 0
    if self.LoggingOn
      self.Log ('NetWebServer._GetSessionPicture', 'Error. SessionID = 0 while asking for Name = ' & p_Name, NET:LogError)
    end
  else
    self._Wait()
    clear(self._SessionDataQueue)
    self._SessionDataQueue.SessionID = p_SessionID
    self._SessionDataQueue.Name = left(lower(p_Name))
    get(self._SessionDataQueue, self._SessionDataQueue.SessionID, self._SessionDataQueue.Name)
    if ErrorCode() = 0
      ReturnValue = self._SessionDataQueue.Picture
    else
      p_GetValueError = 1 ! Not found
    end
    self._Release()
  end
  return clip(ReturnValue)
!------------------------------------------------------------------------------
NetWebServer._SetSessionValue PROCEDURE  (String p_SessionID, string p_Name, string p_Value, <string p_Picture>)
returnValue              long
loc:pic                  string(32)
loc:len                  long
  CODE
  returnValue = 0        ! No error
  !self._trace('_SetSessionValue : ' & clip(p_Name) & ' ' & clip(p_value))
  if not omitted(5)
    loc:pic = p_Picture
  end
  if p_SessionID = ''
    returnValue = -2     ! SessionID = 0
  else
    self._Wait()
    clear(self._SessionDataQueue)
    self._SessionDataQueue.SessionID = p_SessionID
    self._SessionDataQueue.Name = left(lower(p_Name))
    get(self._SessionDataQueue, self._SessionDataQueue.SessionID, self._SessionDataQueue.Name)
    if errorCode() = 0
      do LoadRecord
      if not omitted(5)
        self._SessionDataQueue.Picture = loc:pic
      end
      put(self._SessionDataQueue)
      returnValue = errorcode()
    else
      clear(self._SessionDataQueue)
      self._SessionDataQueue.SessionID = p_SessionID
      self._SessionDataQueue.Name = left(lower(p_Name))
      self._SessionDataQueue.Picture = loc:pic
      do LoadRecord
      add(self._SessionDataQueue, +self._SessionDataQueue.SessionID, +self._SessionDataQueue.Name)
      returnValue = errorcode()
    end
    self._Release()
  end
  return returnValue

LoadRecord  Routine
  loc:len = len(p_value)
  If loc:len <= size(self._SessionDataQueue.Value) and self._SessionDataQueue.ExtValue &= Null
    self._SessionDataQueue.Value = p_Value
    self._SessionDataQueue.ExtValueLen = 0
  Else
    if loc:len > self._SessionDataQueue.ExtValueSize
      dispose(self._SessionDataQueue.ExtValue)
      self._SessionDataQueue.ExtValue &= New(String(loc:len))
      self._SessionDataQueue.ExtValueSize = loc:len
    End
    self._SessionDataQueue.ExtValue = p_Value
    self._SessionDataQueue.ExtValueLen = loc:len
  End
!------------------------------------------------------------------------------

NetWebServer._SetSessionPicture PROCEDURE  (String p_SessionID, string p_Name, string p_Picture) ! Declare Procedure 3
returnValue              long
  CODE
  returnValue = 0        ! No error
  if p_SessionID = ''
    returnValue = -2     ! SessionID = 0
    if self.LoggingOn
      self.Log ('NetWebServer._SetSessionPicture', 'Error. SessionID = 0 while trying to set Name = ' & p_Name, NET:LogError)
    end
  else
    self._Wait()
    clear(self._SessionDataQueue)
    self._SessionDataQueue.SessionID = p_SessionID
    self._SessionDataQueue.Name = left(lower(p_Name))
    get(self._SessionDataQueue, self._SessionDataQueue.SessionID, self._SessionDataQueue.Name)
    if errorCode() = 0
      self._SessionDataQueue.Picture = p_Picture
      put(self._SessionDataQueue)
      returnValue = errorcode()
    else
      if sub(lower(p_picture),1,2) <> '@s'
        clear(self._SessionDataQueue)
        self._SessionDataQueue.SessionID = p_SessionID
        self._SessionDataQueue.Name = left(lower(p_Name))
        self._SessionDataQueue.Picture = p_Picture
        add(self._SessionDataQueue, +self._SessionDataQueue.SessionID, +self._SessionDataQueue.Name)
        returnValue = errorcode()
      End
    end
    self._Release()
  end
  return returnValue

!------------------------------------------------------------------------------

NetWebServer._IfExistsSessionValue PROCEDURE  (String p_SessionID, string p_Name) ! Declare Procedure 3
returnValue        long

  CODE
  if p_SessionID = ''
    if self.LoggingOn
      self.Log ('NetWebServer._GetSessionValueFormat', 'Error. SessionID = 0 while asking for Name = ' & p_Name, NET:LogError)
    end
    returnValue = false
  else
    self._Wait()
    clear(self._SessionDataQueue)
    self._SessionDataQueue.SessionID = p_SessionID
    self._SessionDataQueue.Name = left(lower(p_Name))
    get(self._SessionDataQueue, self._SessionDataQueue.SessionID, self._SessionDataQueue.Name)
    if ErrorCode() = 0
      returnValue = true
    else
      returnValue = false
    end
    self._Release()
  end
  return returnValue


!------------------------------------------------------------------------------

NetWebServer._GarbageCollector PROCEDURE  ()          ! Declare Procedure 3
x             long
t             long
c             long
loc:Hours24   equate(8640000)
RequestData   Group(NetWebServerRequestDataType).
  CODE
  self._Wait()
  x = records(self._SessionQueue)
  if x > 0
    t = clock()
    c = t + loc:Hours24
    loop
      get(self._SessionQueue, x)
      if errorcode() = 0
        if t >= self._SessionQueue.time ! normal conditions
          if (t - self._SessionQueue.time) > self._SessionQueue.ExpiryAfterHS
            if self.LoggingOn
              self.Log ('NetWebServer._GarbageCollector', 'SessionID = ' & clip(self._SessionQueue.SessionID) & ' has expired.')
            end
            do DelSession
          end
        else ! roll over midnight occurred
          if (c - self._SessionQueue.time) > self._SessionQueue.ExpiryAfterHS
            if self.LoggingOn
              self.Log ('NetWebServer._GarbageCollector', 'SessionID = ' & clip(self._SessionQueue.SessionID) & ' has expired (after clock rollover).')
            end
            do DelSession
          end
        end
      end
      x -= 1
      if x <= 0
        break
      end
    end
  end
  self._Release()
  return

DelSession  Routine
  RequestData.WebServer &= self
  RequestData.DataString &= null
  RequestData.RequestMethodType = NetWebServer_DELETESESSION
  RequestData.SessionId = self._SessionQueue.SessionID
  self.StartNewThread(RequestData)

!------------------------------------------------------------------------------

NetWebServer._FreeConnectionDataQueue PROCEDURE  ()
x           long
  CODE
  self._Wait()
  if self.LoggingOn
    self.Log ('NetWebServer._FreeConnectionDataQueue', '_FreeConnectionDataQueue')
  end
  if not self._ConnectionDataQueue &= NULL
    loop x = 1 to records (self._ConnectionDataQueue)
      get (self._ConnectionDataQueue, x)
      dispose (self._ConnectionDataQueue.Data)
    end
    free (self._ConnectionDataQueue)
  end
  self._Release()
  return

!------------------------------------------------------------------------------

NetWebServer._MakeErrorPacket PROCEDURE  (long p_ErrorNumber, string p_ErrorString, string p_ErrorDescription, Long p_Ajax=0, Long p_DefaultHeader=1)
! -----------------------------------------------
! "200"   ; OK
! "201"   ; Created
! "202"   ; Accepted
! "204"   ; No Content
! "301"   ; Moved Permanently
! "302"   ; Moved Temporarily
! "304"   ; Not Modified
! "400"   ; Bad Request
! "401"   ; Unauthorized
! "403"   ; Forbidden
! "404"   ; Not Found
! "500"   ; Internal Server Error
! "501"   ; Not Implemented
! "502"   ; Bad Gateway
! "503"   ; Service Unavailable
! -----------------------------------------------

ReturnString      StringTheory
HeaderDetails     group(NetWebServerHeaderDetailsType).

  CODE
  if self.LoggingOn
    self.Log ('NetWebServer._MakeErrorPacket', 'Error = ' & p_ErrorNumber & ' ' & p_ErrorString & ' : ' & p_ErrorDescription)
  end
  if p_Ajax
    Return ''
  End
  if p_DefaultHeader
    self._Wait()
    HeaderDetails = self.HeaderDetails ! Copy defaults from HeaderDetails
    self._Release()
  end
  case p_ErrorNumber
  of 301 orof 302 ! moved
    HeaderDetails.Location = p_ErrorString
  of 304 ! not modified
  Else
    ReturnString.SetValue(self.MakeErrorPage(p_ErrorNumber,p_ErrorString,p_ErrorDescription))
  End
  if p_DefaultHeader
    HeaderDetails.ResponseNumber = p_ErrorNumber
    HeaderDetails.ResponseString = p_ErrorString
                HeaderDetails.Date = today()
                HeaderDetails.Time = clock()
                HeaderDetails.ContentType = 'text/html'
                HeaderDetails.CacheControl = 'no-store, no-cache, must-revalidate, private,post-check=0, pre-check=0, max-age=0'
                HeaderDetails._Pragma = 'no-cache'
                HeaderDetails.ContentLength = ReturnString.Length()
                HeaderDetails.Connection = 'close'
                ReturnString.Prepend(self._CreateHeader(HeaderDetails) & '<13,10>')
        End
  return ReturnString.GetValue()

!------------------------------------------------------------------------------
NetWebServer.MakeErrorPage PROCEDURE  (long p_ErrorNumber, string p_ErrorString, string p_ErrorDescription)
  code
  return         '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"' &|
                 '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' &|
                 '<html class="no-js"><13,10><head><13,10>' & |
                 '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />' & |
                 '<link href="styles/error.css" rel="stylesheet" type="text/css" />' & |
                 '<title>' & p_ErrorNumber & ' ' & clip(p_ErrorString) & '</title>' &|
                 '</head><13,10>' & |
                 '<body class="ErrorPage"><13,10>' & |
                 '<div class="ErrorDiv"><13,10>' & |
                 '<hr></hr><13,10>' & |
                 '<h1>' & clip(p_ErrorString) & '</h1><13,10>' & clip (p_ErrorDescription) & '<13,10>' & |
                 '<br><br>' &  |
                 '<hr></hr><13,10>' & |
                 '</div>' & |
                 '</body><13,10>' & |
                 '</html><13,10>'

!------------------------------------------------------------------------------

NetWebServer._GetHeaderField PROCEDURE  (string p_SearchString,*string p_MainString,long p_StartPos,long p_EndPos,long p_Offset,long p_CaseInSensitive)
local                 group, pre(loc)
pos1                    long
pos3                    long
tempString              string (1024)
                      end

! -----------------------------------------------------
! Same as NetEmailReceive._GetLineEntry
!
! String1 = 'Description1: Cool<13,10>Description2: Hot<13,10>Description3: Awesome<13,10>'
! Message (GetLineEntry ('description2:', String1, 1, len (clip(String1))))
!
! Displays 'Hot'
! -----------------------------------------------------

  CODE
  if p_startPos > p_endPos or p_startpos < 1
    return ''
  end
  if p_endpos > len(p_MainString)
    return ''
  end

  if p_CaseInSensitive
    loc:pos1 = InString (lower (p_searchString),lower (p_MainString [(p_startPos) : p_endPos]),1,1)
  else
    loc:pos1 = InString (p_searchString, p_MainString [(p_startPos) : p_endPos], 1, 1)
  end
  if loc:pos1 > 0
    loc:pos1 += p_startPos - 1
    if loc:pos1 <= len(p_MainString) and loc:pos1 <= p_endpos and loc:pos1 > 0
      loc:pos3 = InString ('<13,10>',p_MainString [loc:pos1 : p_endPos],1,1)
      if loc:pos3 = 0
        loc:pos3 = p_endPos
      else
        loc:pos3 += loc:pos1 - 1
      end
      if (loc:pos1 + len (clip (p_searchString)) + p_Offset) > 0 and |
         (loc:pos1 + len (clip (p_searchString)) + p_Offset) <= (loc:pos3 - 1) and |
         (loc:pos3 - 1) <= len(p_MainString)
        return (p_MainString [(loc:pos1 + len (clip (p_searchString)) + p_Offset) : (loc:pos3 - 1)])
      end
    end
  end
  return ('')

!------------------------------------------------------------------------------
NetWebServer._CreateHeader PROCEDURE  (NetWebServerHeaderDetailsType p_HeaderDetails,Long p_Started=0)
HeaderStr     StringTheory
x             Long
  CODE
  case p_HeaderDetails.ResponseNumber
  of '200'
    self.StatsReply200Count += 1
  of '304'
    self.StatsReply304Count += 1
  of '404'
    self.StatsReply404Count += 1
  end
  If p_Started = 0
    HeaderStr.SetValue(clip(p_HeaderDetails.HTTP) & ' ' & p_HeaderDetails.ResponseNumber & ' ' & clip (p_HeaderDetails.ResponseString) & '<13,10>')
  Else
    HeaderStr.SetValue('<13,10>')
  End
  if p_HeaderDetails.Date <> 0 or p_HeaderDetails.Time <> 0
    HeaderStr.Append('Date: ' & self._CreateGMTDate(p_HeaderDetails.Date, p_HeaderDetails.Time) &'<13,10>')
  end
  if p_HeaderDetails.Server <> ''
    HeaderStr.Append('Server: ' & clip (p_HeaderDetails.Server) & '<13,10>')
  end
  if p_HeaderDetails.ETag <> ''
    HeaderStr.Append('ETag: ' & clip (p_HeaderDetails.ETag) & '<13,10>')
  end
  if p_HeaderDetails.ExpiresDate <> 0 or p_HeaderDetails.ExpiresTime <> 0
    HeaderStr.Append('Expires: ' & self._CreateGMTDate(p_HeaderDetails.ExpiresDate, p_HeaderDetails.ExpiresTime) & '<13,10>')
  end
  if p_HeaderDetails.LastModifiedDate <> 0 or p_HeaderDetails.LastModifiedTime <> 0
    HeaderStr.Append('Last-Modified: ' & self._CreateGMTDate(p_HeaderDetails.LastModifiedDate, p_HeaderDetails.LastModifiedTime) & '<13,10>')
  end
  if p_HeaderDetails.Age <> ''
    HeaderStr.Append('Age: ' & clip (p_HeaderDetails.Age) & '<13,10>')
  end
  if p_HeaderDetails.ContentLength <> ''
    HeaderStr.Append('Content-Length: ' & clip (p_HeaderDetails.ContentLength) & '<13,10>')
  end
  if p_HeaderDetails.ContentEncoding <> ''
    HeaderStr.Append('Content-Encoding: ' & clip (p_HeaderDetails.ContentEncoding) & '<13,10>')
  end
  if p_HeaderDetails.ContentType <> ''
    HeaderStr.Append('Content-Type: ' & clip (p_HeaderDetails.ContentType) & '<13,10>')
  end
  if p_HeaderDetails.ContentDisposition <> ''
    HeaderStr.Append('Content-Disposition: ' & clip (p_HeaderDetails.ContentDisposition) & '<13,10>') !'Content-Disposition: inline; filename="employees.csv"
  end
  if p_HeaderDetails.CacheControl <> ''
    HeaderStr.Append('Cache-Control: ' & clip (p_HeaderDetails.CacheControl) & '<13,10>')
  end
  if p_HeaderDetails._Pragma <> ''
    HeaderStr.Append('Pragma: ' & clip (p_HeaderDetails._Pragma) & '<13,10>')
  end
  if p_HeaderDetails.Location <> ''
    HeaderStr.Append('Location: ' & clip (p_HeaderDetails.Location) & '<13,10>')
  end
  if p_HeaderDetails.WWWAuthenticate <> ''
    HeaderStr.Append('WWW-Authenticate: ' & clip (p_HeaderDetails.WWWAuthenticate) & '<13,10>')
  end
  HeaderStr.Append(clip(p_HeaderDetails.cookies))
  if p_HeaderDetails.Connection <> ''
    HeaderStr.Append('Connection: ' & clip (p_HeaderDetails.Connection) & '<13,10>')
  end
  if p_HeaderDetails.AccessControlAllowOrigin <> ''
    HeaderStr.Append('Access-Control-Allow-Origin: ' & clip (p_HeaderDetails.AccessControlAllowOrigin) & '<13,10>')
  end
  if p_HeaderDetails.AccessControlAllowCredentials <> ''
    HeaderStr.Append('Access-Control-Allow-Credentials: ' & clip (p_HeaderDetails.AccessControlAllowCredentials) & '<13,10>')
  end
  if p_HeaderDetails.AccessControlAllowMethods <> ''
    HeaderStr.Append('Access-Control-Allow-Methods: ' & clip (p_HeaderDetails.AccessControlAllowMethods) & '<13,10>')
  end
  if p_HeaderDetails.AccessControlMaxAge <> ''
    HeaderStr.Append('Access-Control-Allow-MaxAge: ' & clip (p_HeaderDetails.AccessControlMaxAge) & '<13,10>')
  end
  if p_HeaderDetails.Php <> ''
    HeaderStr.Append(clip (p_HeaderDetails.Php) & '<13,10>')
  end
  return (HeaderStr.GetValue())

!------------------------------------------------------------------------------

NetWebServer._CreateGMTDate PROCEDURE  (long p_Date, long p_Time) ! Declare Procedure 3
tempTime          string (10)
TimeZone          string (5)
t1                long
t2                long
MinDiff           long ! Minutes Difference (from TimeZone)
ONE_DAY           equate (8640000)
  CODE
  ! This doesn't work for Internet Explorer (tested with v6.0.2900) with the +0200 format
  ! So we convert the time to GMT


  TimeZone = NetGetTimeZone()
  t1 = TimeZone [2:3]
  t2 = TimeZone [4:5]
  MinDiff = t1 * 60 + t2
  if TimeZone[1] = '-'
    p_Time = p_Time + MinDiff*6000
  else
    p_Time = p_Time - MinDiff*6000
  end
  if p_Time < 0
    p_Time += ONE_DAY
    p_Date -= 1
  end
  if p_Time > ONE_DAY
    p_Time -= ONE_DAY
    p_Date += 1
  end

  tempTime = format(p_Time,@T4)

  ! Fri, 8 Oct 2004 16:54:48 +0200

  return (NET:DayOfWeek [(p_Date % 7) + 1] & ', ' & format (day(p_Date),@N02) & ' ' & NET:Months[Month(p_Date)] & |
          ' ' & year(p_Date) & ' ' & format(tempTime[1:2],@N02) & ':' & format(tempTime[4:5],@N02) & |
          ':' & format(tempTime[7:8],@N02) & ' ' & 'GMT')

!------------------------------------------------------------------------------
NetWebServer.AddScript    PROCEDURE  (string p_Script,Long p_NoAutoCheckCache=0)
cc  cstring(5)
  CODE
  if instring('?',p_Script,1,1) = 0
    if self._SitesQueue.defaults.AutoCheckCache and p_NoAutoCheckCache = 0 then cc = '?c=1'.
  end
  if lower(left(p_script,5)) = 'http:' or lower(left(p_script,6)) = 'https:'
    RETURN('<script src="' & clip(p_Script) & cc & '" type="text/javascript"></script><13,10>')
  elsif left(p_Script,1) = '/'
    RETURN('<script src="' & clip(p_Script) & cc & '" type="text/javascript"></script><13,10>')
  else
    RETURN('<script src="/' & clip(self._SitesQueue.defaults.ScriptsDir) & '/' & clip(p_Script) & cc & '" type="text/javascript"></script><13,10>')
  end
!------------------------------------------------------------------------------
NetWebServer.AddStyle     PROCEDURE  (string p_Style,Long p_theme=0)
cc  cstring(5)
  CODE
  if instring('?',p_Style,1,1) = 0
    if self._SitesQueue.defaults.AutoCheckCache then cc = '?c=1'.
  end
  if lower(left(p_Style,5)) = 'http:' or lower(left(p_Style,6)) = 'https:'
    RETURN('<link href="' & clip(p_Style) & cc & '" rel="stylesheet" /><13,10>')
  elsif p_theme
    RETURN('<link href="/' & clip(self._SitesQueue.defaults.ThemesDir) & '/' & clip(p_Style)  & cc & '" rel="stylesheet" /><13,10>') ! html5
  Elsif left(p_Style,1) = '/'
    RETURN('<link href="' & clip(p_Style) & cc & '" rel="stylesheet" /><13,10>')
  else
    RETURN('<link href="/' & clip(self._SitesQueue.defaults.StylesDir) & '/' & clip(p_Style) & cc & '" rel="stylesheet"/><13,10>') ! html5
  end
!------------------------------------------------------------------------------
NetWebServer.AddGoogleFont  PROCEDURE(string p_Family, long p_regular,long p_bold, long p_italic, long p_bolditalic,long p_Desktop, long p_Mobile,<String p_Charset>,<String p_Alphabet>,<String p_Effect>)
st   stringTheory
found  long
  code
  if p_family = '' or (p_Desktop = 0 and p_Mobile = 0)
    return
  end
  st.SetValue('family=' & clip(p_Family))
  st.replace(' ','+')
  if p_bold or p_italic or p_bolditalic
    st.appendA(':')
    if p_regular
      if found then st.appendA(',r') else st.appendA('r').
      found = 1
    end
    if p_bold
      if found then st.appendA(',b') else st.appendA('b').
      found = 1
    end
    if p_italic
      if found then st.appendA(',i') else st.appendA('i').
      found = 1
    end
    if p_bolditalic
      if found then st.appendA(',bi') else st.appendA('bi').
    end
  end
  if not omitted(9) & p_Alphabet <> ''       ! alphabet superceeds charset
    st.append('&text=' & self.translate(p_Alphabet))
  elsif not omitted(8) & p_charset <> ''
    st.append('&subset=' & clip(p_charset))
  end
  if not omitted(10) & p_Effect <> ''
    st.append('&effect=' & clip(p_Effect))
  end

  if p_Desktop
    if self._SitesQueue.Defaults.GoogleFonts then self._SitesQueue.Defaults.GoogleFonts = clip(self._SitesQueue.Defaults.GoogleFonts) & '|'.
    self._SitesQueue.Defaults.GoogleFonts = clip(self._SitesQueue.Defaults.GoogleFonts) & st.GetValue()
  end
  if p_Mobile
    if self._SitesQueue.Defaults.GoogleFontsMobile then self._SitesQueue.Defaults.GoogleFonts = clip(self._SitesQueue.Defaults.GoogleFonts) & '|'.
    self._SitesQueue.Defaults.GoogleFontsMobile = clip(self._SitesQueue.Defaults.GoogleFonts) & st.GetValue()
  end

!------------------------------------------------------------------------------
NetWebServer.AddLog    PROCEDURE (FILE p_File,*String p_Field, *String p_Name, String p_DataString,<String p_ip>)
loc:ip  String(NET:IPStringSize+1)
  CODE
  if omitted(6)
  else
    loc:ip = ',' & p_ip
  end
  if self._logOpen = false or Status(p_File) = 0
    do OpenFile
    self._logOpen = true
  end
  p_Field = '<item>' & format(today(),@d1) & ',' & format(clock(),@t4) & clip(loc:ip) & '</item><13,10><request>,' & clip(p_DataString) & '</request><13,10>'
  add(p_File,len(clip(p_Field)))
  If Bytes(p_File) > 10000000
    do OpenFile
  End

OpenFile  routine
  If Status(p_File) <> 0
    close(p_file)
  end
  p_Name = clip(self.LogPath) & '\' & clip(self.WebLogName) & format(today(),@d12) & '-' & format(clock(),@t2) & '.log'
  Create(p_File)
  Open(P_File,42h)
!------------------------------------------------------------------------------
NetWebServer.AddLog     PROCEDURE (String p_Data,<String p_ip>)
  code

!------------------------------------------------------------------------------
NetWebServer.AddLog     PROCEDURE (NetWebServerWorker p_web)
  code

!------------------------------------------------------------------------------
NetWebServer.SendError PROCEDURE  (long p_ErrorNumber, string p_ErrorString, string p_ErrorDescription, Long p_DefaultHeader=1)
  code
  self._wait()
  self.packet.binData = self._MakeErrorPacket(p_ErrorNumber, p_ErrorString, p_ErrorDescription,p_DefaultHeader)
  self.packet.binDataLen = len (clip(self.packet.binData))
  self.packet.ToIP = self.packet.FromIP
  self.Send()
  self.CloseServerConnection(self.packet.OnSocket, self.packet.SockID)
  self._ConnectionDataQueue.Ignore = true
  put (self._ConnectionDataQueue)
  self.error = p_ErrorNumber
  self._release()

!------------------------------------------------------------------------------
NetWebServer.MakeFolders PROCEDURE()
szDirName    CSTRING(FILE:MaxFilePath),AUTO

  CODE
  szDirName = CLIP(self._SitesQueue.defaults.webFolderPath)
  mkdir(szDirName)
  szDirName = CLIP(self._SitesQueue.defaults.webFolderPath) & '\reports'
  mkdir(szDirName)
  szDirName = CLIP(self._SitesQueue.defaults.webFolderPath) & '\' & Self._SitesQueue.defaults.scriptsdir
  mkdir(szDirName)
  szDirName = CLIP(self._SitesQueue.defaults.webFolderPath) & '\' & Self._SitesQueue.defaults.stylesdir
  mkdir(szDirName)
  szDirName = CLIP(self._SitesQueue.defaults.webFolderPath) & '\' & Self._SitesQueue.defaults.themesdir
  mkdir(szDirName)
  szDirName = CLIP(self._SitesQueue.defaults.UploadsPath)
  mkdir(szDirName)
  szDirName = CLIP(self.LogPath)
  mkdir(szDirName)
!------------------------------------------------------------------------------
! moves the contents of one folder, to another folder, deleting the files in the from folder.
NetWebServer.MoveFolder   Procedure(string p_from, string p_to)
szDirName     CSTRING(FILE:MaxFilePath),AUTO
affQueue      queue(FILE:queue),pre(affQueue).
  code
  szDirName = p_To
  mkdir(szDirName)
  Directory(affQueue,clip(p_from)&'\*.*',ff_:NORMAL+ff_:ARCHIVE+ff_:HIDDEN)
  loop while records(affQueue)
    get(affQueue,1)
    Copy(clip(p_from) & '\' & clip(affQueue.name),clip(p_to))
    If errorcode() = 0 and exists(clip(p_to) & '\' & clip(affQueue.name))
      Remove(clip(p_from) & '\' & clip(affQueue.name))
    end
    delete(affQueue)
  End

!------------------------------------------------------------------------------
NetWebServer.GetCountry  PROCEDURE (String pIP,Long pFlag=0)
  code
  Return ''

!------------------------------------------------------------------------------
NetWebServer.jsonOk      Procedure(String p_notok,long p_flags=0)
st  StringTheory
  code
  st.SetValue(p_notOk)
  st.replace('\','\\')
  st.replace('"','\"')
  return st.GetValue()

!------------------------------------------------------------------------------
! caller must pass in a return string of sufficient length or result is truncated.
! function returns the length that should have been used.
NetWebServer._jsok                  PROCEDURE  (string p_notok,long p_html,*String p_ok)
x      long,auto
y      long,auto
ln     long,auto
lo     long,auto
dont   long,auto
ax     long,auto
bx     long,auto
xss    long
  CODE
  y = 1
  ln = len(clip(p_notok))
  lo = len(p_ok)
  ! if html is ok, then do a quick check on the whole content to see if any of it fails the xss test.
  ! if any part fails, then it must all be rejected.
  if band(p_html,Net:HtmlOk) <> 0 and band(p_html,Net:UnsafeHtmlOk) = 0 !
    loop x = 1 to ln
      case p_notok[x]
      of '<'
        xss = self.unSafeHtml(p_notok,x)
      end
      if xss then break.
    end
  end
  loop x = 1 to ln
    case val(p_notok[x])
    of 0  ! null
      if y <= lo then p_ok[y] = ' '.
      y +=1
    of 1 to 9     ! control characters (5.24)
    orof 11 to 12
    orof 14 to 31
      ! remove control characters.
      p_ok[y] = ' '
      y +=1
    of 92 ! \
      if band(p_html,Net:ForwardSlash)>0
       p_ok[y] = '/'
       y += 1
      elsif band(p_html,Net:Parameter)>0
        if y+9 <= lo then p_ok[y : y+9] = '&#92;&#92;'.
        y += 10
      else
        if y+4 <= lo then p_ok[y : y+4] = '&#92;'.
        y += 5
      end
    of 60 ! <
      if band(p_html,Net:HtmlOk + Net:UnsafeHtmlOk)=0 or xss
        if y+3 <= lo then p_ok[y : y+3] = '&lt;'.
        y += 4
      else
        if y <= lo then p_ok[y] = p_notok[x].
        y +=1
      end
    of 62 ! >
      if band(p_html,Net:HtmlOk + Net:UnsafeHtmlOk)=0 or xss
        if y+3 <= lo then p_ok[y : y+3] = '&gt;'.
        y += 4
      else
        if y <= lo then p_ok[y] = p_notok[x].
        y +=1
      end
    of 34 ! "
      if band(p_html,Net:HtmlOk + Net:UnsafeHtmlOk)=0 or xss
        if y+5 <= lo then p_ok[y : y+5] = '&quot;'.
        y += 6
      else
        if y <= lo then p_ok[y] = p_notok[x].
        y +=1
      end
    of 38 ! &
      dont = 0
      if ln > x+1
        case p_notok[x : x+1]
        of '&#'
          dont = 1
        end
      end
      if ln > x+2
        case p_notok[x : x+3]
        of '&lt;' orof '&gt;'
          dont = 1
        end
      end
      if ln > x+3
        case p_notok[x : x+4]
        of '&amp;'
        orof '&#92;'
          dont = 1
        end
      end
      if ln > x+4
        case p_notok[x : x+5]
        of '&quot;'
          dont = 1
        end
      end
      if dont
        if y <= lo then p_ok[y] = p_notok[x].
        y +=1
      else
        if ln > x+4
          case p_notok[x : x+5]
          of '&nbsp;'
            if y+5 <= lo then p_ok[y : y+5] = '&#160;'.
            y += 6
            x += 5
          else
            if band(p_html,Net:DoubleAmp) = 0
              if y+4 <= lo then p_ok[y : y+4] = '&amp;'.
              y += 5
            else
              if y+8 <= lo then p_ok[y : y+8] = '&amp;amp;'.
              y += 9
            end
          end
        else
          if band(p_html,Net:DoubleAmp) = 0
            if y+4 <= lo then p_ok[y : y+4] = '&amp;'.
            y += 5
          else
            if y+8 <= lo then p_ok[y : y+8] = '&amp;amp;'.
            y += 9
          end
        end
      end
    of 127 to 255
      if band(p_html,net:utf8)
        if band(val(p_notok[x]),11100000b) = 11000000b ! 2 char utf
          if x+1 <= ln
            ax = self._utfdecode(p_notok[x : x+1])
          else
            ax = val(p_notok[x])
          end
          x += 1
        elsif band(val(p_notok[x]),11110000b) = 11100000b ! 3 char utf
          if x+2 <= ln
            ax = self._utfdecode(p_notok[x : x+2])
          else
            ax = val(p_notok[x])
          end
          x += 2
        elsif band(val(p_notok[x]),11111000b) = 11110000b ! 4 char utf
          if x+3 <= ln
            ax = self._utfdecode(p_notok[x : x+3])
          else
            ax = val(p_notok[x])
          end
          x += 3
        end
        bx = len(clip(ax))-1 + 3
        if y+bx <= lo then p_ok[y : y+bx] = '&#'&ax&';'.
        y += bx + 1
      else
        if y <= lo then p_ok[y] = p_notok[x].
        y +=1
      end
    else
      if y <= lo
        p_ok[y] = p_notok[x]
      end
      y +=1
    end
  end
  !self._trace('jsok : incoming=' & clip(p_notok) & ' html=' & p_html & ' result=' & clip(p_ok) & ' returning ' & y-1)
  return(y-1)
!------------------------------------------------------------------------------
NetWebServer.TagOk  Procedure(string p_tag)
  code
  case lower(p_tag)
  of 'a' orof 'abbr' orof 'address' orof 'area' orof 'article' orof 'aside' orof 'audio'
  orof 'b' orof 'bdi' orof 'bdo' orof 'blockquote' orof 'br'
  orof 'caption' orof 'cite' orof 'code' orof 'col' orof 'colgroup'
  orof 'data' orof 'dd' orof 'del' orof 'details' orof 'dfn' orof 'div' orof 'dl' orof 'dt'
  orof 'em' orof 'fieldset' orof 'figcaption' orof 'figure' orof 'footer' orof 'font'
  orof 'h1' orof 'h2' orof 'h3' orof 'h4' orof 'h5' orof 'h6' orof 'header' orof 'hgroup' orof 'hr'
  orof 'i' orof 'img' orof 'ins' orof 'kbd' orof 'label' orof 'legend' orof 'li'
  orof 'mark' orof 'marquee' orof 'map' orof 'meter' orof 'nav' orof 'noscript' orof 'ol' orof 'output'
  orof 'p' orof 'pre' orof 'progress' orof 'q' orof 'ruby' orof 'rp' orof 'rt' orof 'response'
  orof 's' orof 'samp' orof 'section' orof 'small' orof 'source' orof 'span' orof 'strong' orof 'sub' orof 'summary' orof 'sup'
  orof 'table' orof 'tbody' orof 'td' orof 'tfoot' orof 'th' orof 'thead' orof 'time' orof 'title' orof 'tr' orof 'track'
  orof 'u' orof 'ul' orof 'var' orof 'video' orof 'wbr'
    return true
  else
    return false
  end
!------------------------------------------------------------------------------
! html stored in the database should be scrubbed before displaying it. This method performs this scrubbing.
! compares the opening tag against a white list of permissable opening tags and then checks for other
! potentially bad signs.
! generally speaking, html should be "safe" with no 'action' (other than a simple link).
! returns 1 for unsafe, 0 for not safe.
! string at pos x contains the < char.
! excluded by design: base, body, button, canvas, comments, command, datalist, embed, eventsource, frameset, frame
!                     form, head, html, iframe, input, keygen, link, menu, meta, nav, object, optgroup,
!                     option, param, script, select, style, textarea,
NetWebServer.UnSafeHtml  Procedure(string p_html, long p_x)
ans  long
tag  string(15)
y    long
z    long
indq long
ingq long
inq  long
ed   long
pt1  string(1024)
blankquotes  long
  code
  ans = true ! assume unsafe
  tag = lower(sub(p_html,p_x+1,size(tag)))
  y = instring(' ',tag,1,1)
  z = instring('%20',tag,1,1)
  y = choose((z < y and z <> 0) or y  = 0,z,y)
  z = instring('>',tag,1,1)
  y = choose((z < y and z <> 0) or y  = 0,z,y)
  z = instring('/',tag,1,2)
  y = choose((z < y and z <> 0) or y  = 0,z,y)

  tag = sub(tag,1,y-1)
  if tag[1] = '/' then tag = sub(tag,2,size(tag)).
  if self.TagOk(tag)
    pt1 = sub(p_html,p_x,size(pt1))
    blankquotes = 1
    do Morphpt1
    if ed ! if there's no end tag withing the space allowed, then reject this html.
      pt1 = self.maxdecode(sub(pt1,1,ed))
      if instring(' on',lower(pt1),1,1) = 0 and instring('script',lower(pt1),1,1) = 0 ! event handlers, and script tags are a no-no
        ! now happy with the "outside quotes" parts, but need a very specific test on inside quotes
        pt1 = sub(p_html,p_x,size(pt1))
        blankquotes = 0
        do Morphpt1
        pt1 = self.maxdecode(sub(pt1,1,ed))
        if instring('javascript:',lower(pt1),1,1) = 0
          ! in theory it could be possible for this word to be harmless, like if it's used in a title, but
          ! that goes down as a false positive. Better to be a bit heavy handed, than a bit light. remember this
          ! is _just_ inside the actual tag, of user generated content.
          ans = 0
        else
          self._trace('UnSafeHtml (1): "javascript:" detected in ' & clip(pt1))
        end
      else
        self._trace('UnSafeHtml (2): " on" or "script" detected in ' & clip(pt1))
      end
    else
      self._trace('UnSafeHtml (3): No end tag detected in ' & clip(pt1))
    end
  else
    self._trace('UnSafeHtml (4): Tag not allowed: ' & clip(tag))
  end
  !self._trace('----- answer = ' & ans)
  return ans

MorphPt1  routine
  loop y = 1 to len(clip(pt1))
    if pt1[y] = '"'              ! double quotes
      indq = choose(indq=0,1,0)
    elsif pt1[y] = ''''          ! single quotes
      inq = choose(inq=0,1,0)
    elsif pt1[y] = '`'           ! grave quotes
      ingq = choose(ingq=0,1,0)
    elsif indq or inq or ingq
       if blankquotes then pt1[y] = ' '.
    elsif pt1[y] = '>'
      ed = y
      break
    end
  end


!------------------------------------------------------------------------------
NetWebServer.MaxDecode              Procedure (String p_html)
x   long
l   long
j   long
a   long
t   long
num long
  code
  x = 1
  l =  len(clip(p_html))
  loop while x < l
    case p_html[x]
    of '<9>' orof '<10>' orof '<13>' orof '<0>' ! remove tabs, cr, lf, null
      p_html = choose(x=1,sub(p_html,2,l),sub(p_html,1,x-1) & sub(p_html,x+1,l))
      cycle
    of '%'
      do MakeNum
      p_html = choose(x=1,chr(num) & sub(p_html,j+1,l),sub(p_html,1,x-1) & chr(num) & sub(p_html,j+1,l))
      ! don't increment x (because if the char is 9,10 etc it needs to still be removed.)
      cycle
    end
    case p_html[x : x + 1]
    of '&#'
      do getNum
      if num < 256 ! we don't really care if chars over 255 are encoded
        p_html = choose(x=1,chr(num) & sub(p_html,j+1,l),sub(p_html,1,x-1) & chr(num) & sub(p_html,j+1,l))
        ! don't increment x (because if the char is 9,10 etc it needs to still be removed.)
        cycle
      end
    end
    x += 1
    l =  len(clip(p_html))
  end
  return p_html

getNum  routine  ! j returns the position of the last digit in the number
  num = 0
  j = x + 1
  if l > x + 1
    if lower(p_html[x+2]) = 'x'
      ! number is in hex format
      j = x + 2
      loop t = x+3 to x+3+6
        if t > l then break.
        case p_html[t]
        of 'a' to 'f'
          num = (num*16) + val(p_html[t]) - 87
          j = t
        orof 'A' to 'F'
          num = (num*16) + val(p_html[t]) - 55
          j = t
        orof '0' to '9'
          num = (num*16) + val(p_html[t]) - 48
          j = t
        of ';'
          j = t
          break
        else
          break
        end
      end
    else
      loop t = x+2 to x+2+6
        if t > l then break.
        case p_html[t]
        of '0' to '9'
          num = (num * 10) + val(p_html[t]) - 48
          j = t
        of ';'
          j = t
          break
        else
          break
        end
      end
    end
  end

MakeNum  routine ! j returns the position of the last digit in the number
  num = 0
  j = x
  If x + 2 > l then exit.
  j = x + 2
  case p_html[x+1]
  of 0 to 9
    num = val(p_html[x+1]) - 48 ! 0
  of 'A' to 'F'
    num = val(p_html[x+1]) - 55 ! A=10
  of 'a' to 'f'
    num = val(p_html[x+1]) - 87 ! a=10
  end
  num = num * 16
  case p_html[x+2]
  of 0 to 9
    num = num + val(p_html[x+2]) - 48
  of 'A' to 'F'
    num = num + val(p_html[x+2]) - 55
  of 'a' to 'f'
    num = num + val(p_html[x+2]) - 87 ! a=10
  end


!------------------------------------------------------------------------------
NetWebServer._utfencode             Procedure (Long p_utf,*Long rLen)
returnValue  string(4)
  code
  case p_utf
  of 0 to 127
    rlen = 1
    return chr(p_utf)
  of 128 to 2047
    returnvalue[1] = chr(11000000b + band(bshift(p_utf,-6) ,11111b))
    returnvalue[2] = chr(10000000b + band(p_utf,111111b))
    rlen = 2
    return returnvalue[1:2]
  of 2048 to 65535
    returnvalue[1] = chr(11100000b + bshift(p_utf,-12))
    returnvalue[2] = chr(10000000b + band(bshift(p_utf,-6) ,111111b))
    returnvalue[3] = chr(10000000b + band(p_utf,111111b))
    rlen = 3
    return returnvalue[1:3]
  of 65536 to 1114111
    returnvalue[1] = chr(11110000b + bshift(p_utf,-24))
    returnvalue[2] = chr(10000000b + band(bshift(p_utf,-12) ,111111b))
    returnvalue[3] = chr(10000000b + band(bshift(p_utf,-6) ,111111b))
    returnvalue[4] = chr(10000000b + band(p_utf,111111b))
    rlen = 4
    return returnvalue[1:4]
  end
!------------------------------------------------------------------------------
NetWebServer._utfdecode             Procedure (String p_text)
ReturnValue  long
  code
  case len(p_text)
  of 1
    returnvalue = band(val(p_text[1]),1111111b)
  of 2
    returnvalue = bshift(band(val(p_text[1]),11111b),6) + band(val(p_text[2]),111111b)
  of 3
    returnvalue = bshift(band(val(p_text[1]),1111b),12) + bshift(band(val(p_text[2]),11111b),6) + band(val(p_text[3]),111111b)
  of 4
    returnvalue = bshift(band(val(p_text[1]),1111b),18) + bshift(band(val(p_text[2]),111111b),12) + bshift(band(val(p_text[3]),111111b),6) + band(val(p_text[4]),111111b)
  end
  return returnValue
!------------------------------------------------------------------------------
NetWebServer._PerfStartThread     Procedure()
ans  Long
  code
  self._wait()
  self.Performance.NumberOfThreads += 1
  ans = self.Performance.NumberOfThreads
  if ans >  self.Performance.MaximumThreads
    self.Performance.MaximumThreads = ans
  end
  self._release()
  Return ans
!------------------------------------------------------------------------------
NetWebServer._PerfEndThread     Procedure(real p_TimeTaken,long p_spider,long p_error)
c  long
  Code
  self._wait()

  ! special case, called from WebServer just to count number of 500 (server too busy) errors.
  if p_TimeTaken = 0 and p_spider = 0 and p_error = 500
    self.Performance.NumberOf500Requests += 1
    self._release()
    return
  end

  self.Performance.NumberOfThreads -= 1
  self.Performance.NumberOfSessions = records(self._SessionQueue)
  self.Performance.NumberOfSessionData = records(self._SessionDataQueue)

  ! special case - DeleteSession (or Notify Delete Session) thread. So only thread counter updated,
  !                all the time counters are ignored.
  if p_error = net:NoTimeTaken
    self._release()
    return
  end

  p_TimeTaken = p_TimeTaken / 100  ! convert to seconds
  self.Performance.NumberOfRequests += 1
  if p_spider > 0
    self.Performance.NumberOfSpiderRequests += 1
  end
  if p_error = 404
    self.Performance.NumberOf404Requests += 1
  end

  self.Performance.TotalRequestTime += p_TimeTaken
  if p_timeTaken < 50
    self.Performance.NumberOfRequestsLTHalf += 1
    self.Performance.RequestTimeLTHalf += p_TimeTaken
    self.Performance.AverageRequestTimeLTHalf = self.Performance.RequestTimeLTHalf / self.Performance.NumberOfRequestsLTHalf
  elsif p_timeTaken < 100
    self.Performance.NumberOfRequestsLTOne  += 1
    self.Performance.RequestTimeLTOne += p_TimeTaken
    self.Performance.AverageRequestTimeLTOne = self.Performance.RequestTimeLTOne / self.Performance.NumberOfRequestsLTOne
  elsif p_timeTaken < 200
    self.Performance.NumberOfRequestsLTTwo  += 1
    self.Performance.RequestTimeLTTwo += p_TimeTaken
    self.Performance.AverageRequestTimeLTTwo = self.Performance.RequestTimeLTTwo / self.Performance.NumberOfRequestsLTTwo
  elsif p_timeTaken < 500
    self.Performance.NumberOfRequestsLTFive += 1
    self.Performance.RequestTimeLTFive += p_TimeTaken
    self.Performance.AverageRequestTimeLTFive = self.Performance.RequestTimeLTFive / self.Performance.NumberOfRequestsLTFive
  Else
    self.Performance.NumberOfRequestsGTFive += 1
    self.Performance.RequestTimeGTFive += p_TimeTaken
    self.Performance.AverageRequestTimeGTFive = self.Performance.RequestTimeGTFive / self.Performance.NumberOfRequestsGTFive
  end
  c = clock() % 8640000
  self.PerformanceLoad.RequestsPerMinute[(clock()%360000)/6000+1,clock()/360000+1,day(today()),month(today())] += 1

  if self.Performance.NumberOfSessions > self.Performance.MaximumSessions
    self.Performance.MaximumSessions = self.Performance.NumberOfSessions
  end

  if self.Performance.NumberOfSessionData > self.Performance.MaximumSessionData
    self.Performance.MaximumSessionData = self.Performance.NumberOfSessionData
  end
  self._release()
!------------------------------------------------------------------------------
!------------------------------------------------------------------------------
NetWebServerWorker.Construct PROCEDURE  ()
  CODE
  self._LocalDataQueue &= new (NetWebServerLocalDataQueueType)
  self._SaveFormStateQueue  &= new (NetFormStateQueueType)
  self._PreCallQueue &= new (NetPreCallQueueType)
  self._ResponseQueue &= new(NetHandlerResponseQueueType)
  self.PlannerQueue &= new(NetPlannerQueueType)
  self.BodyToSend &= new (StringTheory)
  self.UseMapQueue  &= new (NetUseMapQueueType)
  self.EventStack &= new (EventStackType)
  self.noopChar = '.'
  self.noopGap = 1500 ! 15 seconds
  self.DontCleanFilter = 1 ! on in 5.16 for field testing, will be off once all is ok.
  self.verbose = 1
  self.CRLF = '<13,10>'
!------------------------------------------------------------------------------
NetWebServerWorker.Destruct PROCEDURE  ()
x  long
  CODE
  dispose(self.BodyToSend)
  dispose(self.WholeURL)

  if Not self.RequestData.DataString &= NULL ! dispose of this memory
    dispose (self.RequestData.DataString)
    self.RequestData.DataString &= NULL
    self.RequestData.DataStringLen = 0
  end

  If Not self._LocalDataQueue &= Null
    Loop x = 1 to records(self._LocalDataQueue)
      Get(self._LocalDataQueue,x)
      Dispose(self._LocalDataQueue.ExtValue)
    End
    free (self._LocalDataQueue)
    dispose (self._LocalDataQueue)
  end

  if not self._SaveFormStateQueue &= null
    free(self._SaveFormStateQueue)
    dispose(self._SaveFormStateQueue)
  End

  if not self._PreCallQueue &= null
    free(self._PreCallQueue)
    dispose(self._PreCallQueue)
  End

  if not self.PlannerQueue &= null
    self.EmptyPlannerQueue()
    dispose(self.PlannerQueue)
  End

  if not self._ResponseQueue &= null
    free(self._ResponseQueue)
    dispose(self._ResponseQueue)
  End

  if Not (self.UseMapQueue &= Null)
    free(self.UseMapQueue)
    dispose (self.UseMapQueue)
    self.UseMapQueue &= null
  end

  if not (self.EventStack &= Null)
    free(self.EventStack)
    dispose(self.EventStack)
    self.EventStack &= null
  end

!------------------------------------------------------------------------------
NetWebServerWorker.Redirect PROCEDURE(String p_to, Long p_ToSecure=0, Long p_Repost=1, Long p_Type=301)
  code
  self.RequestData.Webserver._Redirect(self.RequestData,p_to,p_ToSecure,p_Repost,p_Type)
  self.alldone = 1
!------------------------------------------------------------------------------
NetWebServerWorker.ProcessRequest PROCEDURE  (string p_String)
  CODE
  self.RequestData = p_String
  self.PerfStartThread()

  self._CopyDefaultSiteDetails()  ! Copies the Default HeaderDetails from the main WebServer object, to this object

  if self.RequestData.RequestMethodType = NetWebServer_NOTIFYDELETESESSION ! Pre-Delete Session - called by HOST for all DLL's (expect itself)
    self.SessionId = self.RequestData.SessionId
    self.RequestData.WebServer.AddLog(self)
    if self.SessionId <> ''
      self.NotifyDeleteSession()
    End
    self.PerfEndThread(net:NoTimeTaken)
    return
  elsif self.RequestData.RequestMethodType = NetWebServer_DELETESESSION ! Delete Session - not called by HOST (except for itself)
    self.SessionId = self.RequestData.SessionId
    self.RequestData.WebServer.AddLog(self)
    if self.SessionId <> ''
      self.NotifyDeleteSession()
      self.DeleteSession()
    End
    self.PerfEndThread(net:NoTimeTaken)
    return
  End
  if (self.RequestData.DataString &= NULL)
    self._trace('Bad Data passed to NetWebServerWorker.ProcessRequest')
    self.PerfEndThread(net:NoTimeTaken)
    return
  End

  self._SetUserAgent()
  self.RequestMethodType = self.RequestData.RequestMethodType
  self._ParseRequestHeader()
  if self.ReplyContentType = ''
    self.ReplyContentType = self._GetContentType(self.PageName)
  end
  if self.requestdata.webserver.SSL = 0
    if instring('\' & clip(lower(self.site.securedir)) & '\',lower(self.RequestFileName),1,1)
      self.Redirect('',Net:Web:ToSecure,Net:Web:Repost,Net:Web:PermRedirect)
      self.PerfEndThread()
      Return
    end
  end
  self._FindSessionID()
  ! To prevent Session Spoofing, do a check on the From IP of the session
  If self.SessionID <> '' and self.GetSessionIP() <> self.requestdata.fromip and self.site.NoIPSpoofing = 1
    self.SendError (403,'', 'Bad Session ID')
    self.PerfEndThread()
    return
  End
  if self.GetSessionValue('_ChainRefer') <> ''
    self.RequestReferer = self.GetSessionValue('_ChainRefer')
    self.SetSessionValue('_ChainRefer','')
  end
  self._ReadCookies(self.RequestData.DataString)
  self.ProcessVerb()
  self.ReplyComplete()

!------------------------------------------------------------------------------
NetWebServerWorker.ProcessVerb PROCEDURE  ()
  Code
  case self.RequestMethodType
  of NetWebServer_GET
  orof NetWebServer_DELETE
  orof NetWebServer_HEAD
    self._HandleGet()
    if self.AllDone then return.
    self._HandleGetRest()
    self._SelectField()
  of NetWebServer_POST
  orof NetWebServer_PUT
    self._HandlePost()
    if self.AllDone then return.
    self._HandleGetRest()
    self._SelectField()
  end

!------------------------------------------------------------------------------
NetWebServerWorker.ProcessTag PROCEDURE  (string p_TagString)
! parameters inside the tag string are assumed to be "formatted", not raw.
  CODE
  case lower(self.ParseValueString(p_tagstring))
  of 'commonheader'
    self.CommonHeader('')
  of 'selectfield'
    Self._SelectField()
  of 'message'
    Self.Message('alert',self.GetValue('message'),,Net:Send)!send directly
  of 'busy'
    Self.Busy(Net:Send) !send directly
  else
    if self.IfExistsValue('_parentPage') = 0
      self.SetValue('_parentPage',self.UserURL)
    End
  end

!------------------------------------------------------------------------------
NetWebServerWorker.SendString PROCEDURE  (StringTheory p_DataString, long p_DataStart, long p_DataEnd, Long p_SendHeader)
  code
  self.SendString(p_DataString.value,p_DataStart,p_DataEnd,p_SendHeader)
  return

!------------------------------------------------------------------------------
NetWebServerWorker.SendString PROCEDURE  (*String p_DataString, long p_DataStart, long p_DataEnd, Long p_SendHeader)
loc:justheader     long
  code
  if p_DataStart = 0
    p_DataStart = 1
  end
  If p_DataEnd = 0
    If clip(p_DataString) <> ''
      p_DataEnd = size(p_DataString)
    Else
      p_DataEnd = 1
      loc:justheader = true
    End
  end
  if (self.site.ReuseConnections or self.site.CompressDynamic)
    if loc:justheader = false
      self.BodyToSend.AppendA(p_DataString[p_DataStart : p_DataEnd])
    end
  else
    self.SendStringNow(p_DataString,p_DataStart,p_DataEnd,p_SendHeader,loc:justheader)
  end
  return
!------------------------------------------------------------------------------
NetWebServerWorker.SendStringNow PROCEDURE  (StringTheory p_Data, long p_DataStart, long p_DataEnd, Long p_SendHeader, Long p_JustHeader)
  code
  self.SendStringNow(p_Data.Value,p_DataStart,p_DataEnd,p_SendHeader,p_JustHeader)

!------------------------------------------------------------------------------
NetWebServerWorker.SendStringNow PROCEDURE  (*String p_DataString, long p_DataStart, long p_DataEnd, Long p_SendHeader, Long p_JustHeader)

loc:x              long
loc:StartPos       long,auto
loc:EndPos         long,auto
loc:SendHeader     long,auto
loc:HeaderLen      long

  CODE
  loc:SendHeader = p_SendHeader
  if loc:SendHeader
    loc:HeaderLen = len (clip (self.ReplyHeaderString))
    if loc:HeaderLen = 0
      self.SetHeader200()
      loc:HeaderLen = len (clip (self.ReplyHeaderString))
      if loc:HeaderLen = 0
        loc:SendHeader = false ! don't send, there is no header
      end
    end
  end
  if (p_DataStart > p_DataEnd) or (p_DataEnd > size (p_DataString))
    self.error = ERROR:InvalidStringSize
    self.WinSockError = 0
    self.SSLError = 0
    self.Log ('NetWebServerWorker.SendString', 'Error. Invalid String Start and String End positions. No data will be sent', NET:LogError)
    return
  end
  loc:StartPos = p_DataStart
  loc:EndPos = p_DataEnd
  if (loc:EndPos - loc:StartPos + 1 + loc:HeaderLen) > Net:MaxBinData
    loc:EndPos = loc:StartPos + Net:MaxBinData - 1 - loc:HeaderLen
  end

  if self.LoggingOn
    self.Log ('NetWebServerWorker.SendString', 'About to wait.')
  end
  self.RequestData.WebServer._Wait()

  self.RequestData.WebServer.Packet.SockID = self.RequestData.SockID
  self.RequestData.WebServer.Packet.ToIP = self.RequestData.FromIP
  self.RequestData.WebServer.Packet.OnSocket = self.RequestData.OnSocket
  loop
    if loc:SendHeader and self._HttpHeaderSent = 0
      loc:SendHeader = false
      if p_JustHeader
        self.RequestData.WebServer.Packet.BinData = self.ReplyHeaderString [1 : loc:HeaderLen]
        self.RequestData.WebServer.Packet.BinDataLen = loc:EndPos - loc:StartPos + loc:HeaderLen
      else
        self.RequestData.WebServer.Packet.BinData = self.ReplyHeaderString [1 : loc:HeaderLen] & p_DataString [loc:StartPos : loc:EndPos]
        self.RequestData.WebServer.Packet.BinDataLen = loc:EndPos - loc:StartPos + 1 + loc:HeaderLen
      End
      self._HttpHeaderSent = 1
    else
      self.RequestData.WebServer.Packet.BinData = p_DataString [loc:StartPos : loc:EndPos]
      self.RequestData.WebServer.Packet.BinDataLen = loc:EndPos - loc:StartPos + 1
    end
    self.RequestData.WebServer.Send()
    self._sentbytes += self.RequestData.WebServer.Packet.BinDataLen
    if loc:EndPos >= p_DataEnd
      break ! done
    end
    loc:StartPos = loc:EndPos + 1 ! Get data ready for next send
    loc:EndPos = loc:StartPos + Net:MaxBinData - 1
    if loc:EndPos > p_DataEnd
      loc:EndPos = p_DataEnd
    end
  end
  self.RequestData.WebServer._Release()
  return

!------------------------------------------------------------------------------
NetWebServerWorker._FlushSend  Procedure(StringTheory p_Buffer,Long p_compress=-1)
  code
  if self.flushed then return.

  if self.RequestGzip and p_compress <> net:AlreadyCompressed and p_compress <> net:dontCompress and p_Buffer.LengthA() > 1500
    case p_Buffer.Gzip() ! if you get a compile error here, then update StringTheory
    of st:Z_DLL_ERROR
      self._trace('NetTalk WebServer - unable to compress data - zlibwapi.dll Not found')
    of st:Z_OK
      self.ReplyContentEncoding = 'gzip'
      self.HeaderDetails.ContentEncoding = self.ReplyContentEncoding
    end
  end
  if self._HttpHeaderSent = false
    if self.site.ReuseConnections
      self.HeaderDetails.Connection = 'keep-alive'
    end
    If Self.RequestMethodType <> NetWebServer_HEAD or self.HeaderDetails.ContentLength = ''
      self.HeaderDetails.ContentLength = p_Buffer.LengthA()
    End
    if self.HeaderDetails.ResponseNumber = '200' or self.HeaderDetails.ResponseNumber = ''
      self.SetHeader200()
    end
    self.ReplyHeaderString = self.CreateHeader(self.HeaderDetails) & '<13,10>'
    If Self.RequestMethodType = NetWebServer_HEAD
      p_Buffer.SetValue(self.ReplyHeaderString,st:clip)
    Else
      p_Buffer.Prepend(clip(self.ReplyHeaderString))
    End
    self._HttpHeaderSent = true
  elsif Self.RequestMethodType = NetWebServer_HEAD
    p_Buffer.SetValue('')
  end

  self.RequestData.WebServer._Wait()

  self.RequestData.WebServer.Packet.SockID = self.RequestData.SockID
  self.RequestData.WebServer.Packet.ToIP = self.RequestData.FromIP
  self.RequestData.WebServer.Packet.OnSocket = self.RequestData.OnSocket
  self.RequestData.WebServer.Send(p_Buffer)
  self.RequestData.WebServer._Release()
  self.flushed = true
  return
!------------------------------------------------------------------------------
NetWebServerWorker._CopyDefaultSiteDetails PROCEDURE  ()
  CODE
  self._Wait()
  self.HeaderDetails = self.RequestData.WebServer.HeaderDetails ! Copy the default values from the WebServer object
  if self.RequestData.SiteId = 0 then self.RequestData.SiteId = 1.
  Get(self.RequestData.WebServer._SitesQueue,self.RequestData.SiteId)
  Self.Site = self.RequestData.WebServer._SitesQueue
  self._Release()
!------------------------------------------------------------------------------
NetWebServerWorker.SendError PROCEDURE  (long p_ErrorNumber, string p_ErrorString, string p_ErrorDescription, Long p_DefaultHeader=1)
! -----------------------------------------------
! "200"   ; OK
! "201"   ; Created
! "202"   ; Accepted
! "204"   ; No Content
! "301"   ; Moved Permanently
! "302"   ; Moved Temporarily
! "304"   ; Not Modified
! "400"   ; Bad Request
! "401"   ; Unauthorized
! "403"   ; Forbidden
! "404"   ; Not Found
! "500"   ; Internal Server Error
! "501"   ; Not Implemented
! "502"   ; Bad Gateway
! "503"   ; Service Unavailable
! -----------------------------------------------
OurErrorString      stringtheory
  CODE
  if p_errorNumber >= 400
    self._trace ('SendError = ' & p_ErrorNumber & ' ' & p_ErrorString & ' : ' & p_ErrorDescription & ' FileRequest=[' & clip (self.PageName) & ']')
  end
  OurErrorString.SetValue(self.MakeErrorPacket (p_ErrorNumber, p_ErrorString, p_ErrorDescription, p_DefaultHeader))
  self.SendStringNow (OurErrorString, 1, OurErrorString.Length(), NET:NoHeader, false ) ! OurErrorString already contains a header.
  self.HtmlError = p_ErrorNumber

!------------------------------------------------------------------------------
NetWebServerWorker.ProcessPost PROCEDURE  ()
  Code
  Return 0
!------------------------------------------------------------------------------
NetWebServerWorker.ProcessLink PROCEDURE  (<string p_action>)
loc:submitbutton  String(20)
loc:lookupfield   String(256)
loc:refresh       like (NetWebServerLocalDataQueueType.Value)
F                 &FILE
FNULL             &File
K                 &KEY
err               Long
loc:action        Long
x                 Long
loc:temppagename  like(NetWebServerWorker.pagename)
initstage         Long
  CODE
  if self.AllDone then return.
  FNULL &= NULL

  if omitted(2) !(p_action)
    loc:submitbutton = self.SubmitButton()
  elsif p_action = ''
    loc:submitbutton = self.SubmitButton()
  else
    loc:submitbutton = lower(p_Action)
  end
  !self._trace('ProcessLink: loc:SubmitButton=' & clip(loc:SubmitButton) )
  !self._trace('PL1: self.FormSettings.file=' & clip(self.FormSettings.file) & ' State=' & self.GetValue('FormState') & ' action=' & self.FormSettings.action )
  loop
    !self._trace('Inside Loop loc:SubmitButton=' & loc:SubmitButton)
    case loc:SubmitButton
    of 'save'
      self.GetSettings(self.GetValue('FormState'))
      !self._trace('SAVE: State=' & self.GetValue('FormState') & ' Formsettings: action=' & self.FormSettings.action & ' parent=' & clip(self.FormSettings.parentpage) & ' id=' & clip(self.FormSettings.RecordId[1]) &' target=' & clip(self.FormSettings.target) & ' originalaction=' & self.formSettings.OriginalAction)
      self._InsertAfterSave = 0
      do AssignFileKey
      if self.SaveForm(F,K,self.FormSettings.action) <> Net:Web:InvalidRecord  ! F is allowed to be null here for Memory forms.
        if self._InsertAfterSave = 1
          self.DeleteValue('Save_btn')
          loc:submitbutton = 'insert'
          do AssignFileKey
          If not K &= NULL
            Self.InitializeForm(F,K,loc:submitbutton)
          End
        else
          self.SetValue('FormDone_Btn','1')
          self.DeleteValue('Save_btn')   ! allows chaining from one form to another.
          if self.IfExistsValue('_popup_')
            Case self.formSettings.OriginalAction
            of NET:ChangeRecord
              self.SetValue('_EIPRow_',1)
            Else
              self.SetValue('_EIPTable_',1)
            End
          end
          !self._trace('BEFORE POPS: ' & ' Formsettings: action=' & self.FormSettings.action & ' parent=' & clip(self.FormSettings.parentpage) & ' state=' & clip(self.formstate))
          if self.FormSettings.target = '' or self.FormSettings.target = '_self'
            self.PopSettings(self.formstate)
          end
          !self._trace('AFTER POPS: ' & ' Formsettings: action=' & self.FormSettings.action & ' parent=' & clip(self.FormSettings.parentpage) & ' state=' & clip(self.formstate))
          if self.IfExistsValue('_auto_')
            self.Requestfilename = self.GetValue('_auto_')
          end
          self.ProcessLink()
        end
      end

    of 'deletef'
      self.GetSettings(self.GetValue('FormState'))
      do AssignFileKey
      if not K &= NULL
        if self.SaveForm(F,K,Net:DeleteRecord) <> Net:Web:InvalidRecord
          self.SetValue('FormDone_Btn','1')
          self.DeleteValue('Deletef_btn')   ! allows chaining from one form to another.
          self.PopSettings(self.formstate)
          self.ProcessLink()
        end
      else
        self.PopSettings(self.formstate)
      end

    of 'cancel'
    orof 'close'
      self.GetSettings(self.GetValue('FormState'))
      self.SetValue('_form:inited_',1)
      do AssignFileKey
      If Not K &= NULL
        case self.FormSettings.action
        of net:changerecord
          self._openfile(F)
          if self._LoadRecord(F,K,self.FormSettings.recordID) = 0
            self.CallForm(F,Net:Web:Cancel)
          end
          Self.ProcessFormCancel(F,K)
          self._closefile(F)
        of net:insertrecord orof net:copyrecord
          self._openfile(F)
          self.CallForm(F,Net:Web:Cancel)
          Self.ProcessFormCancel(F,K)
          self._closefile(F)
        end
        !Self.ProcessFormCancel(F,K)
      Else
        self.CallForm(FNULL,Net:Web:Cancel)
      End
      self.SetValue('FormDone_btn','1')
      self.DeleteValue('Cancel_Btn')   ! allows chaining from one form to another.
      self.DeleteValue('Close_Btn')
      self.DeleteValue('_parentProc_')  ! going from a form back to a browse, leaves the parent in.
      self.PopSettings(self.formstate)
      self.ProcessLink()

    of 'insert'
      Self.CallForm(FNULL,NET:WEB:SetPics)
      self.LocalQueueToSessionQueue()
      loc:action = self.CallForm(FNULL,Net:Web:Init + Net:InsertRecord)
      if loc:action
        do AssignFileKey
        If not K &= NULL
          Self.InitializeForm(F,K,loc:submitbutton)
        End
      end
      !self._trace('INSERT: ' & ' Formsettings: action=' & self.FormSettings.action & ' parent=' & clip(self.FormSettings.parentpage) & ' state=' & clip(self.formstate))
    of 'change'
      Self.CallForm(FNULL,NET:WEB:SetPics)
      self.LocalQueueToSessionQueue()
      loc:action = self.CallForm(FNULL,Net:Web:Init + Net:ChangeRecord)
      if loc:action
        do AssignFileKey
        If not K &= NULL
          Self.InitializeForm(F,K,loc:submitbutton) ! loads the record from disk
        End
        If self.IfExistsValue('_popup_')
          loc:action = self.CallForm(FNULL,Net:Web:Populate + Net:ChangeRecord)
        End
      end
      if self.IfExistsValue('_auto_')
        self.DeleteValue('Change_btn')
        !self._trace('self.formstate=' & self.formstate)
        self.SetValue('FormState',self.formstate)
        loc:SubmitButton = 'save'
        cycle
      End

    of 'copy'
      Self.CallForm(FNULL,NET:WEB:SetPics)
      self.LocalQueueToSessionQueue()
      loc:action = self.CallForm(FNULL,Net:Web:Init + Net:CopyRecord)
      if loc:action
        do AssignFileKey
        If not K &= NULL
          Self.InitializeForm(F,K,loc:submitbutton)
        End
      end

    of 'view'
      Self.CallForm(FNULL,NET:WEB:SetPics)
      self.LocalQueueToSessionQueue()
      If self.IfExistsValue('_popup_')
        loc:action = self.CallForm(FNULL,Net:Web:Populate + Net:ViewRecord)
      Else
        loc:action = self.CallForm(FNULL,Net:Web:Init + Net:ViewRecord)
      End

      if loc:action
        do AssignFileKey
        If not K &= NULL
          Self.InitializeForm(F,K,loc:submitbutton)
        End
        self.SetValue('ViewOnly',1)
      end

    of 'deleteb'
      self.GetSettings(self.GetValue('FormState'))        ! needed for browse embedded on form.
      self.formsettings.proc = self.GetValue('_fromform_')  ! want the browse's form, not the parent form
      Self.CallForm(FNULL,NET:WEB:SetPics)
      self.LocalQueueToSessionQueue()
      loc:TempPageName = SELF.PageName
      SELF.PageName = self.GetValue('_fromform_')
      loc:action = self.CallForm(FNULL,Net:Web:Init + Net:DeleteRecord)
      if loc:action
        do AssignFileKey
        If not K &= NULL
          Self.InitializeForm(F,K,loc:submitbutton)
        End
        SELF.PageName = loc:TempPageName
      end
      self.GetSettings(self.GetValue('FormState')) ! re-load original settings

    of 'lookup'
      Self.CallForm(FNULL,NET:WEB:SetPics)
      self.GetSettings(self.GetValue('FormState')) ! needed for multiple lookups & insert mode.
      !self._trace('Lookup button pressed self.FormSettings.file=' & clip(self.FormSettings.file) & ' State=' & self.GetValue('FormState') & ' action=' & self.FormSettings.action )
      self.LocalQueueToSessionQueue(self.FormSettings.file)
      self.setsessionvalue('ForeignField',self.getvalue('ForeignField'))
      self.setsessionvalue('Push1',self.getvalue('FormState'))

    of 'browsecancel'
      self.SetValue('_form:inited_',1)
      if self.GetSessionValue('Push1') <> ''
        self.GetSettings(self.GetSessionValue('Push1'))
        self.RestoreSessionValues(self.formsettings.file)
      end
      self.CallForm(FNULL,Net:Web:Init)
      if self.IfExistsValue('LookupField')
        self.CallForm(FNULL,Net:Web:AfterLookup + Net:Web:Cancel)
      end

    of 'select'
      if self.GetSessionValue('Push1') <> ''
        self.GetSettings(self.GetSessionValue('Push1'))
        self.RestoreSessionValues(self.formsettings.file)
      end
      self.SetValue('_form:inited_',1)
      loc:LookupField = self.getvalue('LookupField')
      !F &= self._LoadLookupRecord()
      self.GetBrowseValue(self.GetValue('_bidv_'),Net:Web:Record) ! loads the primary key of the lookup table into the session queue, and loads the record as well.
      self.CallForm(FNULL,Net:Web:Init)
      if self.getvalue(self.getSessionvalue('ForeignField'))
        self.setvalue       (loc:LookupField,self.getvalue(self.getSessionvalue('ForeignField')))
        self.setsessionvalue(loc:LookupField,self.getvalue(self.getSessionvalue('ForeignField')))
      elsif self.getsessionvalue('ForeignField')
        self.setvalue       (loc:LookupField,self.getsessionvalue(self.getsessionvalue('ForeignField')))
        self.setsessionvalue(loc:LookupField,self.getsessionvalue(self.getsessionvalue('ForeignField')))
      end
      self.CallForm(FNULL,Net:Web:AfterLookup)
    of 'logout'
      self.SetSessionLoggedIn(0)
    else
      self.CallForm(FNULL,Net:Web:Init)
      if self.IfExistsValue('_refresh_')
        loc:refresh = self.GetValue('_refresh_')
        if Instring(':',loc:refresh,1,1)
          self.SetSessionValue(loc:refresh,self.GetValue(loc:refresh))
          self.SetValue('_refresh_','first')
        End
      end
    end
    break
  end ! loop
  !self._trace('end of ProcessLink')
  !self._trace('PL2: self.FormSettings.file=' & clip(self.FormSettings.file) & ' State=' & self.GetValue('FormState') & ' action=' & self.FormSettings.action )

AssignFileKey  routine
  F &= self._NetWebFileNamed(self.FormSettings.file)
  If Not F &= NULL
    K &= self._NetWebKeyNamed(F,self.FormSettings.key)
  Else
    K &= NULL
  End
!------------------------------------------------------------------------------
! obsolete
NetWebServerWorker._LoadLookupRecord   PROCEDURE ()
LF         &FILE
LK         &KEY
err        Long
  CODE
  LF &= self._NetWebFileNamed(self.getvalue('lookupfile'))
  If Not LF &= NULL
    LK &= self._NetWebKeyNamed(LF,self.getvalue('lookupkey'))
    err = self._LoadRecord(LF,LK)
    if err = 0
      self.FileToSessionQueue(LF) ! only handles up to single dimensioned arrays
    end
  End
  Return LF
!------------------------------------------------------------------------------
NetWebServerWorker.ProcessGet PROCEDURE  ()
  CODE
!------------------------------------------------------------------------------
NetWebServerWorker.GetValue PROCEDURE  (string p_Name)
ReturnValue          like (NetWebServerLocalDataQueueType.Value)
RValue               &String
  CODE
  rValue &= Null
  self.GetValueError = 0
  clear (self._LocalDataQueue)
  self._LocalDataQueue.Name = lower(p_Name)
  get(self._LocalDataQueue, self._LocalDataQueue.Name)
  If ErrorCode() = 0
    If self._LocalDataQueue.ExtValue &= Null
      If self._LocalDataQueue.ValueFormatted = NET:FORMATTED and self._LocalDataQueue.Picture <> ''
        ReturnValue = self.dformat(self._LocalDataQueue.Value,self._LocalDataQueue.Picture)
      Else
        ReturnValue = self._LocalDataQueue.Value
      End
    Else
      RValue &= self._LocalDataQueue.ExtValue
    End
  Else
    self.GetValueError = 1 ! Not found
  End
  If rValue &= Null
    return clip(ReturnValue)
  Else
    return clip(rValue)
  End
!------------------------------------------------------------------------------
NetWebServerWorker.GetValueLength PROCEDURE  (string p_Name)
ReturnValue          Long
  CODE
  clear (self._LocalDataQueue)
  self._LocalDataQueue.Name = lower(p_Name)
  get(self._LocalDataQueue, self._LocalDataQueue.Name)
  If ErrorCode() = 0
    If self._LocalDataQueue.ExtValue &= Null
      ReturnValue = Len(Clip(self._LocalDataQueue.Value))
    Else
      returnValue = self._LocalDataQueue.ExtValueLen
    End
  End
  return ReturnValue
!------------------------------------------------------------------------------
NetWebServerWorker.RestoreValue  PROCEDURE  (string p_name,Long p_eval=1)
  Code
  If self.IfExistsValue(p_name)
    return(self.GetValue(p_name))
  ElsIf self.IfExistsSessionValue(p_name)
    return(self.GetSessionValue(p_name))
  elsif Band(p_eval,1)
    return(Evaluate(p_name))
  end
  Return 0
!------------------------------------------------------------------------------
NetWebServerWorker.StoreValue  PROCEDURE  (string p_name,<string p_valuename>)
loc:valuename  string(Net:ValueNameSize),auto
  Code
  if omitted(3)
    loc:valuename = p_name
  else
    loc:valuename = p_valuename
  end
  If self.IfExistsValue(loc:valuename)
    Self._CopyValueToSession('',p_name)
  End
  return
!------------------------------------------------------------------------------
NetWebServerWorker.GetValueFormat PROCEDURE  (string p_Name)
ReturnValue          like (NetWebServerLocalDataQueueType.Value)
rValue               &String
  CODE
  rValue &= Null
  self.GetValueError = 0
  clear (self._LocalDataQueue)
  self._LocalDataQueue.Name = lower(p_Name)
  get(self._LocalDataQueue, self._LocalDataQueue.Name)
  if ErrorCode() = 0
    if self._LocalDataQueue.Picture <> ''
      If self._LocalDataQueue.ExtValue &= Null
        If self._LocalDataQueue.ValueFormatted = NET:FORMATTED
          ReturnValue = self._LocalDataQueue.Value
        Else
          ReturnValue = format (self._LocalDataQueue.Value, self._LocalDataQueue.Picture)
        End
      Else
        ReturnValue = format (self._LocalDataQueue.ExtValue, self._LocalDataQueue.Picture)
      End
    else
      If self._LocalDataQueue.ExtValue &= Null
        ReturnValue = self._LocalDataQueue.Value
      Else
        RValue &= self._LocalDataQueue.ExtValue
      End
    end
  else
    self.GetValueError = 1 ! Not found
  end
  If rValue &= Null
    return clip(ReturnValue)
  Else
    return clip(rValue)
  End

!------------------------------------------------------------------------------

!NetWebServerWorker.GetValueName PROCEDURE  (string p_Value)
!! Returns the first matching name, that has the same value as p_Value
!
!ReturnValue          like (NetWebServerLocalDataQueueType.Name)
!  CODE
!  self.GetValueError = 0
!  clear (self._LocalDataQueue)
!  self._LocalDataQueue.Value = lower(p_Value)
!  get(self._LocalDataQueue, self._LocalDataQueue.Value)
!  if ErrorCode() = 0
!    ReturnValue = self._LocalDataQueue.Name
!  else
!    self.GetValueError = 1 ! Not found
!  end
!  return clip(ReturnValue)

!------------------------------------------------------------------------------

NetWebServerWorker.GetPicture PROCEDURE  (string p_Name)
ReturnValue          like (NetWebServerLocalDataQueueType.Picture)
  CODE
  self.GetValueError = 0
  clear (self._LocalDataQueue)
  self._LocalDataQueue.Name = lower(p_Name)
  get(self._LocalDataQueue, self._LocalDataQueue.Name)
  if ErrorCode() = 0
    ReturnValue = self._LocalDataQueue.Picture
  else
    self.GetValueError = 1 ! Not found
  end
  return clip(ReturnValue)
!------------------------------------------------------------------------------
NetWebServerWorker.SetValue PROCEDURE  (string p_Name, <string p_Value>, <string p_Picture>,long p_formatted=0,long p_overWrite=1) ! Declare Procedure 3
returnValue          long
loc:picture          string(32)
loc:name             string(Net:ValueNameSize),Auto
loc:value            string(Net:ValueSize),Auto
loc:len              Long
x                    Long,Auto
onlyname             long
max                  LONG
index                long
  CODE
  if not omitted(4)
    loc:picture = p_picture
  End

  if omitted(3)
    x = instring('=',p_name,1,1)
    if x = 0
      loc:name = lower(p_Name)
      loc:value = ''
    else
      loc:name = lower(sub(p_name,1,x-1))
      loc:value = sub(p_name,x+1,len(p_name))
    End
  Else
    loc:name = lower(p_Name)
    loc:Value = p_Value
  End
  if loc:name = '_event_'
    self.PushEvent(loc:value)
  end

  if right(loc:name,1) = ']'
    onlyname = instring('[',loc:name,1,1)
    if onlyname
      max = self.GetValue(sub(loc:name,1,onlyname-1) & '_maximum_')
      if right(loc:name,2) = '[]'
        max += 1
        Self.SetValue(sub(loc:name,1,onlyname-1)& '_maximum_',max)
        loc:name = sub(loc:name,1,onlyname-1) & '[' & max & ']'
      else
        index = sub(loc:name,onlyname+1,6)
        if index >  max
          Self.SetValue(sub(loc:name,1,onlyname-1)& '_maximum_',index)
        end
      end
    end
  end
  returnValue = 0        ! No error
  clear (self._LocalDataQueue)
  self._LocalDataQueue.Name = loc:name
  get(self._LocalDataQueue, self._LocalDataQueue.Name)
  If ErrorCode() = 0
    If self._LocalDataQueue.Value = ';|;multiple;|;'
      self._LocalDataQueue.Picture += 1
      put(self._LocalDataQueue)
      ReturnValue = self.SetValue(clip(loc:name)&self._LocalDataQueue.Picture,loc:value,loc:picture)
    Else
      if p_overWrite or loc:value <> ''
        If not omitted(4)
          self._LocalDataQueue.Picture = loc:picture
        End
        self._LocalDataQueue.ValueFormatted = p_formatted
        Do LoadRecord
        Put(self._LocalDataQueue)
        !self._trace('Putting Value ' & clip(self._LocalDataQueue.Name) & ' = ' & self._LocalDataQueue.ValueFormatted)
        ReturnValue = errorcode()
      else
        ReturnValue = 0
      End
    End
  Else
    self._LocalDataQueue.Name = loc:name
    self._LocalDataQueue.Picture = loc:picture
    self._LocalDataQueue.ValueFormatted = p_formatted
    do LoadRecord
    add(self._LocalDataQueue, +self._LocalDataQueue.Name)
    returnValue = errorcode()
    !self._trace('Adding Value ' & clip(self._LocalDataQueue.Name) & ' = ' & self._LocalDataQueue.ValueFormatted & ' :: ' & self._LocalDataQueue.Value)
  End
  Return returnValue

LoadRecord  Routine
  if omitted(3)
    self._LocalDataQueue.Value = loc:value
  else
    loc:len = len(p_value)
    If loc:len <= size(self._LocalDataQueue.Value) and self._LocalDataQueue.ExtValue &= Null
      self._LocalDataQueue.Value = p_value
    Else
      if loc:len > self._LocalDataQueue.ExtValueSize
        dispose(self._LocalDataQueue.ExtValue)
        self._LocalDataQueue.ExtValue &= New(String(loc:len))
        self._LocalDataQueue.ExtValueSize = loc:len
      End
      self._LocalDataQueue.ExtValue = p_Value
      self._LocalDataQueue.ExtValueLen = loc:len
    End
  End

!------------------------------------------------------------------------------

NetWebServerWorker.SetPicture PROCEDURE  (string p_Name, string p_Picture) ! Declare Procedure 3
returnValue              long
  CODE
  clear (self._LocalDataQueue)
  self._LocalDataQueue.Name = lower(p_Name)
  get(self._LocalDataQueue, self._LocalDataQueue.Name)
  if errorCode() = 0
    self._LocalDataQueue.Picture = p_Picture
    put(self._LocalDataQueue)
    returnValue = errorcode()
  else
    clear(self._LocalDataQueue)
    self._LocalDataQueue.Name = lower(p_Name)
    self._LocalDataQueue.Picture = p_Picture
    add(self._LocalDataQueue, +self._LocalDataQueue.Name)
    returnValue = errorcode()
  end
  return returnValue

!------------------------------------------------------------------------------
! be aware that this can happen before, or after the delete. The only property valid here is
! self.sessionid. Do NOT write code in here that accesses the session queue, or any other
! built-in queue based on the session id.
NetWebServerWorker.NotifyDeleteSession PROCEDURE  ()
  CODE

!------------------------------------------------------------------------------
NetWebServerWorker.DeleteSession PROCEDURE  ()
  CODE
  self.ClearBrowse()
  if self.RequestData.WebServer._DeleteSession(self.SessionID).
!------------------------------------------------------------------------------
NetWebServerWorker.NewSession PROCEDURE  ()
referer stringTheory
  CODE
  referer.SetValue(self._GetHeaderField('Referer:', self.RequestData.DataString, 1, self.RequestData.DataStringLen, 1, 1)) ! don't use pre-existing property because that has been already adjusted to match the default page.
  self.SessionID = self.RequestData.WebServer._NewSession(self.SessionID,self.site.SessionExpiryAfterHS,referer.GetValue(),self.site.SessionLength)
  If self.RequestData.WebServer.PreserveSessionId
    self.SetCookie('SessionID',self.SessionID,self.RequestData.WebServer.PreserveSessionId,,,,,true)
  Else
    self.SetCookie('SessionID',self.SessionID,,,,,,true)
  End
  self.SetSessionIP(self.requestdata.fromip)
  return (self.SessionID)
!------------------------------------------------------------------------------
NetWebServerWorker.DeleteSessionValue PROCEDURE  (string p_Name)
  CODE
  self.RequestData.WebServer._DeleteSessionValue (self.SessionID, p_Name, self.GetValueError)
!------------------------------------------------------------------------------
NetWebServerWorker.GetSessionValue PROCEDURE  (string p_Name)
  CODE
  return (self.RequestData.WebServer._GetSessionValue (self.SessionID, p_Name, self.GetValueError))
!------------------------------------------------------------------------------
NetWebServerWorker.GetSessionValueLength PROCEDURE  (string p_Name)
  CODE
  return (self.RequestData.WebServer._GetSessionValueLength (self.SessionID, p_Name))
!------------------------------------------------------------------------------
NetWebServerWorker.GSV             PROCEDURE  (string p_Name)
  CODE
  return (self.GetSessionValue (p_Name))
!------------------------------------------------------------------------------
NetWebServerWorker.RestoreSessionValues PROCEDURE  (string p_Prefix)
  CODE
  return self.RequestData.WebServer._RestoreSessionValues (self.SessionID,p_Prefix)
!------------------------------------------------------------------------------
NetWebServerWorker.GetSessionValueFormat PROCEDURE  (string p_Name)
  CODE
  return (self.RequestData.WebServer._GetSessionValueFormat (self.SessionID, p_Name, self.GetValueError))
!------------------------------------------------------------------------------
NetWebServerWorker.GetSessionPicture PROCEDURE  (string p_Name)
  CODE
  return (self.RequestData.WebServer._GetSessionPicture (self.SessionID, p_Name, self.GetValueError))
!------------------------------------------------------------------------------
NetWebServerWorker.SetSessionIP           PROCEDURE (String p_IP)
  CODE
  return (self.RequestData.WebServer._SetSessionIP (self.SessionID, p_IP))
!------------------------------------------------------------------------------
NetWebServerWorker.SetSessionReferer     PROCEDURE (String p_Referer)
  CODE
  return (self.RequestData.WebServer._SetSessionReferer (self.SessionID, p_Referer))
!------------------------------------------------------------------------------
NetWebServerWorker.SetSessionValue PROCEDURE  (string p_Name, string p_Value)
  CODE
  return (self.RequestData.WebServer._SetSessionValue (self.SessionID, p_Name, p_Value))
!------------------------------------------------------------------------------
NetWebServerWorker.SetSessionValue PROCEDURE  (string p_Name, string p_Value, string p_Picture)
  CODE
  return (self.RequestData.WebServer._SetSessionValue (self.SessionID, p_Name, p_Value, p_Picture))
!------------------------------------------------------------------------------
NetWebServerWorker.SSV PROCEDURE  (string p_Name, string p_Value)
  CODE
  return (self.SetSessionValue (p_Name,p_Value))
!------------------------------------------------------------------------------
NetWebServerWorker.SSV PROCEDURE  (string p_Name, string p_Value, string p_Picture)
  CODE
  return (self.SetSessionValue (p_Name, p_Value, p_Picture))
!------------------------------------------------------------------------------
NetWebServerWorker.SetSessionPicture PROCEDURE  (string p_Name, string p_Picture)
  CODE
  return (self.RequestData.WebServer._SetSessionPicture (self.SessionID, p_Name, p_Picture))
!------------------------------------------------------------------------------
NetWebServerWorker.GetSessionLoggedIn PROCEDURE  ()
  CODE
  return (self.RequestData.WebServer._GetSessionLoggedIn (self.SessionID))
!------------------------------------------------------------------------------
NetWebServerWorker.GetSessionLevel PROCEDURE  ()
  CODE
  return (self.RequestData.WebServer._GetSessionLevel (self.SessionID))
!------------------------------------------------------------------------------
NetWebServerWorker.GetSessionIP PROCEDURE  ()
  CODE
  return (self.RequestData.WebServer._GetSessionIP (self.SessionID))
!------------------------------------------------------------------------------
NetWebServerWorker.GetSessionReferer PROCEDURE  ()
  CODE
  return (self.RequestData.WebServer._GetSessionReferer (self.SessionID))
!------------------------------------------------------------------------------
NetWebServerWorker.TouchSession PROCEDURE  ()
x  long
  CODE
  x = (self.RequestData.WebServer._TouchSession (self.SessionID))
  If x
    self.NewSession()
    self.SetSessionIP(self.requestdata.fromip)
  End
  Return x
!------------------------------------------------------------------------------
NetWebServerWorker.MakeErrorPacket        PROCEDURE (long p_ErrorNumber, string p_ErrorString, string p_ErrorDescription, Long p_DefaultHeader=1)
returnValue  StringTheory
  CODE
  returnValue.SetValue(self.RequestData.WebServer._MakeErrorPacket(p_ErrorNumber,p_ErrorString,p_ErrorDescription,self.Ajax,p_DefaultHeader))
  if p_DefaultHeader = 0
    returnValue.Prepend(self.CreateHeader(self.HeaderDetails))
  End
  return ReturnValue.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker._GetHeaderField        PROCEDURE (string p_SearchString,*string p_MainString,long p_StartPos,long p_EndPos,long p_Offset,long p_CaseInSensitive)
  CODE
  Return self.RequestData.WebServer._GetHeaderField(p_SearchString, p_MainString,p_StartPos,p_EndPos,p_OffSet,p_CaseInsensitive)
!------------------------------------------------------------------------------
NetWebServerWorker.GetHeaderField        PROCEDURE (string p_SearchString)
  CODE
  Return self.RequestData.WebServer._GetHeaderField(p_SearchString, self.requestdata.datastring,1,len(clip(self.requestdata.DataString)),1,1)

!------------------------------------------------------------------------------
NetWebServerWorker.SetSessionLoggedIn PROCEDURE  (lONG p_Value)
Ans        String(Net:MaxSessionLength)
deleteNow  long
  CODE
  Ans = self.RequestData.WebServer._SetSessionLoggedIn (self.SessionID, p_Value,self.site.ChangeSessionOnLogInOut,self.site.DeleteSessionOnLogout,DeleteNow)
  If Ans <> ''
    if Deletenow ! triggered by "delete session on logout"
      self.DeleteSession()
    End
    If ans <> Self.SessionId
      self.SessionId = Ans ! session number changed.
      If self.RequestData.WebServer.PreserveSessionId
        self.SetCookie('SessionID',self.SessionID,self.RequestData.WebServer.PreserveSessionId,,,,,true)
      Else
        self.SetCookie('SessionID',self.SessionID,,,,,,true)
      End
    End
    self.LoginChanged(p_Value)
    return 0
  else
    return 1
  end
!------------------------------------------------------------------------------
NetWebServerWorker.SetSessionLevel PROCEDURE  (lONG p_Value)
Ans  Long
  CODE
  Ans = self.RequestData.WebServer._SetSessionLevel (self.SessionID, p_Value)
  Return Ans

!------------------------------------------------------------------------------
NetWebServerWorker.LoginChanged        PROCEDURE(Long p_ToWhat)
  CODE
  ! Pure Virtual.
!------------------------------------------------------------------------------
NetWebServerWorker.IfExistsSessionValue PROCEDURE  (string p_Name)
  CODE
  return (self.RequestData.WebServer._IfExistsSessionValue (self.SessionID, p_Name))
!------------------------------------------------------------------------------
NetWebServerWorker.ViewToSessionQueue  Procedure(View p_View,Long P_AlsoQueue=0)
F   &File
x   Long
  code
  Loop x = 1 to p_View{prop:files}
    F &= p_View{prop:file,x}
    self.FileToSessionQueue(F,P_AlsoQueue)
  End

!------------------------------------------------------------------------------
! only handles up to single dimensioned arrays, multi dimensioned arrays
! are placed in the session queue as [(x-1)*maximum(x)+y] not [x,y]
NetWebServerWorker.FileToSessionQueue PROCEDURE  (*File p_F,Long P_AlsoQueue=0,<string p_prefix>)
numfields         long
x                 long
y                 long
r                 &group
a                 any
f                 string (Net:ValueNameSize)
d                 Long
loc:prefix        string(5)
  CODE
    r &= p_F{prop:record}
    numfields = p_F{prop:fields}
    if not omitted(4)
      loc:prefix = p_prefix
    end
    loop x = 1 to numfields
      d = p_F{prop:dim,x}
      if p_F{prop:over,x} <> 0
        cycle
      end
      if d = 0
        a = What(r,x)
        f = clip(loc:prefix) & p_F{prop:label,x}
        if band(P_AlsoQueue,Net:NotSessionValueQueue) = 0
          self.SetSessionValue(f,a)
        End
        if band(P_AlsoQueue,Net:AlsoValueQueue) > 0
          self.SetValue(f,a,,net:raw)
        End
      else
        loop y = 1 to d
          a = What(r,x,y)
          f = clip(loc:prefix) & p_F{prop:label,x} & '[' & y & ']'
          if band(P_AlsoQueue,Net:NotSessionValueQueue) = 0
            self.SetSessionValue(f,a)
          end
          if band(P_AlsoQueue,Net:AlsoValueQueue) > 0
            self.SetValue(f,a,,net:raw)
          End
        end
      end
    end
    numfields = p_F{prop:memos}
    loop x = 1 to numfields
      f = clip(loc:prefix) & p_F{prop:label,-x}
      a = p_F{prop:value,-x}
      if band(P_AlsoQueue,Net:NotSessionValueQueue) = 0
        self.SetSessionValue(f,a)
      end
      if band(P_AlsoQueue,Net:AlsoValueQueue) > 0
        self.SetValue(f,a,,net:raw)
      End
    end
!------------------------------------------------------------------------------
NetWebServerWorker.SessionQueueToFile PROCEDURE  (*File p_File)
numfields         long
!prop:id           long
x                 long
y                 long
r                 &group
a                 any
f                 string (Net:ValueNameSize)
d                 long
  CODE
    r &= p_File{prop:record}
    numfields = p_File{prop:fields}
    loop x = 1 to numfields
      if p_File{prop:over,x} <> 0
        cycle
      end
      d = p_File{prop:dim,x}
      if d = 0
        f = p_File{prop:label,x}
        a &= What(r,x)
        a = self.GetSessionValue(f)
      else
        loop y = 1 to d
          f = p_File{prop:label,x} & '[' & y & ']'
          a &= What(r,x,y)
          a = self.GetSessionValue(f)
        end
      end
    end

    numfields = p_File{prop:memos}
    loop x = 1 to numfields
      f = p_File{prop:label,-x}
      p_File{prop:value,-x} = self.GetSessionValue(f)
    end
!------------------------------------------------------------------------------
NetWebServerWorker.GroupToSessionQueue  PROCEDURE (*Group p_g,Long p_AlsoQueue=0,<string p_prefix>)
an     Any
x      Long
fn     String (Net:ValueNameSize)
pfx    String(5)
  Code
  if not omitted(4)
    pfx = p_prefix
  end
  x = 1
  loop
    an &= What(p_g,x)
    If an &= NULL
      Break
    End
    fn = clip(pfx) & Clip(Who(p_g, x))
    if band(P_AlsoQueue,Net:NotSessionValueQueue) = 0
      self.SetSessionValue(fn,an)
    End
    if band(P_AlsoQueue,Net:AlsoValueQueue) > 0
      self.SetValue(fn,an,,net:raw)
    End
    x += 1
  end
  return

!------------------------------------------------------------------------------
NetWebServerWorker.SessionQueueToGroup  PROCEDURE (*Group p_g)
an     Any
x      Long
fn     String (Net:ValueNameSize)
  Code
  x = 1
  loop
    an &= What(p_g,x)
    If an &= NULL
      Break
    End
    fn = Clip(Who(p_g, x))
    an = self.GetSessionValue(fn)
    x += 1
  end
!------------------------------------------------------------------------------
NetWebServerWorker.FileType PROCEDURE  (string p_FileName) ! Declare Procedure 3
  CODE
  ! Return 1 if the file must be parse, otherwise return 0
  ! This method can be overriden
  if instring ('.htm',lower(p_FileName),1,1)  ! Parse .htm files
    return (1)
  end
  if instring ('.php',lower(p_FileName),1,1)  ! Parse .php files
    return (1)
  end
  return (0)

!------------------------------------------------------------------------------

NetWebServerWorker.ValidateFileName PROCEDURE  (string p_FileName) ! Declare Procedure 3
  CODE
  if clip(p_FileName) = ''
    return (-1) ! blank file name
  elsif instring('..', p_FileName, 1, 1)
    return (-2)  ! prevents hacking
  !elsif instring('.exe', lower(p_FileName), 1, 1) ! may possibly mean .exe files can not be served? probably a good thing.
  !  return (-2)  ! prevents hacking
  !elsif instring('.com', lower(p_FileName), 1, 1)
  !  return (-2)  ! prevents hacking
  elsif instring('.pif', lower(p_FileName), 1, 1)
    return (-2)  ! prevents hacking
  elsif instring('certificates', lower(p_FileName), 1, 1)
    return (-2)  ! prevents hacking
  end
  return (0)

!------------------------------------------------------------------------------
NetWebServerWorker.IfExistsValue PROCEDURE  (string p_Name)
returnValue           long,auto
  CODE
  self._LocalDataQueue.Name = lower(p_Name)
  get(self._LocalDataQueue, self._LocalDataQueue.Name)
  if ErrorCode() = 0
    returnValue = true
  else
    returnValue = false
  end
  return returnValue

!------------------------------------------------------------------------------
NetWebServerWorker.DeleteValue PROCEDURE  (string p_Name)
returnValue           long
  CODE
  clear(self._LocalDataQueue)
  self._LocalDataQueue.Name = lower(p_Name)
  get(self._LocalDataQueue, self._LocalDataQueue.Name)
  if ErrorCode() = 0
    delete(self._LocalDataQueue)
  end

!------------------------------------------------------------------------------

NetWebServerWorker.CreateHeader PROCEDURE  (NetWebServerHeaderDetailsType p_HeaderDetails)
  CODE
  p_HeaderDetails.cookies = self._MakeCookies()
  return (self.RequestData.WebServer._CreateHeader(p_HeaderDetails,self._OkSent))
!------------------------------------------------------------------------------

NetWebServerWorker._WinError  PROCEDURE  (long p_ErrorNumber=0)
err       long
mess      cstring(256)
result    long
  CODE
  if p_ErrorNumber = 0
    err = OS_GetLastError() ! instead get the error from LastError()
  else
    err = p_ErrorNumber
  end
  result = OS_FormatMessage(1000h, 0, err, 0, mess, size(mess)-1, 0) ! result = number of TCHAR characters
  if result >= 2 and result <= size (mess)
    ! Strip off last CRLF if there is one.
    if mess [result-1 : result] = '<13,10>'
      mess [result-1] = '<0>'
    end
  end
  return(clip(mess) & ' (' & Err & ')')
!------------------------------------------------------------------------------
NetWebServerWorker.ReadFile PROCEDURE  (*string p_FileName, StringTheory p_Buffer, *long p_FileDate, *long p_FileTime, *Long p_Parse)
local                   group, pre(loc)
FileSize                  long
cFileName                 cstring(260)
CheckSize                 long
hFile                     long ! 20/7/2005 - was ulong
SizeHigh                  long ! 20/7/2005 - was ulong
result                    long
BytesRead                 long ! 20/7/2005 - was ulong
LastError                 long
                        end
st  stringtheory
  CODE
  ! Note: The method that calls this method, must Dispose p_BufferGroup !!
  if self.site.PreCompressed = 1 and self.RequestGzip = 1
    loc:cFileName = clip (p_FileName) & '.gz'
    If Exists(loc:cFileName) = true
      p_Parse = 0
      self.ReplyContentEncoding = 'gzip'
    Else
      loc:cFileName = clip (p_FileName)
    End
  Else
    loc:cFileName = clip (p_FileName)
  End
  if lower(st.FileNameOnly(loc:cFileName)) = 'favicon.ico'
    if not exists(p_FileName)
      loc:cFileName = 'defaultfavicon.ico'
    end
  End
  if instring ('\',loc:cFileName,1,1) = 0
    loc:cFileName = clip(self.site.WebFolderPath) & '\' & loc:cFileName
  end
  loc:hFile = OS_CreateFile(loc:cFileName, NET:OS:GENERIC_READ, NET:OS:FILE_SHARE_READ, 0, NET:OS:OPEN_EXISTING, NET:OS:FILE_ATTRIBUTE_NORMAL, 0)
  if loc:hFile = 0
    self._trace('Error opening file (doesn''t exist): ' & clip(loc:cFileName))
    return (404) ! Fail
  end
  if loc:hFile < 0
    self._trace('Error ['&loc:hfile & ':' & OS_GetLastError() &'] opening file: ' & clip(loc:cFileName))
    return (404) ! Fail
  end

  loc:FileSize = OS_GetFileSize (loc:hFile, loc:SizeHigh) ! Read up on function if filesize > 2^32 bytes
  if self.LoggingOn
    self.Log ('NetWebServerWorker.GetFile', 'File Size = ' & loc:FileSize)
  end

  self._GetFileTime (loc:hFile, p_FileDate, p_FileTime)

  if loc:FileSize = 0
    self._trace('Error opening file (size=0): ' & clip(loc:hFile))
    return (404) ! Fail
  end

  if Self.RequestMethodType <> NetWebServer_HEAD
    p_Buffer.SetLength(loc:FileSize)
    loc:result = OS_ReadFile(loc:hFile, p_Buffer.Value, p_Buffer.LengthA(), loc:BytesRead, 0)
    if loc:result = 0
      self._trace('Error reading file ' & clip(loc:hFile))
      return (404) ! Fail
    end
  end
  loc:result = OS_CloseHandle(loc:hFile)
  if loc:result = 0
    self._trace('Error 404 closing file: ' & clip(loc:hFile))
    ! Should never happen, but just in case.
    return (404) ! Fail
  end
  self.HeaderDetails.ContentLength = loc:filesize
  if self.ReplyContentEncoding = 'gzip'
    p_Buffer.gzipped = 1
  end
  return 0 ! Success


!------------------------------------------------------------------------------

NetWebServerWorker.WebLog PROCEDURE  ()
  CODE
  ! This virtual method exists so that you can build your own Web Log.
  ! For example you can use the following information
  ! self.SessionID
  ! self.RequestMethodType            ! set to NetWebServer_POST or NetWebServer_GET (See netall.inc)
  ! self.RequestReferer
  ! self.RequestAction
  ! self.UserURL          ! contains page name and relative path, no parameters
  ! self.RequestFileName  ! contains page name and full local path, no parameters
  ! self.PageName         ! contains page name with no path
  ! self.WholeURL         ! contains pagename and parameters (no path)
  ! self.RequestHost      ! contains the site name as used on the url.
!------------------------------------------------------------------------------
NetWebServerWorker.GetPageName PROCEDURE  (string p_FileName)
returnValue       string(512)
i                 long,auto
dp                long
  CODE
  loop i = 1 to len(clip(p_FileName))
    if p_FileName[i] = '\' or p_FileName[i] = '/'
      returnValue = sub(p_FileName,i+1,len(p_FileName))
      if i > 1
        if p_FileName[i-1] = '\' or p_FileName[i-1] = '/'
          dp = 1
        else
          dp = 0
        end
      end
    end
  end

  if dp
    returnValue = self.site.DefaultPage
  end
  if clip(returnValue) = '' then returnValue = p_FileName.

  loop i = 1 to len(clip(returnValue))
    if returnValue[i] = '?' or returnValue[i] = '!'
      returnValue = sub(returnValue,1,i-1)
      break
    end
  end

  if clip(returnValue) = '' then returnValue = p_FileName.

  return clip(returnValue)

!------------------------------------------------------------------------------
NetWebServer.ColorWeb  PROCEDURE  (String p_Color)
loc:clarionhex         string(8)
loc:webhex             string(7)
HEX                    &STRING
HexDigitsUp            STRING('0123456789ABCDEF')
HexDigitsLow           STRING('0123456789abcdef')
LongColor              LONG
BColor                 BYTE,DIM(4),OVER(LongColor)
  CODE
  if sub(p_color,1,1) = '#'
    loc:webhex = p_color
  elsif p_Color = color:none
    loc:webhex = ''
  else
    LongColor = p_Color
    HEX &= HexDigitsUp
    loc:clarionhex[1] = HEX [Bshift(BColor[4],-4) + 1]
    loc:clarionhex[2] = HEX [Band(BColor[4], 0FH) + 1]
    loc:clarionhex[3] = HEX [Bshift(BColor[3],-4) + 1]
    loc:clarionhex[4] = HEX [Band(BColor[3], 0FH) + 1]
    loc:clarionhex[5] = HEX [Bshift(BColor[2],-4) + 1]
    loc:clarionhex[6] = HEX [Band(BColor[2], 0FH) + 1]
    loc:clarionhex[7] = HEX [Bshift(BColor[1],-4) + 1]
    loc:clarionhex[8] = HEX [Band(BColor[1], 0FH) + 1]
    loc:webhex = '#' & loc:clarionhex[7:8] & loc:clarionhex [5:6] & loc:clarionhex [3:4]
  end
  !self._trace('ColorWeb called: ' & clip(p_color) & ' = ' & clip(loc:webhex))
  return clip(loc:webhex)

!------------------------------------------------------------------------------
NetWebServerWorker.ColorWeb PROCEDURE  (String p_Color)
  code
  return (self.RequestData.WebServer.ColorWeb(p_Color))

!------------------------------------------------------------------------------
NetWebServer.ColorClarion PROCEDURE  (String p_Color)
loc:webhex             string(7)
HexDigitsUp            STRING('0123456789ABCDEF')
ans                    Long
BColor                 BYTE,DIM(4),OVER(ans)
  CODE
  loc:webhex = upper(p_color)
  if loc:webhex[1] = '#'
    loc:webhex = sub(loc:webhex,2,6)
    bColor[1] = (instring(loc:webhex[1],HexDigitsUp,1,1)-1) * 16 + (instring(loc:webhex[2],HexDigitsUp,1,1)-1)
    bColor[2] = (instring(loc:webhex[3],HexDigitsUp,1,1)-1) * 16 + (instring(loc:webhex[4],HexDigitsUp,1,1)-1)
    bColor[3] = (instring(loc:webhex[5],HexDigitsUp,1,1)-1) * 16 + (instring(loc:webhex[6],HexDigitsUp,1,1)-1)
  else
    ans = p_color
  end
  !self._trace('ColorClarion: ' & clip(p_color) & ' = ' & ans)
  return clip(ans)

!------------------------------------------------------------------------------
NetWebServerWorker.ColorClarion PROCEDURE  (String p_Color)
  code
  return (self.RequestData.WebServer.ColorClarion(p_Color))
!------------------------------------------------------------------------------
NetWebServerWorker._SetMobile PROCEDURE  ()
  CODE
  if self.IfExistsValue('_mobile_')
    self.StoreValue('_mobile_')
    self.mobile = self.GetSessionValue('_mobile_')
  elsIf self.IfExistsSessionValue('_mobile_')
    self.mobile = self.GetSessionValue('_mobile_')
  else
    self.SetMobileBasedOnAgent()
    self.SetMobileBasedOnSize()
  end
!------------------------------------------------------------------------------
NetWebServerWorker.SetMobileBasedOnSize PROCEDURE  ()
sw long
sh long
  code
  sw = self.GetSessionValue('_screenWidth_')
  sh = self.GetSessionValue('_screenHeight_')
  !self._trace('SetMobileBasedOnSize width=' & sw & 'height=' & sh & ' self.site.mobile.phonemax=' & self.site.mobile.phonemax)
  if self.site.mobile.phonemax and sw and sh and sw < self.site.mobile.phonemax and sh < self.site.mobile.phonemax
    self.mobile = 7 ! generic phone size
    self.SSV('_mobile_',self.mobile)
    !self._trace('SetMobileBasedOnSize mobile=' & self.mobile)
  elsif self.site.mobile.phonemax and sw > self.site.mobile.phonemax and sh > self.site.mobile.phonemax
    self.mobile = 0 ! generic tablet size
    self.SSV('_mobile_',self.mobile)
    !self._trace('SetMobileBasedOnSize mobile=' & self.mobile)
  end
  return
!------------------------------------------------------------------------------
NetWebServerWorker.SetMobileBasedOnAgent PROCEDURE  ()
ans  long
  code
  if instring('opera mobi',self._useragent,1,1)
    ans = 1
  elsif instring('iemobile ',self._useragent,1,1)
    ans = 2
  elsif instring('ipod ',self._useragent,1,1)
    ans = 4
  elsif instring('iphone ',self._useragent,1,1)
    ans = 3
  elsif instring('android ',self._useragent,1,1)
    ans = 5
  elsif instring('blackberry ',self._useragent,1,1)
    ans = 6
  else
    ans = 0
  end
  self.mobile = ans

!------------------------------------------------------------------------------
NetWebServerWorker._SetUserAgent PROCEDURE  ()
  CODE
  self.spider = 0
  self._useragent = lower(self._GetHeaderField ('User-Agent:', self.requestdata.DataString, 1, len(clip(self.requestdata.DataString)), 1, 1))
  if instring('msnbot',lower(self._useragent),1,1)
    self.useragent = 'msnbot'
    self.br = '<br></br>' & self.CRLF
    self.spider = 1
  elsif instring('yahoo! slurp',lower(self._useragent),1,1)
    self.useragent = 'slurp'
    self.br = '<br></br>' & self.CRLF
    self.spider = 1
  elsif instring('baiduspider',lower(self._useragent),1,1)
    self.useragent = 'Baiduspider'
    self.br = '<br></br>' & self.CRLF
    self.spider = 1
  elsif instring('ia_archiver',lower(self._useragent),1,1)
    self.useragent = 'ia_archiver'
    self.br = '<br></br>' & self.CRLF
    self.spider = 1
  elsif instring('googlebot',lower(self._useragent),1,1)
    self.useragent = 'googlebot'
    self.br = '<br></br>' & self.CRLF
    self.spider = 1

  elsif instring('curl',lower(self._useragent),1,1)  ! linux command line page fetcher
    self.useragent = 'curl'
    self.br = '<br></br>' & self.CRLF
    self.nocookies = 1

  elsif instring('opera',lower(self._useragent),1,1)
    self.useragent = 'opera'
    self.br = '<br />' & self.CRLF
  elsif instring('msie 10',lower(self._useragent),1,1)
    self.useragent = 'msie10'
    self.br = '<br />' & self.CRLF
  elsif instring('msie 9',lower(self._useragent),1,1)
    self.useragent = 'msie9'
    self.br = '<br />' & self.CRLF
  elsif instring('msie 8',lower(self._useragent),1,1)
    self.useragent = 'msie8'
    self.br = '<br />' & self.CRLF
  elsif instring('msie 7',lower(self._useragent),1,1)
    self.useragent = 'msie7'
    self.br = '<br />' & self.CRLF
  elsif instring('msie 6',lower(self._useragent),1,1)
    self.useragent = 'msie'
    self.br = '<br />' & self.CRLF
  elsif instring('msie',lower(self._useragent),1,1)
    self.useragent = 'msie10'
    self.br = '<br />' & self.CRLF
  elsif instring('firefox',lower(self._useragent),1,1)
    self.useragent = 'firefox'
    self.br = '<br />' & self.CRLF
  elsif instring('chrome',lower(self._useragent),1,1)
    self.useragent = 'chrome'
    self.br = '<br />' & self.CRLF
  elsif instring('safari',lower(self._useragent),1,1)
    self.useragent = 'safari'
    self.br = '<br></br>' & self.CRLF
  elsif instring('mozilla',lower(self._useragent),1,1)
    self.useragent = 'mozilla'
    self.br = '<br></br>' & self.CRLF
  elsif instring('the clarion handy tools',lower(self._useragent),1,1)
    self.useragent = 'cwhandy'
    self.br = '<br></br>' & self.CRLF
    self.spider = 0
  elsif instring('tinycurl',lower(self._useragent),1,1)
    self.useragent = 'tinycurl'
    self.br = '<br></br>' & self.CRLF
  else
    self.useragent = 'other'
    self.br = '<br />' & self.CRLF
  end
!------------------------------------------------------------------------------
NetWebServerWorker.FreeUseMap PROCEDURE  ()
  code
  free(self.UseMapQueue)
!------------------------------------------------------------------------------
NetWebServerWorker.AddArea PROCEDURE  (String p_Shape, String p_Coords, String p_Href, <String p_Target>, <String p_Alt> )
  code
  clear(self.UseMapQueue)
  self.UseMapQueue.Shape = p_Shape
  self.UseMapQueue.Href = p_Href
  self.UseMapQueue.Coords = p_Coords
  if not omitted(5) then self.UseMapQueue.Target = p_Target.
  if not omitted(6) then self.UseMapQueue.Alt = p_Alt.
  add(self.UseMapQueue)

!------------------------------------------------------------------------------
NetWebServerWorker.CreateUseMap PROCEDURE  (String p_Name)
returnValue   StringTheory
x             long
  code
  returnvalue.setvalue('<map name="'&clip(p_name)&'">' & self.CRLF)
  loop x = 1 to records(self.UseMapQueue)
    get(self.UseMapQueue,x)
    returnvalue.append('<area shape="'&clip(self.UseMapQueue.Shape)&'" coords="'&clip(self.UseMapQueue.Coords)&'" href="'&clip(self.UseMapQueue.Href)&'"')
    if self.UseMapQueue.Target then returnValue.append(' target="'&clip(self.UseMapQueue.Target)&'"').
    if self.UseMapQueue.Alt then returnValue.append(' alt="'&clip(self.UseMapQueue.Alt)&'"').
    returnvalue.append('/>' & self.CRLF)
  end
  returnvalue.append('</map>' & self.CRLF)
  return ReturnValue.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker.CreateImage PROCEDURE  (STRING p_Src, Long p_Width=-1, Long p_Height=-1, <STRING p_Alt>, <STRING p_URL>, <STRING p_Target>, <String p_Script>,<String p_tip>,Long p_Email=0,<String p_class>,<string p_UseMap>)
returnValue           string(Net:MaxBinData),Auto
loc:src               string(256)
loc:width             string(12)
loc:height            string(12)
loc:alt               string(256)
loc:script            string(1024)
loc:usemap            string(256)
loc:tip               string(Net:MaxBinData)
loc:class             string(StyleStringSize)
  CODE
  if p_src = ''
    return ''
  end
  loc:src = self._MakeURL(self.EncodeWebString(p_src,net:bigplus+net:dos+net:NoColon))
  if not omitted(3)
    if p_width > 0
      loc:width = p_width
    end
  end
  if not omitted(4)
    if p_height > 0
      loc:height = p_height
    end
  end
  if not omitted(5)
    if p_alt <> ''
      loc:alt = self._jsok(p_alt)
    end
  end

  if not omitted(8)
    loc:script = p_script
  end

  if not omitted(9)
    loc:tip = self.CreateTip(p_tip)
  end

  if not omitted(11)
    loc:class = self.wrap('class',p_class)
  end

  if not omitted(12)
    loc:usemap = self.wrap('usemap',p_UseMap)
  end

  if loc:height and not loc:width then loc:width = loc:height.
  if loc:width and not loc:height then loc:height = loc:width.

  case lower(self.UserAgent)
  of 'msie'
  orof 'msie7'
  orof 'msie8'
  orof 'msie9'
  orof 'msie10'
    if loc:Width = '' ! Internet Explorer does not like empty with attribute
      returnValue = '<img src="'&clip(loc:Src)&'" alt="'&clip(loc:Alt)&'" border="0" align="middle"' & clip(loc:tip)&' '&clip(loc:script)&' '&clip(loc:class)&' '&clip(loc:usemap)&' />'
    else
      returnValue = '<img src="'&clip(loc:Src)&'" width="'&clip(loc:Width)&'" height="'&clip(loc:Height)&'" alt="'&clip(loc:Alt)&'" border="0" align="middle"' & clip(loc:tip)&' '&clip(loc:script)&' '&clip(loc:class)&' '&clip(loc:usemap)&' />'
    end
  of 'firefox'
  orof 'opera'
  orof 'safari'
  orof 'chrome'
  orof 'mozilla'
      returnValue = '<img src="'&clip(loc:Src)&'" width="'&clip(loc:Width)&'" height="'&clip(loc:Height)&'" alt="'&clip(loc:Alt)&'" border="0" align="absmiddle"' & clip(loc:tip)&' '&clip(loc:script)&' '&clip(loc:class)&' />'
  else
      returnValue = '<img src="'&clip(loc:Src)&'" width="'&clip(loc:Width)&'" height="'&clip(loc:Height)&'" alt="'&clip(loc:Alt)&'" border="0" align="middle"' & clip(loc:tip)&' '&clip(loc:script)&' '&clip(loc:class)&' />'
  end
  returnValue = self.CreateHyperLink(returnValue,p_url,p_target,,,p_tip,p_EMail,Net:HtmlOk + Net:UnsafeHtmlOk)
  return clip(returnValue)

!------------------------------------------------------------------------------
NetWebServerWorker.CreateMailURL     PROCEDURE(String p_To,<String p_Subject>,<String p_CC>,<String p_BCC>,<String p_Body>,Long p_Encode=1)
ReturnValue String(Net:MaxBinData),Auto
loc:to      String(10)
  Code
  if lower(sub(p_to,1,7)) <> 'mailto:'
    loc:to = 'mailto:'
  end
  returnValue =  clip(loc:to) & clip(p_to)
  if not omitted(3)
    returnValue = clip(returnValue) & '?Subject=' & clip(p_subject)
  End
  if not omitted(4)
    returnValue = clip(returnValue) & '?cc=' & clip(p_cc)
  End
  if not omitted(5)
    returnValue = clip(returnValue) & '?bcc=' & clip(p_bcc)
  End
  if not omitted(6)
    returnValue = clip(returnValue) & '?body=' & clip(p_body)
  End
  returnValue = self._jsok(self._MakeURL(returnValue))
  return clip(ReturnValue)

!------------------------------------------------------------------------------
NetWebServerWorker.CreateHyperLink  PROCEDURE(String p_text,<String p_Url>,<String p_Target>,<String p_class>,<String p_Script>,<String p_tip>,Long p_Email=0,long p_html=0,long p_span=1,<string p_datado>)
ReturnValue String(Net:MaxBinData),Auto
loc:Url     String(256)
loc:target  String(256)
loc:script  string(1024)
loc:tip     String(Net:MaxBinData)
loc:class   String(StyleStringSize)
loc:datado  String(256)
  Code
  if not omitted(3) and p_url <> ''
    loc:url = p_Url
  end

  If not omitted(4) and p_Email = 0 and p_target <> ''
    loc:target = p_target
  End

  if not omitted(6) and p_script <> ''
    loc:script = ' ' & p_script
  end

  If not omitted(5) and p_class <> ''
    if instring('onclick="',loc:script,1,1)
      loc:class = self.wrap('class','nt-fakeget ' & p_class)
    else
      loc:class = self.wrap('class',p_class)
    end
  else
    if instring('onclick="',loc:script,1,1)
      !loc:url = '' ! 7.12 ! removed in 7.13 - progress controls have both a URL and a script with "onclick".
      loc:class = self.wrap('class','nt-fakeget')
    end
  end
  if not omitted(7) and p_tip <> ''
    loc:tip = self.CreateTip(p_tip)
  end
  if not omitted(11) and p_datado <> ''
    loc:datado = self.wrap('data-do',p_datado)
  end
  If p_Email
    if lower(sub(loc:url,1,7)) <> 'mailto:'
      loc:url = 'mailto:' & clip(loc:url)
    end
  ElsIf lower(sub(loc:url,1,4)) = 'www.'
    loc:url = 'http://' & loc:url
  ElsIf lower(sub(loc:url,1,7)) = 'mailto:'
    p_Email = 1
  End

  If loc:url = ''
    if loc:class = '' and loc:tip = '' and loc:script = ''
      ReturnValue = self._jsok(p_text,p_html)
    Elsif p_span
      ReturnValue = '<span ' & clip(loc:class) & clip(loc:tip) & clip(loc:script) & clip(loc:datado) &'>' & self._jsok(p_text,p_html) & '</span>'
    Else
      ReturnValue = '<div ' & clip(loc:class) & clip(loc:tip) & clip(loc:script) & clip(loc:datado) &'>' & self._jsok(p_text,p_html) & '</div>'
    End
  Else
    If loc:target = ''
      returnValue = '<a ' & clip(loc:class) & ' href="' & self._jsok(self._MakeURL(loc:url))&'"' & clip(loc:tip)& clip(loc:script) & clip(loc:datado) &'>' & self._jsok(p_text,p_html) & '</a>'
    else
      if instring('<img',p_text,1,1) and instring('</img>',p_text,1,1)
        p_html = band(p_html,net:htmlok)
      end
      returnValue = '<a ' & clip(loc:class) & ' href="' & self._jsok(self._MakeURL(loc:url))&'" target="'&self._jsok(p_target)&'"' & clip(loc:tip)& clip(loc:script) & clip(loc:datado) &'>' & self._jsok(p_text,p_html) & '</a>'
    end
  End
  Return clip(ReturnValue)

!------------------------------------------------------------------------------
NetWebServerWorker.CreateSelect PROCEDURE  (string p_Name,<string p_Class>, <String p_Readonly>,<string p_size>,<string p_width>,Long p_Multiple=0,<String p_Javascript>,<String p_tip>,<String p_Extra>,<String p_Id>,<String p_datado>)
ReturnValue String(Net:MaxBinData),Auto
  CODE
  If p_multiple > 0
    ReturnValue  = '<input type="hidden" name="'&self._jsok(self._NoColon(p_name))&'" value=";|;multiple;|;"/>'!</input>'
  else
    ReturnValue = ''
  End
  ReturnValue  = clip(ReturnValue) & '<select name="' & self._jsok(self._NoColon(p_name)) & '" id="' & self._jsok(self._NoColon(p_name)) & '"'

  if not omitted(3) ! p_class
    returnValue = clip(ReturnValue) & self.wrap('class',p_class)
  end

  if not omitted(4) ! p_readonly
    if p_readonly
      returnValue = clip(ReturnValue) & self.wrap('disabled',p_readonly)
    end
  end

  if not omitted(5) ! p_size
    returnValue = clip(ReturnValue) & self.wrap('size',p_size)
  end
  if not omitted(6) ! p_width
    returnValue = clip(ReturnValue) & ' style="width: '&p_width&'px"'
  end
  if p_multiple > 0
    returnValue = clip(ReturnValue) & ' multiple="multiple"'
  end
  if not omitted(8) ! p_javascript
    returnValue = clip(ReturnValue) & ' ' & clip(p_JavaScript)
  end
  if not omitted(9)
    returnValue = clip(ReturnValue) & self.CreateTip(p_tip)
  end
  if not omitted(10)
    returnValue = clip(ReturnValue) & ' ' & clip(p_extra)
  end
  If not omitted(12) and p_DataDo <> ''
    returnValue = clip(ReturnValue) & self.wrap('data-do',p_datado)
  end

  returnValue = clip(ReturnValue) &' >' & self.CRLF
  return ReturnValue

!------------------------------------------------------------------------------
NetWebServerWorker.CreateDateInput PROCEDURE  (string p_Id,string p_Value,<string p_class>,long p_Readonly=0,long p_disabled=0,<string p_picture>,<string p_Javascript>,<string p_Options>,<string p_extra>,<String p_tip>,Long p_size=0,Long p_MinDate=0,Long p_MaxDate=0,<String p_placeholder>,Long p_ForceLookup=0,<String p_Name>,<String p_datado>,<String p_LookupPicture>,Long pUnix=0)
returnValue           string(NET:MaxBinData)
x               long
loc:value       string(255)
loc:class       string(StyleStringSize)
loc:picture        string(100)
loc:lookuppicture  string(100)
loc:javascript  string(JavascriptStringLen)
options         StringTheory
loc:readonly    string(50)
loc:disabled    string(50)
loc:tip         string(Net:MaxBinData)
loc:extra       string(255)
loc:id          string(255)
loc:placeholder string(255)
loc:size        long
loc:randomid    string(4)
loc:name        string(252)
loc:imm         string(20)
  code

  if not omitted(4) and p_class <> ''
    loc:class = self.wrap('class',p_class)
  end
  if p_Readonly <> 0
    loc:class = clip(loc:class) & ' readonly="readonly"'
  end
  if p_disabled <> 0
    loc:class = clip(loc:class) & ' disabled="disabled"'
  end
  if not omitted(7) and p_picture <> ''
    loc:picture = p_picture
  else
    loc:picture = self.site.datepicture
  end
  x = instring('U',upper(loc:picture),1,1)
  if x
    pUnix = 1
    loc:picture[x] = ' '
  end
  if p_Value <> '' and p_value <> 0
    if pUnix then p_Value = self.UnixToClarionDate(p_Value).
    loc:value = self.wrap('value',self._jsok(left(self.mformat(p_Value,loc:picture))))
  end
  if not omitted(8) and p_Javascript <> ''
    loc:javascript = ' ' & p_Javascript
  end
  if not omitted(9) and p_options <> ''
    options.SetValue(self._jsok(p_options,Net:SingleQuoteOk))
  end
  if not omitted(18) and p_dataDo <> ''
    loc:imm = self.wrap('data-do',p_datado)
  end
  if not omitted(19) and p_LookupPicture <> ''
    loc:picture = p_LookupPicture
  end

  self.SetOption(options,'dateFormat',self.ClarionDatePictureToJQuery(loc:picture,loc:size),true)
  loc:size += 1 ! make it just a touch bigger to accomodate wider fonts.

  if p_size > loc:size then loc:size = p_size.

  if p_Mindate > 0
    self.SetOption(options,'minDate',p_minDate - today(),true)
  end
  if p_Maxdate > 0
    self.SetOption(options,'maxDate',p_MaxDate - today(),true)
  end

  if not omitted(10)
    loc:extra = ' ' & p_extra
  end
  if not omitted(11)
    loc:tip = self.CreateTip(p_tip)
  end

  if instring ('*',p_id,1,1) or instring ('&',p_id,1,1) or instring ('/',p_id,1,1) or instring ('\',p_id,1,1) or instring ('?',p_id,1,1) or instring ('@',p_id,1,1) or instring ('{{',p_id,1,1) or instring ('}',p_id,1,1) or instring ('<<',p_id,1,1) or instring ('>',p_id,1,1)
    loc:id = 'inp' & self.crc32(p_id)
  else
    loc:id = self._jsok(self._nocolon(p_id))
  end

  if not omitted(15) and p_placeholder <> ''
    loc:placeholder = ' placeholder="'&self.translate(p_placeholder)&'"'
  else
    loc:placeholder = ' placeholder="' & self.ClarionDatePicture(loc:picture) &'"'
  end
  if not omitted(17) and p_name <> 11
    loc:name = p_Name
  else
    loc:name = p_Id
  end
  ! don't want random id's on date fields, because form uses id when doing an ajax onchange.
  If p_Readonly <> 0 or self.IsMobile()
    returnValue = '<input type="text" name="'&clip(loc:name)&'" id="'&clip(loc:id)&clip(loc:randomid)&'" size="'&loc:size&'" '&clip(loc:value)& clip(loc:class) & clip(loc:javascript) & clip(loc:tip) & clip(loc:readonly) & clip(loc:disabled) & clip(loc:extra) & clip(loc:placeholder) & clip(loc:imm) & '/>'
  Else
    if p_ForceLookup then loc:readonly = ' readonly="readonly"'.
    returnValue = '<input type="text" name="'&clip(loc:name)&'" id="'&clip(loc:id)&clip(loc:randomid)&'" size="'&loc:size&'" '&clip(loc:value)& clip(loc:class) & clip(loc:javascript) & clip(loc:tip) & clip(loc:readonly) & clip(loc:disabled) & clip(loc:extra) & clip(loc:placeholder) & clip(loc:imm) & '/>'
    self.jQuery('#' & clip(loc:id)&loc:randomid,'datepicker',options)
  End
  Return clip(Returnvalue)
!------------------------------------------------------------------------------
NetWebServerWorker.ClarionDatePicture     PROCEDURE (String p_pic)
ReturnValue  String(50)
pic          long
sep          string(1)
  Code
  if p_pic = '' then return ''.
  if p_pic[1] = '@' then p_pic = sub(p_pic,2,10).
  if p_pic[1] = 'D' or p_pic[1] = 'd' then p_pic = sub(p_pic,2,10).
  if p_pic[1] = '0' then p_pic = sub(p_pic,2,10).
  if p_pic[len(clip(p_pic))] = 'B' or p_pic[len(clip(p_pic))] = 'b' then p_pic[len(clip(p_pic))] = ' '.
  pic = p_pic
  sep = sub(p_pic,len(clip(pic))+1,1)
  if sep = '' then sep = '/'.
  case pic
  of 1
    returnvalue = 'mm' & sep & 'dd' & sep & 'yy'
  of 2
    returnvalue = 'mm' & sep & 'dd' & sep & 'yyyy'
  of 3
    returnvalue = 'mmm dd,yyyy'
  of 4
    returnvalue = 'mmmmmm dd,yyyy'
  of 5
    returnvalue = 'dd' & sep & 'mm' & sep & 'yy'
  of 6
    returnvalue = 'dd' & sep & 'mm' & sep & 'yyyy'
  of 7
    returnvalue = 'dd mmm yy'
  of 8
    returnvalue = 'dd mmm yyyy'
  of 9
    returnvalue = 'yy' & sep & 'mm' & sep & 'dd'
  of 10
    returnvalue = 'yyyy' & sep & 'mm' & sep & 'dd'
  of 11
    returnvalue = 'yymmdd'
  of 12
    returnvalue = 'yyyymmdd'
  of 13
    returnvalue = 'mm' & sep & 'yy'
  of 14
    returnvalue = 'mm' & sep & 'yyyy'
  of 15
    returnvalue = 'yy' & sep & 'mm'
  of 16
    returnvalue = 'yyyy' & sep & 'mm'
  end
  Return clip(ReturnValue)
!------------------------------------------------------------------------------
NetWebServerWorker.ClarionDatePictureToJQuery PROCEDURE (string p_ClarionDatePic,*Long p_Width)
cd   string(20)
ans  string(20)
lz   long
n    long
s    string(1)
m    string(2)
d    string(2)
y    string(2)
b    long
  code
  cd = p_ClarionDatePic
  if cd[1] = '@' or cd[1] = 'd' or cd[1] = 'D' then cd[1] = ' '.
  if cd[2] = 'd' or cd[2] = 'D' then cd[2] = ' '.
  cd = left(cd)
  if cd[1] = '0' then lz = 1.
  if cd[len(clip(cd))] = 'b' or cd[len(clip(cd))] = 'B'
    b = 1
    cd[len(clip(cd))] = ' '
  end
  n = cd
  s = cd[len(clip(cd))]
  case s
  of '.'
  orof '-'
  of '_'
    s = ' '
  of '`'
    s = ','
  else
    case n
    of 3 orof 4 orof 7 orof 8
      s = ' '
    else
      s = '/'
    end
  end
  case n
  of 1
    if lz then ans = 'mm' & s & 'dd' & s & 'y' else ans = 'm' & s & 'dd' & s & 'y'.
    p_width = 8
  of 2
    if lz then ans = 'mm' & s & 'dd' & s & 'yy' else ans = 'm' & s & 'dd' & s & 'yy'.
    p_width = 10
  of 3
    if lz then ans = 'M dd, yy' else ans = 'M d, yy'.
    p_width = 11
  of 4
    if lz then ans = 'MM dd, yy' else ans = 'MM d, yy'.
    p_width = 18
  of 5
    if lz then ans = 'dd' & s & 'mm' & s & 'y' else ans = 'd' & s & 'mm' & s & 'y'.
    p_width = 8
  of 6
    if lz then ans = 'dd' & s & 'mm' & s & 'yy' else ans = 'd' & s & 'mm' & s & 'yy'.
    p_width = 10
  of 7
    if lz then ans = 'dd M y' else ans = 'd M y'.
    p_width = 9
  of 8
    if lz then ans = 'dd M yy' else ans = 'd M yy'.
    p_width = 11
  of 9
    ans = 'y' & s & 'mm' & s & 'dd'
    p_width = 8
  of 10
    ans = 'yy' & s & 'mm' & s & 'dd'
    p_width = 10
  of 11
    ans = 'ymmdd'
    p_width = 6
  of 12
    ans = 'yymmdd'
    p_width = 8
  of 13
    if lz then ans = 'mm' & s & 'y' else ans = 'm' & s & 'y'.
    p_width = 5
  of 14
    if lz then ans = 'mm' & s & 'yy' else ans = 'm' & s & 'yy'.
    p_width = 7
  of 15
    ans = 'y' & s & 'mm'
    p_width = 5
  of 16
    ans = 'yy' & s & 'mm'
    p_width = 7
  end
  p_width +=1 ! need an extra space to allow for font / width
  return clip(ans)
!------------------------------------------------------------------------------
NetWebServerWorker.CreateMedia            PROCEDURE (string p_id,String p_URL,<String p_text>,<string p_class>, <string p_tip>, <string p_extra>, <string p_javascript>)
returnValue           string(NET:MaxBinData)
loc:text              string(255)
  code
  if not omitted(4)
    loc:text = p_text
  end
  if not omitted(6)
    returnValue = clip(ReturnValue) & self.CreateTip(p_tip)
  end

  returnvalue = '<a id="'&self._jsok(self._nocolon(p_id))&'" href="'&clip(p_url)&'" '&clip(returnvalue)&'>'&self._jsok(loc:text)&'</a>'
  return clip(returnValue)
!------------------------------------------------------------------------------
NetWebServerWorker.CreateInput PROCEDURE  (string p_Type, string p_Name, string p_Value,<string p_class>,<string p_Status>, <string p_Extra>, <String p_Picture>, <String p_Javascript>, <Long p_MaxLength>,<String p_tip>,<String p_id>,<String p_placeholder>,<String p_datado>,<Long p_Min>,<Long p_Max>,<Long p_Step>)
returnValue           string(NET:MaxBinData),Auto
loc:name              string(256)
loc:id                string(256)
loc:picture           string(32)
loc:Length            Long
  CODE
  if not omitted(8) !(p_picture)
    loc:picture = p_picture
  end
  if instring ('*',p_name,1,1) or instring ('&',p_name,1,1) or instring ('/',p_name,1,1) or instring ('\',p_name,1,1) or instring ('?',p_name,1,1) or instring ('@',p_name,1,1) or instring ('{{',p_name,1,1) or instring ('}',p_name,1,1) or instring ('<<',p_name,1,1) or instring ('>',p_name,1,1)
    loc:name = 'inp' & self.crc32(p_name)
  else
    loc:name = self._jsok(self._nocolon(p_name))
  end
  if omitted(12)
    loc:id = self._jsok(self._nocolon(loc:name))
  else
    loc:id = self._jsok(self._nocolon(p_id))
  end
  if loc:picture and lower(p_type) <> 'number'
    returnValue = '<input type="'&p_Type&'" name="'&clip(loc:Name)&'" id="'&clip(loc:Id)&'" value="'&self._jsok(left(self.mformat(p_Value,loc:picture)))&'"'
  else
    returnValue = '<input type="'&p_Type&'" name="'&clip(loc:Name)&'" id="'&clip(loc:Id)&'" value="'&self._jsok(p_Value)&'"'
  end
  if not omitted(5) !p_class
    if p_class <> ''
      returnValue = clip(ReturnValue) & ' class="'&clip(p_Class)&'"'
    End
  end
  if not omitted(6) !p_status
    if instring('checked',lower(p_Status),1,1)
      returnValue = clip(ReturnValue) & ' checked="checked"'
    end
    if instring('disabled',lower(p_Status),1,1)
      returnValue = clip(ReturnValue) & ' disabled="disabled"'
    end
    if instring('readonly',lower(p_Status),1,1) or p_status = 1
      returnValue = clip(ReturnValue) & ' readonly="readonly"'
    end
  end
  if not omitted(7) !p_extra
    returnValue = clip(ReturnValue) & ' ' & clip(p_Extra)
  end
  if not omitted(9) !p_javascript
    returnValue = clip(ReturnValue) & ' ' & clip(p_JavaScript)
  end
  if not omitted(10) ! p_MaxLength
    returnValue = clip(ReturnValue) & ' maxlength="' & p_MaxLength & '"'
  else
    case lower(p_type)
    of 'text' orof 'file' orof 'password' orof 'email' orof 'url'
      if not omitted(7) !(p_picture)
        loc:length = self.PicLength(p_picture)
        if loc:length > 0
          returnValue = clip(ReturnValue) & ' maxlength="' & loc:length & '"'
        end
      end
    end
  end

  if not omitted(11)
    returnValue = clip(ReturnValue) & self.CreateTip(p_tip)
  end

  if not omitted(13)
    returnValue = clip(ReturnValue) & ' placeholder="' & self.Translate(p_placeholder) & '"'
  end

  if not omitted(14)
    returnValue = clip(ReturnValue) & self.wrap('data-do',p_datado)
  end

  if not omitted(15)
    returnValue = clip(ReturnValue) & ' min="' & p_min & '"'
  end

  if not omitted(16)
    returnValue = clip(ReturnValue) & ' max="' & p_max & '"'
  end

  if not omitted(17)
    returnValue = clip(ReturnValue) & ' step="' & p_step & '"'
  end

  !if lower(p_type) <> 'hidden' and lower(p_type) <> 'radio' and lower(p_type) <> 'checkbox'
  !  returnValue = clip(ReturnValue) & ' onkeypress="return oe(this,event);"'
  !end

  returnValue = clip(ReturnValue)&'/>'! </input>'
  if self.IsMobile()
    return '<div class="nt-left">' & clip(returnvalue) & '</div>'
  else
    return clip(returnValue)
  end
!------------------------------------------------------------------------------
NetWebServerWorker.CreateSlider   PROCEDURE (String p_id,Long p_ValueMin,<Long p_ValueMax>, String p_Class, <String p_Width>,<String p_ReadOnly>,<String p_Options>,<String p_Min>, <String p_Max>, <String p_Step>)
returnValue           string(NET:MaxBinData),Auto
options               StringTheory
width                 String(252)
  code
  if omitted(5) or p_width = ''! p_width
    width = '20em'
  else
    width = p_Width
  end
  returnValue = '<div style="width:'&clip(Width)&';"> <div style="padding: 5px !important;"><div id="'&self._jsok(self._nocolon(p_id))&'_slider"></div></div></div>'

  if not omitted(7) and p_options <> ''
    options.setValue(p_options,st:clip)
  end
  if not omitted(8) ! p_min
    self.SetOption(options,'min',clip(p_min),true)
  end
  if not omitted(9) ! p_max
    self.SetOption(options,'max',clip(p_max),true)
  end
  if not omitted(10) ! p_step
    self.SetOption(options,'step',clip(p_step),true)
  end
  If not omitted(6) and p_readonly <> ''
    self.SetOption(options,'disabled','true',true)
  end
  self.SetOption(options,'slide','function(event,ui){{$( "#'&self._jsok(self._nocolon(p_id))&'").val(ui.value);}')
  self.SetOption(options,'change','function(event,ui){{$("#'&self._jsok(self._nocolon(p_id))&'").change();}')
  if omitted(4)
    self.SetOption(options,'values','[' & p_valueMin &']')
  else
    if p_ValueMax < p_ValueMin
      self.SetOption(options,'values','[' & p_valueMax & ',' & p_ValueMin &']')
    else
      self.SetOption(options,'values','[' & p_valueMin & ',' & p_ValueMax &']')
    end
  end
  self.jQuery('#' & self._jsok(self._nocolon(p_id)) &'_slider','slider',options)
  return clip(returnvalue)

!------------------------------------------------------------------------------
NetWebServerWorker.CreateRadio PROCEDURE  (string p_Name, string p_id, string p_Value,String p_Label,<string p_class>,<string p_Status>, <string p_Extra>, <String p_Javascript>, <String p_tip>,<String p_optiontip>,<String p_datado>)
returnValue           string(NET:MaxBinData),Auto
loc:name              string(252)
loc:id                string(252)
loc:status            string(252)
loc:extra             string(252)
loc:javascript        string(JavascriptStringLen)
loc:class             string(StyleStringSize)
loc:Length            Long
loc:tip               string(Net:MaxBinData)
  CODE
  if instring ('*',p_name,1,1) or instring ('&',p_name,1,1) or instring ('/',p_name,1,1) or instring ('\',p_name,1,1) or instring ('?',p_name,1,1) or instring ('@',p_name,1,1) or instring ('{{',p_name,1,1) or instring ('}',p_name,1,1) or instring ('<<',p_name,1,1) or instring ('>',p_name,1,1)
    loc:name = 'inp' & self.crc32(p_name)
  else
    loc:name = self._jsok(self._nocolon(p_name))
  end
  loc:id = self._jsok(self._nocolon(p_id))

  if not omitted(6) and p_class <> ''
    loc:class = ' class="'&clip(p_Class)&'"'
  end

  if not omitted(7) !p_status
    if instring(' checked',' ' & lower(p_Status),1,1)
      loc:status = ' checked="checked"'
    end
    if instring(' disabled',' ' & lower(p_Status),1,1)
      loc:status = clip(loc:status) & ' disabled="disabled"'
    end
    if instring(' readonly',' ' & lower(p_Status),1,1) or p_status = 1
      loc:status = clip(loc:status) & ' readonly="readonly"'
    end
  end

  if not omitted(8) !p_extra
    loc:extra = ' ' & p_extra
  end

  if not omitted(9) !p_javascript
    loc:javascript = ' ' & clip(p_JavaScript)
  end

  returnValue = '<input type="radio" name="'&clip(loc:Name)&'" id="'&clip(loc:Id)&'" value="'&self._jsok(p_Value)&'"' & clip(loc:class) & clip(loc:status) & clip(loc:extra) & clip(loc:javascript)

  if not omitted(11) and p_optiontip <> ''
    loc:tip = self.CreateTip(p_optiontip)
  end
  if loc:tip = '' and not omitted(10) and p_tip <> '' ! p_tip
    loc:tip = self.CreateTip(p_tip)
  end

  if not omitted(12) and p_datado <> '' ! p_datado
    returnValue = clip(ReturnValue) & self.wrap('data-do',p_datado)
  end

  returnValue = clip(ReturnValue) & clip(loc:tip) &'/><label for="'&clip(loc:id)&'" '&clip(loc:tip)&'>'&self.translate(p_label)&'</label>'

  return clip(returnValue)
!------------------------------------------------------------------------------
NetWebServerWorker.CreateStdBrowseButton  PROCEDURE (Long p_Equate,String p_BrowseName,<String p_RecordID>,Long p_disabled=0,Long p_popup=0,<String p_FormProc>)
Btn      &NetWebButtonGroup
Id       String(1000)  ! parameters to pass to browseTable method
onClick  string(JavascriptStringLen)
loc:formproc  String(255)
loc:role       string(255)
  Code
  if omitted(4) = 0
    id = p_RecordID
  end
  if omitted(7) = 0
    loc:formproc = p_FormProc
  end
  Execute p_Equate
    Btn &= self.site.SaveButton
    Btn &= self.site.CancelButton
    Btn &= self.site.CloseButton
    Btn &= self.site.DeletebButton
    Btn &= self.site.SelectButton
    Btn &= self.site.InsertButton
    Btn &= self.site.ChangeButton
    Btn &= self.site.ViewButton
    Btn &= self.site.LocateButton
    Btn &= self.site.FirstButton
    Btn &= self.site.PreviousButton
    Btn &= self.site.NextButton
    Btn &= self.site.LastButton
    Btn &= self.site.PrintButton
    Btn &= self.site.StartButton
    Btn &= self.site.UploadButton
    Btn &= self.site.LookupButton
    Btn &= self.site.SmallDeleteButton
    Btn &= self.site.SmallSelectButton
    Btn &= self.site.SmallInsertButton
    Btn &= self.site.SmallChangeButton
    Btn &= self.site.SmallViewButton
    Btn &= self.site.DeletefButton
    Btn &= self.site.BrowseCancelButton
    Btn &= self.site.BrowseCloseButton
    Btn &= self.site.DateLookupButton
    Btn &= self.site.WizPreviousButton
    Btn &= self.site.WizNextButton
    Btn &= self.site.SmallOtherButton
    Btn &= self.site.SmallPrintButton
    Btn &= self.site.CopyButton
    Btn &= self.site.SmallCopyButton
    Btn &= self.site.ClearButton
    Btn &= self.site.LogoutButton
    Btn &= self.site.AddFileButton
    Btn &= self.site.ClearFileButton
    Btn &= self.site.StartFileButton
    Btn &= self.site.CancelFileButton
    Btn &= self.site.RemoveFileButton
  Else
    Return ''
  End
  case p_Equate
  of Net:Web:InsertButton
  orof Net:Web:SmallInsertButton
    loc:role = 'insert'

  of Net:Web:ChangeButton
  orof Net:Web:SmallChangeButton
    loc:role = 'change'

  of Net:Web:ViewButton
  orof Net:Web:SmallViewButton
    loc:role = 'view'

  of Net:Web:CopyButton
  orof Net:Web:SmallCopyButton
    loc:role = 'copy'

  of Net:Web:DeletebButton
  orof Net:Web:SmallDeleteButton
    loc:role = 'deleteb'

  of Net:Web:SelectButton
  orof Net:Web:SmallSelectButton
    loc:role = 'select'

  of Net:Web:BrowseCancelButton
    loc:role = 'browsecancel'

  of Net:Web:BrowseCloseButton
    loc:role = 'close'
!    if p_Popup
!      onclick = 'ntd.close();'
!    end

  of Net:Web:LookupButton
    loc:role = 'lookup'
  End
  Return Self.CreateButton('button',clip(btn.name),clip(btn.textvalue),clip(btn.class),,,,onclick,,p_disabled,clip(btn.image),btn.imagewidth,btn.imageheight,clip(btn.imagealt),clip(btn.Tooltip),btn.UseJavaScript,btn.JsIcon,,,,btn.Compact,btn.MobileIcon,btn.MobileText,loc:role)

!------------------------------------------------------------------------------
NetWebServerWorker.CreateStdButton        PROCEDURE (string p_type, Long p_Equate, <string p_form>, <string p_action>, <string p_target>,<string p_onClick>,<string p_Javascript>,Long p_Disabled=0,<String p_class>)
Btn &NetWebButtonGroup
DefaultButton  long
loc:class      string(StyleStringSize)
loc:role       string(255)
  Code
  if p_Equate = NET:WEB:SaveButton then DefaultButton = 1.
  Execute p_Equate
    Btn &= self.site.SaveButton
    Btn &= self.site.CancelButton
    Btn &= self.site.CloseButton
    Btn &= self.site.DeletebButton
    Btn &= self.site.SelectButton
    Btn &= self.site.InsertButton
    Btn &= self.site.ChangeButton
    Btn &= self.site.ViewButton
    Btn &= self.site.LocateButton
    Btn &= self.site.FirstButton
    Btn &= self.site.PreviousButton
    Btn &= self.site.NextButton
    Btn &= self.site.LastButton
    Btn &= self.site.PrintButton
    Btn &= self.site.StartButton
    Btn &= self.site.UploadButton
    Btn &= self.site.LookupButton
    Btn &= self.site.SmallDeleteButton
    Btn &= self.site.SmallSelectButton
    Btn &= self.site.SmallInsertButton
    Btn &= self.site.SmallChangeButton
    Btn &= self.site.SmallViewButton
    Btn &= self.site.DeletefButton
    Btn &= self.site.BrowseCancelButton
    Btn &= self.site.BrowseCloseButton
    Btn &= self.site.DateLookupButton
    Btn &= self.site.WizPreviousButton
    Btn &= self.site.WizNextButton
    Btn &= self.site.SmallOtherButton
    Btn &= self.site.SmallPrintButton
    Btn &= self.site.CopyButton
    Btn &= self.site.SmallCopyButton
    Btn &= self.site.ClearButton
    Btn &= self.site.LogoutButton
    Btn &= self.site.AddFileButton
    Btn &= self.site.ClearFileButton
    Btn &= self.site.StartFileButton
    Btn &= self.site.CancelFileButton
    Btn &= self.site.RemoveFileButton
  Else
    Return ''
  End
  Execute p_Equate
    loc:role = 'save' ! self.site.SaveButton
    loc:role = 'cancel' ! self.site.CancelButton
    loc:role = '' ! self.site.CloseButton
    loc:role = '' ! self.site.DeletebButton
    loc:role = '' ! self.site.SelectButton
    loc:role = '' ! self.site.InsertButton
    loc:role = '' ! self.site.ChangeButton
    loc:role = '' ! self.site.ViewButton
    loc:role = '' ! self.site.LocateButton
    loc:role = 'first' ! self.site.FirstButton
    loc:role = 'previous' ! self.site.PreviousButton
    loc:role = 'next' ! self.site.NextButton
    loc:role = 'last' ! self.site.LastButton
    loc:role = '' ! self.site.PrintButton
    loc:role = '' ! self.site.StartButton
    loc:role = '' ! self.site.UploadButton
    loc:role = '' ! self.site.LookupButton
    loc:role = '' ! self.site.SmallDeleteButton
    loc:role = '' ! self.site.SmallSelectButton
    loc:role = '' ! self.site.SmallInsertButton
    loc:role = '' ! self.site.SmallChangeButton
    loc:role = '' ! self.site.SmallViewButton
    loc:role = 'deletef' ! self.site.DeletefButton
    loc:role = 'browsecancel' ! self.site.BrowseCancelButton
    loc:role = 'close' ! self.site.BrowseCloseButton
    loc:role = '' ! self.site.DateLookupButton
    loc:role = 'wizprev' ! self.site.WizPreviousButton
    loc:role = 'wiznext' ! self.site.WizNextButton
    loc:role = '' ! self.site.SmallOtherButton
    loc:role = '' ! self.site.SmallPrintButton
    loc:role = '' ! self.site.CopyButton
    loc:role = '' ! self.site.SmallCopyButton
    loc:role = 'clo' ! self.site.ClearButton
    loc:role = '' ! self.site.LogoutButton
    loc:role = '' ! self.site.AddFileButton
    loc:role = '' ! self.site.ClearFileButton
    loc:role = '' ! self.site.StartFileButton
    loc:role = '' ! self.site.CancelFileButton
    loc:role = '' ! self.site.RemoveFileButton
  End
  if not omitted(10)
    loc:class = self.combine(btn.class,p_class)
  else
    loc:class = btn.class
  End
  if p_type = 'submit' and p_disabled = 0 then p_disabled = -1.
  Return Self.CreateButton(p_type,clip(btn.name),clip(btn.textvalue),clip(loc:class),p_form,clip(p_action),p_target,p_onClick,p_javascript,p_disabled,clip(btn.image),btn.imagewidth,btn.imageheight,clip(btn.imagealt),clip(btn.Tooltip),btn.UseJavaScript,clip(btn.JsIcon),DefaultButton,,,btn.Compact,btn.MobileIcon,btn.MobileText,loc:role)

!------------------------------------------------------------------------------
NetWebServerWorker.CreateButton PROCEDURE  (string p_type, string p_name,<string p_text>, <string p_class>, <string p_form>, <string p_action>, <string p_target>,<string p_onClick>,<string p_Javascript>,Long p_Disabled=0,<string p_ImageSrc>,Long p_ImageWidth=-1,Long p_ImageHeight=-1,<String p_ImageAlt>,<String p_tip>,long p_UseJs=0,<String p_JsIcon>,Long p_Default=0,<String p_Options>,<String p_ImageCss>,Long p_Compact=0,<String p_MobileIcon>,<string p_MobileText>,<String p_role>)
! p_type = button, submit, reset
returnValue           string(NET:MaxBinData),Auto
loc:name              string(256)
loc:text              string(256)
loc:type              string(32)
loc:class             string(StyleStringSize)
!loc:tableclass        string(256)
loc:form              string(256)
loc:target            string(256)
loc:onClick           string(JavascriptStringLen)
loc:more              string(1024)
loc:javascript        string(JavascriptStringLen)
loc:disabled          String(20)
loc:image             String(1024)
loc:imagealt          String(256)
DontDoJs              Long
DspOk                 Long
loc:tip               string(Net:MaxBinData)
loc:randomid          string(4)
options               stringTheory
loc:default           string(50)
loc:compact           string(50)
loc:mobileicon        string(250)
loc:role              string(255)
  code
  !  we want to support 4 kinds of buttons here.
  ! a) buttons with just text                                     p_text <> '', p_imageSrc is omitted
  ! b) buttons with text and image                                p_text <> '', p_imageSrc <> ''
  ! c) buttons with just an image                                 p_text = '', p_imagesrc <> ''
  ! d) images that are a button (with no "button" around them)    done via the CSS Class

  loc:randomid = self.RandomId()
  if p_Default
    loc:default = ' data-nt-default="1"'
  end
  if not omitted(25)
    loc:role = self.wrap('data-do',p_role)
    if self.IsMobile()
      case p_role
      of 'close' orof 'select' orof 'cancel'
      loc:role = clip(loc:role) & ' data-direction="reverse"'
      end
    end
  end
  if self.IsMobile()
    if not omitted(24)
      loc:text = self.translate(p_mobiletext)
    end
  else
    if not omitted(4)
      loc:text = self.translate(p_text)
    end
  end
  loc:type = p_type
  loc:name = self._jsok(self._nocolon(p_name))
  do nospaces

  if not omitted(16) and p_tip <> ''
    loc:tip = self.CreateTip(p_tip)
  end

  if not omitted(12) ! p_ImageSrc
    if p_ImageSrc <> ''
      if not omitted(21)
        !self._trace('p_ImageSrc=' & clip(p_ImageSrc) & ' p_ImageCss=' & clip(p_ImageCss))
      else
        !self._trace('p_ImageSrc=' & clip(p_ImageSrc))
      end
      loc:Image = self.CreateImage(p_ImageSrc,p_ImageWidth,p_ImageHeight,loc:ImageAlt,,,,p_tip,,p_ImageCss)
    End
  End

  if not omitted(5) !p_class
    if p_class <> ''
      loc:class = p_class
      !loc:class = ' class="'&clip(p_class)&'"'
      !loc:tableclass = ' class="'&clip(p_class)&'Table"'
    end
  end
  if not omitted(20)
    options.SetValue(p_options,st:clip)
  end

  if p_UseJs and not self.IsMobile()
    if loc:text = ''
      self.SetOption(options,'text','false',true)
    end
    If not omitted(18) and p_JsIcon <> ''
      self.SetOption(options,'icons','{{primary:''ui-icon-'&clip(p_JsIcon)&'''}',true)
    end
    if p_disabled = 1
      self.SetOption(options,'disabled','true',true)
    end
  end
  if loc:class <> ''
    loc:class = ' class="' & clip(loc:class) &'"'
  end
  if omitted(8)
    loc:target = '_self'
  else
    loc:target = p_target
  end

  if not omitted(6) !p_form
    loc:form = p_Form
    if p_form <> ''
      if omitted(7) or p_action = '' !p_action
      else
        loc:onClick = 'document.' & clip(p_form) & '.action='''& self._jsok(self._MakeURL(p_action))&'''; document.' & clip(p_form) & '.target=''' & clip(loc:target) & ''';'
      end
    end
  end

  if not omitted(9) ! p_onClick
    if p_OnClick <> ''
      if loc:onclick <> ''
        if sub(loc:onclick,len(clip(loc:onclick)),1) <> ';' then loc:onClick = clip(loc:onClick)& ';'.
        loc:onClick = clip(p_onClick) & loc:onClick
      else
        loc:onClick = p_onClick
      end
    end
  end

  if not omitted(10) ! p_javascript
    loc:javascript = p_javascript
  end

  if p_disabled = 1
    loc:disabled = ' disabled="disabled"'
    loc:onClick = ''
    dspok = 1
  elsif p_disabled = -1 and loc:name <> '' and loc:form <> '' and lower(loc:type) = 'submit'
    loc:type = 'button'
    dspok = 1
    if loc:onclick <> '' and sub(loc:onclick,len(clip(loc:onclick)),1) <> ';' then loc:onClick = clip(loc:onClick)& ';'.
    loc:onClick = left(clip(loc:onClick) & ' dsb(event,'&clip(loc:form)&',0,'''&clip(loc:name)&''','''&clip(self._thisrow)&''','''&clip(self._thisvalue)&''');')
  elsif lower(loc:type) = 'submit' and loc:form = '' and p_action
    loc:onClick = self.WindowOpen(p_action,p_target)
    dspok = 1
  elsif loc:type = 'button'
    dspok = 1
  end
  do js

  if not omitted(15) ! p_ImageAlt
    loc:ImageAlt = p_ImageAlt
  end

  if p_compact and self.IsMobile()
    loc:compact = ' data-inline="true"'
  end

  if not omitted(23)
    if p_MobileIcon and self.IsMobile()
      loc:MobileIcon = ' data-icon="'&clip(p_MobileIcon)&'"'
    end
  end
  If self.IsMobile()
    if loc:text
      returnValue = '<a'&clip(loc:disabled)&' name="'&clip(loc:Name)&'" id="'&clip(loc:name)&clip(loc:randomid)&'" value="' & self._jsok(loc:text) & '"' & clip(loc:class) & ' ' &left(clip(loc:onClick)) & clip(loc:tip)&' '& clip(loc:javascript) & clip(loc:default) & clip(loc:compact) & clip(loc:MobileIcon) & clip(loc:role) & ' data-role="button" data-transition="slide">'
    else
      returnValue = '<button'&clip(loc:disabled)&' data-iconpos="notext" type="'&clip(loc:type)&'" name="'&clip(loc:Name)&'" id="'&clip(loc:name)&clip(loc:randomid)&'" value="' & self._jsok(loc:text) & '"' & clip(loc:class) & ' ' &left(clip(loc:onClick)) & clip(loc:tip)&' '& clip(loc:javascript) & clip(loc:default) & clip(loc:compact) & clip(loc:MobileIcon) & clip(loc:role) &  ' data-role="button" data-transition="slide">'
    end
    if loc:text <> '' and loc:image = ''     ! button type (a)
      returnValue = clip(ReturnValue) & clip(loc:text)
    elsif loc:text <> '' and loc:image <> '' ! button type (b)
      returnValue = clip(ReturnValue) & clip(loc:image) & ' ' & clip(loc:text)
    elsif loc:text = '' and loc:image <> ''  ! button type (c)
      returnValue = clip(ReturnValue) & loc:image
    elsif loc:text = ''
      !returnValue = clip(ReturnValue) & '&#160;'
    end
    if loc:text
      returnValue = clip(ReturnValue) & '</a>' & self.CRLF
    else
      returnValue = clip(ReturnValue) & '</button>' & self.CRLF
    end

  ElsIf (self.useragent = 'msie' or self.useragent = 'msie7') and dspok = 0  ! MSIE (6 and earlier) doesn't support the <button> tag properly.
    returnValue = '<input type="'&clip(loc:type)&'"'&clip(loc:disabled)&' name="'&clip(loc:Name)&'" id="'&clip(loc:name)&clip(loc:randomid)&'" value="' & self._jsok(loc:text) & '"' & clip(loc:class) & ' ' &left(clip(loc:onClick)) & clip(loc:tip)&' '& clip(loc:javascript) & clip(loc:default) & clip(loc:compact) & clip(loc:MobileIcon & clip(loc:role) ) &'/>' & self.CRLF
    If p_UseJs
      self.JQuery('#' & clip(loc:name)&loc:randomid,'button',options,loc:more)
    End
  else
    returnValue = '<button'&clip(loc:disabled)&' type="'&clip(loc:type)&'" name="'&clip(loc:Name)&'" id="'&clip(loc:name)&clip(loc:randomid)&'" value="' & self._jsok(loc:text) & '"' & clip(loc:class) & ' ' &left(clip(loc:onClick)) & clip(loc:tip)&' '& clip(loc:javascript) & clip(loc:default) & clip(loc:compact) & clip(loc:MobileIcon) &  clip(loc:role) & '>'
    if loc:text <> '' and loc:image = ''     ! button type (a)
      returnValue = clip(ReturnValue) & clip(loc:text)
    elsif loc:text <> '' and loc:image <> '' ! button type (b)
      returnValue = clip(ReturnValue) & clip(loc:image) & ' ' & clip(loc:text)
    elsif loc:text = '' and loc:image <> ''  ! button type (c)
      returnValue = clip(ReturnValue) & loc:image
    elsif loc:text = ''
      returnValue = clip(ReturnValue) & '&#160;'
    end
    returnValue = clip(ReturnValue) & '</button>' & self.CRLF
    If p_UseJs and not self.IsMobile()
      self.JQuery('#' & clip(loc:name)&loc:randomid,'button',options,loc:more)
    End
  end
  return clip(returnValue)

js  routine
  data
x  Long
y  Long
  code
  if loc:onClick <> ''
    x = instring('onclick="',loc:javascript,1,1)
    y = instring(';"',loc:javascript,1,x+1)
    if self.Ajax = 1
      loc:onClick = self._jsok(loc:onClick)
    end
    if x = 0 or y = 0
      loc:onClick = 'onclick="' & clip(loc:onClick) & '"'
      If (self.useragent = 'msie' or self.useragent = 'msie7')
        do addreferer
      End                                            ! http://www.nathanm.com/ie-windowopen-and-no-referrer-referer/
    Else
      loc:javascript = sub(loc:javascript,1,y) & ' ' & clip(loc:onclick) & '"'
      loc:onclick = '' ! 4.31
    end
  end

! 4.35
! bug in IE7 and earlier, fixed in IE8, does not pass Referer to window.open
! add it as a parameter instead.
addreferer  routine
  data
io long
iq long
  code
  io = instring('window.open',loc:onclick,1,1)
  if io
   iq = instring ('?',loc:onclick,1,io)
   if iq
     if self.Ajax = 1
       loc:onclick = sub(loc:onclick,1,iq) & '__Referer__=''+escape(location.href)+''&amp;' & sub(loc:onclick,iq+1,size(loc:onclick))
     else
       loc:onclick = sub(loc:onclick,1,iq) & '__Referer__=''+escape(location.href)+''&' & sub(loc:onclick,iq+1,size(loc:onclick))
     end
   end
 end

nospaces  routine
  data
x  Long
  code
  loop x = 1 to len(clip(loc:name))
    if loc:name[x] = ' '
      loc:name[x] = '_'
    End
  End
!------------------------------------------------------------------------------
NetWebServerWorker.PicLength   PROCEDURE (string p_picture)
loc:pic     String(32)
x           Long
ReturnValue Long
  code
  loc:pic = p_picture
  if loc:pic[1] = '@'
    loc:pic = loc:pic[2:32]
  End
  loc:pic[1] = upper(loc:pic[1])
  case loc:pic[1]
  of 'S'
    returnvalue = sub(loc:pic,2,30)
  of 'D'
    x = sub(loc:pic,2,30)
    execute x
      returnValue = 8  !d1
      returnValue = 10 !d2
      returnValue = 11 !d3
      returnValue = 16 !d4
      returnValue = 8  !d5
      returnValue = 10 !d6
      returnValue = 9  !d7
      returnValue = 11 !d8
      returnValue = 8  !d9
      returnValue = 10 !d10
      returnValue = 6  !d11
      returnValue = 8  !d12
      returnValue = 5  !d13
      returnValue = 7  !d14
      returnValue = 5  !d15
      returnValue = 7  !d16
    else
      returnValue = 12
    end
  of 'T'
    x = sub(loc:pic,2,30)
    execute x
      returnValue = 7  !t1    hh:mm (but also allow 2 for am/pm)
      returnValue = 6  !t2    hhmm
      returnValue = 7  !t3    hh:mmXM
      returnValue = 10 !t4    hh:mm:ss
      returnValue = 8  !t5    hhmmss
      returnValue = 10  !t6   hh:mm:ssXM
    else
      returnValue = 10
    end
  of 'N'
    x = 2
    if loc:pic[x] = '$'
      x += 1
    elsif loc:pic[x] = '~'
      x += 3
    end
    if loc:pic[x] = '-' or loc:pic[x] = '('
      x += 1
    end
    if loc:pic[x] = '0' or loc:pic[x] = '_' or loc:pic[x] = '*'
      x += 1
    end
    returnvalue = sub(loc:pic,x,30)
    if returnvalue < 0 then returnvalue = returnvalue * -1 + 1.
  of 'E'
    returnvalue = sub(loc:pic,2,30)
    if returnvalue < 0 then returnvalue = returnvalue * -1 + 1.
  of 'P'
    if loc:pic[len(clip(loc:pic))] = 'B' then loc:pic[len(clip(loc:pic))] = ' '.
    ReturnValue = len(clip(loc:pic)) - 2
  of 'K'
    if loc:pic[len(clip(loc:pic))] = 'B' then loc:pic[len(clip(loc:pic))] = ' '.
    loc:pic[len(clip(loc:pic))] = ' ' ! remove trailing K
    loop x = 2 to len(clip(loc:pic))
      case loc:pic[x]
      of '|'
      of '\'
        ReturnValue +=1
        x += 1
      else
        ReturnValue += 1
      end
    end
  end
  return ReturnValue
!------------------------------------------------------------------------------
! the _htmlString method has been deprecated in favor of the _jsok method. - ver 4.30.7
NetWebServerWorker._HtmlString PROCEDURE (string p_Value)
!loc:x                 Long
!loc:value             string(NET:MaxBinData)
!loc:check             string(1)
!loc:with              string(6)
  code
  return(self._jsok(p_value))
!  loc:value = p_value
!
!  loc:check = '"'
!  loc:with = '&quot;'
!  do rep
!
!  loc:check = '<'
!  loc:with = '&lt;'
!  do rep
!
!  loc:check = '>'
!  loc:with = '&gt;'
!  do rep
!
!  return clip(loc:value)
!
!rep  routine
!  loop
!    loc:x = Instring(loc:check,loc:value,1,1)
!    if loc:x = 0 then break.
!    loc:value = sub(loc:value,1,loc:x-1) & clip(loc:with) & sub(loc:value,loc:x+1,NET:MaxBinData)
!  end

!------------------------------------------------------------------------------
NetWebServerWorker.CreateGoogleMap  Procedure(String p_ID, <String p_Class>, <String p_Width>, <String p_Height>)
returnValue           string(NET:MaxBinData),Auto
loc:style                  string(StyleStringSize)
loc:class                  string(StyleStringSize)
  code
  if not omitted(4) and p_width <> ''
    if instring('px',lower(p_width),1,1) or instring('pt',lower(p_width),1,1) or instring('em',lower(p_width),1,1) or instring('%',p_width,1,1)
      loc:style = 'width:' & clip(p_width) & ';'
    else
      loc:style = 'width:' & clip(p_width) & 'px;'
    end
  end
  if not omitted(5) and p_height <> ''
    if instring('px',lower(p_height),1,1) or instring('pt',lower(p_height),1,1) or instring('em',lower(p_height),1,1) or instring('%',p_height,1,1)
      loc:style = clip(loc:style) & 'height:' & clip(p_height) & ';'
    else
      loc:style = clip(loc:style) & 'height:' & clip(p_height) & 'px;'
    end
  end
  if loc:style
    loc:style = ' style="' & clip(loc:style) & '"'
  end
  if not omitted(3) and p_Class <> ''
    loc:class = ' class="'& clip(p_class)&'"'
  end
  returnValue = '<div id="'&self._jsok(self._nocolon(p_id))&'"' & clip(loc:style) & clip(loc:class)& '></div>'
  return clip(ReturnValue)

!------------------------------------------------------------------------------
!NetWebServerWorker.CreateGoogleMap  Procedure(String p_Name, String p_Class, Real p_lat, Real p_long, Long p_Zoom, String p_type)
!returnValue           string(NET:MaxBinData),Auto
!  code
!  ReturnValue = 'var latlng = new google.maps.LatLng('&p_lat&', '&p_long&');' &|
!    'var myOptions = {{' &|
!    'zoom: '&p_zoom&',' &|
!    'center: latlng,' &|
!    'mapTypeId: google.maps.MapTypeId.ROADMAP' &|
!    '};' &|
!    'var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);'
!  return clip(ReturnValue)
!
!------------------------------------------------------------------------------
NetWebServerWorker.CreateTextArea PROCEDURE  (string p_Name, string p_Value, long p_Rows, long p_Cols,<string p_class>, <string p_Status>, <string p_Extra>,<String p_Javascript>, Long p_MaxLength=0,<String p_tip>,Long p_Display=0,<String p_placeholder>,Long p_Send=0,<String p_Id>,<String p_datado>,long p_editor=0)
returnValue           string(NET:MaxBinData),Auto
loc:name              string(255)
loc:class             string(StyleStringSize)
  CODE
  loc:name = self._jsok(self._nocolon(p_name))

  if not omitted(7) ! p_status
    if instring('readonly',p_status,1,1) and p_display = Net:Web:HtmlOk
      if p_send
        self._jsok(self.HtmltoxHtml(p_Value),p_display,p_send)
        return ''
      else
        return(self._jsok(self.HtmlToxHtml(p_Value),p_display))
      end
    end
  end

  if omitted(15)
    returnValue = '<textarea id="'&clip(loc:Name)&'" name="'&clip(loc:Name)&'" rows="'&p_Rows&'" cols="'&p_Cols&'"'
  else
    returnValue = '<textarea id="'&self._jsok(self._nocolon(p_id))&'" name="'&clip(loc:Name)&'" rows="'&p_Rows&'" cols="'&p_Cols&'"'
  end
  if not omitted(6) ! p_class
    loc:class = p_Class
  end
  case p_editor
  of net:HTMLTinyMCE
    loc:class = 'nt-tinymce ' & loc:class
  of net:HTMLRedactor
    loc:class = 'nt-redactor ' & loc:class
  end
  if loc:class
    returnValue = clip(ReturnValue) & ' class="'&clip(loc:Class) & '"'
  end

  if not omitted(7) ! p_status
    case clip(lower(p_status))
    of 'disabled'
    orof 'readonly'
       returnValue = clip(ReturnValue) & ' ' & clip(lower(p_Status)) &' ="' & clip(lower(p_Status)) &'"'
    end
  end
  if not omitted(8) ! p_extra
    returnValue = clip(ReturnValue) & ' ' & clip(p_Extra)
  end
  if not omitted(9) !p_javascript
    returnValue = clip(ReturnValue) & ' ' & clip(p_JavaScript)
  end

  If not omitted(10) ! p_maxlength
    If p_MaxLength > 0
      returnValue = clip(ReturnValue) & ' onkeypress="return ml(this,'&p_maxLength&',event);"'
    End
  end

  if not omitted(11)
    returnValue=clip(returnvalue) & self.CreateTip(p_tip)
  end

  if not omitted(13) ! p_placeholder
    returnValue=clip(returnvalue) & ' placeholder="' & self.Translate(p_placeholder) & '"'
  end

  if not omitted(16) ! p_datado
    returnValue=clip(returnvalue) & self.wrap('data-do',p_datado)
  end

  if p_Value = ''
     ReturnValue = clip(ReturnValue) & '></textarea>' & self.CRLF
     if p_send
       self.SendString(ReturnValue,1,len(clip(ReturnValue)),net:NoHeader)
       return ''
     else
       return clip(ReturnValue)
     end
   !end
  else
    if p_send
      ReturnValue = clip(ReturnValue) & '>'
      self.SendString(ReturnValue,1,len(clip(ReturnValue)),net:NoHeader)
      self._jsok(self.HtmlToxHtml(p_Value),p_display,p_Send)
      ReturnValue = '</textarea>' & self.CRLF
      self.SendString(ReturnValue,1,len(clip(ReturnValue)),net:NoHeader)
      return ''
    else
      return clip(ReturnValue)&'>'&self._jsok(self.HtmlToXhtml(p_Value),p_display)&'</textarea>' & self.CRLF
    end
  end

!------------------------------------------------------------------------------
NetWebServerWorker.CreateOption PROCEDURE  (<string p_Description>, string p_Value, byte p_Selected, <STRING p_Class>, <string p_Extra>,<String p_tip>)
returnValue           string(NET:MaxBinData),Auto
  CODE
  returnValue = '<option value="'&self._jsok(p_Value)&'"'
  if p_selected = 1
    returnValue = clip(ReturnValue) & ' selected="selected"'
  end
  if not omitted(5)
    returnValue = clip(ReturnValue) & ' class="' & clip(p_Class) & '"'
  end

  if not omitted(6)
    returnValue = clip(ReturnValue) & ' ' & clip(p_Extra)
  end
  if not omitted(7)
    returnValue = clip(ReturnValue) & self.CreateTip(p_tip)
  end
  if omitted(2)
    returnValue = clip(ReturnValue) &'>' & self.Translate(p_Value) & '</option>'
  Else
    returnValue = clip(ReturnValue) &'>' & self.Translate(p_Description) & '</option>'
  End
  return clip(returnValue)

!------------------------------------------------------------------------------
NetWebServerWorker.CreatePrompt PROCEDURE (STRING p_text,STRING p_id)
  Code
  Return Clip(p_text)
!------------------------------------------------------------------------------
NetWebServerWorker.GetView      PROCEDURE (View p_View,String p_Proc,String p_RandomId)
ReturnValue  Long
  Code
  Open(p_View)
  If Errorcode()
    ReturnValue = Errorcode()
  Else
    p_View{prop:order} = self.CleanFilter(p_view,self.GetSessionValue(clip(p_Proc)&'_prop:Order_' & p_RandomId))
    p_View{prop:filter} = self.CleanFilter(p_view,self.GetSessionValue(clip(p_Proc)&'_prop:Filter_' & p_RandomId))
    self.ResetPosition(p_view,self.ViewPos)
    Next(p_view)
    If Errorcode()
      ReturnValue = Errorcode()
      !self._trace('GetView (view,proc) NEXT failed ' & errorcode() & ' ' & error())
    End
    Close(p_View)
  End
  Return ReturnValue

!------------------------------------------------------------------------------
!  opens view, regets record based on pos,prop:order and prop:filter, and closes view. -- Reget(view,pos)
!  used to refresh a single row in the browse.
NetWebServerWorker.GetView      PROCEDURE (View p_View,String p_Pos,String p_Proc,String p_RandomId)
ReturnValue  Long
  Code
  Open(p_View)
  If Errorcode()
    ReturnValue = Errorcode()
  Else
    p_View{prop:order} = self.CleanFilter(p_view,self.GetSessionValue(clip(p_Proc)&'_prop:Order_' & p_RandomId))
    p_View{prop:filter} = self.CleanFilter(p_view,self.GetSessionValue(clip(p_Proc)&'_prop:Filter_' & p_RandomId))
    Reget(p_View,p_Pos)
    If Errorcode()
      ReturnValue = Errorcode()
      !self._trace('GetView (view,pos) REGET failed ' & errorcode() & ' ' & error())
    End
    Close(p_View)
  End
  Return ReturnValue
!------------------------------------------------------------------------------
NetWebServerWorker.GetView      PROCEDURE (View p_View,File p_File,String p_Proc,String p_RandomId)
ReturnValue  Long
  Code
  Open(p_View)
  If Errorcode()
    ReturnValue = Errorcode()
  Else
    p_View{prop:order} = self.CleanFilter(p_view,self.GetSessionValue(clip(p_Proc)&'_prop:Order_' & p_RandomId))
    p_View{prop:filter} = self.CleanFilter(p_view,self.GetSessionValue(clip(p_Proc)&'_prop:Filter_' & p_RandomId))
    reset(p_view,p_file)
    if errorcode()
      !self._trace('GetView (view,file) RESET failed ' & errorcode() & ' ' & error())
    end
    next(p_view)
    if errorcode()
      !self._trace('GetView (view,file) NEXT failed ' & errorcode() & ' ' & error())
      next(p_view)
      if errorcode()
        !self._trace('GetView (view,file) NEXT failed ' & errorcode() & ' ' & error())
      end
    end
    ReturnValue = Errorcode()
    Close(p_View)
  End
  Return ReturnValue

!------------------------------------------------------------------------------
NetWebServerWorker.Escape PROCEDURE  (string p_text, Long p_Flags=0)
! normally _jsok is used. However Escape should be used when adding a string to a HREF URL.
! specifically if the string may contain an '&' char, then js_ok will fail to encode it so _ParseSettings
! can decode it.
  CODE
  return self.EncodeWebString(p_text,p_Flags)
!------------------------------------------------------------------------------
NetWebServerWorker.ParseValueString PROCEDURE (string p_String)
  ! This function returns the "objectname" at the beginning of the string,
  ! and adds the rest to the value queue.
  ! EXAMPLE:
  ! Lookup?file=Customers&Field=CUS:FirstName (ButtonName)
  ! Browse?File=Suppliers&display=10 (Tag)
  ! settings passed to this function are assumed to be "formatted", not "raw".
objecttype      string(255)
x               long
y               long
i               long
prop            string(255)
val             string(255)
attribs         string(512)
  CODE
  x = instring('?',p_string,1,1)
  objecttype = sub(p_string,1,x-1)                 ! so this is the bit before the ?
  if x
    attribs = clip(sub(p_string,x+1,len(p_string)))
    x = 0
    loop i = 1 to len(clip(attribs))
      if attribs[i] = '='
        prop = sub(attribs,x+1,i-1-x)
        x = i
      elsif attribs[i] = '&'
        val = sub(attribs,x+1,i-1-x)
        x = i
        self.setvalue(prop,val,,net:formatted)
        self.setsessionvalue(prop,val)
      elsif i = len(clip(attribs))
        val = sub(attribs,x+1,i-x)
        self.setvalue(prop,val,,net:formatted)
        self.setsessionvalue(prop,val)
      end
    end
  end
  return clip(objecttype)

!------------------------------------------------------------------------------
NetWebServerWorker._NetWebKeyNamed PROCEDURE  (FILE p_File,String p_Label)
Loc:Key  &Key
Loc:k    &Key
Loc:x    Long,Auto
  Code
  Loc:Key &= NULL
  If Not p_File &= Null
    loop loc:x = 1 to p_File{prop:keys}
      Loc:k &= p_File{prop:key,loc:x}
      if upper(loc:k{prop:label}) = upper(p_Label)
        Loc:key &= p_File{prop:key,loc:x}
        Break
      End
    End
  End
  Return Loc:Key
!------------------------------------------------------------------------------
NetWebServerWorker._LoadRecord PROCEDURE (*file p_File, *KEY p_Key,<*String[] p_Value>)
! return 0 for success
f                     &file
k                     &key
x                     long
y                     long
loc:any               any
loc:group             &group
loc:found             Long
!loc:isguid            Long
loc:err               Long
loc:FieldLabel        String(256)
loc:IDField           String(256)
loc:opened            Long
  CODE
  If p_FILE &= NULL
    Return -1
  End

  f &= p_File

  if status(f) = 0
    self._openfile(f)
    loc:opened = true
  end

  if not p_Key &= NULL
    k &= p_Key
    do UseKey
    loc:found = 1
  end
  if loc:found = 0
    do FindPK
  end
  if loc:found = 0
    do FindUK
  end

  If Loc:Found = 0
    Loc:Err = -2
  End
  if loc:opened = true
    self._closefile(F)
  end
  return loc:Err

FindUK Routine
  loc:group &= f{prop:Record}
  loc:IDField = upper(self.GetValue('IDField'))
  if loc:IDField
    loop x = 1 to f{prop:keys}
      k &= f{prop:key,x}
      If k{PROP:Components} = 1
        y = 1
        Loc:FieldLabel = self._SetLabel(f,k{prop:field,y})
        If upper(loc:FieldLabel) = loc:IdField
          do UseKey
          loc:found = 1
          break
        end
      end
    end
  end

FindPK routine
    loop x = 1 to f{prop:keys}
      k &= f{prop:key,x}
      if k{prop:primary} = 1
        do UseKey
        loc:found = 1
        break
      end
    end

UseKey  Routine
  data
backcheck  string(255)
  code
  loc:group &= f{prop:Record}
  if not k &= null
    x = k{PROP:Components}
    loop y = 1 to x
      if y > Net:MaxKeyFields then break.
      Loc:any &= what(loc:group,k{prop:field,y})
      Loc:FieldLabel = self._SetLabel(f,k{prop:field,y})
      if omitted(4)
        Loc:any = self.GetValue(Loc:FieldLabel)
        backcheck = loc:any
        if backcheck <> self.GetValue(Loc:FieldLabel)
          Loc:err = -2
          exit
        end
        !get(f,k)   ! get shouldn't be here, only used for debugging
        !self._trace(' _LoadRecord - error = ' & errorcode() & ' ' & error() & ' loc:err=' & loc:err)
      else
        Loc:Any = clip(p_Value[y]) ! clip in case loc:any is a cstring
        backcheck = loc:any
        if backcheck <> clip(p_Value[y])
          Loc:err = -2
          exit
        end
        !get(f,k)   ! get shouldn't be here, only used for debugging
        !self._trace(' _LoadRecord - error = ' & errorcode() & ' ' & error() & ' loc:err=' & loc:err)
      End
    end
    Loc:Err = self._getfile(f,k)
    !self._trace('Load record - error = ' & errorcode() & ' ' & error() & ' loc:err=' & loc:err)
  end
  !self._trace('Load record done Loc:err=' &Loc:err )
!------------------------------------------------------------------------------
NetWebServerWorker.LocalQueueToSessionQueue PROCEDURE  (<string p_Modifier>)
loc:x               long
loc:prefix          cString(256)
  CODE
  if omitted(2)
    loc:prefix = ''
  elsif p_Modifier = ''
    loc:prefix = ''
  else
    loc:prefix = lower(clip(p_Modifier)) & '**'
  end
  self.setsessionvalue(loc:prefix,today() & ':' & clock())
  loop loc:x = 1 to records(self._LocalDataQueue)
    get(self._LocalDataQueue,loc:x)
    !self._trace('LocalQueueToSessionQueue: local data queue name=' & clip(self._LocalDataQueue.name) & ' value=' & clip(self._LocalDataQueue.Value) & ' formatted=' &  self._LocalDataQueue.ValueFormatted & ' loc:prefix=' & clip(loc:prefix))
    self._CopyValueToSession(loc:prefix)
  end

!------------------------------------------------------------------------------
NetWebServerWorker._CopyValueToSession PROCEDURE(STRING p_Prefix,<String p_SessionName>)  ! Value Queue MUST be loaded to correct place before calling this method.
loc:pic             string(32)
loc:sessionname     string(256)
  code
    if omitted(3)
      loc:sessionname = clip(p_Prefix) & self._LocalDataQueue.Name
      loc:pic = self.GetSessionPicture(self._LocalDataQueue.Name)
    else
      loc:sessionname = clip(p_Prefix) & p_SessionName
      loc:pic = self.GetSessionPicture(p_SessionName)
    end
    if  self._LocalDataQueue.ExtValue &= Null
      if loc:pic <> '' and lower(loc:pic[1:2]) <> '@s'
        if self._LocalDataQueue.ValueFormatted = NET:FORMATTED
          self.SetSessionValue(loc:sessionname,self.dformat(clip(self._LocalDataQueue.Value),clip(loc:pic)))
          !self._trace('1 Setting ' & clip(loc:sessionname) & ' to ' & self.dformat(clip(self._LocalDataQueue.Value),clip(loc:pic)))
        else
          self.SetSessionValue(loc:sessionname,self._LocalDataQueue.Value)
          !self._trace('2 Setting ' & clip(loc:sessionname) & ' to ' & self._LocalDataQueue.Value)
        end
      else
        loc:pic = self._LocalDataQueue.Picture
        if loc:pic <> '' and lower(loc:pic[1:2]) <> '@s'
          if self._LocalDataQueue.ValueFormatted = NET:FORMATTED
            self.SetSessionValue(loc:sessionname,self.dformat(clip(self._LocalDataQueue.Value),clip(loc:pic)))
          else
            self.SetSessionValue(loc:sessionname,self._LocalDataQueue.Value)
          end
        Else
          self.SetSessionValue(loc:sessionname,self._LocalDataQueue.Value)
        End
      end
    Else
      if loc:pic <> '' and lower(loc:pic[1:2]) <> '@s'
        self.SetSessionValue(loc:sessionname,self.dformat(clip(self._LocalDataQueue.ExtValue),clip(loc:pic)))
      else
        loc:pic = self._LocalDataQueue.Picture
        if loc:pic <> '' and lower(loc:pic[1:2]) <> '@s'
          self.SetSessionValue(loc:sessionname,self.dformat(clip(self._LocalDataQueue.ExtValue),clip(loc:pic)))
        Else
          self.SetSessionValue(loc:sessionname,self._LocalDataQueue.ExtValue)
        End
      end
    End
!------------------------------------------------------------------------------
NetWebServerWorker._ParsePart  PROCEDURE (*STRING p_Part,Long p_Start,Long p_End,Long p_OnlySessionID=0)
done         Long
cDisposition String(256)
cname        string(256)
cFileName    string(1024)
cType        string(256)
cBoundary    string(256)
cEncoding    string(256)
x            Long
y            Long
m1           Long
m2           Long
loc:name     like (NetWebServerLocalDataQueueType.Name)
valnum       long

  Code
  if p_start < 1 or p_Start > p_end then return done.

  !self._trace('PARSEPART ' & p_Part[p_start : p_end])
  x = instring('<13,10,13,10>',p_Part[p_start : p_end],1,1)
  if x = 0
    x = instring('<10,10>',p_Part[p_start : p_end],1,1)
  end
  if x
    cDisposition = self._GetHeaderField('Content-Disposition:', p_Part[p_start : p_end], 1, x, 1, 1)  ! part me
    y = instring ('; name=',cDisposition,1,1)
    if y
      cname = sub(cDisposition,y+2,size(cDisposition))
      m1 = instring('"',cname,1,1)
      m2 = instring('"',cname,1,m1+1)
      loc:name = sub(cname,m1+1,m2-m1-1)
    end
    y = instring ('; filename=',cDisposition,1,1)
    if y
      cfilename = sub(cDisposition,y+2,size(cDisposition))
      m1 = instring('"',cfilename,1,1)
      m2 = instring('"',cfilename,1,m1+1)
      cFileName = sub(cfilename,m1+1,m2-m1-1)
      if cFileName = ''
        cFilename = '-1'
      end
    end
    cEncoding = self._GetHeaderField('Content-Transfer-Encoding:', p_Part[p_start : p_end], 1, x, 1, 1) ! part me
    cType = self._GetHeaderField('Content-Type:', p_Part[p_start : p_end], 1, x, 1, 1)                  ! part me
    y = instring (';',cType,1,1)
    if y
      cBoundary = sub(cType,y+2,size(cType))
      cType = sub(cType,1,y-1)
    end
    case lower(cType)
    of 'multipart/mixed'
      !recurse here
      Return Done
    else
    End
    y = len(clip(p_part[p_start : p_end])) - 1 - 4 - x

    case lower(loc:name)
    of 'sessionid'
      self.SessionID = sub(p_part[p_start : p_end],x+4,y)
      self.TouchSession()
      if p_OnlySessionID then done = 1.
    of '' ! no name, do nothing
    else
      if p_OnlySessionID=0
        if cFileName = '-1'
          loc:name = self._UnEscape(loc:name)
          ! blank upload file vars are erased from the session. This means the value
          ! currently in the file will not be overwritten.
          !self.deletesessionvalue(loc:name)
          self.deletevalue(loc:name)
        elsif cFileName <> ''
          loc:name = self._UnEscape(loc:name)
          if self.IfExistsValue(loc:name)      ! 6.32, handles multiple files
            valnum = 2
            loop
              if self.IfExistsValue(clip(loc:name) & '_' & valnum) = 0
                loc:name = clip(loc:name) & '_' & valnum
                break
              end
              valnum += 1
            end
          end
          cFileName = self.Renamefile(loc:name,cFileName)
          ! NT7 - doen't save file here - passed to form, which does the saving.
          !cFileName = self.HandleFile(loc:name,cfilename,p_part[x+4 : len(p_part)],len(p_part)-x-5) ! not updated for p_start and p_end

          self.SetValue(loc:name, self._CleanIncoming(self._UnEscape(cFileName,net:NoPlus+net:NoColon),loc:name),,net:formatted)
          self.SetValue(lower(clip(loc:name)) & '_start',p_start-1+x+4)
          self.SetValue(lower(clip(loc:name)) & '_length',p_end-p_start + 1 -x-5)
          !self._trace('Set Value ' & clip(loc:name) & ' to ' & self._CleanIncoming(self._UnEscape(cFileName,net:NoPlus+net:NoColon),loc:name) & ' from ' & cFileName)
        else ! not a file, a regular variable
          loc:name = self._UnEscape(loc:name)
          case lower(loc:name)
          of '_bidv_'
            if p_OnlySessionID=0
              self.SetValue(loc:name,self._CleanIncoming(sub(p_part[p_start : p_end],x+4,y),loc:name))
              self.GetBrowseValue(self._CleanIncoming(sub(p_part[p_start : p_end],x+4,y),loc:name))
            End
          of '_unixdate_'
            self.setValue('_date_',self.UnixToClarionDate(self._CleanIncoming(sub(p_part[p_start : p_end],x+4,y),loc:name)))
            self.setValue('_time_',self.UnixToClarionTime(self._CleanIncoming(sub(p_part[p_start : p_end],x+4,y),loc:name)))
          of '_xrequestedwithxmlhttprequest_'
            self.Ajax = 1
          else
            if lower(self.site.htmlcharset) = 'utf-8' and self.site.StoreDataAs <> net:StoreAsUTF8
              self.SetValue(loc:name, self._CleanIncoming(self.UtfToAscii(sub(p_part[p_start : p_end],x+4,y)),loc:name),,net:formatted) ! values can contain __
            else
              self.SetValue(loc:name, self._CleanIncoming(sub(p_part[p_start : p_end],x+4,y),loc:name),,net:formatted) ! values can contain __
            end
          end
        end
      end
    end
  End
  Return Done
!------------------------------------------------------------------------------
! p_name is the name of the incoming field
! p_filename is the name of the file, as submitted by the browser
! p_path is an optional place for the file to go. If omitted then self.site.UploadsPath is used.
NetWebServerWorker.RenameFile PROCEDURE (STRING p_name,STRING p_filename,<String p_path>)
Ans      String(1024)
loc:path String(256)
x        Long
  Code
  ! first remove path (if there is one)
  x = Len(clip(p_Filename))
  loop until x < 1
    If p_FileName[x] = '/' or p_FileName[x] = '\'
      Break
    End
    x -= 1
  End
  If x > 0
    ans = sub(p_filename,x+1,255)
  else
    ans = p_filename
  end
  ! then add in default path
  if not omitted(4)
    loc:path = p_path
  end
  if loc:path = ''
    Ans = clip(self.site.UploadsPath) & '\' & ans
  else
    case right(loc:path,1)
    of '/' orof '\'
    else
      loc:path = clip(loc:path) & '\'
    End
    Ans = clip(p_path) & ans
  End
  Return Ans

!------------------------------------------------------------------------------
! This method is deprecated - use .Savefile instead. Included here for backward compatibility reasons.
NetWebServerWorker.HandleFile  PROCEDURE (STRING p_name,STRING p_filename,*STRING p_fileContents,LONG p_len)
  code
  return self.SaveFile (p_name,p_filename,p_fileContents,p_len)
!------------------------------------------------------------------------------
! p_name is the contents of the name="xxx" attribute in the incoming post.
! p_filename is the name of the file as you want to save it. If omitted the GetValue(p_name) is used.
! the Value is set earlier (in ParsePart, via RenameFile) to an adjusted version of the incoming name.
NetWebServerWorker.SaveFile  PROCEDURE (STRING p_name,<STRING p_filename>)
s long
l long
  code
  s = self.GetValue(lower(clip(p_name)) & '_start')
  l = self.GetValue(lower(clip(p_name)) & '_length')
  if s = 0 or l = 0 or s > size(self.RequestData.DataString)
    return 0
  elsif omitted(3)
    return self.SaveFile(p_name,self.getValue(lower(clip(p_name))),self.RequestData.DataString[s],l)
  else
    return self.SaveFile(p_name,p_filename,self.RequestData.DataString[s],l)
  end
!------------------------------------------------------------------------------
! p_name is passed in for the benefit of overriding this in the web handler derived method.
NetWebServerWorker.SaveFile  PROCEDURE (STRING p_name,STRING p_filename,*STRING p_fileContents,LONG p_len)
Local     group, pre (loc)
cStr        CString(FILE:MAXFILENAME)
hFile       long
Result      long
Result2     long
Written     long
x           long
          end
  CODE
  if p_len <= 0                              ! The data is too small to save (ie 0 bytes or less)
    return ''
  end
  if self.site.MaxPostSize > 0 and self.site.MaxPostSize < p_Len  ! the data is too big - don't write to disk.
    self._trace('the file was not saved to disk because the size ('&p_len&') > max ('&self.site.maxpostsize&')')
    return ''
  end
  loc:cStr = clip(p_FileName)

  !Create the file
  loc:hFile = OS_CreateFile(loc:cStr, NET:OS:GENERIC_WRITE, NET:OS:FILE_SHARE_READ, 0, NET:OS:CREATE_ALWAYS, 0, 0)
  !If the file was created and we have a handle for it
  if loc:hFile
    !Write the data to the file using the Windows API
    if OS_WriteFile(loc:hFile, p_fileContents, p_len, loc:Written,0).

    !Close the newly created file
    self.error = 0
    loc:result = OS_FlushFileBuffers(loc:hFile)  ! 0 return result = Failure
    loc:result2 = OS_CloseHandle(loc:hFile)      ! 0 return result = Failure
    if loc:result = 0 or loc:result2 = 0
      self.error = ERROR:FileAccessError
    end
  else
    self._trace('Error writing file ' & clip(loc:cStr))
    self.error = ERROR:FileAccessError
  end
  loc:x = len(CLIP(self.site.webFolderPath) & '\')
  if sub(p_filename,1,loc:x-1) <> self.site.webFolderPath
    loc:x = instring('\',p_filename,-1,255)
  end
  loc:Cstr = sub(p_filename,loc:x+1,255)
  return loc:CStr

!------------------------------------------------------------------------------
NetWebServerWorker._AddSetting PROCEDURE  (string p_Setting,Long p_OnlySessionID=0,Long p_Plus=0)
x           long
loc:name    like (NetWebServerLocalDataQueueType.Name)
done        Long
  CODE
  p_setting = self._UnEscape(p_setting,p_Plus+Net:NoColon) ! ok to use because _unescape always reduces size.
  !If self.Ajax = 1
  if lower(self.site.htmlcharset) = 'utf-8' and self.site.StoreDataAs <> net:StoreAsUTF8
    p_setting = self.UtfToAscii(p_setting) ! Ascii is always <= utf in size
  end
  x = instring('=',p_Setting,1,1)
  if x > 0
    loc:name = self._unescape(left(sub(p_setting,1,x-1)),p_Plus) ! changes __ to :
    case lower(loc:name)
    of 'sessionid'
      self.SessionID = sub(p_setting,x+1,len(clip(p_Setting))-x) !loc:value
      self.TouchSession()
      if p_OnlySessionID then done = 1.
    of '_bidv_'
      if p_OnlySessionID=0
        self.GetBrowseValue(self._CleanIncoming(sub(p_setting,x+1,len(clip(p_Setting))-x),loc:name))
        self.setValue(loc:name,self._CleanIncoming(sub(p_setting,x+1,len(clip(p_Setting))-x),loc:name))
      End
    of '_unixdate_'
      self.setValue('_date_',self.UnixToClarionDate(self._CleanIncoming(sub(p_setting,x+1,len(clip(p_Setting))-x),loc:name)))
      self.setValue('_time_',self.UnixToClarionTime(self._CleanIncoming(sub(p_setting,x+1,len(clip(p_Setting))-x),loc:name)))
    of '_xrequestedwithxmlhttprequest_'
      self.Ajax = 1
    else
      if p_OnlySessionID=0
        self.SetValue(loc:name, self._CleanIncoming(sub(p_setting,x+1,len(clip(p_Setting))-x),loc:name),,net:formatted)
      End
    end
  end
  Return Done
!------------------------------------------------------------------------------
! p_name is passed in for the benefit of overriding this in the web handler derived method.
NetWebServerWorker._CleanIncoming PROCEDURE  (String p_html,String p_name)
x long
y long
  Code
  if p_html = '' then Return ''.
  loop
    x = Instring('<!--',p_html,1,1)
    if x = 0 then break.
    p_html[x] = ' '
    p_html[x+1] = ' '
  end
  loop
    x = Instring('&#',p_html,1,x+1)
    if x = 0 then break.
    if p_html[x : x+4] = '&#39;' then cycle.
    p_html[x] = ' '
    p_html[x+1] = ' '
  end
  loop
    x = Instring('javascript:',lower(p_html),1,1)
    if x = 0 then break.
    loop y = 0 to 6
      p_html[x+y] = ' '
    end
  end
  loop
    x = Instring('expression(',lower(p_html),1,1)
    if x = 0 then break.
    loop y = 0 to 10
      p_html[x+y] = ' '
    end
  end
  Return clip(p_html)
!------------------------------------------------------------------------------
! the _clean method has been deprecated in favor of the _jsok method. - ver 4.30.3
NetWebServerWorker._Clean PROCEDURE  (String p_html)
!loc:Html  String(NET:MaxBinData)
!x long
!y long
  code
  return clip(self._jsok(p_html))

!  loc:Html = p_Html
!  x = len(clip(loc:Html))
!  y = 0
!  loop
!    y += 1
!    if y > x then break.
!    case val(loc:html[y])
!    of 60 ! <                  n
!    orof 62 ! >                n
!    orof 34 ! "                n
!    orof 35 ! #
!    orof 39 ! '
!    orof 59 ! ;
!    orof 38 ! &                n
!      loc:html = sub(loc:html,1,y-1) & '&#' & val(loc:html[y]) &';' & sub(loc:html,y+1,size(loc:html)-y)
!      y += 4
!    End
!  end
!  return clip(loc:Html)
!------------------------------------------------------------------------------
NetWebServerWorker._CloseConnection PROCEDURE  (long p_Force=false)
  CODE
  if self.RequestData.WebServer &= null
    return
  end
  if self.Flushed = false and (self.site.ReuseConnections or self.site.CompressDynamic)
    self._FlushSend(self.BodyToSend,self.site.CompressDynamic)
  end
  If self.RequestKeepAlive and self.site.ReuseConnections = 1 and p_Force = false
  else
    self.RequestData.WebServer._Wait()
    self.RequestData.WebServer.closeServerConnection(self.RequestData.OnSocket,self.RequestData.SockID)
    self.RequestData.WebServer._Release()
  end

!------------------------------------------------------------------------------

NetWebServerWorker._GetContentType PROCEDURE  (string p_FileName)
l                 long
Ext               string (40)
x                 long
ct                string(255)
  CODE
  l = len (clip(p_FileName))
  loop x = l to 1 by -1
    case p_FileName [x]
    of '.'
      if x < l
        Ext = p_FileName [(x+1) : l]
      end
      break
    end
  end
  l = instring('?',Ext,1,1)
  if l
    Ext = sub(Ext,1,l-1)
  end
  l = instring('&',Ext,1,1)
  if l
    Ext = sub(Ext,1,l-1)
  end

  case clip (Ext)
  of   'js'
  orof 'css'
  orof 'bmp'
  orof 'jpg'
  orof 'png'
  else
    self.RequestData.WebServer._PagesServed += 1
  end
  ct = parent._GetContentType(p_fileName)
  if ct = ''
    return (clip (self.RequestData.WebServer._UnknownContentType))
  else
    return clip(ct)
  end

!------------------------------------------------------------------------------
NetWebServerWorker._HandleGet PROCEDURE  ()
  CODE
  if instring ('\',self.Requestfilename,1,1) = 0 ! in case RequestFileName has been reset by the template
    self.Requestfilename = clip(self.site.WebFolderPath) & '\' & self.Requestfilename
  end
  self._ParseURL()
  self._SetAjax()
  if self.Ajax >= 0
    Self._SetMobile()
    self.ChangeTheme()
    self.ProcessLink()
  end

!------------------------------------------------------------------------------
NetWebServerWorker._SetAjax PROCEDURE  ()
  code
  self.Ajax = self.GetValue('_ajax_')
  ! Ajax requests must have a session ID set in a cookie, otherwise nothing is returned
  ! This is to prevent Javascript hacking as per http://www.fortifysoftware.com/advisory.jsp
  If self.Ajax = 1 and self.SessionID = ''
    ! ignore ajax requests that do not include a SessionID in the cookie.
    self._trace('ajax Request has no session id - request rejected')
    self._CloseConnection(true)
    self.PerfEndThread()
    self.Ajax = -1
    self.DontSendFile = 1
    return
  End

!------------------------------------------------------------------------------
NetWebServerWorker._HandleGetRest PROCEDURE  ()
  CODE
  if self.GetSessionLoggedIn() = 0 and instring('\' & clip(lower(self.site.loggedInDir)) & '\',lower(self.RequestFileName),1,1)
    self.SendError(401, 'Not Logged In', 'You need to Log In before you can view that page')
  Else
    self.ProcessGet()
    !self._trace('_HandleGetRest self.DontSendFile=' & self.DontSendFile & ' self.Requestfilename=' & self.Requestfilename)
    if self.DontSendFile
    else
      self._SendFile (clip(self.Requestfilename))
    end
  End
!------------------------------------------------------------------------------
NetWebServerWorker._HandlePost PROCEDURE  ()
! data passed in Post statements is assumed to be formatted.
loc:FileName     string(260)
x                long
  CODE
  if instring ('\',self.Requestfilename,1,1) = 0  ! in case RequestFileName has been reset by the template
    self.setvalue('requestfilename',self.PageName)
    self.Requestfilename = clip(self.site.WebFolderPath) & '\' & self.Requestfilename
  end
  self.RequestAction = sub(self.RequestData.DataString,7,260)
  x = instring(' ', self.RequestAction, 1, 1)
  if x > 0
    self.RequestAction = lower(sub(self.RequestAction, 1, x-1))
  end

  x = instring('<13,10,13,10>', self.RequestData.DataString, 1, 1)
  if (x > 0) and (x+4 <= self.RequestData.DataStringLen)
    if self.RequestBoundary = '' and x
      self.SetValue('_postheader_',self.RequestData.DataString[1 : x-1])
      if x + 4 <= self.RequestData.DataStringLen
        self.SetValue('_postdata_',self.RequestData.DataString[(x+4) : self.RequestData.DataStringLen])
      end
    end
    if self.RequestBoundary = '' and self.RequestContentType = 'application/json'
      self.SetValue('_json_',self.RequestData.DataString[(x+4) : self.RequestData.DataStringLen])
    elsif self.RequestBoundary = '' and self.RequestContentType = 'text/xml'
      self.SetValue('xml',self.RequestData.DataString[(x+4) : self.RequestData.DataStringLen])
    elsif self.RequestBoundary <> ''
      self._ParseSettings(self.RequestData.DataString,x+4,self.RequestData.DataStringLen,0,self.RequestBoundary,net:Multipart)
    else
      self._ParseSettings(self.RequestData.DataString,x+4,self.RequestData.DataStringLen)
    end
  end
  self.SubmitButton()
  self._ParseURL()
  self._SetAjax()
  if self.Ajax >= 0
    Self._SetMobile()
    self.ChangeTheme()
    self.ProcessLink()
    if self.AllDone then return.
    self.ProcessPost()
  end
!------------------------------------------------------------------------------
NetWebServerWorker._FindSessionID         PROCEDURE ()
x     long
y     long
done  long
  CODE
  ! first look in cookies
  x = Instring('<13,10>cookie:',lower(self.RequestData.DataString),1,1)
  If x
    y = instring('<13,10>',self.RequestData.DataString,1,x+1)
    done = self._ParseSettings(self.RequestData.DataString,x+10,y,Net:OnlySessionID,';',,Net:NoPlus)
  End
  ! Ajax requests _must_ have a valid Sessiond ID set to explictly prevent JavaScript Hacking.
  If self.Ajax = 0
    If not done
      ! then look in Post String
      case self.RequestMethodType
      of NetWebServer_POST orof NetWebServer_PUT
        x = instring('<13,10,13,10>', self.RequestData.DataString, 1, 1)
        if x
          if (x > 0) and (x+4 <= self.RequestData.DataStringLen)
            if self.RequestBoundary <> ''
              done = self._ParseSettings(self.RequestData.DataString,x+4,self.RequestData.DataStringLen,Net:OnlySessionID,self.RequestBoundary,net:Multipart)
            else
              done = self._ParseSettings(self.RequestData.DataString,x+4,self.RequestData.DataStringLen,Net:OnlySessionID)
            end
          end
        end
      end
    end
    if not done and not (self.WholeURL &= Null)
      ! Then look on URL
      x = instring('?', self.WholeURL, 1, 1)
      if x > 0
        y = len(clip(self.WholeURL))
        if (x+1) <= y
          done = self._ParseSettings(self.WholeURL,x+1,y,Net:OnlySessionID,,,Net:NoPlus)
        end
      end
    end
    if not done
      self.NewSession()
    End
  End
!------------------------------------------------------------------------------
NetWebServerWorker._ReadCookies  PROCEDURE  (String p_Header)
x  Long
y  Long
  Code
  x = Instring('<13,10>cookie: ',lower(p_header),1,1)
  y = instring('<13,10>',p_header,1,x+1)
  self._ParseSettings(p_header,x+10,y,0,';',,Net:NoPlus)
!------------------------------------------------------------------------------
NetWebServerWorker.DeleteCookie  PROCEDURE (String p_name)
  code
  self.SetCookie(p_Name,'',100)

!------------------------------------------------------------------------------
NetWebServerWorker._MakeCookies  PROCEDURE  ()
ReturnValue  String(4096)
dt           String(80)
pt           String(1024)
dm           String(100)
sc           String(10)
ho           String(15)
x            Long
  Code
  loop x = 1 to records(self._CookieQueue)
    get(self._CookieQueue,x)
    if self._CookieQueue.ExpiresDate <> 0
      if self._CookieQueue.ExpiresTime = 0
        self._CookieQueue.ExpiresTime = 100
      end
      dt = '; expires=' &choose(self._CookieQueue.ExpiresDate % 7 + 1,'Sun','Mon','Tue','Wed','Thu','Fri','Sat') & |
      ', ' & format(self._CookieQueue.ExpiresDate,@d08-) & ' '& format(self._CookieQueue.ExpiresTime,@T04) & ' GMT;'
    else
      dt = ''
    end
    if self._CookieQueue.Path <> ''
      pt = '; path=' & clip(self._CookieQueue.Path)
    else
      pt = ''
    end
    if self._CookieQueue.Domain <> ''
      dm = '; domain=' & clip(self._CookieQueue.Domain)
    else
      dm =''
    end
    if self._CookieQueue.Secure <> 0
      sc = '; secure'
    else
      sc = ''
    End
    if self._CookieQueue.HttpOnly <> 0
      ho = '; HttpOnly'
    else
      ho = ''
    End
    ReturnValue = clip(ReturnValue) & 'Set-Cookie: ' & clip(self._CookieQueue.Name) & |
                  '=' & clip(self._CookieQueue.Value) &|
                  clip(dt) & clip(pt) & clip(dm) & clip(sc) & clip(ho) & '<13,10>'
  end
  return(clip(ReturnValue))
!------------------------------------------------------------------------------
! somewhat recursive, because this is called by ParseHTML and it in turn calls ParseHTML
! for Net:f: tags.
NetWebServerWorker._HandleTag PROCEDURE  (string p_TagString,<String p_context>)
tag                 StringTheory
q                   long
st                  StringTheory
x                   long
found               long

  CODE
  if self._InHandleTag > 10 then return.
  self._InHandleTag += 1
  tag.setvalue(lower(p_TagString))
  q = tag.instring('?')
  if q = 0 then q = tag.Length() else q -=1.
  case tag.left(2)
  of 'c:'
    do CodeBlocks
  of 's:'
    do Sessions
  of 'x:'
    do xml
  of 'v:'
    do Values
  of 'd:'
    do DateTimes
  of 'f:'
    found = st.LoadFile(clip(self.site.WebFolderPath)& '\' & tag.slice(3,q))
    if found
      self.parseHTML(st,,,Net:NoHeader)
      found = 2
      self.RemoveOneServe(clip(self.site.WebFolderPath)& '\' & tag.slice(3,q))
    end
  end
  case found
  of 1
    if st.Length() > 0
      self.SendString(st.value, 1, st.Length(), NET:NoHeader)
    end
  of 2
  else
    self.ProcessTag (p_TagString)
  end
  self._InHandleTag -= 1
  return

! --------------------------
Xml  routine
  st.SetValue(self.GetSessionValueFormat(tag.slice(3,q)))
  If self.GetValueError = Net:UnsafeHtmlOk
    st.SetValue(self.translate(st.GetValue(),Net:UnsafeHtmlOk+Net:HtmlOk))
    self.GetValueError = 0
  else
    st.SetValue(self.translate(st.GetValue(),Net:HtmlOk))
  end
  if self.GetValueError = 0
    found = true
  end

! --------------------------
Sessions Routine
  case tag.slice(3,q)
  ! ------------
  of   'sessionid'
    if self.SessionID = ''
      self.NewSession()
    end
    found = true
    st.SetValue('<input type="hidden" name="SessionID" value="'&clip(self.SessionID)&'"/>')!</input>')

  ! ------------
  of 'sid'
    if self.SessionID = ''
      self.NewSession()
    end
    found = true
    if not omitted(3)
      if p_context = '?' or p_context = '&'
        st.SetValue('SessionID=' & self.SessionID)
      else
        st.SetValue('?SessionID=' & self.SessionID)
      end
    else
      st.SetValue('?SessionID=' & self.SessionID)
    end
  of '-sid'
    if self.SessionID = ''
      self.NewSession()
    end
    found = true
    st.SetValue('SessionID=' & self.SessionID)

  of '-sessionid'
    st.SetValue(self.SessionID)
    found = true

  ! ------------
  of   'referer'
  orof 'ref'
    found = true
    st.SetValue(clip(self.RequestReferer))

  ! ------------
  else
    st.SetValue(self.GetSessionValueFormat(tag.slice(3,q)))
    if tag.instring('-t0-',1,q)
      self.translateoff += 1
    end
    If self.GetValueError = Net:UnsafeHtmlOk
      st.SetValue(self.translate(st.GetValue(),Net:UnsafeHtmlOk))
      self.GetValueError = 0
    else
      st.SetValue(self.translate(st.GetValue()))
    end
    if tag.instring('-t0-',1,q)
      self.translateoff -= 1
    end
    if self.GetValueError = 0
      found = true
    end
  end

!---------------------------
DateTimes  routine
  case tag.slice(3,q)
  of 'dow'
    st.SetValue(today() % 7)
    found = true
  of 'weekday'
    st.SetValue(self.GetWeekdayName(today(),NetName:Full))
    found = true
  of 'dayname'
    st.SetValue(self.GetDayName(day(today())))
    found = true
  of 'day'
    st.SetValue(day(today()))
    found = true
  of 'month'
    st.SetValue(month(today()))
    found = true
  of 'monthname'
    st.SetValue(self.GetMonthName(month(today()),0,netName:Full))
    found = true
  of 'year'
    st.SetValue(year(today()))
    found = true
  of 'hour'
    st.SetValue(int((clock()+1) / 360000))
    found = true
  of 'minute'
    st.SetValue(int(((clock()+1) % 360000) / 6000))
    found = true
  of 'second'
    st.SetValue(int(((clock()+1) % 6000) / 100))
    found = true
  of 'today'
    st.SetValue(self.mformat(today(),self.site.datepicture))
    found = true
  of 'clock'
    st.SetValue(self.mformat(clock(),'@t4'))
    found = true
  end
!---------------------------
Values Routine
  case tag.slice(3,q)
  ! ------------
  of   'referer'
  orof 'ref'
    found = true
    st.SetValue(clip(self.RequestReferer))

  ! ------------
  else
    st.SetValue(self.GetValueFormat(tag.slice(3,q)))
    if self.GetValueError = 0
      found = true
    end
  end

!---------------------------
CodeBlocks Routine
  case tag.slice(3,q)
  ! ------------
  of 'head'
     st.SetValue(self.IncludeStyles() & self.IncludeScripts())
     found = true
  of 'scripts'
     st.SetValue(self.IncludeScripts())
     found = true
  of 'styles'
     st.SetValue(self.IncludeStyles())
     found = true
  of 'body'
     st.SetValue(self.BodyOnLoad())
     found = true
  end

!------------------------------------------------------------------------------

NetWebServerWorker._ParseRequestHeader PROCEDURE  ()
local         group, pre(loc)
s               long
x               long
y               long
z               long
e               long
Okay            long
              end

  CODE
  self.RequestFileName = ''
  self.UserURL = ''

  ! set pagename, RequestFileName, anchor, WholeURL and UserURL.
  Do Calc_FileName
  Do Calc_BrowserCacheDetails
  do Calc_Ajax
  Do Calc_Referer
  do Calc_Host
  do Calc_ContentType
  do Calc_RequestGzip
  do Calc_RequestKeepAlive
  self.WebLog()

! ------------------------------------------
Calc_ContentType  Routine
  self.RequestContentType = self._GetHeaderField('Content-Type:', self.RequestData.DataString, 1, self.RequestData.DataStringLen, 1, 1)
  loc:z = instring(';', self.RequestContentType,1,1)
  if loc:z
    if instring('multipart/form-data',self.RequestContentType,1,1)
      self.RequestBoundary = '--' & self._GetHeaderField('boundary',self.RequestContentType, 1, size(self.RequestContentType), 1, 1)
    end
    self.RequestContentType = sub(self.RequestContentType,1,loc:z-1)
  end

! ------------------------------------------
Calc_BrowserCacheDetails Routine
  self.IfModifiedSince = self._GetHeaderField ('If-Modified-Since:', self.RequestData.DataString, 1, self.RequestData.DataStringLen, 1, 1)
  self.IfNoneMatch = self._GetHeaderField ('If-None-Match:', self.RequestData.DataString, 1, self.RequestData.DataStringLen, 1, 1)

! ------------------------------------------
Calc_Referer Routine
  self.RequestReferer = self._GetHeaderField('Referer:', self.RequestData.DataString, 1, self.RequestData.DataStringLen, 1, 1)
  if sub(self.RequestReferer,len(clip(self.RequestReferer)),1) = '/'
    self.RequestReferer = clip(self.RequestReferer) & self.site.DefaultPage
  end
  if self.RequestReferer = ''
    If self.Ajax = 1
      self.RequestReferer = '-1'
    else
      self.RequestReferer =  self.site.DefaultPage
    End
  end

! ------------------------------------------
Calc_Host   Routine
  self.RequestHost = self._GetHeaderField ('host:', self.RequestData.DataString, 1, self.RequestData.DataStringLen, 1, 1)

! ------------------------------------------
Calc_Ajax Routine
  if self._GetHeaderField('x-requested-with:', self.RequestData.DataString, 1, self.RequestData.DataStringLen, 1, 1)
!    self.Ajax = 1
  End
! ------------------------------------------
Calc_RequestGzip Routine
  data
h   string(512)
  code
  h = self._GetHeaderField('Accept-Encoding:', self.RequestData.DataString, 1, self.RequestData.DataStringLen, 1, 1)
  if instring('gzip',h,1,1) > 0
    self.RequestGzip = 1
  else
    self.RequestGzip = 0
  end
! ------------------------------------------
Calc_RequestKeepAlive Routine
  data
h   string(512)
  code
  h = self._GetHeaderField('Connection:', self.RequestData.DataString, 1, self.RequestData.DataStringLen, 1, 1)
  if instring('keep-alive',h,1,1) > 0
    self.RequestKeepAlive = 1
  else
    self.RequestKeepAlive = 0
  end
! ------------------------------------------
Calc_FileName Routine
  ! Find first CR
  loc:z = instring('<13,10>',self.RequestData.DataString,1,1)
  if loc:z = 0
    loc:z = self.RequestData.DataStringLen
  else
    if loc:z > 1
      loc:z -= 1
    else
      exit
    end
  end

  ! Find starting point of requested url/file
  loc:x = instring('/',self.RequestData.DataString[1:loc:z],1,1)
  loc:y = instring(' ',self.RequestData.DataString[1:loc:z],1,1)
  if loc:y > 0 and ((loc:y+1) < loc:x)
    loc:x = loc:y
  end

  if loc:x = 0 or loc:x > loc:z
    exit ! Maybe this should rather report an error ?
  end

  ! Find end of url/file
  loc:y = instring(' ',self.RequestData.DataString[1:loc:z],1,loc:x)
  if loc:y = 0
    loc:y = loc:z
  else
    loc:y -= 1
  end

  if loc:y >= loc:x
    ! set self.WholeURL
    if not (self.WholeURL &= Null)
      dispose(self.WholeURL)
    end
    Self.WholeURL &= NEW (String(loc:y-loc:x+1))
    self.WholeURL = self.RequestData.DataString[loc:x : loc:y]

    ! chop at ? or ! mark
    loc:s = instring('?',self.WholeURL,1,1)
    loc:e = instring('!',self.WholeURL,1,1)
    If loc:e > 0
      If loc:s > 0
        Self.anchor = sub(self.WholeURL,loc:e+1,loc:s-loc:e-1)
      Else
        Self.anchor = sub(self.WholeURL,loc:e+1,loc:y-loc:e)
      End
    End

    if loc:s = 0 and loc:e = 0
      self.RequestFileName = self.WholeURL
    elsif loc:s = 0 and loc:e <> 0
      self.RequestFileName = self.WholeURL[1: loc:e-1]
    elsif loc:e <= loc:s and loc:e <> 0
      self.RequestFileName = self.WholeURL[1: loc:e-1]
    elsif loc:s
      self.RequestFileName = self.WholeURL[1: loc:s-1]
    end
    ! not sure yet about the comments in the block below.
    !if lower(self.site.htmlcharset) = 'utf-8' and self.site.StoreDataAs <> net:StoreAsUTF8
      self.RequestFileName =  self.UtfToAscii(self._UnEscape(self.RequestFileName,Net:NoColon))
    !else
      !self.RequestFileName =  self._UnEscape(self.RequestFileName,Net:NoColon)
    !end

    ! Convert / to \
    loop loc:s = 1 to len(clip(self.RequestFileName))
      if self.RequestFileName[loc:s] = '/'
        self.RequestFileName[loc:s] = '\'
      end
    end
    if (loc:y-loc:x+1) >= 2
      if self.RequestFileName[1] = '\'
        ! remove first \
        self.RequestFileName = sub(self.RequestFileName,2,size(self.RequestFileName)-1)
      end
    end
    self.UserURL = self.RequestFileName
    if self.RequestFileName = '' or self.RequestFileName = '\'
      self.UserURL = self._TranslateFileName(self.site.DefaultPage)
      self.RequestFileName = clip(self.site.WebFolderPath) & '\' & self._TranslateFileName(self.site.DefaultPage)
    else
      self.RequestFileName = clip(self.site.WebFolderPath) & '\' & self._TranslateFileName(self.RequestFileName)
    end
    self.RequestFileName = self._UnEscape (self.RequestFileName,net:NoPlus+net:NoColon)
    self.pagename = self.GetPageName(Self.RequestFileName)
  end

!------------------------------------------------------------------------------

NetWebServerWorker._ParseURL PROCEDURE  ()
x          long
sl         long
  CODE
  if self.WholeURL &= Null then return.
  !self._trace('_ParseURL ' & clip(self.WholeURL))

  x = instring('?', self.WholeURL, 1, 1)
  sl = len(clip(self.WholeURL))
  if x > 0
    if (x+1) <= sl
      self._ParseSettings(self.WholeURL,x+1,sl)
    end
  end
  ! IE6 and 7 do not pass the referer with a window.open
  !   this next bit allows the RequestReferer to be set as a value
  !   See also CreateButton command.
  if self.GetValue('a') and self.anchor = ''
    self.anchor = self.GetValue('a')
  end
  if self.RequestReferer =  self.site.DefaultPage
    if self.IfExistsValue(':Referer:')
      self.RequestReferer = self.GetValue(':Referer:')
    end
  end

!------------------------------------------------------------------------------
NetWebServerWorker.ParseHTML PROCEDURE  (StringTheory p_Buffer, long p_DataStart=1, long p_DataEnd=0, Byte p_Header=NET:SendHeader)
  code
  self.parseHTML(p_buffer.value,p_DataStart,p_DataEnd,p_Header)

!------------------------------------------------------------------------------
NetWebServerWorker.ParseHTML PROCEDURE  (*String p_Data, long p_DataStart=1, long p_DataEnd=0, Byte p_Header=NET:SendHeader)

loc:x                long,auto           ! Start of <!--
loc:y                long,auto           ! Last position processed until
loc:z                long           ! End of <!--
loc:e                long,auto           ! End of data
loc:ParseString      string (256)
loc:ParmString       string (256)
loc:SendHeader       long
loc:context          string(1)
loc:startat          Long(1)

  CODE
  if Self._HttpHeaderSent
    p_Header = Net:NoHeader
  end
  loc:e = size (p_Data)
  if loc:e = 0
    !self._trace('ParseHTML passed a blank string')
    !self.SendError(404, 'Not Found', '5. The requested URL was not found on this server') ! File Not Found
    return
  end
  If p_DataEnd > 0 and p_DataEnd < loc:e
    loc:e = p_DataEnd
  ElsIf p_DataEnd = 0
    loc:e = len(clip(p_Data))
    if loc:e = 0 and p_Header = Net:NoHeader  ! nothing to do
      return
    End
  End
  If self.Ajax = 1 and self.ReplyContentFixed = 0
    self.ReplyContentType = 'text/xml'
  end
  If self.Ajax = 1 or band(p_Header,Net:DontCache)
    self.HeaderDetails.CacheControl = 'no-store, no-cache, must-revalidate, private,post-check=0, pre-check=0, max-age=0 '
    self.HeaderDetails._Pragma = 'no-cache'
    self.HeaderDetails.Date = today()
    self.HeaderDetails.Time = clock()
    self.HeaderDetails.ExpiresDate = today() - 365
    self.HeaderDetails.ExpiresTime = clock()
    p_header = band(p_header,Net:SendHeader)
  End
  If loc:e > len(clip(self.site._CheckParseHeader))
    loc:x = Instring(clip(self.site._CheckParseHeader),p_Data,1,1)  ! <!-- NetWebServer -->
    If loc:x
      self.HeaderDetails.ContentLength = '' ! length of dynamic files is unknown.
      loc:y = len(clip(self.site._CheckParseHeader))
      If Loc:x = 1
        p_Data[1 : loc:e-loc:y] = p_data[ loc:x+loc:y : loc:e]
        loc:e -= loc:y
      Else
        p_Data[loc:x : loc:x+loc:y-1] = all(' ' ,255)
      End
    end
  End
  loc:x = 0
  loc:y = 0
  If loc:e > 9 ! <!-- Net:
    loc:x = instring ('<!-- Net:', p_Data, 1, loc:x+1)
  End
  if loc:x = 0
    if band(p_Header,NET:SendHeader) > 0
      self._sendMemory(p_Data,loc:e)
    else
      if loc:e > 0
        self.SendString(p_Data,1,loc:e, p_Header)
      end
    end
  else
    if band(p_Header,NET:SendHeader) > 0 and self._HttpHeaderSent = 0
      self.HeaderDetails.ContentLength = '' ! We don't know it - it will be dynamic
      self.SetHeader200(net:DontCache)
      self.ReplyHeaderString = self.CreateHeader(self.HeaderDetails) & '<13,10>'
      loc:SendHeader = true
    End
    loop
      if loc:x = 0
        break
      end
      Do FirstBit
      Do MiddleBit
      loc:x = instring ('<<!-- Net:', p_Data, 1, loc:x+1)
    end
    Do LastBit
  end

! -----------------------------------------------
FirstBit Routine
  if loc:x = 1 then exit.
  if (loc:x-1) >= loc:y
    if loc:SendHeader
      self.SendString (p_data, loc:y, (loc:x-1), NET:SendHeader)
      loc:SendHeader = false
    else
      self.SendString (p_data, loc:y, (loc:x-1), NET:NoHeader)
    end
  end

! -----------------------------------------------
MiddleBit Routine
  loc:z = instring ('-->', p_Data, 1, loc:x+1)
  if loc:z = 0
    return ! All done - return from function
  end
  if (loc:x+9) <= (loc:z-1)
    loc:ParseString = p_Data[loc:x+9 : loc:z-1]
    if loc:x > 1
      loc:context = p_Data[loc:x-1]
    end
  else
    loc:ParseString = ''
  end
  loc:parseString = self.ParseValueString(loc:ParseString) ! tags that contain data are assumed to be "formatted".

  self.PushFormState()
  if self.SessionID = ''
    self.NewSession()
  end
  self._HandleTag (clip(loc:ParseString),loc:context)
  self.PopFormState()

  loc:y = loc:z+3

! -----------------------------------------------
LastBit Routine
  if loc:e >= loc:y
    self.SendString (p_data, loc:y, loc:e, NET:NoHeader)
  end

!------------------------------------------------------------------------------
NetWebServerWorker._ParseSettings PROCEDURE  (*string p_Settings,Long p_Start,Long p_End,Long p_OnlySessionID=0,<String p_Sep>,Long p_Encoded=Net:Inline,Long p_Plus=Net:Plus)
x                long
y                long
length           long
sep              String(256)
done             Long
  CODE
  !self._trace('PARSESETTINGS 0 pStart=' & p_Start & ' pEnd=' & p_End & ' ' & clip(p_settings[p_start : p_end]))
  if omitted(6) !(p_sep)
    if p_Encoded=Net:Inline
      sep = '&'
    else
      sep = self.RequestBoundary
    end
  else
    sep = p_sep
  end
  y = len(clip(sep))
  loop x = p_End to p_Start + 1 by -1  ! clip, and remove <13,10>'s from string to search
    !if p_Settings[x-1 : x] = '<13,10>'
    !  p_End -= 2
    !  cycle
    !end
    if p_Settings[x] = ' ' or p_Settings[x] = '<13>' or p_Settings[x] = '<10>'
      p_End -= 1
      cycle
    end
    break
  end
  if p_end = 0 or p_end < p_start then return 0.

  loop while p_end >= p_start
    !self._trace('PARSESETTINGS LOOP pStart=' & p_Start & ' pEnd=' & p_End & ' ' & clip(p_settings[p_start : p_end]) & ' SEP=' & clip(sep))
    x = instring (clip(sep),p_settings[p_start : p_end],1,1)
    if x = 0
      if p_Encoded = Net:Inline
        done = self._AddSetting(p_settings[p_Start : p_End],p_OnlySessionID,p_Plus)
      else
        done = self._ParsePart(p_settings,p_Start,p_End,p_OnlySessionID)
      end
      break
    else
      if p_Encoded = Net:Inline
        done = self._AddSetting(p_settings [p_start : p_start-1+x-1],p_OnlySessionID,p_Plus)
      else
        done = self._ParsePart(p_settings,p_start,p_start-1+x-1,p_OnlySessionID)
      end
      p_start += x+y-1
    end
    if done then break.
  end
  if self.RequestReferer = '-1' and self.IfExistsValue('referer') > 0
    self.RequestReferer = self.GetValue('referer')
  end
  return Done

!------------------------------------------------------------------------------
NetWebServerWorker._SendPhpFile PROCEDURE (string p_FileName, Long p_header=NET:SendHeader)
  Compile('***',_ODDJOB_=1)
qPos                long
strData             StringTheory
jobControl          JobObject           ! Job and process creation and management
loc:filename    string(255)
  ***
  Code
  Omit('***',_ODDJOB_=1)
  self.SendError (501, 'Not Implemented', 'PHP Support not activated on this server.') ! <!-- filename = ' & clip(p_FileName) & ' current path = ' & clip(longpath()) & ' -->' )
  return
  ***
  Compile('***',_ODDJOB_=1)
        putIni('PHP','doc_root',Clip(self.site.WebFolderPath),clip(self.site.PhpPath) & '\php.ini')

        jobControl.Init()
                                                                        !jobControl.Debug('WebHandler(): Set up the environment and call CreateProcess')
        ! Set CGI environment variables
        jobControl.ClearEnvironment()

        jobControl.AddEnv('SERVER_SOFTWARE', 'NetTalkWebServer')        ! The name and version of the information server software answering the request (and running the gateway). Format: name/version

        !jobControl.AddEnv('REDIRECT_STATUS', '200 OK')                 ! Required for php-cgi to allow the document to execute (it prevents direct access to files)

        !Get(self.RequestData.WebServer._SitesQueue, self.RequestData.SiteID)
        !if not ErrorCode()
        !    jobControl.AddEnv('SERVER_NAME', self.requestData.WebServer._SitesQueue.HostName) ! The server's hostname, DNS alias, or IP address as it would appear in self-referencing URLs.
        !else
        !    jobControl.ErrorTrap('Could not set the SERVER_NAME environment variable, could not find the NetWebServer Site relating to the request', JobError:Unknown)
        !end

        jobControl.AddEnv('GATEWAY_INTERFACE', 'CGI/1.1')               ! The revision of the CGI specification to which this server complies. Format: CGI/revision
        jobControl.AddEnv('DOCUMENT_ROOT', self.site.WebFolderPath)                       ! Not included in the standard CGI specification, the root directory that files are served from
        jobControl.AddEnv('SERVER_PROTOCOL', self.HeaderDetails.HTTP)   ! The name and revision of the information protcol this request came in with. Format: protocol/revision
        jobControl.AddEnv('SERVER_PORT', self.RequestData.WebServer.port) ! The port number to which the request was sent.

        if self.RequestData.RequestMethodType = NetWebServer_POST
            jobControl.AddEnv('REQUEST_METHOD', 'POST')                 ! The method with which the request was made. For HTTP, this is "GET", "HEAD", "POST", etc.
        else
            jobControl.AddEnv('REQUEST_METHOD', 'GET')
        end

        !jobControl.AddEnv('REQUEST_URI', self.WholeURL)                ! Not a standard CGI env variable, excluded for now (the full URL). Could be seriously long.

        ! script-URI = <scheme> "://" <server-name> ":" <server-port>
        !           <script-path> <extra-path> "?" <query-string>

        jobControl.AddEnv('PATH_TRANSLATED', self.RequestFileName)      ! The server provides a translated version of PATH_INFO, which takes the path and does any virtual-to-physical mapping to it.


        ! A virtual path to the script being executed, used for
        ! self-referencing URLs (the path part of the URL).
        ! Need to set this to the just the path and page, e.g. /php/test.php
        if Instring(Clip(self.site.WebFolderPath), self.RequestFileName, 1, 1) ! Strip the actual folder name if neccessary
            jobControl.AddEnv('SCRIPT_NAME', self.RequestFileName[Len(Clip(self.site.WebFolderPath)) + 1 : Len(self.RequestFileName)])
        else
            jobControl.AddEnv('SCRIPT_NAME', self.RequestFileName)
        end

        ! The extra path information, as given by the client. In
        ! other words, scripts can be accessed by their virtual pathname, followed
        ! by extra information at the end of this path. The extra information is
        ! sent as PATH_INFO. This information should be decoded by the server if it
        ! comes from a URL before it is passed to the CGI script.
        jobControl.AddEnv('PATH_INF0', self.PageName)

        ! The information which follows the ? in the URL  which referenced this script.
        ! This is the query information. It should not be decoded in any fashion. This
        ! variable should always be set when there is query information, regardless of
        ! command line decoding
        qPos =  Instring('?', self.WholeURL, 1, 1)
        if qPos
            jobControl.AddEnv('QUERY_STRING', self.WholeURL[qPos+1 : Len(Clip(self.WholeURL))])
        end

        ! The hostname making the request. If the server does not have this
        ! information, it should set REMOTE_ADDR and leave this unset
        ! jobControl.AddEnv('REMOTE_HOST', remoteHostName)

        jobControl.AddEnv('REMOTE_ADDR', self.requestdata.fromip)       ! The IP address of the remote host making the request.

        ! If this is a GET, then set the environment variables and call the
        ! php-cgi interface. If it is a POST, then the body of the post also
        ! needs to be passed to the StdIn of the php-cgi interface
        if self.RequestData.RequestMethodType = NetWebServer_POST
            ! Get the body from the posted data
            qPos = instring('<13,10,13,10>', self.RequestData.DataString, 1, 1)
            if qPos and qPos + 4 < self.RequestData.DataStringLen
                strData.Assign(self.RequestData.DataString[qPos + 4 : self.RequestData.DataStringLen])
                jobControl.AddEnv('CONTENT_LENGTH', strData.Len())
                ! If there is a message-body then the CONTENT_TYPE variable is set to the Internet Media Type (for example 'application/x-www-form-urlencoded').
                jobControl.AddEnv('CONTENT_TYPE', self.GetHeaderField('Content-Type:'))
            else
                jobControl.Debug('ERROR: Post with no body received!')
                jobControl.AddEnv('CONTENT_LENGTH', '0')
            end
        end

        jobControl.AddEnv('HTTP_USER_AGENT', self.GetHeaderField('User-agent:'), true)
        jobControl.AddEnv('HTTP_COOKIE', self.GetHeaderField('Cookie:'), true)
        jobControl.AddEnv('HTTP_ACCEPT', self.GetHeaderField('Accept:'), true)
        jobControl.AddEnv('HTTP_ACCEPT_ENCODING', self.GetHeaderField('Accept-Encoding:'), true)
        jobControl.AddEnv('HTTP_ACCEPT_LANGUAGE', self.GetHeaderField('Accept-Language:'), true)
        jobControl.AddEnv('HTTP_CONNECTION', self.GetHeaderField('Connection:'), true)
        if not exists(clip(self.site.PhpPath) & '\php-cgi.exe')
            self.AddLog('Failed to find php exe using "' & clip(self.site.PhpPath) & '\php-cgi.exe".')
        elsif not jobControl.CreateProcess('"' & clip(self.site.PhpPath) & '\php-cgi.exe" "'&clip(self.site.WebFolderPath)&'\' & Clip(self.PageName) & '"', jo:SW_HIDE, false, , , , , strData)
            self.AddLog('Failed to process the PHP file "' & Clip(p_Filename) & '" using "' & clip(self.site.PhpPath) & '\php-cgi.exe". Error ' & jobControl.errorCode & ': ' & Clip(jobControl.errorMsg))
            !--- Error handling here...
        else
            ! The returned data includes additional PHP headers, which need
            ! Split off and added to the standard headers.
            qPos = strData.Instring('<13,10><13,10>')
            if qPos and qPos + 4 < strData.Len()
                self.HeaderDetails.Php = strData.Sub(1, qPos-1)
                strData.SetValue(strData.Slice(qPos + 4, strData.LengthA()))                  ! Extract the body
            end
            self.ParseHTML(strData.value, 1, strData.LengthA(),p_header)             ! ParseHTML (*String p_Data, long p_DataStart=1, long p_DataEnd=0, Byte p_Header=NET:SendHeader)

        end
        jobControl.Kill()
  ***
!------------------------------------------------------------------------------
NetWebServerWorker._SendFile PROCEDURE  (string p_FileName, Long p_header=NET:SendHeader)
local            group, pre(loc)
parse              long
FileSize           long
cFileName          cstring(260)
CheckSize          long
result             long
                 end

BufferData           StringTheory
loc:filename         string(256)
st                   StringTheory

  CODE
  loc:filename = self.DefaultName(p_filename)
  !self._trace('XXX [_SendFile]  self.pagename=[' & clip(self.pagename) & '] ' & len(clip(self.pagename)) & ' pf=' & clip(p_filename) & ' self.Ajax=' & self.Ajax)
  if (self.pagename = 'SetSessionValue' or |
      instring('_xxx_',self.pagename,1,1) or |
      instring('_tabchanged',self.pagename,1,1)) and self.Ajax = 1
    !self._trace('xxx noted - thread will do mostly nothing')
    do SSV
    do Nothing
    do DisposeAndReturn
  elsif self.pagename = 'redactorImageUpload' and self.GetValue('File')
    st.SetValue(self.GetValue('File'))
    st.Replace('\','/')
    self.SendJSON('{{ "filelink": "'&st.GetValue()&'" }')
    do DisposeAndReturn
  elsif self.pagename = 'redactorFileUpload' and self.GetValue('File')
    st.SetValue(self.GetValue('File'))
    st.Replace('\','/')
    self.SendJSON('{{ "filelink": "'&st.GetValue()&'", "filename": "Friendly Name" }')
    do DisposeAndReturn
  End
  ! generic blobs not yet working, see example 40 for code in web handler.
  !if p_header = NET:SendHeader
    !if self._SendBlob(loc:filename) = Net:BlobSent
    !  return
    !End
  !end

  if self.ValidateFileName(loc:filename) <> 0
    ! Something was wrong in the filename
    self.SendError (400, 'Bad Request', 'The requested file was not valid') ! Bad Request
    return
  end

  if Instring('.php', Lower(loc:filename), 1, 1) or self.SendViaPhp
    Self._SendPhpFile(loc:filename,p_header)
    Do DisposeAndReturn
  End

  loc:parse = self.FileType(loc:filename) ! check the file type e.g. .htm .gif .jpg - must we parse?

  ! always serve files in uploads folder as attachments
  self.SetContentAttachment(loc:filename)

  ! --- Get the File
  loc:result = self.ReadFile (loc:filename, BufferData, self.FileDate, self.FileTime,loc:parse)
  if loc:result <> 0
    if self.Ajax = 1 and (records(self._ResponseQueue) or self._headerSent = 1)
    else
      self._trace('Sendfile error 404, Error '&loc:result&' filename=' & clip(loc:filename))
      self.SendError (404,'The page cannot be found', 'The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.')
    end
    Do DisposeAndReturn ! No file to send
  end
  ! --- Got the File
  !if a pre-compressed file, then just send it
  if self.ReplyContentEncoding = 'gzip'
    if self._RequestIsCachedAtBrowser(self.FileDate, self.FileTime)
      self.SendError(304,'Not Modified','',0)
    else
      self._flushSend(BufferData,net:AlreadyCompressed)
    end
    Do DisposeAndReturn
  End
  !
  if loc:Parse = 1
    if self.site._CheckForParseHeader = 1
      if (BufferData.LengthA() < self.site._CheckForParseHeaderSize) | ! Use the smallest
          or (self.site._CheckForParseHeaderSize = 0)          ! if .CheckForParseHeaderSize = 0, then check in whole file
        loc:CheckSize = BufferData.LengthA()
      else
        loc:CheckSize = self.site._CheckForParseHeaderSize
      end
      if Bufferdata.Instring(clip(self.site._CheckParseHeader),1,1,loc:CheckSize) = 0
        loc:Parse = 0
      end
    end
  end

  if p_header = Net:NoHeader ! called when using f: tag
    self.ParseHTML(Bufferdata,,,p_header)
  elsif loc:Parse
    self.ParseHTML(Bufferdata)
  else
    !self._SendMemory(loc:BufferData.Buffer, loc:BufferData.BufferSize)
    if self._RequestIsCachedAtBrowser(self.FileDate, self.FileTime)
      self.SendError(304,'Not Modified','',0)
    else
      case lower(right(loc:filename,4))
      of '.png' orof '.jpg' orof '.zip' orof '.rar' orof '.saf'
        self._flushSend(BufferData,net:AlreadyCompressed)
      else
        case lower(right(loc:filename,3))
        of '.gz' orof '.7z'
          self._flushSend(BufferData,net:AlreadyCompressed)
        else
          self._flushSend(BufferData,self.site.CompressStatic)   ! file off the disk.
        end
      end
    end
  end

  Do DisposeAndReturn

SSV  routine
  data
loc:n                string(80)
loc:v                string(255)
loc:url              string(255)
x                    long
y                    long
packet               string(255)
  code
  if self.WholeURL &= Null then exit.
  loc:url = self.WholeURL
  x = instring('?',loc:url,1,1)
  if x = 0 then exit.
  loc:url = sub(loc:url,x+1,255)
  loop
    x = instring('=',loc:url,1,1)
    if x = 0 then break.
    loc:n = sub(loc:url,1,x-1)
    y = instring('&',loc:url,1,1)
    if y = 0 then y = len(clip(loc:url))+1.
    loc:v = sub(loc:url,x+1,y-x-1)
    self.SetSessionValue(loc:n,loc:v,'')
    loc:url = sub(loc:url,y+1,255)
    Self.SessionValueSetFromBrowser(loc:n,loc:v)
  end

Nothing routine
  if self._headerSent = 0
    self.DivHeader('_a_b_c_nothing',,net:send)
    self.DivFooter(net:send)
  end

DisposeAndReturn Routine
  self.RemoveOneServe(loc:filename)
  return
!------------------------------------------------------------------------------
NetWebServerWorker.RemoveOneServe  Procedure(string p_FileName)
  CODE
  if instring ('\$$$',p_FileName,1,1)
    remove(p_FileName)
    self.trace('error: ' & error() & ' ' & errorcode())
  End

!------------------------------------------------------------------------------
NetWebServerWorker.SessionValueSetFromBrowser   Procedure(string p_Name, string p_Value)
  code
  ! pure virtual, called when the javascript SetSessionValue function is called.
!------------------------------------------------------------------------------
NetWebServerWorker._SendBlob  Procedure(string p_FileName)
ans            Long
loc:keyfield   Any
loc:blobfield  &Blob
loc:FileName   String(256)
x              Long
s              Long
SendString     &String
  code
  ans = Net:BlobNotSent
  if self.blobfile &= null then return ans.
  if self.blobkey &= null then return ans.
  self._openfile(self.blobfile)
  loc:keyfield = self._FindKeyField(self.blobfile,self.blobkey)

  if loc:FileName[1] = '/' then loc:FileName = sub(loc:FileName,2,255).
  loop x = 1 to len(clip(loc:FileName))
    if loc:FileName[x] = '?' then break.
    if loc:FileName[x] = '/' then loc:FileName = '\'.
  end
  loc:keyfield = loc:FileName
  self._getfile(self.blobfile,self.blobkey)
  If Errorcode() = 0
    ! determining the size of the blob is a problem
    s = self.blobfile{prop:size,-1} !BF:Bin{prop:size}
    sendstring &= new(string(s))
    sendstring = self.blobfile{prop:value,-1} !BF:Bin[1: s]
    ! allows the browser to cache this file.
    self.ForceNoCache = 0
    self.HeaderDetails.CacheControl = ''
    self.HeaderDetails._Pragma = ''
    self.FileDate = today() - 200
    self.FileTime = 6000
    ! end of cache settings
    self._SendMemory(sendString,s)
    dispose(sendString)
    ans = Net:BlobSent
  End
  self._closefile(self.blobfile)
  return ans

!------------------------------------------------------------------------------
NetWebServerWorker.SetHeader200 Procedure(Long p_cache=net:Cache)
  code
      !self.HeaderDetails.ContentLength is set before, or after, calling this
      self.HeaderDetails.ResponseNumber = '200'
      self.HeaderDetails.ResponseString = 'OK'
      self.HeaderDetails.Date = today()
      self.HeaderDetails.Time = clock()
      self.HeaderDetails.ContentType = self.ReplyContentType
      self.HeaderDetails.ContentEncoding = self.ReplyContentEncoding
      if self.ForceNoCache > 0  or p_cache = net:DontCache
        self.HeaderDetails.CacheControl = 'no-store, no-cache, must-revalidate, private,post-check=0, pre-check=0, max-age=0 '
        self.HeaderDetails._Pragma = 'no-cache'
        self.HeaderDetails.ExpiresDate = today() - 365
        self.HeaderDetails.ExpiresTime = clock()
      end
      self._SetFileDateHeaders (self.HeaderDetails, self.FileDate, self.FileTime) ! This will add header optimizations so the the Web Browser, can use it's cache on these pages.
      self.ReplyHeaderString = self.CreateHeader(self.HeaderDetails) & '<13,10>'

!------------------------------------------------------------------------------
NetWebServerWorker._SendMemory PROCEDURE  (*string p_String,Long p_Length)
  CODE
    if self._HttpHeaderSent = 0 and self.site.CompressDynamic = 0 and self.site.ReuseConnections = 0
      self.SendString (p_String, 1, p_Length, NET:SendHeader)
    else
      self.SendString (p_String, 1, p_Length, NET:NoHeader)
    end

!------------------------------------------------------------------------------

NetWebServerWorker._TranslateFileName PROCEDURE  (string p_String)
  CODE
  if p_String = '\'
    return (clip(self.site.DefaultPage))
  end
  return clip(p_String) ! Default - remains the same
!------------------------------------------------------------------------------

NetWebServerWorker._UnEscape PROCEDURE  (string p_String, Long p_Flags=0)
! html uses 2 conventions for "special" characters
! first, spaces are replaced with a +
! also any char can become %ab where ab is a hex (ascii) number
! Special Case : a colon is valid in clarion, but not in javascript. So : are translated to __ via the
!                _nocolon method. Thus here we translate __ back into :

! Flags:  Net:NoPlus:  + are not turned into spaces.
!         Net:NoHex:   %xx are not turned into characters.
!         Net:NoColon: __ are not turned into :


c          byte
x          long
z          long
  CODE
  if band(p_Flags,Net:NoPlus) = 0
    ! check for +
    x = instring('+',p_string,1,1)
    loop while x
      p_string[x] = ' '
      x = instring('+',p_string,1,x+1)
    end
  end
  if band(p_Flags,Net:NoHex) = 0
    ! check for ascii hex numbers
    x = instring('%',p_string,1,1)
    z = size(p_string)
    loop while x
      do MakeC
      p_string = sub(p_string,1,x-1) & chr(c) & sub(p_string,x+3,z-x-2)
      x = instring('%',p_string,1,x+1)
    end
  End
  if band(p_Flags,Net:NoColon) = 0
    ! check for __
    x = instring('__',p_string,1,1)
    z = size(p_string)
    loop while x
      p_string = sub(p_string,1,x-1) & ':' & sub(p_string,x+2,z-x-1)
      x = instring('__',p_string,1,1)
    end
  End
  return clip(p_string)

!-----------------
MakeC  routine
  If x+2 > len(p_string) then exit.
  case p_string[x+1]
  of 0 to 9
    c = val(p_string[x+1]) - 48 ! 0
  of 'A' to 'F'
    c = val(p_string[x+1]) - 55
  of 'a' to 'f'
    c = val(p_string[x+2]) - 87 ! a=10
  end
  c = c * 16
  case p_string[x+2]
  of 0 to 9
    c = c + val(p_string[x+2]) - 48
  of 'A' to 'F'
    c = c + val(p_string[x+2]) - 55
  of 'a' to 'f'
    c = c + val(p_string[x+2]) - 87 ! a=10
  end

!------------------------------------------------------------------------------

NetWebServerWorker._SetFileDateHeaders PROCEDURE  (*NetWebServerHeaderDetailsType p_HeaderDetails, long p_FileDate, long p_FileTime)
  CODE
  if p_FileDate <= 4
    return
  end
  p_HeaderDetails.LastModifiedDate = p_FileDate
  p_HeaderDetails.LastModifiedTime = p_FileTime
  p_HeaderDetails.ETag = self._CreateETag (p_FileDate, p_FileTime)

!------------------------------------------------------------------------------

NetWebServerWorker._RequestIsCachedAtBrowser PROCEDURE  (long p_FileDate, long p_FileTime) ! Declare Procedure 3
tempDate    long
tempTime    long
strlen      long
  CODE
  if p_FileDate <= 4
    return (false)
  end
  if self.IfModifiedSince <> ''
    self._SpiltTime (self.IfModifiedSince, tempDate, tempTime)
    if tempDate = p_FileDate and tempTime = p_FileTime
      if self.LoggingOn
        self.Log ('NetWebServerWorker._RequestIsCachedAtBrowser', 'Web Browser has a file matching the date. (304)')
      end
      do Send304
      return (true)
    end
  end
  if self.IfNoneMatch <> ''
    if self.IfNoneMatch = self._CreateETag (p_FileDate, p_FileTime)
      if self.LoggingOn
        self.Log ('NetWebServerWorker._RequestIsCachedAtBrowser', 'Web Browser has a file matching the ETag. (304)')
      end
      do Send304
      return (true)
    end
  end
  return (false)

! sends a complete 304 header instead of the file. Called from _SendFile.
Send304  routine
      self.HeaderDetails.ResponseNumber = '304'
      self.HeaderDetails.ResponseString = 'Not Modified'
      self.HeaderDetails.Date = today()
      self.HeaderDetails.Time = clock()
      self.HeaderDetails.ContentType = self.ReplyContentType
      self.HeaderDetails.ContentLength = 0
      self.HeaderDetails.Connection = 'close'
      self._SetFileDateHeaders (self.HeaderDetails, self.FileDate, self.FileTime) ! This will add header optimizations so the the Web Browser, can use it's cache on these pages.

!------------------------------------------------------------------------------

NetWebServerWorker._CreateETag PROCEDURE  (long p_FileDate, long p_FileTime) ! Declare Procedure 3
  CODE
  return ('"' & p_FileDate & ':' & p_FileTime & '"')
!------------------------------------------------------------------------------

NetWebServerWorker._SpiltTime PROCEDURE  (*string p_DateTimeStr, *long p_Date, *long p_Time) ! Declare Procedure 3
s                   long
e                   long
x                   long
d                   long
m                   long
y                   long
sl                  long
hour                long
min                 long
sec                 long
tempTime            string (10)
TimeZone            string (5)
t1                  long
t2                  long
MinDiff             long ! Minutes Difference (from TimeZone)
ONE_DAY             equate (8640000)

  CODE
  ! Fri, 8 Oct 2004 16:54:48 +0200
  ! Fri, 08 Oct 2004 16:54:48 +0200

  sl = len (clip (p_DateTimeStr))
  s = instring (',', p_DateTimeStr)
  s += 1
  x = s
  loop
    if s > sl
      break
    end
    if sub (p_DateTimeStr, s, 1) <> ' '
      break
    end
    s += 1
  end
  e = instring (' ', p_DateTimeStr, 1, s+1)
  if e = 0
    return
  end
  d = sub (p_DateTimeStr, s, (e-s))

  s = e
  loop
    if s > sl
      break
    end
    if sub (p_DateTimeStr, s, 1) <> ' '
      break
    end
    s += 1
  end
  e = instring (' ', p_DateTimeStr, 1, s+1)
  if e = 0
    return
  end
  case (lower(sub (p_DateTimeStr, s, 3)))
  of 'jan'; m = 1
  of 'feb'; m = 2
  of 'mar'; m = 3
  of 'apr'; m = 4
  of 'may'; m = 5
  of 'jun'; m = 6
  of 'jul'; m = 7
  of 'aug'; m = 8
  of 'sep'; m = 9
  of 'oct'; m = 10
  of 'nov'; m = 11
  of 'dec'; m = 12
  else
    return
  end

  s = e
  loop
    if s > sl
      break
    end
    if sub (p_DateTimeStr, s, 1) <> ' '
      break
    end
    s += 1
  end
  e = instring (' ', p_DateTimeStr, 1, s+1)
  if e = 0
    return
  end
  y = sub (p_DateTimeStr, s, (e-s))

  s = e
  loop
    if s > sl
      break
    end
    if sub (p_DateTimeStr, s, 1) <> ' '
      break
    end
    s += 1
  end
  e = instring (':', p_DateTimeStr, 1, s+1)
  if e = 0
    return
  end
  hour = sub (p_DateTimeStr, s, (e-s))

  s = e+1
  e = instring (':', p_DateTimeStr, 1, s+1)
  if e = 0
    return
  end
  min = sub (p_DateTimeStr, s, (e-s))

  s = e+1
  e = instring (' ', p_DateTimeStr, 1, s+1)
  if e = 0
    e = sl+1
  end
  sec = sub (p_DateTimeStr, s, (e-s))

  p_Date = date (m, d, y)
  if p_Date = 0
    p_Time = 0
    return
  end
  p_Time = 1 + sec * 100 + min * 6000 + hour * 360000

  s = e+1
  loop
    if s > sl
      break
    end
    if sub (p_DateTimeStr, s, 1) <> ' '
      break
    end
    s += 1
  end
  e = instring (' ', p_DateTimeStr, 1, s+1)
  if e = 0
    e = instring (';', p_DateTimeStr, 1, s+1)
    if e = 0
      e = sl
    end
  else
    x = instring (';', p_DateTimeStr, 1, s+1)
    if x > 0 and x < e
      e = x
    end
  end
  TimeZone = sub (p_DateTimeStr, s, (e-s))
  if TimeZone[1:3] = 'GMT'
    TimeZone = '+0000'
  end

  t1 = TimeZone [2:3]
  t2 = TimeZone [4:5]
  MinDiff = t1 * 60 + t2
  if TimeZone[1] = '-'
    p_Time = p_Time + MinDiff*6000
  else
    p_Time = p_Time - MinDiff*6000
  end
  if p_Time < 0
    p_Time += ONE_DAY
    p_Date -= 1
  end
  if p_Time > ONE_DAY
    p_Time -= ONE_DAY
    p_Date += 1
  end
  ! Now the time is in GMT

  ! Now convert to our time format
  TimeZone = NetGetTimeZone()
  t1 = TimeZone [2:3]
  t2 = TimeZone [4:5]
  MinDiff = t1 * 60 + t2
  if TimeZone[1] = '-'
    p_Time = p_Time - MinDiff*6000
  else
    p_Time = p_Time + MinDiff*6000
  end
  if p_Time < 0
    p_Time += ONE_DAY
    p_Date -= 1
  end
  if p_Time > ONE_DAY
    p_Time -= ONE_DAY
    p_Date += 1
  end


!------------------------------------------------------------------------------

NetWebServerWorker._GetFileTime PROCEDURE  (long p_hFile, *long p_Date, *long p_Time)
loc:CreationTime    group(OS_FILETIME).
loc:LastAccessTime  group(OS_FILETIME).
loc:LastWriteTime   group(OS_FILETIME).
loc:SystemTime      group(OS_SYSTEMTIME).
TimeZone            string (5)
t1                  long
t2                  long
MinDiff             long ! Minutes Difference (from TimeZone)
ONE_DAY             equate (8640000)
result              long

  CODE
  result = OS_GetFileTime(p_hFile, address(loc:CreationTime), address(loc:LastAccessTime), address(loc:LastWriteTime))
  result = OS_FileTimeToSystemTime(address(loc:LastWriteTime), address(loc:SystemTime))
  p_Date = Date (loc:SystemTime.wMonth, loc:SystemTime.wDay, loc:SystemTime.wYear)
  p_Time = 1 + loc:SystemTime.wHour*360000 + loc:SystemTime.wMinute*6000 + loc:SystemTime.wSecond*100

  ! Convert from GMT to our TimeZone
  TimeZone = NetGetTimeZone()
  t1 = TimeZone [2:3]
  t2 = TimeZone [4:5]
  MinDiff = t1 * 60 + t2
  if TimeZone[1] = '-'
    p_Time = p_Time - MinDiff*6000
  else
    p_Time = p_Time + MinDiff*6000
  end
  if p_Time < 0
    p_Time += ONE_DAY
    p_Date -= 1
  end
  if p_Time > ONE_DAY
    p_Time -= ONE_DAY
    p_Date += 1
  end

!------------------------------------------------------------------------------
NetWebServerWorker.MakePage     PROCEDURE  (<String p_page>,Long p_Type=0, Long p_Special=0, <STRING p_Title>,<String p_Header>, <string p_footer>)
str            StringTheory
loc:Page       StringTheory
loc:header     StringTheory
loc:footer     StringTheory
  CODE
  If Band(p_Special, Net:Secure) and self.RequestData.WebServer.SSL = 0
    self.Redirect('',Net:Web:ToSecure,Net:Web:Repost,Net:Web:PermRedirect)
    Return
  End

  if omitted(2) or p_page = ''!(p_page)
    loc:Page.SetValue('<!-- Net:' & lower(self.getvalue('type')) & ' -->')
  elsif p_page = ''
    loc:Page.SetValue('<!-- Net:' & lower(self.getvalue('type')) & ' -->')
  else
    loc:Page.SetValue('<!-- Net:' & lower(clip(p_Page)) & ' -->')
  end

  if not self.IsMobile()
    if omitted(6) or p_header = ''
      loc:header.SetValue(self.site.PageHeaderTag,st:clip)
    else
      loc:header.SetValue(p_header,st:clip)
    end
    if omitted(7) or p_footer = ''
      loc:footer.SetValue(self.site.PageFooterTag,st:clip)
    else
      loc:footer.SetValue(p_footer,st:clip)
    end
  end

  If Band(p_Special, Net:Login) and self.GetSessionLoggedIn() = 0
    If not (Self.WholeURL &= Null)
      self.SetValue('ChainTo',self.WholeURL)
    End
    self.SetSessionValue('_ChainRefer',self.RequestReferer)
    self._SendFile(self.site.LoginPage)
    Return
  End
  if self.sessionId = ''
    self.NewSession()
  end
  if not omitted(5)  !(p_title)
    self.site.pagetitle = self.translate(p_Title)
  End
  if self.Ajax = 1
    str.SetValue(loc:Page.GetValue())
    self.ParseHTML(str,1,str.length(),NET:NoHeader)
    self.Sendfooter(3)
    str.free()
  else
    str.SetValue( clip(self.site._CheckParseHeader) & self.CRLF &|
                    '<!-- Net:CommonHeader -->' &|
                    self.BodyOnLoad(self.site.BodyClass,,self.site.BodyDivClass) &|
                    loc:header.GetValue() &|
                    '<!-- Net:Busy -->' &|
                    '<!-- Net:Message -->' &|
                    loc:Page.GetValue()  &|
                    loc:footer.GetValue() &|
                    '<!-- Net:SelectField -->') ! &|
                    ! '</div><!-- body_div --> ' & self.CRLF)
    self._inMakePage = 1
    self.ParseHTML(str)
    self.FlushResponses()
    str.SetValue('</div><!-- body_div --> ' & self.CRLF)
    str.append('</body>' & self.CRLF & '</html>' & self.CRLF)
    self.ParseHTML(str)
    self._inMakePage = 0
  end
  if self._sentbytes = 0 and self.Ajax = 1 and records(self._responseQueue) = 0
    self.AddResponse('a_b_c','_a_b_c_nothing')
  end

!------------------------------------------------------------------------------
NetWebServerWorker.GetSecwinSettings      PROCEDURE  (string p_Procedure)
  CODE
!------------------------------------------------------------------------------
NetWebServerWorker.CallForm               PROCEDURE  (FILE p_file, LONG p_Stage)
Ans  Long
  CODE
  Ans = 0 ! default ans is now 0.
  Return Ans
!------------------------------------------------------------------------------
NetWebServerWorker._SelectField           PROCEDURE  ()
fieldname  string(255)
x          long
  Code
  fieldname = self.GetValue('SelectField')
  if fieldname
    x = instring('.',fieldname,1,1)
    if x
      fieldname = sub(fieldname,x+1,255)
    end
    self.Script('$(''#' & self._noColon(fieldname) &''').focus();')
    self.SetValue('SelectField','')
  End
  return

!------------------------------------------------------------------------------
NetWebServerWorker.IncludeScripts                PROCEDURE()
Ans  StringTheory
  Code
  ans.SetValue(choose(self.IsMobile(),self.site.HtmlCommonScriptsMobile,self.site.HtmlCommonScripts),st:Clip)
  Case lower(self.useragent)
  of 'msie'
    ans.Append(self.site.HtmlMSIE6Scripts,st:Clip)
  of 'msie7'
    ans.Append(self.site.HtmlMSIE7Scripts,st:Clip)
  orof 'msie8'
    ans.Append(self.site.HtmlMSIE8Scripts,st:Clip)
  orof 'msie9'
    ans.Append(self.site.HtmlMSIE9Scripts,st:Clip)
  orof 'msie10'
    ans.Append(self.site.HtmlMSIE10Scripts,st:Clip)
  of 'firefox'
    ans.Append(self.site.HtmlFireFoxScripts,st:Clip)
  of 'opera'
    ans.Append(self.site.HtmlOperaScripts,st:Clip)
  of 'safari'
    ans.Append(self.site.HtmlSafariScripts,st:Clip)
  of 'chrome'
    ans.Append(self.site.HtmlChromeScripts,st:Clip)
  of 'mozilla'
    ans.Append(self.site.HtmlMozillaScripts,st:Clip)
  of 'other'
    ans.Append(self.site.HtmlOtherScripts,st:Clip)
  end
  Return Ans.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker.IncludeStyles                 PROCEDURE()
Ans  StringTheory
  code
  ans.SetValue(choose(self.IsMobile(),self.site.HtmlCommonStylesMobile,self.site.HtmlCommonStyles),st:Clip)
  if self.IsMobile()
    if self.site.GoogleFontsMobile
      if self.requestdata.webserver.SSL
        ans.Append('<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?' & clip(self.site.GoogleFontsMobile) & '" />'  & self.CRLF)
      else
        ans.Append('<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?' & clip(self.site.GoogleFontsMobile) & '" />'  & self.CRLF)
      end
    end
  else
    if self.site.GoogleFonts
      if self.requestdata.webserver.SSL
        ans.Append('<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?' & clip(self.site.GoogleFonts) & '" />' & self.CRLF)
      else
        ans.Append('<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?' & clip(self.site.GoogleFonts) & '" />' & self.CRLF)
      end
    end
  end
  Case lower(self.useragent)
  of 'msie'
    ans.append(self.site.HtmlMSIE6Styles,st:Clip)
  of 'msie7'
    ans.append(self.site.HtmlMSIE7Styles,st:Clip)
  of 'msie8'
    ans.append(self.site.HtmlMSIE8Styles,st:Clip)
  of 'msie9'
    ans.append(self.site.HtmlMSIE9Styles,st:Clip)
  of 'msie10'
    ans.append(self.site.HtmlMSIE10Styles,st:Clip)
  of 'firefox'
    ans.append(self.site.HtmlFireFoxStyles,st:Clip)
  of 'opera'
    ans.append(self.site.HtmlOperaStyles,st:Clip)
  of 'safari'
    ans.append(self.site.HtmlSafariStyles,st:Clip)
  of 'chrome'
    ans.append(self.site.HtmlChromeStyles,st:Clip)
  of 'mozilla'
    ans.append(self.site.HtmlMozillaStyles,st:Clip)
  of 'other'
    ans.append(self.site.HtmlOtherStyles,st:Clip)
  end
  Return ans.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker.SetCustomHTMLHeaders           PROCEDURE  ()
  Code
!------------------------------------------------------------------------------
NetWebServerWorker.CommonHeader           PROCEDURE  (String p_Title)
str  StringTheory
  CODE
  str.SetValue(self.w3Header(self.site.HtmlClass) & '<head>' & self.CRLF &|
                   '<meta http-equiv="Content-Type" content="text/html; charset='& |
                   clip(self.site.HtmlCharset)&'" />' & self.CRLF)
  if instring('msie',lower(self.useragent),1,1)
    str.append('<meta http-equiv="X-UA-Compatible" content="IE=10" />' & self.CRLF) |
  end
  if self.IsMobile()
    str.append('<meta name="viewport" content="width=device-width, initial-scale=1">' & self.CRLF)
  end
  self.SetCustomHTMLHeaders()
  str.append(self.MetaHeaders,st:clip)
  str.append(self.IncludeStyles())
  str.append(self.IncludeScripts())
  if p_Title = ''
    str.append('<title>'&clip(self.site.PageTitle)&'</title>' & self.CRLF&'</head>' & self.CRLF)
  else
    str.append('<title>'&clip(p_Title)&'</title>' & self.CRLF&'</head>' & self.CRLF)
  end
  if str.length()
    Self.SendString (str, 1, str.length(), NET:NoHeader)
  end
  Return

!------------------------------------------------------------------------------
NetWebServerWorker.ProcessFormCancel      PROCEDURE  (FILE p_file,KEY p_key)
  CODE
  If p_FILE &= NULL then Return.
  If self._LoadRecord(p_File,p_Key,self.FormSettings.recordID) = 0
    self.FileToSessionQueue(p_File) ! only handles up to single dimensioned arrays
  End

!------------------------------------------------------------------------------
NetWebServerWorker.SubmitButton           PROCEDURE  ()
ReturnValue           string(32)
temp                  string(32)
  CODE
    If self.IfExistsvalue('pressedbutton') <> ''
      do MakeValue
      self.DeleteValue('pressedbutton')
    End
    If self.IfExistsvalue('save_btn') <> ''
      ReturnValue = 'save'
    Elsif self.IfExistsvalue('cancel_btn') <> ''
      ReturnValue = 'cancel'
    Elsif self.IfExistsvalue('close_btn') <> ''
      ReturnValue = 'close'
    ElsIf self.IfExistsValue('select_btn') <> ''
      ReturnValue = 'select'
    Elsif self.IfExistsValue('insert_btn') <> ''
      ReturnValue = 'insert'
    Elsif self.IfExistsValue('copy_btn') <> ''
      ReturnValue = 'copy'
    Elsif self.IfExistsValue('change_btn') <> ''
      ReturnValue = 'change'
    Elsif self.IfExistsValue('view_btn') <> ''
      ReturnValue = 'view'
    Elsif self.IfExistsValue('deleteb_btn') <> ''
      ReturnValue = 'deleteb'
    Elsif self.IfExistsValue('deletef_btn') <> ''
      ReturnValue = 'deletef'
    Elsif self.IfExistsvalue('first_btn') <> ''
      ReturnValue = 'first'
    Elsif self.IfExistsvalue('previous_btn') <> ''
      ReturnValue = 'previous'
    Elsif self.IfExistsvalue('next_btn') <> ''
      ReturnValue = 'next'
    Elsif self.IfExistsvalue('last_btn') <> ''
      ReturnValue = 'last'
    Elsif self.IfExistsvalue('lookup_btn') <> ''
      ReturnValue = 'lookup'
    Elsif self.IfExistsvalue('logout_btn') <> ''
      ReturnValue = 'logout'
    Elsif self.IfExistsvalue('browsecancel_btn') <> ''
      ReturnValue = 'browsecancel'
    End
    Return Clip(ReturnValue)

MakeValue  routine
  self.DeleteValue('save_btn')
  self.DeleteValue('cancel_btn')
  self.DeleteValue('close_btn')
  self.DeleteValue('select_btn')
  self.DeleteValue('insert_btn')
  self.DeleteValue('copy_btn')
  self.DeleteValue('change_btn')
  self.DeleteValue('view_btn')
  self.DeleteValue('deleteb_btn')
  self.DeleteValue('deletef_btn')
  self.DeleteValue('first_btn')
  self.DeleteValue('previous_btn')
  self.DeleteValue('next_btn')
  self.DeleteValue('last_btn')
  self.DeleteValue('lookup_btn')
  self.DeleteValue('logout_btn')
  self.DeleteValue('browsecancel_btn')
  self.DeleteValue('browseclose_btn')
  self.DeleteValue('print_btn')
  self.DeleteValue('start_btn')
  temp = lower(self.GetValue('pressedbutton'))
  self.SetValue(clip(temp),clip(temp))
  !self._trace('Make Value ' & clip(temp))
  self.DeleteValue('pressedbutton')

!------------------------------------------------------------------------------
NetWebServerWorker.InitializeForm         PROCEDURE  (FILE p_file, KEY p_Key,string p_action)
loc:action               long
loc:saveaction           long
loc:saveid               like(self.FormSettings.recordID)
loc:result               long
loc:x                    long
  CODE
    !self._trace('InitializeForm action= [' & clip(p_action) & ']')
    If not p_FILE &= NULL     ! shouldn't be necessary, but catches some cases where callform fails to find the right form
      If Self.CallForm(p_file,NET:WEB:SetPics).
    End
    If not p_FILE &= NULL
      self._OpenFile(p_file)
    End
    case p_action
    of 'insert'
      If not p_FILE &= NULL
        self.LocalQueueToSessionQueue()
        loc:action = Self.CallForm(p_file,Net:InsertRecord + NET:WEB:StagePre)
        if loc:action
          self.FileToSessionQueue(p_file,Net:AlsoValueQueue) ! only handles up to single dimensioned arrays
          self.FormSettings.Action = loc:action
        end
      End
    of 'copy'
      If not p_FILE &= NULL
        self.LocalQueueToSessionQueue()
        If self.IfExistsValue('_popup_')
          loc:action = self.CallForm(p_file,Net:Web:Populate + Net:CopyRecord)
        Else
          loc:action = Self.CallForm(p_file,Net:CopyRecord + NET:WEB:StagePre)
        End
        if loc:action
          self.FileToSessionQueue(p_file,Net:AlsoValueQueue) ! only handles up to single dimensioned arrays
          self.FormSettings.Action = loc:action
        end
      End
    of 'change'
    orof 'view'
      If not p_FILE &= NULL
        loop 3 times
          loc:result = self._LoadRecord(p_file,p_Key)
          if loc:result = 0 then break.
          yield ; yield ; yield
        end
        !self._trace('Initialize form, after loading record. loc:result=' & loc:result & ' action=' & p_action)
        If loc:result = 0
          loc:action = 0
          case p_action
          of 'change'
            loc:action = self.CallForm(p_file,Net:ChangeRecord + NET:WEB:StagePre)
            self.FormSettings.Action = loc:action
          of 'view'
            loc:action = self.CallForm(p_file,Net:ViewRecord + NET:WEB:StagePre)
            self.FormSettings.Action = loc:action
          end
          if loc:action
            self.FileToSessionQueue(p_file) ! only handles up to single dimensioned arrays
          end
        Else
          !self._trace('record not found: ' & loc:result & ' ' & name(p_file) & ' errorcode=' & errorcode() & ' error=' & error() & self.getvalue('updatekey'))
          self.SetAlert('record not found: (' & loc:result & ') ' & ' Error ' & errorcode() & ': ' & clip(error()) & ' from key ' & self.getvalue('updatekey'),net:popup + net:message,'')
          ! record not found for some reason
        End
      Else ! no file, ie memory or queue
        !self._trace('Form has no file - hence Memory form')
        loc:action = self.CallForm(p_file,Net:ChangeRecord + NET:WEB:StagePre)
      End
    of ''
      If p_FILE &= NULL ! no file, ie memory or queue
        loc:action = self.CallForm(p_file,Net:ChangeRecord + NET:WEB:StagePre)
      End
    of 'deleteb'
    orof 'deletef'
      if self._LoadRecord(p_file,p_Key) = 0
        loc:action = self.CallForm(p_file,Net:DeleteRecord + NET:WEB:StagePre)
        if loc:action
          self.FileToSessionQueue(p_file) ! only handles up to single dimensioned arrays
          loc:saveaction = self.FormSettings.Action
          loop loc:x = 1 to Net:MaxKeyFields
            loc:saveid[loc:x] = self.FormSettings.recordID[loc:x]
          end
          self.FormSettings.Action = loc:action
          clear(self.FormSettings.recordID) ! needs to be clear for route through .ValidateForm
          self.SaveForm(p_file,p_key,Net:DeleteRecord)
          self.FormSettings.Action = loc:saveaction
          loop loc:x = 1 to Net:MaxKeyFields
            self.FormSettings.recordID[loc:x] = loc:saveId[loc:x]
          end
        end
      Else
        ! record not found for some reason
      End
    end

    If not p_FILE &= NULL
      self._closefile(p_file)
    End
!------------------------------------------------------------------------------
NetWebServerWorker.FormReady  Procedure(String p_Proc,String p_action,<String p_Field1>,<String p_Value1>,<String p_Field2>,<String p_Value2>,<String p_Field3>,<String p_Value3>,<String p_Field4>,<String p_Value4>,<String p_Field5>,<String p_Value5>,<String p_Field6>,<String p_Value6>,<String p_Field7>,<String p_Value7>,<String p_Field8>,<String p_Value8>,<String p_Field9>,<String p_Value9>,<String p_Field10>,<String p_Value10>)
loc:temppagename        like(NetWebServerWorker.pagename)
  code
  if self.GetValue('_form:inited_') = 0
    if self.IfExistsValue(clip(p_Proc) & '_form:inited_') = 0 or self.IfExistsValue(clip(p_Proc) & '_form:ready_') = 0
      loc:temppagename = self.pagename
      self.pagename = p_Proc
      if not omitted(4) and not omitted(5)
        self.SetValue(p_Field1,p_Value1)
        if not omitted(6) and not omitted(7)
          self.SetValue(p_Field2,p_Value2)
          if not omitted(8) and not omitted(9)
            self.SetValue(p_Field3,p_Value3)
            if not omitted(10) and not omitted(11)
             self.SetValue(p_Field4,p_Value4)
             if not omitted(12) and not omitted(13)
               self.SetValue(p_Field5,p_Value5)
               if not omitted(14) and not omitted(15)
                 self.SetValue(p_Field6,p_Value6)
                 if not omitted(16) and not omitted(17)
                   self.SetValue(p_Field7,p_Value7)
                   if not omitted(18) and not omitted(19)
                     self.SetValue(p_Field8,p_Value8)
                     if not omitted(20) and not omitted(21)
                       self.SetValue(p_Field9,p_Value9)
                       if not omitted(22) and not omitted(23)
                         self.SetValue(p_Field10,p_Value10)
                       end
                     end
                   end
                 end
               end
             end
           end
         end
       end
      end
      self.ProcessLink(p_action)
      self.pagename = loc:temppagename
    end
  end
!------------------------------------------------------------------------------
NetWebServerWorker.SaveForm               PROCEDURE  (FILE p_file, KEY p_Key,Long p_action)
loc:Action     long ! 1 insert, 2 change, 3 delete
ans            long
  CODE
  If not p_File &= NULL
    self._openfile(p_file)
  End
  self.SetValue('_form:inited_',1)  ! Don't need to re-init when doing a Save.
  if self.CallForm(p_file,Net:Web:SetPics + NET:WEB:StageValidate).   ! memory forms should still be validated, & pics set.
  loc:action = self.ValidateForm(p_file,p_Key,p_action)
  self.failed = 0
  !self._trace('In Saveform, form validated, action = ' & loc:action)
  case loc:action
  of Net:InsertRecord
    If not p_File &= NULL
      self.failed = self._addfile(p_file)
      self.SetSessionValue(name(p_file) & '_FormValue','K' & Position(p_key))
      !self._trace('Setting : ' & name(p_file) & '_FormValue' & ' poslen=' & len(clip(Position(p_key))))
    End
    loc:action = self.CallForm(p_file,Net:InsertRecord + NET:WEB:StagePost)
    If not p_File &= NULL
      if p_File{prop:SQLDriver}
        self.FileToSessionQueue(p_file,Net:AlsoValueQueue) ! only handles up to single dimensioned arrays
      else
        self.FileToSessionQueue(p_file) ! only handles up to single dimensioned arrays
      end
    End
  of Net:CopyRecord
    If not p_File &= NULL
      self.failed = self._addfile(p_file)
      self.SetSessionValue(name(p_file) & '_FormValue','K' & Position(p_key))
    End
    loc:action = self.CallForm(p_file,Net:CopyRecord + NET:WEB:StagePost)
    If not p_File &= NULL
      if p_File{prop:SQLDriver}
        self.FileToSessionQueue(p_file,Net:AlsoValueQueue) ! only handles up to single dimensioned arrays
      else
        self.FileToSessionQueue(p_file) ! only handles up to single dimensioned arrays
      end
    End
  of Net:changeRecord
    If not p_File &= NULL
      self.failed = self._updatefile(p_file)
      self.SetSessionValue(name(p_file) & '_FormValue','K' & Position(p_key))
    End
    loc:action = self.CallForm(p_file,Net:ChangeRecord + NET:WEB:StagePost) ! p_file is allowed to be null for memory forms.
    If not p_File &= NULL
      self.FileToSessionQueue(p_file)
    End
  of Net:DeleteRecord
    If not p_File &= NULL
      self.failed = self._deletefile(p_file)
      clear(p_file)
    end
    loc:action = self.CallForm(p_file,Net:DeleteRecord + NET:WEB:StagePost)
  of Net:Web:InvalidRecord
    If not p_File &= NULL
      self.FileToSessionQueue(p_file)
    End
    ans = loc:action
  end
  If not p_File &= NULL
    self._closefile(p_file)
  End
  if self.GetValue('retryfield') = ''
    self.SetValue('_form:inited_',0)  ! Save complete, might need to init next form.
  End
  return ans

!------------------------------------------------------------------------------
NetWebServerWorker.ValidateForm       PROCEDURE  (FILE p_file, KEY p_Key,Long p_action)
loc:Action    Long
loc:group     &Group
  CODE
    !self._trace('ValidateForm action = ' & p_action)
    case p_action
    of Net:InsertRecord
      If not p_File &= NULL
        loc:Group &= p_File{prop:record}
        If NOT loc:group &= NULL
          clear(loc:Group)
        End
        self.PrimeRecord(p_file,p_action) ! copies the session values into the file record
        loc:action = self.CallForm(p_file,Net:InsertRecord + NET:WEB:StageValidate)
      End
    of Net:CopyRecord
      If not p_File &= NULL
        loc:Group &= p_File{prop:record}
        If NOT loc:group &= NULL
          clear(loc:Group)
        End
        self.PrimeRecord(p_file,p_action) ! copies the session values into the file record
        loc:action = self.CallForm(p_file,Net:CopyRecord + NET:WEB:StageValidate)
      End
    of Net:ChangeRecord
      If not p_File &= NULL
        if self._LoadRecord(p_file,p_Key,self.FormSettings.recordID) = 0
          self._SaveFields(p_file,p_key,Net:Web:SessionQueue)
          self.FileToSessionQueue(p_File,Net:NotSessionValueQueue+Net:AlsoValueQueue,'_old_') ! used by calendar
          self.PrimeRecord(p_file,p_action)       ! copies the session values into the file record
          loc:action = self.CallForm(p_file,Net:ChangeRecord + NET:WEB:StageValidate)
        Else
          loc:action = 0
        End
      Else
        loc:action = self.CallForm(p_file,Net:ChangeRecord + NET:WEB:StageValidate)
      End
    of Net:DeleteRecord
      If not p_File &= NULL
        If self.FormSettings.recordID[1] = ''                               ! in case of deleteB record is already loaded
          loc:action = self.CallForm(p_file,Net:DeleteRecord + NET:WEB:StageValidate)
        ElsIf self._LoadRecord(p_file,p_Key,self.FormSettings.recordID) = 0 ! need to load record in DeleteF case
          loc:action = self.CallForm(p_file,Net:DeleteRecord + NET:WEB:StageValidate)
        Else
          loc:action = 0
        End
      End
    end
  Return loc:Action

!------------------------------------------------------------------------------
NetWebServerWorker.ValidateLogin               PROCEDURE  ()
  CODE
  self.SetSessionLoggedIn(1)
  Return

!------------------------------------------------------------------------------
! p_action is not used in this method, but is passed as a parameter for the benefit of derived classes
! from build 5.00 PR 21 this method only does the session queue, not the value queue. The value queue
! is now done locally in the form procedure.
NetWebServerWorker.PrimeRecord               PROCEDURE  (FILE p_file, Long p_action)
x             long
y             long
loc:any       any
loc:group     &group
loc:pic       cstring(32)
loc:dim       Long
loc:label     string(256)
  CODE
    If p_FILE &= NULL then Return.
    loc:Group &= p_file{prop:Record}
    LOOP x = 1 TO p_file{prop:fields}
      loc:dim = p_File{prop:dim,x}
      if loc:dim = 0
        Loc:Any &= what(loc:Group,x)
        loc:Label = p_file{prop:label,x}
        Loc:Any = self.GetSessionValue(loc:Label)
      Else
        compile('***',_VER_C60)
        Loop y = 1 to loc:dim
          Loc:Any &= what(loc:Group,x,y)
          loc:Label = p_file{prop:label,x} & '[' & y & ']'
          Loc:Any = self.GetSessionValue(loc:Label)
        end
        ***
      End
    End
    Loop x = 1 to p_file{prop:memos}
        p_file{prop:value,-x} = self.GetSessionValue(p_file{prop:label,-x})
    End

!------------------------------------------------------------------------------
NetWebServerWorker.SetSessionPics         PROCEDURE  (FILE p_file)
  CODE
!------------------------------------------------------------------------------
NetWebServerWorker.SetPics                PROCEDURE  (FILE p_file)
  CODE
!------------------------------------------------------------------------------
NetWebServerWorker._openfile   PROCEDURE(FILE p_file)
Loc:Err  Long(-1)
  CODE
  !self._trace('In NetWebServerWorker._openfile')
  If NOT p_FILE &= NULL
    OPEN(p_File,42h)
    Loc:Err = Errorcode()
  End
  Return Loc:Err
!------------------------------------------------------------------------------
NetWebServerWorker._closefile  PROCEDURE(FILE p_file)
Loc:Err  Long(-1)
  CODE
  If NOT p_FILE &= NULL
    CLOSE(p_File)
    Loc:Err = Errorcode()
  End
  Return Loc:Err
!------------------------------------------------------------------------------
NetWebServerWorker._addfile    PROCEDURE(FILE p_file)
Loc:Err  Long(-1)
  CODE
  If NOT p_FILE &= NULL
    ADD(p_File)
    Loc:Err = Errorcode()
  End
  Return Loc:Err
!------------------------------------------------------------------------------
NetWebServerWorker._primefile    PROCEDURE(FILE p_file, Long p_Clear, Long p_forceWrite=False)
  CODE
  return 0
!------------------------------------------------------------------------------
NetWebServerWorker._updatefile PROCEDURE(FILE p_file)
Loc:Err  Long(-1)
  CODE
  If NOT p_FILE &= NULL
    PUT(p_File)
    Loc:Err = Errorcode()
  End
  Return Loc:Err
!------------------------------------------------------------------------------
! usually overiden by template in WebServerHandler
NetWebServerWorker._getfile PROCEDURE(FILE p_file,KEY p_Key)
Loc:Err  Long(-1)
  CODE
  If NOT p_FILE &= NULL
    GET(p_File,p_Key)
    Loc:Err = Errorcode()
    if errorcode()
      !self._trace('_GetFile() failed ' & errorcode() & ' ' & error())
    end
  End
  Return Loc:Err
!------------------------------------------------------------------------------
NetWebServerWorker._deletefile PROCEDURE(FILE p_file)
Loc:Err  Long(-1)
  CODE
  If NOT p_FILE &= NULL
    DELETE(p_File)
    Loc:Err = Errorcode()
  End
  Return Loc:Err
!------------------------------------------------------------------------------
NetWebServerWorker._NetWebFileNamed       PROCEDURE (STRING p_Label)
F &FILE
  CODE
  F &= NULL
  Return F
!------------------------------------------------------------------------------
NetWebServerWorker._noColon PROCEDURE(string p_Name)
! Special Case : a colon is valid in clarion, but not in javascript. So : are translated to __
!                They are translated back automatically via _unescape

loc:name              string(255)
x                     long
  CODE
  loc:name = p_name
  x = instring(':',loc:name,1,1)
  loop while x
    loc:name = sub(loc:name,1,x-1) & '__' & sub(loc:name,x+1,255)
    x = instring(':',loc:name,1,1)
  end
  return clip(Loc:Name)
!------------------------------------------------------------------------------
NetWebServerWorker.GetLocatorValue   PROCEDURE(Long p_type,String p_name,Long p_where)
Loc:LocatorValue  String(256)
  Code
  if lower(self.GetValue('_refresh_')) = 'sort' or lower(self.GetValue('_refresh_')) = 'clearlocate'
    Loc:LocatorValue = ''
    self.SetSessionValue('Locator2' & clip(p_name),'')
    self.SetSessionValue('Locator1' & clip(p_name),'')
  elsif lower(self.GetValue('_refresh_')) = 'lookup'

  else
    IF p_Type = Net:Begins or p_Type = Net:Contains or p_Type = Net:Search
      do FromValue
      if Loc:LocatorValue = ''
        do FromSessionValue
      end
    Else
      do FromValue
    End
  End
  Return Clip(Loc:LocatorValue)

FromValue  Routine
    self.setvalue('selectfield',self.GetValue('focus'))
    case p_Where
    of Net:Above
      if self.IfExistsValue('Locator2' & clip(p_name))
        self.StoreValue('Locator2' & clip(p_name))
        Loc:LocatorValue = self.GetValue('Locator2' & clip(p_name))
        !self.setvalue('selectfield','Locator2' & clip(p_name))
        self.SetSessionValue('Locator1' & clip(p_name),self.GetValue('Locator2' & clip(p_name)))
      end
    of Net:Below
      if self.IfExistsValue('Locator1' & clip(p_name))
        self.StoreValue('Locator1' & clip(p_name))
        Loc:LocatorValue = self.GetValue('Locator1' & clip(p_name))
        !self.setvalue('selectfield','Locator1' & clip(p_name))
        self.SetSessionValue('Locator2' & clip(p_name),self.GetValue('Locator1' & clip(p_name)))
      end
    of Net:Both
      if self.IfExistsValue('Locator2' & clip(p_name))
        self.StoreValue('Locator2' & clip(p_name))
        !self.setvalue('selectfield','Locator2' & clip(p_name))
        self.SetSessionValue('Locator1' & clip(p_name),self.GetValue('Locator2' & clip(p_name)))
      end
      if self.IfExistsValue('Locator1' & clip(p_name))
        self.StoreValue('Locator1' & clip(p_name))
        !self.setvalue('selectfield','Locator1' & clip(p_name))
        self.SetSessionValue('Locator2' & clip(p_name),self.GetValue('Locator1' & clip(p_name)))
      end
      Loc:LocatorValue = self.GetValue('Locator2' & clip(p_name))
      if Loc:LocatorValue = ''
        Loc:LocatorValue = self.GetValue('Locator1' & clip(p_name))
      end
    end

FromSessionValue  Routine
    case p_Where
    of Net:Above
      Loc:LocatorValue = self.GetSessionValue('Locator2' & clip(p_name))
    of Net:Below
      Loc:LocatorValue = self.GetSessionValue('Locator1' & clip(p_name))
    of Net:Both
      Loc:LocatorValue = self.GetSessionValue('Locator2' & clip(p_name))
      if Loc:LocatorValue = ''
        Loc:LocatorValue = self.GetSessionValue('Locator1' & clip(p_name))
      end
    end

!------------------------------------------------------------------------------
NetWebServerWorker._MakeURL      PROCEDURE (String p_Url)
ans  String(512)
x    Long
y    Long
  Code
  ans = p_Url
  x = instring(upper(clip(self.site.WebFolderPath)) &'\',upper(ans),1,1)
  if x = 0
    x = instring(upper(clip(self.site.WebFolderPath)) &'/',upper(ans),1,1)
  end
  if x > 0
    ans = sub(ans,x+len(clip(self.site.WebFolderPath))+1,255)
  end
  if sub(ans,1,4) = 'www.'
    ans = 'http://' & ans
  End
  ans = self.NormalizeURL(ans)
  return(clip(ans))
!------------------------------------------------------------------------------
NetWebServerWorker.GetPosition            Procedure(View p_View)
ans   string(Net:PosSize)
F     &File
  code
  ans = Position(p_View)
  F &= p_view{prop:file,1}
  if len(clip(ans)) = 4 and F{prop:SqlDriver} = 0
    ans = Position(F)
    return 'F' & ans
  else
    return 'V' & ans
  end
!------------------------------------------------------------------------------
NetWebServerWorker.ResetPosition           Procedure(View p_View,String p_Position)
l   long
F   &File
  code
  l = len(p_Position)
  if l < 2
    Set(p_View)
  else
    case p_Position[1]
    of 'F'
      F &= p_view{prop:file,1}
      !self._trace('Reset (reget) on file')
      Reget(F,p_Position[2 : l])
      if errorcode()
        !self._trace('ResetPosition: reget failed ' & errorcode() & ' ' & error())
        set(p_view)
      else
        !self._trace('reget succeeded now doing Reset')
        Reset(p_View,F)
        if errorcode()
          !self._trace('ResetPosition: reset failed ' & errorcode() & ' ' & error())
          set(p_view)
        end
      end

    of 'V'
      Reset(p_View,p_Position[2 : l])
      If Errorcode()
        !self._trace('ResetPosition: Reset on view failed ' & errorcode() & ' ' & error() & ' l=' & l)
        Set(p_view)
      else
        !self._trace('Reset on view worked')
      End
    end
  end

!------------------------------------------------------------------------------
NetWebServerWorker._SetView PROCEDURE (VIEW p_View,FILE P_File,KEY p_Key,Long p_Rows,String p_Proc,String p_Field,Long p_LoadStyle,*Long p_Locator,String p_LocatorValue,Long p_SortDirection,Long p_Opt,*Long p_FillBack,*Long p_Dir,*Long p_NextDis,*Long p_PrevDis,Long p_LocCase,String p_RandomId)

loc:field      ANY
loc:KeyField   ANY
Loc:FieldLabel String(256)
Loc:Pic        String(32)
loc:fieldValue String(256)
pos            String(Net:PosSize)
posnomore      Long
x              long
loc:record     &group
loc:numeric    Long
loc:SortOrderChanged  Long
loc:cending    String(1)
loc:max        String(1)
loc:containsFilter  string(1024)
loc:filterwas       string(1024)

  Code
  !self._trace('SetView : locator = ' & p_locator & ' locatorvalue = ' & clip(p_locatorvalue) & ' direction=' & p_sortdirection & ' refresh=' & self.GetValue('_refresh_') & ' p_dir=' & p_dir & ' p_Field=' & clip(p_Field))
  if p_loadStyle = Net:FileLoad
    do SetFilterAll
    Set(p_View)
    !self._trace('Set(View) ! File Loaded')
  Else
    do Soc ! set SortOrderChanged
    Set(p_View)  ! resets only work if SET is already done. todo: This may affect SQL assertions made later on in this proc.
    !self._trace('Set(View) ! Pre-emptive Set refresh=' & self.GetValue('_refresh_'))
    if self.IfExistsValue('_refresh_')
      do SetFilterAll
      case lower(self.GetValue('_refresh_'))
        of 'next'
          do OnNextPos
        of 'previous'
          do OnPreviousPos
        of 'first'
        orof 'clearlocate'
          do OnFirst
        of 'last'
          do OnLast
        of 'locate'
          do OnLocate
        of 'lookup'
          do OnLocate
        of 'saved'
          do OnSavedRefresh
        of 'save'
        orof 'cancel'
          do OnSaveRefresh
        of 'sort'
          do OnSort
        else
          do OnDefault
      End
    Else
      do SetFilterAll
      if self.IfExistsValue('Save_Btn') or    |
         self.IfExistsValue('Close_Btn') or   |
         self.IfExistsValue('Cancel_Btn') or  |
         self.IfExistsValue('FormDone_Btn') or |
         self.IfExistsValue('Jump_Btn')
        do OnSaveButton
      elsif self.IfExistsValue('Next_btn')
        do OnNextPos
      elsif self.IfExistsValue('Previous_btn')
        do OnPreviousPos
      elsif self.IfExistsValue('First_btn')
        do OnFirst
      elsif self.IfExistsValue('Last_btn')
        do OnLast
      elsif p_locatorvalue <> ''
        do OnLocate
      Else
        do OnDefault
      End
    End
  End

SetFilterAll  Routine
  if band(p_opt,Net:NoFilter)
    exit
  end
  if p_Locator <> Net:None
    do FindField
    If self.IfExistsValue('_locpic')
      self.SetSessionValue(clip(p_proc) & '_LocatorPic_' & clip(p_RandomId),self.GetValue('_locpic'))
    End
    loc:pic = self.GetSessionValue(clip(p_proc) & '_LocatorPic_' & clip(p_RandomId))
    !self._trace('SetView : SetFilterAll : loc:pic = ' & loc:pic)
    if lower(loc:pic[1:2]) = '@s'
      loc:FieldValue = p_locatorvalue
      loc:field = loc:fieldvalue
    elsif lower(loc:pic[1:2]) = '@d'
      loc:FieldValue = self.dformat(clip(p_locatorvalue),loc:pic)
      loc:field = loc:fieldvalue
      p_Locator = Net:Date
    elsif loc:pic
      loc:FieldValue = self.dformat(clip(p_locatorvalue),loc:pic)
      loc:field = loc:fieldvalue
      p_Locator = Net:Position
    else
      loc:FieldValue = p_locatorvalue
      loc:field = loc:fieldvalue
    end
    !self._trace('loc:fieldValue= ' & clip(loc:fieldValue) & ' loc:numeric=' & loc:numeric & ' p_Locator=' & p_Locator & ' Net:Contains=' & Net:Contains)
    loc:filterwas = p_View{prop:filter}
    if (loc:fieldValue <> '' and loc:numeric=0) or (loc:FieldValue <> 0 and loc:numeric=1)
      case p_Locator
      of Net:Position
      orof Net:Lookup
      orof Net:Date
        do SetCending
        if loc:numeric
          if p_View{prop:filter}
            p_View{prop:filter} = self.CleanFilter(p_view,'(' & p_View{prop:filter} & ') AND ('&clip(p_Field)&' '&loc:cending&'= '& clip(loc:fieldValue) & ')')
          else
            p_View{prop:filter} = self.CleanFilter(p_view,clip(p_Field)&' '&loc:cending&'= '& clip(loc:fieldValue))
          End
        else
          if p_View{prop:filter} and p_loccase = 0
              p_View{prop:filter} = self.CleanFilter(p_view,'(' & p_View{prop:filter} & ') AND (UPPER('&clip(p_Field)&') '&loc:cending&'= '''& clip(upper(loc:fieldValue)) & clip(loc:max) & ''')')
          elsif p_View{prop:filter} and p_loccase = 1
              p_View{prop:filter} = self.CleanFilter(p_view,'(' & p_View{prop:filter} & ') AND (('&clip(p_Field)&') '&loc:cending&'= '''& clip(loc:fieldValue) & clip(loc:max) & ''')')
          elsif p_View{prop:filter} = '' and p_loccase = 0
            p_View{prop:filter} = self.CleanFilter(p_view,'UPPER('&clip(p_Field)&') '&loc:cending&'= '''& clip(upper(loc:fieldValue)) & clip(loc:max) &'''')
          else
            p_View{prop:filter} = self.CleanFilter(p_view,clip(p_Field)&' '&loc:cending&'= '''& clip(loc:fieldValue) & clip(loc:max) &'''')
          End
        End
      of Net:Contains
      orof Net:Search
      orof Net:Begins
       p_View{prop:filter} = self.CleanFilter(p_view,self.MakeFilter(p_View{prop:filter},loc:fieldValue,p_Field,p_Locator))
      end
    end
  End
  !self._trace('_SetView FilterAll = ' & p_view{prop:filter})

SetCending  Routine
  data
cma  long
bkt  long
  code
  cma = instring(',',p_view{prop:order},1,1)
  bkt = instring('(',p_view{prop:order},1,1)
  if bkt > cma then bkt = 0.
  loc:cending = sub(p_view{prop:order},bkt+1,1)
  if p_dir > 0
    if loc:cending = '-' then loc:cending = '<' else loc:cending = '>'.
  else
    if loc:cending = '-' then loc:cending = '>' else loc:cending = '<'.
  end
  if loc:cending = '<' then loc:max = 'z' else loc:max = ''.

OnSavedRefresh  Routine
  data
err long
  code
  !self._trace('OnSavedRefresh ' & name(p_File) & '_FormValue')
  pos = self.GetSessionValue(name(p_file) & '_FormValue')  ! contains 'K' & position(key)
  if pos
    Reget(p_File,pos[2 : len(pos)])
    err = self.LoadViewRecord(p_view,p_file,p_key)
    if err = 0
      pos = 'V' & position(p_view)
      self.ResetPosition(p_View,pos)
    end
  else
    pos = self.GetSessionValue(clip(p_Proc) & '_FirstValue_' & clip(p_RandomId))
    if pos = '' or (p_File{Prop:SQLDriver} = 1 and loc:sortorderchanged = 1)
      set(p_View)
      p_Dir = 1
      p_PrevDis = 1
    else
      do SetPrevDis ! check to see if there are any _earlier_ records. If not then disable Previous button.
      self.ResetPosition(p_View,pos) ! sql drivers don't support reset if sort order has changed.
    end
!    err = self.LoadViewRecord(p_view,p_file,p_key)
     !self._trace('LoadViewRecord  err= ' & errorcode() & ' ' & error())
!    if err = 0
!      pos = 'V' & position(p_view)
!      self.ResetPosition(p_View,pos)
!    end
  end

OnSaveRefresh  Routine
  pos = self.GetSessionValue(clip(p_Proc) & '_FirstValue_' & clip(p_RandomId))
  !self._trace('OnSaveRefresh sql=' & p_File{Prop:SQLDriver} & ' loc:sortorderchanged=' & loc:sortorderchanged & ' pos=' & clip(pos))
  if pos = '' or (p_File{Prop:SQLDriver} = 1 and loc:sortorderchanged = 1)
    set(p_View)
    p_Dir = 1
    p_PrevDis = 1
  else
    do SetPrevDis ! check to see if there are any _earlier_ records. If not then disable Previous button.
    self.ResetPosition(p_View,pos) ! sql drivers don't support reset if sort order has changed.
    !self._trace('OnSaveRefresh after reset error=? ' & errorcode() & ' ' & error() )
  end

SetPrevDis  Routine
  self.ResetPosition(p_View,pos)
  posnomore = 0
  Previous(p_view)
  if errorcode() = 0
    Previous(p_view)
  end
  if errorcode()
    p_PrevDis = 1
    p_dir = 1
  End

OnSaveButton  Routine
  data
ft   string(1024)
  code
  !self._trace('OnSaveButton')
  if self._LoadRecord(p_File,p_Key) = 0 !and p_File{Prop:SQLDriver} = 0
    if p_File{Prop:SQLDriver} = 0 ! reset(view,file) gives error 78 after NEXT for MsSql
      Reset(p_view,p_file)
      !self._trace('reset(View,File) ! OnSaveButton ISAM ')
      p_Dir = 2
    Else
      ! sql has a bug with reset, so a hack-around is necessary so that it goes back to the "current" record.
      ft = p_View{prop:filter}
      self.AddKeyToFilter(p_View,p_File,p_Key)
      set(p_view)
      next(p_view)
      pos = 'V' & Position(p_view)
      p_View{prop:filter} = ft
      p_Dir = 2
      do SetPrevDis ! check to see if there are any _earlier_ records. If not then disable Previous button.
      self.ResetPosition(p_View,pos)
    End
  else
    pos = self.GetSessionValue(clip(p_Proc) & '_FirstValue_' & clip(p_RandomId))
    if len(clip(pos)) < 2 or (p_File{Prop:SQLDriver} = 1 and loc:sortorderchanged = 1)
      set(p_View)
      !self._trace('set(View) ! OnSaveButton SQL or SortOrderChanged  or pos blank ' & loc:sortorderchanged)
      p_Dir = 1
      p_PrevDis = 1
    else
      p_Dir = 2
      do SetPrevDis ! check to see if there are any _earlier_ records. If not then disable Previous button.
!      if p_File{Prop:SQLDriver}
        self.ResetPosition(p_View,pos) ! sql drivers don't support reset if sort order has changed.
        !self._trace('reset(View,pos) ! OnSaveButton SQL pos=firstvalue=' & pos)
!      else
!        Reget(p_File,pos)
!        self._trace('reget(file,pos),reset(View,file) ! OnSaveButton pos=firstvalue=' & pos)
!      end
    end
  end

OnLocate routine
  set(p_View)
  if p_Locator = Net:Position or p_locator = net:date
    p_dir = 2
  else
    p_PrevDis = 1
  End
  !self._trace('set(View) ! OnLocate ')

OnNextPos  Routine
  pos = self.GetSessionValue(clip(p_Proc) & '_LastValue_' & clip(p_RandomId))
  self.ResetPosition(p_View,pos)
  Next(p_View)
  If errorcode()
    pos = self.GetSessionValue(clip(p_Proc) & '_FirstValue_' & clip(p_RandomId))
    do SetPrevDis ! check to see if there are any _earlier_ records. If not then disable Previous button.
    self.ResetPosition(p_View,pos)
    p_NextDis = 1
  End

OnPreviousPos  Routine
  pos = self.GetSessionValue(clip(p_Proc) & '_FirstValue_' & clip(p_RandomId))
  self.ResetPosition(p_View,pos)
  Previous(p_view)
  Previous(p_view)
  If errorcode()
    do OnFirst
  else
    next(p_view)
    p_FillBack = 1
    p_Dir = -2
  end

OnFirst  Routine
    set(p_View)
    p_Dir = 1
    p_PrevDis = 1

OnLast  Routine
    set(p_View)
    p_FillBack = 1
    p_Dir = -1
    p_NextDis = 1

OnSort  Routine
! commented out for 5.17, see multi dll example, insert order, lookup customer.
!    if self.ifExistsValue('IDField') and self.IfExistsValue(self.getvalue('IDField')) and p_File{Prop:SQLDriver} = 0
!      do FindKeyField
!      loc:keyfield = self.getvalue(self.getvalue('IDField'))
!      self._trace('SETVIEW: ONSORT: loc:keyfield=' & loc:keyfield)
!      Get(p_File,p_Key)
!      Reset(p_view,p_file)  ! not supported by SQL drivers
!    else
      Set(p_view)
      !self._trace('Set(View) ! On Sort SQL ')
      p_PrevDis = 1
      p_dir = 1
!    end

OnDefault  Routine
    !self._trace('OnDefault')
    pos = self.GetSessionValue(clip(p_Proc) & '_FirstValue_' & clip(p_RandomId))
    ! SQL drivers can't do a reset if the sort order has changed.
    !self._trace('On Default loc:sortOrderChanged=' & loc:sortOrderChanged & ' len(pos)=' & len(clip(pos)) )
    If pos <> '' and (loc:sortOrderChanged = 0 or p_file{prop:sqldriver} = 0)
      do SetPrevDis ! check to see if there are any _earlier_ records. If not then disable Previous button.
      self.ResetPosition(p_View,pos)
    Else
      !self._trace('On Default : Set(View)' )
      Set(p_view)
      p_PrevDis = 1
      p_dir = 1
    End

soc  routine
  If self.GetSessionValue(clip(p_Proc) & '_Prop:Order_' & clip(p_RandomId)) <> p_view{prop:order}
    loc:SortOrderChanged = 1
  End

FindField         Routine
  DATA
loc:fields        long
found             long
y                 long
F                 &file
  CODE
  If p_Field = '' then exit.
  Loop y = 1 TO p_View{PROP:Files}
    F &= p_View{PROP:File, y}
    loc:record &= F{prop:record}
    loc:fields = F{prop:fields}
    loop x = 1 to loc:fields
      ! p_field is the label not the name of the field.
      if clip(upper(F{prop:label,x})) = clip(upper(p_Field))
        loc:field &= What(loc:record,x)
        Loc:FieldLabel = self._SetLabel(F,x)
        case(F{prop:type,x})
        of 'LONG' orof 'ULONG' orof 'DECIMAL' orof 'PDECIMAL' orof 'BYTE' orof 'SHORT' orof 'USHORT' orof 'REAL' orof 'SREAL' orof 'DATE' orof 'TIME'
          loc:numeric = 1
        else
          loc:numeric = 0
        end
        found = 1
        break
      End
    end
    if found then break.
  end

FindKeyField  Routine
  DATA
x                 long
y                 long
loc:record        &group
  CODE
  compile('***',_VER_C55)
    ! Only works in Clarion 5.5 or above only !
    loc:record &= p_File{prop:record}
    x = p_Key{PROP:Components}
    loop y = 1 to x
      Loc:KeyField &= what(loc:record,p_Key{prop:field,y})
    end
  ***

!------------------------------------------------------------------------------
NetWebServerWorker.LoadViewRecord  Procedure(View p_View,File p_File,Key p_Key)
err Long
f   String(1024)
  code
  f = p_view{prop:filter}
  p_view{prop:filter} = ''
  self.AddKeyToFilter(p_View,p_File,p_Key)
  set(p_view)
  next(p_view)
  err = errorcode()
  p_view{prop:filter} = f
  return err

!------------------------------------------------------------------------------
NetWebServerWorker.AddKeyToFilter  Procedure(View p_View,File p_File,Key p_Key)
a   any
x   long
g   &group
n   long
  code
  g &= p_file{prop:record}
  loop x = 1 to p_key{PROP:Components}
     if p_View{prop:filter} <> '' then p_View{prop:filter} =  p_View{prop:filter} & ' and '.
     a &= what(g,p_key{prop:field,x})
     case(p_File{prop:type,p_key{prop:field,x}})
     of 'LONG' orof 'ULONG' orof 'DECIMAL' orof 'PDECIMAL' orof 'BYTE' orof 'SHORT' orof 'USHORT' orof 'REAL' orof 'SREAL' orof 'DATE' orof 'TIME'
       p_View{prop:filter} = p_View{prop:filter} & ' ' & self._SetLabel(p_file,p_key{prop:field,x}) & '=' & a
     else
       p_View{prop:filter} = p_View{prop:filter} & ' ' & self._SetLabel(p_file,p_key{prop:field,x}) & '=''' & clip(a) & ''''
     end
  end

!------------------------------------------------------------------------------
NetWebServerWorker._FindKeyField  PROCEDURE (File p_File,Key p_key)
x                 long
y                 long
loc:record        &group
Loc:KeyField      Any
  CODE
  compile('***',_VER_C55)    ! Only works in Clarion 5.5 or above only !
    loc:record &= p_File{prop:record}
    x = p_Key{PROP:Components}
    loop y = 1 to x
      Loc:KeyField &= what(loc:record,p_Key{prop:field,y})
    end
  ***
  Return Loc:KeyField
!------------------------------------------------------------------------------
NetWebServerWorker._ClearKeyFields  PROCEDURE (File p_File,Key p_key)
x                 long
y                 long
loc:record        &group
Loc:KeyField      Any
  CODE
  compile('***',_VER_C55)    ! Only works in Clarion 5.5 or above only !
    loc:record &= p_File{prop:record}
    x = p_Key{PROP:Components}
    loop y = 1 to x
      Loc:KeyField &= what(loc:record,p_Key{prop:field,y})
      clear(Loc:KeyField)
    end
  ***
!------------------------------------------------------------------------------
NetWebServerWorker._SaveFields  PROCEDURE (File p_File,Key p_key,Long p_To,<String p_prefix>)
x                 long
y                 long
loc:record        &group
Loc:KeyField      Any
Loc:Label         string(255)
loc:prefix        String(255)
  CODE
  if not omitted(5)
    loc:prefix = p_prefix
  end
  compile('***',_VER_C55)    ! Only works in Clarion 5.5 or above only !
    loc:record &= p_File{prop:record}
    x = p_Key{PROP:Components}
    !self._trace('_SaveFields x=' & x)
    loop y = 1 to x
      Loc:KeyField &= what(loc:record,p_Key{prop:field,y})
      Loc:Label = clip(loc:prefix) & self._SetLabel(p_file,p_key{prop:field,y})
      if band(p_to,Net:Web:ValueQueue)
        self.SetValue(Loc:Label,Loc:KeyField,,net:raw)
      end
      if band(p_to,Net:Web:SessionQueue)
        self.SetSessionValue(Loc:Label,Loc:KeyField)
      end
    end
  ***
!------------------------------------------------------------------------------
NetWebServerWorker._RestoreFields  PROCEDURE (File p_File,Key p_key,Long p_From,<String p_prefix>)
x                 long
y                 long
loc:record        &group
Loc:KeyField      Any
Loc:Label         string(255)
loc:prefix        String(255)
  CODE
  if not omitted(5)
    loc:prefix = p_prefix
  end
  compile('***',_VER_C55)    ! Only works in Clarion 5.5 or above only !
    loc:record &= p_File{prop:record}
    x = p_Key{PROP:Components}
    loop y = 1 to x
      Loc:KeyField &= what(loc:record,p_Key{prop:field,y})
      Loc:Label = clip(loc:prefix) & self._SetLabel(p_file,p_key{prop:field,y})
      if band(p_from,Net:Web:ValueQueue)
        Loc:KeyField = self.GetValue(Loc:Label)
      elsif band(p_from,Net:Web:SessionQueue)
        Loc:KeyField = self.GetSessionValue(Loc:Label)
      end
    end
  ***

!------------------------------------------------------------------------------
NetWebServerWorker.CreateSortHeader   PROCEDURE (Long p_CurrentOrder,Long p_SortNumber,String p_Proc,String p_Header,<String p_tip>,Long p_EditType=0,<string p_Class>,Long p_Width=0,Long p_Span=1, Long p_Neg=0,Long p_NotOnThisColumn,Long p_SortType,String p_BrowseFieldType,<String p_DivClass>)
Packet          String(1024)
SortHex         String(1)
tip             string(Net:MaxBinData)
cclass          string(StyleStringSize)
thclass          string(StyleStringSize)
divclass        string(StyleStringSize)
span            string(100)
width           string(100)
x               long
  CODE
  if not omitted(6)
    tip = self.CreateTip(p_tip)
  end
  if not omitted(8) and p_Class <> ''
    cclass = ' class="' & clip(p_Class) & '"'
  end
  if not omitted(15) and p_DivClass <> ''
    x = instring('nt-fakeget',p_DivClass,1,1)
    if x then p_DivClass[x : x+9] = '          '.  ! backward compatibility with headers that specify nt-fakeget
    if p_NotOnThisColumn
      divclass = ' class="' & clip(p_DivClass) & '"'
    else
      divclass = ' class="' & clip(p_DivClass) & ' nt-fakeget"'
    end
  else
    if p_NotOnThisColumn = 0 and p_SortType = Net:ServerSort
      divclass = ' class="nt-fakeget"'
    end
  end
  if p_span <> 1
    span = ' colspan="'& p_span & '"'
  end
  if p_width <> 0 and p_span = 1
    width = ' width="'& p_width & '"'
  end
  thclass = ' class="' & choose(abs(p_CurrentOrder) = p_SortNumber,'nt-browse-header-selected ui-corner-top','nt-browse-header-not-selected ui-corner-top') &'"'
  packet = '<th id="head_'&p_SortNumber& '"' & clip(span) & clip(width) & clip(thclass) &'>'
  if p_EditType = create:check
    packet = clip(packet) & self.CreateInput('checkbox','col_'&p_SortNumber,0,,,,,'onclick="'&clip(p_Proc)&'.ec(this);"')  & self.CRLF
  end
  if p_neg
    SortHex = '-'
  end
  if p_SortType = Net:ServerSort and (p_BrowseFieldType = 'String' or p_BrowseFieldType = 'Date') and p_Header <> ''
    if p_CurrentOrder = -p_SortNumber
      packet = clip(packet) & '<span class="nt-icon-left ui-icon '&choose(self.IsMobile(),'ui-icon-arrow-d','ui-icon-triangle-1-s')&'"></span>'
      SortHex = ''
    elsif p_CurrentOrder = p_SortNumber
      packet = clip(packet) & '<span class="nt-icon-left ui-icon '&choose(self.IsMobile(),'ui-icon-arrow-u','ui-icon-triangle-1-n')&'"></span>'
      SortHex = '-'
    end
  end

  if p_SortType = Net:ServerSort and p_NotOnThisColumn = 0  and (p_BrowseFieldType = 'String' or p_BrowseFieldType = 'Date') and p_Header <> ''
    packet = clip(packet) & '<div data-do="sh" '&clip(divclass)&'>' ! moved the div in here for better styling of empty headers
    packet = clip(packet) & '<a data-value="'&clip(SortHex)&clip(p_SortNumber)&'" ' & clip(tip)&clip(cclass)&'>' & self._jsok(p_Header,1)&'</a>'
    packet = clip(packet) & '</div>'
  elsif p_Header <> ''
    packet = clip(packet) & '<div '&clip(divclass)&'>' & self._jsok(p_Header,1) & '</div>'
  end
  !if p_SortType = Net:ServerSort and p_NotOnThisColumn = 0 and p_BrowseFieldType = 'String' and p_Header <> ''
  !if p_SortType = Net:ServerSort and p_BrowseFieldType = 'String' and p_Header <> ''
    packet = clip(packet) & '</th>' & self.CRLF
  !else
  !  packet = clip(packet) & '</th>' & self.CRLF
  !end
  Return clip(Packet)
!------------------------------------------------------------------------------
NetWebServerWorker.CreateLocator  PROCEDURE (String p_proc,String p_DivName,Long p_LocatorType,String p_locatorValue,String p_ButtonSetOptions,String p_SortHeader,String p_id,String p_RandomId,<String p_Class>,<String p_ButtonSetClass>,<String p_PlaceHolder>,Long p_size=20,Long p_HidePrompt=0,Long p_Position=0,Long p_SearchButton=0, Long p_ClearButton=0,Long p_AutoComplete=0,<String p_AutoCompleteOptions>,Long p_Stage=0,Long p_Imm=0)
Packet              Stringtheory
loc:class           Stringtheory
loc:buttonsetclass  Stringtheory
loc:placeholder     Stringtheory
options             Stringtheory
loc:uid             String(1)
loc:type            String(10)
  Code
  p_proc = lower(p_proc)
  p_id = lower(p_id)
  if not omitted(10)
    loc:class.setValue(p_class)
  end
  if not omitted(11)
    loc:buttonsetclass.SetValue(p_ButtonSetClass)
  end

  if not omitted(12)
    loc:placeholder.SetValue(p_PlaceHolder)
  end
  loc:uid = choose(p_Position = 2,'a','b')

  case p_LocatorType
  of Net:Position
  orof Net:Date
    self.site.LocatePromptText = self.site.LocatePromptTextPosition
  of Net:Contains
  orof Net:Search
    self.site.LocatePromptText = self.site.LocatePromptTextContains
  of Net:Begins
    self.site.LocatePromptText = self.site.LocatePromptTextBegins
  End
  !packet.SetValue('<div id="'&clip(p_DivName)& '_locator_' & loc:uid & '_div">')
  packet.append('<table class="'&self.combine(self.site.style.BrowseLocator,loc:class.getValue())&'"><tr>' &|
  Choose(p_HidePrompt=0 and not self.IsMobile(),'<td>'& self.translate(self.site.LocatePromptText)& ' ' & clip(p_SortHeader) & ': </td>',''))
  Case p_LocatorType
  of Net:Date
    packet.Append('<td><div>' & self.CreateDateInput('locator' & p_Position & clip(p_id),'',self.combine(self.site.style.BrowseLocator,loc:class.GetValue()),,, self.GetSessionValue(clip(p_proc) & '_LocatorPic_' & clip(p_RandomId)),,self.site.dateoptions,'data-do="lo" data-imm="false"',,,,,loc:placeholder.GetValue()) & '</div></td>')
  of Net:Position
    loc:type = Choose(self.useragent <> 'opera' ,'search','input') ! Opera has bug with events on "search" fields. Bug report DSK-355004
    packet.append('<td><div>' & self.CreateInput(clip(loc:type),'locator' & p_Position & clip(p_id),'',self.combine(self.site.style.BrowseLocator,loc:class.GetValue()),,'data-do="lo" data-imm="false" size="' & p_size & '" ',,,,,,loc:placeholder.GetValue()) & '</div></td>')
  Else
    loc:type = Choose(self.useragent <> 'opera' ,'search','input') ! Opera has bug with events on "search" fields. Bug report DSK-355004
    packet.append('<td><div>' & self.CreateInput(clip(loc:type),'locator' & p_Position & clip(p_id),clip(p_locatorValue),self.combine(self.site.style.BrowseLocator,loc:class.GetValue()),,'data-do="lo"  data-imm="'&choose(p_imm,'true','false')&'" size="' & p_size & '" ',,,,,,loc:placeholder.GetValue()) & '</div></td>')
  End
  If p_SearchButton or p_ClearButton
    if self.IsMobile()
      packet.append('<td><div id="'&clip(p_DivName)&'_locate_'&loc:uid&'" class="'&self.combine(self.site.style.BrowseLocateButtonSet,loc:buttonsetclass.GetValue())&'" data-role="controlgroup" data-type="horizontal">')
    else
      packet.append('<td><div id="'&clip(p_DivName)&'_locate_'&loc:uid&'" class="'&self.combine(self.site.style.BrowseLocateButtonSet,loc:buttonsetclass.GetValue())&'">')
    end
    If p_SearchButton
      packet.Append(self.CreateStdButton('button',Net:Web:LocateButton))
    End
    If p_ClearButton
      packet.append(self.CreateStdButton('button',Net:Web:ClearButton))
    End
    packet.append('</div></td>')
  End
  packet.append('</tr></table>')
 ! if self.verbose
 !   packet.append('</div><!-- '&clip(p_DivName)& '_locator_' & loc:uid & '_div -->' & self.CRLF)
 ! else
 !   packet.append('</div>' & self.CRLF)
 ! end
  If (p_SearchButton or p_ClearButton) and self.site.UseLocatorButtonSet and not self.IsMobile() !and p_Silent = 0
    self.jQuery('#' & clip(p_DivName)& '_locate_' & loc:uid,'buttonset',p_ButtonSetOptions)
  End
!  If p_AutoComplete !and p_Silent = 0
!    Case p_LocatorType
!    of Net:Begins
!    of Net:Contains
!    of Net:Search
!      self.SetOption(options,'source','function(req,res){{$.getJSON("'& clip(p_Proc) &'?","_event_=newselection&_ajax_=1&_term_="+req.term,function(data){{res(data);})}')
!      self.SetOption(options,'minLength',1,true)
!      self.SetOption(options,'delay',100,true)
!      self.SetOption(options,'select','function(e, ui) {{ $("#Locator' & p_Position & clip(p_id) & '").attr("data-ac","select") }',true)
!      self.SetOption(options,'open','function(e, ui) {{ $("#Locator' & p_Position & clip(p_id) & '").attr("data-ac","open") }',true)
!      self.SetOption(options,'close','function(e, ui) {{ var x=  $("#Locator' & p_Position & clip(p_id) & '").attr("data-ac"); $("#Locator' & p_Position & clip(p_id) & '").attr("data-ac","close");if (x=="select"){{$("#Locator' & p_Position & clip(p_id) & '").trigger("change") };}',true)
!      self.jQuery('#Locator' & p_Position & clip(p_id),'autocomplete',options)
!    End
!  end
!  if band(p_stage,net:web:GenerateWholeBrowse) <> net:web:GenerateWholeBrowse
!    self.AddResponse(Packet.GetValue(),clip(p_DivName)& '_locator_' & loc:uid & '_div',false,Net:Web:HtmlOk + Net:UnsafeHtmlOk) ! unsafe because it contains an input field.
!    Return ''
!  End
  Return Packet.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker.BodyOnLoad  PROCEDURE (<String p_Class>,<string p_OnLoad>,<string p_DivClass>)
Packet        StringTheory
loc:class     String(StyleStringSize)
  CODE
  if not omitted(2) and p_Class <> ''
    loc:class = self.wrap('class',p_class)
  End
  if not omitted(4) and p_divclass <> ''
    packet.SetValue(self.wrap('class',p_divclass))
  End
  if self.IsMobile()
    Packet.SetValue('<body'&clip(loc:class)&'>'&self.CRLF&|
                    '<div id="body_div"'& packet.GetValue() &' data-role="page" data-add-back-btn="false">' & self.CRLF)
  else
    Packet.SetValue('<body'&clip(loc:class)&'>' & self.CRLF & |
             '<div id="body_div"'& packet.GetValue() &'>' & self.CRLF)
  end
  Return Packet.GetValue()
!------------------------------------------------------------------------------
NetWebServerWorker.InsertAgain  PROCEDURE (String p_proc)
  Code
  self.RequestFileName = p_proc
  self.SetValue('Insert_btn','Insert')
  self._InsertAfterSave = 1
!------------------------------------------------------------------------------
NetWebServerWorker._jsok                  PROCEDURE  (<string p_notok>,long p_html=0,Long p_Send=0)
ReturnValue  &String
  compile('***',_VER_C60)
QuickString  String(Len(p_notok)*8)
  ***
  omit('***',_VER_C60)
QuickString  String(Net:MaxBinData)
  ***
html         long
y            long

  code
  if omitted(2) or p_notok = ''
    return ''
  end
  ReturnValue &= QuickString
  if lower(self.site.htmlcharset) = 'utf-8'
    html = bor(p_html,Net:Utf8)
  else
    html = p_html
  end
  do ParentCall
  if p_Send
    if y > len(QuickString) ! string wasn't long enough
      ReturnValue &= new(String(y+10)) ! so make a long enough string
      do ParentCall                    ! and try again
      self.SendString(ReturnValue,1,y,net:NoHeader)
      Dispose(ReturnValue)
    else
      self.SendString(ReturnValue,1,y,net:NoHeader)
    end
    return ''
  else
    if html <> 0
      return clip(ReturnValue)
      return self.HtmlToxHtml(ReturnValue)
    else
      return clip(ReturnValue)
    end
  end

ParentCall  routine
  if lower(self.site.htmlcharset) = 'utf-8' and self.site.StoreDataAs <> net:StoreAsUTF8
    y = self.RequestData.WebServer._jsok(self.AsciiToUtf(p_notok),html,ReturnValue)
  else
    y = self.RequestData.WebServer._jsok(p_notok,html,ReturnValue)
  End

!------------------------------------------------------------------------------
NetWebServerWorker._divHeader            PROCEDURE (String p_name,<String p_class>,Long p_Send=1,<string p_tip>)
  code
  return self.divheader(p_name,p_class,p_send,p_tip)

NetWebServerWorker.DivHeader            PROCEDURE (String p_name,<String p_class>,Long p_Send=1,<string p_tip>,<string p_extra>,Long p_CRLF=0)
packet    StringTheory
classname cString(256)
loc:tip   string(Net:MaxBinData)
loc:extra string(256)
crc       ulong
  CODE
  If omitted(3)
    classname = self.wrap('class','adiv')
  Else
    classname = self.wrap('class',p_class)
  End
  if not omitted(5)
     loc:tip = self.CreateTip(p_tip)
  end
  if not omitted(6)
     loc:extra = ' ' & p_extra
  end
  if band(p_send,Net:Crc) = Net:Crc
    crc = self.crc32(p_name)
    self.LastDivHeader = 'div' & crc & '_div'
  else
    self.LastDivHeader = self._jsok(lower(self._nocolon(p_name))) & '_div'
  end
  If self.Ajax = 1
    if self._headerSent = 0
      packet.SetValue( |
      '<?xml version="1.0" encoding="'&clip(self.site.HtmlCharset)&'"?>' & self.CRLF &|
      '<ajax-response>' & self.CRLF)
      self.ParseHTML(packet,,,Net:SendHeader)
      self._headerSent = 1
    end
    If self._divcounter = 0
      packet.SetValue('<response type="element" id="'&clip(self.LastDivHeader)&'">' & self.CRLF &|
               '<div id="'&clip(self.LastDivHeader)&'" '&classname & clip(loc:tip) & clip(loc:extra) &'>')    ! do not put 13,10 after the div header as it makes an empty div not empty.
    else
      packet.SetValue('<div id="'&clip(self.LastDivHeader)&'" '&classname & clip(loc:tip) & clip(loc:extra) &'>')
    end
  else
    packet.SetValue('<div id="'&clip(self.LastDivHeader)&'" '&classname & clip(loc:tip) & clip(loc:extra) &'>')
  end
  if p_CRLF then packet.Append(self.CRLF).
  self._divcounter += 1
  if band(p_send,1)
    self.ParseHTML(packet,,,NET:NoHeader)
  end
  Return packet.GetValue()
!------------------------------------------------------------------------------
NetWebServerWorker._DivFooter            PROCEDURE (Long p_Send = 1)
  code
  return self.Divfooter(p_send)

NetWebServerWorker.DivFooter            PROCEDURE (Long p_Send = 1,<string p_comment>)
packet  StringTheory
  CODE
  if self._divcounter = 0 then return ''.
  if omitted(3)
    packet.SetValue('</div>' & self.CRLF)
  else
    packet.SetValue('</div>' & self.comment(p_comment) & self.CRLF)
  end
  self._divcounter -= 1
  If self.Ajax = 1 and self._divcounter = 0
    packet.Append('</response>' & self.CRLF)
  End
  if p_send
    self.ParseHTML(packet,,,NET:NoHeader)
  end
  Return packet.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker.FlushResponses    PROCEDURE()
x      Long
y      Long
s      Long
packet StringTheory
  code
  if records(self._ResponseQueue) = 0 then return.
  If self.Ajax = 1
    if self._headerSent = 0
      self._headerSent = 1
      packet.Append('<?xml version="1.0" encoding="'&clip(self.site.HtmlCharset)&'"?>' & self.CRLF &|
      '<ajax-response>' & self.CRLF)
    end
  end

  If self.Ajax = 1 ! non-script responses can only be sent in ajax mode
    Loop y = 1 to records(self._ResponseQueue)
      Get(self._ResponseQueue,y)
      if self._ResponseQueue.script
        s = true
        cycle
      end
      packet.Append(clip(self._ResponseQueue.response))
    End
  End

  if s or self.Ajax = 0 ! scripts can be accumulated, and added to the bottom of the page in non-ajax mode.
    If self.Ajax = 1
      packet.Append('<response type="script">' & self.CRLF)
    ! document ready does not fire in mobile mode because pages are loaded "async". bind to mobile init instead.
    ElsIf self.IsMobile()
       packet.append('<script defer="defer">' & self.CRLF & '$(document).ready( function(){{')
       ! packet.append('<script defer="defer">' & self.CRLF & '$(document).bind("mobileinit", function(){{')
       ! mobileinit does not fire in example 3, when in mobile mode. document ready seems to work better.
    Else
      packet.append('<script defer="defer">' & self.CRLF & '$(document).ready( function(){{')
    End
    Loop y = 1 to records(self._ResponseQueue)
      Get(self._ResponseQueue,y)
      if self._ResponseQueue.script = 0 then cycle.
      If self.Ajax
        packet.Append(self._jsok(self._ResponseQueue.response,self._ResponseQueue.html))
      Else
        packet.Append(clip(self._ResponseQueue.response))
      End
      if packet.EndsWith(self.CRLF) = false then packet.Append(self.CRLF). ! requires StringTheory 1.86
    End
    If self.Ajax = 1
      packet.Append('</response>' & self.CRLF)
    Else
      packet.append('})</script>' & self.CRLF)
    End
  End
  self.ParseHTML(packet,,,NET:SendHeader)
  Free(self._ResponseQueue)

!------------------------------------------------------------------------------
NetWebServerWorker.AddResponse      Procedure(String p_Response,String p_id,Long p_Send = 1,Long p_Html=0)
  code
  if p_Response = '' then return ''.
  If self.Ajax = 1
    self._ResponseQueue.html = false
    if sub(p_Response,1,10) <> '<response '
      self._ResponseQueue.script = false
      self._ResponseQueue.response =  '<response type="element" id="'&clip(p_id)&'">' & self.CRLF &|
                                      self._jsok(p_Response,p_Html)  & self.CRLF &|
                                      '</response>' & self.CRLF
    else
      self._ResponseQueue.response =  self._jsok(p_Response,p_Html)  & self.CRLF
    end
    add(self._ResponseQueue)
  ElsIf p_send
    self.ParseHTML(p_Response,1,0,NET:NoHeader)
  Else
    Return clip(p_Response)
  End
  Return ''

!------------------------------------------------------------------------------
NetWebServerWorker.Comment           Procedure (String p_Comment)
  code
  if p_Comment and self.verbose
    return '<!-- ' & clip(p_comment) & ' -->'
  else
    return ''
  end

!------------------------------------------------------------------------------
NetWebServerWorker.Script            PROCEDURE (String p_script,Long p_Send = 1,Long p_Html=0,Long p_First=0)
  code
  if p_script = '' then return.
  self._ResponseQueue.response = clip(p_script) & self.CRLF
  self._ResponseQueue.script = true
  self._ResponseQueue.html = p_html
  if p_First
    add(self._ResponseQueue,1)
  Else
    add(self._ResponseQueue)
  End
  Return
!------------------------------------------------------------------------------
NetWebServerWorker.ReplyComplete          Procedure()
  code
  if self.replyCompleted = 0
    Self.Popup(,Net:Send)
    self.SendFooter(2)
    self._CloseConnection()
    self.RequestData.WebServer.AddLog(self)
    self.replyCompleted = 1
  end
  self.PerfEndThread()

!------------------------------------------------------------------------------
NetWebServerWorker.SendFooter            PROCEDURE (Long p_place=0)
packet StringTheory
  code
  if p_place <> 2 then return.
  self.FlushResponses()
  If self.Ajax = 1 and self._footerSent = 0 and self._headerSent = 1
    self._footerSent = 1
    packet.setvalue('</ajax-response>' & self.CRLF)
    self.ParseHTML(packet,1,0,NET:NoHeader)
  End

!------------------------------------------------------------------------------
NetWebServerWorker._RegisterDivEx   PROCEDURE (String p_name, Long p_Timer=0, <String p_par>, <String p_sil>,Long p_Send=1)
divname     cString(256)
loc:par    cString(256)
loc:sil    cString(256)
  CODE
  If p_timer <= 0 then return. ! ''.
  divname = lower(clip(p_name))
  if not omitted(4) then loc:par = clip(p_par).
  if not omitted(5) then loc:sil = clip(p_sil).
  if loc:par = '' then loc:par = ''''''.
  if loc:sil = '' then loc:sil = ''''''.
  self.script('SetTimer('''&divname&''','&p_timer&',' & loc:par &',' & loc:sil &',0);' & self.CRLF)

!------------------------------------------------------------------------------
NetWebServerWorker.GetDescription   PROCEDURE(FILE p_file,<KEY p_key1>,<KEY p_key2>,*? p_valuefield,String p_descfield,String p_value)
ReturnValue Long
  code
  return self.GetDescription(p_file,p_key1,,p_valuefield,,p_value)
!------------------------------------------------------------------------------
NetWebServerWorker.GetDescription   PROCEDURE(FILE p_file,<KEY p_key1>,<KEY p_key2>,*? p_valuefield,<*? p_descfield>,String p_value)
codekey        &Key
desckey        &Key
Err            Long
ReturnValue    Long
loc:value      string(255),dim(Net:MaxKeyFields)
y              Long
loc:record     &group
Loc:KeyField   Any
  Code
  !self._trace('GetDescription on ' & name(p_file))
  If not omitted(3)
    codekey &= p_key1
  else
    codekey &= NULL
  End
  If not omitted(4)
    desckey &= p_key2
  ELSE
    desckey &= NULL
  End
  loc:record &= p_File{prop:record}
  If p_Value = ''
    clear(loc:record)
    return false
  end
  If not codekey &= NULL
    if codekey{prop:components} > 1
      ! higer component fields are assumed to be set in the session queue.
      self._RestoreFields(p_file,codekey,Net:Web:SessionQueue)
      ! set lowest key componet field to p_value
      y = codeKey{PROP:Components}
      Loc:KeyField &= what(loc:record,codeKey{prop:field,y})
      Loc:KeyField = p_value
      ! whole key is primed so try and get the record.
      err = self._getfile(p_file,CodeKey)
    else
      loc:value[1] = p_value
      err = self._LoadRecord(p_file,codekey,loc:value)
    end
    if err <> 0 ! can't find the record using the code, perhaps the user typed in the description?
      if not desckey &= NULL
        if desckey{prop:components} > 1
          loc:record &= p_File{prop:record}
          ! higer component fields are assumed to be set in the session queue.
          self._RestoreFields(p_file,desckey,Net:Web:SessionQueue)
          ! set lowest key componet field to p_value
          y = desckey{PROP:Components}
          Loc:KeyField &= what(loc:record,desckey{prop:field,y})
          Loc:KeyField = p_value
          ! whole key is primed so try and get the record.
          err = self._getfile(p_file,desckey)
          !self._trace('After getfile, err=' & err)
        else
          loc:value[1] = p_value
          err = self._LoadRecord(p_file,desckey,loc:value)
          !self._trace('After loadrecord, err=' & err)
        end
        if err = 0
          if not p_ValueField &= NULL
            self.SetValue('NewValue',p_valuefield,,Net:raw)   ! If so correct the code
            self.SetValue('Value',p_valuefield,,Net:raw)      ! If so correct the code (browse uses 'value')
            ReturnValue = true
          End
        end
      end
    Else
      ReturnValue = true
    End
  End
  if ReturnValue = false
    clear(loc:record)
  end
  Return ReturnValue
!------------------------------------------------------------------------------
NetWebServerWorker._DateFormat  PROCEDURE (<String p_DatePic>)
ReturnValue  String(20)
loc:x        Long
loc:datepic  string(20)
  CODE
  if omitted(2) or p_DatePic = ''
    loc:datepic = self.site.DatePicture
  else
    loc:datepic = p_datePic
  end
  loc:x = sub(loc:datepic,3,2)
  execute loc:x
    ReturnValue = 'mm/dd/yy'              !1
    ReturnValue = 'mm/dd/yyyy'
    ReturnValue = 'mmm dd,yyyy'
    ReturnValue = 'mmmmmmmmm dd, yyyy'
    ReturnValue = 'dd/mm/yy'              !5
    ReturnValue = 'dd/mm/yyyy'
    ReturnValue = 'dd mmm yy'
    ReturnValue = 'dd mmm yyyy'
    ReturnValue = 'yy/mm/dd'
    ReturnValue = 'yyyy/mm/dd'              !10
    ReturnValue = 'yymmdd'
    ReturnValue = 'yyyymmdd'
    ReturnValue = 'mm/yy'
    ReturnValue = 'mm/yyyy'
    ReturnValue = 'yy/mm'                !15
    ReturnValue = 'yyyy/mm'
  End
  return ReturnValue
!------------------------------------------------------------------------------
NetWebServerWorker.w3header     PROCEDURE  (<string pClass>)
ans  StringTheory
  CODE
  ans.setvalue('<!DOCTYPE html><html')
  if omitted(2) or pClass = ''
    ans.append(' class="no-js"')
  else
    ans.append(' class="'&clip(pClass)&' no-js"')
  end
  ans.append('>' & self.CRLF)
  return ans.getvalue()
!------------------------------------------------------------------------------
NetWebServerWorker.AddScript    PROCEDURE  (string p_Script)
cc  cstring(3)
  CODE
  if self.site.AutoCheckCache then cc = '?cc'.
  if instring('http:',lower(clip(p_Script)), 1,1) > 0
    RETURN('<script src="' & clip(p_Script) & cc & '"type="text/javascript"></script>' & self.CRLF)
  else
    RETURN('<script src="/' & clip(self.site.ScriptsDir) & '/' & clip(p_Script) & cc & '" type="text/javascript"></script>' & self.CRLF)
  end
!------------------------------------------------------------------------------
NetWebServerWorker.AddStyle     PROCEDURE  (string p_Style,Long p_theme=0)
cc  cstring(3)
  CODE
  if self.site.AutoCheckCache then cc = '?cc'.
  if instring('http:',lower(clip(p_Style)), 1,1) > 0
    RETURN('<link href="' & clip(p_Style) & cc &'" rel="stylesheet" />' & self.CRLF) !html5
  elsif p_theme
    RETURN('<link href="/' & clip(self.site.ThemesDir) & '/' & clip(p_Style) & cc &'" rel="stylesheet" />' & self.CRLF) !html5
  else
    RETURN('<link href="/' & clip(self.site.StylesDir) & '/' & clip(p_Style) & cc &'" rel="stylesheet" />' & self.CRLF) !html5
  end
!------------------------------------------------------------------------------
NetWebServerWorker.RedactorInit  PROCEDURE (String p_FieldId,Long p_Flags=0,<string p_manual>)
options  StringTheory
  Code
  if not omitted(4)
    options.SetValue(p_manual)
  end
  self.RedactorInit(p_FieldId,p_Flags,options)

!------------------------------------------------------------------------------
NetWebServerWorker.RedactorInit  PROCEDURE (String p_FieldId,Long p_Flags=0,StringTheory p_Options)
  Code
  self.SetOption(p_Options,'fixed','true',true)
  self.SetOption(p_Options,'imageUpload','"redactorImageUpload"',true)
  self.SetOption(p_Options,'fileUpload','"redactorFileUpload"',true)
  self.SetOption(p_Options,'autoresize','true',true)
  if self.site.RedactorLanguage
    self.SetOption(p_Options,'lang','"' & clip(self.site.redactorLanguage) &'"')
  end
  self.script('$(''#'& self._nocolon(p_FieldId) &''').redactor({{'&p_Options.GetValue()&'});',Net:NoSend)
  return
!------------------------------------------------------------------------------
NetWebServerWorker._TinyMCEInit PROCEDURE (String p_FieldId,Long p_Flags=0,<string p_manual>)
ReturnValue  StringTheory
  CODE
  ReturnValue.SetValue( |
    '<script type="text/javascript">'&|
    'tinyMCE.init({{'&|
    'entity_encoding : "raw",' &|
    'mode : "exact",elements : "'&self._nocolon(p_FieldId)&'",' &|               !zoom,save
    'plugins : "table,advhr,advimage,advlink,emotions,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,spellchecker",' &|
    'theme_advanced_buttons1 : "newdocument,search,replace,print,preview,spellchecker,separator,cut,copy,paste,pastetext,pasteword,undo,redo,link,unlink,separator,tablecontrols",' &|
    'theme_advanced_buttons2 : "styleselect,fontselect,fontsizeselect,bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,numlist,bullist,outdent,indent,forecolor,backcolor",' &|
    'theme_advanced_buttons3 : "insertdate,inserttime,emotions,media,flash,advhr,ltr,rtl,fullscreen,code",' &|
    'theme_advanced_toolbar_location : "top",'&|
    'theme_advanced_toolbar_align : "left",')

!    'plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",'&|
!    'theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",'&|
!    'theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",'&|
!    'theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",'&|
!    'theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",'&|
!    'theme_advanced_statusbar_location : "bottom",'&|
!    'theme_advanced_resizing : true,'&|

  If not omitted(4)
    ReturnValue.Append(p_manual,st:clip)
  End

!    '// Skin options<13,10>'&|
!    'skin : "o2k7",'&|
!    'skin_variant : "silver",'&|
!    ''&|
!    '// Example content CSS (should be your site CSS)<13,10>'&|
!    'content_css : "/styles/all.css",'&|
!    ''&|
!    '// Drop lists for link/image/media/template dialogs<13,10>'&|
!    'template_external_list_url : "js/template_list.js",'&|
!    'external_link_list_url : "js/link_list.js",'&|
!    'external_image_list_url : "js/image_list.js",'&|
!    'media_external_list_url : "js/media_list.js",'&|
!    ''&|
!    '// Replace values for the template plugin<13,10>'&|
!    'template_replace_values : {{'&|
!    'username : "Some User",'&|
!    'staffid : "991234"'&|
!    '}'&|
!    '});'&|
  ReturnValue.Append( |
    'theme : "advanced"'&|
    '});'&|
    '</script>')
  Return ReturnValue.GetValue()
!------------------------------------------------------------------------------
NetWebServerWorker.Mformat PROCEDURE (String p_Value,<String p_picture>)
  code
  return self.RequestData.WebServer.mFormat(p_value,p_Picture)

!------------------------------------------------------------------------------
NetWebServer.Mformat PROCEDURE (String p_Value,<String p_picture>)
ans        StringTheory
loc:pic    string(20)
x          long
  code
  if omitted(3) or p_Picture = ''
    return clip(p_Value)
  end
  loc:pic = p_Picture
  if loc:pic[1] <> '@' then loc:pic = '@' & loc:pic.
  ans.SetValue(p_Value)
  case upper(sub(loc:pic,1,2))
  of '@D'
    if instring('U',upper(loc:pic),1,1)
      p_Value = self.UnixToClarionDate(ans.GetValue())
    end
    ans.SetValue(Format(p_Value,loc:pic))
  of '@T'
    x = instring('U',upper(loc:pic),1,1)
    if x
      loc:pic[x] = ' '
      ans.SetValue(self.UnixToClarionTime(ans.GetValue()))
    end
    ans.SetValue(Format(ans.GetValue(),loc:pic))
  else
    ans.SetValue(Format(ans.GetValue(),loc:pic))
  end
  return ans.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker.Dformat PROCEDURE (String p_Value,<String p_picture>)
!digit      long
x          long
ans        String(255)
loc:value  String(255)
loc:pic    String(32)
!loc:m      long
  code
  if not omitted(3)
    loc:pic = p_picture
    !self._trace('Dformat pValue=[' & clip(p_value) & '], pic = [' & clip(p_picture) & ']')
  else
    !self._trace('Dformat pValue=[' & clip(p_value) & '], pic = [omitted]')
  end
  If len(clip(p_Value)) > size(loc:value)
    return(p_Value)
  End
  loc:value = p_Value
  ! first clean the incoming data so that it's more properly formatted
  case lower(loc:pic[1:2])
  of '@l' ! color
    case loc:pic[3]
    of '1' ! stored as Clarion Long
      ans = self.ColorClarion(loc:value)
    of '2' ! stored as Web #rrggbb string
      ans = self.ColorWeb(loc:value)
    else
      ans = loc:value
    end
    !self._trace('deformatting color from pic ' & clip(loc:pic) & ' value=' & clip(loc:value) & ' ans=' & ans)

  of '@t'
    ans = self.DformatTime(loc:value,loc:pic)
  of '@p'
  orof '@k'
  !orof ''   ! commented out in 6.36. causes string fields to deformat incorrectly. see http://www.nettalkcentral.com/index.php?option=com_smf&Itemid=36&topic=3708.0#new
    ans = left(deformat(loc:value))
  of '@d'
    ans = self.Dformatdate(loc:value,loc:pic)
  of '@e'
    ans = deformat(loc:value,loc:pic)
  of '@n' ! @n moved here in 5.00, for entry pic @n5_`2
    !self._trace('dFORMAT v=' & clip(loc:value) )
    if instring('`',loc:pic)
      loop x = 1 to len(clip(loc:value))
        if loc:value[x] = ','
          loc:value[x] = '.'
        elsif loc:value[x] = '.'
          loc:value[x] = ' '
        end
      end
    end
    if instring('(',loc:pic) and (loc:value[1] = '(' or loc:value[1] = '-')
      ans = '-' & deformat(loc:value[2 : size(loc:value)])
    else
      ans = deformat(loc:value)
    end
   ! ans = choose(instring('`',loc:pic,1,1)=0,deformat(loc:value),deformat(loc:value,loc:pic))
  of '@s'
    ans = loc:value
  else
    ans = loc:value
  end
  !self._trace('dFORMAT v=' & clip(loc:value) & ' pic=' & clip(loc:pic) & ' def(v)=' & deformat(loc:value) & ' def(v,p)' & deformat(loc:value,loc:pic) & ' ans=' & clip(ans))
  return clip(ans)
!------------------------------------------------------------------------------
NetWebServerWorker.DformatTime PROCEDURE (String p_Value,String p_pic)
ONEHOUR        Equate(360000)
HOURS12        Equate(ONEHOUR*12)
ONEMINUTE      Equate(6000)
ONESECOND      Equate(100)
ReturnValue    Long
part           string(20),dim(4)
am             long
pm             long
  code
    do parts
    if part[3]<>'' or part[2]<>''
      returnvalue = (part[1] * ONEHOUR) + (part[2] * ONEMINUTE) + (part[3] * ONESECOND) + 1
    else
      case len(clip(part[1]))
      of 1   !h
      orof 2 !hh
        returnvalue = part[1] * ONEHOUR + 1
      of 3 ! hmm
        returnvalue = sub(part[1],1,1) * ONEHOUR + sub(part[1],2,2) * ONEMINUTE + 1
      of 4 !hhmm
        returnvalue = sub(part[1],1,2) * ONEHOUR + sub(part[1],3,2) * ONEMINUTE + 1
      of 5 ! hmmss
        returnvalue = sub(part[1],1,1) * ONEHOUR + sub(part[1],2,2) * ONEMINUTE + sub(part[1],4,2) * ONESECOND + 1
      of 6 ! hhmmss
        returnvalue = sub(part[1],1,2) * ONEHOUR + sub(part[1],3,2) * ONEMINUTE + sub(part[1],5,2) * ONESECOND + 1
      end
    end
    if pm and returnvalue < HOURS12 ! still am
      returnvalue += HOURS12 ! + 12 hours
    elsif am and returnvalue >= HOURS12
      returnvalue -= HOURS12 ! - 12 hours
    end
    if instring('U',upper(p_pic),1,1)
      returnvalue = returnValue / 100
    end
    If ReturnValue = 1 and instring('b',lower(p_pic),1,1) then ReturnValue = 0.
    Return ReturnValue

parts  routine
  data
x  long
p  long
  code
    x = instring('a',lower(p_value),1,1)
    if x > 0
      p_value[x : x+1] = '  '
      am = 1
    Else
      x = instring('p',lower(p_value),1,1)
      if x > 0
        p_value[x : x+1] = '  '
        pm = 1
      end
    End

  p = 1
  p_value = left(p_value)
  loop x = 1 to len(clip(p_value))
    case p_value[x]
    of '0' to '9'
      part[p] = clip(part[p]) & p_value[x]
      if len(clip(part[p])) = 2
        p+=1
        if p > maximum(part,1) then break.
      end
    else
      if part[p] <> ''
        p += 1
        if p > maximum(part,1) then break.
      end
    end
  end

!------------------------------------------------------------------------------
NetWebServerWorker.Dformatdate PROCEDURE (String p_Value,String p_pic)
ReturnValue  Long
pic          long
part         string(20),dim(3)
numberofparts long
monthnumber  long
daypart      long
daynumber    long
monthpart    long
yearpart     long
yearnumber   long
sep          string(1)
  Code
  !self._trace('DformatDate pValue=[' & clip(p_value) & '], pic = [' & clip(p_pic) & ']')
  if p_value = '' then return ''.
  if p_pic[1] = '@' then p_pic = sub(p_pic,2,4).
  if p_pic[1] = 'D' or p_pic[1] = 'd' then p_pic = sub(p_pic,2,3).
  pic = p_pic

  do MakeParts
  ReturnValue = self.DformatDateText(NumberOfParts,part[1],part[2],part[3],sep)
  If ReturnValue
    Return ReturnValue
  End
  if part[1] = 0 and part[2] = 0 and part[3] = 0  ! needs to be after Textparts
    Return 0
  End
  do MakeYearNumber  ! if set, then has to be right. if not set then 2 digit year < 32 is there, or the year is not there at all
  do MakeMonthNumber ! set only if name used. if not set month could be

  if numberofparts = 1                   ! 1 part
    if monthpart
      returnvalue = date(monthnumber,1,year(today()))
    elsif yearpart
      returnvalue = date(1,1,yearnumber)
    elsif part[1] < 32
      ReturnValue = date(month(today()),part[1],year(today()))
    elsif p_value > 366
      ReturnValue = part[1]
    else
      do DaysInYear
    end
    Return ReturnValue
  end

  if numberofparts = 2
    ! know year for sure, month is the other part
    if yearpart and monthpart = 0
      monthpart = choose(yearpart=1,2,1)
      monthnumber = part[monthpart]
    end
    if yearpart and monthpart
      !self._trace('monthnumber=' & monthnumber & ' yearnumber = ' & yearnumber & ' yearpart=' & yearpart & ' monthpart=' & monthpart)
      returnvalue = date(monthnumber,1,yearnumber)
      return returnvalue
    end
    ! know month for sure, day is the other part
    if monthpart and yearpart = 0
      daypart = choose(monthpart=1,2,1)
      daynumber = part[daypart]
      returnvalue = date(monthnumber,daynumber,year(today()))
      return returnvalue
    end
    ! two unknown parts, check if one is > 12 (not >31 else the year would have claimed this part)
    do MakeDayNumber
    ! if know day then other part is the month part
    if daypart
      monthpart = choose(daypart=1,2,1)
      monthnumber = part[monthpart]
      returnvalue = date(monthnumber,daynumber,year(today()))
      return returnvalue
    end
    ! two unknown parts, both < 13. Use pic to give a hint as to which is which
    case pic
    of 1 orof 2 orof 3 orof 4 ! mm/dd
      daypart = 2
      monthpart = 1
      daynumber = choose(part[daypart] >= 1 and part[daypart] <= 31,part[daypart],1)
      monthnumber = choose(part[monthpart] >= 1 and part[monthpart] <=12,part[monthpart],1)
      returnvalue = date(monthnumber,daynumber,year(today()))
      return returnvalue
    of 5 orof 6 orof 7 orof 8 ! dd/mm
      daypart = 1
      monthpart = 2
      daynumber = choose(part[daypart] >= 1 and part[daypart] <= 31,part[daypart],1)
      monthnumber = choose(part[monthpart] >= 1 and part[monthpart] <=12,part[monthpart],1)
      returnvalue = date(monthnumber,daynumber,year(today()))
      return returnvalue
    else                      ! yy/mm
      yearpart = 1
      monthpart = 2
      monthnumber = choose(part[monthpart] >= 1 and part[monthpart] <=12,part[monthpart],1)
      yearnumber = choose(part[yearpart] >= 1,part[yearpart],year(today()))
      returnvalue = date(monthnumber,1,yearnumber)
      return returnvalue
    end
  end

  if numberofparts = 3
    ! know year part and monthpart, then day is the other part
    if yearpart and monthpart
      if yearpart <> 3 and monthpart <> 3 then daypart = 3
      elsif yearpart <> 2 and monthpart <> 2 then daypart = 2
      elsif yearpart <> 1 and monthpart <> 1 then daypart = 1
      end
      daynumber = choose(part[daypart] >= 1 and part[daypart] <= 31,part[daypart],1)
      returnvalue = date(monthnumber,daynumber,yearnumber)
      return returnvalue
    end
    ! know year part, try and get daypart
    if yearpart
      do MakeDayNumber
      if daypart
        if yearpart <> 1 and daypart <> 1 then monthpart = 1
        elsif yearpart <> 2 and daypart <> 2 then monthpart = 2
        else monthpart = 3
        end
        monthnumber = choose(part[monthpart] >= 1 and part[monthpart] <=12,part[monthpart],1)
      ! know yearpart, but monthpart and daypart are both < 13 so if year part =1 then y/m/d
      elsif yearpart = 1
        monthpart = 2
        monthnumber = choose(part[monthpart] >= 1 and part[monthpart] <=12,part[monthpart],1)
        daypart = 3
        daynumber = choose(part[daypart] >= 1 and part[daypart] <= 31,part[daypart],1)
      else
        ! know yearpart, but monthpart and daypart are both < 13, and yearpart > 1
        case pic
        of 1 orof 2 orof 3 orof 4 ! mm/dd
          daypart = 2
          monthpart = 1
        of 5 orof 6 orof 7 orof 8 ! dd/mm
          daypart = 1
          monthpart = 2
        else
          daypart = 2
          monthpart = 1
        end
        daynumber = choose(part[daypart] >= 1 and part[daypart] <= 31,part[daypart],1)
        monthnumber = choose(part[monthpart] >= 1 and part[monthpart] <=12,part[monthpart],1)
      end
      returnvalue = date(monthnumber,daynumber,yearnumber)
      return returnvalue
    end
    ! 3 completely undistingushable parts, use pic
    case pic
    of 1 orof 2 orof 3 orof 4 ! m/d/y
      daypart = 2
      monthpart = 1
      yearpart = 3
    of 5 orof 6 orof 7 orof 8 ! d/m/y
      daypart = 1
      monthpart = 2
      yearpart = 3
    else                      ! y/m/d
      daypart = 3
      monthpart = 2
      yearpart = 1
    end
    if part[monthpart] > 12 and part[daypart] <= 12
      daynumber = monthpart
      monthpart = daypart
      daypart = daynumber
    end
    daynumber = choose(part[daypart] >= 1 and part[daypart] <= 31,part[daypart],1)
    monthnumber = choose(part[monthpart] >= 1 and part[monthpart] <=12,part[monthpart],1)
    yearnumber = choose(part[yearpart] >= 1,part[yearpart],year(today()))
    returnvalue = date(monthnumber,daynumber,yearnumber)
    return returnvalue
  end

MakeParts  routine
  data
x    long
p    long
mode long(-1)
  code
  p = 0
  loop x = 1 to len(clip(p_value))
    case p_value[x]
    of '0' to '9'
      if mode = 2 or mode = -1
        mode = 1
        p += 1
        if p > 3 then break.
      end
      part[p] = clip(part[p]) & p_value[x]
    of ',' orof '.' orof '/' orof '-' orof ' ' orof '_' orof '\' orof '+'
      if p = 0 then p = 1.
      mode = 0
      sep = p_value[x]
      if part[p] <> ''
        p += 1
        if p > 3 then break.
      end
    of 'a' to 'z'
    orof 'A' to 'Z'
      if mode = 1 or mode = -1
        mode = 2
        p += 1
        if p > 3 then break.
      end
      part[p] = clip(part[p]) & p_value[x]
    end
  end
  if part[3] <> '' then numberofparts = 3
  elsif part[2] <> '' then numberofparts = 2
  else numberofparts = 1
  end

makedaynumber   routine
  data
x long
  code
  loop x = 1 to 3
    if part[x] >= 13 and part[x] <= 31
      daypart = x
      daynumber = part[x]
      break
    end
  end

makeyearnumber  routine
  data
x long
  code
  loop x = 1 to 3
    if (part[x] >= 1800 and part[x] <= 2100) or (part[x] > 31 and part[x] < 100)
      yearpart = x
      yearnumber = part[x]
      break
    end
  end

makemonthnumber  routine
  data
x long
  code
  monthnumber = 0
  loop x = 1 to 3
    case lower(self.interpret(part[x]))
    of self.translate('jan') orof self.translate('january')
      monthnumber = 1
    of self.translate('feb') orof self.translate('february')
      monthnumber = 2
    of self.translate('mar') orof self.translate('march')
      monthnumber = 3
    of self.translate('apr') orof self.translate('april')
      monthnumber = 4
    of self.translate('may')
      monthnumber = 5
    of self.translate('jun') orof self.translate('june')
      monthnumber = 6
    of self.translate('jul') orof self.translate('july')
      monthnumber = 7
    of self.translate('aug') orof self.translate('august')
      monthnumber = 8
    of self.translate('sep') orof self.translate('sept') orof self.translate('september')
      monthnumber = 9
    of self.translate('oct') orof self.translate('october')
      monthnumber = 10
    of self.translate('nov') orof self.translate('november')
      monthnumber = 11
    of self.translate('dec') orof self.translate('december')
      monthnumber = 12
    end
    if monthnumber = 0
      case lower(part[x,1])
      of 'f'
        monthnumber = 2
      of 's'
        monthnumber = 9
      of 'o'
        monthnumber = 10
      of 'n'
        monthnumber = 11
      !of 'd'  finds d in dd,mm,yy
      !  monthnumber = 12
      end
    end
    if monthnumber > 0
      monthpart = x
      break
    end
  end

DaysInYear  routine
  data
monthlen     long,dim(12)
x            long
y            long
  code
  y = year(today())
  if p_value = 366
    returnvalue = date(12,31,y)
    exit
  end
  monthlen[1] = 31
  monthlen[2] = monthlen[1] + choose(y%4=0 and (y%100 <> 0 or y%400 = 0),29,28)
  monthlen[3] = monthlen[2] + 31
  monthlen[4] = monthlen[3] + 30
  monthlen[5] = monthlen[4] + 31
  monthlen[6] = monthlen[5] + 30
  monthlen[7] = monthlen[6] + 31
  monthlen[8] = monthlen[7] + 31
  monthlen[9] = monthlen[8] + 30
  monthlen[10] = monthlen[9] + 31
  monthlen[11] = monthlen[10] + 30
  monthlen[12] = monthlen[11] + 31
  loop x = 2 to 12
    if p_value < monthlen[x]
      returnvalue = date(x,p_value-monthlen[x-1],year(today()))
      break
    end
  end
!------------------------------------------------------------------------------
NetWebServerWorker.DformatDateText PROCEDURE (Long pNumberofParts, String pPart1, String pPart2, String pPart3, String pSep)
t            Long
ReturnValue  Long
  code
  !self._trace('DformatDateText pNumberofParts=' & pNumberofParts & ' pPart1=[' & clip(pPart1) & ']' & ' pPart4=[' & clip(pPart2) & ']' & ' pPart3=[' & clip(pPart3) & ']')
  t = today()
  case lower(self.interpret(pPart1))
  of self.translate('week') orof 'w' orof 'u'
    ReturnValue = date(1,1,year(today()))  ! ISO 8601 standard for week numbers http://en.wikipedia.org/wiki/ISO_8601
    loop until ReturnValue % 7 = 4         ! find first thursday
      ReturnValue += 1
    end
    ReturnValue -= 3 + ((pPart2-1) *7)     ! go back to monday, and add the number of weeks.
  of self.translate('tomorrow') orof 'tm'
    ReturnValue = t+1
  of self.translate('yesterday') orof 'yd' orof 'y'
    ReturnValue = t-1
  of self.translate('today') orof 't'
      case pNumberOfParts
      of 1
        ReturnValue = t
      of 2
        if pSep = '+'
          ReturnValue = t+pPart2
        elsif pSep = '-'
          ReturnValue = t-pPart2
        end
      end
  of self.translate('next') orof 'nx'
      case lower(self.interpret(pPart2))
      of self.translate('day') orof 'd'
        returnValue = t + 1
      of self.translate('week') orof 'w'
        returnValue = t + 7
      of self.translate('month') orof 'm'
        returnValue = date(month(t)+1,1,year(t))
      of self.translate('quarter') orof 'q'
        returnValue = date(int((month(t)-1)/3)*3+4,1,year(t))
      of self.translate('year') orof 'y'
        returnValue = date(1,1,year(t)+1)
      of self.translate('mon') orof self.translate('monday')
        returnvalue = choose(1+t%7,t+1,t+7,t+6,t+5,t+4,t+3,t+2)
      of self.translate('tue') orof self.translate('tuesday')
        returnvalue = choose(1+t%7,t+2,t+1,t+7,t+6,t+5,t+4,t+3)
      of self.translate('wed') orof self.translate('wednesday')
        returnvalue = choose(1+t%7,t+3,t+2,t+1,t+7,t+6,t+5,t+4)
      of self.translate('thu') orof self.translate('thur') orof self.translate('thursday')
        returnvalue = choose(1+t%7,t+4,t+3,t+2,t+1,t+7,t+6,t+5)
      of self.translate('fri') orof self.translate('friday')
        returnvalue = choose(1+t%7,t+5,t+4,t+3,t+2,t+1,t+7,t+6)
      of self.translate('sat') orof self.translate('saturday')
        returnvalue = choose(1+t%7,t+6,t+5,t+4,t+3,t+2,t+1,t+7)
      of self.translate('sun') orof self.translate('sunday')
        returnvalue = choose(1+t%7,t+7,t+6,t+5,t+4,t+3,t+2,t+1)
      end
  of self.translate('last') orof self.translate('previous')  orof 'pv' orof 'ls'
      case lower(self.interpret(pPart2))
      of self.translate('day') orof 'd'
        returnValue = t - 1
      of self.translate('week') orof 'w'
        returnValue = t - 7
      of self.translate('month') orof 'm'
        returnValue = date(month(t)-1,1,year(t))
      of self.translate('quarter') orof 'q'
        returnValue = date(int((month(t)-1)/3)*3-2,1,year(t))
      of self.translate('year') orof 'y'
        returnValue = date(1,1,year(t)-1)
      of self.translate('mon') orof self.translate('monday')
        returnvalue = choose(1+t%7,t-6,t-7,t-1,t-2,t-3,t-4,t-5)
      of self.translate('tue') orof self.translate('tuesday')
        returnvalue = choose(1+t%7,t-5,t-6,t-7,t-1,t-2,t-3,t-4)
      of self.translate('wed') orof self.translate('wednesday')
        returnvalue = choose(1+t%7,t-4,t-5,t-6,t-7,t-1,t-2,t-3)
      of self.translate('thu') orof self.translate('thur') orof self.translate('thursday')
        returnvalue = choose(1+t%7,t-3,t-4,t-5,t-6,t-7,t-1,t-2)
      of self.translate('fri') orof self.translate('friday')
        returnvalue = choose(1+t%7,t-2,t-3,t-4,t-5,t-6,t-7,t-1)
      of self.translate('sat') orof self.translate('saturday')
        returnvalue = choose(1+t%7,t-1,t-2,t-3,t-4,t-5,t-6,t-7)
      of self.translate('sun') orof self.translate('sunday')
        returnvalue = choose(1+t%7,t-7,t-1,t-2,t-3,t-4,t-5,t-6)
      end
  End
  return ReturnValue

!------------------------------------------------------------------------------
NetWebServerWorker.NextMonth  Procedure(Long p_Date)
ans  Long
  code
  if month(p_date) = 12
    ans = date(1,1,year(p_date)+1)
  else
    ans = date(month(p_date)+1,1,year(p_date))
  end
  return ans
!------------------------------------------------------------------------------
NetWebServerWorker.PreviousMonth  Procedure(Long p_Date)
ans  Long
  code
  if month(p_date) = 1
    ans = date(12,1,year(p_date)-1)
  else
    ans = date(month(p_date)-1,1,year(p_date))
  end
  return ans
!------------------------------------------------------------------------------
NetWebServerWorker.DrawMonth  Procedure(Long p_Date,<String p_HeaderClass>,<String p_HeaderCellClass>,<String p_DayCellclass>,<String p_EmptyDayCellclass>,<String p_tip>,Long p_ViewOnly=0,Long p_MonthName=9,Long p_DayName=3,Long p_Compress=0,Long p_FirstDay=0)
Ans       String (Net:MaxBinData)
m         Long
y         Long
ditm      Long
mn                  String(40)
loc:monthclass      String(256)
loc:headerclass     String(256)
loc:headercellclass String(256)
loc:daycellclass    String(256)
loc:emptycellclass  String(256)
loc:onclick         String(256)
loc:draggable       String(10)
loc:tip             String(Net:MaxBinData)
loc:id              string(256)
loc:image           string(512)
loc:place           string(20)
loc:monthwhere      string(256)
loc:style           string(StyleStringSize)
loc:labelclass      string(StyleStringSize)
loc:labelemptyclass string(StyleStringSize)
loc:contentclass    string(StyleStringSize)
i         Long
j         Long
w         Long
z         Long
  Code
  m = month(p_date)
  y = year(p_date)
  p_date = date(m,1,y)
  ditm = self.DaysInMonth(m,y)

  if not omitted(3) ! p_HeaderClass
    loc:headerclass = ' class="' & clip(p_HeaderClass) & '"'
  end
  if not omitted(4) ! p_HeaderCellClass
    loc:headercellclass = ' class="' & clip(p_HeaderCellClass) & '"'
  end
  if not omitted(5) ! p_DayCellclass
    loc:daycellclass = p_DayCellclass
  end
  if not omitted(6) ! p_EmptyDayCellclass
    loc:emptycellclass = ' class="' & clip(p_EmptyDayCellclass) & '"'
  end

  loc:monthwhere = ' data-nt-month="'&p_date&'"'

  ans = self.divheader('d_' & month(p_date) & '_' & year(p_date) ,loc:monthclass,Net:NoSend,'',loc:monthwhere) &|
        '<div data-nt-head="header" '&clip(loc:headerclass)&'><span class="nt-heading" data-nt-heading="' & self.GetMonthName(m,y,p_MonthName) & '">A</span></div>' & self.CRLF &|
        '<div'&clip(loc:headercellclass)&'>'&|
        '<div>'&self.GetWeekDayName(0+p_FirstDay,p_DayName)&'</div><div>'&self.GetWeekDayName(1+p_FirstDay,p_DayName)&'</div>'&|
        '<div>'&self.GetWeekDayName(2+p_FirstDay,p_DayName)&'</div><div>'&self.GetWeekDayName(3+p_FirstDay,p_DayName)&'</div>'&|
        '<div>'&self.GetWeekDayName(4+p_FirstDay,p_DayName)&'</div><div>'&self.GetWeekDayName(5+p_FirstDay,p_DayName)&'</div>'&|
        '<div>'&self.GetWeekDayName(6+p_FirstDay,p_DayName)&'</div></div>'

  w = ((p_Date - p_firstDay) % 7) + 1 ! column of first day, 1 to 7
  z = 1
  loop i = 1 to 6 ! 6 weeks in a month
    if p_compress and z > ditm
      break
    end
    ans = clip(ans) & '<div class="nt-wide">' & self.CRLF
    loop j = 1 to 7
      if (i > 1 or j >= w) and z <= ditm
        if not omitted(7) ! p_tip
          if self.MonthData[z].tip=''
            self.MonthData[z].Tip = self.Translate(p_tip)
          end
        end
        loc:tip = ''
        loc:id = ''
        loc:place = ''
        loc:style = ''
        loc:image = ''

        case self.MonthData[z].info
        of 'i'
        orof 'r'
          loc:id = ' data-nt-info="'&clip(self.MonthData[z].info) & '"'
          if self.MonthData[z].hash
            loc:id = clip(loc:id ) & ' data-nt-id="uk"'
          end
        else
          if self.MonthData[z].hash
            loc:id = ' data-nt-form="'&clip(self.MonthData[z].form)&'" data-nt-id="'&clip(self.MonthData[z].hash)&'"'
          end
        end
        if self.MonthData[z].hash
          if self.MonthData[z].tip
            loc:tip = ' title="'&clip(self.MonthData[z].tip)&'"'
          end
          if self.MonthData[z].place
            loc:place = ' data-nt-place="'&clip(self.MonthData[z].place)&'"'
          end
          if self.MonthData[z].imageBig
            loc:image = ' data-nt-image-big="'&clip(self.MonthData[z].imageBig)&'"'
          end
          if self.MonthData[z].imageSmall
            loc:image = clip(loc:image) & ' data-nt-image-small="'&clip(self.MonthData[z].imageSmall)&'"'
          end
        end
        if self.MonthData[z].style
          loc:style = ' style="'&clip(self.MonthData[z].style)&'"'
        end
        ans = clip(ans) & '<div data-nt-date="'&date(m,z,y)&'" class="'& clip(loc:daycellclass) & ' ' & clip(self.MonthData[z].css)&'"' & clip(loc:id) & clip(loc:tip) & clip(loc:place) & clip(loc:style) & clip(loc:image)&'>'

        if self.MonthData[z].label=''
          !self.MonthData[z].label = z
        end
        If self.MonthData[z].content
          ans = clip(ans) & '<div'&clip(loc:labelclass)&'>' & clip(self.MonthData[z].label) & '</div><div'&clip(loc:contentclass)&'>' & clip(self.MonthData[z].content) & '</div></div>'
        Else
          ans = clip(ans) & '<div'&clip(loc:labelemptyclass)&'>' & clip(self.MonthData[z].label) & '</div></div>'
        End
        z += 1
      else
        ans = clip(ans) & '<div'&clip(loc:emptycellclass)&'>&#160;</div>'  !&#160;
      end
    end
    ans = clip(ans) & '</div>' & self.CRLF
  end
  ans = clip(ans) & self.DivFooter(Net:NoSend)
  Return clip(ans)
!------------------------------------------------------------------------------
NetWebServerWorker.DrawMonths  Procedure(String p_Proc, String p_Div, Long p_date=0, Long p_Months=1, <String p_Setclass>,<String p_HeaderClass>,<String p_HeaderCellClass>,<String p_DayCellclass>,<String p_EmptyDayCellclass>,<String p_Tip>,Long p_ViewOnly=0,Long p_MonthName=9,Long p_DayName=3,Long p_Compress=0,Long p_FirstDay=0)
Packet     String(Net:MaxBinData)
m          Long
y          Long
rowcounter long
colcounter long
loc:class  String(StyleStringSize)
donemonths Long

  Code
  if p_date = 0 then p_date = today().
  if p_Months <= 0 then p_Months = 1.

  m = month(p_date)
  y = year(p_date)
  if day(p_date) <> 1 then p_date = date(m,1,y).
  if not omitted(7)
    loc:class = p_SetClass
  end
  self.DivHeader(clip(p_Div)&'_cal',loc:class)
  loop Donemonths = 1 to p_Months
    ! get data for month
    self.PopulateMonthData(p_proc,Date(m,1,y))
    ! create html for month
    packet = self.DrawMonth(date(m,1,y),p_HeaderClass,p_HeaderCellClass,p_DayCellClass,p_EmptyDayCellClass,p_tip,p_ViewOnly,p_MonthName,p_DayName,p_Compress,p_FirstDay)
    ! send html
    do SendPacket
    !Go to the next month
    m = choose(m=12,1,m+1)
    y = choose(m=1, y+1, y)
  end
  !Finish off the calendar
  self.DivFooter()
  Return

SendPacket  routine
  self.ParseHTML(packet,1,0,NET:NoHeader)
  packet = ''

!------------------------------------------------------------------------------
NetWebServerWorker.GetPlannerQueueRecordByTime  Procedure(Long p_time)
x  long
  code
  if records(self.plannerQueue) = 0 then return 1.
  loop x = records(self.plannerQueue) to 1 by -1
    get(Self.PlannerQueue,x)
    if Self.PlannerQueue.time <= p_time
      break
    end
  end
  return 0
!------------------------------------------------------------------------------
NetWebServerWorker.GetPlannerQueueRecordByDate  Procedure(Long p_date)
x  long
  code
  if records(self.plannerQueue) = 0 then return 1.
  Self.PlannerQueue.Date = p_date
  get(Self.PlannerQueue,Self.PlannerQueue.Date)
  return errorcode()
!------------------------------------------------------------------------------
NetWebServerWorker.DrawPlanRow   Procedure(String p_ParentId,NetPlannerDataQueueType p_Queue)
Packet     String(Net:MaxBinData)
loc:data     String(Net:MaxBinData)
y  Long
  code
    loc:data = 'data-nt-row="data" data-nt-parent="' & clip(p_ParentId) & '"'
    self.DivHeader(p_ParentId,'planner-row-size nt-relative',Net:Send+Net:Crc,,loc:data)

    loop y = 1 to records(p_Queue)
      Get(p_Queue,y)
      loc:data = ''
      If p_Queue.StartDate or p_Queue.StartTime
        loc:data = clip(loc:data) & self.wrap('data-nt-start-date',p_Queue.StartDate)
        loc:data = clip(loc:data) & self.wrap('data-nt-start-time',p_Queue.StartTime)
      end
      If p_Queue.EndDate or p_Queue.EndTime
        if p_Queue.EndTime > 8640000
          p_Queue.EndDate += (p_Queue.EndTime/8640000)
          p_Queue.EndTime = p_Queue.EndTime % 8640000
        end
        loc:data = clip(loc:data) & self.wrap('data-nt-end-date',p_Queue.EndDate)
        loc:data = clip(loc:data) & self.wrap('data-nt-end-time',p_Queue.EndTime)
      end
      loc:data = clip(loc:data) & self.wrap('data-nt-column',p_Queue.column)
      loc:data = clip(loc:data) & self.wrap('data-nt-id',p_Queue.Hash)
      loc:data = clip(loc:data) & self.wrap('data-nt-form',p_Queue.Form)
      loc:data = clip(loc:data) & self.CreateTip(p_Queue.Tip)
      if p_Queue.Css
        loc:data = clip(loc:data) & ' class="nt-hard-left planner-data-width planner-data-height planner-data ui-corner-all nt-hidden '&clip(p_Queue.Css)&'"'
      Else
        loc:data = clip(loc:data) & ' class="nt-hard-left planner-data-width planner-data-height planner-data ui-corner-all nt-hidden"'
      End
      loc:data = clip(loc:data) & self.wrap('style',p_Queue.Style)
      packet = clip(packet) & '<div '&clip(loc:data)&'>' & clip(p_Queue.Content) & '</div>' & self.CRLF
      do SendPacket
    end
    self.DivFooter()

SendPacket  routine
  self.ParseHTML(packet,1,0,NET:NoHeader)
  packet = ''

!------------------------------------------------------------------------------
NetWebServerWorker.DrawPlan     Procedure(String p_Div,Long p_date=0, Long p_columns=7,Long p_Height=52,String p_header,<String p_HeaderClass>,<NetHeaderQueueType p_Headers>)
Packet          String(Net:MaxBinData)
x               Long
y               Long
loc:headerclass String(StyleStringSize)
hdr:name        string(255)
hdr:css         string(StyleStringSize)
loc:tip         string(Net:MaxBinData)
  code
  if not omitted(7)
    loc:headerclass = p_HeaderClass
  end
  self.DivHeader(clip(p_Div)&'_cal')
  ! fixed column(s) on left
  self.DivHeader(clip(p_Div)&'_users','nt-hard-left cal-names')
  packet='<div class="ui-corner-left">' & self.CRLF
  packet = clip(packet) & '<div data-nt-row="name-header" class="planner-header-row-size">' & self.CRLF
  packet=clip(packet) & '<div class="'& clip(loc:headerclass) &' ui-widget-header ui-corner-top planner-names-width planner-header-data-height">'& self.translate(p_header) &'</div>' & self.CRLF ! this is the header on the left
  do SendPacket
  Loop y = 1 to records(self.PlannerQueue)
    get(self.PlannerQueue,y)
    packet = '<div data-nt-row="name" class="planner-row-size" style="height:'&p_Height&'px;">' & self.CRLF
    loc:tip = self.CreateTip(self.PlannerQueue.tip)
    packet = clip(packet) & '<div class="planner-names-width planner-data-height planner-border '&clip(self.PlannerQueue.css)&'"'&clip(loc:tip)&'>' & clip(self.PlannerQueue.Name) & '</div>' & self.CRLF
    packet = clip(packet) & '</div>' & self.CRLF
    do SendPacket
  End
  packet='</div>' & self.CRLF                 ! </data-nt-row="header">
  packet = clip(packet) &'</div>' & self.CRLF ! ui-corner-left
  do SendPacket
  self.DivFooter()

  self.DivHeader(clip(p_Div)&'_resize','nt-left')                !d1
  self.DivHeader(clip(p_Div)&'_columns','nt-left cal-scroll')       !d2
  packet='<div id="a12" class="cal-planner-table ui-corner-right">' & self.CRLF   !e1

  packet = clip(packet) & '<div data-nt-row="header" class="planner-header-row-size">' & self.CRLF      !e2
  loop y = 1 to p_Columns
    if omitted(8)
      hdr:name = day(p_date+y-1) & ' ' & self.GetMonthName(month(p_date+y-1),0,netName:Short)
      hdr:css = ''
      loc:tip = ''
    Else
      p_Headers.column = y
      Get(p_Headers,p_Headers.column)
      If ErrorCode() or p_headers.Header = ''
        hdr:name = day(p_date+y-1) & ' ' & self.GetMonthName(month(p_date+y-1),0,netName:Short)
      Else
        hdr:name = p_headers.Header
      End
      loc:tip = self.CreateTip(p_headers.tip)
      hdr:css = p_Headers.Css
    end
    packet = clip(packet) & '<div class="'& clip(hdr:css) &' ui-widget-header ui-corner-top nt-hard-left planner-data-width planner-header-data-height" '&clip(loc:tip)&'>' & |
                            self.Translate(hdr:name) & |
                            '</div>' & self.CRLF
  end
  packet = clip(packet) &'</div>' & self.CRLF ! </data-nt-row="header"> !e2
  do SendPacket

  Loop x = 1 to records(self.PlannerQueue)
    get(self.PlannerQueue,x)
    self.DrawPlanRow(self.PlannerQueue.Hash,self.PlannerQueue.Data)
  End

  packet='</div>' & self.CRLF  !e1
  do SendPacket
  self.DivFooter()
  self.DivFooter()
  self.DivFooter()
  !self.DivFooter() ! removed in 6.18
  self.emptyPlannerQueue()

SendPacket  routine
  self.ParseHTML(packet,1,0,NET:NoHeader)
  packet = ''

!------------------------------------------------------------------------------
NetWebServerWorker.GetMonthName PROCEDURE  (Long p_month,Long p_year=0,Long p_Flag=0)
ans  string(255)
  CODE
! this method returns the Name, Abreviation, or Initial for a month.

  if p_month < 1 or p_month > 12 then p_month = 1.
  execute p_month
    ans = self.Translate('January')
    ans = self.Translate('February')
    ans = self.Translate('March')
    ans = self.Translate('April')
    ans = self.Translate('May')
    ans = self.Translate('June')
    ans = self.Translate('July')
    ans = self.Translate('August')
    ans = self.Translate('September')
    ans = self.Translate('October')
    ans = self.Translate('November')
    ans = self.Translate('December')
  end
  case band(p_flag,NetName:All)
  of NetName:Full
    ! nothing to do, ans is already set
  of NetName:Short
    ans = ans[1:3]
  of NetName:Initial
    ans = ans[1]
  end
  if band(p_flag,NetName:Year) and p_Year <> 0
    ans = clip(ans) & ' ' & p_Year
  end
  return(clip(ans))

!------------------------------------------------------------------------------
NetWebServerWorker.GetDayName PROCEDURE  (Long p_day)
ans  string(10)
  CODE
! returns the "short" name for a number, ie 1st, 2nd, 3rd and so on.
  case p_day
  of 1
  orof 21
  orof 31
    ans = 'st'
  of 2
  orof 22
    ans = 'nd'
  of 3
  orof 23
    ans = 'rd'
  else
    ans = 'th'
  end
  ans = p_day & ans
  return(clip(ans))

!------------------------------------------------------------------------------
NetWebServerWorker.GetWeekdayName PROCEDURE  (Long p_date,Long p_Flag=0)
ans  string(255)
  CODE
  execute (p_date % 7)+1
    ans = self.Translate('Sunday')
    ans = self.Translate('Monday')
    ans = self.Translate('Tuesday')
    ans = self.Translate('Wednesday')
    ans = self.Translate('Thursday')
    ans = self.Translate('Friday')
    ans = self.Translate('Saturday')
  end

  case band(p_flag,NetName:All)
  of NetName:Full  ! full name
  of NetName:Short  ! short name
    ans = ans[1:3]
  of NetName:Initial ! initial
    ans = ans[1]
  end
  return(clip(ans))

!------------------------------------------------------------------------------
NetWebServerWorker.DaysInMonth            Procedure(Long p_month,Long p_Year)
Ans Long
  Code
  Execute p_month
    ans = 31
    ans = 28
    ans = 31
    ans = 30
    ans = 31
    ans = 30
    ans = 31
    ans = 31
    ans = 30
    ans = 31
    ans = 30
    ans = 31
  End
  if p_month = 2
    if p_year%100 = 0
      if p_year%400 = 0
        ans = 29
      end
    elsif p_year%4 = 0
      ans = 29
    end
  end
  Return ans
!------------------------------------------------------------------------------
! WebHandler uses p_proc to route the call to the correct procedure.
NetWebServerWorker.PopulateMonthData      Procedure(String p_proc,Long p_Date)
  Code
  ! Pure virtual method.
  ! in your code populate the MonthData property
!------------------------------------------------------------------------------
NetWebServerWorker.EmptyPlannerQueue  Procedure()
  Code
  if not self.PlannerQueue &= NULL
    loop while records(self.PlannerQueue)
      get(self.PlannerQueue,1)
      if not self.PlannerQueue.Data &= Null
        free(self.PlannerQueue.Data)
        dispose(self.PlannerQueue.Data)
      end
      delete(self.PlannerQueue)
    End
  End
!------------------------------------------------------------------------------
NetWebServerWorker._FirstField            PROCEDURE (String p_SortOrder)
x long
y long
ans  String(256),auto
  Code
  ans = p_SortOrder
  ! remove any secondary variables.
  x = instring(',',ans,1,1)
  if x then ans = sub(ans,1,x-1).
  ! remove leading + and -
  if ans[1] = '+' or ans[1] = '-' then ans = sub(ans,2,256).
  ! remove UPPER
  x = instring('(',ans,1,1)
  y = instring(')',ans,1,1)
  if x and y and y > x + 1 then ans = ans[x+1 : y-1].
  Return Clip(Left(ans))
!------------------------------------------------------------------------------
NetWebServerWorker.PrimeForLookup   Procedure(File p_File, Key p_Key,string p_field)
k            &key
loc:record   &group
loc:field    ANY
loc:idfield  ANY
x            Long
loc:value    string(255),dim(Net:MaxKeyFields)

  code
  !self._Trace('PrimeForLookup  LF=' & self.GetValue('LookupField') & ' V=' & self.GetValue(self.GetValue('LookupField')))
  compile('***',_VER_C55)
  loc:record &= p_File{prop:record}
  loc:value[1] = self.GetValue(self.GetValue('LookupField'))
  if self._LoadRecord(p_file,p_Key,loc:value)
    loc:field &= What(loc:record,self._FindField(p_file,p_field))
    loc:field = self.GetValue(self.GetValue('LookupField'))
    x = self._FindKey(p_File,p_field)
    if x > 0
      k &= p_File{prop:key,x}
      Set(k,k)
      Next(p_File)
    End
  End
  loc:idfield &= What(loc:record,self._FindField(p_file,self.GetValue('ForeignField')))
  self.SetValue('IDField',self.GetValue('ForeignField'))
  self.SetValue(self.GetValue('ForeignField'),loc:idfield,,net:raw)
  ***

!------------------------------------------------------------------------------
NetWebServerWorker._FindField    Procedure(*File p_File,String p_Field)
x                 long
Ans               Long
  CODE
  compile('***',_VER_C55)     ! Clarion 5.5 or above only !
    loop x = 1 to p_File{prop:fields}
      ! p_field is the label not the name of the field.
      if clip(upper(p_File{prop:label,x})) = clip(upper(p_Field))
        ans = x
        break
      End
    end
  ***
  return ans

!------------------------------------------------------------------------------
NetWebServerWorker._FindKey   Procedure(*File p_File,String p_Name)
x    long
ans  Long
k    &key
  code
  compile('***',_VER_C55)
  loop x = 1 to p_File{prop:keys}
    k &= p_File{prop:key,x}
    If k{prop:components} > 0
      If upper(self._SetLabel(p_File,k{PROP:Field,1})) = upper(p_Name)
        ans = x
        break
      End
    end
  end
  ***
  Return Ans

!------------------------------------------------------------------------------
NetWebServerWorker._SetLabel   Procedure(*File p_File,Long p_x)
n Long
Ans  string(255)
loc:record   &group
  Code
  loc:record &= p_File{prop:record}
  ans = who(loc:record,p_x)  ! tip . WHO returns the external name if it exists.
  n = Instring('|',ans,1,1)
  if n
    ans = sub(ans,1,n-1)
  end
  loop n = 1 to p_File{prop:fields}
    if p_File{prop:name,n} <> ''
      if upper(ans) = upper(p_File{prop:name,n})
        ans = p_File{prop:label,n}
        break
      end
    end
  End
  return(clip(ans))
!------------------------------------------------------------------------------
NetWebServerWorker.Popup                  Procedure(<String p_Text>,Long p_Send=0)
  code
  If self.popUpDone = 0
    If Omitted(2) or p_Text = ''
       if self.GetValue('Alert') <> ''
         self.popUpDone = 1
         self.Script('ntAlert(''' & self.jsParm(self._utfjs(self.GetValue('Alert'))) & ''','''&self.translate('Alert')&''');',p_Send)
       end
    Else
      self.popUpDone = 1
      self.Script('ntAlert(''' & self.jsParm(self._utfjs(p_text)) & ''','''&self.translate('Alert')&''');',p_Send)
    End
  End
  return !''

!------------------------------------------------------------------------------
! default class, bdiv, default image /images/_busy.gif
NetWebServerWorker.Busy                Procedure(Long p_Send=0)
packet     StringTheory
loc:class  String(StyleStringSize)
loc:ver    string(20)
x   long
  code
  If self._busydone = 0
    If self.IsMobile() = 0
      loc:ver = NETTALK:Version
      loop
        x = instring('.',loc:ver,1,1)
        if x = 0 then break.
        loc:ver = sub(loc:ver,1,x-1) & sub(loc:ver,x+1,20)
      end
      packet.SetValue('<div id="_busy" class="'&clip(self.site.busyclass)&'" data-nt-busy="busy">' & |
                      '<img src="'&clip(self.site.busyimage)&'" />' & |
                      '</div>' & self.CRLF)
      if self.site.NoJavaScriptCheck = 0
        packet.Append('<div id="_ver'& clip(loc:ver) &'" class="nt-alert ui-state-error ui-corner-all">Error in site JavaScript</div>' & self.CRLF)
        self.script('versionCheck('''&NetTalk:Version&''');')
      End
      If self.Site.NoScreenSizeCheck = 0
        self.script('getScreenSize();')
      End
    Else
      If self.Site.NoScreenSizeCheck = 0
        self.script('getScreenSize();')
      End
    End
    if p_send = 1
      Self.ParseHTML(packet,1,0,NET:NoHeader)
      packet.setvalue('')
    end
    self._busydone = 1
  End
  return clip(packet.GetValue())

!------------------------------------------------------------------------------
NetWebServerWorker.Message                Procedure(String p_div,<String p_message>,<String p_class>,Long p_Send=0)
loc:SendString   String(2048)
loc:class        String(StyleStringSize)
  code
  if omitted(3)
    loc:SendString = self.GetValue('message')
  else
    loc:SendString = p_message
  end
  loc:SendString = self.Translate(loc:SendString)
  if omitted(4)
    loc:class = self.site.MessageClass ! self._MessageClass
  else
    loc:class = p_class
  end
  if loc:sendstring = ''
    loc:class = clip(loc:class) & ' ui-helper-hidden'
  else
    loc:class = clip(loc:class) & ' nt-inline'
  end
  if p_send = 1 and self.MessageDone = 0 and self.Ajax
    if loc:sendstring = ''
      self.script('$(''#' & self.GetValue('_alertProc_') & '_frm'').ntform(''hideMessage'');');
    else
      self.script('$(''#' & self.GetValue('_alertProc_') & '_frm'').ntform(''showMessage'',''' & clip(loc:sendString) & ''');');
    end
  elsif p_send = 1 and self.MessageDone = 0
    self.DivHeader(p_div,loc:class)
    do SendString
    self.DivFooter()
    self.MessageDone = 1
  elsif self.Ajax and self.MessageDone = 0
    loc:SendString = self.DivHeader(p_div,loc:class,0) & self._jsok(loc:sendstring) & self.DivFooter(0)
    self.MessageDone = 1
  elsif self.MessageDone = 0
    loc:SendString = self.DivHeader(p_div,loc:class,0) & clip(loc:sendstring) & self.DivFooter(0)
    self.MessageDone = 1
  end
  return clip(loc:SendString)

SendString Routine
  if len(clip(loc:SendString))
    Self.SendString (loc:SendString, 1, len(clip(loc:SendString)), NET:NoHeader)
  end
!------------------------------------------------------------------------------
NetWebServerWorker.AddLog  Procedure(String p_Data)
  code
  self.requestdata.webserver.AddLog(p_Data)

!------------------------------------------------------------------------------
! file must be open before calling this
NetWebServerWorker._PreCopyRecord  PROCEDURE (FILE p_file, KEY p_key,Long p_AutoNumbered=0)
err  long
  code
  if p_AutoNumbered = Net:Web:Autonumbered
    self._SaveFields(p_file,p_key,Net:Web:ValueQueue,'**')
    err = self._loadrecord(p_file,p_key)
    self._RestoreFields(p_file,p_key,Net:Web:ValueQueue,'**')
  else
    !self._openfile(p_file)
    err = self._loadrecord(p_file,p_key)        ! prime key fields from local queue & fetch record
    self._clearKeyFields(p_file,p_key)
    !self._closefile(p_file)
  end
!------------------------------------------------------------------------------
! there are bugs in the View Reset code. Closing and Opening a view appears to
! refresh the view engine, and workaround some of these bugs.
NetWebServerWorker._BounceView      PROCEDURE (VIEW p_View)
f   string(4096),auto
o   string(2048),auto
  Code
  f = p_View{prop:filter}
  o = p_View{prop:order}
  Close(p_View)
  Open(p_View)
  p_View{prop:filter} = clip(f)
  p_View{prop:order} = clip(o)
  Set(p_View)
!------------------------------------------------------------------------------
NetWebServerWorker.CRC32    Procedure (string p_text,Long p_len=0)
Ans  ULong
  Code
  if p_len = 0 then p_len = len(clip(p_text)).
  ans = CRC32(p_text,p_len,0)
  return ans
!------------------------------------------------------------------------------
NetWebServerWorker.Translate  PROCEDURE (<String p_String>,Long p_AllowHtml=0)
  code
  if lower(self.site.htmlcharset) = 'utf-8'
    if self.site.StoreDataAs = net:StoreAsUTF8
      return(self.RequestData.WebServer.Translate(p_String,bor(p_AllowHtml,Net:Utf8)))
    else
      return(self.RequestData.WebServer.Translate(self.AsciiToUtf(p_String),bor(p_AllowHtml,Net:Utf8)))
    end
  Else
    return(self.RequestData.WebServer.Translate(p_String,p_AllowHtml))
  End

!------------------------------------------------------------------------------
NetWebServerWorker.Interpret  PROCEDURE (String p_String)
  code
  return clip(p_String)

!------------------------------------------------------------------------------
! note that this behaves like css text-transform capitalize. ie it does NOT lower anything.
NetWebServerWorker.Capitalize  PROCEDURE (String p_Text)
ans  string(1024)
x    long
  Code
  ans = p_Text
  ans[1] = upper(ans[1])
  loop x = 2 to len(clip(p_Text))
    if ans[x-1] = ' '
      ans[x] = upper(ans[x])
    end
  end
  return clip(ans)
!------------------------------------------------------------------------------
NetWebServerWorker.Noop  PROCEDURE ()
packet  string(50)
  code
  if not self.replyCompleted
    if self._OkSent = 0
      self._OkSent = 1
      packet = 'HTTP/1.1 200 OK' ! definitly no <13,10>
      self.SendStringNow(packet,1,len(clip(packet)),NET:NoHeader,false) ! don't want the whole header
      self._nooptime = clock()
    else
      if self._nooptime > clock() or clock() - self._nooptime > self.NoopGap
        self.SendStringNow(self.noopchar,1,len(clip(self.noopchar)),NET:NoHeader,false) ! sends just 1 extra . after the OK in the response string
        self._nooptime = clock()
      end
    end
  end
  return
!------------------------------------------------------------------------------
NetWebServerWorker.AddSettings            PROCEDURE ()
ans  string(Net:FormStateSize)
  code
  ans =  self.RequestData.WebServer._AddSettingsQueue (self.SessionID,self.formsettings.file,self.formsettings.key,self.formsettings.action,self.formsettings.parentpage,self.formsettings.proc,self.formsettings.RecordId,self.GetValue('FormState'),self.formsettings.target,self.formsettings.originalAction)
  !self._trace('Adding Settings ' & ans & ' parent state=' & self.GetValue('FormState') & ' session=' & clip(self.SessionID))
  return ans

!------------------------------------------------------------------------------
NetWebServerWorker.GetSettings            PROCEDURE (String p_FormState)
x  long
  code
  If self.RequestData.WebServer._GetSettingsQueue (self.SessionID, p_FormState, self.FormSettings) = 0
    self.FormState = p_FormState
    loop x = 1 to Net:MaxKeyFields
      if self.formsettings.FieldName[x]
        self.DeleteValue(self.formsettings.FieldName[x])
      end
    end
  Else
    self.FormState = ''  ! if formstate not cleared, form won't create a new formstate.
  End
  !self._trace('2 Get Settings ' & p_FormState)
  return

!------------------------------------------------------------------------------
NetWebServerWorker.DeleteSettings         PROCEDURE (String p_FormState)
  code
  !self._trace('Delete Settings ' & p_FormState)
  self.RequestData.WebServer._DeleteSettingsQueue (self.SessionID, p_FormState)
  clear(self.FormSettings)
  clear(self.FormState)
  return
!------------------------------------------------------------------------------
NetWebServerWorker.PopSettings         PROCEDURE (String p_FormState)
temp   String(Net:formStateSize)
  code
  !self._trace('Pop Settings ' & p_FormState)
  self.GetSettings(p_FormState)
  temp = self.FormSettings.ParentState
  self.DeleteSettings(p_FormState)
  if temp
    self.GetSettings(temp)
  else
    clear(self.formsettings)
    clear(self.formstate)
  end
  return
!------------------------------------------------------------------------------
NetWebServerWorker.jQuery  PROCEDURE (String p_id,String p_Action,StringTheory p_options,<String p_more>,Long p_Html=0,Long p_First=0)
  code
  self.jQuery(p_id,p_Action,p_options.GetValue(),p_more,p_html,p_First)

!------------------------------------------------------------------------------
NetWebServerWorker.jQuery  PROCEDURE (String p_id,String p_Action,String p_options,<String p_more>,Long p_Html=0,Long p_First=0)
options      StringTheory
comma        long
colon        long
  code
  options.SetValue(p_options,st:clip)
  if options.Length()
    colon = options.instring (':')
    if colon
      Comma = options.instring(',')
      if comma = 0 or comma > colon
        options.Prepend('{{')
        options.Append('}')
      end
    end
  end
  If omitted(5)
    self.script( |
                '$("' & self._nocolon(p_id)&'").' & clip(p_action) & '('& options.GetValue() &');'&|
                 self.CRLF,Net:Send,p_Html,p_First)
  Else
    self.script( |
                '$("'&self._nocolon(p_id)&'").'&clip(p_action)&'('& options.GetValue() &')'&clip(p_more)&';'&|
                 self.CRLF,Net:Send,p_Html,p_First)
  End
  return
!------------------------------------------------------------------------------
NetWebServerWorker.Combine   Procedure (<String p_str1>,<String p_str2>,<String p_str3>,<String p_str4>,<String p_str5>)
str    cstring(252),dim(5)
x      long
y      long
  code
  if not omitted(2) then str[1] = p_str1.
  if not omitted(3) then str[2] = p_str2.
  if not omitted(4) then str[3] = p_str3.
  if not omitted(5) then str[4] = p_str4.
  if not omitted(6) then str[5] = p_str5.

  loop x = 5 to 2 by -1
    if str[x] <> '' and str[x,1] <> ' '
      loop y = x-1 to 1 by -1
        str[y] = ''
      end
    end
  end
  return clip(str[1]) & clip(str[2]) & clip(str[3]) & clip(str[4]) & clip(str[5])
!------------------------------------------------------------------------------
NetWebServerWorker.RandomId    Procedure (Long p_Length=4)
ans       string(255)
alphabet  string('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
x   long
  code
  loop x = 1 to p_Length
    ans[x] = alphabet[random(1,size(alphabet))]
  end
  return clip(ans)
!------------------------------------------------------------------------------
NetWebServerWorker.Wrap    Procedure (<String p_attr>,<String p_Value>)
  code
  if omitted(3) or p_Value = ''
    return ''
  Else
    if omitted(2)
      return clip(p_Value)
    else
      return ' ' & clip(p_attr) & '="' & clip(p_value) & '"'
    end
  End
!------------------------------------------------------------------------------
NetWebServerWorker.UtfToAscii   Procedure (String p_text)
  compile('***',_VER_C60)
ReturnValue  String(Len(p_text))
  ***
  omit('***',_VER_C60)
ReturnValue  String(Net:MaxBinData)
  ***
x   long
y   long
z   long
utf     long
utfmap  Long,dim(9000)
  code
  !self._trace('UtfToAscii ' & clip(p_text) & ' self.site.StoreDataAs=' & self.site.StoreDataAs)
  case self.site.StoreDataAs
  of net:StoreAsPolish
  orof net:StoreAsCentralEurope
    utfmap[260] = 0A5h  ! start polish
    utfmap[261] = 0B9h
    utfmap[280] = 0CAh
    utfmap[281] = 0EAh
    utfmap[211] = 0D3h
    utfmap[243] = 0F3h
    utfmap[262] = 0C6h
    utfmap[263] = 0E6h
    utfmap[321] = 0A3h
    utfmap[322] = 0B3h
    utfmap[323] = 0D1h
    utfmap[324] = 0F1h
    utfmap[346] = 08Ch
    utfmap[347] = 09Ch
    utfmap[377] = 08Fh
    utfmap[378] = 09Fh
    utfmap[379] = 0AFh
    utfmap[380] = 0BFh ! end polish

  of net:StoreAsSpanish
  orof net:StoreAsPortuguese
  orof net:StoreAsFrench
  orof net:StoreAsWesternEurope
  orof net:StoreAsAscii

  of net:StoreAsDanish
    utfmap[8364] = 128

  of net:StoreAsBaltic
    utfmap[260] = 192
    utfmap[261] = 224
    utfmap[268] = 200
    utfmap[269] = 232
    utfmap[278] = 203
    utfmap[279] = 235
    utfmap[280] = 198
    utfmap[281] = 230
    utfmap[302] = 193
    utfmap[303] = 225
    utfmap[352] = 208
    utfmap[353] = 240
    utfmap[362] = 219
    utfmap[363] = 251
    utfmap[370] = 216
    utfmap[371] = 248
    utfmap[381] = 222
    utfmap[382] = 254
                      ! end baltic

of net:StoreAsEastEurope
    ! croatian
    utfmap[382] = 158  ! changed from 190
    utfmap[381] = 142  ! changed from 174
    utfmap[268] = 200
    utfmap[269] = 232
    utfmap[263] = 230
    utfmap[262] = 198
    utfmap[353] = 154  ! changed from 185
    utfmap[352] = 138  ! changed from 169
    utfmap[273] = 240
    utfmap[272] = 208

    ! Romanian
    utfmap[194] = 194   ! Capital A-circumflex
    utfmap[226] = 226   ! Lowercase a-circumflex
    utfmap[206] = 206   ! Capital I-circumflex
    utfmap[238] = 238   ! Lowercase i-circumflex

    utfmap[258] = 195   ! Capital A-breve
    utfmap[259] = 227   ! Lowercase a-breve

    utfmap[350] = 170   ! Capital S-cedilla
    utfmap[351] = 186   ! Lowercase s-cedilla
    utfmap[354] = 222   ! Capital T-cedilla
    utfmap[355] = 254   ! Lowercase t-cedilla
    utfmap[536] = 170   ! Capital s-comma - stored as cedilla
    utfmap[537] = 186   ! Lowercase s-comma - stored as cedilla
    utfmap[538] = 222   ! Capital T-comma - stored as cedilla
    utfmap[539] = 254   ! Lowercase t-comma - stored as cedilla

  of net:StoreAsArabic ! start Arabic
    utfmap[020ACh] = 080h    ! #EURO SIGN
    utfmap[0067Eh] = 081h    ! #ARABIC LETTER PEH
    utfmap[0201Ah] = 082h    ! #SINGLE LOW-9 QUOTATION MARK
    utfmap[00192h] = 083h    ! #LATIN SMALL LETTER F WITH HOOK
    utfmap[0201Eh] = 084h    ! #DOUBLE LOW-9 QUOTATION MARK
    utfmap[02026h] = 085h    ! #HORIZONTAL ELLIPSIS
    utfmap[02020h] = 086h    ! #DAGGER
    utfmap[02021h] = 087h    ! #DOUBLE DAGGER
    utfmap[002C6h] = 088h    ! #MODIFIER LETTER CIRCUMFLEX ACCENT
    utfmap[02030h] = 089h    ! #PER MILLE SIGN
    utfmap[00679h] = 08Ah    ! #ARABIC LETTER TTEH
    utfmap[02039h] = 08Bh    ! #SINGLE LEFT-POINTING ANGLE QUOTATION MARK
    utfmap[00152h] = 08Ch    ! #LATIN CAPITAL LIGATURE OE
    utfmap[00686h] = 08Dh    ! #ARABIC LETTER TCHEH
    utfmap[00698h] = 08Eh    ! #ARABIC LETTER JEH
    utfmap[00688h] = 08Fh    ! #ARABIC LETTER DDAL
    utfmap[006AFh] = 090h    ! #ARABIC LETTER GAF
    utfmap[02018h] = 091h    ! #LEFT SINGLE QUOTATION MARK
    utfmap[02019h] = 092h    ! #RIGHT SINGLE QUOTATION MARK
    utfmap[0201Ch] = 093h    ! #LEFT DOUBLE QUOTATION MARK
    utfmap[0201Dh] = 094h    ! #RIGHT DOUBLE QUOTATION MARK
    utfmap[02022h] = 095h    ! #BULLET
    utfmap[02013h] = 096h    ! #EN DASH
    utfmap[02014h] = 097h    ! #EM DASH
    utfmap[006A9h] = 098h    ! #ARABIC LETTER KEHEH
    utfmap[02122h] = 099h    ! #TRADE MARK SIGN
    utfmap[00691h] = 09Ah    ! #ARABIC LETTER RREH
    utfmap[0203Ah] = 09Bh    ! #SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
    utfmap[00153h] = 09Ch    ! #LATIN SMALL LIGATURE OE
    utfmap[0200Ch] = 09Dh    ! #ZERO WIDTH NON-JOINER
    utfmap[0200Dh] = 09Eh    ! #ZERO WIDTH JOINER
    utfmap[006BAh] = 09Fh    ! #ARABIC LETTER NOON GHUNNA
    utfmap[000A0h] = 0A0h    ! #NO-BREAK SPACE
    utfmap[0060Ch] = 0A1h    ! #ARABIC COMMA
    utfmap[000A2h] = 0A2h    ! #CENT SIGN
    utfmap[000A3h] = 0A3h    ! #POUND SIGN
    utfmap[000A4h] = 0A4h    ! #CURRENCY SIGN
    utfmap[000A5h] = 0A5h    ! #YEN SIGN
    utfmap[000A6h] = 0A6h    ! #BROKEN BAR
    utfmap[000A7h] = 0A7h    ! #SECTION SIGN
    utfmap[000A8h] = 0A8h    ! #DIAERESIS
    utfmap[000A9h] = 0A9h    ! #COPYRIGHT SIGN
    utfmap[006BEh] = 0AAh    ! #ARABIC LETTER HEH DOACHASHMEE
    utfmap[000ABh] = 0ABh    ! #LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
    utfmap[000ACh] = 0ACh    ! #NOT SIGN
    utfmap[000ADh] = 0ADh    ! #SOFT HYPHEN
    utfmap[000AEh] = 0AEh    ! #REGISTERED SIGN
    utfmap[000AFh] = 0AFh    ! #MACRON
    utfmap[000B0h] = 0B0h    ! #DEGREE SIGN
    utfmap[000B1h] = 0B1h    ! #PLUS-MINUS SIGN
    utfmap[000B2h] = 0B2h    ! #SUPERSCRIPT TWO
    utfmap[000B3h] = 0B3h    ! #SUPERSCRIPT THREE
    utfmap[000B4h] = 0B4h    ! #ACUTE ACCENT
    utfmap[000B5h] = 0B5h    ! #MICRO SIGN
    utfmap[000B6h] = 0B6h    ! #PILCROW SIGN
    utfmap[000B7h] = 0B7h    ! #MIDDLE DOT
    utfmap[000B8h] = 0B8h    ! #CEDILLA
    utfmap[000B9h] = 0B9h    ! #SUPERSCRIPT ONE
    utfmap[0061Bh] = 0BAh    ! #ARABIC SEMICOLON
    utfmap[000BBh] = 0BBh    ! #RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
    utfmap[000BCh] = 0BCh    ! #VULGAR FRACTION ONE QUARTER
    utfmap[000BDh] = 0BDh    ! #VULGAR FRACTION ONE HALF
    utfmap[000BEh] = 0BEh    ! #VULGAR FRACTION THREE QUARTERS
    utfmap[0061Fh] = 0BFh    ! #ARABIC QUESTION MARK
    utfmap[006C1h] = 0C0h    ! #ARABIC LETTER HEH GOAL
    utfmap[00621h] = 0C1h    ! #ARABIC LETTER HAMZA
    utfmap[00622h] = 0C2h    ! #ARABIC LETTER ALEF WITH MADDA ABOVE
    utfmap[00623h] = 0C3h    ! #ARABIC LETTER ALEF WITH HAMZA ABOVE
    utfmap[00624h] = 0C4h    ! #ARABIC LETTER WAW WITH HAMZA ABOVE
    utfmap[00625h] = 0C5h    ! #ARABIC LETTER ALEF WITH HAMZA BELOW
    utfmap[00626h] = 0C6h    ! #ARABIC LETTER YEH WITH HAMZA ABOVE
    utfmap[00627h] = 0C7h    ! #ARABIC LETTER ALEF
    utfmap[00628h] = 0C8h    ! #ARABIC LETTER BEH
    utfmap[00629h] = 0C9h    ! #ARABIC LETTER TEH MARBUTA
    utfmap[0062Ah] = 0CAh    ! #ARABIC LETTER TEH
    utfmap[0062Bh] = 0CBh    ! #ARABIC LETTER THEH
    utfmap[0062Ch] = 0CCh    ! #ARABIC LETTER JEEM
    utfmap[0062Dh] = 0CDh    ! #ARABIC LETTER HAH
    utfmap[0062Eh] = 0CEh    ! #ARABIC LETTER KHAH
    utfmap[0062Fh] = 0CFh    ! #ARABIC LETTER DAL
    utfmap[00630h] = 0D0h    ! #ARABIC LETTER THAL
    utfmap[00631h] = 0D1h    ! #ARABIC LETTER REH
    utfmap[00632h] = 0D2h    ! #ARABIC LETTER ZAIN
    utfmap[00633h] = 0D3h    ! #ARABIC LETTER SEEN
    utfmap[00634h] = 0D4h    ! #ARABIC LETTER SHEEN
    utfmap[00635h] = 0D5h    ! #ARABIC LETTER SAD
    utfmap[00636h] = 0D6h    ! #ARABIC LETTER DAD
    utfmap[000D7h] = 0D7h    ! #MULTIPLICATION SIGN
    utfmap[00637h] = 0D8h    ! #ARABIC LETTER TAH
    utfmap[00638h] = 0D9h    ! #ARABIC LETTER ZAH
    utfmap[00639h] = 0DAh    ! #ARABIC LETTER AIN
    utfmap[0063Ah] = 0DBh    ! #ARABIC LETTER GHAIN
    utfmap[00640h] = 0DCh    ! #ARABIC TATWEEL
    utfmap[00641h] = 0DDh    ! #ARABIC LETTER FEH
    utfmap[00642h] = 0DEh    ! #ARABIC LETTER QAF
    utfmap[00643h] = 0DFh    ! #ARABIC LETTER KAF
    utfmap[000E0h] = 0E0h    ! #LATIN SMALL LETTER A WITH GRAVE
    utfmap[00644h] = 0E1h    ! #ARABIC LETTER LAM
    utfmap[000E2h] = 0E2h    ! #LATIN SMALL LETTER A WITH CIRCUMFLEX
    utfmap[00645h] = 0E3h    ! #ARABIC LETTER MEEM
    utfmap[00646h] = 0E4h    ! #ARABIC LETTER NOON
    utfmap[00647h] = 0E5h    ! #ARABIC LETTER HEH
    utfmap[00648h] = 0E6h    ! #ARABIC LETTER WAW
    utfmap[000E7h] = 0E7h    ! #LATIN SMALL LETTER C WITH CEDILLA
    utfmap[000E8h] = 0E8h    ! #LATIN SMALL LETTER E WITH GRAVE
    utfmap[000E9h] = 0E9h    ! #LATIN SMALL LETTER E WITH ACUTE
    utfmap[000EAh] = 0EAh    ! #LATIN SMALL LETTER E WITH CIRCUMFLEX
    utfmap[000EBh] = 0EBh    ! #LATIN SMALL LETTER E WITH DIAERESIS
    utfmap[00649h] = 0ECh    ! #ARABIC LETTER ALEF MAKSURA
    utfmap[0064Ah] = 0EDh    ! #ARABIC LETTER YEH
    utfmap[000EEh] = 0EEh    ! #LATIN SMALL LETTER I WITH CIRCUMFLEX
    utfmap[000EFh] = 0EFh    ! #LATIN SMALL LETTER I WITH DIAERESIS
    utfmap[0064Bh] = 0F0h    ! #ARABIC FATHATAN
    utfmap[0064Ch] = 0F1h    ! #ARABIC DAMMATAN
    utfmap[0064Dh] = 0F2h    ! #ARABIC KASRATAN
    utfmap[0064Eh] = 0F3h    ! #ARABIC FATHA
    utfmap[000F4h] = 0F4h    ! #LATIN SMALL LETTER O WITH CIRCUMFLEX
    utfmap[0064Fh] = 0F5h    ! #ARABIC DAMMA
    utfmap[00650h] = 0F6h    ! #ARABIC KASRA
    utfmap[000F7h] = 0F7h    ! #DIVISION SIGN
    utfmap[00651h] = 0F8h    ! #ARABIC SHADDA
    utfmap[000F9h] = 0F9h    ! #LATIN SMALL LETTER U WITH GRAVE
    utfmap[00652h] = 0FAh    ! #ARABIC SUKUN
    utfmap[000FBh] = 0FBh    ! #LATIN SMALL LETTER U WITH CIRCUMFLEX
    utfmap[000FCh] = 0FCh    ! #LATIN SMALL LETTER U WITH DIAERESIS
    utfmap[0200Eh] = 0FDh    ! #LEFT-TO-RIGHT MARK
    utfmap[0200Fh] = 0FEh    ! #RIGHT-TO-LEFT MARK
    utfmap[006D2h] = 0FFh    ! #ARABIC LETTER YEH BARREE

!----------------------------------------------------------------------------------------------------------
!             Use this mapping for russian charset
!                     START RUSSIAN CHARSET
! for more information about ASCII CP-1251 see http://www.ascii.ca/cp1251.htm
!----------------------------------------------------------------------------------------------------------
  of net:StoreAsCyrillic
    utfmap[1040] = 192
    utfmap[1041] = 193
    utfmap[1042] = 194
    utfmap[1043] = 195
    utfmap[1044] = 196
    utfmap[1045] = 197
    utfmap[1046] = 198
    utfmap[1047] = 199
    utfmap[1048] = 200
    utfmap[1049] = 201
    utfmap[1050] = 202
    utfmap[1051] = 203
    utfmap[1052] = 204
    utfmap[1053] = 205
    utfmap[1054] = 206
    utfmap[1055] = 207
    utfmap[1056] = 208
    utfmap[1057] = 209
    utfmap[1058] = 210
    utfmap[1059] = 211
    utfmap[1060] = 212
    utfmap[1061] = 213
    utfmap[1062] = 214
    utfmap[1063] = 215
    utfmap[1064] = 216
    utfmap[1065] = 217
    utfmap[1066] = 218
    utfmap[1067] = 219
    utfmap[1068] = 220
    utfmap[1069] = 221
    utfmap[1070] = 222
    utfmap[1071] = 223
    utfmap[1072] = 224
    utfmap[1073] = 225
    utfmap[1074] = 226
    utfmap[1075] = 227
    utfmap[1076] = 228
    utfmap[1077] = 229
    utfmap[1078] = 230
    utfmap[1079] = 231
    utfmap[1080] = 232
    utfmap[1081] = 233
    utfmap[1082] = 234
    utfmap[1083] = 235
    utfmap[1084] = 236
    utfmap[1085] = 237
    utfmap[1086] = 238
    utfmap[1087] = 239
    utfmap[1088] = 240
    utfmap[1089] = 241
    utfmap[1090] = 242
    utfmap[1091] = 243
    utfmap[1092] = 244
    utfmap[1093] = 245
    utfmap[1094] = 246
    utfmap[1095] = 247
    utfmap[1096] = 248
    utfmap[1097] = 249
    utfmap[1098] = 250
    utfmap[1099] = 251
    utfmap[1100] = 252
    utfmap[1101] = 253
    utfmap[1102] = 254
    utfmap[1103] = 255
    utfmap[1025] = 168
    utfmap[1105] = 184
    utfmap[8470] = 185
!------------------------------------------------------------
!                     END RUSSIAN CHARSET
!------------------------------------------------------------

  end
  y = 1
  loop x = 1 to len(clip(p_text))
    case val(p_text[x])
    of 0 to 127
      returnValue[y] = p_text[x]
    else
      if band(val(p_text[x]),11100000b) = 11000000b ! 2 char utf
        utf = self._utfdecode(p_text[x : x+1])
        x += 1
      elsif band(val(p_text[x]),11110000b) = 11100000b ! 3 char utf
        utf = self._utfdecode(p_text[x : x+2])
        x += 2
      elsif band(val(p_text[x]),11111000b) = 11110000b ! 4 char utf
        utf = self._utfdecode(p_text[x : x+3])
        x += 3
      end
      if utf <= maximum(utfmap,1)
        if utfmap[utf] > 0
          returnValue[y] = chr(utfmap[utf])
        elsif utf <= 255
          returnValue[y] = chr(utf) !p_text[x]
        else
          returnValue[y] = chr(255) ! probably means the utfmap needs to be expanded
        end
      else
        returnValue[y] = chr(255) ! probably means the utfmap needs to be expanded
      end
    end
    y += 1
  end
  !self._trace('Utf2Ascii returning '  & clip(returnvalue))
  return(clip(returnvalue))

!------------------------------------------------------------------------------
NetWebServerWorker.AsciiToUtf   Procedure (String p_text,Long p_Maybe=0,Long p_StoredAs=-1)
  compile('***',_VER_C60)
ReturnValue     String(4+len(clip(p_text))*4)
  ***
  omit('***',_VER_C60)
ReturnValue  String(Net:MaxBinData)
  ***
rvl       Long
Ascii     Long,dim(255)
utf             string(4)
utf_len         long
x               long
y               long
StoredAs        long
  code
  !self._trace('AsciiToUtf. Text=' & clip(p_text) & ' OnlyIfUTF=' & p_maybe)
  if p_text = '' then return ''.
  if p_Maybe = net:OnlyIfUTF and lower(self.site.htmlcharset) <> 'utf-8'
    return p_text
  end
  StoredAs = Choose(p_StoredAs = -1,self.site.StoreDataAs,p_StoredAs)
  case StoredAs
  of net:StoreAsPolish
    Ascii[0A5h] = 260
    Ascii[0B9h] = 261
    Ascii[0CAh] = 280
    Ascii[0EAh] = 281
    Ascii[0D3h] = 211
    Ascii[0F3h] = 243
    Ascii[0C6h] = 262
    Ascii[0E6h] = 263
    Ascii[0A3h] = 321
    Ascii[0B3h] = 322
    Ascii[0D1h] = 323
    Ascii[0F1h] = 324
    Ascii[08Ch] = 346
    Ascii[09Ch] = 347
    Ascii[08Fh] = 377
    Ascii[09Fh] = 378
    Ascii[0AFh] = 379
    Ascii[0BFh] = 380

  of net:StoreAsSpanish
  orof net:StoreAsPortuguese
  orof net:StoreAsFrench
  orof net:StoreAsWesternEurope
  orof net:StoreAsASCII

  of net:StoreAsDanish
    Ascii[128] = 8364

  of net:StoreAsBaltic
    Ascii[192] = 260
    Ascii[224] = 261
    Ascii[200] = 268
    Ascii[232] = 269
    Ascii[203] = 278
    Ascii[235] = 279
    Ascii[198] = 280
    Ascii[230] = 281
    Ascii[193] = 302
    Ascii[225] = 303
    Ascii[208] = 352
    Ascii[240] = 353
    Ascii[219] = 362
    Ascii[251] = 363
    Ascii[216] = 370
    Ascii[248] = 371
    Ascii[222] = 381
    Ascii[254] = 382

 of net:StoreAsEastEurope
    ! Croatian
    Ascii[158] = 382  ! changed from 190
    Ascii[142] = 381  ! changed from 174
    Ascii[200] = 268
    Ascii[232] = 269
    Ascii[230] = 263
    Ascii[198] = 262
    Ascii[154] = 353  ! changed from 185
    Ascii[138] = 352  ! changed from 169
    Ascii[240] = 273
    Ascii[208] = 272  ! end croatian

    ! Romanian
    Ascii[194] = 194   ! Capital A-circumflex
    Ascii[226] = 226   ! Lowercase a-circumflex
    Ascii[206] = 206   ! Capital I-circumflex
    Ascii[238] = 238   ! Lowercase i-circumflex

    Ascii[195] = 258   ! Capital A-breve
    Ascii[227] = 259   ! Lowercase a-breve

    Ascii[170] = 536   ! Capital S-cedilla    ! stored as cedilla, displayed as comma (cedilla=350)
    Ascii[186] = 537   ! Lowercase s-cedilla  ! stored as cedilla, displayed as comma (cedilla=351)
    Ascii[222] = 538   ! Capital T-cedilla    ! stored as cedilla, displayed as comma
    Ascii[254] = 539   ! Lowercase t-cedilla  ! stored as cedilla, displayed as comma

 of net:StoreAsArabic
    Ascii[080h] = 020ACh     ! #EURO SIGN
    Ascii[081h] = 0067Eh     ! #ARABIC LETTER PEH
    Ascii[082h] = 0201Ah     ! #SINGLE LOW-9 QUOTATION MARK
    Ascii[083h] = 00192h     ! #LATIN SMALL LETTER F WITH HOOK
    Ascii[084h] = 0201Eh     ! #DOUBLE LOW-9 QUOTATION MARK
    Ascii[085h] = 02026h     ! #HORIZONTAL ELLIPSIS
    Ascii[086h] = 02020h     ! #DAGGER
    Ascii[087h] = 02021h     ! #DOUBLE DAGGER
    Ascii[088h] = 002C6h     ! #MODIFIER LETTER CIRCUMFLEX ACCENT
    Ascii[089h] = 02030h     ! #PER MILLE SIGN
    Ascii[08Ah] = 00679h     ! #ARABIC LETTER TTEH
    Ascii[08Bh] = 02039h     ! #SINGLE LEFT-POINTING ANGLE QUOTATION MARK
    Ascii[08Ch] = 00152h     ! #LATIN CAPITAL LIGATURE OE
    Ascii[08Dh] = 00686h     ! #ARABIC LETTER TCHEH
    Ascii[08Eh] = 00698h     ! #ARABIC LETTER JEH
    Ascii[08Fh] = 00688h     ! #ARABIC LETTER DDAL
    Ascii[090h] = 006AFh     ! #ARABIC LETTER GAF
    Ascii[091h] = 02018h     ! #LEFT SINGLE QUOTATION MARK
    Ascii[092h] = 02019h     ! #RIGHT SINGLE QUOTATION MARK
    Ascii[093h] = 0201Ch     ! #LEFT DOUBLE QUOTATION MARK
    Ascii[094h] = 0201Dh     ! #RIGHT DOUBLE QUOTATION MARK
    Ascii[095h] = 02022h     ! #BULLET
    Ascii[096h] = 02013h     ! #EN DASH
    Ascii[097h] = 02014h     ! #EM DASH
    Ascii[098h] = 006A9h     ! #ARABIC LETTER KEHEH
    Ascii[099h] = 02122h     ! #TRADE MARK SIGN
    Ascii[09Ah] = 00691h     ! #ARABIC LETTER RREH
    Ascii[09Bh] = 0203Ah     ! #SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
    Ascii[09Ch] = 00153h     ! #LATIN SMALL LIGATURE OE
    Ascii[09Dh] = 0200Ch     ! #ZERO WIDTH NON-JOINER
    Ascii[09Eh] = 0200Dh     ! #ZERO WIDTH JOINER
    Ascii[09Fh] = 006BAh     ! #ARABIC LETTER NOON GHUNNA
    Ascii[0A0h] = 000A0h     ! #NO-BREAK SPACE
    Ascii[0A1h] = 0060Ch     ! #ARABIC COMMA
    Ascii[0A2h] = 000A2h     ! #CENT SIGN
    Ascii[0A3h] = 000A3h     ! #POUND SIGN
    Ascii[0A4h] = 000A4h     ! #CURRENCY SIGN
    Ascii[0A5h] = 000A5h     ! #YEN SIGN
    Ascii[0A6h] = 000A6h     ! #BROKEN BAR
    Ascii[0A7h] = 000A7h     ! #SECTION SIGN
    Ascii[0A8h] = 000A8h     ! #DIAERESIS
    Ascii[0A9h] = 000A9h     ! #COPYRIGHT SIGN
    Ascii[0AAh] = 006BEh     ! #ARABIC LETTER HEH DOACHASHMEE
    Ascii[0ABh] = 000ABh     ! #LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
    Ascii[0ACh] = 000ACh     ! #NOT SIGN
    Ascii[0ADh] = 000ADh     ! #SOFT HYPHEN
    Ascii[0AEh] = 000AEh     ! #REGISTERED SIGN
    Ascii[0AFh] = 000AFh     ! #MACRON
    Ascii[0B0h] = 000B0h     ! #DEGREE SIGN
    Ascii[0B1h] = 000B1h     ! #PLUS-MINUS SIGN
    Ascii[0B2h] = 000B2h     ! #SUPERSCRIPT TWO
    Ascii[0B3h] = 000B3h     ! #SUPERSCRIPT THREE
    Ascii[0B4h] = 000B4h     ! #ACUTE ACCENT
    Ascii[0B5h] = 000B5h     ! #MICRO SIGN
    Ascii[0B6h] = 000B6h     ! #PILCROW SIGN
    Ascii[0B7h] = 000B7h     ! #MIDDLE DOT
    Ascii[0B8h] = 000B8h     ! #CEDILLA
    Ascii[0B9h] = 000B9h     ! #SUPERSCRIPT ONE
    Ascii[0BAh] = 0061Bh     ! #ARABIC SEMICOLON
    Ascii[0BBh] = 000BBh     ! #RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
    Ascii[0BCh] = 000BCh     ! #VULGAR FRACTION ONE QUARTER
    Ascii[0BDh] = 000BDh     ! #VULGAR FRACTION ONE HALF
    Ascii[0BEh] = 000BEh     ! #VULGAR FRACTION THREE QUARTERS
    Ascii[0BFh] = 0061Fh     ! #ARABIC QUESTION MARK
    Ascii[0C0h] = 006C1h     ! #ARABIC LETTER HEH GOAL
    Ascii[0C1h] = 00621h     ! #ARABIC LETTER HAMZA
    Ascii[0C2h] = 00622h     ! #ARABIC LETTER ALEF WITH MADDA ABOVE
    Ascii[0C3h] = 00623h     ! #ARABIC LETTER ALEF WITH HAMZA ABOVE
    Ascii[0C4h] = 00624h     ! #ARABIC LETTER WAW WITH HAMZA ABOVE
    Ascii[0C5h] = 00625h     ! #ARABIC LETTER ALEF WITH HAMZA BELOW
    Ascii[0C6h] = 00626h     ! #ARABIC LETTER YEH WITH HAMZA ABOVE
    Ascii[0C7h] = 00627h     ! #ARABIC LETTER ALEF
    Ascii[0C8h] = 00628h     ! #ARABIC LETTER BEH
    Ascii[0C9h] = 00629h     ! #ARABIC LETTER TEH MARBUTA
    Ascii[0CAh] = 0062Ah     ! #ARABIC LETTER TEH
    Ascii[0CBh] = 0062Bh     ! #ARABIC LETTER THEH
    Ascii[0CCh] = 0062Ch     ! #ARABIC LETTER JEEM
    Ascii[0CDh] = 0062Dh     ! #ARABIC LETTER HAH
    Ascii[0CEh] = 0062Eh     ! #ARABIC LETTER KHAH
    Ascii[0CFh] = 0062Fh     ! #ARABIC LETTER DAL
    Ascii[0D0h] = 00630h     ! #ARABIC LETTER THAL
    Ascii[0D1h] = 00631h     ! #ARABIC LETTER REH
    Ascii[0D2h] = 00632h     ! #ARABIC LETTER ZAIN
    Ascii[0D3h] = 00633h     ! #ARABIC LETTER SEEN
    Ascii[0D4h] = 00634h     ! #ARABIC LETTER SHEEN
    Ascii[0D5h] = 00635h     ! #ARABIC LETTER SAD
    Ascii[0D6h] = 00636h     ! #ARABIC LETTER DAD
    Ascii[0D7h] = 000D7h     ! #MULTIPLICATION SIGN
    Ascii[0D8h] = 00637h     ! #ARABIC LETTER TAH
    Ascii[0D9h] = 00638h     ! #ARABIC LETTER ZAH
    Ascii[0DAh] = 00639h     ! #ARABIC LETTER AIN
    Ascii[0DBh] = 0063Ah     ! #ARABIC LETTER GHAIN
    Ascii[0DCh] = 00640h     ! #ARABIC TATWEEL
    Ascii[0DDh] = 00641h     ! #ARABIC LETTER FEH
    Ascii[0DEh] = 00642h     ! #ARABIC LETTER QAF
    Ascii[0DFh] = 00643h     ! #ARABIC LETTER KAF
    Ascii[0E0h] = 000E0h     ! #LATIN SMALL LETTER A WITH GRAVE
    Ascii[0E1h] = 00644h     ! #ARABIC LETTER LAM
    Ascii[0E2h] = 000E2h     ! #LATIN SMALL LETTER A WITH CIRCUMFLEX
    Ascii[0E3h] = 00645h     ! #ARABIC LETTER MEEM
    Ascii[0E4h] = 00646h     ! #ARABIC LETTER NOON
    Ascii[0E5h] = 00647h     ! #ARABIC LETTER HEH
    Ascii[0E6h] = 00648h     ! #ARABIC LETTER WAW
    Ascii[0E7h] = 000E7h     ! #LATIN SMALL LETTER C WITH CEDILLA
    Ascii[0E8h] = 000E8h     ! #LATIN SMALL LETTER E WITH GRAVE
    Ascii[0E9h] = 000E9h     ! #LATIN SMALL LETTER E WITH ACUTE
    Ascii[0EAh] = 000EAh     ! #LATIN SMALL LETTER E WITH CIRCUMFLEX
    Ascii[0EBh] = 000EBh     ! #LATIN SMALL LETTER E WITH DIAERESIS
    Ascii[0ECh] = 00649h     ! #ARABIC LETTER ALEF MAKSURA
    Ascii[0EDh] = 0064Ah     ! #ARABIC LETTER YEH
    Ascii[0EEh] = 000EEh     ! #LATIN SMALL LETTER I WITH CIRCUMFLEX
    Ascii[0EFh] = 000EFh     ! #LATIN SMALL LETTER I WITH DIAERESIS
    Ascii[0F0h] = 0064Bh     ! #ARABIC FATHATAN
    Ascii[0F1h] = 0064Ch     ! #ARABIC DAMMATAN
    Ascii[0F2h] = 0064Dh     ! #ARABIC KASRATAN
    Ascii[0F3h] = 0064Eh     ! #ARABIC FATHA
    Ascii[0F4h] = 000F4h     ! #LATIN SMALL LETTER O WITH CIRCUMFLEX
    Ascii[0F5h] = 0064Fh     ! #ARABIC DAMMA
    Ascii[0F6h] = 00650h     ! #ARABIC KASRA
    Ascii[0F7h] = 000F7h     ! #DIVISION SIGN
    Ascii[0F8h] = 00651h     ! #ARABIC SHADDA
    Ascii[0F9h] = 000F9h     ! #LATIN SMALL LETTER U WITH GRAVE
    Ascii[0FAh] = 00652h     ! #ARABIC SUKUN
    Ascii[0FBh] = 000FBh     ! #LATIN SMALL LETTER U WITH CIRCUMFLEX
    Ascii[0FCh] = 000FCh     ! #LATIN SMALL LETTER U WITH DIAERESIS
    Ascii[0FDh] = 0200Eh     ! #LEFT-TO-RIGHT MARK
    Ascii[0FEh] = 0200Fh     ! #RIGHT-TO-LEFT MARK
    Ascii[0FFh] = 006D2h     ! #ARABIC LETTER YEH BARREE

!----------------------------------------------------------------
!             Use this mapping for russian charset
!                     START RUSSIAN CHARSET
!----------------------------------------------------------------
  of net:StoreAsCyrillic
    Ascii[192] = 1040
    Ascii[193] = 1041
    Ascii[194] = 1042
    Ascii[195] = 1043
    Ascii[196] = 1044
    Ascii[197] = 1045
    Ascii[198] = 1046
    Ascii[199] = 1047
    Ascii[200] = 1048
    Ascii[201] = 1049
    Ascii[202] = 1050
    Ascii[203] = 1051
    Ascii[204] = 1052
    Ascii[205] = 1053
    Ascii[206] = 1054
    Ascii[207] = 1055
    Ascii[208] = 1056
    Ascii[209] = 1057
    Ascii[210] = 1058
    Ascii[211] = 1059
    Ascii[212] = 1060
    Ascii[213] = 1061
    Ascii[214] = 1062
    Ascii[215] = 1063
    Ascii[216] = 1064
    Ascii[217] = 1065
    Ascii[218] = 1066
    Ascii[219] = 1067
    Ascii[220] = 1068
    Ascii[221] = 1069
    Ascii[222] = 1070
    Ascii[223] = 1071
    Ascii[224] = 1072
    Ascii[225] = 1073
    Ascii[226] = 1074
    Ascii[227] = 1075
    Ascii[228] = 1076
    Ascii[229] = 1077
    Ascii[230] = 1078
    Ascii[231] = 1079
    Ascii[232] = 1080
    Ascii[233] = 1081
    Ascii[234] = 1082
    Ascii[235] = 1083
    Ascii[236] = 1084
    Ascii[237] = 1085
    Ascii[238] = 1086
    Ascii[239] = 1087
    Ascii[240] = 1088
    Ascii[241] = 1089
    Ascii[242] = 1090
    Ascii[243] = 1091
    Ascii[244] = 1092
    Ascii[245] = 1093
    Ascii[246] = 1094
    Ascii[247] = 1095
    Ascii[248] = 1096
    Ascii[249] = 1097
    Ascii[250] = 1098
    Ascii[251] = 1099
    Ascii[252] = 1100
    Ascii[253] = 1101
    Ascii[254] = 1102
    Ascii[255] = 1103
    Ascii[185] = 8470
    Ascii[184] = 1105
    Ascii[168] = 1025
!------------------------------------------------------------
!                     END RUSSIAN CHARSET
!------------------------------------------------------------

  else
    return clip(p_text)
  end

  y = 1
  rvl = size(returnValue) - 4
  loop x = 1 to len(clip(p_text))
    if y > rvl then break.
    if val(p_text[x]) = 0
      returnValue[y] = p_text[x]
      y += 1
    elsif Ascii[val(p_text[x])]
      utf = self._utfencode(Ascii[val(p_text[x])],utf_len)
      returnValue[y : y + utf_len - 1] = utf[1: utf_len]
      y += utf_len
    elsif val(p_text[x]) > 127
      utf = self._utfencode(val(p_text[x]),utf_len)
      returnValue[y : y + utf_len - 1] = utf[1: utf_len]
      y += utf_len
    else
      returnValue[y] = p_text[x]
      y += 1
    end
  end
  !self._trace('Ascii2UTF8 incoming=' & clip(p_text) & ' y=' & y & ' return=' & clip(returnValue))
  return clip(returnValue)

!------------------------------------------------------------------------------
NetWebServerWorker._utfencode             Procedure (Long p_utf,*Long rLen)
  code
  return(self.RequestData.WebServer._utfencode(p_utf,rLen))
!------------------------------------------------------------------------------
NetWebServerWorker._utfdecode             Procedure (String p_text)
  code
  return(self.RequestData.WebServer._utfdecode(p_text))
!------------------------------------------------------------------------------
NetWebServerWorker._utfjs                 Procedure (String p_text)
  compile('***',_VER_C60)
ReturnValue     String(len(p_text)+10)
  ***
  omit('***',_VER_C60)
ReturnValue  String(Net:MaxBinData)
  ***
x  long
y  long
hex  string(4)
d            Long
HexDigitsLow STRING('0123456789abcdef')   ! \u00e6 &#nnnm;
  code
  x = 1
  ReturnValue = p_text
  loop
    x = instring('&#',ReturnValue,1,x)
    if x = 0 then break.
    y = instring(';',ReturnValue,1,x)
    if y = 0 then break.
    d = sub(returnValue,x+2,5)
    do makeHex
    ReturnValue = sub(returnValue,1,x-1) & '\u' &clip(hex) & sub(ReturnValue,y+1,size(ReturnValue)-y)
  End
  Return clip(ReturnValue)

MakeHex  routine
  hex = ''
  loop 4 times
    hex = HexDigitsLow[1+band(d,1111b)] & hex
    d = bshift(d,-4)
  end

!------------------------------------------------------------------------------
NetWebServerWorker.PushFormState          PROCEDURE ()
  code
  self._SaveFormStateQueue.Settings = self.formsettings
  self._SaveFormStateQueue.FormState    = self.formstate
  add(self._SaveFormStateQueue,1)
!------------------------------------------------------------------------------
NetWebServerWorker.PopFormState           PROCEDURE ()
  code
  get(self._SaveFormStateQueue,1)
  self.formsettings  = self._SaveFormStateQueue.Settings
  self.formstate     = self._SaveFormStateQueue.FormState
  delete(self._SaveFormStateQueue)
!------------------------------------------------------------------------------
NetWebServerWorker.SetContentAttachment  PROCEDURE (String p_FileName,Long p_Force=0)
x  long
y  long
  code
  ! files served from "uploads" folder should always be an attachment
  x = len(clip(self.site.uploadspath)) + 1
  if (sub(p_FileName,1,x) = clip(self.site.UploadsPath) & '\' and self.HeaderDetails.ContentDisposition = '') or p_Force
    x = 0
    loop
      x = instring('\',p_FileName,1,x+1)
      if x = 0 then break.
      y = x
    end
    self.HeaderDetails.ContentDisposition = 'attachment; filename="'&clip(sub(p_FileName,y+1,255))&'"'
  End
!------------------------------------------------------------------------------
NetWebServerWorker.AddPreCall Procedure(String pName)
  code
  self._PreCallQueue.Name = lower(pName)
  get(self._PreCallQueue,self._PreCallQueue.Name)
  If errorcode()
    self._PreCallQueue.Name = lower(pName)
    Add(self._PreCallQueue)
  End
  Return
!------------------------------------------------------------------------------
NetWebServerWorker.GetPreCall Procedure(String pName)
  code
  self._PreCallQueue.Name = lower(pName)
  get(self._PreCallQueue,self._PreCallQueue.Name)
  if errorcode()
    return 0
  else
    return 1
  end
!------------------------------------------------------------------------------
NetWebServerWorker.AddBrowseValue PROCEDURE (String p_proc,String p_Filename, Key p_Key,View p_View)
  code
  return self.AddBrowseValue(p_proc,p_Filename,p_Key,,,,,,,,,,,p_View)
!------------------------------------------------------------------------------
NetWebServerWorker.AddBrowseValue PROCEDURE (String p_proc,String p_Filename, Key p_Key,<String p_Field1>, <String p_field2>,<String p_field3>, <String p_field4>, <String p_field5>, <String p_field6>, <String p_field7>, <String p_field8>, <String p_field9>, <String p_field10>, <View p_View>)
F   &File
FD1 &String
FD2 &String
FD3 &String
FD4 &String
FD5 &String
FD6 &String
FD7 &String
FD8 &String
FD9 &String
FD10 &String
pos  String(net:posSize)
  code
  F &= self._NetWebFileNamed(p_FileName)
  FD1 &= p_Field1  ! turns omitted fields into a NULL, which in turn makes them omitted in the call to the WebServer
  FD2 &= p_Field2
  FD3 &= p_Field3
  FD4 &= p_Field4
  FD5 &= p_Field5
  FD6 &= p_Field6
  FD7 &= p_Field7
  FD8 &= p_Field8
  FD9 &= p_Field9
  FD10 &= p_Field10
  if not omitted(15)
    self.Viewpos = self.GetPosition(p_View)
  end
  return(self.RequestData.WebServer._AddBrowseValue(self.SessionId,p_proc,p_filename,F,p_key,FD1,FD2,FD3,FD4,FD5,FD6,FD7,FD8,FD9,FD10,self.Viewpos))
!------------------------------------------------------------------------------
NetWebServerWorker.SetBrowseValueStatus PROCEDURE (String p_Hash,Long p_Contracted)
  code
  return(self.RequestData.WebServer._SetBrowseValueStatus(self.SessionId,p_hash,p_Contracted))

!------------------------------------------------------------------------------
NetWebServerWorker.GetBrowseValueStatus PROCEDURE (String p_Hash)
x long
  code
  x = self.RequestData.WebServer._GetBrowseValueStatus(self.SessionId,p_hash)
  return x
!------------------------------------------------------------------------------
! takes an incoming _bidv_ value, and explodes it into the composite field values.
NetWebServerWorker.GetBrowseValue PROCEDURE (String p_Hash,Long p_what=Net:Web:ValueQueue)
ky  string(255)
fn  string(255)
k   &Key
f   &File
Loc:FieldLabel  String(255)
y    Long
Fld  String(255),dim(Net:MaxKeyFields)
Ans  Long
Err  Long
  code
  !self._trace('worker.GetbrowseValue. hash=' & p_hash)
  ans = self.RequestData.WebServer._GetBrowseValue(self.sessionId,p_hash,fn,ky,fld[1],fld[2],fld[3],fld[4],fld[5],fld[6],fld[7],fld[8],fld[9],fld[10],Self.ViewPos)
  If Ans
    F &= self._NetWebFileNamed(fn)
    if not F &= NULL
      K &= self._NetWebKeyNamed(F,ky)
      if not k &= NULL
        loop y = 1 to K{prop:components}
          Loc:FieldLabel = self._SetLabel(F,k{prop:field,y})
          if band(p_what,Net:Web:SessionQueue)
            self.SetSessionValue(Loc:FieldLabel,Fld[y])
          end
          if band(p_what,Net:Web:ValueQueue+Net:Web:Record)
            self.SetValue(Loc:FieldLabel,Fld[y])
          End
        End
        if band(p_what,Net:Web:Record)
          err = self._loadrecord(F,K)
        End
      end
    End
  Else
    self.SetValue('_bidv_',p_hash,,net:formatted)
  End
  return Ans
!------------------------------------------------------------------------------
NetWebServerWorker.ClearBrowse PROCEDURE (<String p_proc>)
  code
  self.RequestData.WebServer._ClearBrowse(self.SessionId,p_proc)
  return
!------------------------------------------------------------------------------
! on mobile systems it is convenient to be able to suppress tips
NetWebServerWorker.CreateTip  PROCEDURE (String p_tip)
  code
  if self.site.notips = 0
    return self.wrap('title',self.translate(p_tip))
  end
  return ''
!------------------------------------------------------------------------------
NetWebServerWorker.SetUserSetting         Procedure(String pName,String pValue)
  code
  self.SetSessionValue(pName,pValue)
!------------------------------------------------------------------------------
NetWebServerWorker.GetUserSetting         Procedure(String pName,<String pDefault>)
  code
  if self.IfExistsSessionValue(pName) = 0
    if not omitted(3)
      return clip(pDefault)
    Else
      Return ''
    End
  Else
    Return self.GetSessionValue(pName)
  End
!------------------------------------------------------------------------------
NetWebServerWorker.LoadUserSetting         Procedure(String pName,<String pDefault>)
  code
  self.SetSessionValue(pName,self.GetUserSetting(pName,pDefault))
!------------------------------------------------------------------------------
NetWebServerWorker.GetContent         Procedure(String p_form,String p_Parms)
   code
   return 'aGet('''&clip(p_form)&''','''&clip(p_Parms)&''');'
!------------------------------------------------------------------------------
NetWebServerWorker.OpenDialog         Procedure(String p_form,String p_Header,Long p_Action,String p_CalledFrom, String p_Row, <String p_ViewState>, <String p_Equate>,<String p_Other>,<String p_parent>,<string p_browseid>)
calledfrom   string(256)
other        string(256)
viewstate    string(256)
equ          string(256)
par          string(256)
browseid     string(256)
  code
  if p_CalledFrom = ''
    calledfrom = ''''''
  else
    calledfrom  = p_CalledFrom
  end
  if not omitted(7)
    viewstate = p_viewstate
  end
  if not omitted(8)
    equ = p_equate
  end
  if not omitted(9)
    other = p_other
  end
  if not omitted(10)
    par = p_parent
  end
  if not omitted(11)
    browseid = lower(p_browseid)
  end
  return 'ntd.push('''&clip(p_form)&''','''','''&self.jsparm(p_Header)&''',1,'&p_Action&',null,'&clip(calledfrom)&','''&clip(p_Row)&''','''&clip(other)&''',0,'''','''','''&clip(viewstate)&''','''& clip(equ)&''','''&clip(par)&''','''&clip(browseid)&''');'
!------------------------------------------------------------------------------
! Converts a Clarion Date & Time to a Unix date/time  (which is stored as Seconds since Jan 1 1970)
NetWebServerWorker.ClarionToUnixDate     Procedure (Long p_Date,Long p_Time=0)
  code
  return self.RequestData.WebServer.ClarionToUnixDate(p_Date,p_Time)
!------------------------------------------------------------------------------
! Extracts Clarion Date from a Unix date/time (which is stored as Seconds since Jan 1 1970)
NetWebServerWorker.UnixToClarionDate      Procedure (Long p_DateTime)
  code
  return self.RequestData.WebServer.UnixToClarionDate(p_Datetime)
!------------------------------------------------------------------------------
 ! Extracts Clarion Time from a Unix date/time (which is stored as Seconds since Jan 1 1970)
NetWebServerWorker.UnixToClarionTime      Procedure (Long p_DateTime)
  code
  return self.RequestData.WebServer.UnixToClarionTime(p_DateTime)
!------------------------------------------------------------------------------
NetWebServerWorker.WindowOpen    Procedure(string pUrl, <String pTarget>)
  code
  if lower(sub(pUrl,1,6)) = 'mailto'
    return 'window.open(''' & clip(pUrl) & ''',''_self'');'
  elsif  omitted(3) or ptarget = '' or ptarget = '''''''' or ptarget = '_self'
    return 'location.href=''' & self._jsok(self._MakeURL(pUrl)) & ''''
  else
    return 'window.open(''' & self._jsok(self._MakeURL(pUrl)) & ''',''' & self._jsok(pTarget) & ''');'
  end
!------------------------------------------------------------------------------
NetWebServerWorker.CleanFilter Procedure (View p_View,String p_Filter)
x           long
l           long
state       long
inConstant  equate(1)
inExpr      equate(0)
F           &File
  code
  ! need to take care of the case where the right hand side of the argument has also been Uppered.
  ! for example Upper(x) = 'XXX' is not the same as x = 'xxx'
  return p_Filter

  if self.DontCleanFilter then return p_Filter.
  F &= p_View{prop:file,1}
  if F{prop:SqlDriver} = 0 then return p_Filter.
  l = len(clip(p_Filter))
  loop x = 1 to l
    if p_Filter[x] = ''''
      If State = inExpr
        State = InConstant
      ElsIf x = l
        break
      Elsif p_Filter[x+1] = ''''
        x +=1
      else
        state = InExpr
      end
    elsif state = InExpr
      if x < l-6
        if upper(sub(p_Filter,x,5)) = 'UPPER'
          p_Filter[x : x+4] = '    '
        end
      end
    end
  end
  return p_Filter

!------------------------------------------------------------------------------
NetWebServerWorker.SetAlert               Procedure(String pMessage,Long pWhere=1,String pProcedure)
  code
  if pProcedure then self.SetValue('_alertProc_',pProcedure).
  if band(pwhere,net:Alert)
    self.SetValue('alert',self.translate(pMessage))
  end
  if band(pwhere,net:Message)
    self.SetValue('message',pMessage)
  end

!------------------------------------------------------------------------------
NetWebServerWorker.SetMessage             Procedure(String pMessage,String pProcedure)
  code
  self.SetAlert(pMessage,net:Message,pProcedure)

!------------------------------------------------------------------------------
NetWebServerWorker.jsParm               Procedure(String p_String,Long p_html=0)
st  stringtheory
  code
  if instring ('''',p_string,1,1) = 0 and instring ('"',p_string,1,1) = 0
    return clip(p_string)
  end
  st.SetValue(p_String)
  st.replace('''','\''')
  st.replace('"','\"')
  return st.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker.jsString               Procedure(String p_String,Long p_html=0)
  code
  if p_html = 0
    return '"' & clip(p_string) & '"'
  elsif instring('''',p_string,1,1)
    return '"' & clip(p_string) & '"'
  else
    return '''' & clip(p_string) & ''''
  end
!------------------------------------------------------------------------------
! note that _PerfstartThread is now called by the Server procedure, before the thread is actually started
! this is to handle the case where a large amount of threads are being started at the same time, so the
! counter gets "behind".
NetWebServerWorker.PerfStartThread     Procedure()
  Code
  if self._perfStarted = 1
    return self.RequestData.Webserver.Performance.NumberOfThreads
  end
  self._perfStarted = 1
  self._timestamp = Clock()
  return self.RequestData.Webserver.Performance.NumberOfThreads
  !return self.RequestData.Webserver._PerfStartThread()

!------------------------------------------------------------------------------
NetWebServerWorker.PerfEndThread     Procedure(Long p_Error=0)
  Code
  if self._perfStarted = 0 then return.
  if p_Error = 0 then p_Error = self.htmlerror.
  self._perfStarted = 0
  self.RequestData.Webserver._PerfEndThread(Clock() - self._timestamp,self.spider,p_error)

!------------------------------------------------------------------------------
NetWebServerWorker.IsMobile               Procedure()
  code
  if self.mobile and self.site.mobile.supportmobile
    return 1
  else
    return 0
  end
!------------------------------------------------------------------------------
NetWebServerWorker.IsMenuTouch               Procedure()
  code
  if instring('opera mobi',self._useragent,1,1) or |
     instring('iemobile ',self._useragent,1,1) or |
     instring('ipod ',self._useragent,1,1) or |
     instring('ipad ',self._useragent,1,1) or |
     instring('iphone ',self._useragent,1,1) or |
     instring('android ',self._useragent,1,1) or |
     instring('blackberry ',self._useragent,1,1) or |
     (instring('arm;',self._useragent,1,1) and instring('touch',self._useragent,1,1) )
     return true
  else
    return false
  end
!------------------------------------------------------------------------------
NetWebServerWorker.CreateMenuItem  Procedure(Long p_MenuType,<String p_Text>,<String p_URL>,<String p_Target>,<String p_Parms>,<String p_Class>,Long p_OpenAs=1,Long p_NotUsed=0,<String p_Title>,<String p_OnClick>,<String p_Image>,Long p_ImageH=0, Long p_ImageW=0,<String p_ImageAlt>,Long p_ImageBreak=0,Long p_AllowHtml=0)
loc:a      string(256)
loc:ae     string(10)
loc:class  string(256)
loc:image  string(256)
loc:spacer string(256)
loc:break  string(256)
loc:text   string(256)
loc:title  string(256)
loc:parms  string(2048)
  code
  If not omitted(4) and p_Url  ! p_url
    if not omitted(6)          ! p_parms
      loc:parms = p_parms
    end
    If not Omitted(5) and p_target <> ''  ! if target set, open as link
      p_OpenAs = net:openAsLink
    Elsif self.IsMobile()                 ! in mobile mode, open as link
      p_OpenAs = net:openAsLink
    ElsIf self.site.ContentBody = '' and p_OpenAs = net:OpenAsContent ! if content not available, open as link
      p_OpenAs = net:openAsLink
    ElsIf lower(sub(p_url,1,7)) = 'http://' or lower(sub(p_url,1,8)) = 'https://' ! if full url then open as link
      p_OpenAs = net:openAsLink
    End

    if not omitted(10)        ! p_title
      loc:title = p_title
    end

    case p_OpenAs
    of net:OpenAsPopup
      loc:a = '<a href="#" onclick="' & self.OpenDialog(p_URL,self._jsok(loc:Title),'action','' ,'',,,self._jsok(loc:Parms)) &'"'
    of net:OpenAsContent
       if loc:parms
         loc:parms = clip(loc:parms) & '&_rid_=' & random(1,2000000000)
       else
         loc:parms = '_rid_=' & random(1,2000000000)
       end
       loc:a = '<a href="#" onclick="' & self.GetContent(p_URL,self._jsok(loc:Parms)) &'"'
    ELSE !     of net:OpenAsLink
      loc:a =  '<a href="'&self._jsok(self._MakeUrl(clip(p_URL) & '&' & clip(loc:Parms))) & '"'
    end

    loc:ae = '</a>'
    If not omitted(5) and p_Target  ! p_target
      loc:a = clip(loc:a) & ' target="'&clip(p_Target)&'"'
    End
    If not omitted(11) and p_OnClick ! p_onclick
      loc:a = clip(loc:a) & ' onclick="'&clip(p_OnClick)&'"'
    End
    loc:a = clip(loc:a) & '>'
  ElsIf p_MenuType = Net:Web:Ddm
    ! ddm requires an <a> tag.
    loc:a = '<a href="#">'
    loc:ae = '</a>'
  ElsIf p_MenuType = Net:Web:Windows
    if not omitted(7) and p_Class  ! p_Class
      loc:a = '<div class="'&self.combine(p_Class)&'">'
    else
      loc:a = '<div>'
    end
    loc:ae = '</div>'
  End
  if not omitted(3) and p_text ! p_text
    loc:text = self.Translate(p_text,p_AllowHtml)
  end
  if not omitted(12) and p_Image  ! p_image
    if not omitted(15) and p_ImageAlt ! p_imagealt
      loc:image = self.CreateImage(p_Image, p_ImageH,  p_ImageW,  p_ImageAlt)
    else
      loc:image = self.CreateImage(p_Image, p_ImageH,  p_ImageW,  '')
    end
    if loc:Text
      If p_ImageBreak = 1 !or (p_MenuType = Net:Web:Accordion and p_ImageBreak = 0)
        loc:spacer = self.br
      else
        loc:spacer = '&#160;'
      End
    End
  End
  case p_MenuType
  of Net:Web:DDM
  orof Net:Web:Windows
    return clip(loc:a) & clip(loc:image) & clip(loc:spacer) & clip(loc:text) & clip(loc:ae)
  of Net:Web:Accordion
  orof Net:Web:TaskPanel
  orof Net:Web:XPTaskPanel
    loc:break = self.br
  end
  if loc:image and loc:text
    return clip(loc:a) & clip(loc:image) & clip(loc:ae) & clip(loc:spacer) & clip(loc:a) & clip(loc:text) & clip(loc:ae) & clip(loc:break)
  else
    return clip(loc:a) & clip(loc:image) & clip(loc:text) & clip(loc:ae) & clip(loc:break)
  end
!------------------------------------------------------------------------------
NetWebServerWorker.CreateGalleryStart      Procedure(String p_Id)
  code
  return self.DivHeader(p_id,'ad-gallery',net:nosend) &|
         '<div class="ad-image-wrapper"></div>'&|
         '<div class="ad-controls"></div>'&|
         '<div class="ad-nav">'&|
         '<div class="ad-thumbs">'&|
         '<ul class="ad-thumb-list">'

!------------------------------------------------------------------------------
NetWebServerWorker.CreateGalleryItem      Procedure(String p_image,String p_thumbnail,<String p_title>,<String p_alt>)
ans    string(2048)
  code

  ans = '<li><a href="'&clip(p_image)&'">' & self.CRLF & |
        '<img src="'&clip(p_thumbnail)&'"'
  if not omitted(4) and p_title ! p_title
    ans = clip(ans) & ' title="'&self._jsok(p_title)&'"'
  end
  if not omitted(5) and p_alt! p_alt
    ans = clip(ans) & ' alt="'&self._jsok(p_alt)&'"'
  end
  ans = clip(ans) & '/></a></li>'
  return clip(ans)

!------------------------------------------------------------------------------
NetWebServerWorker.CreateGalleryEnd      Procedure()
  code
  return '</ul></div></div>' & self.DivFooter(net:NoSend)

!------------------------------------------------------------------------------
NetWebServerWorker.SetProgress            Procedure(String p_Name,String p_Value)
  code
  if p_name <> ''
    if p_Value <> '' and Numeric(p_Value) = 0
      self.SSV(p_name,p_Value)
    elsif p_value > 0 and p_value < 100
      self.SSV(p_name,p_Value)
    end
  End
!------------------------------------------------------------------------------
NetWebServerWorker.ChangeTheme   Procedure(<string p_Theme>)
tt        cstring(252)
newTheme  cstring(252)
xx        long
yy        long
ll        long
ol        long

  code
  if not omitted(2)
    self.SetSessionValue('_theme_',p_theme)
  else
    self.StoreValue('_theme_')
  end
  newtheme = self.GetSessionValue('_theme_')
  if newtheme = '' then return.
  if lower(newtheme) = lower(clip(self.site.defaultTheme)) then return.
  if Not Exists(clip(self.site.webfolderpath) & '\' & clip(self.site.ThemesDir) & '\' & newtheme) then return.
  newtheme = newtheme & '/'
  tt = lower('/' & clip(self.site.defaultTheme) & '/')
  ll = len(tt)
  yy = 1
  loop
    xx = instring(tt,lower(self.site.HtmlCommonStylesMobile),1,yy)
    if xx = 0 then break.
    self.site.HtmlCommonStylesMobile = self.site.HtmlCommonStylesMobile[1 : xx] & newtheme & sub(self.site.HtmlCommonStylesMobile,xx+ll,size(self.site.HtmlCommonStylesMobile))
    yy = xx+tt
  end
  yy = 1
  loop
    xx = instring(tt,lower(self.site.HtmlCommonStyles),1,yy)
    if xx = 0 then break.
    self.site.HtmlCommonStyles = self.site.HtmlCommonStyles[1 : xx] & newtheme & sub(self.site.HtmlCommonStyles,xx+ll,size(self.site.HtmlCommonStyles))
    yy = xx+tt
  end

!------------------------------------------------------------------------------
NetWebServerWorker.DefaultName    Procedure(String p_name)
q              queue(FILE:queue),pre(q).
loc:filename   string(256),auto
a              string(1)
x              long
  code
  loc:filename = p_name
  a = sub(loc:filename,len(clip(loc:filename)),1)
  if a = '\' or a = '/' then loc:filename[len(clip(loc:filename))] = ' '.
  DIRECTORY(q,loc:filename,ff_:DIRECTORY)
  loop x = 1 to records(q)
    get(q,x)
    if Band(q.attrib,ff_:DIRECTORY)
      loc:filename = clip(loc:filename) & '\' & self.site.DefaultPage
      break
    end
  end
  return clip(loc:filename)

!------------------------------------------------------------------------------
NetWebServerWorker.GetLocation            Procedure(Long p_Send=1)
  code
  self.script('getLocation();',p_send)

!------------------------------------------------------------------------------
NetWebServerWorker.SendJSON               Procedure(String p_JSON)
  code
  self._SendMemory(p_JSON,len(clip(p_JSON)))

!------------------------------------------------------------------------------
NetWebServerWorker.FilterQuote  Procedure(String p_part)
st  StringTheory
  CODE
  st.SetValue(p_part,st:clip)
  st.replace('''','''''')
  return st.GetValue()
!------------------------------------------------------------------------------
NetWebServerWorker.MakeFilter   Procedure(String p_Filter,String p_Sub, String p_Str,Long p_Type,Long p_Case=0)
ans          string(1024)
MaxPartsA    Equate(10)
PartLen      Equate(100)
strpart      string(PartLen),dim(MaxPartsA)
allstrparts  string(1024)
subpart      string(PartLen),dim(MaxPartsA)
subpartoa    long,dim(MaxPartsA)
cx           long
cy           long
iStrParts    long
iSubParts    long
iOrParts     long
iAndParts    long
iNotParts    long
partOR       equate(0)
partAND      equate(1)
partNOT      equate(-1)
tx           long
  code
  p_Sub = left(p_sub)
  ans = p_Filter
  do strparts
  case p_Type
  of Net:Begins
    if iStrParts
      if ans
        ans = '(' & clip(ans) & ') AND ('
      else
        ans = '('
      end
      loop cy = 1 to iStrParts
        if cy > 1
          ans = clip(ans) & ' OR'
        end
        if p_Case
          ans = clip(ans) & ' '&self.FilterQuote(strpart[cy])&' >= ''' & self.FilterQuote(p_Sub)&''' AND ' &|
                            ' '&self.FilterQuote(strpart[cy])&' <= ''' & self.FilterQuote(p_Sub)&'<254>'''
        else
          ans = clip(ans) & ' UPPER('&self.FilterQuote(strpart[cy])&') >= ''' & upper(self.FilterQuote(p_Sub))&''' AND ' &|
                            ' UPPER('&self.FilterQuote(strpart[cy])&') <= ''' & upper(self.FilterQuote(p_Sub))&'<254>'''
        end

      end
      ans = clip(ans) & ')'
    end

  of Net:Contains
    if iStrParts
      if ans
        ans = '(' & clip(ans) & ') AND ('
      else
        ans = '('
      end
      loop cy = 1 to iStrParts
        if cy > 1
          ans = clip(ans) & ' OR'
        end
        if p_case
          ans = clip(ans) & ' INSTRING('''&self.FilterQuote(p_Sub)&''','&clip(strpart[cy])&',1,1) <> 0'
        elsif self.clipfilter
          ! clip is required when field is a Cstring using ODBC (Unify SQL)
          ans = clip(ans) & ' INSTRING('''&self.FilterQuote(upper(p_Sub))&''',UPPER(CLIP('&clip(strpart[cy])&')),1,1) <> 0'
        else
          ans = clip(ans) & ' INSTRING('''&self.FilterQuote(upper(p_Sub))&''',UPPER('&clip(strpart[cy])&'),1,1) <> 0'
        end
      end
      ans = clip(ans) & ')'
    end

  of Net:Search
    do subparts
    if iStrParts and iSubParts
      ans = Choose(ans <> '', '(' & clip(ans) & ') AND (', '(' )
      tx = 1
      ! do all the OR parts
      if iOrParts
        loop cx = 1 to iSubParts
          if subpartoa[cx] = partOR
            if tx > 1
              ans = clip(ans) & ' OR '
            end
            if p_case
              ans = clip(ans) & ' INSTRING('''&self.FilterQuote(subpart[cx])&''','&clip(allStrParts)&',1,1) <> 0'
            elsif self.clipfilter
              ans = clip(ans) & ' INSTRING('''&self.FilterQuote(upper(subpart[cx]))&''',CLIP(UPPER('&clip(allStrParts)&')),1,1) <> 0'
            else
              ans = clip(ans) & ' INSTRING('''&self.FilterQuote(upper(subpart[cx]))&''',UPPER('&clip(allStrParts)&'),1,1) <> 0'
            end
            tx += 1
          end
        end
      end
      ! then do all the AND and NOT parts
      if iAndParts or iNotParts
        if iOrParts > 0
          ans = clip(ans) & ') AND ('
        end
        tx = 1
        loop cx = 1 to iSubParts
          if subpartoa[cx] <> partOR
            if tx > 1
              ans = clip(ans) & ' AND'
            end
            if subpartoa[cx] = partNOT
              ans = clip(ans) & ' NOT('
            end
            if p_case
              ans = clip(ans) & ' INSTRING('''&self.FilterQuote(subpart[cx])&''','&clip(allStrParts)&',1,1) <> 0'
            elsif self.clipfilter
              ans = clip(ans) & ' INSTRING('''&self.FilterQuote(upper(subpart[cx]))&''',CLIP(UPPER('&clip(allStrParts)&')),1,1) <> 0'
            else
              ans = clip(ans) & ' INSTRING('''&self.FilterQuote(upper(subpart[cx]))&''',UPPER('&clip(allStrParts)&'),1,1) <> 0'
            end
            if subpartoa[cx] = partNOT
              ans = clip(ans) & ' )'
            end
            tx += 1
          end
        end
      end
      ans = clip(ans) & ')'
    end
  end
  return ans

strparts  routine
  iStrParts = 1
  allStrParts = ''
  loop
    cx = instring(',',p_Str,1,1)
    if cx = 0
      if p_Str
        strpart[iStrParts] = p_Str
        allStrParts = choose(allStrParts='',strpart[iStrParts], clip(allStrParts) & ' & ' & strpart[iStrParts])
      else
        iStrParts -= 1
      end
      break
    end
    strpart[iStrParts] = sub(p_Str,1,cx-1)
    allStrParts = choose(allStrParts='',strpart[iStrParts], clip(allStrParts) & ' & ' & strpart[iStrParts])
    iStrParts += 1
    if iStrParts > MaxPartsA then break.
    p_Str = sub(p_Str,cx+1,255)
  end

subparts  routine
  iSubParts = 1
  loop
    cx = instring(' ',clip(p_Sub),1,1)
    cy = instring('"',clip(p_Sub),1,1)
    if cy and cy < cx
      cx = instring('"',clip(p_Sub),1,cy+1)
    end
    if cx = 0
      if p_Sub
        subpart[iSubParts] = p_Sub
        do SubPartA
      end
      break
    end
    subpart[iSubParts] = sub(p_Sub,1,cx)
    do SubPartA
    if iSubParts > MaxPartsA then break.
    p_Sub = sub(p_Sub,cx+1,255)
  end
  iSubParts -= 1

subpartA routine
  do StripQuotes
  if subpart[iSubParts,1] = '+'
    subpartoa[iSubParts] =  partAND
    subpart[iSubParts] = sub(subpart[iSubParts],2,100)
    iAndParts += 1
    iSubParts += 1
    do StripQuotes
  elsif subpart[iSubParts,1] = '-'
    subpartoa[iSubParts] = partNOT
    subpart[iSubParts] = sub(subpart[iSubParts],2,100)
    iNotParts += 1
    iSubParts += 1
    do StripQuotes
  elsif subpart[iSubParts] <> ''
    iOrParts += 1
    subpartoa[iSubParts] = partOR
    iSubParts += 1
    do StripQuotes
  end

StripQuotes  routine
  data
ln long
  code
  if iSubParts < 1 or iSubParts > MaxPartsA then exit.
  if subpart[iSubParts,1] = '"'
    subpart[iSubParts] = sub(subpart[iSubParts],2,100)
  end
  ln = len(clip(subpart[iSubParts]))
  if ln > 0 and ln <= PartLen
    if subpart[iSubParts,ln] = '"'
      subpart[iSubParts,ln] = ' '
    End
  end
!------------------------------------------------------------------------------
NetWebServerWorker.MakeCrumb   Procedure(String p_Text, String p_URL, Long p_Flag)
ans      StringTheory
url      StringTheory
  code
  url.SetValue(self.wrap('href',p_Url),st:clip)
  if p_Flag =  net:first
    ans.SetValue('  <div class="nt-float-left nt-whole-crumb">' & |
          '    <a class="nt-float-left nt-crumb-first ui-corner-left" '&url.GetValue()&'>'&clip(p_Text)&'</a>  ' & |
          '    <div class="nt-float-left nt-crumb-right"></div>' & |
          '  </div>')
  elsif p_Flag = net:last
    if url.GetValue()
      ans.SetValue('  <div class="nt-float-left nt-crumb-last ui-corner-all"><a class="nt-crumb-link" '&url.GetValue()&'>'&clip(p_Text)&'</a></div>')
    else
      ans.SetValue('  <div class="nt-float-left nt-crumb-last ui-corner-all">'&clip(p_Text)&'</div>')
    end
  else
    ans.SetValue('  <div class="nt-float-left nt-whole-crumb">' & |
          '    <div class="nt-float-left nt-crumb-left"></div>        ' & |
          '    <a class="nt-float-left nt-crumb" '&url.GetValue()&'>'&clip(p_Text)&'</a> ' & |
          '    <div class="nt-float-left nt-crumb-right"></div>' & |
          '  </div>')
  end
  return ans.GetValue()
!------------------------------------------------------------------------------
NetWebServerWorker.MakeAnchor   Procedure(String p_Anchor)
ans  string(size(p_Anchor))
y    long
  code
  loop y = 1 to len(clip(p_Anchor))
    case p_Anchor[y]
    of 'a' to 'z'
    orof '0' to '9'
    orof 'A' to 'Z'
      !ans = clip(ans) & lower(sub(p_Anchor,y,1))
      ans = clip(ans) & sub(p_Anchor,y,1)
    end
  end
  return clip(ans)

!------------------------------------------------------------------------------
NetWebServerWorker.SetSqlTimeout    Procedure(File p_File,Long pSet)
  code
  If p_File{prop:driver} = 'MSSQL' and self.Site.SqlTimeout <> -1
    if pSet = Net:On
      if self.Site.SqlTimeout = 0 then self.Site.SqlTimeout = 10000.
      p_File{prop:sql} = 'Set Lock_Timeout ' & self.Site.SqlTimeout
    elsif pSet = Net:Off
      p_File{prop:sql} = 'Set Lock_Timeout -1'
    end
  End

!------------------------------------------------------------------------------
NetWebServerWorker.ChangeTab Procedure(Long pStyle,string pProc,Long pTab)
  Code
  Case pStyle
  of Net:Web:Accordion
    self.jquery('#tab_' & lower(clip(pProc)) & '_div','accordion','"option", "active",' & pTab);
  of Net:Web:Tab
    self.jquery('#tab_' & lower(clip(pProc)) & '_div','tabs','"option", "active",' & pTab);
  of Net:Web:Wizard
    self.jquery('#tab_' & lower(clip(pProc)) & '_div','ntwiz','"option", "active",' & pTab);
  End

!------------------------------------------------------------------------------
NetWebServerWorker.SetOption    Procedure(StringTheory pOptions,string pOption, string pValue, long pDefault=0)
  code
  pOptions.SetValue(self.SetOption(pOptions.Value,pOption,pValue,pDefault))
!------------------------------------------------------------------------------
NetWebServerWorker.SetOption    Procedure(String pOptions,string pOption, string pValue, long pDefault=0)
vu     StringTheory
st     StringTheory
ln     StringTheory
x      long
found  long
  code
  vu.SetValue(pValue,st:clip)
  st.SetValue(pOptions,st:clip)
  st.split(',','(|{{|[|"',')|}|]|"',false,,,'|',st:nested)  ! requires StringTheory version 1.88

  loop x = 1 to st.records()
    ln.setvalue(st.getline(x))
    ln.split(':')
    if ln.getline(1) = pOption
      found = 1
      if pDefault = 0
        if numeric(vu.GetValue()) or vu.sub(1,4) = 'null' or vu.sub(1,8) = 'function' or vu.sub(1,1) = '"' or vu.sub(1,1) = '['  or vu.sub(1,1) = '{{' or vu.GetValue() = 'true' or vu.GetValue() = 'false'
          st.setline(x,clip(pOption) &':' & vu.GetValue())
        else
          vu.replace('"','''')
          vu.remove('<13>')
          vu.remove('<10>')
          st.setline(x,clip(pOption) &':"' & vu.GetValue() &'"')
        end
      end
    end
  end
  if not found
    if numeric(vu.GetValue()) or vu.sub(1,4) = 'null' or vu.sub(1,8) = 'function' or vu.sub(1,1) = '"' or vu.sub(1,1) = '['  or vu.sub(1,1) = '{{' or vu.GetValue() = 'true' or vu.GetValue() = 'false'
      if right(pOption,1) = ':'
        st.addline(1,clip(pOption) & vu.GetValue())
      else
        st.addline(1,clip(pOption) & ':' & vu.GetValue())
      end
    else
      vu.replace('"','''')
      vu.remove('<13>')
      vu.remove('<10>')
      if right(pOption,1) = ':'
        st.addline(1,clip(pOption) & '"' & vu.GetValue() &'"')
      else
        st.addline(1,clip(pOption) & ':"' & vu.GetValue() &'"')
      end
    end
  end
  st.sort(st:SortNocase)  ! requires StringTheory 1.74
  st.join(',')
  return st.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker.MakeWebRelative  Procedure(String p_FileName)
l  long
  code
  l = len(clip(self.site.webfolderpath))
  if lower(sub(p_filename,1,l)) = lower(self.site.webfolderpath)
    return clip(sub(p_FileName,l+1,255))
  else
    return clip(p_fileName)
  end
!------------------------------------------------------------------------------
NetWebServerWorker.HtmlToxHtml            Procedure(String p_Html)
st  stringtheory
x   long
y   long
  code
  st.SetValue(p_Html)
  st.replace('<br>','<br/>')
  st.replace('<hr>','<hr/>')
  !st.replace('<p>','<p/>')
  if st.instring('<image') and not st.instring ('</image>')
    loop
      x = st.instring('<image',1,y)
      if x = 0 then break.
      y = st.instring('>',1,x)
      if y = 0 then break.
      if st.sub(y-1,2) = '/>' then cycle.
      st.replace('>', '/>', 1,y,y,)
    end
  end
  return st.GetValue()

!------------------------------------------------------------------------------
NetWebServerWorker.InterpretFormStage     Procedure(Long pStage)
ans  string(255)
  code
  if band(pStage,net:web:StagePre) then ans = clip(ans) & ' net:web:StagePRE'.
  if band(pStage,net:web:StageValidate) then ans = clip(ans) & ' net:web:StageVALIDATE'.
  if band(pStage,net:web:StagePost) then ans = clip(ans) & ' net:web:StagePOST'.

  if band(pStage,7) = Net:InsertRecord then ans = clip(ans) & ' net:web:Insert'.
  if band(pStage,7) = Net:ChangeRecord then ans = clip(ans) & ' net:web:Change'.
  if band(pStage,7) = Net:DeleteRecord then ans = clip(ans) & ' net:web:Delete'.
  if band(pStage,7) = Net:CopyRecord then ans = clip(ans) & ' net:web:Copy'.
  if band(pStage,7) = Net:ViewRecord then ans = clip(ans) & ' net:web:View'.
  if band(pStage,7) = Net:LookupRecord then ans = clip(ans) & ' net:web:Lookup'.
  if band(pStage,7) = Net:WriteMask then ans = clip(ans) & ' net:web:WriteMask'.
  if pStage = net:web:Generate then ans = clip(ans) & ' net:web:Generate'.

  if band(pStage,net:web:SetPics) then ans = clip(ans) & ' net:web:SetPics'.
  if band(pStage,net:web:Init) then ans = clip(ans) & ' net:web:Init'.
  if band(pStage,Net:Web:AfterLookup) then ans = clip(ans) & ' net:web:AfterLookup'.
  if band(pStage,Net:Web:Div) then ans = clip(ans) & ' net:web:Div'.
  if band(pStage,Net:Web:Cancel) then ans = clip(ans) & ' net:web:Cancel'.
  if band(pStage,Net:Web:Populate) then ans = clip(ans) & ' net:web:Populate'.
  if band(pStage,Net:Web:Popup) then ans = clip(ans) & ' net:web:Popup'.
  if band(pStage,Net:Web:GetSecwinSettings) then ans = clip(ans) & ' net:web:GetSecwinSettings'.
  if band(pStage,Net:Web:DrawMonth) then ans = clip(ans) & ' net:web:DrawMonth'.
  if band(pStage,Net:Web:Partial) then ans = clip(ans) & ' net:web:Partial'.
  if band(pStage,Net:Web:FocusBack) then ans = clip(ans) & ' net:web:FocusBack'.
  if band(pStage,Net:Web:NextTab) then ans = clip(ans) & ' net:web:NextTab'.

  if band(pStage,Net:Web:Resized) then ans = clip(ans) & ' Net:Web:Resized'.
  if band(pStage,Net:Web:ClearBrowse) then ans = clip(ans) & ' Net:Web:ClearBrowse'.
  if band(pStage,Net:Web:ChildUpdated) then ans = clip(ans) & ' Net:Web:ChildUpdated'.
  if band(pStage,Net:Web:EIProw) then ans = clip(ans) & ' Net:Web:EIProw'.
  if band(pStage,Net:Web:EIPColumn) then ans = clip(ans) & ' Net:Web:EIPColumn'.
  if band(pStage,Net:Web:RowClicked) then ans = clip(ans) & ' Net:Web:RowClicked'.

  return clip(ans)

!------------------------------------------------------------------------------
NetWebServerWorker.Part      Procedure(String p_Values,Long p_Part,string p_Delim)
st  StringTheory
  code
  st.SetValue(p_Values)
  st.split(p_Delim)
  return st.GetLine(p_Part)
!------------------------------------------------------------------------------
NetWebServerWorker.jsonOk      Procedure(String p_notok,long p_flags=0)
  code
  return self.RequestData.WebServer.jsonOk(p_notok,p_flags)
!------------------------------------------------------------------------------
NetWebServerWorker.PushEvent  Procedure(String pEvent)
  code
  self.Event = pEvent
  self.EventStack.Event = pEvent
  add(self.EventStack,1)
!------------------------------------------------------------------------------
NetWebServerWorker.PopEvent  Procedure()
  code
  get(self.EventStack,1)
  if errorcode() = 0
    Delete(self.EventStack)
    get(self.EventStack,1)
    If Errorcode()
      self.EventStack.Event = ''
    End
  Else
    self.EventStack.Event = ''
  End
  self.Event = self.EventStack.Event

!------------------------------------------------------------------------------
! pure debugging method.
NetWebServerWorker.calldebug              Procedure()
  code
  return

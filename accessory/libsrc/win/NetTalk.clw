! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2012 CapeSoft Software  - www.capesoft.com


      Member()
         omit('***$***',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
         ***$***
      Include('Equates.CLW'),ONCE
      Include('Keycodes.CLW'),ONCE
      Include('Errors.CLW'),ONCE
      Map

      End ! map
      Include('NetTalk.inc'),ONCE
   ! includes - NetMap Module Map
   include('NetMap.inc'),once

! gThreadedAnnounced
Net:gThreadedAnnounced            long,thread    ! used in NetServer.Announce()

! Files - DOS Driver - for NetAuto NetFileServer

NETDosFileName               string(256),thread ! 14/4/2003 - previously had static
NETDosFileType               file,driver('DOS'),thread,create,name(NETDosFileName),pre(NetDosFile)
record                         record
a                                string(NET:MAXBinData)
                               end
                             end

NetServer.Init       PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
  compile ('****', NETDLLDEBUG=1)
result      long
param       string (255)
  ****

  compile ('****', NETTALKLOG=1)
temp1       string (30)
temp2       string (30)
  ****
  CODE
  ! ----------------------- Logging ------------------------------------------
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      if band (p_Operation, NET:StartService)
        temp1 = 'Starting'
      elsif band (p_Operation, NET:StopService)
        temp1 = 'Stopping'
      else
        temp1 = '*** Invalid operation'
      end
      if band (p_Operation, NET:Private)
        temp2 = 'private'
      else
        temp2 = 'non-private'
      end
      self.Log ('NetServer.Init', clip (temp1) & ' ' & clip (temp2) & ' Service Name = ' & clip (p_ServiceName) & |
               ' Operation = ' & p_Operation)
    end
  ****
  ! ----------------------- End Logging --------------------------------------


  self.prime()                                       ! Prime NetAuto Instance

  if ~omitted(2)
    self.Service.ServiceName = p_ServiceName         ! set the Service name
  end

  If ~omitted(3)
    self.Service.Options = bor(p_Operation,Net:StartService)  ! see if options overridden
  end

  if self.UseThisThread <> 0                         ! New 6 Feb 2002 (Jono)
    self.service.Thread = self.UseThisThread
  else
    self.service.Thread = thread()                   ! the thread to post the message to
  end

  Net:gThreadedAnnounced = 0                         ! Net:gThreadedAnnounced is declared in NetTalk.clw

  self.error = NetAutoSetService(self.Service)           ! call the NetTalkDLL to start the service
  if self.error = 0
    self.sendreply = 0                               ! default to no reply.
    self._RunningState = 1                           ! The service is up and running
  else
    self._RunningState = 0
    self.errortrap('An error occurred while attempting to Start the ' & clip(self.service.ServiceName) & ' Service', '(Parent)NetServer.Init')                  ! Trap the error
  end

  self._WaitingState = 0                              ! number of requests waiting to be processed


  ! --------------------- More logging stuff --------------------------------
  compile ('****', NETDLLDEBUG=1)
    if command ('/nettalkwindow')           ! You need a special version of the DLL for this
      param = '0'
      result = NetOptions (NET:SHOWWHO, param)
      result = NetOptions (NET:SHOWTASKS, param)
      result = NetOptions (NET:SHOWTRACE, param)
      result = NetOptions (NET:SHOWLOCAL, param)
      result = NetOptions (NET:SHOWREMOTE, param)
      result = NetOptions (NET:SHOWNOTIFY, param)
    end
  ****
  ! --------------------- End logging stuff --------------------------------


  ! basic initalisation stuff. self.Init makes sure each instance of each object gets
  ! its own events.

  ! because this is a server a 'service' group is primed and the NetSetService function
  ! is called.

  ! Some error checking comes after that...
!------------------------------------------------------------------------------
NetServer.Kill       PROCEDURE  (long p_Options=NET:StopService) ! Declare Procedure 3
  CODE
  ! the opposite of the Init method, this kills the service.
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetServer.Kill', 'ServiceName = ' & clip(self.Service.ServiceName))
    end
  ****

  if ~self._initalised
    return
  end

  self.service.Options = p_Options                          ! Service options

  Net:gThreadedAnnounced = 0                                ! Net:gThreadedAnnounced is declared in NetTalk.clw

  self.error = NetAutoSetService(self.Service)              ! Call the dll to stop service

  !    You can't clear the thread number before calling NetSetService otherwise it
  !    can't get rid of the service !!
  self.service.Thread = 0                                   ! Clear this property

  NetFreeUniqueEvent(self.service.Event)

  self._initalised = 0                                      ! Sean October 2007: Added this so that when Init() and Kill() are called multiple times the object does not croak
  self._RunningState = 0

  if self.error
    self.errortrap('An error occurred while attempting to Stop the ' & clip (self.Service.ServiceName) & ' Service', '(Parent)NetServer.Kill')
  end
!------------------------------------------------------------------------------
NetServer.TakeEvent  PROCEDURE                             ! Declare Procedure 4
  CODE
  ! this is by far the most complicated bit, so it's described as it goes. Basically
  ! the DLL triggers us doing things by sending events. Most of the events are handled
  ! here. The good news is that because it's done here you don't need to worry about
  ! doing it in each derivative object...

  ! notice that to keep every thing going, and to ensure that that only one call is
  ! processed at a time, this method sends events to itself. That's no big deal, it's
  ! like doing a "return, but come back to point n just now" type call.

  ! remember this method ultimately governs the receiving *and* sending of reply packets.

  if ~self._initalised
    return                                      ! no point coming in here if the object not initalised
  end

  if event() = self.service.event               ! a request for this service is waiting
    self._waitingState += 1                     ! increment the waiting count
    post(self.service.event+1)                  ! send event to get next waiting request
  elsif event() = self.service.event+1          ! get next waiting request
    if self._runningState = 1 and self._waitingState  ! if the server is avaliable, and someone is waiting...
      self._runningState = 2                    ! sets the server to unavaliable for now
      self._waitingState -= 1                   ! reduce the waiting count by 1

      ! Clear the contents of the packet
      clear(self.packet.Command)
      clear(self.packet.BinData)
      clear(self.packet.BinDataLen)
      clear(self.packet.ToNetName)
      clear(self.packet.FromNetName)
      clear(self.packet.ServiceName)
      clear(self.packet.PacketNumber)
      clear(self.packet.SetNumber)
      clear(self.packet.Options)
      clear(self.packet.SentDate)
      clear(self.packet.SentTime)
      clear(self.packet.ToThread)
      clear(self.packet.FromThread)
      clear(self.packet.Reserved)

      self.Packet.ServiceName = self.service.ServiceName   ! get the request for this service
      self.Packet.FromNetName = ''                         ! Specify to NetReceive that we will accept Packets from any NetName (NetAuto process)

      self.error = NetAutoReceive(self.Packet)                 ! get the packet from the DLL

      if self.error
        self.errortrap('An error occurred while attempting to receive a Packet for the ' & clip (self.Service.ServiceName) & ' service', '(parent)NetServer.TakeEvent')
        self._runningState = 1                  ! Make the server available again
      else                                      ! No Error
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetServer.TakeEvent', 'Received Packet for (and from) ServiceName = ' & clip(self.Service.ServiceName) & |
                      ' FromNetName = ' & clip(self.Packet.FromNetName) & |
                      ' FromThread = ' & self.Packet.FromThread & |
                      ' ToNetName = ' & clip(self.Packet.ToNetName) & |
                      ' ToThread = ' & self.Packet.ToThread & |
                      ' Command = ' & self.Packet.Command & |
                      ' DataLength = ' & self.Packet.BinDataLen & |
                      ' SetNumber = ' & self.Packet.SetNumber & |
                      ' PacketNumber = ' & self.Packet.PacketNumber & |
                      ' Options = ' & self.Packet.Options)
          end
        ****

        self.sendreply = 0                      ! Added Jono 29 Dec 1999
        self.Process()                          ! Call the virtual method to Process the packet that just arrived
        if self.sendReply = 0                   ! No reply needs to be sent
          self._runningState = 1                ! make server avaliable again
          if self._waitingState
            compile ('****', NETTALKLOG=1)
              if self.LoggingOn
                self.Log ('NetServer.TakeEvent', '(self._waiting state) Ignored Posting event + 1')  ! Shouldn't happen
              end
            ****
          end
        else
                                                ! Okay, we need to send a reply to this packet.
          self.packet.options = bor(self.packet.options,net:response)  ! responds to originator.
          post(self.service.event+2,,,1)        ! send event to ourselves, this will get the MultiPacketReply method called. Note we need to process this event asap to avoid the packet structure being trashed. That's why we post it to the front of the queue.
          self.sendreply=0
        end
      end                                       ! No Error
    else
      ASSERT(self._waitingState)
      if self._waitingState
        post(self.service.event+1)              ! waitig for running to come back to 1
      else
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetServer.TakeEvent', 'Assert failed. ServiceName = ' & clip(self.Service.ServiceName) & |
                      ' self.waitingState = ' & self._waitingState & |
                      ' self.runningState = ' & self._runningState, NET:LogError)
          end
        ****
      end
    end
  elsif event() = self.service.event+2          ! send next reply packet
    self.MultiPacketReply()                     ! virtual method to prepare the multi-packet reply packets

    self.packet.options = bor(self.packet.options,net:response)
                                                ! Set the Net:Response in the options - the DLL needs this

    self.packet.ServiceName = clip(self.service.ServiceName)
                                                ! Set the Service name in the packet.

    self.Packet.PacketNumber = self._nextPacketNumber  ! Objects should not change PacketNumber and SetNumber
    self.Packet.SetNumber = self._nextSetNumber        ! They can set self.Packet.Options to NET:PARTOFSET and NET:LASTOFSET


    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetServer.TakeEvent', 'Sending Reply. ServiceName = ' & clip(self.Service.ServiceName) & |
                  ' ToNetName = ' & clip(self.Packet.ToNetName) & |
                  ' ToThread = ' & self.Packet.ToThread & |
                  ' FromNetName = ' & clip(self.Packet.FromNetName) & |
                  ' FromThread = ' & self.Packet.FromThread & |
                  ' Command = ' & clip(self.Packet.Command) & |
                  ' DataLength = ' & self.Packet.BinDataLen & |
                  ' Options = ' & self.Packet.Options & |
                  ' SetNumber = ' & self.Packet.SetNumber & |
                  ' PacketNumber = ' & self.Packet.PacketNumber)
      end
    ****
    if self.packet.binDataLen > NET:MaxBinData
      self.error = ERROR:InvalidStructureSize
      self.errortrap('Trying to send too much data in self.packet.binData and self.packet.binDataLen', 'NetServer.TakeEvent')
      return
    end


    if band (self.Packet.Options, Net:SYNCHRONOUS)
      self.error = NetAutoSendSync (self.Packet)           ! Synchronous send reply packet
    else
      self.error = NetAutoSendAsync (self.Packet)          ! Asynchronous send reply packet (better)
    end

    if band (self.Packet.Options, NET:PARTOFSET)! Multi-Packet Set
      if band (self.Packet.Options, NET:LASTOFSET) ! Last packet in set
        self._nextSetNumber += 1
        self._nextPacketNumber = 0
        self._runningState = 1                  ! make server avaliable again - Jono 29 Dec 1999
      else                                      ! Not last packet in set
        self._nextPacketNumber += 1
        post(self.service.event+2,,,1)          ! send event to handle another reply packet. This will ensure MultiPacketReply() is called again. - Jono 29 Dec 1999 - also added queue order !
      end
    else  ! SinglePacket
      self._nextSetNumber += 1
      self._nextPacketNumber = 0
      self._runningState = 1                    ! make server avaliable again - Jono 29 Dec 1999
    end

    if self.error
      self.errortrap('An error occurred while attempting to send the reply packet. Service = ' & clip (self.Service.ServiceName) & '.', '(parent)NetServer.TakeEvent')
    end
  end
!------------------------------------------------------------------------------
NetServer.ErrorTrap  PROCEDURE  (string errorStr,string functionName) ! Declare Procedure 3
temp2        string (512)
  CODE
  temp2 = self.InterpretError()

  if self.error = 0
    self.ErrorString = clip(errorStr) & '.'
  else
    self.ErrorString = clip(errorStr) & '.' & ' The error number was ' & |
                       self.error & ' which means ' & clip(temp2) & '.'
  end

  if not (self.SuppressErrorMsg)
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetServer.ErrorTrap', |
                  'Error = ' & self.error & |
                  ' = ' & clip(errorStr) & |
                  ' : ' & clip (temp2) & |
                  ' Function = ' & clip(functionName) & |
                  ' ServiceName = ' & clip (self.Service.ServiceName) |
                  , NET:LogError)
      end
    ****
    if self.error = 0
      message('A nettalk network communication error has occurred.||Error = ' & clip(errorStr) & '.' & |
              '||Error occurred in function ' & clip(functionName) & |
              '|ServiceName = ' & clip (self.Service.ServiceName) |
              , clip(NET:ErrorTitle), Icon:Asterisk)
    elsif self.error <> ERROR:DllNotActive
      message('A nettalk network communication error has occurred.||Error = ' & clip(errorStr) & '.' & |
              '||The error number was ' & self.error & |
              ' which means ' & clip(temp2) & '.' & |
              '||Error occurred in function ' & clip(functionName) & |
              '|ServiceName = ' & clip (self.Service.ServiceName) |
              , clip(NET:ErrorTitle), Icon:Asterisk)
    end
  else                                       ! Only log to file
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetServer.ErrorTrap', '<<no display> ' & |
                  'Error = ' & self.error & |
                  ' = ' & clip(errorStr) & |
                  ' : ' & clip (temp2) & |
                  ' Function = ' & clip(functionName) & |
                  ' ServiceName = ' & clip (self.Service.ServiceName) |
                  , NET:LogError)
      end
    ****
  end

  ! this is a simple error handler. It means every error received interrupts your program
  ! (which you may want to override). The actual message is provided by the
  ! InterpretError method, which can also be overridden.


!------------------------------------------------------------------------------
NetServer.Process    PROCEDURE                             ! Declare Procedure 4
  CODE
!---------------------------------------------------------------------------------
! This is a virtual method handled by derivative classes.
! The packet will contain the data of the received packet.
! If you want to send a reply to the packet set SendReply = 1, this will call
! the PreparePacket() method
!---------------------------------------------------------------------------------
!------------------------------------------------------------------------------
NetServer.MultiPacketReply PROCEDURE                       ! Declare Procedure 4
  CODE
!---------------------------------------------------------------------------------
! This is a virtual proc handled by derivative classes.
! It will be called when the server is ready to send the a reply packet.
!
! This method is only called if you set self.sendreply = 1 in the Process() method
!
! You only really need to derive a MultiPacketReply() method if you want to send
! a multi-packet set
!
! If you want to send a multi-packet set, then set self.packet.options = Net:PartOfSet
! this will call this method again. When you want to send your last packet in the set,
! set self.packet.options = Net:PartOfSet + Net:LastOfSet
!---------------------------------------------------------------------------------
!------------------------------------------------------------------------------
NetClient.Init       PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
local                 group, pre (loc)
StoreSuppressErrorMsg   byte
StoreCurrentError       long
StoreCurrentErrorString string (255)
                      end
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetClient.Init', 'Called for ServiceName = ' & clip (p_ServiceName))
    end
  ****

  self._myservers &= new(Net:AutoPublicServicesQType)
  self.servers &= self._myservers

  parent.init(p_ServiceName, p_Operation)

  self.service.event += 3
  NetAutoNotify(self.service)
  self.service.event -= 3

  loc:StoreSuppressErrorMsg = self.SuppressErrorMsg ! Save
  self.SuppressErrorMsg = false
  loc:StoreCurrentError = self.error                ! Save, this now so that if the Init does not return errors that GetServers may normally produce
  loc:StoreCurrentErrorString = self.ErrorString    ! Save

  self.GetServers()   ! Refresh the self.servers queue with the list of public servers
                      ! If the programmer redirects this queue to his own queue
                      ! he/she must call self.GetServers() & ServersChanged() again

  self.SuppressErrorMsg = loc:StoreSuppressErrorMsg  ! Restore
  self.Error = loc:StoreCurrentError                 ! Restore
  self.ErrorString = loc:StoreCurrentErrorString     ! Restore

  if records (self.servers) > 0
    self.ServersChanged()       ! If there are records in self.servers after calling GetServers call ServersChanged()
  end

  ! The self.prime is a counter function which makes sure that each object gets
  ! it's own messages.

  ! Then we need to create a queue to hold the list of servers.  We assign this queue to the
  ! Servers property. But what if the user changes the property ? Then we won't be able to
  ! destroy the queue - the memory will LEAK !! ARGGHH !! No worries, we assign the queue
  ! to MyServers as well (which is private - so can't be changed by any user) and we'll
  ! destroy it using that.

  ! you might wonder why not just make the self.Servers property private ? But then the list
  ! of servers can't be accessed by the owner of the object. For example in the Demo app, there
  ! is a chat window which does exactly this so it can display the list of servers on the
  ! window.

  ! okay so much for that, now check for parameters, set defaults, and call the Server.init
  ! method. (Remember this Client class is based on the server class. That's because
  ! pretty much all clients are also Servers - in the sense that they'll receive a reply
  ! and have to process it.)

  ! NetNotify is one of the dll functions, it tells the Dll to notify this client (using
  ! the self.service.message+3 event) whenever the list of possible servers change. This
  ! keeps this client always uptodate on server availablity.

  ! Not that NetNotify doesn't update our list of servers when a server arrives
  ! or leaves, that's up to us to do (in the takeevent method ) - this merely informs
  ! that a change happened...
!------------------------------------------------------------------------------
NetClient.GetServers PROCEDURE                             ! Declare Procedure 4
x  long
  CODE
  self.error = NetAutoGetServer (self.Service,self.Servers)    ! get a list of the possible servers

  if self.error
    if self.error <> ERROR:NoServers and self.error <> ERROR:ServerNotFound
      self.errortrap('An error occurred while attempting to Get Servers on service ' & clip(self.Service.ServiceName), '(parent)NetClient.GetServers') ! Handle Errors except No Servers Error
    end
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetClient.GetServers', 'ServiceName = ' & clip(self.Service.ServiceName) & |
                ' records (self.server) = ' & records (self.servers))
    end
  ****

!------------------------------------------------------------------------------
NetClient.TakeEvent  PROCEDURE                             ! Declare Procedure 4
  CODE
  if ~self._initalised
    return                                   ! no point coming in here if the object not initalised
  end

  if event() = self.service.event + 3        ! other servers exist, get new list
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetClient.TakeEvent', 'Got a request to execute GetServers again (Will call ServersChanged and GetServers). ServiceName = ' & clip(self.Service.ServiceName))
      end
    ****

    self.GetServers()
    self.ServersChanged()                    ! Call Virtual Service - Servers Changed
                                             ! Added by Jono 5 June 2000
  end

  parent.takeEvent()                         ! Call NetServers.TakeEvent()

  ! if we received the self.service.message+3 event (which is the only client
  ! specific event) then we need to update our list of servers. We do that using
  ! the Self.GetServers method.

  ! Anyway, even if it isn't call the parent (server) bit to see if it's interested
  ! in this event...
!------------------------------------------------------------------------------
NetClient.Send       PROCEDURE                             ! Declare Procedure 4
x        long
cur      long
  CODE
  !---------------------------------------------------------------------------------
  ! packet.number (if used) , packet.binData packet.options and packet.datalen
  ! must be set by the actual client object before this is called
  ! also ExpectReply (defaults to 0), Broadcast(also defaults to 0), and if
  ! it's not a broadcast message self.UseServer (defaults to the 'fastest' server)

  if self.packet.binDataLen > NET:MaxBinData
    self.error = ERROR:InvalidStructureSize
    self.errortrap('Size of self.packet.binDataLen (' & self.packet.binDataLen & ') is larger than allowed (' & NET:MaxBinData & ').', 'NetClient.Send')
    return
  end

  if self.sendreply = 1                             ! The user wants to reply to the person who sent the packet.
    self.packet.options = bor(self.packet.options,net:response) ! Turn on the net:response option. The DLL will swop the ToNetName & FromNetName, the ToThead & FromThread and sort out the To IP address
  end
  self.sendreply = 0

  self.packet.ServiceName = self.Service.ServiceName

  if band (self.packet.options,net:response)
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetClient.Send', 'Sending this packet as a send reply packet (Net:Response)')
      end
    ****
  else
    self.Packet.FromNetName = NetAutoGetNetName()              ! Get the NetName of this process (e.g. spiff (first instance) or spiff1 (2nd instance))
    self.Packet.ToThread = NET:SERVER_THREAD               ! Jono added 23/6/1999
    self.Packet.FromThread = self.Service.Thread           ! Jono added 23/6/1999
  end

  if (self.Broadcast = 0)
    if self.Packet.ToNetName = ''
      if records (self.servers) > 0
        cur = pointer (self.servers)
        get (self.servers, random (1, records (self.servers))) ! Choose random server
        self.packet.ToNetName = self.servers.NetName
        get (self.servers, cur)
      end
    end
    do Sending
  else
    loop x = 1 to records(self.Servers)              ! loop through all the servers
      get(self.Servers,x)                            ! and send to each one
      self.Packet.ToNetName = self.Servers.NetName
      do Sending
    end
  end

!- - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - -
Sending  routine
    ! When you send to a server (from a client) you don't know what ToThread they are
    ! so we put in SERVER_THREAD, which tells the other machine you are looking for
    ! a server.
    ! You set the FromThread to yourself, so that the server knows who to respond to.

    self.Packet.PacketNumber = self._nextPacketNumber   ! Objects should not change PacketNumber and SetNumber
    self.Packet.SetNumber = self._nextSetNumber         ! They can set self.Packet.Options to NET:PARTOFSET and NET:LASTOFSET

    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetClient.Send', 'ServiceName = ' & clip(self.Service.ServiceName) & |
                  ' Sending Packet ToNetName = ' & clip(self.Packet.ToNetName) & |
                  ' ToThread = ' & self.Packet.ToThread & |
                  ' FromNetName = ' & clip(self.Packet.FromNetName) & |
                  ' FromThread = ' & self.Packet.FromThread & |
                  ' DataLength = ' & self.Packet.BinDataLen & |
                  ' Command = ' & self.Packet.Command & |
                  ' SetNumber = ' & self.Packet.SetNumber & |
                  ' PacketNumber = ' & self.Packet.PacketNumber & |
                  ' Options = ' & self.Packet.Options)
      end
    ****

    if band (self.Packet.Options, Net:SYNCHRONOUS)
      self.error = NetAutoSendSync (self.Packet)           ! Synchronous send
    else
      self.error = NetAutoSendAsync (self.Packet)          ! Asynchronous send (better)
    end

    if band (self.Packet.Options, NET:PARTOFSET)
      if band (self.Packet.Options, NET:LASTOFSET)
        self._nextSetNumber += 1
        self._nextPacketNumber = 0
      else
        self._nextPacketNumber += 1
      end
    else
      self._nextSetNumber += 1
      self._nextPacketNumber = 0
    end

    if self.error
      self.errortrap('An error occurred while attempting to send a packet from service ' & clip (self.service.serviceName), '(parent) NetClient.Send')
    end

!---------------------------------------------------------------------------------
  ! this is the typical stuff that needs to be done whenever a client wants to send
  ! a packet to a server.  Because this always has to be done we do it here, to save
  ! effort later.

  ! firstly we prime a lot of routine stuff for the packet, who it's from, the
  ! service name, the date and time.

  ! then based on the self.broadcast property send it to one or all of the servers
  ! The NetChat object is an example of sending to all servers, the NetTimeClient is
  ! an example of a client that uses just one server.

!------------------------------------------------------------------------------
NetChat.Init         PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetChat.Init', 'Starting NetChat Object')
    end
  ****
  parent.init(p_ServiceName, p_Operation)  ! Public Service
  self.broadcast = 1
  if self.personname = ''
    self.personname = NetAutoGetNetName()
  end

  ! NetChat is an example of both a server and a client.  Well actually all clients
  ! are servers, but this one is a 'public' server. In other words it can receive
  ! stuff it hasn't asked for. The NetTimeClient is an example of a 'private' server
  ! which can only receive relies to questions that it's asked.

  ! Self.Broadcast means that all messages are sent to all servers, not just
  ! a particular server.

  ! Self.PersonName is specific to this object - it gets the name of the computer
  ! which can then displayed on the chat window.
!------------------------------------------------------------------------------
NetChat.Send         PROCEDURE                             ! Declare Procedure 4
  CODE
  self.getservers()
  if self.broadcast = 1
    self.packet.ToNetName = ''                               ! Broadcast is on, send to everyone
  end
  self.packet.binData = clip(self.PersonName) & ': ' & |
                        self.entrycontrol{prop:screentext} & '<13,10>'
  self.packet.BinDataLen = len(clip(self.packet.BinData))
  self.packet.command = 0                                  ! We don't use command
  self.packet.options = Net:SinglePacket                   ! Single packet
  parent.send()

  display(self.entrycontrol)
  select(self.entrycontrol)
  display(self.viewcontrol)

  self.process()             ! show own text in amongst the rest sent to us

  ! this is the reverse of the process method, this is called when the user clicks the
  ! send button.  So we
  ! a) refresh the servers list
  ! b) set the packet data (bindata) and the length (datalength)
  ! c) call the send method to send the packet.

  ! Then cunningly because we want to see our text in with the stuff that got
  ! sent to us, we callour own process method to display it.
!------------------------------------------------------------------------------
NetChat.Process      PROCEDURE                             ! Declare Procedure 4
  CODE
  self.viewcontrol{prop:text} = clip (self.packet.bindata[1 : self.packet.BinDataLen]) & |
                                clip(self.viewcontrol{prop:text})
  display(self.viewcontrol)

  ! remember this method is called when data is received from another NetChat
  ! object. So what we do here is process the packet.

  ! In this case we strip out the text, and add it to the 'text' control
  ! which is on the chat window. Oh and of course do a 'display' so the screen
  ! is updated.
!------------------------------------------------------------------------------
NetFileServer.Process PROCEDURE                            ! Declare Procedure 4
  CODE
    ! a request for a file has been received
    ! self.packet.bindata = the filename of the file required...
    if size(self.filename) < self.packet.BinDataLen         ! Ensure that the file name passed is not too long (should never happen)
                                                            self.Log ('NetFileServer.Process', 'FileSize is too big', NET:LogError)
      self.packet.options = Net:SinglePacket                ! Single packet only
      self.packet.binDataLen = 0                            ! No BinData
      self.packet.command = 1                               ! 1 = filename too large
      self.sendreply = 1                                    ! Send back this reply
    else
      self.filename = self.packet.bindata[1 : self.packet.BinDataLen]
      Do HandleGet
    end


HandleGet routine
                                                            self.Log ('NetFileServer.Process', 'Received request for file = ' & clip(self.filename))
    Share(self.dosfile)
    if errorcode()                                          ! Cannot read from file
        self.Log ('NetFileServer.Process', 'Error could not open file = ' & clip(self.filename), NET:LogError)
        self.packet.options = Net:SinglePacket              ! Single packet only
        self.packet.binDataLen = 0                          ! No BinData
        self.packet.command = 2                             ! 2 = unable to open file
        self.sendreply = 1                                  ! Send back this reply
    else
                                                            self.Log ('NetFileServer.Process', 'Successfully opened file = ' & clip(self.filename))
        self.filesize = bytes(self.dosfile)
        self.filecounter += 1
        set(self.dosfile)
        self.sending = true                                 ! Okay we are sending this file (see MultiPacketReply for more)
        self.firstpacket = true                             ! About to send the firstpacket in MultiPacketReply
        self.packet.command = 0                             ! All fine
        self.sendreply = 1                                  ! Send back data using MultiPacketReply method
    end
!------------------------------------------------------------------------------
NetFileServer.MultiPacketReply PROCEDURE                   ! Declare Procedure 4
  CODE
  if self.sending                                     ! We are trying to send the file
    if self.packet.command = 0
      if self.firstpacket
        self.packet.binData = self.fileSize                      ! first send length of file
        self.packet.BinDataLen = len(clip(self.packet.binData))  ! set packet length
        self.packet.command = 0
        self.packet.options = Net:PartOfSet                      ! more to come ....
        self.firstpacket = false                                 ! The next packet wont be the firstpacket
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetFileServer.Process', 'Sending filesize = ' & clip(self.filesize))
          end
        ****
        if self.progresscontrol
          self.progresscontrol{PROP:RangeLow} = 0      ! zero to 100 percent
          self.progresscontrol{PROP:RangeHigh} = 100   ! zero to 100 percent
          self.progresscontrol{prop:hide} = 0
          self.progresscontrol{prop:progress} = 0
        end
      else
        ! Not the firstpacket, so send bits of the file
        next(self.dosfile)
        if errorcode()
          self.packet.command = 3
          self.packet.binDataLen = 0                             ! 3 is the error place
          self.packet.options = NET:PartOfSet + Net:LastOfSet    ! no more packets, after this
          self.sending = false
          compile ('****', NETTALKLOG=1)
            if self.LoggingOn
              self.Log ('NetFileServer.Process', 'Could not read next part of file = ' & clip(self.filename) & ' will close the file and send error 3 to client', NET:LogError)
            end
          ****
          close(self.dosfile)
          if self.progresscontrol
            self.progresscontrol{prop:hide} = 1
            self.progresscontrol{prop:progress} = 0
          end
        else
          self.packet.binData = self.record
          self.packet.BinDataLen = bytes(self.dosfile)           ! number of bytes read from file
          self.packet.options = Net:PartOfSet
          self.packet.command = 0
          self.fileposition += bytes(self.dosfile)
          compile ('****', NETTALKLOG=1)
            if self.LoggingOn
              self.Log ('NetFileServer.Process', 'Sending next part of file = ' & clip(self.filename) & ' fileposition = ' & self.fileposition)
            end
          ****
          if self.progresscontrol
            self.progresscontrol{prop:progress} = (self.FilePosition / self.FileSize) * 100  ! Percentage Progress
          end
          if self.fileposition >= self.filesize
            self.sending = false
            self.packet.options = NET:LastOfSet + Net:PartOfSet  ! Last packet (MultiPacketReply() will not be called again)
            self.fileposition = 0
            close (self.dosfile)                                 ! Added by Jono 2 Aug 1999
            compile ('****', NETTALKLOG=1)
              if self.LoggingOn
                self.Log ('NetFileServer.Process', 'Finished sending file = ' & clip(self.filename) & ' -- Last packet')
              end
            ****
            if self.progresscontrol
              self.progresscontrol{prop:hide} = 1
              self.progresscontrol{prop:progress} = 0
            end
          else
                                                                  ! There will be another packet after this one
          end
        end
      end
    else ! Something went wrong in Process() and command <> 0
      ! Packet will be sent
      ! Because we don't set self.packet.options to NET:PartOfSet there won't be another packet after this and MultiPacketReply() wont be called
      self.sending = false
    end
  end

!------------------------------------------------------------------------------
NetTimeServer.Process PROCEDURE                            ! Declare Procedure 4
ourData    group                 ! We make a group to store
time         long                ! the data we want to send
date         long
           end

ourString  string(size(ourData)),over(ourData)
                                 ! then we create a string
                                 ! over it.

  CODE
  ! This method is triggered when a request is received from a client. All we have to
  ! do is prime the packet information with the correct data, the server object will
  ! do the rest.

  if self.packet.command = 1                   ! Client has requested the time
    self.dataChanged = 1                       ! Our count, and time and date data has changed
    self.counter += 1                          ! store how many times this object has received a request
    self.lastRequestFrom = self.packet.FromNetName ! Store who requested the time last - jono 16/6/1999

    ourData.time = clock()                     ! prime the data in the packet. ourData is a local group,
    ourData.date = today()                     ! over ourString, which is a string

    self.lastTime = ourData.time               ! store last time sent
    self.lastDate = ourData.Date               ! store last date sent

    self.packet.bindata = ourString [1 : size(ourString)]
    self.packet.BinDataLen = 8                 ! set the length of the data
    self.packet.command = 2                    ! 2 = here comes the date and time
    self.packet.options = Net:SinglePacket     ! Single packet
    self.SendReply = 1                         ! and tell the server object to send this packet
  end

  ! note : there is no "MultiPacketReply" method defined for the timeserver object so
  !        just the one packet (which is all we need) will get sent to the client

  ! because this is a virtual method, it will not be called in classes which are
  ! derived from this class, and have their own PROCESS method.
!------------------------------------------------------------------------------
NetGetTime.Ask       PROCEDURE                             ! Declare Procedure 4
  CODE
  ! this method fetches the time from a server. .

  self.getservers()                         ! will hunt down servers and pick one...
  if self.error = 0                         ! if a server exists...
    self.packet.ToNetName = ''              ! We don't mind who serves us. (or we could specify a specific server)
    self.packet.Command = 1                 ! Command 1 = Get the Time
    self.packet.BinDataLen = 0              ! the server doesn't take any data...
    self.packet.options = Net:SinglePacket  ! Single packet

    self.send()                           ! simply asking will be enough...
  end

  ! when the answer is received, then the NetGetTime.Process method
  ! will get called...

  !Note that if no Servers exist self.error will not be 0 - jono 16/6/1999
!------------------------------------------------------------------------------
NetGetTime.Process   PROCEDURE                             ! Declare Procedure 4
ourData    group                 ! We make a group to store
time         long                ! the data we want to send
date         long
           end

ourString  string(size(ourData)),over(ourData)
                                 ! then we create a string
                                 ! over it.

  CODE
! This method is called when the answer from the TimeServer object is received.

  ! overD is a local string var, over the group D.
  ! bindata is the data received from the server

  if self.packet.command = 2                  ! Server has sent us the date and time
    self.receivedFrom = self.packet.fromNetName   ! store which server sent us the data

    ourString = self.packet.bindata[1 : size(ourString)]

    ! there are two local variables ourData.date and ourData.time which now contain the
    ! date and time, in standard clarion formats, as received from the NetTimeServer
    ! object.  We can do pretty much anything with them.

    self.date = ourData.date
    self.time = ourData.time
  end

  ! this sets the object's properties so the stuff can be used by the calling program
!------------------------------------------------------------------------------
NetServer.Prime      PROCEDURE                             ! Declare Procedure 4
  CODE
  self._Wait()
  assert (~self._initalised) ! jono 16/6/1999

  if ~self._initalised
    self.service.Event = NetGetUniqueEvent()
    if self.service.Event = 0
      compile ('****', NETTALKLOG=1)
        self.Log ('NetServer.Prime', 'Error getting new unique event. None was available.', NET:LogError)
      ****
    end
    self._initalised = 1
  end
  self._Release()

  ! this is a simple method which sets the base message for this object.
  ! self.service.Event is set so that each object get's it's own events.
  ! Each instance of each object is currently assigned 5 events.
!------------------------------------------------------------------------------
NetFileServer.Init   PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
  CODE
  ! This method starts (initialises) the server running on the computer. Actually it's all
  ! done by the Server's init method - except we set the name of the service
  ! here.

  parent.init(p_ServiceName, p_Operation)  ! Public Service
  self.dosfile &= NETdosfiletype
  self.filename &= NETdosfilename
  self.record &= NETdosfile:a

!------------------------------------------------------------------------------
NetGetFile.Init      PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
  CODE
  ! this method performs the job of initalising the object

  p_Operation = bor (p_Operation, Net:Private)  ! Private Service
  parent.init(p_ServiceName, p_Operation)

  if band (p_Operation, Net:Silent)
    self.silentmode = true             ! suppresses error & information messages
  end

  self.currentSetNumber = -1           ! We aren't receiving a file at the moment

  self.dosfile &= NETDosFileType       ! Setup the DOS file properties
  self.record &= NETDosfile:a
  self.filename &= NETDosFileName

  ! NetFileSend is the name of the server we're looking for. If you look at the
  ! NetFileSend object, at the Init method, you'll see where we've set the name.

  ! The private bit is because this is a client, not a general server. However
  ! it is a 'private' server because it can receive a reply from the genuine
  ! public server.

!------------------------------------------------------------------------------
NetGetFile.Ask       PROCEDURE  (string fileNameOnServer,string fileNameLocal) ! Declare Procedure 3
Thisfile   String(255)
  CODE
    ! This method gets a file from a server
    self.filename = fileNameLocal                           ! local file name to store the file we get in...
    self.serverFileName = Clip(fileNameOnServer)
    if self.silentmode = false
        Do TestForReplace                                   ! Test if we need to replace the file
    end
                                                            self.Log ('NetGetFile.Ask', 'Requesting file = ' & clip (self.filename))
    self.Getservers()                                       ! Find servers
    if self.error = 0                                       ! If a server exists...
        if self.UseThisServer <> ''
          self.packet.toNetName = Clip(self.useThisServer)  ! Use the one specified by the user
        else
          self.packet.toNetName = ''                        ! We don't mind who serves us (NetClient will choose a random one for us)
        end
        self.packet.command = 0                             ! We don't use command
        self.packet.bindata = self.serverFileName           ! Contains the name of the file to get
        self.packet.BinDataLen = len(clip(self.packet.bindata))
        self.packet.options = Net:SinglePacket              ! Single Packet
        self.Send()                                         ! Send the packet
        if self.error <> 0
            if self.silentMode = false
                Message ('Could not send your request to a File Server', 'NetTalk GetFile', ICON:ASTERISK)
            end
                                                            self.Log ('NetGetFile.Ask', 'Could not send your request to a File Server', NET:LogError)
            self.Done(1)
            !Close(self.dosfile)
        end
    else
        if self.silentMode = false
            Message ('Sorry no servers could be located. Error = ' & self.InterpretError(), 'NetTalk GetFile', ICON:ASTERISK)
        end
                                                            self.Log ('NetGetFile.Ask', 'No Server could be located', NET:LogError)
        self.Done(1)
    end


TestForReplace Routine
  open(self.dosfile)                                        ! Try and open the local file
  if errorcode() = 0                                        ! 0 = file exits and it opened fine
    close (self.dosfile)
    if message ('Do you want to replace your existing ' & clip (self.filename),'NetTalk Get File',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No,0) = BUTTON:Yes
      ! All cool. File will be replaced in Process()
    else
      ! The guy does not want to replace it so ignore it
      self.done(1)
      return                                                ! Bail out
    end
  end

  ! when the answer is received, then the Process method
  ! will get called...
!------------------------------------------------------------------------------
!------------------------------------------------------------------------------
NetGetFile.Process   PROCEDURE                             ! Declare Procedure 4
static:count      long, static

  CODE
  if self.packet.command = 0                          ! Valid command (ie the file is being sent fine)
    if self.packet.packetNumber = 0
      Do ProcessFirstPacket                           ! First Packet
    else
      Do ProcessRemainingPackets                      ! The packet received is not the First packet
    end
  elsif self.packet.command = 1
    Do Error1
  elsif self.packet.command = 2
    Do Error2
  elsif self.packet.command = 3
    Do Error3
  end

!----------------------------------------------------------------------------------------

ProcessFirstPacket Routine
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetGetFile.Process (FirstPacket)', 'Opening FileName = ' & self.filename, NET:LogError)
    end
  ****
  open(self.dosfile)                              ! Try and open the local file
  if errorcode() = 0                              ! 0 = file exits and it opened fine
    Do EmptyFile                                  ! Delete the file so we can replace it
  elsif errorcode() = 2
    create(self.dosfile)                          ! Create the file, it did not exist
    if errorcode()
      if self.silentMode = false
        message ('Error creating file: Error = ' & clip(Error()) & '.', 'NetTalk File Transfer', ICON:ASTERISK)
      end
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetGetFile.Process', 'Could not create the local file. Error = ' & clip(Error()) & '.', NET:LogError)
        end
      ****
      Do CancelFile
      self.Done (1)                               ! Call self.done but an error occurred
      return
    end
    open(self.dosfile)
  end
  if errorcode()
    if self.silentMode = false
      message ('Error opening file: Error = ' & clip(Error()) & '.', 'NetTalk File Transfer', ICON:ASTERISK)
    end
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetGetFile.Process', 'Error opening file. Error = ' & Error(), NET:LogError)
      end
    ****
    Do CancelFile
    self.Done (1)                                 ! Call self.done but an error occurred
    return
  end

  if self.progresscontrol                          ! Update the Progress Control Control if there is one
    self.progresscontrol{PROP:RangeLow} = 0        ! zero to 100 percent
    self.progresscontrol{PROP:RangeHigh} = 100     ! zero to 100 percent
    self.progresscontrol{prop:hide} = 0
    self.progresscontrol{prop:progress} = 0
    display (self.progresscontrol)
  end
                                                   ! Get the data sent to us in the packet
  self.filesize = self.packet.bindata[1 : self.packet.BinDataLen]  ! contains the length of the file
  self.fileposition = 0

  self.currentSetNumber = self.packet.SetNumber
  self.currentFromNetName = self.packet.FromNetName

  ! This object can only handle reading one file from a sever object at a time.
  ! Use the done() method to know when you can request the next file.

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetGetFile.Process', 'Received first packet. filesize = ' & self.filesize)
    end
  ****

!----------------------------------------------------------------------------------------

ProcessRemainingPackets Routine
  if (self.currentSetNumber <> self.packet.SetNumber) and (self.currentFromNetName <> self.packet.FromNetName)
    exit  ! We received an old SetNumber (each file arrives as a set, so ignore this packet)
  end
  if self.packet.binDataLen > 0
    self.record = self.packet.bindata[1 : self.packet.BinDataLen]  ! Populate the Dos Record with the data in the packet
  else
    self.record = ''
  end
  self.fileposition += self.packet.BinDataLen      ! Update our fileposition
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetGetFile.Process', 'Received' & |
                ' self.packet.number = ' & self.packet.PacketNumber & |
                ' self.fileposition = ' & self.fileposition)
    end
  ****
  add(self.dosfile,self.packet.BinDataLen)         ! save the record to disk
  if errorcode()
    if self.silentMode = false
      message ('Error adding to the file that is being downloaded ' & clip(error()) & '.', 'NetTalk File Transfer', ICON:ASTERISK)
    end
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetGetFile.Process', 'Could not perfrom add command to the local file', NET:LogError)
      end
    ****
    Do CancelFile
    self.Done (1)                                  ! Call self.done but an error occurred
    return
  end

  if self.progresscontrol                          ! Update the Progress Control
    static:count += 1
    if (static:count % 10) = 1
      self.progresscontrol{prop:progress} = (self.FilePosition / self.FileSize) * 100  ! Percentage Progress
      display(self.progresscontrol)
    end
  end

  if self.fileposition >= self.filesize            ! We have finished
    display()
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetGetFile.Process', 'Closing file (' & clip (self.filename) & ') '  & |
                  ' self.fileposition = ' & self.fileposition & |
                  ' self.filesize = ' & self.filesize)
      end
    ****
    close(self.dosfile)
    if errorcode () then
      if self.silentMode = false
        message ('Could not close the file being downloaded.|Error = ' & clip(Error()) & '.', 'NetTalk File Transfer', ICON:ASTERISK)
      end
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetGetFile.Process', 'Could not close the local file. Error = ' & Error(), NET:LogError)
        end
      ****
      Do CancelFile
      self.Done (1)                               ! Call self.done but an error occurred
    else
      if self.progresscontrol
        self.progresscontrol{prop:progress} = 100
      end
      if self.silentMode = false
        !message ('Downloaded ' & clip(self.filename) & ' from ' & clip(self.DefaultServerNetName) & ' successfully.', 'NetTalk File Transfer', ICON:ASTERISK)
        message ('Downloaded ' & clip(self.filename) & ' from ' & clip(self.packet.FromNetName) & ' successfully.', 'NetTalk File Transfer', ICON:ASTERISK)
      end
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetGetFile.Process', clip(self.filename) & ' downloaded successfully.')
        end
      ****
      if self.progresscontrol
        self.progresscontrol{prop:progress} = 0
        self.progresscontrol{prop:hide} = 1
      end
      self.currentSetNumber = -1                  ! We aren't busy with a file at the moment
      self.currentFromNetName = ''
      self.Done (0)                               ! Call self.done all fine
    end
  end

CancelFile Routine
  self.currentSetNumber = -1                      ! Ignore further packets received for this file
  self.currentFromNetName = ''

  ! XXX We should send a message back to the sender here, to cancel the sending of further packets

!----------------------------------------------------------------------------------------

DeleteFile Routine
  remove (self.dosfile)
  if errorcode()
    if self.silentMode = false
      message ('Error deleting file: Error = ' & clip(Error()) & '.', 'NetTalk File Transfer', ICON:ASTERISK)
    end
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetGetFile.Process', 'Could not delete the contents of the local file. Error = ' & Error(), NET:LogError)
      end
    ****
    Do CancelFile
    self.done(1)
    return
  end

EmptyFile Routine
  empty (self.dosfile)
  if errorcode()
    if self.silentMode = false
      message ('Error deleting file: Error = ' & clip(Error()) & '.', 'NetTalk File Transfer', ICON:ASTERISK)
    end
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetGetFile.Process', 'Could not empty the contents of the local file. Error = ' & Error(), NET:LogError)
      end
    ****
    Do CancelFile
    self.done(1)
    return
  end

!----------------------------------------------------------------------------------------

Error1 Routine
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetGetFile.Process', 'Received Error Packet Number 1.' & |
                ' This is a bizzare error where the filename is longer than the memory allocated for it on the Server. Closing the local File', NET:LogError)
    end
  ****

  Do DeleteFile

  if self.silentMode = false
    message ('Could not download ' & clip(self.filename) & ' file name too long.', 'NetTalk File Transfer', ICON:ASTERISK)
  end
  if self.progresscontrol
    self.progresscontrol{prop:progress} = 0
    self.progresscontrol{prop:hide} = 1
  end
  close(self.dosfile)
  Do CancelFile
  self.done(1)

!----------------------------------------------------------------------------------------

Error2 Routine                                       ! File not found (or could not be opened) on the server !
  ! This Routine was changed to not try to get the file from another Server, if the file could not be found.
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetGetFile.Process', 'Received Error Packet Number 2.' & |
                ' This means the server could not open the file. We will attempt to use another server to locate the file.')
    end
  ****
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetGetFile.Process', 'The file could not be found on any of the servers that were tried. Closing the local file.', NET:LogError)
    end
  ****
  if self.silentMode = false
    message ('Could not download ' & clip(self.filename) & '|File not found on Server.', 'NetTalk File Transfer', ICON:ASTERISK)
  end
  if self.progresscontrol
    self.progresscontrol{prop:progress} = 0
    self.progresscontrol{prop:hide} = 1
  end
  close(self.dosfile)
  Do CancelFile
  self.done(1)

!----------------------------------------------------------------------------------------

Error3 Routine
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetGetFile.Process', 'Received Error Packet Number 3.' & |
                ' The file could not be read on the Server. Closing the local File', NET:LogError)
    end
  ****

  close(self.dosfile)
  Do DeleteFile

  if self.silentMode = false
    message ('Could not download ' & clip(self.filename) & '|The Server has failed to read the file.', 'NetTalk File Transfer', ICON:ASTERISK)
  end
  if self.progresscontrol
    self.progresscontrol{prop:progress} = 0
    self.progresscontrol{prop:hide} = 1
  end
  Do CancelFile
  self.done(1)


!------------------------------------------------------------------------------
NetGetTime.Init      PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
  CODE
  ! this method performs the job of initalising the object

  p_Operation = bor (p_Operation, Net:Private)  ! Private Service
  parent.init(p_ServiceName, p_Operation)

  ! NetTimeServer is the name of the server we're looking for. If you look at the
  ! NetTimeServer object, at the Init method, you'll see where we've set the name.

  ! The private bit is because this is a client, not a general server. However
  ! it is a 'private' server because it can receive a reply from the genuine
  ! public server.
!------------------------------------------------------------------------------
NetClient.Kill       PROCEDURE  (long p_Options=NET:StopService) ! Declare Procedure 3
  CODE
  if ~self._initalised
    return  ! bj 29/6/01
  end
  ! This was previously causing trouble...! for some reason this is causing a gpf - temporarily removed...
  ! Jono uncommented it and changed self.myservers to self._myservers 12 Dec 2000

  parent.kill(p_Options)

  if ~self._myservers&=NULL  ! Moved after kill by Jono 15/3/2001
    dispose(self._myservers)
  end

  ! This stops (kills) the client.
  ! remember the trick we played with the queue pointer? [See NetClient.Init]
  ! Okay so here we prevent the leak by disposing of the queue we created.

  ! The we call the server part of this client to kill it as well...
!------------------------------------------------------------------------------
NetCloseApps.Init    PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
  CODE
  p_Operation = bor (p_Operation, Net:Private)                ! Private Service
  parent.init(p_ServiceName, p_Operation)

  self.broadcast = 1                                          ! BroadCast mode (ie sent to all)

  ! This is a private service, unlike the NetAutoClose object which is non-private
  ! Self.Broadcast means that all messages are sent to all servers, not just
  ! a particular server.

!------------------------------------------------------------------------------
NetCloseApps.SendWarning PROCEDURE  (string WarningMessage) ! Declare Procedure 3
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetCloseApps.SendWarning', 'Sending Warning to all machines')
    end
  ****

  self.getservers()                                         ! Refresh our list of servers
  self.packet.command = 1                                   ! 1 means send warning. With warning text in BinData
  self.packet.BinData = clip (WarningMessage)
  self.packet.BinDataLen = len (clip(self.packet.binData))
  self.packet.options = Net:SinglePacket

  self.send()                                             ! Send the packet

! This method sends a Shutdown Warning to all of the other programs.
! The actual packet data, in this case just a Chr (1) is interpreted
! by the Process method to be a warning


!------------------------------------------------------------------------------
NetCloseApps.SendShutdown PROCEDURE  (string CloseMessage) ! Declare Procedure 3
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetCloseApps.SendShutdown', 'Sending Shutdown to all machines')
    end
  ****

  self.packet.command = 2                   ! 2 means it is a Force Close command
  self.packet.BinData = clip (CloseMessage)
  self.packet.BinDataLen = len (clip(self.packet.binData))
  !self.packet.BinDataLen = 0
  self.packet.options = Net:SinglePacket

  self.send()                             ! Send the packet

! This method sends a Shutdown command to all of the other programs.
! The actual packet data, in this case just a Chr (2) is interpreted
! by the Process method to be an actual shutdown.


!------------------------------------------------------------------------------
NetAutoCloseServer.Process PROCEDURE                       ! Declare Procedure 4
sendEvent long   ! Stores a copy of any NetTalk Events we receive for this object

  CODE
  ! This is where we work out what to do when a NetCloseAnApp sends us a message

  case Self.Packet.Command
  !======
  of 1
  !======
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetAutoCloseServer.Process', 'Warning message received from ' & clip (self.packet.FromNetName) & '. Will call self.ProcessWarningRequest()')
      end
    ****
    self.ProcessWarningRequest()

  !======
  of 2
  !======
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetAutoCloseServer.Process', 'Shutdown message received from ' & clip (self.packet.FromNetName) & '. Will call self.ProcessShutdownRequest()')
      end
    ****
    self.ProcessShutdownRequest()

  !======
  !of 3
  !======
  !  halt

  end

!------------------------------------------------------------------------------
NetClient.UseAnotherServer PROCEDURE                       ! Declare Procedure 4
local                         group, pre (loc)
lastDefaultServerNetName        like (self.packet.ToNetName)
x                               long
recs                            long
                              end

loc:ourQ                      Queue
x                               long
                              end
  CODE
  ! UseNextServer will choose another server.
  ! This caters for objects which may have multiple servers running on different machines,
  ! but each server may only be able to deliver a subset of the total functionality of all the
  ! servers put together. For instance 3 file servers may be running, but a particular file may
  ! only be found on two of the servers and not the third. UseNextServer would provide the object
  ! with the next server to try and locate the file on

  self.GetServers() ! Make sure self.servers is as up-to-date as possible

  loc:lastDefaultServerNetName = self.packet.ToNetName
  self.error = 0                                            ! All fine
  loc:recs = records (self.Servers)

  if loc:recs = 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetClient.UseNextServers', 'ServiceName = ' & clip(self.Service.ServiceName) & |
                  ' There are no Servers in the self.Servers queue. You must call GetServers before calling UseNextServers. Don''t call UseNextServers when GetServers fail.', NET:LogError)
      end
    ****
    self.error = ERROR:NoServers
    return
  elsif loc:recs = 1
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetClient.UseNextServers', 'ServiceName = ' & clip(self.Service.ServiceName) & |
                  ' Warning. There is only one record in the Servers Queue. Will have to use this one.', NET:LogError)
      end
    ****
    get (self.Servers,1)
    self.packet.ToNetName = self.Servers.NetName
  else
    ! Try a quick shot (will work well when self.servers is large)
    Get(self.Servers, random (1, loc:recs))   ! Get random item
    if clip(self.Servers.NetName) <> clip (loc:lastDefaultServerNetName)
      ! Cool
      self.packet.ToNetName = self.Servers.NetName
    else
      ! Not cool - we picked the same one as last time. Will have to use a more intelligent (but slower) algorithm
      ! Build up a temp Queue of all the record numbers in self.servers
      loop loc:x = 1 to loc:recs
        loc:ourQ.x = loc:x
        add (loc:ourQ)
      end

      loop loc:x = loc:recs to 1 by -1
        Get(loc:ourQ, random (1, loc:x)) ! Get one random item from ourQ
        Get(self.Servers, loc:ourQ.x)   ! Get the matching item from self.servers
        if clip(self.Servers.NetName) <> clip (loc:lastDefaultServerNetName)
          ! cool we found one.
          self.packet.ToNetName = self.Servers.NetName
          break
        end
      end
      if clip(self.Servers.NetName) = clip (loc:lastDefaultServerNetName)
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetClient.UseNextServers', 'ServiceName = ' & clip(self.Service.ServiceName) & |
                      ' Warning. We tried to get a different server but could not. Will have to use this one. ToNetname = ' & clip (self.packet.ToNetName), NET:LogError)
          end
        ****
      end
    end
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetClient.UseNextServer', 'ServiceName = ' & clip(self.Service.ServiceName) & |
                ' lastToNetName = ' & clip (loc:lastDefaultServerNetName) & |
                ' Using next self.Packet.ToNetName = ' & clip(self.packet.ToNetName))
    end
  ****


!------------------------------------------------------------------------------
NetGetFile.Done      PROCEDURE  (byte ErrorOccurred)       ! Declare Procedure 3
  CODE
! Virtual Process to notify the user that the file has been received
! If ErrorOccurred = true then an error occurred, but you can request another now
!------------------------------------------------------------------------------
NetClient.ServersChanged PROCEDURE                         ! Declare Procedure 4
  CODE
!---------------------------------------------------------------------------------
! This is a virtual method handled by derivative classes.
! This method is called when there is a change (or possible change) in the Servers list
! It will sometimes get called even when there is no change to the Servers List.
! You can put your own code in your object.ServersChanged list
!---------------------------------------------------------------------------------

!------------------------------------------------------------------------------
NetAutoCloseServer.Init PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
  CODE
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetAutoCloseServer.Init', 'Starting AutoCloseServer Object')
    end
  ****
  if self.DefaultWarningMessage = ''
    self.DefaultWarningMessage = 'For maintainence reasons this program will teminate in the next 60 seconds. Please immediately save what you''re doing and exit the program.'
  end
  if self.DefaultCloseDownMessage = ''
    self.DefaultCloseDownMessage = ''
  end
  parent.init(p_ServiceName, p_Operation)
!------------------------------------------------------------------------------
NetAutoCloseServer.CloseApp PROCEDURE                      ! Declare Procedure 4
x    long

  CODE
! This is a virtual method that gets called when the application must close
! You can derive you own code in it.

  post(event:closewindow,,1)    ! Shut down our whole application
  post(event:closewindow,,1)
  loop x = self.MaxThreads to 1 by -1
    post (event:Maximize,,x)
    post (event:closewindow,,x)    ! Shut down our whole application
    post (event:closedown,,x)      ! Shut down our whole application
    post (event:GainFocus,,x)
    post (event:LoseFocus,,x)
    post (event:GainFocus,,x)
    yield()
  end
  loop x = self.MaxThreads to 1 by -1
    post (event:Maximize,,x)
    post (event:closewindow,,x)    ! Shut down our whole application
    post (event:closedown,,x)      ! Shut down our whole application
    post (event:GainFocus,,x)
    post (event:LoseFocus,,x)
    post (event:GainFocus,,x)
    yield()
  end
  display()
  yield()


!------------------------------------------------------------------------------
NetSecurityClient.CheckLicense PROCEDURE                   ! Declare Procedure 4
  CODE
!------------------------------------------------------------------------------
NetSecurityClient.ServersChanged PROCEDURE                 ! Declare Procedure 4
  CODE
  clear (self.packet)
  if (self.RequestsRequired = 0) or (self._RequestsSent < self.RequestsRequired)
    if records (self.servers) >= (self._RequestsSent + 1)
      get (self.servers, (self._RequestsSent + 1))
      if errorcode() = 0
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetSecurityClient.ServersChanged', 'Will request license from ' & clip (self.servers.NetName))
          end
        ****
        self.packet.ToNetName = clip (self.servers.NetName)
        self.packet.command = NET:RequestSecPacket   ! Request Packet
        self.packet.binDataLen = 0
        self.packet.options = NET:SinglePacket
        self.send()
        self._RequestsSent += 1
      end
    end
  end
!------------------------------------------------------------------------------
NetSecurityClient.WriteLicense PROCEDURE                   ! Declare Procedure 4
  CODE
!------------------------------------------------------------------------------
NetSecurityClient.Process PROCEDURE                        ! Declare Procedure 4
  CODE
  if self.packet.command = NET:SecPacket
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSecurityClient.Process', 'Received a license from ' & clip (self.packet.FromNetName) & ' will call WriteLicense()')
      end
    ****
    self._RequestsRepliedTo += 1
    self.writeLicense()
  end


!------------------------------------------------------------------------------
NetSecurityServer.Process PROCEDURE                        ! Declare Procedure 4
  CODE
  if self.packet.command = NET:RequestSecPacket
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSecurityServer.Process', 'Got a request to send a license to ' & clip (self.packet.FromNetName))
      end
    ****
    self.sendreply = 1
    self.packet.command = NET:SecPacket
    self.PopulatePacket()
    ! Send will occurr automatically after Populate Packet as self.sendreply = 1
  elsif self.packet.command = NET:SecPacket
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSecurityServer.Process', 'Got a license from ' & clip (self.packet.FromNetName) & ' will call WriteLicense()')
      end
    ****
    self.writeLicense()
    self.countUsers()
  end

!------------------------------------------------------------------------------
NetSecurityServer.PopulatePacket PROCEDURE                 ! Declare Procedure 4
  CODE
!------------------------------------------------------------------------------
NetSecurityClient.Init PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
  CODE
  p_Operation = bor (p_Operation, Net:Private)  ! Private Service
  parent.init(p_ServiceName, p_Operation)

!------------------------------------------------------------------------------
NetSecurityServer.WriteLicense PROCEDURE                   ! Declare Procedure 4
  CODE
!------------------------------------------------------------------------------
NetSecurityServer.SendLicense PROCEDURE                    ! Declare Procedure 4
  CODE
  clear (self.packet)
  self.packet.command = NET:SecPacket
  self.packet.ToNetName = ''
  self.PopulatePacket()
  if self.packet.binDataLen <> 0
    self.broadcast = 1       ! all
    self.send()
    self.broadcast = 0
  end

!------------------------------------------------------------------------------
NetSecurityServer.TooManyCopies PROCEDURE                  ! Declare Procedure 4
  CODE
!------------------------------------------------------------------------------
NetSecurityServer.ServersChanged PROCEDURE                 ! Declare Procedure 4
  CODE
  self.countUsers()

!------------------------------------------------------------------------------
NetSecurityServer.CountUsers PROCEDURE                     ! Declare Procedure 4
loc:AlreadyWarned    byte, static
  CODE
  self.TooMany = false
  if self.CopiesAllowed > 0
    if records (self.servers) > (self.CopiesAllowed - 1)
      self.TooMany = true
      if loc:AlreadyWarned = 0
        loc:AlreadyWarned = 1
        self.TooManyCopies()        ! This will only get called as once for each time the users goes over the limit
      end
    end
  end

  loc:AlreadyWarned = self.TooMany
!------------------------------------------------------------------------------
NetRefresh.Init      PROCEDURE  (string p_ServiceName,long p_Operation=Net:StartService) ! Declare Procedure 3
  CODE
  if self.noRefreshes
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetRefresh.', 'self.noRefreshes = 0. Will NOT Initialise NetRefresh. ServiceName = ' & clip (p_ServiceName))
      end
    ****
    return
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetRefresh.', 'Initialising NetRefresh. ServiceName = ' & clip (p_ServiceName))
    end
  ****

  PARENT.Init(p_ServiceName,p_Operation)
  self.broadcast = 1

  self.Event = event:user+146

!------------------------------------------------------------------------------
NetRefresh.Process   PROCEDURE                             ! Declare Procedure 4
ThisRefreshCount        long
  CODE
  if self.noRefreshes
    return
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetRefresh.Process', 'Received Message, will post update command to all threads.')
    end
  ****

  if self.Date = today() and (clock()-self.time < self.WaitThisLong)
    return
  end

  if self.packet.binDataLen > 0
    self.depend = self.packet.bindata
  else
    self.depend = ''
  end
  self.Date = today()
  self.Time = clock()
  loop ThisRefreshCount = 1 to 64
    post(self.event,0,ThisRefreshCount)
  end

!------------------------------------------------------------------------------
NetRefresh.Send      PROCEDURE  (<STRING p>, long pMyself=1) ! Declare Procedure 3
  CODE
  if self.noRefreshes
    return
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetRefresh.Send', 'Sending Update to all other machines.')
    end
  ****

  self.packet.bindata = ''

  if not omitted(2)
    self.packet.bindata = p
  end

  self.packet.bindatalen = len(clip(self.packet.bindata))

  PARENT.Send()

  if pMyself
    self.Process()
  end

!------------------------------------------------------------------------------
NetServer.ReSync     PROCEDURE  (long p_PacketType=NET:PACKET:ICanDo,long p_Options=2) ! Declare Procedure 3
  ! This method causes the NetAuto objects to re-announce to all the other NetAuto instances
  ! the list of Services it offers.

  CODE
  NetAutoSendBroadCast (p_PacketType, p_Options)
!------------------------------------------------------------------------------
NetRefresh.NeedReset PROCEDURE  (*Long p_Date,*Long p_Time,<*String p_Depend>) ! Declare Procedure 3
ReturnValue   byte
dep           string(1024)
pkt           string(1024)
x             long
y             long
z             long

  CODE
  if self.noRefreshes
    return(0)
  end

  case event()
  of self.event
    if not omitted(4)
      dep = p_Depend
    end
    p_date = today()
    p_time = clock()
    if self.Depend = '' or dep = ''
      ReturnValue = 1
    else
      do CheckDependancies
    end
  of event:gainfocus
    if (self.Date > p_Date) or ((self.Time > p_time) and (self.Date = p_Date))
     p_date = today()
     p_time = clock()
     ReturnValue = 1
    end
  end
  Return ReturnValue

CheckDependancies  Routine
  pkt = self.Depend
  if pkt[1] <> '|'
    exit
  end
  x = 1
  loop
    y = instring('|',pkt,1,x+1)
    if y = 0
      break
    end
    if x > 1024 or y > 1024
      break
    end
    z = instring(pkt[x : y],dep,1,1)
    if z > 0
      ReturnValue = 1
      Break
    end
    x = y
  end


!------------------------------------------------------------------------------
NetAutoCloseServer.ProcessWarningRequest PROCEDURE  ()     ! Declare Procedure 3
! -----------------------------------------------------------------------------
! Programmers - You may want to override this method yourself.
! -----------------------------------------------------------------------------
DisplayText          STRING(400)
CloseDownText        STRING(400)
MessageFrom          STRING(80)


WarningWindow WINDOW('Application Shutdown Warning'),AT(,,239,118),FONT('Tahoma',8,,FONT:regular),CENTER, |
         TIMER(4500),SYSTEM,GRAY,DOUBLE
       BUTTON('OK'),AT(179,98,50,14),USE(?CloseButton1),DEFAULT
       BOX,AT(3,2,230,113),USE(?Box1),ROUND,COLOR(COLOR:Gray),LINEWIDTH(1)
       STRING('Shutdown Warning'),AT(14,6,208,15),USE(?String2),TRN,CENTER,FONT('Tahoma',14,0A00000H,FONT:regular)
       STRING(@s64),AT(9,100,165,8),USE(MessageFrom,,?WarningMessageFrom),TRN,LEFT,FONT(,,COLOR:Gray,)
       TEXT,AT(9,25,219,68),USE(DisplayText),VSCROLL,CENTER,READONLY
       BUTTON,AT(199,3,27,20),USE(?Button2),SKIP,FLAT,ICON(ICON:Exclamation)
     END


sendEvent long   ! Stores a copy of any NetTalk Events we receive for this object

  CODE
  if 0{Prop:iconize} = true
    0{Prop:iconize} = false
  end

  if self.packet.BinDataLen > 0 ! Jan 2005 Index Range Fix
    DisplayText = self.packet.binData [1 : self.packet.BinDataLen]
  else
    DisplayText = ''
  end
  messageFrom = self.translate('Message sent from') & ' ' & clip (self.packet.FromNetName)
  if DisplayText = ''
    DisplayText = self.DefaultWarningMessage
  end
  sendEvent = 0
  open (warningwindow)  ! This window is displayed for 45 seconds
  WarningWindow{prop:text} = self.translate(WarningWindow{prop:text})
  ?CloseButton1{prop:text} = self.translate(?CloseButton1{prop:text})
  ?String2{prop:text} = self.translate(?String2{prop:text})

  WarningWindow{prop:timer} = self.WarningTimer
  accept
    case event()
    of event:timer
    orof Event:Accepted
      break
    of (self.service.event) TO (self.service.event + NET:MAX_RESERVED_EVENTS)  ! NetTalk event for this object
      sendEvent = event()
      break
    end
  end
  close(warningwindow)
  if sendEvent
    post (sendEvent)           ! Send the NetTalk event again.
  end

!------------------------------------------------------------------------------
NetAutoCloseServer.Translate PROCEDURE (String pText)
  Code
  return clip(pText)
!------------------------------------------------------------------------------

!------------------------------------------------------------------------------
NetAutoCloseServer.ProcessShutdownRequest PROCEDURE  ()
! -----------------------------------------------------------------------------
! Programmers - You may want to override this method yourself.
! -----------------------------------------------------------------------------

DisplayText          STRING(400)
CloseDownText        STRING(400)
MessageFrom          STRING(80)

CloseDownWindow WINDOW('Application Shutdown Message'),AT(,,241,119),FONT('Tahoma',8,,FONT:regular),CENTER, |
         TIMER(1000),SYSTEM,GRAY,DOUBLE
       BUTTON('Close Now'),AT(180,97,50,14),USE(?CloseButton2),DEFAULT
       BOX,AT(5,2,230,113),USE(?Box2),ROUND,COLOR(COLOR:Red),LINEWIDTH(1)
       PROMPT('Shutdown Message'),AT(16,6,208,15),USE(?Prompt1),TRN,CENTER,FONT('Tahoma',14,COLOR:Red,FONT:regular)
       STRING(@s64),AT(11,99,165,8),USE(MessageFrom,,?CloseMessageFrom),TRN,LEFT,FONT(,,COLOR:Gray,)
       BUTTON,AT(201,3,27,20),USE(?Button4),SKIP,FLAT,ICON(ICON:Exclamation)
       TEXT,AT(11,25,219,68),USE(CloseDownText),VSCROLL,CENTER,READONLY
     END


sendEvent long   ! Stores a copy of any NetTalk Events we receive for this object

  CODE
  if 0{Prop:iconize} = true
    0{Prop:iconize} = false
  end
  if self.packet.BinDataLen > 0  ! Jan 2005 Index Range Fix
    CloseDownText = self.packet.binData [1 : self.packet.BinDataLen]
  else
    CloseDownText = ''
  end
  messageFrom = self.translate('Message sent from') & ' ' & clip (self.packet.FromNetName)
  if CloseDownText = ''
    CloseDownText = self.translate(self.DefaultCloseDownMessage)
  end
  open (CloseDownWindow)       ! This window is displayed for 5 seconds
  CloseDownWindow{prop:text} = self.translate(CloseDownWindow{prop:text})
  ?CloseButton2{prop:text} = self.translate(?CloseButton2{prop:text})
  ?Prompt1{prop:text} = self.translate(?Prompt1{prop:text})
  CloseDownWindow{prop:timer} = self.CloseDownTimer
  accept
    case event()
    of event:timer
    orof Event:Accepted
      break
    end
  end
  close(CloseDownWindow)
  self.CloseApp()               ! Virtual Call to closeApp() method
  display()                     ! Get everything to refresh


!------------------------------------------------------------------------------
NetAutoCloseServer.Construct PROCEDURE                     ! Declare Procedure 4
  CODE
  self.MaxThreads = 64
  compile('****',_VER_C60)         ! Clarion 6 only
    self.MaxThreads = 256          ! Although your application could have more (see Thread() in Clarion 6 help), but you can set your own value.
  ****
!------------------------------------------------------------------------------
NetServer.Announce   PROCEDURE  (long p_OnlyOncePerThread=0) ! Declare Procedure 3
ret           long
  CODE
  if p_OnlyOncePerThread
    if Net:gThreadedAnnounced <> NET:OnlyOncePerThread ! Net:gThreadedAnnounced is declared in NetTalk.clw
      Net:gThreadedAnnounced = 1
      ret = NetAutoSendBroadCast(NET:PACKET:ICanDo, 0)
    end
  else
    ret = NetAutoSendBroadCast(NET:PACKET:ICanDo, 0)
  end
!------------------------------------------------------------------------------

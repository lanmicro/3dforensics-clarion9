!ABCIncludeFile(pdlu)

  OMIT('_EndOfInclude_',_pdLUPresent_)
_pdLUPresent_  EQUATE(1)

LU_NotRequired             EQUATE(1)                                 !-- Matching record not required.
LU_PopUpOnError            EQUATE(2)                                 !-- If required, pop up browse on entry error.
LU_HideButton              EQUATE(4)                                 !-- To hide lookup button
LU_ShowButton              EQUATE(8)                                 !-- To show and enable at all times
LU_DefaultBeep             EQUATE(10h)                               !-- To set beep
LU_HandBeep                EQUATE(20h)                               !--
LU_QuestionBeep            EQUATE(40h)
LU_ExclamationBeep         EQUATE(80h)
LU_AsteriskBeep            EQUATE(100h)
LU_SkipDuplicates          EQUATE(200h)                              !-- Skip records with duplicate lookup field
LU_DisableButton           EQUATE(400h)                              !-- Not used
LU_ClearOnInsert           EQUATE(800h)                              !-- Clear lookup and display fields on insert.
LU_ResetOnAccepted         EQUATE(1000h)                             !-- Reset window with each new character
LU_ResetOnNewSelection     EQUATE(2000h)                             !-- Reset window when field is completed.
LU_DisableOnEdit           EQUATE(4000h)                             !-- Disable all display/update fields when not request<>insertrecord
LU_ValidateOnAccept        EQUATE(8000h)                             !-- For ClarioNET support.
LU_FeqReadOnly             EQUATE(10000h)                            !-- <<>> 06/05/03 - Make field readonly on edit.
LU_FeqDisable              EQUATE(20000h)                            !-- <<>> 06/05/03 - Disable field on edit.
LU_IsSql                   EQUATE(40000h)                            !-- <<>> 07/13/03 - Sql Flag
LU_EvalOnReset             EQUATE(80000h)                            !-- <<>> 11/03/04 - Enable evaluation of filter on reset.
LU_InactiveInvisible       EQUATE(100000h)                           !-- <<>> 11/03/04 - Enable evaluation of filter on reset.
LU_FillIfBlank             EQUATE(200000h)                           !-- <<>> 07/07/05 - Enable fill if blank when record changing.
luPairsQT                  QUEUE,TYPE
Left                         ANY
Right                        ANY
DisableOnEdit                BYTE
IsBlank                      BYTE                                    !-- <<>> 07/17/05 - Add fill if field is blank when opened.
FillIfBlank                  BYTE                                    !-- <<>> 07/17/05 - "
                           END                                       !--

  INCLUDE('AbFile.equ'),ONCE

PDLUCT                   CLASS,MODULE('PDLU.CLW'),TYPE,LINK('PDLU.CLW',_PDLULinkMode_),DLL(_PDLUDllMode_)
!-- Private Properties
AtEnd                       BYTE,PRIVATE                             !--
CursorPos                   BYTE,PRIVATE                             !--
FilePosition                STRING(1024),PRIVATE                     !--
Initialized                 BYTE(0),PRIVATE                          !--
IsError                     BYTE(0),PRIVATE                          !--
MidWord                     BYTE(0),PRIVATE                          !--
PC                          &luPairsCT,PRIVATE
SavNameToFind               STRING(255),PRIVATE                      !--
SavRequest                  BYTE,PRIVATE                             !--
Spaces                      BYTE,PRIVATE                             !--
IsBlank                     BYTE,PRIVATE                             !-- Field is blank when window opened.
!-- Protected Propeties
AddEditFeq                  SIGNED,PROTECTED                         !-- Add/Edit Button Field Equate
ID                          ANY,PROTECTED                            !-- ID field
SavedID                     ANY,PROTECTED                            !-- ID field
InactiveInvisible           BYTE,PROTECTED                           !-- <<>> 02/20/05 pw - Added.
FillIfBlank                 BYTE,PROTECTED                           !-- Fill display fields if blank. 07/07/05
BeepChoice                  USHORT(0),PROTECTED                      !-- Entry Error Notification
ButtonFeq                   SIGNED,PROTECTED                         !-- Button equate
DisableOnEdit               BYTE,PROTECTED                           !-- <<>> 06/05/03 - Disable change of screenfield on edit
DebugOn                     BYTE,PROTECTED                           !-- Turn debuggin on.
Feq                         SIGNED,PROTECTED                         !-- Entry field equate
FeqReadOnly                 BYTE,PROTECTED                           !-- <<>> 06/05/03 - Added to make field readonly on edit.
FeqDisable                  BYTE,PROTECTED                           !-- <<>> 06/05/03 - Added to disable field on edit.
File                        &FILE,PROTECTED                          !-- Fie to use
FileIsSet                   BYTE,PROTECTED                           !--
Flags                       ULONG,PROTECTED                          !-- Bit mapped flag field
HasFocus                    BYTE,PROTECTED                           !-- True if field has focus
IDKey                       &KEY                                     !-- ID Key
IsBeep                      BYTE(0),PROTECTED                        !-- Beep if entry error
IsRequired                  BYTE(1),PROTECTED                        !-- True if entry is required
IsSql                       BYTE,PROTECTED                           !-- Sql Flag
LastEntry                   ASTRING,PROTECTED                        !-- Last lookup entry
LastLookup                  ASTRING,PROTECTED                        !-- Last Lookup
Locator                     STRING(255),PROTECTED                    !-- Current locator
LookupField                 ANY,PROTECTED                            !-- Pointer to lookup field
LookupFieldNo               SHORT,PROTECTED                          !-- Field number of lookup field (key component) <<>> 07/09/03
LookupFilter                STRING(1024),PROTECTED                   !-- Filter
LookupKey                   &KEY                                     !-- Lookup Field Key (Must be last in key)
LookupView                  &VIEW,PROTECTED                          !-- View
LookupOrder                 CSTRING(1024),PROTECTED                  !-- Order property
LookupSqlFilter             STRING(1024),PROTECTED                   !-- Sql Filter
LookupSqlOrder              CSTRING(1024),PROTECTED                  !-- Sql Order
Opened                      BYTE,PROTECTED                           !-- Flag that window has been opened
PopUpOnError                BYTE(0),PROTECTED                        !-- Popup browse on entry error
Request                     ANY,PROTECTED                            !-- Request
ScreenField                 ANY,PROTECTED                            !-- Pointer to entry field
StuffSysId                  BYTE(1),PROTECTED                        !-- True if ID is to be saved
!-- Private Methods
AfterCurrent                PROCEDURE,PRIVATE
BeforeCurrent               PROCEDURE,PRIVATE
!-- Public Methods
Init                        PROCEDURE(? pRequest,<*?  pScreenField>,*? pLookup,VIEW P_LookupView,FILE pLookupFile,KEY pLookupKey,<*? pIDToLookup>,<*? pIDToSave>,<KEY pIDKey>,<STRING pFilter>,<ULONG pFlags>,SIGNED pButtonFEQ=0,SIGNED pFeq) !,FINAL
InitServer                  PROCEDURE(? pRequest,*? pLookup,VIEW pLookupView,KEY pLookupKey,SIGNED pFeq)
InitID                      PROCEDURE(*? pIDToLookup,*? pIDToSave,KEY pIDKey)
Construct                   PROCEDURE
Destruct                    PROCEDURE
Kill                        PROCEDURE !,EXTENDS
Version                     PROCEDURE,STRING !,EXTENDS
AddDisplay                  PROCEDURE(*? pLookup,<*? pDisplay>,BYTE pDisableOnEdit=0,BYTE pFillIfBlank=0) !,EXTENDS
!-- Public Virtual Methods
AssignRecord                PROCEDURE(BYTE pClearAll=0),VIRTUAL !,PRIVATE
AfterLookup                 PROCEDURE,VIRTUAL
BeforeLookup                PROCEDURE,VIRTUAL
CallLookup                  PROCEDURE,BYTE,VIRTUAL
DebugOut                    PROCEDURE(STRING pS,STRING pHdr,<STRING pData>),PROTECTED,VIRTUAL
EvaluateID                  PROCEDURE,VIRTUAL                        !-- <<>> 07/13/03 - Add
HideButton                  PROCEDURE,VIRTUAL
KeyToOrder                  PROCEDURE,VIRTUAL                        !-- <<>> 07/14/03 - Convert lookup key to order.
Next                        PROCEDURE,BYTE,VIRTUAL                   !-- <<>> 04/02/05 - Added
Open                        PROCEDURE,VIRTUAL
Previous                    PROCEDURE,BYTE,VIRTUAL                   !-- <<>> 04/02/05 - Added
!PositionChanged             PROCEDURE,BYTE,VIRTUAL                  !-- <<>> 4/30/03 pw - vcr
PrimeRecord                 PROCEDURE,VIRTUAL
Reset                       PROCEDURE(BYTE pForce=0),VIRTUAL
Select                      PROCEDURE(SIGNED pFeq=0,SIGNED p1=0,SIGNED p2=0),VIRTUAL
SetAlerts                   PROCEDURE,VIRTUAL
SetFile                     PROCEDURE,VIRTUAL
SetFilter                   PROCEDURE(STRING pFilter),VIRTUAL
SetFilter                   PROCEDURE(STRING pFilter,STRING pSqlFilter),VIRTUAL
SetOrder                    PROCEDURE,VIRTUAL
SetSqlOrder                 PROCEDURE,VIRTUAL                        !-- <<>> 07/14/03 - Add SQL prop:order
SetSqlOrder                 PROCEDURE(STRING pSqlOrder),VIRTUAL      !-- <<>> 07/14/03 - "
SetFlags                    PROCEDURE(ULONG pFlags=0),VIRTUAL
SetSelects                  PROCEDURE,VIRTUAL
TakeAccepted                PROCEDURE,BYTE,PROC,VIRTUAL
TakeBtnAccepted             PROCEDURE,VIRTUAL
TakeBtnFieldEvent           PROCEDURE,BYTE,PROC,VIRTUAL
TakeCharacter               PROCEDURE,VIRTUAL
TakeClarioNETAccepted       PROCEDURE,BYTE,PROC,VIRTUAL
TakeDownKey                 PROCEDURE,VIRTUAL
TakeEntryError              PROCEDURE,VIRTUAL
TakeEvent                   PROCEDURE,BYTE,PROC,VIRTUAL
TakeFieldEvent              PROCEDURE,BYTE,PROC,VIRTUAL
TakeKey                     PROCEDURE,BYTE,VIRTUAL
TakeMouseLeft               PROCEDURE,VIRTUAL
TakeMouseLeft2              PROCEDURE,VIRTUAL
TakeNewSelection            PROCEDURE,BYTE,PROC,VIRTUAL
TakeSelected                PROCEDURE,BYTE,PROC,VIRTUAL
TakeUpKey                   PROCEDURE,VIRTUAL
ValidateRecord              PROCEDURE,BYTE,VIRTUAL                   !-- <<>> 04/02/05 - Added
!-- Virtual Methods - To work with ABC Library
FileTryFetch                PROCEDURE,BYTE,PROC,VIRTUAL              !-- File Manager holder
FileReset                   PROCEDURE,VIRTUAL                        !-- File Manager holder
WindowReset                 PROCEDURE,VIRTUAL                        !-- Window Manager Place Holder
WindowUpdate                PROCEDURE,VIRTUAL                        !-- Window Manager Place Holder
                          END

luPairsCT                CLASS,MODULE('PDLU.CLW'),TYPE,LINK('PDLU.CLW',_PDLULinkMode_),DLL(_PDLUDllMode_)
List                        &luPairsQT
FillIfBlank                 &BYTE,PRIVATE                            !-- <<>> 07/06/05 pw - Add
IsBlank                     &BYTE,PRIVATE                            !-- <<>> 07/06/05 pw - Add
AddPair                     PROCEDURE(*? Left,*? Right,BYTE pDisableOnEdit=0,BYTE pFillIfBlank=0),VIRTUAL
AddItem                     PROCEDURE(*? Left,BYTE pDisableOnEdit=0) ! Added to left, copy taken on right
AssignLeftToRight           PROCEDURE(BYTE pRequest)
AssignRightToLeft           PROCEDURE
ClearLeft                   PROCEDURE
ClearRight                  PROCEDURE(BYTE pRequest)
Kill                        PROCEDURE
Init                        PROCEDURE !,EXTENDS
                          END

_EndOfInclude_
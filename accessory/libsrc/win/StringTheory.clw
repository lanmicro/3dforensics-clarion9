    Member
        Map
            SortCaseSensitive(*LinesGroupType p1,*LinesGroupType p2),Long
            SortLength(*LinesGroupType p1,*LinesGroupType p2),Long
            SortCaseInsensitive(*LinesGroupType p1,*LinesGroupType p2),Long

            Module ('')
                ToUpper (byte char), byte, name('Cla$isftoupper')     ! ToUpper is far more efficient for single character conversion
                MemCmp(ulong buf1, ulong buf2, unsigned count), long, name('_memcmp')     ! GCR
                MemChr(ulong buf, long c, unsigned count), long, name('_memchr')      ! GCR
            end

            MODULE('Zlib')
              stDeflateInit2_(ulong pStream, long pLevel, long pMethod, long pWindowBits, long pMemLevel, long pStrategy, long pVersion, long pStreamSize ),long,Pascal,raw,dll(_fp_)
              stDeflate(ulong pStream, long pFlush ),long,Pascal,raw,dll(_fp_)
              stDeflateEnd(ulong pStream),long,Pascal,RAW,dll(_fp_)

              stInflateInit2_(ulong pStream, Long pWindowBits,uLong pVersion, Long pSize),long,Pascal,raw,dll(_fp_)
              stInflate(ulong pStream, Long pFlush),long,Pascal,RAW,dll(_fp_)
              stInflateEnd(ulong pStream),long,Pascal,RAW,dll(_fp_)
            End

            Module('WinApi')
                stGetLastError(), ulong, raw, pascal, name('GetLastError')
                stFormatMessage(ulong dwFlags, ulong lpSource, ulong dwMessageId, ulong dwLanguageId, *cstring lpBuffer, |
                                ulong nSize, ulong Arguments), ulong, raw, pascal, name('FormatMessageA')
                stCreateFile(*cstring lpFileName, long dwDesiredAccess, long dwSharedMode, long lpSecurityAttributes, |
                                long dwCreationDisposition, long dwFlagsAndAttributes, long hTemplateFile), long, raw, pascal, name('CreateFileA')
                stReadFile(long hFile, *string lpBuffer, long nNumberOfBytesToRead, *long lpNumberOfBytesRead, long lpOverlapped), raw, long, pascal, name('ReadFile')
                stCloseHandle(long hObject),long, proc, pascal, name('CloseHandle')
                stWriteFile(long hFile, *string lpBuffer, long nNumberOfBytesToWrite, *long lpNumberOfBytesWritten, |
                               long lpOverlapped), long, proc, raw, pascal, name('WriteFile')
                stGetFileSize(long hFile,*long SizeHigh), long, pascal, name('GetFileSize')
                stSetFilePointer(long hFile,Long lDistanceToMove,*Long lpDistanceToMoveHigh,long dwMoveMethod), |
                               long, raw, pascal,proc, name('SetFilePointer')
                stFlushFileBuffers(long hFile), long, pascal, name('FlushFileBuffers')
                stDeleteFile(*cstring lpFileName), long, proc, raw, pascal, name('DeleteFileA')

                stSetErrorMode(ulong nMode), long, proc, raw, pascal, name('SetErrorMode')
                stOutputDebugString(*cstring msg), raw, pascal, name('OutputDebugStringA')
                stWideCharToMultiByte(ulong CodePage, long dwFlags, *string lpWideCharStr, long cchWideChar, *string lpMultiByteStr, long cbMultiByte, long lpDefaultChar=0, long lpUsedDefaultChar=0), long, raw, pascal, name('WideCharToMultiByte')
                stMultiByteToWideChar(ulong CodePage, unsigned dwFlags, *string lpMultiByteStr, long cbMultiByte, *string lpWideCharStr, long cchWideChar), long, raw, pascal, name('MultiByteToWideChar')

                !--- Runtime Dll loading
                stLoadLibrary(*cstring lpLibFileName), long, pascal, raw, name('LoadLibraryA')
                stGetProcAddress(ulong hModule, *cstring lpProcName), ulong, pascal, raw, name('GetProcAddress')
            end

          ! External C Functions (MD5)
          Compile ('****',MD5=1)
            Module('MD5.c')
                stMD5Init(Long Context),LONG, RAW, NAME('_MD5Init')
                stMD5Update(LONG Context, *STRING pInput, LONG pLength),LONG, RAW, NAME('_MD5Update')
                stMD5Final(*CSTRING Digest, LONG Context),LONG, RAW, NAME('_MD5Final')
            end
          ****
        end

! runtime loaded DLL functions
hZlib             unsigned
fp_DeflateInit2_  unsigned,name('stDeflateInit2_')
fp_Deflate        unsigned,name('stDeflate')
fp_DeflateEnd     unsigned,name('stDeflateEnd')
fp_InflateInit2_  unsigned,name('stInflateInit2_')
fp_Inflate        unsigned,name('stInflate')
fp_InflateEnd     unsigned,name('stInflateEnd')

!----------------------------------------------------------!
!    Generic string handling class method declarations.    !
!                                                          !
!   CapeSoft StringTheory class is Copyright � 2011        !
!   CapeSoft Software                                      !
!   Based on a StringClass by Rick Martin as published in  !
!   www.clarionmag.com.
!----------------------------------------------------------!

  include('StringTheory.inc'), once


SortCaseSensitive          Procedure(*LinesGroupType p1,*LinesGroupType p2)
  code
  If p1.line = p2.line then return  0
  Elsif p1.line > p2.line then return  1
  Else return -1
  End

SortLength          Procedure(*LinesGroupType p1,*LinesGroupType p2)
  code
  If len(clip(p1.line)) = len(clip(p2.line)) then return  0
  Elsif len(clip(p1.line)) > len(clip(p2.line)) then return  1
  Else return -1
  End

SortCaseInsensitive        Procedure(*LinesGroupType p1,*LinesGroupType p2)
d  cstring(255)
  code
  If Upper(p1.line) = Upper(p2.line) then return 0
  Elsif Upper(p1.line) > Upper(p2.line) then return  1
  Else return -1
  End

!!! <summary>Assign a new value to the string</summary>
!!! <param name="newValue">The new string to assign to the class</param>
!!! <remarks>A new value can be assigned to the class regardless
!!! if it already has a value. The old value is automatically disposed.</remarks>
StringTheory.SetValue Procedure(string newValue, long pClip = false)
strLen  long,auto
  code
    if pClip
        strLen = Len(clip(newValue))
    else
        strLen = Len(newValue)
    end
    if strLen = 0
        self.Free(false)
        return
    end

    if strLen <> self.LengthA()
        self.Free(false)
        self.value &= new string(strLen)
    end
    self.value = newValue

    self.base64 = 0


!!! <summary>Assign simply provides a different name for SetValue</summary>
StringTheory.Assign Procedure(*string newValue)
  code
    self.SetValue(newValue)


!!! <summary>version of SetValue that take a pointer to a string rather
!!! than  the value. Functionally equivilent, but somewhat more efficient
!!! especially for large strings</summary>
StringTheory.SetValue Procedure(*string newValue, long pClip = false)
strLen  long,auto
  code
    if pClip
        strLen = Len(Clip(newValue))
    else
        strLen = Len(newValue)
    end
    if strLen = 0
        self.Free(false)
        return
    end
    if strLen <> self.LengthA()
        self.Free(false)
        self.value &= new string(strLen)
    end
    self.value = newValue

    self.base64 = 0


!!! <summary>Load the value from a BLOB into the StringTheory object</summary>
!!! <param name="blobField">The BLOB to get the data from</param>
StringTheory.FromBlob Procedure(*blob blobField)
strLen      long, auto
  code
    if blobField &= null or blobField{prop:size} = 0
        self.ErrorTrap('The passed BLOB pointer was null or the BLOB did not contain any data.', 'FromBlob')
        return False
    end
    self.Free(false)
    strLen = blobField{prop:size}
    self.value &= new string(strLen)
    self.value = blobField[0 : strLen - 1]
    self.base64 = 0
    return True


!!! <summary>Store the current string value in the passed BLOB</summary>
!!! <param name="blobField">The BLOB to store the data in</param>
StringTheory.ToBlob Procedure(*blob blobField)
strLen      long, auto
  code
    if blobField &= null
        self.ErrorTrap('The passed BLOB pointer was null', 'ToBlob')
        return 0
    end
    strLen = self.LengthA()
    blobField{prop:size} = strLen
    if strLen = 0                                           ! Jun10: Added boundary condition handling
        return 1                                            ! Empty BLOB
    elsif strLen = 1
        blobField[0] = self.value
    else
        blobField[0 : strLen -1] = self.value
    end

    return 1


!!! <summary>Append the new value to the current string</summary>
!!! <remarks>If no value already exists then the new value is assigned
!!! as if Assign had been called instead of Append.
!!! in Clarion 5.5 use Affix instead of Append</remarks>
  omit ('****', _OLD_)
   ! if you are running Clarion 6.2 or earlier, and getting a compile error here, then add _OLD_=>1 to your project settings.
StringTheory.Append Procedure(string newValue, long pClip = false, <string pSep>)
  code
  if omitted(4) ! in a method, so first parameter is implied Self.
    self.AppendA(newValue,pClip)
  else
    self.AppendA(newValue,pClip,pSep)
  end
  ****


!!! <summary>Append the new value to the current string</summary>
!!! <remarks>If no value already exists then the new value is assigned
!!! as if Assign had been called instead of Append.
!!! in Clarion 6 and later use Append instead of Affix</remarks>
StringTheory.AppendA Procedure(string newValue, long pClip = false, <string pSep>)
  code
    if band(pClip,st:NoBlanks) > 0 and newValue = ''
      return
    end
    if not self.value &= null
      if not omitted(4)
        self.SetValue(self.value & pSep & newValue,band(pClip,st:Clip))
      else
        self.SetValue(self.value & newValue,band(pClip,st:Clip))
      end
    else
        self.SetValue(newValue,band(pClip,st:Clip))
    end

StringTheory.AppendBinary Procedure(long pValue,Long pLength=4)
str  string(4),over(pValue)
  code
  case pLength
  of 1
    self.AppendA(str[1])
  of 2
    self.AppendA(str[1:2])
  of 3
    self.AppendA(str[1:3])
  of 4
    self.AppendA(str[1:4])
  end

!!! <summary>Encodes the string using base64, if the string contains a non base64 value</summary>
!!! <remarks>Length of the string is automatically adjusted to the new length</remarks>
StringTheory.Base64Encode Procedure()
encData         &string
dataLen         long, auto
encLen          long, auto
  code
    if self.base64
        self.Trace('already base 64 encoded')
        return
    end
    encLen = self.LengthA()
    dataLen = (encLen + 3 - (encLen % 3))/3 * 4               ! Apr10: Calculate the correct length, plus padding

    if not self.base64nowrap                                ! Apr10: Option for no line wrapping
        dataLen += ((dataLen / 76) * 2) + 2
    end

    encData &= new string(dataLen)
    self.Base64Encode(encData, encLen)

    Dispose(self.value)
    self.value &= encData
    !self._length = dataLen

    self.base64 = true


!!! <summary>Decodes the string using base64, if the string contains a base64 value</summary>
!!! <remarks>Length of the string is automatically adjusted to the new length</remarks>
StringTheory.Base64Decode Procedure()
encLen          long, auto
  code
    !if not self.base64                                     ! Apr10: String may have been loaded from anywhere, and hence .base64 may well not have been set.
    !    return
    !end

    encLen = self.LengthA()

    self.Base64Decode(self.value, encLen)
    self.SetLength(encLen)

    self.base64 = false


!!! <summary>Return the Base64 encoded version of the current string</summary>
!!! <remarks>The caller must pass in a string of adequate length.
!!!  Convert regular binary data (including text) to the base64 scheme.
!!!  The length of the converted data will be returned in pLen, and the Base64
!!!  data will be returned in pText
!!!</remarks>
StringTheory.Base64Encode Procedure(*string pText, *long pLen)
x        long, auto
y        long, auto
z        long, auto
a        long
b        long, auto
triplet  string(4)
bits     long, over(triplet)
table    string('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=')
  code
    if pLen > self.LengthA() or pLen < 1
        pLen = self.LengthA()
    end

    if pLen%3 = 0
      y = pLen
    else
        y = (pLen/3) * 3 + 1   ! basically y is just pushed out to the next triplet boundry.
    end

    z = 1                      ! place holder in answer string
    b = 0
    loop x = 1 to y by 3
        if x+2 <= pLen
            triplet = self.value[x+2] & self.value[x+1] & self.value[x] & '<0>'
        elsif x+1 <= pLen
            triplet = '<0>' & self.value[x+1] & self.value[x] & '<0>'
        elsif x <= pLen
            triplet = '<0>' & '<0>' & self.value[x] & '<0>'
        end

        a = bshift(bits,-18) + 1
        pText[z] = table[a]
        z += 1

        if z > size(pText) then break.

        a = band(bshift(bits,-12),111111b) + 1
        pText[z] = table[a]
        z += 1

        if z > size(pText) then break.
        if x+1 <= pLen
            a = band(bshift(bits,-6),111111b) + 1
        else
            a = 65                                    ! the end is padded out with '=' signs
        end

        pText[z] = table[a]
        z += 1
        if z > size(pText) then break.

        if x+2 <= pLen
            a = band(bits,111111b) + 1
        else
            a = 65
        end

        pText[z] = table[a]
        z += 1
        if z+1 > size(pText)
            break
        end
        b += 4
        if not self.base64NoWrap and b%76 = 0
            pText[z : z+1] = '<13,10>'
            z += 2
            if z > size(pText)
                self.ErrorTrap('The passed string was not large enough to hold the Base64 encoded data. The data has been truncated.', 'Base64Encode')
                break
            end
            b = 0
        end
    end
    pLen = z-1
    return




!!! <summary>Returns the current string after Base64 decoding it</summary>
!!! <remarks>The decoded text is return in the pText parameter, and the
!!!  pLen parameter is set to the length of the data returned. pText must be
!!!  large enough to hold the Base64 decoded data
!!!</remarks>
StringTheory.Base64Decode Procedure(*string pText, *long pLen)
triplet     string(4), auto
bits        long,over(triplet)

table       string('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=')
!                !                         !                         !         ! !
!                0                        26                        52        62 Pad
x           long, auto
a           long
b           long
z           long, auto
y           long, auto
v           byte
  code

    if pLen > Len(self.value) or pLen=0
        pLen = Len(self.value)
    end
    z = 1
    x = 1
    y = 0
    bits = 0

    loop until x > pLen
        v = val (self.value[x])
        if (v >= 65) and (v <= 90)      ! A to Z
            a = v - 65
        elsif (v >= 97) and (v <= 122)  ! a to z
            a = v - 71
        elsif (v >= 48) and (v <= 57)   ! 0 to 9
            a = v + 4
        elsif v = 43                    ! +
            a = 62
        elsif v = 47                    ! /
            a = 63
        elsif v = 61                    ! =
            b += 1
            if b > 2
                b = 2
            end
            a = 0
        else
            x += 1
            cycle
        end

        bits = Bshift(bits,6) + a
        y += 1
        if y = 4
            if z = 0 or z+2 > Len(pText)
                break
            end

            pText[z :z+2] = triplet[3] & triplet[2] & triplet[1]
            z += 3
            if z+2 > Len(pText)     ! Should never happen in reality
                self.ErrorTrap('The passed string was not large enough to hold the Base64 decoded data. The data has been truncated.', 'Base64Decode')
                break
            end
            y = 0
            bits = 0
        end
        x += 1
    end
    pLen = z-1-b
    return



!!! <summary>Return the postition of the sub string in the current string.</summary>
!!! <param name="SearchValue">Sub-string to search for</param>
!!! <param name="Step">The number of characters to jump. Default is 1</param>
!!! <param name="Start">Optional parameter to indicate what position to start search. Default is beginning.</param>
!!! <param name="End">Optional parameter to indicate what position to end search. Default is end of string.</param>
!!! <param name="NoCase">Optional parameter: Ignore case in comparision. Default is case-sensitive.</param>
!!! <remarks>If the sub-string does not exist then zero is returned.</remarks>
StringTheory.Instring Procedure(string pSearchValue, long pStep=1, long pStart=1, long pEnd=0, long pNoCase=0, long pWholeWord=0)!,long
x           long, auto
lsv         long
  code
    lsv = len(pSearchValue)
    if self.value &= null or lsv = 0  ! GCR
        return 0
    end
    loop
      if pNoCase or pStep <> 1
          x = InString(Choose(pNoCase, Upper(pSearchValue), pSearchValue), Choose(pNoCase, Upper(self.value), self.value), pStep, pStart)
          if x and pEnd and x + lsv - 1 > pEnd
              x = 0
          end
      elsif lsv = 1
          x = self.findChar(pSearchValue, pStart, pEnd)  ! use single char version
      else
          x = self.findChars(pSearchValue, pStart, pEnd)
      end
      if pWholeword = 0 or x = 0 then break.
      if x > 1
        case self.value[x-1]
        of 'A' to 'Z'
        orof 'a' to 'z'
        orof '0' to '9'
          pstart = x+1
          cycle
        end
      end
      if x + lsv <= self.lengthA()
        case self.value[x+lsv]
        of 'A' to 'Z'
        orof 'a' to 'z'
        orof '0' to '9'
          pstart = x+1
          cycle
        end
      end
      break
    end
    return x


!!! <summary>
!!!     Test "Instring" method. Uses pointers and the Hoorspool Boyer-Moore algorithm
!!!     Finds the first match of pFind in pVal. No significant performance improvement
!!!     over the Clarion builtin.
!!! </summary>
!StringTheory.Find Procedure(*string pFind, *string pVal, long pStart=1, long pEnd=0, bool noCase=False, bool clipLen=true)
!UCHAR_MAX       equate(255)
!scan            long
!bad_char_skip   long, dim(UCHAR_MAX + 1)
!
!nLen            long    ! Length of the search term
!hLen            long
!fpos            long
!skip            long
!  code
!!    self.Trace('------------- Find -----------------')
!
!    ! Check parameters
!    if clipLen
!        nLen = Len(Clip(pFind))
!        hLen = Len(Clip(pVal))
!    else
!        nLen = Len(pFind)
!        hLen = Len(pVal)
!    end
!    if nLen = 0 or hLen = 0
!        return 0
!    end
!    if pEnd = 0
!        pEnd = hLen
!    end
!    if pStart <= 0
!        pStart =  1
!    elsif pStart > pEnd
!        return 0
!    end
!
!!    self.Trace('Find: ' & Clip(pFind) & '. In: ' & Clip(pVal))
!
!    !--- Preprocess
!    ! Initialize the table to default value */
!    loop scan = 1 to UCHAR_MAX + 1
!        bad_char_skip[scan] = nlen
!    end
!
!    ! Then populate it with the analysis of the find term
!    loop scan = 1 to nLen
!!        self.trace('Search char ' & scan & ' has value: ' & Val(pFind[scan]) & ', set Skip to: ' & nlen - scan + 1)
!        bad_char_skip[Val(pFind[scan])] = nlen - scan + 1
!    end
!
!    !--- Do the matching
!!    self.Trace('--- Do match')
!    fpos = pStart
!
!    ! Search the pVal, while the needle can still be within it
!    loop while hLen >= nlen
!        ! scan from the end of the search term
!        scan = nLen
!!        self.Trace('search ' & scan & ' chars from ' & fpos+scan-1 & ' backwards')
!        loop while pVal[fpos+scan-1] = pFind[scan]
!            if scan = 1                 ! If the first byte matches, we've found it.
!                return fpos
!            end
!            scan -= 1
!        end
!
!        ! Skip some bytes and start again.
!!        self.Trace('Find skip values for ' & pVal[fPos + skip] & '(' & Val(pVal[fpos + skip]) & ')')
!        skip = bad_char_skip[Val(pVal[fPos + skip])]
!!        self.Trace('skip value: ' & skip)
!        hlen -= skip
!        fpos += skip + 1
!    end
!
!    return 0


!!! <summary>Return the part of the string between two search strings (exclusively)</summary>
!!! <param name="Left">The "left hand" sub-string to search for</param>
!!! <param name="Right">The "right hand" sub-string to search for</param>
!!! <param name="Start">Optional parameter to indicate what position to start search. Default is beginning.</param>
!!! <param name="End">Optional parameter to indicate what position to end search. Default is end of string.</param>
!!! <param name="NoCase">Optional parameter: Ignore case in comparision. Default is case-sensitive.</param>
!!! <remarks>If either sub-string does not exist then a blank string is returned.</remarks>
StringTheory.Between Procedure(string pLeft, string pRight, long pStart=1, long pEnd=0, long pNoCase=0, long pExclusive=true)
!x long,auto
!y long,auto
  code
    return self.FindBetween(pLeft, pRight, pStart, pEnd, pNoCase, pExclusive)

!    x = self.Instring(pLeft, 1, pStart, pEnd, pNoCase)
!    if not x
!        return ''
!    end
!    x = x + Len(pLeft)                                      ! Shift the starting position to after the left hand delimeter
!
!    y = self.Instring(pRight, 1, x, pEnd, pNoCase)
!    if not y
!        return ''
!    end
!    y -= 1                                                  ! Shift to before the delimeter
!
!    if x > y
!        return ''
!    end
!
!    return self.value[x : y]



! Finds the string between the passed left and right delimiter, and returns it. The passed pStart and pEnd
! are set to the start and end position of the returned value in the string. If pStart or pEnd is passed as less
! than or equal to zero then they are set to the start and end of the stored string repectively. Otherwise
! they are used as the bounds for the search. This allows FindBetween to be called multiple times to search
! for multiple occurances using the same delimiter:
! loop
!     betweenVal = st.FindBetween('[[', ']]', pStart, pEnd)
!     if betweenVal = ''
!         break
!     else
!         ! do something with the returned value
!     end
!     pEnd = 0            ! Reset pEnd to allow the next iteration to search to the end of the string
! end
StringTheory.FindBetween Procedure(string pLeft, string pRight, *long pStart, *long pEnd, bool pNoCase=false, long pExclusive=true)
x          long, auto
  code
    if pStart <= 0
        pStart = 1
    end
    if pEnd < 0
        pEnd = 0
    end

    if pLeft = ''
      pStart = 1
    else
      pStart = self.Instring(pLeft, 1, pStart, pEnd, pNoCase)
    End
    if pStart = 0
        return ''
    end
    if pExclusive = true
      pStart += Len(pLeft)                                        ! Shift the starting position to after the left hand delimeter
    end
    if pRight = ''
      pEnd = self.lengthA()
    else
      pEnd = self.Instring(pRight, 1, pStart, pEnd, pNoCase)
    end
    if not pEnd
        return ''
    end
    if pRight = ''
    elsif pExclusive = true
      pEnd -= 1                                                   ! Shift to before the delimeter
    else
      pEnd += len(pRight) - 1
    end

    if pStart > pEnd
        return ''
    end

    return self.value[pStart : pEnd]



!!! <summary>Return the part of the string after the searchstring</summary>
!!! <param name="SearchValue">The sub-string to search for</param>
!!! <param name="Start">Optional parameter to indicate what position to start search. Default is beginning.</param>
!!! <param name="End">Optional parameter to indicate what position to end search. Default is end of string.</param>
!!! <param name="NoCase">Optional parameter: Ignore case in comparision. Default is case-sensitive.</param>
!!! <remarks>If the sub-string does not exist then a blank string is returned.</remarks>
StringTheory.After Procedure(string pSearchValue, long pStart=1, long pEnd=0, long pNoCase=0)
x long,auto
  code
  x = self.Instring(pSearchValue,1,pStart,pEnd,pNoCase)
  if x and x + Len(pSearchValue) <= self.LengthA()
    return self.value[x + len(pSearchValue) : self.LengthA()]
  else
    return ''
  end


!!! <summary>Return the part of the string before the searchstring</summary>
!!! <param name="SearchValue">The sub-string to search for</param>
!!! <param name="Start">Optional parameter to indicate what position to start search. Default is beginning.</param>
!!! <param name="End">Optional parameter to indicate what position to end search. Default is end of string.</param>
!!! <param name="NoCase">Optional parameter: Ignore case in comparision. Default is case-sensitive.</param>
!!! <remarks>If the sub-string does not exist then a blank string is returned.</remarks>
StringTheory.Before Procedure(string pSearchValue, long pStart=1, long pEnd=0, long pNoCase=0)
x long,auto
  code
    x = self.Instring(pSearchValue, 1, pStart, pEnd, pNoCase)
    if x > 1
        return self.value[1 : x-1]
    else
        return ''
    end


!!! <summary>Return the file name when the full path is passed.</summary>
!!! <param name="fPath">String that contains the full path and file name</param>
!!! <remarks>Handles forward and back-slashes, as well as just a
!!! path without a file name being passed</remarks>
StringTheory.FileNameOnly Procedure(<string fPath>,Long pIncludeExtension=true)
i               long, auto
j               long, auto
pLen            long, auto
  code

    if Omitted(2)
        if self.value &= null
            return ''
        end
        return self.FileNameOnly(self.GetValue(),pIncludeExtension)
    end

    if Len(fPath) < 1 or Len(Clip(fPath)) < 1               ! Does not contain a file name
        return ''
    end

    ! Check for Windows style backslash "\"
    pLen = Len(Clip(fPath))
    if fPath[pLen] = '/' or fPath[pLen] = '\'               ! The passed string is just a path (directory) without a filename'
        return ''
    end

    loop i = pLen to 1 by -1
        if fpath[i] = '\'                                   ! found the last '\'
          if pIncludeExtension
            return(fpath[i+1 : pLen])
          else
            loop j = pLen to i+2 by -1
              if fpath[j] = '.'
                return(fpath[i+1 : j - 1])
              end
            end
            return(fpath[i+1 : pLen]) ! no extension found
          end
        end
    end

    ! Check for forward slashes
    loop i = pLen to 1 by -1
        if fpath[i] = '/'                                   ! found the last /
          if pIncludeExtension
            return(fpath[i+1 : pLen])
          else
            loop j = pLen to i+2 by -1
              if fpath[j] = '.'
                return(fpath[i+1 : j - 1])
              end
            end
            return(fpath[i+1 : pLen]) ! no extension found
          end
        end
    end

    if pIncludeExtension = false
      loop i =  pLen to 1 by -1
        if fpath[i] = '.'
          if i = 1
            return ''
          else
            return(fpath[1:i-1])
          end
        end
      end
    end

    return Clip(fPath)


!!! <summary>Return just the file extension when passed the full file name and path.</summary>
!!! <param name="fPath">String that contains the full path and file name</param>
StringTheory.ExtensionOnly Procedure(<string fPath>)
i               long, auto
pLen            long, auto
  code
  if Omitted(2)
    return self.ExtensionOnly(self.GetValue())
  end
  pLen = Len(Clip(fPath))
  if pLen = 0 or fpath[pLen] = '/' or fpath[pLen] = '\' or fpath[pLen] = '.'
   return ''
  end
  loop i = pLen-1 to 1 by -1
    if fpath[i] = '.'
      return fpath[i+1 : pLen]
    end
    if fpath[i] = '/' or fpath[i] = '\'
      break
    end
  end
  return ''

!!! <summary>Return just the path when passed the full file name and path.</summary>
!!! <param name="fPath">String that contains the full path and file name</param>
!!! <remarks>Handles forward as well as back-slashes. If there
!!! is no path a blank string is returned</remarks>
! Jun10: Changed string slice to sub to handle the case where the path
!  contains only a single character (a forward or back slash)
StringTheory.PathOnly Procedure(<string fPath>)
i               long, auto
pLen            long, auto
  code
    if Omitted(2)
        return self.PathOnly(self.GetValue())
    end

    ! Check for Windows style backslash "\"
    pLen = Len(Clip(fPath))
    loop i = pLen to 1 by -1
        if fpath[i] = '\'                                   ! found the last '\'
            return(Sub(fPath, 1, i-1))
        end
    end

    ! Check for forward slashes
    loop i = pLen to 1 by -1
        if fpath[i] = '/'                                   ! found the last /
            return(Sub(fPath, 1, i-1))
        end
    end

    return ''                                               ! No path


!!! <summary>Prepend the passed string to the current stored string.</summary>
!!! <param name="newValue">The new value for the string</param>
!!! <remarks>If no value already exists then the new value is assigned
!!! as if Assign had been called instead of Append.</remarks>
StringTheory.Prepend Procedure(string newValue, long pClip = false,<String pSep>)
  code
    if band(pClip,st:NoBlanks) > 0 and newValue = ''
      return
    end
    if not self.value &= null
      If not omitted(4)
        self.SetValue(clip(newValue) & pSep & self.value,band(pClip,st:Clip))
      else
        self.SetValue(clip(newValue) & self.value,band(pClip,st:Clip))
      end
    else
        self.SetValue(newValue,band(pClip,st:Clip))
    end


!!! <summary>Allocate dynamic memory when class goes into of scope.</summary>
StringTheory.Construct Procedure()
  code
  self.Lines &= new LinesQType

!!! <summary>Deallocate dynamic memory when class goes out of scope.</summary>
StringTheory.Destruct Procedure()
  code
    self.Free(false)
    self.FreeLines()
    if not self.lines &= null
        Dispose(self.Lines)
    end



!!! <summary>Return current string</summary>
!!! <remarks>Deprecated. Use GetValue instead</remarks>
StringTheory.GetVal Procedure() !,STRING
  code
  return self.GetValue()


!!! <summary>Return current string</summary>
!!! <remarks>If no string has been assigned an empty string is returned.</remarks>
StringTheory.GetValue Procedure() !,STRING
  code
    if not self.value &= null
        return self.value
    else
        return ''
    end

StringTheory.GetValue Procedure(long maxLen) !,STRING
  code
    if not self.value &= null
        if Len(self.value) > maxLen
            return self.value[1 : maxLen]
        end
        return self.value
    else
        return ''
    end


StringTheory.LengthA Procedure() !,long
  code
    if not self.value &= null
        return Len(self.value)
    else
        return 0
    end


  compile ('****', _C60_)
!!! <summary>Return the length of the existing string value.</summary>
!!! <remarks>If no string has been assigned zero is returned.</remarks>
StringTheory.Length Procedure() !,long
  code
    return self.LengthA()
  ****

StringTheory.Len Procedure() !,long
  code
    return self.LengthA()


!!! <summary>Adjusts the space for the string to the specified length</summary>
!!! <remarks>Used internally when more, or less, space is required.
!!! If the length is shorter than the existing length then the string is truncated.</remarks>
StringTheory.SetLength  Procedure(Long NewLength)
oldString   &string
oldLength   long
  code
    if newlength = self.LengthA()
        return
    elsif newLength < 1
        self.Free(false)
        return
    end

    if not self.value &= NULL
        oldLength = len(self.value)
        oldString &= self.value
        self.value &= Null
    end
    self.value &= new string(newLength)

    if oldLength = 0
      self.value = ''
    else
      self.value = oldString
    end
    Dispose(oldString)
    return

!!! <summary>Replace occurences of one string with another in class value.</summary>
!!! <param name="OldValue">Sub-string to search for</param>
!!! <param name="NewValue">New value to replace with</param>
!!! <param name="Count">Optional parameter: How many occurences to replace. Default is all.</param>
!!! <remarks>This operation is non-overlapping. If the OldValue occurs in the NewValue the
!!! occurences from inserting NewValue will not be replaced.</remarks>
StringTheory.Replace Procedure(string pOldValue, string pNewValue, long pCount=0, long pStart=1, long pEnd=0, long pNoCase=0, bool pRecursive=false)
lCount              long
lStrPos             long, auto
lOld                long,auto
lNew                long,auto
lValue              long,auto
lDiff               long,auto
xPos                long,auto
nCount              long,auto
newstring           &String
  code
  if self.value &= null or Len(pOldValue) <= 0
    return 0
  end
  if pStart < 1
    pStart = 1
  end
  if pStart > Len(self.value)
    return 0
  elsif pCount < 0
    return 0
  elsif pRecursive and pCount <> 0
    if (Instring(pOldValue, pNewValue, 1, 1) and pNoCase = 0)  or | ! The old value is a substring of the new value, which will cause an infinite loop when pRecursive is set
       (Instring(upper(pOldValue), upper(pNewValue), 1, 1) and pNoCase)
      self.ErrorTrap('Invalid recursive replacement - the value being replaced is a substring of the new value. This will result in an infinite loop', 'Replace')
      self.trace('Invalid recursive replacement - the value being replaced is a substring of the new value. This will result in an infinite loop')
      return 0
    end
  end
  if pNoCase
    pOldValue = Upper(pOldValue)
  end
  if pEnd <= 0 or pEnd > Len(self.value)
    pEnd = Len(self.value)
  end
  lOld = Len(pOldValue)
  lNew = Len(pNewValue)
  if lOld = lNew                                                                                                      ! replace with string of same size
    if pNoCase
      pOldValue = upper(pOldValue)
      loop lStrPos = pStart to pEnd - lOld + 1
        if Upper(self.value[lStrPos : lStrPos + lOld - 1]) = pOldValue
          self.value[lStrPos : lStrPos + lNew - 1] = pNewValue
          lCount += 1
          if pCount <> 0 and lCount >= pCount then break.
        end
      end
    else
      loop lStrPos = pStart to pEnd - lOld + 1
        if self.value[lStrPos : lStrPos + lOld - 1] = pOldValue
          self.value[lStrPos : lStrPos + lNew - 1] = pNewValue
          lCount += 1
          if pCount <> 0 and lCount >= pCount then break.
        end
      end
    end
  else
    lDiff = lNew - lOld
    loop ! it's possible the replacement could create new instances, so we need to re-check in a loop.
      nCount = self.Count(pOldValue,1,pStart,pEnd,pNoCase,false,false)
      if nCount = 0 then break.
        lValue = self.LengthA()
        if nCount > pCount and pCount <> 0 then nCount = pCount.
        newstring &= new(string(lValue + lDiff * nCount))
        if pStart > 1
          newstring[1 : pStart-1] = self.value[1 : pStart-1]
          xpos = pStart
        else
          xPos = 1
        end
        if pNoCase
          pOldValue = upper(pOldValue)
          loop lStrPos = pStart to pEnd - lOld + 1
            if upper(self.value[lStrPos : lStrPos + lOld - 1]) = pOldValue
              if lNew
                newstring[xPos : xpos + lNew - 1] = pNewValue
                xPos += lNew
              end
              lStrPos += lOld - 1
              lCount += 1
              if pCount <> 0 and lCount >= pCount
                lStrPos += 1
                break
              end
            else
              newstring[xPos] = self.value[lStrPos]
              xPos += 1
            end
          end
        else
          loop lStrPos = pStart to pEnd - lOld + 1
            if self.value[lStrPos : lStrPos + lOld -1] = pOldValue
              if lNew
                newstring[xPos : xpos + lNew - 1] = pNewValue
                xPos += lNew
              end
              lStrPos += lOld - 1
              lCount += 1
              if pCount <> 0 and lCount >= pCount
                lStrPos += 1
                break
              end
            else
              newstring[xPos] = self.value[lStrPos]
              xPos += 1
            end
          end

        end
        if lStrPos-1 <> lValue
          newstring[xPos : xPos + (lValue - lStrPos)] = self.value[lStrPos : lValue]
        end
        dispose(self.value)
        self.value &= newstring
        if pEnd <= 0 or pEnd > Len(self.value)
          pEnd = Len(self.value)
        end
        if (pCount <> 0 and lCount >= pCount) or pRecursive = 0 or lCount = 0 then break.
      end
    end
    return lCount

!!! <summary>Count the occurences of a sub-string in class value.</summary>
!!! <param name="SearchValue">Sub-string to search for</param>
!!! <param name="Step">The number of characters to jump. Default is 1</param>
!!! <param name="Start">Optional parameter to indicate what position to start search. Default is beginning.</param>
!!! <param name="End">Optional parameter to indicate what position to end search. Default is end of string.</param>
!!! <param name="NoCase">Optional parameter: Ignore case in comparision. Default is case-sensitive.</param>
StringTheory.Count Procedure(string pSearchValue, long pStep=1, long pStart=1, long pEnd=0, long pNoCase=0, bool softClip=true, long pOverlap=true) !,long
px          long, auto
ans         long
clipPos     long
  code
  if self.value &= null
    return 0
  end
  if pNoCase
    pSearchValue = Upper(pSearchValue)
  end
  if not softClip                                         ! "soft" clipping allows the string to "overlap" the end position
    clipPos = Len(pSearchValue) - 1
  end
  loop
    px = InString(pSearchValue, Choose(pNoCase, Upper(self.value), self.value), pStep, pStart)
    if pEnd and px + clipPos > pEnd                     ! Jul11: Added support for "soft" clipping
      break
    end
    if px = 0
      break
    end
    if pOverlap = true
      pStart = px + 1
    else
      pStart = px + Len(pSearchValue)
    end
    ans += 1
  end
  return ans

!!! <summary>Searches the lines for a specific substring.</summary>
!!! <remarks>returns the line number containing the Search Value.</remarks>
StringTheory.InLine   Procedure (string pSearchValue, long pStep=1, long pStart=1, long pEnd=0, long pNocase=0, long pWholeLine=0)
x   long,auto
ans long
  code
  if pEnd = 0 then pEnd = self.records().
  if pEnd < pStart then pEnd = pStart.
  if pNoCase then pSearchValue = upper(pSearchValue).
  loop x = pStart to pEnd by pStep
    if pWholeLine
      if pNocase
        if upper(self.GetLine(x)) = pSearchValue
          ans = x
          break
        end
      else
        if self.GetLine(x) = pSearchValue
          ans = x
          break
        end
      end
    else
      if pNocase
        if instring(pSearchValue,upper(self.GetLine(x)),1,1)
          ans = x
          break
        end
      else
        if instring(pSearchValue,self.GetLine(x),1,1)
          ans = x
          break
        end
      end
    end
  end
  return ans

!!! <summary>Return specific line after calling Split method.</summary>
!!! <param name="LineNumber">Line to return. If LineNumber is greater than the number of lines in queue
!!! then an empty string is returned.</param>
!!! <remarks>If split has not been called an empty string is returned.</remarks>
StringTheory.GetLine Procedure(long pLineNumber) !,STRING
  code
  if self.Lines &= null
    return ''
  else
    Get(self.Lines,pLineNumber)
    if ErrorCode()
      return ''
    else
      return self.Lines.line
    end
  end

!!! <summary>Sets a specific line to a specific value</summary>
!!! <param name="LineNumber">Line to set. If LineNumber is greater than the number of lines in queue
!!! then the string is added to the queue at the next available line.</param>
!!! <remarks></remarks>
StringTheory.SetLine Procedure(long pLineNumber,String pValue)
  code
    if self.Lines &= null
        return
    else
        Get(self.Lines,pLineNumber)
        if ErrorCode()
            self.Lines.line &= new string(len(pValue))
            self.Lines.line = pValue
            add(self.Lines,pLineNumber)
        else
            if not self.Lines.Line &= NULL
                Dispose(self.Lines.Line)
            end
            self.Lines.line &= new string(len(pValue))
            self.Lines.line = pValue
            put(self.lines)
        end
    end

!!! <summary>Adds a specific line to the queue at a specific position</summary>
!!! <param name="LineNumber">Line to add. If LineNumber is greater than the number of lines in queue
!!! then the string is added to the queue at the next available line.</param>
!!! <remarks></remarks>
StringTheory.AddLine Procedure(long pLineNumber,String pValue)
  code
    if self.Lines &= null
        return
    else
        self.Lines.line &= new string(len(pValue))
        self.Lines.line = pValue
        add(self.Lines,pLineNumber)
    end

!!! <summary>Delete a specific line after calling Split method.</summary>
!!! <param name="LineNumber">Line to delete. If LineNumber is greater than the number of lines in queue
!!! then nothing is deleted.</param>
!!! <remarks>Returns 1 if nothing is deleted, 0 if the line is deleted</remarks>
StringTheory.DeleteLine Procedure(long pLineNumber) !,LONG
  code
    if self.Lines &= null
        return 1
    else
        Get(self.Lines,pLineNumber)
        if ErrorCode() = 0
            if not self.Lines.Line &= NULL
                Dispose(self.Lines.Line)
            end
            Delete(self.Lines)
            return 0
        end
    end
    return 1

!!! <summary>Remove "empty" lines from the Lines Queue.</summary>
!!! <remarks>Allows you to define the characters that constitue "emptyness".
!!!          Returns the number of lines deleted.</remarks>
StringTheory.RemoveLines  Procedure(<String pAlphabet>)
ans           long
x             long,auto
  code
  loop x = self.records() to 1 by -1
    if self.getline(x) = ''
      self.deleteline(x)
      ans += 1
    elsif not omitted(2) and self.IsAll(' ' & pAlphabet,self.getline(x))
      self.deleteline(x)
      ans += 1
    end
  end
  return ans

!!! <summary>Return the number of lines a string value was broken into after calling Split.</summary>
!!! <remarks>If split has not been called zero is returned.</remarks>
StringTheory.Records Procedure() !,long
  code
    if self.Lines &= null
        return 0
    else
        return Records(self.Lines)
    end


!!! <summary>Breakdown the current string value into a series of string. Use the passed string value
!!! as a delimiter.</summary>
!!! <param name="SplitStr">Sub-String used to break up string. </param>
!!! <remarks>The sub-string is consumed by the command and does not appear in the lines.
!!! Use Records and GetLine methods to return information about the split queue.</remarks>
StringTheory.Split Procedure(string pSplitStr, <string pQuotestart>, <string pQuoteEnd>, bool removeQuotes = false, bool pClip = false, bool pLeft=false, <string pSeparator>,Long pNested=false)
splitStrPos         long, auto
startPos            long(1)
QuoteStart          String(255)
QuoteEnd            String(255)
QuoteStartLen       Long
QuoteEndLen         Long
qStart              StringTheory
qEnd                StringTheory
Nest                Long
ix                  long, auto
slen                long, auto
vlen                long, auto
QuotesExist         long
MultiQuotes         long
start               long
state               long
LOOKINGFORBOUNDARY  equate(1)
LOOKINGFORENDQUOTE  equate(2)

pn                  equate('StringTheory.Split')
sPos                long
ePos                long
  code
  self.freeLines()
  if self.value &= null
    return
  end
  if not omitted(3) and pQuoteStart <> ''
    qStart.SetValue(pQuoteStart,st:clip)
    if omitted(4) or pQuoteEnd = ''
      qEnd.SetValue(pQuoteStart,st:clip)
    else
      qEnd.SetValue(pQuoteEnd)
    end
    QuotesExist = true
    If not omitted(8) and pSeparator <> '' and Instring(pSeparator,pQuoteStart,1,1)
      qStart.Split(pSeparator)
      qEnd.Split(pSeparator)
      if qStart.Records() > 1
        MultiQuotes = true
      Else
        QuoteStart = qStart.GetValue()
        QuoteEnd = qEnd.GetValue()
        QuoteStartLen = qStart.LengthA()
        QuoteEndLen   = qEnd.LengthA()
      End
    Else
      QuoteStart = qStart.GetValue()
      Quoteend = qEnd.GetValue()
      QuoteStartLen = qStart.LengthA()
      QuoteEndLen   = qEnd.LengthA()
    End
  End
  slen = Len(pSplitStr) - 1
  vLen = self.LengthA()
  if QuotesExist
    startPos = 1
    state = LOOKINGFORBOUNDARY
    loop splitStrPos = 1 to vLen
      case state
        of LOOKINGFORBOUNDARY
          if splitStrPos + sLen <= vLen and self.value[splitStrPos : splitStrPos + sLen] = pSplitStr
            do AddLine
            splitStrPos += sLen
            startPos = splitStrPos + 1
          elsif multiQuotes
            loop ix = 1 to qStart.records()
              QuoteStart = qStart.GetLine(ix)
              QuoteStartLen = len(clip(QuoteStart))
              if QuoteStartLen and splitStrPos + QuoteStartLen <= vLen and self.value[splitStrPos : splitStrPos + QuoteStartLen - 1] = QuoteStart
                state = LOOKINGFORENDQUOTE
                splitStrPos = splitStrPos + QuoteStartLen - 1
                QuoteEnd = qEnd.GetLine(ix)
                QuoteEndLen = len(clip(QuoteEnd))
                break
              End
            End
          elsif splitStrPos + QuoteStartLen - 1 <= vLen and self.value[splitStrPos : splitStrPos + QuoteStartLen - 1] = QuoteStart
            state = LOOKINGFORENDQUOTE
            splitStrPos = splitStrPos + QuoteStartLen - 1 !2
          end
        of LOOKINGFORENDQUOTE
          if pNested and QuoteStart <> QuoteEnd and splitStrPos + QuoteStartLen - 1 <= vLen and self.value[splitStrPos : splitStrPos + QuoteStartLen - 1] = QuoteStart
            nest += 1
          elsif splitStrPos + QuoteEndLen <= vLen and self.value[splitStrPos : splitStrPos + QuoteEndLen - 1] = QuoteEnd
            If pNested and nest > 0
              nest -= 1
            Else
              state = LOOKINGFORBOUNDARY
              splitStrPos = splitStrPos + QuoteEndLen - 1 !2
            End
          end
        end
      end
      if splitStrPos > StartPos
        do AddLine
      end
    else
      loop
        splitStrPos = self.findChars(pSplitStr, startPos)
        if splitStrPos
          do AddLine
          StartPos = SplitStrPos + Len(pSplitStr)
          if startPos > vLen then break.
        else
          if pClip and pLeft
            self.Lines.line &= new string(len(clip(left(self.value[startPos : vLen]))))
            self.Lines.line = clip(left(self.value[startPos : vLen]))
          elsif pClip
            self.Lines.line &= new string(len(clip(self.value[startPos : vLen])))
            self.Lines.line = clip(self.value[startPos : vLen])
          elsif pLeft
            self.Lines.line &= new string(len(left(self.value[startPos : vLen])))
            self.Lines.line = left(self.value[startPos : vLen])
          else
            self.Lines.line &= new string(vLen - StartPos + 1)
            self.Lines.line = self.value[startPos : vLen]
          end
          Add(self.Lines)
          break
        end
      end
    end

AddLine Routine
  sPos = startPos
  ePos = splitStrPos
  if removeQuotes and QuotesExist
  ! only quotes at the start and end of the line are removed.
    if Sub(self.value, sPos, QuoteStartLen) = quoteStart then sPos += QuoteStartLen. !+1.
    if Sub(self.value, ePos - QuoteEndLen, QuoteEndLen) = quoteEnd then ePos -= QuoteEndLen.
  end

  if ePos - sPos <= 0                                     ! Empty string, so add a single character entry that contains a space
    self.Lines.line &= new string(1)
    Clear(self.Lines.line)
  else
    if pClip and pLeft
      self.Lines.line &= new string(len(clip(left(self.value[sPos : ePos-1]))))
      self.Lines.line = clip(left(self.value[sPos : ePos-1]))
    elsif pClip
      self.Lines.line &= new string(len(clip(self.value[sPos : ePos-1])))
      self.Lines.line = clip(self.value[sPos : ePos-1])
    elsif pLeft
      self.Lines.line &= new string(len(left(self.value[sPos : ePos-1])))
      self.Lines.line = left(self.value[sPos : ePos-1])
    Else
      self.Lines.line &= new string(ePos - sPos)
      self.Lines.line = self.value[sPos : ePos-1]
    End
  end
  Add(self.Lines)

!!! <summary>Splits the string in numChars lengths and stores each new substring
!!!     as a line in the self.lines queue
!!! </summary>
!!! <param name="numChars">The number of characters that each new substring should contain, i.e.
!!!     the length to split the string up using</param>
!!! <remarks>If the string is shorter than numChars then a single "line" entry is added which contains
!!!     the string. If the string is not an exact multiple of numChars then the last substring split
!!!     contains however many characters remain</remarks>
StringTheory.SplitEvery Procedure(long numChars)
sPos            long
ePos            long
  code
    self.freeLines()
    if self.value &= null or numChars < 1
        return
    end

    sPos = 1

    loop
        ePos = sPos + numChars - 1
        if ePos >= self.Len()                                ! The last section, which may be shorter than numChars
            ePos = self.Len()
            self.lines.line &= new string(ePos - sPos + 1)
            self.lines.line = self.value[sPos : ePos]
            Add(self.lines)
            break
        else                                                ! Add the next numChars substring
            self.lines.line &= new string(numChars)
            self.lines.line = self.value[sPos : ePos]
            Add(self.lines)
        end
        sPos = ePos + 1
    end

    omit ('****', _OLD_)
     ! if you are running Clarion 6.2 or earlier, and getting a compile error here, then add _OLD_=>1 to your project settings.
StringTheory.Sort Procedure (Long pSortType,string pSplitStr,<string pQuotestart>,<string pQuoteEnd>, bool pClip = false, bool pLeft=false)
  code
  self.split(pSplitStr,pQuotestart,pQuoteEnd,true,pClip,pLeft)
  self.sort(pSortType)
  self.join(pSplitStr,pQuotestart,pQuoteEnd)
  self.freelines()

StringTheory.Sort Procedure(Long pSortType)
  code
  case pSortType
  of st:SortNoCase
    sort(self.lines,SortCaseInsensitive)
  of st:SortCase
    sort(self.lines,SortCaseSensitive)
  of st:SortLength
    sort(self.lines,SortLength)
  end
      ****

StringTheory.Join Procedure(string pBoundary,<string pQuotestart>,<string pQuoteEnd>)
QuoteStart            string(1)
QuoteEnd              string(1)
q                     long
px                    long,auto
e                     long,auto
  code
    self.Free(false)
    if self.Lines &= null
        return
    end
    if not omitted(3)
        quoteStart = pQuoteStart
        if not omitted(4) and pQuoteEnd <> ''
              quoteEnd = pQuoteEnd
        else
              quoteEnd = quoteStart
        end
        if quoteStart <> ''
              q = 1
        end
    end

    loop px = 1 to Records(self.lines)
        Get(self.Lines, px)
        if self.lines.line &= null
            cycle
        end
        if q
            if instring(pBoundary,self.Lines.line,1,1) > 0
                e = len(self.Lines.line)
                if e > 0
                  if self.Lines.line[1] = quoteStart and self.Lines.line[e] = quoteEnd
                    if px = 1
                      self.AppendA(self.Lines.line)
                    else
                      self.AppendA(pBoundary & self.Lines.line )
                    end
                  elsif self.Lines.line[1] = quoteStart
                    if px = 1
                      self.AppendA(self.Lines.line & quoteend)
                    else
                      self.AppendA(pBoundary & self.Lines.line & quoteend)
                    end
                  elsif self.Lines.line[e] = quoteEnd
                    if px = 1
                      self.AppendA(quoteStart & self.Lines.line)
                    else
                      self.AppendA(pBoundary & quoteStart & self.Lines.line)
                    end
                  else
                    if px = 1
                      self.AppendA(quoteStart & self.Lines.line & quoteEnd)
                    else
                      self.AppendA(pBoundary & quoteStart & self.Lines.line & quoteEnd)
                    end
                  end
                end
            else
              if px = 1
                self.AppendA(self.Lines.line)
              else
                self.AppendA(pBoundary & self.Lines.line)
              end
            end
        else
          if px = 1
            self.AppendA(self.Lines.line)
          else
            self.AppendA(pBoundary & self.Lines.line)
          end
        end
    end

!!! <summary>Filter the current Lines Queue</summary>
!!! <param name="Include">Only include lines containing this string</param>
!!! <param name="Exclude">Exclude all lines containing this string</param>
!!! <remarks>If the include parameter is blank, then only the exclude parameter applies, and vice versa.</remarks>
StringTheory.Filter Procedure(string pInclude, string pExclude)
x  long
  code
  if self.Lines &= null
    return
  end
  loop x = records(self.lines) to 1 by -1
    get(self.lines,x)
    if pInclude
      if instring(upper(clip(pInclude)),upper(self.lines.line),1,1) = 0
        delete(self.lines)
        cycle
      end
    end
    if pExclude
      if instring(upper(clip(pExclude)),upper(self.lines.line),1,1) <> 0
        delete(self.lines)
        cycle
      end
    end
  end

!!! <summary>Return sub-string from the current string value.</summary>
!!! <param name="Start">Start of sub-string.</param>
!!! <param name="Length">Length of sub-string.</param>
!!! <remarks>If the Length paramter is omitted then one character, at position Start, is returned.</remarks>
StringTheory.Sub Procedure(ulong pStart=1, ulong pLength=1) !, string
  code
    if self.value &= null
        return ''
    end
    if pStart < 1
        pStart = 1
    end
    if pStart > Len(self.value)
        return ''
    end

    if pLength+pStart-1 > Len(self.value)
        pLength = Len(self.value) - pStart + 1
    end
    if pLength < 1
        return ''
    end
    return self.Value[pStart : pStart + pLength - 1]


!!! <summary>Return sub-string from the current string value.</summary>
!!! <param name="Start">Start of sub-string.</param>
!!! <param name="End">End of sub-string.</param>
!!! <remarks>If the End position is greater than the length of the string the string length is used
!!! as the stop position. If the Start position greater than the stop position or the length
!!! of the string then an empty string is returned.</remarks>

StringTheory.Slice Procedure(ulong pStart=1, ulong pEnd=0)
  code
    if self.value &= null
        return ''
    end
    if pEnd > Len(self.value) or pEnd < 1
        pEnd = Len(self.value)
    end
    if pStart < 1
        pStart = 1
    end
    if pStart > Len(self.value)
        return ''
    end
    if pStart > pEnd
        return ''
    end

    return self.Value[pStart : pEnd]

!!! <summary>Extracts a "subset" from the string and assigns stores the resultant "slice"
!!! of the string, replacing the original string with the sliced substring</summary>
!!! <param name="Start">Position of the first character to slice from</param>
!!! <param name="Stop">Position of the last character in the slice</param>
!!! <remarks>If the stored string is empty or the start or end positions are invalid then
!!! the stored string is set to an empty string</remarks>
StringTheory.Crop Procedure(ulong pStart=1, ulong pEnd=0)
  code
    if self.value &= null
        return
    end
    if pEnd = 0
        pEnd = self.LengthA()
    end
    if pStart > Len(self.value) or pStart > pEnd or pStart < 1
        return
    end
    if pEnd > Len(self.value) or pEnd < 1
        pEnd = Len(self.value)
    end
    self.SetValue(self.Slice(pStart, pEnd))


!!! <summary>Populates a string with psuedo random alpha numeric characters</summary>
!!! <param name="Length">Position of the first character to slice from</param>
!!! <param name="Stop">Position of the last character in the slice</param>
!!! <param name="pFlags">A combination of the following flags:
!!!     st:Upper       Equate(1)
!!!     st:Lower       Equate(2)
!!!     st:Number      Equate(4)
!!!     st:AlphaFirst  Equate(8)
!!!     st:Punctuation Equate(16) ! warning may not be SQL, html, or xml friendly.
!!! </param>
!!! <remarks>
!!!     If the stored string is empty or the start or end positions are invalid
!!!     then the stored string is set to an empty string. Not that is not a cryptographically
!!!     random string, it is sufficiently random for most uses.
!!! </remarks>
StringTheory.Random Procedure(long pLength=16, long pFlags=0, <String pAlphabet>)
s       &string
px      long, auto
s4      string('GCAT')
s5      string('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
s6      string('abcdefghijklmnopqrstuvwxyz')
s7      string('0123456789')
s8      string('!@#$^&*()-_+={{}[]:;,.`~')
s9      string('ABCDEF0123456789')
s99     string(256)
sl      long, auto
  code
    if pLength = 0
        return ''
    end
    if omitted(pAlphabet) and pFlags = 0 then pFlags = st:Upper.

    s &= new string(pLength)
    if band(pFlags, st:Dna)
        s99 = Clip(s99) & s4
    end
    if band(pFlags, st:Upper)
        s99 = Clip(s99) & s5
    end
    if band(pFlags, st:Lower)
        s99 = clip(s99) & s6
    end
    if band(pFlags, st:Number)
        s99 = Clip(s99) & s7
    end
    if band(pFlags, st:Punctuation)
        s99 = Clip(s99) & s8
    end
    if band(pFlags, st:Hex) and band(pFlags, st:upper) = 0 and band(pFlags, st:lower) = 0 and band(pFlags, st:Number)= 0
      s99 = Clip(s99) & s9
    end
    if not omitted(pAlphabet)
      s99 = Clip(s99) & clip(pAlphabet)
    end
    if s99 = ''
      s99 = s5
    end

    sl = Len(Clip(s99))
    loop px = 1 to pLength
        if px = 1 and Band(pFlags, st:AlphaFirst)
            s[px] = Chr(Random(65,90))
        else
            s[px] = s99[Random(1, sl)]
        end
    end
    self.SetValue(s)
    Dispose(s)
    return self.GetValue()


!-------------------- Internal Methods --------------------!
!!!
!!! <summary>Internal method to dispose of dynamic memory allocated by Split method.</summary>
StringTheory.FreeLines Procedure()
i           long, auto
  code
  if not self.Lines &= null
    loop i = 1 to Records(self.Lines)
      Get(self.Lines, i)
      Dispose(self.Lines.Line)
    end
    Free(self.Lines)
  end

!!! <summary>Internal method to dispose of string value.</summary>
StringTheory.Free Procedure(Long pLines=False)
  code
  if pLines then self.FreeLines().
  Dispose(self.value)

!!! <summary>Returns the clipped length of the string (the length excluding any spaces
!!! on the end of the string.</summary>
StringTheory.ClipLen Procedure()
  code
  return self.ClipLength()


!!! <summary>
!!!     Returns the clipped length of the string (the length excluding
!!!     any spaces on the end of the string.
!!! </summary>
StringTheory.ClipLength Procedure()
  code
    if self.value &= null
        return 0
    else
        return Len(Clip(self.value))
    end


!!! <summary>Loads the specified file into memory</summary>
!!! <param name="fileName>The name of the file to read from</param>
!!! <returns>True if successful, False for errors. There ErrorTrap method will be
!!! called to allow errors to be trapped and handled</returns>
!!! <seealso>SaveFile, ErrorTrap</seealso>
StringTheory.LoadFile Procedure (string fileName,Long pOffset=0, Long pLength=0)
pn                      equate('StringTheory.LoadFile')
hFile                   long, auto
lpFileName              cstring(st:MAX_PATH+1), auto
sizeHigh                long
binDataLen              long, auto
FNULL                   &long
result                  long
  code
    self.free(true)
    self.bytes = 0
    if fileName = ''
        self.winErrorCode = 0
        self.ErrorTrap('LoadFile', 'Cannot load the file, the name is blank.')
        return false
    end
    lpFileName = Clip(fileName)
    hFile = stCreateFile(lpFileName, st:GENERIC_READ, st:FILE_SHARE_READ, 0, st:OPEN_EXISTING, st:FILE_ATTRIBUTE_NORMAL, 0)
    if hFile = st:INVALID_HANDLE_VALUE
        self.winErrorCode = stGetLastError()
        self.ErrorTrap('LoadFile', 'Error loading the file, could not open ' & Clip(lpFileName) & '.')
        return false
    end

    binDataLen = stGetFileSize (hFile, sizeHigh)
    if binDataLen = 0
        stCloseHandle(hFile)
        self.winErrorCode = 0
        self.ErrorTrap('LoadFile', 'Error loading the file, the file size is zero. no data to read.')
        return false
    end

    if pLength < 0 then pLength = 0.
    if pLength > 0
      if pOffset > 0
        binDataLen -= pOffset
      elsif pOffset < 0
        binDataLen = abs(pOffset)
      end
      if binDataLen > pLength then binDataLen = pLength.
    elsif pOffset > 0
      if pOffset >= bindatalen
        bindatalen = 0 ! nothing to read
      else
        binDataLen -= pOffset
      end
    elsif pOffset < 0
      if abs(pOffset) > bindatalen
        pOffset = 0
      else
        binDataLen = abs(pOffset)
      end
    end

    if binDataLen = 0
        stCloseHandle(hFile)
        self.winErrorCode = 0
        self.ErrorTrap('LoadFile', 'Error loading the file, the offset is greater than the file length.')
        return false
    end

    if pOffset < 0
      result = stSetFilePointer(hFile, pOffset, FNULL, st:FILE_END)
    elsif pOffset > 0
      result = stSetFilePointer(hFile, pOffset, FNULL, st:FILE_BEGIN)
    end

    self.base64 = 0
    self.value &= new string(binDataLen)
    !self._length = binDataLen

    if not stReadFile(hFile, self.value, binDataLen, self.bytes, 0)
        stCloseHandle(hFile)
        Dispose(self.value)
        binDataLen = 0
        self.winErrorCode = stGetLastError()
        self.ErrorTrap('LoadFile', 'Error loading the file, the ReadFile API failed.')
        return false
    end

    stCloseHandle(hFile)
    return true



!!! <summary>Save the current string to disk</summary>
!!! <param name="fileName">The name of the file to write to</param>
!!! <param name="pAppendFlag">If this is set to true (1) then the string is appended
!!! to the file, otherwise it overwrites the file if it exists.</param>
!!! <returns>None</returns>
StringTheory.SaveFile Procedure (string fileName, bool pAppendFlag=false)
  code
  return(self.SaveFile(self.value, fileName, pAppendFlag))


!----------------------------------------------------------!
! Saves the specified string to disk, either appending or
! overwriting if the file exists.
!----------------------------------------------------------!
StringTheory.SaveFileA Procedure(string WriteString, string fileName, bool pAppendFlag=false)
  code
  return(self.SaveFile(writeString, fileName, pAppendFlag))


StringTheory.SaveFile Procedure (*string writeString, string fileName, bool pAppendFlag, long dataLen=0)
hFile                   long, auto
lpFileName              cstring(st:MAX_PATH+1), auto
bytesWritten            long
writeSize               long, auto
FNULL                   &long
  code
    if fileName = ''
        self.winErrorCode = 0
        self.ErrorTrap('SaveFile', 'Cannot save the file, the file name passed is blank.')
        return false
    end

    lpFileName = Clip(fileName)
    hFile = stCreateFile(lpFileName, st:GENERIC_WRITE, st:FILE_SHARE_READ, 0, Choose(not pAppendFlag, st:CREATE_ALWAYS, st:OPEN_ALWAYS), 0, 0)
    if hFile = st:INVALID_HANDLE_VALUE
        self.winErrorCode = stGetLastError()
        self.ErrorTrap('SaveFile', 'Could not save the file, the CreateFile API failed to create ' & Clip(fileName))
        return false
    end
    if pAppendFlag
        if stSetFilePointer(hFile, 0, FNULL, st:FILE_END) = st:INVALID_SET_FILE_POINTER
            stCloseHandle(hFile)
            self.winErrorCode = stGetLastError()
            self.ErrorTrap('SaveFile', 'Failed to save the file, SetFilePointer failed, so could not append.')
            return false
        end
    end

    if dataLen <= 0
        writeSize = Size(writeString)
    else
        writeSize = dataLen
    end

    if not stWriteFile(hFile, writeString, writeSize, bytesWritten, 0) ! (handle, data, size, *bytesWritten, overlapped)
        stCloseHandle(hFile)
        self.winErrorCode = stGetLastError()
        self.ErrorTrap('SaveFile', 'Failed to save the file, WriteFile failed to write to ' & Clip(fileName))
        return false
    end
    stCloseHandle(hFile)
    return true


StringTheory.Upper Procedure(<String pQuote>, <String pQuoteEnd>)
x            long
startquote   string(1)
endquote     string(1)
inquotes     long
  code
  if not self.value &= null
    if omitted(2) or pQuote = ''
      self.value = Upper(self.value)
    else
      startQuote = pQuote
      if omitted(3)
        endQuote = pQuote
      else
        endQuote = pQuoteEnd
      end
      loop x = 1 to self.Length()
        if inquotes
          if self.value[x] = endquote
            inquotes = 0
          end
        elsif self.value[x] = startquote
          inquotes = 1
        elsif self.value[x] >= 'a' and self.value[x] <= 'z'
          self.value[x] = chr(val(self.value[x])-32)
        end
      end
    end
  end

StringTheory.Lower Procedure(<String pQuote>, <String pQuoteEnd>)
x            long
startquote   string(1)
endquote     string(1)
inquotes     long
  code
  if not self.value &= null
    if omitted(2) or pQuote = ''
      self.value = Lower(self.value)
    else
      startQuote = pQuote
      if omitted(3)
        endQuote = pQuote
      else
        endQuote = pQuoteEnd
      end
      loop x = 1 to self.Length()
        if inquotes
          if self.value[x] = endquote
            inquotes = 0
          end
        elsif self.value[x] = startquote
          inquotes = 1
        elsif self.value[x] >= 'A' and self.value[x] <= 'Z'
          self.value[x] = chr(val(self.value[x])+32)
        end
      end
    end
  end

!!! <summary>Returns a cstring that contains the contents of the stored string</summary>
!!! <remarks>The caller must dispose the returned cstring</remarks>
StringTheory.ToCstring Function()
psz         &cstring
  code
  psz &= new cstring(Len(clip(self.value))+2)
  psz = clip(self.value) & '<0,0>'  ! double 0 to allow for wide-char case.
  return psz


!!! <summary>Error handling method, called if an error occurs with information
!!!     pertaining to the error</summary>
StringTheory.ErrorTrap Procedure(string methodName, string errorMessage)
  code
  self.LastError = errorMessage
  if self.logErrors
    self.Trace(Clip(methodName) & ': ' & Clip(errorMessage))
    if self.winErrorCode
      self.Trace('  API Error: ' & self.WinErrorCode & ': ' & Self.FormatMessage(self.WinErrorCode))
    end
  end



StringTheory.Trace Procedure(<string errMsg>)
szMsg         cString(len(errMsg)+6)
pMsg          &Cstring
  code
  if omitted(2)
    pMsg &= new cstring(self.LengthA()+6)
    if self.instring('[')
      pMsg = self.GetValue()
    else
      pMsg = '[st] ' & self.GetValue()
    end
    stOutPutDebugString(pMsg)
    dispose(pMsg)
  else
    if instring('[',errMsg,1,1)
      szMsg = Clip(errMsg)
    else
      szMsg = '[st] ' & Clip(errMsg)
    end
    stOutPutDebugString(szMsg)
  end

StringTheory.MD5 Procedure()
Digest          cstring(33),auto
_spacer1        cstring(3),auto
StringChecksum  string(32)
RetVal          long,auto
bufLen          long,auto
ctxPtr          long,auto
i               long,auto
j               long,auto

xStr            string('0123456789abcdef')
xChr            string(1),DIM(16),OVER(xStr)

ContextType     group, type
State             long,DIM(4)       !state (ABCD)
Count             long,DIM(2)       !number of bits, modulo 2^64 (lsb first)
MD5Buffer         byte,DIM(64)      !input buffer
                end

Context         group(ContextType),auto.

  code
  compile ('****',MD5=1)
    ctxPtr = Address(Context)
    RetVal = stMD5Init(ctxPtr)
    RetVal = stMD5Update(ctxPtr, self.value, self.LengthA())
    RetVal = stMD5Final(Digest, ctxPtr)

    !Convert Digest to Hexadecimal
    loop i = 1 to 16
      j = BSHIFT(VAL(Digest[i]), -4)
      StringChecksum[(i-1)*2+1] = xChr[j + 1]
      StringChecksum[i*2]       = xChr[VAL(Digest[i]) - BSHIFT(j, +4) + 1]
    END
  ****
  return StringChecksum



StringTheory.FormatMessage Function(long err)
winErrMessage      cstring(255)
numChars           ulong, auto
  code
    numChars = stFormatMessage(st:FORMAT_MESSAGE_FROM_SYSTEM + st:FORMAT_MESSAGE_IGNORE_INSERTS, 0, err, 0, winErrMessage, 255, 0)
    Return(Clip(winErrMessage))


!-----------------------------------------------------------!
!             Unicode processing and conversion             !
!-----------------------------------------------------------!


!-----------------------------------------------------------!
! Converts the passed ANSI string to a UTF-16 (wide) string
! The returned cstring is twice the length in bytes as the ANSI cstring is.
! Parameters:
!   strAnsi [in]: The string to convert to UTF-16
!   unicodeSize [out]: Set to the number of characters (not the number of bytes)
!       in the returned Unicode string.
! Returns
!
StringTheory.AnsiToUtf16 Procedure(*string strAnsi, *long unicodeChars, ulong CodePage=st:CP_US_ASCII)
flags              long, auto
strLen             long, auto
unicodeString      &string
  code
    strLen = Len(Clip(strAnsi))
    flags = 0

    unicodeChars = stMultiByteToWideChar(CodePage, |       ! Code page to convert from
                                 flags,             |       ! dwFlags
                                 strAnsi,           |       ! Multibyte string
                                 strLen,            |       ! Number of chars in string (-1 = auto detect)
                                 unicodeString,     |       ! Buffer for new string
                                 0)                         ! Size of buffer

    if unicodeChars = 0
        self.ErrorTrap('AnsiToUtf16', 'There is no data to convert, the string length is zero')
        unicodeChars = 0
        return unicodeString                               ! Return Null
    end

    unicodeString &= new string(unicodeChars*2)

    ! Convert to Unicode (UTF-16)
    if not stMultiByteToWideChar(CodePage, flags, strAnsi, strLen, unicodeString, unicodeChars)
        self.WinErrorCode = stGetLastError()
        self.ErrorTrap('AnsiToUtf16', 'Converting ANSI String to Unicode failed.')
        unicodeChars = 0
        Dispose(unicodeString)
        return unicodeString                               ! Return a null
    end

    return unicodeString


!-----------------------------------------------------------!
! UTF-16 to ANSI string conversion
! Parameters:
!   unicodeString: A null terminated string that contains the Unicode (wide) text (UTF-16 encoded)
!   unicodeChars: The number of Unicode characters in the string
!   ansiLen: Set to the length of the returned string that contains the ANSI text
! Returns
!   A pointer to a string that contains the ANSI version of the passed
!   Unicode (UTF-16) string. Returns Null if it fails. The caller is responsible
!   for disposing the returned pointer.
StringTheory.Utf16ToAnsi Procedure(*string unicodeString, *long ansiLen, long unicodeChars=-1, ulong CodePage=st:CP_US_ASCII)
flags              long(0)
ansi               &string
lpDefaultChar      long
lpUsedDefaultChar  long
  code
    ! Get the size required
    ansiLen = stWideCharToMultiByte(CodePage,      |       ! CodePage
                                 flags,             |       ! dwFlags
                                 unicodeString,     |       ! wide-character string
                                 unicodeChars,      |       ! number of chars in string
                                 ansi,              |       ! buffer for new string
                                 0,                 |       ! size of buffer
                                 lpDefaultChar,     |       ! lpDefaultChar - default for unmappable chars - fastest when NULL
                                 lpUsedDefaultChar)         !  lpUsedDefaultChar - set when default char used - fastest when NULL
    if ansiLen = 0
        self.winErrorCode = stGetLastError()
        self.ErrorTrap('Utf16ToAnsi', 'Cannot convert he string to ANSI, the length is zero, or an error occured calculating the length')
        return ansi
    end

    ansi &= new string(ansiLen)

    if not stWideCharToMultiByte(CodePage, flags, unicodeString, unicodeChars, ansi, ansiLen, lpDefaultChar, lpUsedDefaultChar)
        Dispose(ansi)
        self.winErrorCode = stGetLastError()
        self.Errortrap('Utf16ToAnsi', 'Conversion from Unicode to ANSI failed')
        return ansi                                         ! Return a null
    end

    return ansi



!-----------------------------------------------------------!
! UTF-8 to UTF-16
! Parameters
!   strUtf8 [in]: A string that contains the UTF-8 string to convert to UTF-16
!   unicodeChars [out]: The number of Unicode (wide) characters in the
!       converted string (each character is 16 bits).
! Returns
!   A Pointer to a string that contains the UTF-16 encoded text, or Null
!   if an error occurs. The caller is responsible for disposing the returned
!   pointer.
StringTheory.Utf8To16 Procedure(*string strUtf8, *long unicodeChars)
strLen             long, auto
flags              long(0)
unicodeString      &string
  code
    strLen = Len(strUtf8)

    unicodeChars = stMultiByteToWideChar(st:CP_UTF8,        |! Code page to convert from
                                 flags,                     |! dwFlags
                                 strUtf8,                   |! Multibyte string
                                 strLen,                    |! Number of byte in string (-1 = auto detect for null terminated string)
                                 unicodeString,             |! Buffer for new string
                                 0)                         ! Size of buffer
    if unicodeChars = 0
        self.ErrorTrap('Utf8ToAnsi', 'There is no data to convert, the string length is zero')
        unicodeChars = 0
        return unicodeString
    end

    unicodeString &= new string(unicodeChars*2)             ! Returned string is NOT null terminated, because the length of the input string is specified

    ! Convert to Unicode (UTF-16)
    if not stMultiByteToWideChar(st:CP_UTF8, 0, strUtf8, strLen, unicodeString, unicodeChars)
        self.winErrorCode = stGetLastError()
        self.ErrorTrap('Utf8ToUtf16', 'Conversion to UTF-16 failed')
        Dispose(unicodeString)
        return unicodeString                                ! Return a null
    end

    return unicodeString


!-----------------------------------------------------------!
! UTF-16 to UTF-8 conversion
! Parameters
!   unicodeString [in]: A pointer to the UTF-16 encoded string to
!   unicodeChars: The number of Unicode (wide) characters in the string.
!       note that this is the number of characters to convert, not the byte length.
!   utf8Len [out]: The length of the returned UTF-8 encoded string
! Returns
!   A pointer to a string that contains the UTF-8 encoded text, or Null in the event
!   of an error. The caller is responsible for disposing the returned pointer.
StringTheory.Utf16to8 Procedure(*string unicodeString, *long utf8Len, long unicodeChars=0)
flags               long
lpDefaultChar       long
lpUsedDefaultChar   long
utf8                &string
  code
    if unicodeChars = 0
        unicodeChars = Size(unicodeString)/2
    end

    ! Get the size required
    utf8Len = stWideCharToMultiByte(st:CP_UTF8,             |! CodePage
                                 flags,                     |! dwFlags
                                 unicodeString,             |! wide-character string
                                 unicodeChars,              |! number of chars in string
                                 utf8,                      |! buffer for new string
                                 0,                         |! size of buffer
                                 lpDefaultChar,             |! lpDefaultChar - default for unmappable chars - fastest when NULL
                                 lpUsedDefaultChar)         !  lpUsedDefaultChar - set when default char used - fastest when NULL

    if utf8Len = 0
        self.ErrorTrap('Utf16To8', 'The passed string did not contain any text to convert, or an API error occured')
        utf8Len = 0
        return utf8
    end

    utf8 &= new string(utf8Len)

    if not stWideCharToMultiByte(st:CP_UTF8, flags, unicodeString, unicodeChars, utf8, utf8Len, lpDefaultChar, lpUsedDefaultChar)
        Dispose(utf8)
        self.WinErrorCode = stGetLastError()
        self.ErrorTrap('Utf16To8', 'Could not convert the passed string')
        utf8Len = 0
        return utf8
    end

    return utf8


!-----------------------------------------------------------!
! Convert the UTF8 string to an ANSI string
! Parameters
!   strUtf8 [in]: A string that contains the UTF-8 encoded text to convert to ANSI
!   ansiLen [out]: Set to the length of the returned ANSI string
! Returns
!   A pointer to a string that contains the ANSI encoded text if successful
!   and Null if an error occurs. The caller is reponsible for disposing the
!   returned string.
StringTheory.Utf8ToAnsi Procedure (*string strUtf8, *long ansiLen, ulong pCodePage=st:CP_US_ASCII)
unicodeString      &string
unicodeChars       long                             ! Number of wchar characters in the string
ansi               &string
  code
    ! Convert to UTF-16
    unicodeString &= self.Utf8To16(strUtf8, unicodeChars)
    if unicodeString &= null
        ansiLen = 0
        return unicodeString
    end

    ! Convert the UTF-16 string to ANSI
    ansi &= self.Utf16ToAnsi(unicodeString, ansiLen, unicodeChars, pCodePage)
    Dispose(unicodeString)
    return ansi


!-----------------------------------------------------------!
! Convert ANSI encoded text to UTF-8 encoded Unicode text
! Parameters:
!   strAnsi [in]: The ANSI encoded text to convert
!   utf8Len [out]: Set to the length of the UTF-8 encoded string returned
! Returns:
!   A pointer to a string that contains the UTF-8 encoded Unicode text
!   if successful and Null if it fails. The caller is responsible for
!   disposing the returned string.
StringTheory.AnsiToUtf8 Procedure(*string strAnsi, *long utf8Len, ulong pCodePage=st:CP_US_ASCII)
unicodeString      &string
unicodeSize        long                                     ! Number of wchar characters in the string
utf8               &string
  code
    ! Convert to UTF-16
    unicodeString &= self.AnsiToUtf16(strAnsi, unicodeSize,pCodePage)
    if unicodeString &= null
        self.Trace('AnsiToUtf8. Failed to convert to UTF-16')
        utf8Len = 0
        return unicodeString                                ! return null
    end

    ! Convert the UTF-16 string to UTF-8
    utf8 &= self.Utf16to8(unicodeString, utf8Len, unicodeSize)
    Dispose(unicodeString)
    return utf8                                             ! Return the UTF 8 string. The number of characters in the string is returned in the utf8Len parameter


! Backward compatibility wrapper
StringTheory.apiAnsiToUtf8 Procedure(*string strAnsi, *long utf8Len)
  code
    return self.AnsiToUtf8(strAnsi, utf8Len)


! Backward compatibility wrapper
StringTheory.apiUtf8ToAnsi Procedure (*string strUtf8, *long ansiLen)
  code
    return self.Utf8ToAnsi(strUtf8, ansiLen)


!-----------------------------------------------------------!
! Convert the current string to Ansi if it contains Unicode data.
! The .encoding property of the object must have been set to
! st:EncodeUtf8 or st:EncodeUtf16 for this method to have been
! called (or the encoding is specified in the call).
! The ToUnicode method sets this property when an ANSI
! string is converted, and this method sets the encoding to
! st:EncodeAnsi if it completes successfully.
! Returns
!   True for success, False for failure.
StringTheory.ToAnsi Procedure(long encoding=0, ulong pCodePage=st:CP_US_ASCII)
encVal          &string
rLen            long
unicodeChars       long
  code
    if encoding
        self.encoding = encoding
    end
    case self.encoding
    of st:EncodeUtf8
        encVal &= self.Utf8ToAnsi(self.value, rLen, pCodePage)
    of st:EncodeUtf16
        encVal &= self.Utf16ToAnsi(self.value, rLen, unicodeChars, pCodePage)
    else
        return False
    end

    if encVal &= null
        return False
    end

    self.Free(false)
    self.encoding = st:EncodeAnsi
    !self._Length = Len(encVal)
    self.value &= encVal
    return True

!-----------------------------------------------------------!
! Converts the stored ANSI text to Unicode
! using either UTF-8 or UTF-16 encoding
StringTheory.ToUnicode Procedure(long encoding=st:EncodeUtf8, ulong pCodePage=st:CP_US_ASCII)
encVal          &string
rLen            long
  code
    if self.encoding <> st:EncodeAnsi                       ! Only conversion from ANSI currently supported
        self.Trace('String is not ANSI, can''t convert it')
        return False
    end

    case encoding
    of st:EncodeUtf8                                        ! Convert to UTF-8
        encVal &= self.AnsiToUtf8(self.value, rLen, pCodePage)
    of st:EncodeUtf16                                       ! Convert to UTF-16
        encVal &= self.AnsiToUtf16(self.value, rLen,pCodePage)
    else
        self.Trace('ToUnicode: invalid encoding')
        return False
    end

    if encVal &= null
        self.Trace('ToUnicode: encoded string is null, return false')
        return False
    end

    if encoding = st:EncodeUtf16
        self.encoding = st:EncodeUtf16
    else
        self.encoding = st:EncodeUtf8
    end

    self.Free(false)
    self.wideChars = rLen
    self.value &= encVal
    return True

!-----------------------------------------------------------!
!!! <summary>
!!!     Converts the passed Base 10 (decimal) integer to the specified
!!!     base (hexidecimal by default). Does not support bases above 16.
!!! </summary>
!!! <returns>
!!!     Returns a string containing the number in the base specified.
!!!     This converts the entire number to the new base, rather than each byte.
!!!     To convert each byte to hex use the ByteToHex, LongToHex and StringToHex methods.
!!! </returns>
StringTheory.DecToBase  Procedure(long num, long base=16, long lowerCase=1)
remainder       long, auto
rs              string(1)
newNum          string(32)
result          long, auto
alphas          string(6), auto
  code
  result = num
  if lowerCase
    alphas = 'abcdef'
  else
    alphas = 'ABCDEF'
  end

  loop while result > 0
    result = Int(num/base)
    remainder = num%base
    num = result

    if remainder >= 10
      rs = alphas[remainder-9]
    else
      rs = remainder
    end
    newNum = rs & newNum                                ! Prefix the string with the next remainder
  end
  return Clip(newNum)

!!! <summary>
!!!     Converts the passed integer to base 10 (decimal)
!!!     Supports bases up to 36.
!!! <summary>
!!! <returns>
!!!     Returns a long that contains the decimal (base 10) integer
!!!     of the number passed. Invalid numbers will return 0.
!!! </returns>
!!! <remarks>The pass string is treated as a single big endian number,
!!!     with the first digit in the string being the most significant.
!!! </remarks>
StringTheory.BaseToDec  Procedure (string num, long base=16)
decNum          long
power           long auto
digit           string(1), auto
dVal            long
i               long, auto
  code
  power = 1
  loop i= Len(Clip(num)) to 1 by -1
    digit = num[i]
    case digit
    of '0' to '9'
      dVal = digit
    of 'a' to 'z'
      dval = val(digit) - 87
    of 'A' to 'Z'
      dval = val(digit) - 55
    else
      dval = 0
    end
    if dVal > base then cycle.  ! digit out of range, so return the number accumulated so far.
    decNum += (dVal * power)
    power = power * base                                ! Move to the next power of the base
  end
  return decNum

!!! <summary>
!!!     Converts a single byte to hex
!!! </summary>
StringTheory.ByteToHex Procedure (byte pByte)
hex                     string(2), auto
hexChars                string('0123456789abcdef')
  code
  hex[1] = hexChars [bshift(pbyte, -4) + 1]
  hex[2] = hexChars [band(pbyte, 0fh) + 1]
  return hex

StringTheory.HexToByte Procedure(string hexVal)
  code
  if Len(hexVal) > 0 and Len(hexVal) < 3
    return self.BaseToDec(hexVal)
  end
  return 0

!!! <summary>
!!!     Converts a long of 4 bytes to hex
!!! </summary>
StringTheory.LongToHex  procedure (long pLong)
hex                     string(8), auto
inb                     byte, dim(4), over(plong)
  code
    hex[1 : 2] = self.ByteToHex(inb[4])
    hex[3 : 4] = self.ByteToHex(inb[3])
    hex[5 : 6] = self.ByteToHex(inb[2])
    hex[7 : 8] = self.ByteToHex(inb[1])

    return hex

StringTheory.StringToHex Procedure (string binData,Long pLen =0,Long pCase=0)
hex             &string
i               long, auto
j               long, auto
  code
  if pLen = 0 then pLen = len(clip(binData)).
  if pLen = 0
    return hex
  end

  hex &= new string(pLen * 2)         ! Need two characters for each byte
  j = 1
  loop i = 1 to pLen
    hex[j : j + 1] = self.ByteToHex(Val(binData[i]))
    j += 2
  end
  case pCase
  of st:Upper
    hex = upper(hex)
  of st:Lower
    hex= lower(hex)
  end
  return hex

StringTheory.HexToString Procedure (string hexData)
bin             &string
i               long, auto
j               long, auto
  code
  if Len(hexData) = 0
    return bin
  end

  bin &= new string(Len(hexData)/2)                       ! Hex uses two characters for each byte
  j = 1
  loop i = 1 to Len(hexData)-1 by 2                       ! Always convert an even number of characters
    bin[j] = Chr(self.BaseToDec(hexData[i : i + 1]))
    j += 1
  end
  return bin

!!! <summary>
!!!     Converts the stored string to a hexidecimal representation
!!!     each byte is converted to the hex equivilent. The resultant
!!!     string is twice the length (each byte requires two characters
!!!     to represent it).
!!! </summary>
StringTheory.ToHex Procedure (Long pCase=0)
hex     &string
  code
  hex &= self.StringToHex(self.value,self.lengthA(),pCase)
  if not hex &= null
    self.Free(false)
    self.value &= hex
    !self._length = Len(hex)
  end

StringTheory.FromHex Procedure ()
bin         &string
  code
  bin &= self.HexToString(self.value)
  if not bin &= null
    self.Free(false)
    self.value &= bin
    !self._length = Len(bin)
  end

StringTheory.WrapText Procedure(long wrapAt=80)
wt          StringTheory
sPos        long, auto                                      ! Start of the current block being processed
ePos        long, auto                                      ! End of the block
w           long, auto
cLast       long, auto                                      ! Last position that was append
  code
    if wrapAt < 0                                           ! Invalid line length
        return
    elsif wrapAt >= self.LengthA()                          ! Text is shorter than a single line
        return
    end

    sPos = 1
    cLast = 0                                               ! The last position that the string was split for appending
    loop
        ePos = sPos + wrapAt - 1                            ! Find white space to break at.
        if ePos > self.LengthA()                            ! End position past the end of the string
            break
        end

        loop w = ePos to sPos by -1
            if self.value[w] = ' ' or self.value[w] = '<11>'
                wt.AppendA(self.Slice(sPos, w-1) & '<13,10>')
                ePos = w
                cLast = w
                break
            end
        end
        if w <= sPos                                        ! Could not break the text, add the non broken section.
            wt.AppendA(self.slice(sPos, ePos))
            cLast = ePos
        end

        sPos = ePos + 1
        if sPos > self.LengthA()
            break
        end
    end

    if cLast = 0                                            ! No white space to wrap at
        return
    elsif cLast < self.LengthA()                            ! Append the remainder
        wt.AppendA(self.Slice(cLast))
    end

    self.Free(false)
    self.value &= wt.value
    wt.value &= null


!!! <summary>Safe copy between two strings. Does bounds checking
!!!     and truncates the output if the string is not large enough
!!!     to contain it.
!!! </summary>
!!! <param name=""
!!! <returns>
!!!     Returns 0 for success, -1 for failure, and if the destination
!!!     string is too small to contain all the data, the size required
!!!     is returned.
!!! </returns>
StringTheory.StrCpy Procedure(*string pIn, *string pOut, bool pClip = true)
sLen        long, auto
dLen        long, auto
  code
    if pClip
        sLen = Len(Clip(pIn))
    else
        sLen = Len(pIn)
    end
    dLen = Len(pOut)

    if dLen = 0
        return -1
    elsif sLen = 0
        Clear(pOut)
        return 0
    end

    pOut = pIn           ! Truncation will occur naturally - no need for "safe" string slicing, also clipping achieves nothing
    return Choose(sLen > dLen, slen, 0)

!----------------------------------------------------------!
! Conversion functions. Push the data into the string as a series of bytes values
! so that no typecasting is done. Allows byte values to be pushed into a string for encryption
! and then retrieved after decryption.
!
! Ideally this could be done with a single method that takes a *? parameter, however because of the
! way that ANYs working in Clarion this is not possible; Clarion doesn't provide a void* equivilent
! and passing a long that stores the address is just ugly and requires the call to pass Address(param)

StringTheory.ToBytes Procedure(*long pVal, *string bVals)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        Clear(bVals, -1)
    else
        bVals = sVal
    end


StringTheory.ToBytes Procedure(*ulong pVal, *string bVals)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        Clear(bVals, -1)
    else
        bVals = sVal
    end


StringTheory.ToBytes Procedure(*short pVal, *string bVals)
sVal        string(Size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        Clear(bVals, -1)
    else
        bVals = sVal
    end


StringTheory.ToBytes Procedure(*ushort pVal, *string bVals)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        Clear(bVals, -1)
    else
        bVals = sVal
    end


StringTheory.ToBytes Procedure(*byte pVal, *string bVals)
sVal        string(Size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        Clear(bVals, -1)
    else
        bVals = sVal
    end


! Unfortunately when a decimal is passed by reference Size() cannot be used
! for an Over(), so the data has the be Peek'd (or memcpy'd)
StringTheory.ToBytes Procedure(*decimal pVal, *string bVals)
  code
    Assert(Size(pVal) = Len(bVals), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    Peek(Address(pVal), bVals)




StringTheory.ToBytes Procedure(*real pVal, *string bVals)
sVal        string(Size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        Clear(bVals, -1)
    else
        bVals = sVal
    end


StringTheory.ToBytes Procedure(*sreal pVal, *string bVals)
sVal        string(Size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        Clear(bVals, -1)
    else
        bVals = sVal
    end


!----------------------------------------------------------!
! Note that the *string and *cstring variants are
! essentially just assignments (direct copies).
! They are provided for convenience so that the caller
! doesn't have to differeniate between data types - the
! ToBytes and FromBytes methods can be called regardless
! of what data type is being handled.
!----------------------------------------------------------!

StringTheory.ToBytes Procedure(*cstring pVal, *string bVals)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Len(pVal)
        Clear(bVals, -1)
    else
        bVals = pVal
    end


StringTheory.ToBytes Procedure(*string pVal, *string bVals)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in ToBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Len(pVal)
        Clear(bVals, -1)
    else
        bVals = pVal
    end

!----------------------------------------------------------!

StringTheory.FromBytes Procedure (*string bVals, *long pVal)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        pVal = 0
    else
        sVal = bVals
    end

StringTheory.FromBytes Procedure (*string bVals, *ulong pVal)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        pVal = 0
    else
        sVal = bVals
    end


StringTheory.FromBytes Procedure (*string bVals, *short pVal)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        pVal = 0
    else
        sVal = bVals
    end


StringTheory.FromBytes Procedure (*string bVals, *ushort pVal)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        pVal = 0
    else
        sVal = bVals
    end


StringTheory.FromBytes Procedure (*string bVals, *byte pVal)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        pVal = 0
    else
        sVal = bVals
    end


StringTheory.FromBytes Procedure (*string bVals, *decimal pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        pVal = 0
    else
        Peek(Address(bVals), pVal)
    end


StringTheory.FromBytes Procedure (*string bVals, *real pVal)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        pVal = 0
    else
        sVal = bVals
    end


StringTheory.FromBytes Procedure (*string bVals, *sreal pVal)
sVal        string(size(pVal)), over(pVal)
  code
    Assert(Len(bVals) = Size(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Size(pVal)
        pVal = 0
    else
        sVal = bVals
    end


StringTheory.FromBytes Procedure (*string bVals, *cstring pVal)
  code
    Assert(Len(bVals) = Len(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Len(pVal)
        Clear(pVal, -1)
    else
        pVal = bVals
    end


StringTheory.FromBytes Procedure (*string bVals, *string pVal)
  code
    Assert(Len(bVals) = Len(pVal), 'StringTheory Error in FromBytes: The parameters are not the same length in bytes!')
    if Len(bVals) <> Len(pVal)
        Clear(pVal, -1)
    else
        pVal = bVals
    end

!----------------------------------------------------------!
! Store the passed parameter as bytes in the StringTheory
! object. The bytes are copied directly without any type
! conversion. For example this allows a long to be be stored in a 4 byte
! string by call SetBytes followed by GetBytes to retrieve the value.
!
! Unfortunately this cannot be done with a single method that takes a
! ?* because of Clarion's storage of the ANY type and there is no void*
! equivilent.
!----------------------------------------------------------!
StringTheory.SetBytes Procedure (*long pVal)
sVal        string(Size(pVal)), over(pVal)
  code
    self.SetLength(Size(pVal))
    self.value = sVal


StringTheory.SetBytes Procedure (*ulong pVal)
sVal        string(Size(pVal)), over(pVal)
  code
    self.SetLength(Size(pVal))
    self.value = sVal


StringTheory.SetBytes Procedure (*short pVal)
sVal        string(Size(pVal)), over(pVal)
  code
    self.SetLength(Size(pVal))
    self.value = sVal


StringTheory.SetBytes Procedure (*ushort pVal)
sVal        string(Size(pVal)), over(pVal)
  code
    self.SetLength(Size(pVal))
    self.value = sVal


StringTheory.SetBytes Procedure (*byte pVal)
sVal        string(Size(pVal)), over(pVal)
  code
    self.SetLength(Size(pVal))
    self.value = sVal


StringTheory.SetBytes Procedure (*decimal pVal)
dLen        long
  code
    dLen = Size(pVal)

    self.SetLength(dLen)
    Peek(Address(pVal), self.value)


StringTheory.SetBytes Procedure (*real pVal)
sVal        string(Size(pVal)), over(pVal)
  code
    self.SetLength(Size(pVal))
    self.value = sVal


StringTheory.SetBytes Procedure (*sreal pVal)
sVal        string(Size(pVal)), over(pVal)
  code
    self.SetLength(Size(pVal))
    self.value = sVal

!----------------------------------------------------------!
! The string methods are a direct assignment
! the same as calling SetValue. They are provided for
! convenience so that the caller doesn't handle to handle
! different data types (for example when processing all data
! in a structure such as a File, Group or Queue).
!----------------------------------------------------------!

StringTheory.SetBytes Procedure (*cstring pVal)
  code
    self.SetLength(Len(pVal))
    self.value = pVal


StringTheory.SetBytes Procedure (*string pVal)
  code
    self.SetLength(Len(pVal))
    self.value = pVal


!----------------------------------------------------------!


StringTheory.GetBytes Procedure (*long pVal)
  code
    self.FromBytes(self.value, pVal)


StringTheory.GetBytes Procedure (*ulong pVal)
  code
    self.FromBytes(self.value, pVal)


StringTheory.GetBytes Procedure (*short pVal)
  code
    self.FromBytes(self.value, pVal)


StringTheory.GetBytes Procedure (*ushort pVal)
  code
    self.FromBytes(self.value, pVal)


StringTheory.GetBytes Procedure (*byte pVal)
  code
    self.FromBytes(self.value, pVal)


StringTheory.GetBytes Procedure (*decimal pVal)
  code
    self.FromBytes(self.value, pVal)


StringTheory.GetBytes Procedure (*real pVal)
  code
    self.FromBytes(self.value, pVal)


StringTheory.GetBytes Procedure (*sreal pVal)
  code
    self.FromBytes(self.value, pVal)


StringTheory.GetBytes Procedure (*cstring pVal)
  code
    self.FromBytes(self.value, pVal)


StringTheory.GetBytes Procedure (*string pVal)
  code
    self.FromBytes(self.value, pVal)



! General handling method that provide byte storage
! for any data type.

StringTheory.SetBytes Procedure (*? pVal, string pType)
_v              string(1)
_v2             string(2)
_v4             string(4)
_v8             string(8)

bVal            byte, over(_v)
sVal            short, over(_v2)
usVal           ushort, over(_v2)
lVal            long, over(_v4)
ulVal           ulong, over(_v4)
srVal           sreal, over(_v4)
rVal            real, over(_v8)
pn              equate('StringTheory.SetBytes')
  code
    self.Free(false)

    case pType
    of 'BYTE'
        bVal = pVal
        self.value &= new string(size(bVal))
        self.value = _v
    of 'SHORT'
        sVal = pVal
        self.value &= new string(size(sVal))
        self.value = _v2
    of 'USHORT'
        usVal = pVal
        self.value &= new string(size(usVal))
        self.value = _v2
    of 'LONG'
        lVal = pVal
        self.value &= new string(size(lVal))
        self.value = _v4
    of 'ULONG'
        ulVal = pVal
        self.value &= new string(size(ulVal))
        self.value = _v4
    of 'SREAL'
        srVal = pVal
        self.value &= new string(size(srVal))
        self.value = _v4
    of 'REAL' orof 'DECIMAL'                                ! Decimals stored in an ANY are stored as REALs and hence may loose precision
        rVal = pVal
        self.value &= new string(size(rVal))
        self.value = _v8
    of 'CSTRING' orof 'STRING'                              ! For strings Len() returns the length of the string stored in the ANY
        if Len(pVal) > 0
            self.value &= new string(Len(pVal))
            self.value = pVal
        else
            self.ErrorTrap(pn, 'Cannot store the passed string, the length is zero')
            return False
        end
    else                                                    ! Invalid or unsupported data type
        self.ErrorTrap(pn, 'Cannot store the passed value, it is not of a valid type (' & pType & ' is not supported)')
        return False
    end

    return True


StringTheory.GetBytes Procedure (*? pVal, string pType)
_v              string(1)
_v2             string(2)
_v4             string(4)
_v8             string(8)

bVal            byte, over(_v)
sVal            short, over(_v2)
usVal           ushort, over(_v2)
lVal            long, over(_v4)
ulVal           ulong, over(_v4)
srVal           sreal, over(_v4)
rVal            real, over(_v8)

dLen            long, auto
pn              equate('StringTheory.GetBytes')
  code
    case pType
    of 'BYTE'
        dLen = 1
    of 'SHORT' orof 'USHORT'
        dLen = 2
    of 'LONG' orof 'ULONG' orof 'SREAL'
        dLen = 4
    of 'REAL' orof 'DECIMAL'
        dLen = 8
    of 'STRING' orof 'CSTRING'
        dLen = Len(pVal)
    else
        self.ErrorTrap(pn, 'Unsupported data type: ' & pType)
        return False
    end

    Assert(Len(self.value) = dLen, 'StringTheory.GetBytes error. ' & Len(self.value) & ' bytes stored, ' & dLen & ' bytes required for type: ' & pType)
    if Len(self.value) <> dLen
        pVal = 0
        return False
    end

    case pType
    of 'BYTE'
        _v = self.value
        pVal = bVal
    of 'SHORT'
        _v2 = self.value
        pVal = sVal
    of 'USHORT'
        _v2 = self.value
        pVal = usVal
    of 'LONG'
        _v4 = self.value
        pVal = lVal
    of 'ULONG'
        _v = self.value
        pVal = ulVal
    of 'SREAL'
        _v4 = self.value
        pVal = srVal
    of 'REAL' orof 'DECIMAL'                                ! Decimals stored in an ANY are stored as REALs and hence may loose precision
        _v8 = self.value
        pVal = rVal
    of 'CSTRING' orof 'STRING'                              ! For strings Len() returns the length of the string stored in the ANY
        pVal = self.value
    else                                                    ! Invalid or unsupported data type (should never get here as the first CASE statement already handles this)
        return False
    end

    return True



! Find the start of the next word in the string from the passed starting position
! Also supports searching within data from and HTML string (handles escaped character etc.)
! Note for HTML this won't skip over HTML tags, that needs to be handled by the caller.
! Parameters
!   pStartPos: The position in the string to start at.
!   textType: Specifies what type of handling of white space is done:
!           0: (the default) Input is treated as text and all punctuation is excluded from "words"
!           1: handling for HTML. Handles ampersands used for escape encoding
!           2: Punctuation is included in the "word" returned. Only space characters are treated as breaking words space.
StringTheory.WordStart Procedure(long pStartPos=1, long textType=ST:TEXT,Long pDir=st:Forwards)
cPos                long, auto
textLen             long, auto
charList            string('. <13><10><9>,-;"''!?&()*/+=<>:')
htmlList            string('. <13><10><9>,-;"''!?()*/+=<>:')
ws                  long, auto
escEnd              long, auto
nws                 long
  code
  textLen = self.LengthA()
  if pStartPos < 1 or pStartPos > textLen
    return 0
  end
  cPos = pStartPos
  loop
    if cPos > textLen
      return 0 !   Not found
    end

    case textType
    of ST:HTML                                                ! HTML
      ws =  Instring(self.value[cPos], htmlList, 1, 1)
    of ST:NOPUNCTUATION                                       ! Only spaces used to break words
      ws = choose(self.value[cPos] = ' ',1,0)
    of ST:TEXT       ! Normal text - handles punctation etc.
      ws = Instring(self.value[cPos], charList, 1, 1)
    end

    if cPos = 1 and pDir = st:Backwards and ws = 0
      return 1
    elsif ws and pDir = st:Forwards                               ! Character is whitespace
      cPos += 1
    elsif ws and pDir = st:Backwards and nws
      return nws
    elsif cPos = 1 and pDir = st:Backwards and ws
      return 0  ! word not found
    elsif ws = 0 and pDir = st:Forwards
                        if textType = ST:HTML                           ! Check for escape codes
                          if self.value[cPos] = '&'
                                  escEnd = self.findChar(';', cPos)
                                        if escEnd
                                                cPos = escEnd + 1
                                                cycle
                                        else                                    ! Not an escape sequence (HTML is probably invalid, so treat this like a single char whitespace).
                                                cPos += 1                           ! skip just the ampersand
                                                cycle
                                        end
                                end
                        end
                        return cPos
                elsif ws and pDir = st:Backwards and nws = 0
                  cPos -= 1
                elsif ws = 0 and pDir = st:Backwards
                  nws = cPos
                  cPos -= 1
    end
  end
  return 0                                                ! Not found


! Returns the end position of the word when passed the start position.
! Returns 0 if the start position is invalid, or the position of the end of
! the word, or the string length if the no white space is found before the end
! of the string.
!
! Parameters
!   pStartPos: The position in the string to start at.
!   textType: Specifies what type of handling of white space is done:
!       ST:TEXT: (the default) Input is treated as text and all punctuation is excluded from "words"
!       ST:HTML: handling for HTML. Handles ampersands used for escape encoding
!       ST:NOPUNCTIONATION: Punctuation is included in the "word" returned. Only space characters are treated as breaking words space.
StringTheory.WordEnd Procedure (long pStartPos=1, long textType=ST:TEXT)
i                   long, auto
textLen             long, auto
charList            string('. <13><10><9>,-;"''!?&()*/+=<>:')      ! Characters which count as whitespace
htmlList            string('. <13><10><9>,-;"''!?&()*/+=<>:')      ! Allows HTML whitespace to be handled differently
  CODE
    textLen = self.LengthA()
    if pStartPos > textLen or pStartPos < 1                 ! Invalid start position
        return 0
    elsif pStartPos = textLen                               ! Only the final character checked (or a single char string)
        !return Choose(Instring(self.value[pStartPos], Choose(not htmlEnc, charList, htmlList), 1, 1), 0, textLen)

        case textType
        of ST:HTML                                         ! HTML
            return choose(Instring(self.value[pStartPos], htmlList, 1, 1),0,textLen)
        of ST:NOPUNCTUATION                                ! Only spaces used to break words
            return choose(self.value[pStartPos] = ' ',0,textLen)
        else                                                ! Normal text (ST:TEXT) - handles punctation etc.
            return choose(Instring(self.value[pStartPos], charList, 1, 1),0,textlen)
        end
    end

    loop i = pStartPos to textLen
        if self.value[i] = ''''                             ! Apostrophes (single quotes) need to be handled separately
            if i = textLen or not IsAlpha(self.value[i + 1])! If the single quote terminates the string, or the next character is not alphabetic, then this is the end of the word
                break
            end
        else
            case textType
            of ST:HTML
                if Instring(self.value[i], htmlList, 1, 1)
                    break
                end
            of ST:NOPUNCTUATION
                if self.value[i] = ' '
                    break
                end
            else                                            ! ST:TEXT
                if Instring(self.value[i], charList, 1, 1)
                    break
                end
            end
        end
    end
    Return Choose(i > pStartPos, i - 1, 0)



StringTheory.CountWords Procedure(long startPos = 1, bool htmlEnc=false)
wordCount       long
endPos          long, auto
sLen            long, auto
  code
    sLen = self.LengthA()

    if startPos < 1 or startPos > sLen
        return 0
    end

    loop
        startPos = self.WordStart(startPos, htmlEnc)
        if not startPos
            break
        else                                                ! Found a word
            wordCount += 1

            ! Find the end of the word
            endPos = self.WordEnd(startPos, htmlEnc)
            if endPos = 0 or endPos >= sLen
                break
            end
            startPos = endPos + 1
        end
    end
    return wordCount


!!! <summary> Removes all attributes from all instances of an html tag in the string.
!!! </summary>
!!! <param name="pTag">The HTML tag, without the brackets from which attributes should be removed.</param>
!!! <returns>
!!!     nothing.
!!! </returns>
StringTheory.RemoveAttributes Procedure(String pTag)
x       long, auto
y       long, auto
l       long, auto
  code
    x = 1
    loop
        l = Len(Clip(pTag))+1
        x = self.findChars('<' & Clip(pTag), x)
        if x
            y = self.findChar('>', x)
            if y
                if y <> x + l
                  self.SetValue(Sub(self.value,1,x) & Clip(pTag) & Sub(self.value, y, self.LengthA()-y+1))
                end
            else
                break
            end
        else
            break
        end
        x += l
    end

StringTheory.Remove  Procedure(string pLeft,<string pRight>,long pNoCase=0,long pContentsOnly=0, long pCount=0)
x      long
y      long ,auto
count  long
ll     long,auto
lr     long,auto
  code
  if omitted(3) or pRight = ''
    count = self.replace(pLeft,'',pCount,,,pNoCase)
  else
    ll = len(clip(pLeft))
    lr = len(clip(pRight))
    loop
      x = self.instring(pleft,,x+1,,pNoCase)
      if x
        y = self.instring(pRight,,x+ll,,pNocase)
        if y
          if pContentsOnly
            x += ll - 1
            self.SetValue(self.slice(1,x) & self.slice(y))
          else
            y += lr
            x -= 1
            if x = 0
              self.SetValue(self.slice(y))
            else
              self.SetValue(self.slice(1,x) & self.slice(y))
            end
          end
          count += 1
          if count >= pCount and pCount <> 0 then break.
        else
          break
        end
      else
        break
      end
    end ! loop
  end
  return count

! Get a given word number
StringTheory.GetWord Procedure(long pWordNumber,long startPos = -1, bool htmlEnc=false) !, string
ps  long
pe  long
  code
  return self.FindWord(pWordNumber,startPos,htmlEnc,ps,pe)

! same as getword, but better name.
StringTheory.Word Procedure(long pWordNumber,long startPos = -1, bool htmlEnc=false) !, string
ps  long
pe  long
  code
  return self.FindWord(pWordNumber,startPos,htmlEnc,ps,pe)

! same as Word, but returns positions for the boundaries of the word as well.
StringTheory.FindWord Procedure(long pWordNumber,long startPos = -1, bool htmlEnc=false,*Long pStart, *Long pEnd) !, string
wordCount       long
endPos          long, auto
sLen            long, auto
Pos             long
  code
  sLen = self.LengthA()
  pStart = 0
  pEnd = 0
  if pWordNumber >= 0
    if startPos < 1 then startPos = 1.
  else
    if startPos < 1 then startPos = sLen.
  end
  if startPos > sLen then startPos = sLen.
  if pWordNumber = 0
    return ''
  elsif pWordNumber > 0
                loop
                        startPos = self.WordStart(startPos, htmlEnc, st:Forwards)
                        if startPos < 1 then break.    ! word not found
                        endPos = self.WordEnd(startPos, htmlEnc)        ! Find the end of the word
                        if endPos < startPos or endPos > sLen then break.    ! word not found
                        wordCount += 1  ! Found a word
                        if wordCount = pWordNumber
                                pStart = startPos
                                pEnd = endPos
                                return self.Value[startPos : endPos]        ! found THE word we wanted
                        end
                        startPos = endPos + 1
                end
        elsif pWordNumber < 0
                loop
                        startPos = self.WordStart(startPos, htmlEnc, st:backwards)
                        if startPos < 1 then break.    ! word not found
                        endPos = self.WordEnd(startPos, htmlEnc)        ! Find the end of the word
                        if endPos < startPos or endPos > sLen then break.    ! word not found
                        wordCount += 1  ! Found a word
                        if wordCount = -pWordNumber
                                pStart = startPos
                                pEnd = endPos
                                return self.Value[startPos : endPos]        ! found THE word we wanted
                        end
                        startPos = startPos - 1
                end
        end

  return ''  ! requested word not found

! Clip the current value
StringTheory.Clip Procedure()
clipLen         long, auto
  code
    clipLen = self.ClipLength()
    if clipLen < self.LengthA()
        self.SetLength(clipLen)
    end

! Remove space off the start and end of the current value
StringTheory.Trim Procedure()
  code
    self.setValue(clip(left(self.GetValue())))


! Remove excessive white space. Each word separated by one space only.
! Shuffles down words in situ then truncates the remaining length.
! Parameters
!       ST:TEXT: (the default) Input is treated as text and all punctuation is excluded from "words" (and hence treated as white space)
!       ST:HTML: handling for HTML. Handles ampersands used for escape encoding.
!       ST:NOPUNCTIONATION: Punctuation is treated as normal characters as not as white space.
StringTheory.Squeeze Procedure(long textType = ST:TEXT)
startPos        long, auto
endPos          long, auto
sLen            long, auto
ptr             long, auto         ! Pointer to current position (end of current string)
newPtrPos       long, auto         ! Pointer to new position
  code
    sLen = self.LengthA()
    if sLen < 1
        return
    end

    startPos = 1
    ptr = 0

    loop
        startPos = self.WordStart(startPos, textType)
        if startPos < 1
            break                                           ! Word not found
        end

        endPos = self.WordEnd(startPos, textType)            ! Find the end of the word
        if endPos < startPos or endPos > sLen
            break    ! word not found
        end

        if ptr                                              ! do we already have a word??
            ptr += 1
            self.value[ptr] = ' '                           ! put a space between words
        end

        ptr += 1
        newPtrPos = ptr + endPos - startPos

        if startPos > ptr
            ! shuffle down characters...
?           Assert(newPtrPos - ptr = endPos - startPos, 'StringTheory Error in Squeeze: The shuffle lengths are not the same!')   ! Check lengths are equal
            self.value[ptr : newPtrPos] = self.value[startPos : endPos]
        end
        ptr = newPtrPos
        startPos = endPos + 1

    end

    if ptr < sLen
        self.SetLength(ptr)                                 ! truncate the length
    end


! Returns True (1) if the string contains at least one of the
! "digit" characters. The alphabet to search against
StringTheory.ContainsADigit  Procedure() !, bool
  code
  return self.ContainsA('0123456789')

! Returns True (1) if the string contains at least one of the
! "digit" characters. The alphabet to search against
StringTheory.ContainsA  Procedure(String pAlphabet,<String pTestString>) !, bool
i       long, auto
  code
  if omitted(3)
    if self.LengthA() < 1
      return false
    end
    loop i = 1 TO len(clip(pAlphabet))
      if self.instring(pAlphabet[i])
        return true
      end
    end
  else
    loop i = 1 TO len(clip(pAlphabet))
      if instring(pAlphabet[i],pTestString,1,1)
        return true
      end
    end
  end
  return false

! Returns True if the string contains only digit characters
! ('0' to '9') or False otherwise.
StringTheory.IsAllDigits  Procedure() !, bool
  code
  return self.IsAll('0123456789')


! Returns True if the string contains only characters
! in the passed parameter
StringTheory.IsAll  Procedure(String pAlphabet,<String pTestString>) !, bool
i       long, auto
  code
  if omitted(3)
                if self.LengthA() < 1
                        return false
                end

                loop i = 1 to self.LengthA()
                        if instring(self.value[i],clip(pAlphabet),1,1) = 0
                                return false
                        end
                end
        else
                loop i = 1 to len(pTestString)
                        if instring(pTestString[i],clip(pAlphabet),1,1) = 0
                                return false
                        end
                end
        end
  return true

! Switches between big endian and little endian
StringTheory.SwitchEndian Procedure(ulong x)
bigEnd          group, over(x)
first               byte
second              byte
third               byte
fourth              byte
                end

temp            byte, auto
  code
    temp = BigEnd.first
    bigEnd.first = BigEnd.fourth
    bigEnd.fourth = temp

    temp = BigEnd.second
    BigEnd.second = BigEnd.third
    BigEnd.third = temp

    return x

! BigEndian and LittleEndian methods, provided for clarity

! Returns a big endian long when passed a little endian one
StringTheory.BigEndian Procedure(ulong x)
  code
    return self.SwitchEndian(x)


! Returns a little endian long when passed a big endian one
StringTheory.LittleEndian Procedure(ulong x)
  CODE
    return self.SwitchEndian(x)



! Reverse the byte order of the stored string
StringTheory.ReverseByteOrder Procedure()
s               string(1), auto
sPos            long, auto
ePos            long, auto
dataLen         long, auto
  code
    if self.value &= null
        return
    end

    ePos = self.LengthA()
    dataLen = Int(ePos/2)
    loop sPos = 1 to dataLen
        s = self.value[sPos]
        self.value[sPos] = self.value[ePos]
        self.value[ePos] = s
        ePos -= 1
    end


! Returns the current value as a string (equivilent to GetValue())
! o
StringTheory.Str Procedure() !, string
  code
    if self.value &= null
        return ''
    else
        return self.value
    end


! Sets the value to newValue and returns the value set
! or an empty string is the passed string length is zero.
StringTheory.Str Procedure(string newValue) !, string
strLen          long,auto
  code

    strLen = Len(newValue)
    if strLen = 0
        self.Free(false)
        return ''
    end

    if strLen <> self.LengthA()
        self.Free(false)
        self.value &= new string(strLen)
    end
    self.value = newValue
    self.base64 = 0
    return self.value


StringTheory.Str Procedure(*string newValue) !, string
strLen          long, auto
  code

    strLen = Len(newValue)
    if strLen = 0
        self.Free(false)
        return ''
    end

    if strLen <> self.LengthA()
        self.Free(false)
        self.value &= new string(strLen)
    end
    self.value = newValue
    self.base64 = 0
    return self.value


! Insert a sub-string of the passed string into the stored
! string at the specified position.
StringTheory.SetSlice Procedure(ulong pStart=1, ulong pEnd=0, string newValue)
  code
    if pStart < 1
        return
    elsif pEnd = 0
        if len(newValue) > 0
            pEnd = pStart + len(newValue) - 1
        else
            pEnd = pStart
        end
    elsif pEnd < pStart
        return
    end

    if pEnd > self.lengthA()
        self.SetLength(pEnd)    ! expand the string
    end

    self.Value[pStart : pEnd] = newValue


StringTheory.Insert Procedure(long pStart, string insertValue)
strLen          long, auto
  code
    if pStart < 1 or len(insertValue) = 0
        return
    end

    strLen = self.LengthA()
    if pStart > strLen
        strlen = pStart + len(insertValue) - 1
    else
        strlen += len(insertValue)
    end

    self.SetLength(strLen)                         ! expand the string

    if pStart = 1
        self.value = insertValue & self.value      ! insert at start
    elsif strLen = pStart + len(insertValue) - 1
        self.value[pStart : strLen] = insertValue  ! insert at end
    else
        self.value[pStart : strLen] = insertValue & self.value[pStart : strLen - len(insertValue)]
    end

! Quote the string
StringTheory.Quote Procedure(<string pQuotestart>, <string pQuoteEnd>)
quoteStart          string(1), auto
quoteEnd            string(1), auto
strLen              long, auto
  code

    if omitted(2)
        quoteStart = '"'
    else
        quoteStart = pQuoteStart
    end

    if omitted(3)
        quoteEnd = quoteStart
    else
        quoteEnd = pQuoteEnd
    end

    strLen = self.LengthA()
    if strLen > 1 and self.value[1] = quoteStart and self.value[strLen] = quoteEnd
        ! do nothing as string is already quoted
    else
        self.SetLength(strLen + 2)                          ! expand the string
        self.value = quoteStart & self.value[1 : strlen] & quoteEnd
    end


! Remove quotes from the string
StringTheory.UnQuote Procedure(<string pQuotestart>, <string pQuoteEnd>)
quoteStart          string(1), auto
quoteEnd            string(1), auto
strLen              long, auto
  code

    strLen = self.LengthA()
    if strLen < 2
        return
    end

    if omitted(2)
        if self.value[1] = ''''
            quoteStart = ''''                               ! single quote
        else
            quoteStart = '"'                                ! double quote (default)
        end
    else
        quoteStart = pQuoteStart
    end

    if omitted(3)
        quoteEnd = quoteStart
    else
        quoteEnd = pQuoteEnd
    end

    if self.value[1] = quoteStart and self.value[strLen] = quoteEnd
        if strLen = 2
            self.free(false)
        else
            self.value = self.value[2 : strLen - 1]
            self.SetLength(strLen - 2)                      ! truncate the string
        end
    end


! Find a specific character and return the position of the match
StringTheory.FindChar Procedure(string pSearchValue, long pStart=1, long pEnd=0)!,long
strLen      long, auto
i           long, auto
numChars    long, auto

  code
    strLen = self.LengthA()
    if strLen = 0 or len(pSearchValue) = 0
        return 0
    end

    if pStart < 1
        pStart = 1
    end

    if pEnd < 1 or pEnd > strLen
        pEnd = strLen
    end

    numChars = pEnd - pStart + 1
    if numChars < 1
        return 0
    end

    i = memchr(Address(self.value) + pStart - 1, val(pSearchValue[1]), numChars)
    if i = 0
        return 0                                            ! not found
    else
        return i - Address(self.value) + 1
    end


! Find a string and return the position of the match
StringTheory.FindChars Procedure(string pSearchValue, long pStart=1, long pEnd=0)!,long
strLen      long, auto
searchLen   long, auto
i           long, auto
charVal     long, auto
endAddress  long, auto

  code
    strLen = self.LengthA()
    searchLen = size(pSearchValue)
    if strLen = 0 or searchLen = 0
        return 0
    end

    if pStart < 1
        pStart = 1
    end

    if pEnd < 1 or pEnd > strLen
        pEnd = strLen
    end

    if pEnd - pStart + 1 < searchLen
        return 0                                           ! cannot match as string to be searched is shorter than search value
    end

    charVal = val(pSearchValue[1])
    endAddress = Address(self.value) + pEnd - searchLen
    i = Address(self.value) + pStart - 1                   ! set up the initial address to search from

    loop
        i = memchr(i, charVal, endAddress - i + 1)         ! get match on 1st char of pSearchValue
        if i = 0
            return 0                                       ! first character was not found
        end

        ! we have a match on the first character - i holds the address -
        ! now check for a match on the whole search string
        if memcmp(i, Address(pSearchValue), searchLen) = 0 ! do we have match? (with memcmp 0 = match)
            return(i - Address(self.value) + 1)            ! return byte position where pSearchValue was matched
        end

        if i < endAddress
            i += 1                                         ! no match so increment start address i and try again
        else
            return 0                                       ! pSearchValue was not found
        end
    end


! Convert a color stored in a long to a hex string. If addHash is passed the
! color is returned using the standard web format: #FFFFFF
StringTheory.ColorToHex Function(long claColor, bool addHash=false)
rgb                 group,over(claColor)
r                       byte
g                       byte
b                       byte
                    end
h                   string(1)
  code
    if claColor < 0 or claColor > 0FFFFFFh                 ! Invalid color
        return ''
    end
    if addHash
        h = '#'
    end

    ! Flip the byte order, as Clarion stores the red value in the lowest order byte and not the highest
    return Clip(h) & self.ByteToHex(rgb.r) & self.ByteToHex(rgb.g) & self.ByteToHex(rgb.b)


! Returns a long that contains the color when passed the a string
! containing a hexidecimal representation of the RGB value.
! Supports string of the form: #FFFFFF or FFFFFF
! Returns -1 (COLOR:None) for invalid input
StringTheory.ColorFromHex Function(string hexCol)
claColor            long
rgb                 group,over(claColor)
r                       byte
g                       byte
b                       byte
                    end
  code
    if Len(Clip(hexCol)) < 6
        return -1
    end
    if hexCol[1] = '#'
        if Len(Clip(hexCol)) < 7
            return -1
        end
        hexCol = hexCol[2 : Len(hexCol)]
    end
    rgb.r = self.HexToByte(hexCol[1:2])
    rgb.g = self.HexToByte(hexCol[3:4])
    rgb.b = self.HexToByte(hexCol[5:6])

    return claColor


!-----------------------------------------------------------
! URL encode the stored string (also known as percent encoding)
! Encoding defined by RFC2396
StringTheory.UrlEncode Procedure (long flags=0,<String pDelimiter>,<String pSpace>)
mapping         string('0123456789ABCDEF')
x               long, auto
y               long, auto
Enc             StringTheory            ! The encoded value
slen            long
loc:delim  string(1)
loc:space  string(1)

  code
    slen = self.LengthA()
    if not slen
        return
    end
    Enc.SetLength(3 * self.LengthA())

    loc:delim = choose(omitted(2) or pDelimiter='','%',pDelimiter)
    loc:space = choose(omitted(3) or pSpace='','+',pSpace)

    y = 1
    loop x = 1 to slen
        case Val(self.value[x])
        of loc:delim orof loc:space
          Enc.value[y] = loc:delim
          Enc.value[y+1] = mapping[ Bshift(Band(Val(self.value[x]), 0F0h), -4) + 1 ]
          Enc.value[y+2] = mapping[ Band(Val(self.value[x]), 0Fh) + 1 ]
          y += 3
        of val('/') orof val('\') orof val('.') orof val('$')
            if Band(flags, st:dos)
                Enc.value[y] = self.value[x]
                y += 1
            else
                Enc.value[y] = loc:delim
                Enc.value[y+1] = mapping[ bshift(band(val(self.value[x]),0F0h),-4)+1 ]
                Enc.value[y+2] = mapping[ band(val(self.value[x]),0Fh)+1 ]
                y += 3
            end
        of 48 to 57                                         ! 0 to 9
        orof 65 to 90                                       ! A to Z
        orof 97 to 122                                      ! a to z
        orof Val('-')  orof Val('_') orof Val('.') orof Val('!') orof Val('.')   ! unreserved characters: - _ . ! ~ * ' ( )
        orof Val('~') orof Val('*') orof Val('''') orof Val('(') orof Val(')')
            Enc.value[y] = self.value[x]
            y += 1
        of 32                                               ! space
            if Band(flags, st:NoHex + st:NoPlus)
                Enc.value[y] = self.value[x]
            elsif Band(flags, st:BigPlus)
                Enc.value[y : y+2] = loc:delim & '20'
                y += 2
            else
                Enc.value[y] = loc:space
            end
            y += 1
        else
          Enc.value[y] = loc:delim
          Enc.value[y+1] = mapping[ Bshift(Band(Val(self.value[x]), 0F0h), -4) + 1 ]
          Enc.value[y+2] = mapping[ Band(Val(self.value[x]), 0Fh) + 1 ]
          y += 3
        end
    end

    Enc.Clip()
    self.Free(false)
    self.value &= Enc.value
    Enc.value &= null

!-----------------------------------------------------------
StringTheory.UrlDecode Procedure (<String pDelimiter>,<String pSpace>)
x               long
y               long
loc:delim   string(1)
loc:space   string(1)
cntr        long
  code
    if self.value &= null
        return
    end
    loc:delim = choose(omitted(2) or pDelimiter='','%',pDelimiter)
    loc:space = choose(omitted(3) or pSpace='','+',pSpace)

    self.Replace(loc:space, ' ')              ! turn '+' into ' ' (space)

    ! turn %ab into a single char.
    x = self.Instring(loc:delim)
    loop until x = 0
        case self.value[x+1]
        of '0' to '9'
            y = self.value[x+1] * 16
        of 'A' to 'F'
            y = (Val(self.value[x+1])-55) * 16
        end

        case self.value[x+2]
        of '0' to '9'
            y += self.value[x+2]
        of 'A' to 'F'
            y += val(self.value[x+2])-55
        end
        if x <= 1 and Len(self.value) < x+3
            self.value = Chr(y)
        elsif x <= 1
            self.value = Chr(y) & self.value[x+3 : Len(self.value)]
        elsif x+3 < self.LengthA()
            self.value = self.value[1 : x-1] & Chr(y) & self.value[x+3 : Len(self.value)]
        else
            self.value = self.value[1 : x-1] & Chr(y)
        end
        cntr += 2
        x = self.Instring(loc:delim, 1, x+1)
    end
    self.SetLength(self.LengthA()-cntr)

!-----------------------------------------------------------
!!! <summary>
!!!     Remove illegal characters from a file name. Note that this takes the file name only,
!!!     NOT the full path. If a full path is passed then all characters that are legal in a path,
!!!     but illegal in a file name (such as :, \ and /) will be replaced!
!!! <summary>
!!! <param name="fileName">The file name to create a clean version of</param>
!!! <param name="replaceChar">Optional parameter for the replacement character. Defaults to
!!!     an underscore</param>
!!! <returns>A string which contains the cleaned file name</returns>
StringTheory.CleanFileName     Procedure(<string pfileName>, <string pReplaceChar>)
i           long
cleanName   string(File:MaxFileName)
r           string(1)
  code
  if Omitted(3) or pReplaceChar = ''
    r = '_'
  else
    r = pReplaceChar[1]
  end
  if omitted(2) or pfileName = ''
    cleanName = self.GetValue()
  else
    cleanName = pfileName
  end
  loop i = 1 to Len(Clip(cleanName))
    case cleanName[i]
    of '\' orof '/' orof '*' orof ':' orof '?' orof '"' orof '<' orof '>' orof '|'
      cleanName[i] = r
    end
  end
  if omitted(2)
    self.SetValue(CleanName,st:clip)
  end
  return Clip(cleanName)

!-----------------------------------------------------------
StringTheory.PeekRam       Procedure(uLong pAdr,Long pLen)
dbv  string(1024)
b    byte
  code
  self.trace('PeekRam at ' & pAdr)
  loop x# = 1 to pLen
    peek(pAdr,b)
    pAdr += 1
    dbv = clip(dbv) & ' ' & b & '[' & chr(b) & ']'
  end
  self.trace('RAM:' & clip(dbv))

!-----------------------------------------------------------
StringTheory.Gzip       Procedure(Long pLevel=5)
result  long
gzstream  group(z_stream_s).
src       &string
srclen    Long
zversion  cstring(20)
dstlen    long
buffer    &string!(2000)
x         long

  code
  if self.gzipped then return st:Z_OK.
  self.loadlibs()
  if fp_DeflateInit2_ = 0 or fp_Deflate = 0 or fp_DeflateEnd = 0
    return st:Z_DLL_ERROR
  end

  ! store a pointer to the (currently) compressed string
  src &= self.value
  srcLen = self.LengthA()

  ! clear the official value field, and then give it some space to accept the decompression
  ! bypassing the normal allocation, because don't want to DISPOSE the old value yet.
  self.value &= Null
  !self._length = 0

  ! make a buffer of a suitable size.
  dstlen = srclen
  buffer &= new(string(dstlen))

  ! prepare the stream structure
  gzstream.next_in  = address(src)
  gzstream.avail_in = srclen
  gzstream.total_in = srclen
  gzstream.next_out   = address(buffer[1])
  gzstream.avail_out  = dstlen
  gzstream.data_type   = st:Z_UNKNOWN
  zversion = '1.2.5.0'

  ! init the stream structure
  result = stDeflateInit2_(address(gzstream), pLevel, st:Z_DEFLATED, (15+16), 9, st:Z_DEFAULT_STRATEGY,address(zversion),56)
  case result
  of st:Z_OK
    loop
      result = stDeflate(address(gzstream),st:Z_SYNC_FLUSH)
      case result
      of st:Z_OK
        if gzstream.avail_out = 0
          self.AppendA(buffer)
          gzstream.next_out   = address(buffer[1])
          gzstream.avail_out  = dstlen
          cycle
        else
          x = gzstream.total_out % dstlen
          if x = 0 then x = dstlen.
          self.AppendA(buffer[1:x])
          ! need to dispose the compressed version of the string now.
          dispose(src)
          result = st:Z_OK
          self.gzipped = 1
          break ! all finished
        end
      !orof st:Z_STREAM_ERROR
      !orof st:Z_BUF_ERROR
      ! failed, so restore pointer back the way it was
      else
        dispose(self.value)
        !self._length = srclen
        self.value &= src
      end
      break
    end
    x = stDeflateEnd(address(gzstream))
  else
    ! failed, so restore pointer back the way it was
    dispose(self.value)
    !self._length = srclen
    self.value &= src
  end
  dispose(buffer)
  return result

!-----------------------------------------------------------
StringTheory.Gunzip     Procedure()

result  long
gzstream  group(z_stream_s).
src       &string
srclen    Long
zversion  cstring(20)
dstlen    long
buffer    &string!(2000)
x         long
  code
  if not self.gzipped then return st:Z_OK.
  self.loadlibs()
  if fp_InflateInit2_ = 0 or fp_Inflate = 0 or fp_InflateEnd = 0
    return st:Z_DLL_ERROR
  end

  ! store a pointer to the (currently) compressed string
  src &= self.value
  srcLen = self.LengthA()

  ! clear the official value field, and then give it some space to accept the decompression
  ! bypassing the normal allocation, because don't want to DISPOSE the old value yet.
  self.value &= Null
  !self._length = 0

  ! make a buffer of a suitable size.
  dstlen = srclen * 4
  buffer &= new(string(dstlen))

  ! prepare the stream structure
  gzstream.next_in  = address(src)
  gzstream.avail_in = srclen
  gzstream.next_out   = address(buffer[1])
  gzstream.avail_out  = dstlen
  zversion = '1.2.5.0'

  ! init the stream structure
  result = stInflateInit2_(address(gzstream), 32+15,address(zversion),56)
  case result
  of st:Z_OK
    loop
      result = stInflate(address(gzstream),st:Z_SYNC_FLUSH)
      if result = st:Z_OK and gzstream.avail_in = 0 then result = st:Z_STREAM_END.
      case result
      of st:Z_OK
        self.AppendA(buffer)
        gzstream.next_out   = address(buffer[1])
        gzstream.avail_out  = dstlen
        cycle
      of st:Z_STREAM_END
        x = gzstream.total_out % dstlen
        if x = 0 then x = dstlen.
        self.AppendA(buffer[1:x])
        ! need to dispose the compressed version of the string now.
        dispose(src)
        result = st:Z_OK
        self.gzipped = 0
        break ! all finished
      !of st:Z_NEED_DICT
      !orof st:Z_DATA_ERROR
      !orof st:Z_STREAM_ERROR
      !orof st:Z_MEM_ERROR
      !orof st:Z_BUF_ERROR
      ! failed, so restore pointer back the way it was
      else
        dispose(self.value)
        !self._length = srclen
        self.value &= src
      end
      break
    end
    x = stInflateEnd(address(gzstream))
  else
    !of st:Z_MEM_ERROR
    !of st:Z_VERSION_ERROR
    !of st:Z_STREAM_ERROR
    ! failed, so restore pointer back the way it was
    dispose(self.value)
    !self._length = srclen
    self.value &= src
  end
  dispose(buffer)
  return result
!-----------------------------------------------------------
StringTheory.LoadLibs   Procedure()
cname   cstring(50)
  code
  if hZlib = 0
    cname = 'zlibwapi.dll'
    hZlib = stLoadLibrary(cname)
    if hZlib = 0                                             ! check library handle is not zero
      self.Trace('Failed to load '&clip(cname))
      return
    end

    ! Retrieve the required function pointers for the JobObject API
    cName = 'deflateInit2_'
    fp_DeflateInit2_  = stGetProcAddress(hZlib, cName)
    cName = 'deflate'
    fp_Deflate        = stGetProcAddress(hZlib, cName)
    cName = 'deflateEnd'
    fp_DeflateEnd     = stGetProcAddress(hZlib, cName)
    cName = 'inflateInit2_'
    fp_InflateInit2_  = stGetProcAddress(hZlib, cName)
    cName = 'inflate'
    fp_Inflate        = stGetProcAddress(hZlib, cName)
    cName = 'inflateEnd'
    fp_InflateEnd     = stGetProcAddress(hZlib, cName)
  end
!-----------------------------------------------------------
StringTheory.Left  Procedure(Long pLength=0,Long pwhat=1,<String pPad>)
x    long
pad  string(1)
  code
  if not omitted(4) then pad = pPad.
  if pLength = 0 and pWhat = st:Spaces and pPad = ''
    return left(self.value)
  elsif pWhat = st:Spaces and pPad = ''
    return left(self.value,pLength)
  else
    loop x = 1 to self.LengthA()
      case self.value[x]
      of ' '
        if band(pWhat,st:spaces) then cycle.
      of '<9>'
        if band(pWhat,st:tabs) then cycle.
      of '<13>'
        if band(pWhat,st:cr) then cycle.
      of '<10>'
        if band(pWhat,st:lf) then cycle.
      end
      break
    end
    ! x now = the first non-space character in the string
    if pLength = 0 then pLength = self.LengthA().
    if pLength <= self.LengthA()-x+1
      return sub(self.GetValue(),x,pLength)
    else
      return sub(self.GetValue(),x,pLength) & all(pad,x-1)
    end
  end
!-----------------------------------------------------------
StringTheory.SetLeft  Procedure(Long pLength=0,Long pwhat=1,<String pPad>)
pad  string(1)
  code
  if not omitted(4) then pad = pPad.
  self.SetValue(self.Left(pLength,pWhat,pad))

!-----------------------------------------------------------
StringTheory.Right  Procedure(Long pLength=0,Long pwhat=1,<String pPad>)
x    long
pad  string(1)
  code
  if not omitted(4) then pad = pPad.
  if pLength = 0 and pWhat = st:Spaces and pad = ''
    return right(self.value)
  elsif pWhat = st:Spaces and pad = ''
    return right(self.value,pLength)
  else
    loop x = self.LengthA() to 1 by -1
      case self.value[x]
      of ' '
        if band(pWhat,st:spaces) then cycle.
      of '<9>'
        if band(pWhat,st:tabs) then cycle.
      of '<13>'
        if band(pWhat,st:cr) then cycle.
      of '<10>'
        if band(pWhat,st:lf) then cycle.
      end
      break
    end
    x -= 1 ! x is now the rightmost character in the string
    if pLength = 0 then pLength = self.LengthA().
                if pLength = x
                        return sub(self.GetValue(),1,x)
                elsif pLength < x
                        return sub(self.GetValue(),x-pLength+1,pLength)
                elsif pLength > x
                        return all(pad,pLength-x) & sub(self.GetValue(),1,x)
                end
  end
!-----------------------------------------------------------
StringTheory.SetRight  Procedure(Long pLength=0,Long pwhat=1,<String pPad>)
pad  string(1)
  code
  if not omitted(4) then pad = pPad.
  self.SetValue(self.Right(pLength,pWhat,pad))
!-----------------------------------------------------------
StringTheory.SetAll  Procedure(Long pLength=255)
  code
  self.SetValue(All(self.GetValue(),pLength))

!-----------------------------------------------------------
StringTheory.All  Procedure(Long pLength=255)
  code
  return All(self.GetValue(),pLength)

!-----------------------------------------------------------
!The form is: "=?charset?encoding?encoded text?=".
! charset may be any character set registered with IANA. Typically it would be the same charset as the message body.
! encoding can be either "Q" denoting Q-encoding that is similar to the quoted-printable encoding, or "B" denoting base64 encoding.
! encoded text is the Q-encoded or base64-encoded text.
! An encoded-word may not be more than 75 characters long, including charset, encoding, encoded text, and delimiters. If it is desirable to encode more text than will fit in an encoded-word of 75 characters, multiple encoded-words (separated by CRLF SPACE) may be used.

StringTheory.EncodedWordDecode  Procedure()
s string(10)
str  stringtheory
res  stringtheory
x    long
  code
  if Left(self.value,1) = '=' and Right(self.value,1) = '='
    self.split('<13,10>')
    loop x = 1 to Self.records()
      str.SetValue(self.GetLine(x))
      str.split('?')
      str.DeleteLine(1)  ! leading =
      str.DeleteLine(1)  ! iso-8859-1
      str.DeleteLine(str.Records()) ! trailing =
      s = str.GetLine(1)
      if s = 'Q'
        str.DeleteLine(1)  ! S
        str.SetValue(str.getline(1))
        str.UrlDecode('=','_')
        res.AppendA(str.GetValue())
      elsif s ='B'
        str.DeleteLine(1) ! B
        str.SetValue(str.getline(1))
        str.Base64Decode()
        res.AppendA(str.GetValue())
      else
      end
    end
    self.SetValue(res.GetValue())
  End
!-----------------------------------------------------------
StringTheory.EncodedWordEncode  Procedure(<String pCharset>,long pEncoding=2)
  code
  case pEncoding
  of st:Base64
    self.Base64Encode()
    self.prepend('B?')
  else
    self.URLEncode(st:BigPlus,'=')
    self.prepend('Q?')
  end
  if omitted(2) or pCharset = ''
    self.prepend('=?iso-8859-1?')
  else
    self.prepend('=?' & clip(pCharset) & '?')
  end
  self.appendA('?=')

!-----------------------------------------------------------
StringTheory.MergeXml   Procedure(String pNew, Long pWhere)
pos  long
len  long
  code
  len = self.LengthA()
  case pWhere
  of st:first
    pos = self.Instring('>')
    if pos = 0
      self.SetValue(self.GetValue() & clip(pNew))
    else
      self.setvalue(self.slice(1,pos) & clip(pNew) & self.slice(pos+1,len))
    end
  of st:last
    pos = self.Instring('<',-1,len)
    if pos = 0 or pos = 1
      self.SetValue(self.GetValue() & clip(pNew))
    elsif pos > 1
      self.setvalue(self.slice(1,pos-1) & clip(pNew) & self.slice(pos,len))
    end
  end
  return
!-----------------------------------------------------------
StringTheory.LineEndings  Procedure(Long pEndings=1)
  code

  case pEndings
  of st:windows ! CRLF
    self.replace('<13,10>','<13>')
    self.replace('<10>','<13>')
    self.replace('<13>','<13,10>')
  of st:mac     ! CR
    self.replace('<13,10>','<13>')
    self.replace('<10>','<13>')
  of st:unix    ! LF
    self.replace('<13,10>','<10>')
    self.replace('<13>','<10>')
  end

!-----------------------------------------------------------
! consider if the string contains a human-formatted time.
! the following are detected as time.
! h:mm, hh:mm, hPM, hhAM, h:mmAM, hh:mmPM
! note that this method does not detect a lot of values that would be acceptable to the
! DeformatTime method. That method assumes the value IS a time, and so is not considering that
! it might just be a number. This method narrows the pattern considerably.

StringTheory.IsTime PROCEDURE (String pValue)
loc  StringTheory
  code
  loc.SetValue(pValue)
  return loc.IsTime()

StringTheory.IsTime PROCEDURE ()
  code
  if self.IsAll('0123456789: apm') = false then return false. ! simple exclusion for other chars.

  if self.instring(':') = 0 and self.instring('am',,,,true) = 0 and self.instring('pm',,,,true) = 0
    return false ! must contain at least one colon, or ap/pm sign.
  end
  return true


!-----------------------------------------------------------
! Formats supported.
!  [@][T][n][s][B][Z]
! @, T : optional.
! n : up to 3 digit number. If n is omitted defaults to 1.
!     Leading 0 indicates zero-filled hours (hh). Two leading zeros (hhh), Three leading zeros (hhhh).
!
!@T1       hh:mm        17:30
!@T2       hhmm         1730
!@T3       hh:mmXM      5:30PM
!@T03      hh:mmXM      05:30PM
!@T4       hh:mm:ss     17:30:00
!@T5       hhmmss       173000
!@T6       hh:mm:ssXM   5:30:00PM
!@T7                    Windows Control Panel setting for Short Time (*)
!@T8                    Windows Control Panel setting for Long Time  (*)
!@T91      hh:mm:ss:cc    17:30:00:19
!@T92      hh:mm:ss:ccXM  5:30:00:19pm

! s : separator. Can be _ ' . -
! B : set to blank if time is < 0. (also blank if time is 0 and Z not used).
! Z : time is formatted as "base 0"- in other words 0 = 0:00 and 360000 = 1:00
!
! (*) these formats do not support times > 23:59
!-----------------------------------------------------------
StringTheory.FormatTime PROCEDURE (String pFormat)
  code
  self.SetValue(self.FormatTime(self.GetValue(),pFormat))

StringTheory.FormatTime PROCEDURE (Long pValue,string pFormat)
ONEHOUR        Equate(360000)
ONEMINUTE      Equate(6000)
ONESECOND      Equate(100)
result         string(15)
h              long
m              long
s              long
cs             long
zero           long
fnum           long
sep            string(1)
blank          long
base           long
pic            string(6)
  code
  sep = ':'
  base = 1
  if sub(pFormat,1,1) = '@'
    pFormat = sub(pFormat,2,10)
  end
  if upper(sub(pFormat,1,1)) = 'T'
    pFormat = sub(pFormat,2,10)
  end
  loop 3 times
    if sub(pFormat,1,1) = '0'
      zero += 1
      pFormat = sub(pFormat,2,10)
    else
      break
    end
  end
  if sub(pFormat,1,1) >= '0' and sub(pFormat,1,1) <= '9' and sub(pFormat,2,1) >= '0' and sub(pFormat,2,1) <= '9'
    fnum = sub(pFormat,1,2)
    pFormat = sub(pFormat,3,10)
  elsif sub(pFormat,1,1) >= '0' and sub(pFormat,1,1) <= '9'
    fnum = sub(pFormat,1,1)
    pFormat = sub(pFormat,2,10)
  else
    fnum = 1
  end
  case sub(pFormat,1,1)
  of '-'
  orof '.'
  orof '_'
  orof ''''
    sep = sub(pFormat,1,1)
    pFormat = sub(pFormat,2,10)
    if sep = '_' then sep = ' '.
    if sep = '''' then sep = ','.
  end
  case sub(pFormat,1,1)
  of 'b' orof 'B'
    blank = 1
    pFormat = sub(pFormat,2,10)
  end
  case sub(pFormat,1,1)
  of 'z' orof 'Z'
    base = 0
    pFormat = sub(pFormat,2,10)
  end
  if pValue > 0
    h = (pValue-base) / ONEHOUR
    m = ((pValue-base) % ONEHOUR) / ONEMINUTE
    s = ((pValue-base) % ONEMINUTE) / ONESECOND
    cs = (pValue-base) % ONESECOND
  end

  if blank and h = 0 and m = 0 and s = 0 and cs = 0
    result = ''
  else
    pic = choose(zero=0,'@n4','@n0' & zero+1)
    case fnum
    of 1  ! hh:mm
      result = format(h,pic) & sep & format(m,'@n02')
    of 2  ! hhmm
      result = format(h,pic) & format(m,'@n02')
    of 3  ! hh:mmXM
      result = format(choose(h=12,12,h%12),pic) & sep & format(m,'@n02') & choose(h <12,'am','pm')
    of 4  ! hh:mm:ss
      result = format(h,pic) & sep & format(m,'@n02') & sep & format(s,'@n02')
    of 5  ! hhmmss
      result = format(h,pic) & format(m,'@n02') & format(s,'@n02')
    of 6  ! hh:mm:ssXM
      result = format(choose(h=12,12,h%12),pic) & sep & format(m,'@n02') & sep & format(s,'@n02') & choose(h <12,'am','pm')
    of 7  ! windows short
      result = choose(base=1,format(pValue,'@t7'&sep&choose(blank=1,'B','')),format(pValue+1,'@t7'&sep&choose(blank=1,'B','')))
    of 8  ! windows long
      result = choose(base=1,format(pValue,'@t8'&sep&choose(blank=1,'B','')),format(pValue+1,'@t8'&sep&choose(blank=1,'B','')))
    of 91 ! hh:mm:ss:cc
      result = format(h,pic) & sep & format(m,'@n02') & sep & format(s,'@n02') & sep &  format(cs,'@n02')
    of 92 ! hh:mm:ss:ccXM
      result = format(choose(h=12,12,h%12),pic) & sep & format(m,'@n02') & sep & format(s,'@n02') & sep &  format(cs,'@n02') & choose(h <12,'am','pm')
    end
  end
  return result

!-----------------------------------------------------------
! If a value is passed to the method, then the deformat of the value is returned,
! but the contents of the existing string is not changed.
!-----------------------------------------------------------
StringTheory.DeformatTime PROCEDURE (String pValue)
loc  StringTheory
  code
  loc.SetValue(pValue)
  loc.DeformatTime()
  return loc.GetValue()

!-----------------------------------------------------------
! deformats the contents of the string into standard clarion time format.
! pic is not needed because the format is unambiguous.
! result is placed in the string.
! if the contents are not recognised as a time, then the string is unchanged.
!-----------------------------------------------------------
StringTheory.DeformatTime PROCEDURE ()

ONEHOUR        Equate(360000)
HOURS12        Equate(ONEHOUR*12)
ONEMINUTE      Equate(6000)
ONESECOND      Equate(100)
ReturnValue    Long
part           string(20),dim(5)
am             long
pm             long
  code
  do parts
  if part[4]<>'' or part[3]<>'' or part[2]<>''
    if part[4] = 0 then part[4] = 1.
    returnvalue = (part[1] * ONEHOUR) + (part[2] * ONEMINUTE) + (part[3] * ONESECOND) + part[4]
  else
    case len(clip(part[1]))
    of 1   !h
    orof 2 !hh
      returnvalue = part[1] * ONEHOUR + 1
    of 3 ! hmm
      returnvalue = sub(part[1],1,1) * ONEHOUR + sub(part[1],2,2) * ONEMINUTE + 1
    of 4 !hhmm
      returnvalue = sub(part[1],1,2) * ONEHOUR + sub(part[1],3,2) * ONEMINUTE + 1
    of 5 ! hmmss
      returnvalue = sub(part[1],1,1) * ONEHOUR + sub(part[1],2,2) * ONEMINUTE + sub(part[1],4,2) * ONESECOND + 1
    of 6 ! hhmmss
      returnvalue = sub(part[1],1,2) * ONEHOUR + sub(part[1],3,2) * ONEMINUTE + sub(part[1],5,2) * ONESECOND + 1
    of 7 ! hmmsscc
      returnvalue = sub(part[1],1,1) * ONEHOUR + sub(part[1],2,2) * ONEMINUTE + sub(part[1],4,2) * ONESECOND + sub(part[1],6,2)
    of 8 ! hhmmsscc
      returnvalue = sub(part[1],1,2) * ONEHOUR + sub(part[1],3,2) * ONEMINUTE + sub(part[1],5,2) * ONESECOND + sub(part[1],7,2)
    end
  end
  if pm and returnvalue < HOURS12 ! still am
    returnvalue += HOURS12 ! + 12 hours
  elsif am and returnvalue >= HOURS12
    returnvalue -= HOURS12 ! - 12 hours
  end
  if ReturnValue > 0
    self.setvalue(ReturnValue)
  end

parts  routine
  data
x  long
p  long
  code
  x = self.instring('a',,,,true)
  if x > 0
    am = 1
  Else
    x = self.instring('p',,,,true)
    if x > 0
      pm = 1
    end
  End

  p = 1
  self.setleft()
  loop x = 1 to self.lengthA()
    case self.value[x]
    of '0' to '9'
      part[p] = clip(part[p]) & self.value[x]
      if len(clip(part[p])) = 2
        p+=1
        if p > maximum(part,1) then break.
      end
    else
      if part[p] <> ''
        p += 1
        if p > maximum(part,1) then break.
      end
    end
  end
!-----------------------------------------------------------
StringTheory.startsWith    Procedure (String pStr,Long pCase=True)
x  long
  code
  x = len(clip(pStr))
  if x = 0 then return true.
  if pCase
    if self.Left(x) = pStr then return true.
  else
    if lower(self.Left(x)) = lower(pStr) then return true.
  end
  return false
!-----------------------------------------------------------
StringTheory.EndsWith    Procedure (String pStr,Long pCase=True)
x  long
  code
  x = len(clip(pStr))
  if x = 0 then return true.
  if pCase
    if self.Right(x) = pStr then return true.
  else
    if lower(self.Right(x)) = lower(pStr) then return true.
  end
  return false
!-----------------------------------------------------------


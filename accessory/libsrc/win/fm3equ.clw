
   !---- Equates for the callback procedure
FM3equ:OldFiles                    equate(1)
FM3equ:UpgradingFile               equate(2)
FM3equ:CopyingRecord               equate(3)
FM3equ:UpgradingFileDetailsString  equate(4)
FM3equ:EndUpgradeFile              equate(5)

FM3:UFDGroupType group,type
FromPrefix        string(16)
ToPrefix          string(16)
FromVersionNumber long
ToVersionNumber   long
FromLabel         string(252)
                end
!ABCIncludeFile(NETTALK)
! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2012 CapeSoft Software  - www.capesoft.com

  include ('NetSmppEq.inc'), once
  include ('StringTheory.inc'), once    

session_sequence    long                                    ! Global session sequence number

!-----------------------------------------------------------
!
!   Utility classes for SMPP addresses, dates and times etc.
!
!-----------------------------------------------------------

!-----------------------------------------------------------
SmppAddress         Class(), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
ton                     long
npi                     long
addr                    cstring(256)

Init                    Procedure (long adrton, long adrnpi, string addr)
GetLength               Procedure (), long, virtual
Equals                  Procedure (*SmppAddress rhs), virtual
                    end


!-----------------------------------------------------------
SmppDate            Class(), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
isNull                  bool
m_time                  group(NET_SYSTEMTIME)
                        end

Construct               Procedure ()

SetDate                 Procedure (*cstring sd), virtual
SetDate                 Procedure (*NET_SYSTEMTIME sd), virtual
ToString                Procedure (), string, virtual
Equals                  Procedure (*SmppDate rhs), virtual
SetToNull               Procedure (), virtual
GetLength               Procedure (), long, virtual
                    end


!-----------------------------------------------------------
! Core class containing general methods
!-----------------------------------------------------------

SmppCore            Class(), type, Module('NetSmppBase.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Log                     Procedure (string msg), virtual
                    end


!-----------------------------------------------------------
! A message that contains a UDH (User Data Header) indicating
! a particular format. Currently the class supports UDH messages
! for concatenated text messages.
!-----------------------------------------------------------

UdhMessage          Class(SmppCore), type, Module('NetSmppBase.clw'), Link('NetSmppBase.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
udhl                    long                                ! Length of the User Data Header (in octets)

iei                     long                                ! Information Element Identifier (the type of UDH that this is)
iel                     long                                ! Information Element Length (number of UDH bytes that follow)

refNumber               long                                ! Reference number (the same for all message parts for a specific message)
totalParts              long                                ! Total number of parts
currentPart             long                                ! The number of this part

message_text            &StringTheory                       ! The message text

Construct               Procedure ()
Destruct                Procedure ()
Load                    Procedure (*MessagePacketBase pMsg), bool, proc, virtual
GetUdhMessage           Procedure (), string, virtual
Reset                   Procedure (), virtual

ToString                Procedure (), string, virtual
                    end

!-----------------------------------------------------------
!           BInt - Big Endian Integer conversion
!  Provides Methods for reading and writing integers in Big 
!      Endian byte order to and from a buffer (string)
!-----------------------------------------------------------

BInt                Class() type, Module('NetSmpp.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
WriteInt                Procedure (*string pby, long ival, long offset=0), bool, proc, virtual
ReadInt                 Procedure (*string pby, long offset=0), long, virtual
                    end


!-----------------------------------------------------------
!
! Packet base classes
!
!-----------------------------------------------------------

!-----------------------------------------------------------
PacketBase          Class(SmppCore), type, Module('NetSmppBase.clw'), Link('NetSmppBase.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
command_length          long
command_id              long
command_status          long
sequence_number         long
buffer                  &StringTheory

Construct               Procedure ()
Destruct                Procedure ()

NextSequenceNumber      Procedure (), long, virtual
GetHeaderLength         Procedure (), long, virtual
EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, proc, virtual
GetCommandLength        Procedure (), long, virtual

WriteAddress            Procedure (*SmppAddress addr), virtual
WriteByte               Procedure (long ival), virtual
WriteNull               Procedure (), virtual

ReadString              Procedure (*string pby, *long pos, *cstring dest_field), virtual
ReadString              Procedure (*string pby, *long pos, *StringTheory dest_field, ulong fLen=0), virtual

ReadByte                Procedure (*string pby, *long pos, *long dest_field), virtual
ReadAddress             Procedure (*string pby, *long pos, *SmppAddress dest_addr), virtual

ToString                Procedure (), string, virtual       ! Prints the object properties as a human readable string
                    end


!-----------------------------------------------------------                    
BindPacketBase      Class(PacketBase), type, Module('NetSmppBase.clw'), Link('NetSmppBase.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
system_id               cstring(256)
password                cstring(256)
system_type             cstring(256)
interface_version       long
address_range           &SmppAddress

Construct               Procedure ()
Destruct                Procedure ()

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, proc, virtual
GetCommandLength        Procedure (), long, virtual
SetSourceRange          Procedure (*SmppAddress addr)

ToString                Procedure (), string, virtual       ! Prints the object properties as a human readable string
                    end


!-----------------------------------------------------------
MessagePacketBase   Class(PacketBase), type, Module('NetSmppBase.clw'), Link('NetSmppBase.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
service_type            cstring(256)

source                  &SmppAddress
destination             &SmppAddress

esm_class               long
protocol_id             long
priority_flag           long

scheduled_delivery      &SmppDate
validity_period         &SmppDate

registered_delivery     long
replace_if_present      long

data_coding             long
default_msg_id          long

message                 &StringTheory

Construct               Procedure ()
Destruct                Procedure ()

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, proc, virtual
GetCommandLength        Procedure (), long, virtual
GetMessage              Procedure (*string msg, *long nsz), virtual

SetMessage              Procedure (string msg, long nsz=0), virtual
SetDestination          Procedure (*SmppAddress dst), virtual
SetSource               Procedure (*SmppAddress src), virtual
SetDataCoding           Procedure (long data_coding), virtual
SetEsmClass             Procedure (long esm_class), virtual

SetEsmClassAttrib       Procedure (long bVal, long attrib), virtual
GetEsmClassAttrib       Procedure (long attrib), long, virtual

EsmAttribToString       Procedure(long attrib, long attrib_val), string, virtual
EsmClassToString        Procedure(), string, virtual

DataCodingToString      Procedure(), string, virtual

IsUdh                   Procedure (), bool, virtual

ToString                Procedure (), string, virtual
                    end


!-----------------------------------------------------------
DataPacketBase      Class(PacketBase), type, Module('NetSmppBase.clw'), Link('NetSmppBase.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
service_type            cstring(256)

source                  &SmppAddress
destination             &SmppAddress

esm_class               long
registered_delivery     long
data_coding             long

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, proc, virtual
GetCommandLength        Procedure (), long, virtual

ToString                Procedure (), string, virtual
                    end


!-----------------------------------------------------------
DataPacketRespBase  Class(PacketBase), type, Module('NetSmppBase.clw'), Link('NetSmppBase.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
message_id              cstring(256)

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, proc, virtual
GetCommandLength        Procedure (), long, virtual

ToString                Procedure (), string, virtual
                    end


!-----------------------------------------------------------
BindRespBase       Class(PacketBase), type, Module('NetSmppBase.clw'), Link('NetSmppBase.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
system_id               cstring(256)

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, proc, virtual
GetCommandLength        Procedure (), long, virtual

ToString                Procedure (), string, virtual
                    end



!-----------------------------------------------------------
MessageRespBase     Class(PacketBase), type, Module('NetSmppBase.clw'), Link('NetSmppBase.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
message_id              cstring(256)

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, proc, virtual
GetCommandLength        Procedure (), long, virtual

ToString                Procedure (), string, virtual
                    end
! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2012 CapeSoft Software  - www.capesoft.com


      Member()
         omit('***$***',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
         ***$***
      Include('Equates.CLW'),ONCE
      Include('Keycodes.CLW'),ONCE
      Include('Errors.CLW'),ONCE
      Map

  module('WinAPI_CriticalSection')
    COMPILE('***',_WIDTH32_)
      NETOS_InitializeCriticalSection(*Net:CriticalSectionType),PASCAL,RAW,NAME('InitializeCriticalSection'),Dll(dll_mode)
      NETOS_EnterCriticalSection(*Net:CriticalSectionType),PASCAL,RAW,NAME('EnterCriticalSection'),Dll(dll_mode)
      NETOS_LeaveCriticalSection(*Net:CriticalSectionType),PASCAL,RAW,NAME('LeaveCriticalSection'),Dll(dll_mode)
      NETOS_DeleteCriticalSection(*Net:CriticalSectionType),PASCAL,RAW,NAME('DeleteCriticalSection'),Dll(dll_mode)
    ****
  end
! Win32 API Declarations
    module('NetCrit using Windows API')
      NetCrit_OS_OutputDebugString(long lp_CStr),raw,pascal,name('OutputDebugStringA'),Dll(dll_mode)
      NetCrit_OS_ShellExecute(long,long,long,long,long,LONG),long,PASCAL,RAW,NAME('ShellExecuteA'),DLL(dll_mode) ! 20/7/2005 was (ulong,ulong,ulong,ulong,ulong,LONG),ULONG
    end
      End ! map
      Include('NetCrit.inc'),ONCE
! NetCritical Shared Data
  compile ('****', NetCritLog=1)
NetCritThreadedCount         long,thread
NetCritCheckLoggedOnce       long

NetCriticalSectionChecker    _NetCriticalSectionCheck,thread ! Class
  ****
_NetCriticalSection.Construct PROCEDURE  ()                ! Declare Procedure 3
  CODE
  if self.Initialized = 0
    self.Initialized = 1
    NETOS_InitializeCriticalSection(self.NetCriticalSectionData)
  end

  compile ('****', NetCritLog=1)
    self.LoggingOn = true
  ****

  compile ('****', NetCritLog=1)
    if self.LoggingOn
      self.Log ('[' & thread() & '] Construct Critical Section')
    end
  ****

!------------------------------------------------------------------------------
_NetCriticalSection.Destruct PROCEDURE  ()                 ! Declare Procedure 3
  CODE
  NETOS_DeleteCriticalSection(self.NetCriticalSectionData)

  compile ('****', NetCritLog=1)
    if self.LoggingOn
      self.Log ('[' & thread() & '] Destruct Critical Section')
    end
  ****
!------------------------------------------------------------------------------
_NetCriticalSection.Wait PROCEDURE  (long p_Count=-50)     ! Declare Procedure 3
  CODE
  compile ('****', NetCritLog=1)
    if self.LoggingOn
      self.Log ('[' & thread() & '] About to try Enter Critical Section (' & p_Count & ')')
    end
  ****

  if self.Initialized = 1
    NETOS_EnterCriticalSection(self.NetCriticalSectionData)
  else
    self.Construct() ! Make sure construct is called before Wait - sounds silly - but it's possible with global data from non-DLL code to use the object defined in NeTTalk DLL before it's constructor has fired.
    if self.Initialized = 1
      NETOS_EnterCriticalSection(self.NetCriticalSectionData)
    end
  end

  compile ('****', NetCritLog=1)
    NetCritThreadedCount += 1
  ****

  compile ('****', NetCritLog=1)
    if self.LoggingOn
      self.Log ('[' & thread() & '] Enter Critical Section  (' & p_Count & ') *** ThreadedCount = ' & NetCritThreadedCount)
    end
  ****
!------------------------------------------------------------------------------
_NetCriticalSection.Release PROCEDURE  (long p_Count=-51)  ! Declare Procedure 3
  CODE
  compile ('****', NetCritLog=1)
    NetCritThreadedCount -= 1
  ****

  compile ('****', NetCritLog=1)
    if self.LoggingOn
      self.Log ('[' & thread() & '] About to Release Critical Section  (' & p_Count & ') *** [' & Thread() & '] ThreadedCount = ' & NetCritThreadedCount)
    end
  ****

  if self.Initialized = 1
    NETOS_LeaveCriticalSection(self.NetCriticalSectionData)
  end

  compile ('****', NetCritLog=1)
    if self.LoggingOn
      self.Log ('[' & thread() & '] Release Critical Section (' & p_Count & ')')
    end
  ****
!------------------------------------------------------------------------------
_NetCriticalSection.Log PROCEDURE  (string p_Text)         ! Declare Procedure 3
  compile ('****', NetCritLog=1)
SHORT_STRING_LEN     equate (256)
ShortCStr            cstring (SHORT_STRING_LEN),auto
LongCStr             &cstring,auto
ls                   long,auto
  ****
  CODE
  compile ('****', NetCritLog=1)
    if self.LoggingOn = 0
      return
    end

    ls = len (p_Text) + 21
    if ls < SHORT_STRING_LEN ! < because we need 1 extra char for cstring
      ShortCStr = '[NetCriticalSection] ' & p_Text
      NetCrit_OS_OutputDebugString (Address(ShortCStr))
    else
      LongCStr &= new(cstring(ls+1))
      if LongCStr &= NULL
        Stop ('Can''t allocate memory to display LOG file')
        Stop ('[NetCriticalSection] ' & p_Text)
      else
        LongCStr = '[NetCriticalSection] ' & p_Text
        NetCrit_OS_OutputDebugString (Address(LongCStr))
        dispose (LongCStr)
      end
    end
  ****

!------------------------------------------------------------------------------
_NetCriticalSectionCheck.Construct PROCEDURE  ()           ! Declare Procedure 3
  CODE
  compile ('****', NetCritLog=1)
    self.LoggingOn = true
    if self.LoggingOn
      self.Log ('[' & thread() & '] _NetCriticalSectionCheck: Construct Check')
    end
  ****
!------------------------------------------------------------------------------
_NetCriticalSectionCheck.Destruct PROCEDURE  ()            ! Declare Procedure 3
  compile ('****', NetCritLog=1)
MyRun            cstring(256),auto
MyParameters     cstring(256),auto
MyRet            long,auto
  ****
  CODE
  compile ('****', NetCritLog=1)
    if self.LoggingOn
      self.Log ('[' & thread() & '] _NetCriticalSectionCheck: Destruct Check ' & NetCritThreadedCount)
    end
    if NetCritThreadedCount <> 0
      if NetCritCheckLoggedOnce = 0
        NetCritCheckLoggedOnce = 1
        self.Log ('[' & thread() & '] _NetCriticalSectionCheck: Error !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Destruct Check ' & NetCritThreadedCount)
        if self.LoggingOn and self._UseBlat
          !MyRun = 'blat'
          !MyParameters = ' -to someone@example.com -subject "Critical Section Fail at ' & format (clock(),@T4) & '" -body "[' & Thread() & '] Error !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Destruct Check ' & NetCritThreadedCount & '"'
          !MyRet = NETCRIT_OS_ShellExecute(0{prop:clienthandle}, 0, address(MyRun), address(MyParameters), 0, 1)
        end
      end
    end
  ****
!------------------------------------------------------------------------------
_NetCriticalSectionCheck.Log PROCEDURE  (string p_Text)    ! Declare Procedure 3
  compile ('****', NetCritLog=1)
SHORT_STRING_LEN     equate (256)
ShortCStr            cstring (SHORT_STRING_LEN),auto
LongCStr             &cstring,auto
ls                   long,auto
  ****
  CODE
  compile ('****', NetCritLog=1)
    if self.LoggingOn = 0
      return
    end

    ls = len (p_Text) + 21
    if ls < SHORT_STRING_LEN ! < because we need 1 extra char for cstring
      ShortCStr = '[NetCriticalSection] ' & p_Text
      NetCrit_OS_OutputDebugString (Address(ShortCStr))
    else
      LongCStr &= new(cstring(ls+1))
      if LongCStr &= NULL
        Stop ('Can''t allocate memory to display LOG file')
        Stop ('[NetCriticalSection] ' & p_Text)
      else
        LongCStr = '[NetCriticalSection] ' & p_Text
        NetCrit_OS_OutputDebugString (Address(LongCStr))
        dispose (LongCStr)
      end
    end
  ****

!------------------------------------------------------------------------------

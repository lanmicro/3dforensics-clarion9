[Messages]
M1=Poss�vel mudan�a de arquivo sem altera��o no n�mero da vers�o do arquivo. Arquivo :
M2=Imposs�vel abrir o arquivo UPG : causa
M3=Imposs�vel atualizar arquivo que n�o tenha um prefixo:
M4=Imposs�vel atualizar arquivo - Arquivo n�o "utiliz�vel"
M5=Imposs�vel atualizar arquivo - Nome do arquivo n�o dispon�vel
M6=Imposs�vel atualizar arquivo - Erro ao criar arquivo de destino
M7=Imposs�vel atualizar arquivo - Erro lendo arquivo fonte
M8=Erro escrevendo arquivo de destino
M9=Imposs�vel identificar driver - Utilize ds_AddDriver para registrar drivers.
M10=CUIDADO - Driver n�o localizado - GPF
M12=Por Favor Aguarde. Atualizando Arquivo.
M13=Arquivo MEM faltando
M14=Formato interno do arquivo muito grande. Arquivo :
M15=Imposs�vel Atualizar. Acesso ao arquivo existente danificado.
M16=Por Favor Aguarde - Copiando Arquivo
M17=Por Favor Aguarde - Construindo chaves
M18=Por Favor Aguarde - Reconstruindo Arquivo
M19=Arquivo Corrompido. Reinicie programa para carregar AutoFix.

[AutoNet]
M1=Upgrading program files from the network...
M2=Please be patient....
M3=Initializing :
M4=Copying :

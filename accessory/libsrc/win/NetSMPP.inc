!ABCIncludeFile(NETTALKST)
! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software  - www.capesoft.com

  include('NetAll.inc'), once
  include('NetSimp.inc'), once
  include('NetSmppBase.inc'), once

!-----------------------------------------------------------
!
!     ESME Classes (transmitter, receiver and tranceiver)
! These classes handle communication between the ESME (the app
!          and SMSC (Short Message Service Center)
!
!-----------------------------------------------------------

! Base class
SmppConnection      Class(NetSimple), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
system_id               cstring(256)
password                cstring(256)
system_type             cstring(256)
address_range           &SmppAddress
server                  cstring(256)
port                    ushort

refNumber               long                ! For UDH concatenated SMS messages, the current set reference number

Construct               Procedure ()
Destruct                Procedure ()

Init                    Procedure (uLong mode=NET:SimpleClient), virtual

Open                    Procedure (string Server,uShort Port=0) ,virtual

DoBind                  Procedure (string sysid, string password, string systype, string addr, ushort npi=0, ushort ton=0), virtual
DoBind                  Procedure (string sysid, string password, string systype, *SmppAddress addr_range), virtual
DoUnbind                Procedure (), virtual
EnquireLink             Procedure (), virtual

ProcessPacket           Procedure (*PacketBase pak), virtual  ! Callback

SendPacket              Procedure (*PacketBase pak), virtual
Send                    Procedure (*string data, long dataLen=0), virtual

Process                 Procedure (), virtual
_ParsePacket            Procedure (*string pby, long nsz), virtual

_CloseIdleConnection    Procedure(), virtual

ErrorText               Procedure(long command_status), string, virtual
PacketType              Procedure(long pt), string, virtual
                    end



NetSmppReceiver     Class(SmppConnection), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
DoBind                  Procedure (string sysid, string password, string systype, *SmppAddress src_range), virtual
LinkListen              Procedure (), bool, virtual        ! Outbind not support in this version (requires a listening connection)

ProcessPacket           Procedure (*PacketBase pak), virtual  ! Callback

_ParsePacket            Procedure (*string pby, long nsz), virtual
                    end


NetSmppTransmitter  Class(SmppConnection), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
DoBind                  Procedure (string sysid, string password, string systype, *SmppAddress src_range), virtual

SubmitMessage           Procedure (*SubmitSM pak), long, proc, virtual
SubmitMessage           Procedure (*cstring msg, string dst, long ton, long npi), long, proc, virtual
SubmitMessage           Procedure (*cstring msg, *SmppAddress dst), long, proc, virtual
SubmitMessage           Procedure (*string msg, long msglen, long enc, *SmppAddress dest, long esm=0), long, proc, virtual

ProcessPacket           Procedure (*PacketBase pak), virtual  ! Callback

_ParsePacket            Procedure (*string pby, long nsz), virtual
                    end


NetSmppTransceiver  Class(SmppConnection), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
DoBind                  Procedure (string sysid, string password, string systype, *SmppAddress addr_range), virtual

SubmitMessage           Procedure (*SubmitSM pak), long, proc, virtual
SubmitMessage           Procedure (*cstring msg, string dst, long ton, long npi), long, proc, virtual
SubmitMessage           Procedure (*cstring msg, *SmppAddress dst), long, proc, virtual
SubmitMessage           Procedure (*string msg, long msglen=0, long enc, *SmppAddress dest, long esm=0), long, proc, virtual

ProcessPacket           Procedure (*PacketBase pak), virtual  ! Callback

_ParsePacket            Procedure (*string pby, long nsz), virtual
                    end



!-----------------------------------------------------------
!
! Packet classes
!
!-----------------------------------------------------------

! Special packet for link close
LinkClose           Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
EncodeBody              Procedure (*StringTheory pby), virtual
                    end


BindTransmitter     Class(BindPacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
                    end


BindReceiver        Class(BindPacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
                    end


BindTransceiver     Class(BindPacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
                    end


OutBind             Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
system_id               cstring(256)
password                cstring(256)

Construct               Procedure ()

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, virtual
GetCommandLength        Procedure (), long, virtual
                    end


QuerySM             Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
message_id              cstring(256)
source                  &SmppAddress

Construct               Procedure ()
Destruct                Procedure ()

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, virtual
GetCommandLength        Procedure (), long, virtual
ToString                Procedure (), string, virtual       ! Prints the object properties as a human readable string
                    end


CUnbind             Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
EncodeBody              Procedure (*StringTheory pby), virtual
                    end


SubmitSM            Class(MessagePacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
                    end

DeliverSM           Class(MessagePacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
                    end


DataSM              Class(DataPacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
                    end


BindTransmitterResp Class(BindRespBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
Init                    Procedure(*BindTransmitter pak), virtual
                    end


BindReceiverResp    Class(BindRespBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
Init                    Procedure(*BindReceiver pak), virtual
                    end


BindTransceiverResp Class(BindRespBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
Init                    Procedure(*BindTransceiver pak), virtual
                    end


QuerySMResp         Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
message_id              cstring(256)
final_date              &SmppDate
message_state           long
error_code              long

Construct               Procedure ()
Init                    Procedure (*QuerySM pak), virtual

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, virtual
GetCommandLength        Procedure (), long, virtual
ToString                Procedure (), string, virtual
                    end


SubmitSMResp        Class(MessageRespBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
Init                    Procedure(*SubmitSM pak), virtual
                    end


DeliverSMResp       Class(MessageRespBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
Init                    Procedure(*DeliverSM pak), virtual
                    end


UnbindResp          Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
Init                    Procedure(*CUnbind pak), virtual
EncodeBody              Procedure (*StringTheory pby), virtual
                    end


EnquireLink         Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
EncodeBody              Procedure (*StringTheory pby), virtual
                    end


EnquireLinkResp     Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
Init                    Procedure (long seqNo), virtual
EncodeBody              Procedure (*StringTheory pby), virtual
                    end


DataSMResp          Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
Init                    Procedure(*DataSM pak), virtual
                    end


GenericNack         Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
Construct               Procedure ()
EncodeBody              Procedure (*StringTheory pby), virtual
                    end


AlertNotification   Class(PacketBase), type, Module('NetSMPP.clw'), Link('NetSMPP.clw', _ABCLinkMode_), Dll(_ABCDllMode_)
source                  &SmppAddress
esme                    &SmppAddress

Construct               Procedure ()
Destruct                Procedure ()

EncodeBody              Procedure (*StringTheory pby), virtual
LoadPacket              Procedure (*string pby, long nsz), bool, virtual
GetCommandLength        Procedure (), long, virtual
                    end

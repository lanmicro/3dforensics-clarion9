                MEMBER()
! (C) copyright 1996-2003 ProDomus, Inc.
! ALL RIGHTS RESERVED

! Applies library to ABC chain.  Derived from PDLUCT (PDLU.INC,CLW)

                INCLUDE('PDABLU.INC'),ONCE
                INCLUDE('PDLU.INC'),ONCE
                INClUDE('PDLU.TRN'),ONCE
                INCLUDE('ABWINDOW.INC'),ONCE

!------------------------------------------------------------------------------
PDLookupCT.AddItem PROCEDURE(SIGNED pAddEditFeq,BYTE pAddEditAskProcedure)
!------------------------------------------------------------------------------
  CODE
  ASSERT(pAddEditFeq,'No AddEditFeq identified')
  ASSERT(pAddEditAskProcedure,'No AddEditAskProcedure identified')
  SELF.AddEditFeq=pAddEditFeq
  SELF.AddEditAskProcedure=pAddEditAskProcedure
!------------------------------------------------------------------------------
PDLookupCT.Init PROCEDURE(WindowManager pWM,<*?  pScreenField>,*? pLookup,VIEW pLookupView,FileManager pLookupFile,KEY pLookupKey,<*? pIDToLookup>,<*? pIDToSave>,<KEY pIDKey>,<STRING pFilter>,<ULONG pFlags>,<SIGNED pButtonFEQ>,SIGNED pFeq,BYTE pAskProcedure=0)
!------------------------------------------------------------------------------
Parm ITEMIZE(2)
eWM                   EQUATE
eScreenField          EQUATE
eLookup               EQUATE
eLookupView           EQUATE
eLookupFile           EQUATE
eLookupKey            EQUATE
eIDToLookup           EQUATE
eIDToSave             EQUATE
eIDKey                EQUATE
eFilter               EQUATE
eFlags                EQUATE
eButtonFEQ            EQUATE
eFeq                  EQUATE
     END
  CODE
  SELF.WM &= pWM
  ASSERT(NOT SELF.WM &= NULL,'Window Manager is Null')
  SELF.WM.AddItem(SELF.WindowComponent)
  SELF.AskProcedure=pAskProcedure
  SELF.LookupMgr &= pLookupFile
  SELF.File &= pLookupFile.File                                      !-- <<>> 7/6/03 - Set file property
  SELF.LookupMgr.UseFile()                                           !-- <<>> 6/25/03 - Be sure file is open.
  SELF.InitServer(SELF.WM.Request,pLookup,pLookupView,pLookupKey,pFeq)
  SELF.ButtonFEQ=pButtonFEQ
  IF NOT OMITTED(eScreenField)
    SELF.ScreenField &= pScreenField
  END
  IF OMITTED(eIDToLookup) OR OMITTED(eIDToSave) OR OMITTED(eIDKey)
    SELF.StuffSysId=FALSE
  ELSE
    SELF.InitID(pIDToLookup,pIDToSave,pIDKey)
  END
  IF OMITTED(eFlags)
    SELF.IsRequired=1
    SELF.SetFlags(pFlags)                                            !-- <<>> 10/28/04 - Add
  ELSE
    SELF.IsRequired =CHOOSE(~BAND(pFlags,LU_NotRequired),1,0)
    SELF.SetFlags(pFlags)
  END
  IF NOT OMITTED(eFilter)
    SELF.SetFilter(pFilter)
  END
!----------------------------------------------------------------------------
PDLookupCT.CallLookup PROCEDURE !,BYTE,DERIVED
!----------------------------------------------------------------------------
RV BYTE
  CODE
  IF SELF.AskProcedure
    SELF.DebugOut('Begin','CallLookup(ABC)')
    RV = SELF.WM.Run(SELF.AskProcedure,SelectRecord)
    SELF.DebugOut('End','CallLookup(ABC)')
    RETURN RV
  ELSE
    RETURN 0
  END
!----------------------------------------------------------------------------
PDLookupCT.CallAddEdit PROCEDURE(BYTE pAction) !,BYTE,DERIVED
!----------------------------------------------------------------------------
RV BYTE
  CODE
  IF SELF.AddEditAskProcedure AND pAction
    SELF.DebugOut('Begin','CallAddEdit(ABC)')
    RV = SELF.WM.Run(SELF.AddEditAskProcedure,pAction)
    SELF.DebugOut('End','CallAddEdit(ABC)')
    RETURN RV
  ELSE
    RETURN 0
  END
!!----------------------------------------------------------------------------
!PDLookupCT.PositionChanged PROCEDURE
!!----------------------------------------------------------------------------
!RV BYTE
!  CODE
!  COMPILE('@@',_C60_)
!    IF NOT SELF.WM.FormVCR &= NULL
!      IF SELF.ViewPosition<>SELF.WM.FormVcr.ViewPosition
!        SELF.ViewPosition=SELF.WM.FormVCR.ViewPosition
!        RV=TRUE
!      END
!    END
!  !@@
!  RETURN RV
!----------------------------------------------------------------------------
PDLookupCT.WindowReset PROCEDURE !,DERIVED
!------------------------------------------------------------------------------
  CODE
!  SELF.DebugOut('Begin','WindowReset(ABC)')
  SELF.WM.Reset(1)
!  SELF.DebugOut('End','WindowReset(ABC)')
!------------------------------------------------------------------------------
PDLookupCT.WindowUpdate PROCEDURE !,DERIVED
!------------------------------------------------------------------------------
  CODE
!  SELF.ForceReset=TRUE
  SELF.WM.Update()
!------------------------------------------------------------------------------
PDLookupCT.FileTryFetch PROCEDURE !,BYTE,PROC,VIRTUAL     
!------------------------------------------------------------------------------
RV BYTE
  CODE
  IF SELF.IsRequired                                                 !-- <<>> 6/28/03 - Add required condition so that not cleared if match not required.
    RV = SELF.LookupMgr.Fetch(SELF.IDKey)                            !-- <<>> 5/23/03 - Clear Record
    SELF.DebugOut('Required RV('&RV&')','FileTryFetch(ABC)')
  ELSE
    RV = SELF.LookupMgr.TryFetch(SELF.IDKey)
    SELF.DebugOut('~Required RV('&RV&')','FileTryFetch(ABC)')
  END
  RETURN RV
!------------------------------------------------------------------------------
PDLookupCT.FileReset PROCEDURE !,VIRTUAL                  
!------------------------------------------------------------------------------
  CODE
  SELF.DebugOut('Begin','FileReset(ABC)')
  RESET(SELF.LookupView,SELF.LookupMgr.File)
  SELF.DebugOut('End','FileReset(ABC)')
!------------------------------------------------------------------------------
PDLookupCT.Reset PROCEDURE(BYTE pForce)
!------------------------------------------------------------------------------
  CODE
!  SELF.DebugOut('Begin Force('&pforce&')','Reset(ABC)')
  SELF.Request=SELF.WM.Request  ! to handle change in request (vcrform)
  PARENT.Reset(pForce)
!  SELF.DebugOut('End Force('&pforce&')','Reset(ABC)')
!------------------------------------------------------------------------------
PDLookupCT.TakeEvent PROCEDURE
!------------------------------------------------------------------------------
  CODE
  IF SELF.AddEditFEQ AND FIELD()=SELF.AddEditFeq
    SELF.TakeAddEditEvent()
  END
  RETURN PARENT.TakeEvent()
!------------------------------------------------------------------------------
PDLookupCT.TakeAddEditEvent PROCEDURE
!------------------------------------------------------------------------------
Ptr USHORT
SavLU ASTRING
  CODE
  IF SELF.ID &= NULL OR SELF.IDKey &= NULL OR SELF.SavedID &= NULL OR NOT SELF.AddEditFeq
    RETURN
  END

  IF EVENT()<>EVENT:Accepted THEN RETURN.
  IF 0{PROP:AcceptAll} THEN RETURN.

  ptr=SELF.LookupMgr.SaveBuffer()
  IF SELF.SavedID
    SELF.ID=SELF.SavedID
    IF NOT SELF.LookupMgr.TryFetch(SELF.IDKey)
      IF SELF.CallAddEdit(ChangeRecord)=RequestCompleted
        IF NOT SELF.Screenfield &= NULL
          SELF.ScreenField=SELF.LookupField
        ELSE
          SavLU=SELF.LookupField
        END
        SELF.AssignRecord(0)
        SELF.AfterLookup()
        SELF.Wm.Reset()
      END
    END
  ELSE
    IF NOT SELF.LookupMgr.PrimeRecord()
      IF SELF.CallAddEdit(InsertRecord)=RequestCompleted
        SELF.SavedID=SELF.ID
        IF NOT SELF.Screenfield &= NULL
          SELF.ScreenField=SELF.LookupField
        ELSE
          SavLU=SELF.LookupField
        END
        SELF.AssignRecord(0)
        SELF.AfterLookup()
        SELF.Wm.Reset()
      END
    END
  END
  SELF.LookupMgr.RestoreBuffer(ptr)
  IF SELF.Screenfield &= NULL
    SELF.LookupField=SavLU
  END

!------------------------------------------------------------------------------
PDLookupCT.WindowComponent.Kill PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.Kill
!------------------------------------------------------------------------------
PDLookupCT.WindowComponent.Reset PROCEDURE(BYTE Force)
!------------------------------------------------------------------------------
  CODE
  SELF.Reset(Force)
!------------------------------------------------------------------------------
PDLookupCT.WindowComponent.ResetRequired PROCEDURE
!------------------------------------------------------------------------------
RV      BYTE
  CODE
  SELF.Reset
  RETURN RV
!------------------------------------------------------------------------------
PDLookupCT.WindowComponent.SetAlerts PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.SetAlerts
!------------------------------------------------------------------------------
PDLookupCT.WindowComponent.TakeEvent PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.TakeEvent()
  RETURN Level:Benign
!------------------------------------------------------------------------------
PDLookupCT.WindowComponent.Update PROCEDURE
!------------------------------------------------------------------------------
  CODE
  SELF.Reset
!------------------------------------------------------------------------------
PDLookupCT.WindowComponent.UpdateWindow PROCEDURE
  CODE
  SELF.Reset

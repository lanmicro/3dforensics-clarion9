!ABCIncludeFile(NETTALK)
! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2012 CapeSoft Software  - www.capesoft.com

SMPP_DEFAULT_PORT       equate(5000)

SMPP_VERSION_33         equate(033h)
SMPP_VERSION_34         equate(034h)
SMPP_INTERFACE_VERSION  equate(SMPP_VERSION_34)

! The type of NetSmpp object
!NET:SMPP_RECEIVER       equate(0)
!NET:SMPP_TRANSMITTER    equate(1)
!NET:SMPP_TRANCEIVER     equate(2)

! Represents a SYSTEMTIME
NET_SYSTEMTIME          group, type
wYear                       ushort
wMonth                      ushort
wDayOfWeek                  ushort
wDay                        ushort
wHour                       ushort
wMinute                     ushort
wSecond                     ushort
wMilliseconds               ushort
                        end
                    
! Represents the Smpp time format (both absolute and relative)
! Used for converting between SMPP C octet strings and SYSTEMTIME
NET_SMPP_TIME           group, type
yy                          string(2)
mm                          string(2)
dd                          string(2)
hh                          string(2)
min                         string(2)   ! two digit minute
sec                         string(2)   ! two digit second
t                           string(1)
nn                          string(2)
p                           string(1)
                        end


! special command_id (defined in SMSC Vendor range)
SMPP_SPECIAL_LINKCLOSE      equate(0000101A5h)        

! command_id
SMPP_GENERIC_NACK           equate(080000000h)        

SMPP_BIND_RECEIVER          equate(000000001h)        
SMPP_BIND_RECEIVER_RESP     equate(080000001h)        

SMPP_BIND_TRANSMITTER       equate(000000002h)        
SMPP_BIND_TRANSMITTER_RESP  equate(080000002h)        

SMPP_QUERY_SM               equate(000000003h)        
SMPP_QUERY_SM_RESP          equate(080000003h)        

SMPP_SUBMIT_SM              equate(000000004h)        
SMPP_SUBMIT_SM_RESP         equate(080000004h)        

SMPP_DELIVER_SM             equate(000000005h)        
SMPP_DELIVER_SM_RESP        equate(080000005h)        

SMPP_UNBIND                 equate(000000006h)        
SMPP_UNBIND_RESP            equate(080000006h)        

SMPP_REPLACE_SM             equate(000000007h)        
SMPP_REPLACE_SM_RESP        equate(080000007h)        

SMPP_CANCEL_SM              equate(000000008h)        
SMPP_CANCEL_SM_RESP         equate(080000008h)        

SMPP_BIND_TRANSCEIVER       equate(000000009h)        
SMPP_BIND_TRANSCEIVER_RESP  equate(080000009h)        

SMPP_OUTBIND                equate(00000000Bh)        

SMPP_ENQUIRE_LINK           equate(000000015h)        
SMPP_ENQUIRE_LINK_RESP      equate(080000015h)        

SMPP_SUBMIT_MULTI           equate(000000021h)        
SMPP_SUBMIT_MULTI_RESP      equate(080000021h)        

SMPP_ALERT_NOTIFICATION     equate(000000102h)        

SMPP_DATA_SM                equate(000000103h)        
SMPP_DATA_SM_RESP           equate(080000103h)        


! data_coding
! Some of the more common encodings are::
!    ASCII  (001h)    7 bit, max 160 chars
!    LATIN1 (003h)    8 bit, max 140 chars
!    UCS2   (008h)    8 bit, max 70
!
! data coding       value   type    bits/char   max     description
!CODING_DEFAULT     (000h)  LATIN1  8           140     Default Alphabet (assumed 8 bit data)
!CODING_ANSI_X3_4   (001h)  ASCII   7           160     IA5 (CCITT T.50)/ASCII (ANSI X3.4)
!CODING_OCTET1      (002h)  binary  8           140     Octet unspecified (8-bit binary)
!CODING_ISO8859_1   (003h)  LATIN1  8           140     Latin 1 (ISO-8859-1)
!CODING_OCTET2      (004h)  binary  8           140     Octet unspecified (8-bit binary)
!CODING_JIS         (005h)                              Standard 8 bit octets stored)
!CODING_CYRLLIC     (006h)                              Standard 8 bit octets stored)
!CODING_LATIN       (007h)  LATIN                       Standard 8 bit octets stored)
!CODING_UCS2        (008h)  Unicode 16          UCS2    Standard 8 bit octets stored
!CODING_PICTOGRAM   (009h)                              Standard 8 bit octets stored
!CODING_ISO_2200_JP (00Ah)                              Standard 8 bit octets stored
    
CODING_DEFAULT              equate(000h)                ! Default Alphabet
CODING_ANSI_X3_4            equate(001h)                ! IA5 (CCITT T.50)/ASCII (ANSI X3.4)
CODING_OCTET1               equate(002h)                ! Octet unspecified (8-bit binary)
CODING_ISO8859_1            equate(003h)                ! Latin 1 (ISO-8859-1)
CODING_OCTET2               equate(004h)                ! Octet unspecified (8-bit binary)
CODING_JIS                  equate(005h)                ! JIS (X 0208-1990)
CODING_CYRLLIC              equate(006h)                ! Cyrllic (ISO-8859-5)
CODING_LATIN                equate(007h)                ! Latin/Hebrew (ISO-8859-8)
CODING_UCS2                 equate(008h)                ! UCS2 (ISO/IEC-10646)
CODING_PICTOGRAM            equate(009h)                ! Pictogram Encoding
CODING_ISO_2200_JP          equate(00Ah)                ! ISO-2200-JP


! esm_class
! Equates for specifying which part of the esm_class
! is being set or retrieved
CLASS_MESSAGE_MODE          equate(0)
CLASS_MESSAGE_TYPE          equate(1)
CLASS_ANSI41                equate(2)
CLASS_GSM                   equate(3)

! Message Mode (bits 1-0)
CLASS_DEFAULT               equate(000000000b)          ! Default MC Mode (e.g. Store and Forward)
CLASS_DATAGRAM              equate(000000001b)          ! Datagram mode
CLASS_FORWARD               equate(000000100b)          ! Forward (i.e. Transaction) mode
CLASS_STORE_FORWARD         equate(000000011b)          ! Store and Forward mode (use to select Store and Forward mode if Default MC Mode is non Store and Forward)

! Message Type (bits 2 and 5)
CLASS_DEFAULT_TYPE          equate(000000000b)          ! Default message Type (i.e. normal message)
CLASS_DELIVERY_RECEIPT      equate(000000100b)          ! Short Message contains MC Delivery Receipt
CLASS_INTERMEDIATE          equate(000100000b)          ! Short Message contains Intermediate Delivery Notification

! ANSI-41 specific (bits 5-2)
CLASS_DELIVERY_ACK          equate(000001000b)          ! Short Message contains Delivery Acknowledgement
CLASS_USER_ACK              equate(000010000b)          ! Short Message contains Manual/User Acknowledgement
CLASS_CONVERSATION_ABORT    equate(000011000b)          ! Short Message contains Conversation Abort (Korean CDMA)

! GSM Specific (bits 7-6)
CLASS_NONE                  equate(000000000b)          ! No specific features selected
CLASS_UDHI                  equate(001000000b)          ! UDH Indicator
CLASS_SET_REPLY_PATH        equate(010000000b)          ! Set Reply Path (only relevant for GSM network)
CLASS_UDHI_REPLY_PATH       equate(011000000b)          ! Set UDHI and Reply Path (only relevant for GSM network)


! command_status
! CMD_STATUS_CODE
ESME_ROK                    equate(000000000h)          ! No error
ESME_RINVMSGLEN             equate(000000001h)          ! Message length is invalid
ESME_RINVCMDLEN             equate(000000002h)          ! Command length is invalid
ESME_RINVCMDID              equate(000000003h)          ! Invalid command id
ESME_RINVBNDSTS             equate(000000004h)          ! Incorrect BIND status for given command
ESME_RALYBND                equate(000000005h)          ! ESME already in bound state
ESME_RINVPRTFLG             equate(000000006h)          ! Invalid priority flag
ESME_RINVREGDLVFLG          equate(000000007h)          ! Invalid registered delivery flag
ESME_RSYSERR                equate(000000008h)          ! System error
ESME_RINVSRCADR             equate(00000000Ah)          ! Invalid source address
ESME_RINVDSTADR             equate(00000000Bh)          ! Invalid destination address
ESME_RINVMSGID              equate(00000000Ch)          ! Invalid message id
ESME_RBINDFAIL              equate(00000000Dh)          ! Bind failed
ESME_RINVPASWD              equate(00000000Eh)          ! Invalid password
ESME_RINVSYSID              equate(00000000Fh)          ! Invalid system id
ESME_RCANCELFAIL            equate(000000011h)          ! Cancel SM failed
ESME_RREPLACEFAIL           equate(000000013h)          ! Replace SM failed
ESME_RMSGQFUL               equate(000000014h)          ! Message queue full
ESME_RINVSERTYP             equate(000000015h)          ! Invalid service type
ESME_RINVNUMDESTS           equate(000000033h)          ! Invalid number of destinations
ESME_RINVDLNAME             equate(000000034h)          ! Invalid distribution list name
ESME_RINVDESTFLAG           equate(000000040h)          ! Destination flag is invalid
ESME_RINVSUBREP             equate(000000042h)          ! Invalid 'submit_with_replace' request
ESME_RINVESMCLASS           equate(000000043h)          ! Invalid 'esm_class' field data
ESME_RCNTSUBDL              equate(000000044h)          ! Cannot submit to distribution list
ESME_RSUBMITFAIL            equate(000000045h)          ! 'submit_sm' or 'submit_multi' failed
ESME_RINVSRCTON             equate(000000048h)          ! Invalid source address TON
ESME_RINVSRCNPI             equate(000000049h)          ! Invalid source address NPI
ESME_RINVDSTTON             equate(000000050h)          ! Invalid destination address TON
ESME_RINVDSTNPI             equate(000000051h)          ! Invalid destination address NPI
ESME_RINVSYSTYP             equate(000000053h)          ! Invalid 'system_type' field
ESME_RINVREPFLAG            equate(000000054h)          ! Invalid 'replace_if_present' flag
ESME_RINVNUMMSGS            equate(000000055h)          ! Invalid number of messages
ESME_RTHROTTLED             equate(000000058h)          ! Throttling error
ESME_RINVSCHED              equate(000000061h)          ! Invalid Scheduled Delivery Time
ESME_RINVEXPIRY             equate(000000062h)          ! Invalid Message Validity Period (Expiry time)
ESME_RINVDFTMSGID           equate(000000063h)          ! Predefined Message Invalid or Not Found
ESME_RX_T_APPN              equate(000000064h)          ! ESME Receiver Temporary App Error Code
ESME_RX_P_APPN              equate(000000065h)          ! ESME Receiver Permanent App Error Code
ESME_RX_R_APPN              equate(000000066h)          ! ESME Receiver Reject Message Error Code
ESME_RQUERYFAIL             equate(000000067h)          ! 'query_sm' request failed
ESME_RINVOPTPARSTREAM       equate(0000000C0h)          ! Error in the optional part of the PDU Body
ESME_RINVPARLEN             equate(0000000C2h)          ! Invalid Parameter Length
ESME_RMISSINGOPTPARAM       equate(0000000C3h)          ! Expected Optional Parameter missing
ESME_ROPTPARNOTALLWD        equate(0000000C4h)          ! Invalid Optional Parameter Value
ESME_RDELIVERYFAILURE       equate(0000000FEh)          ! Delivery Failure (used for 'data_sm_resp')
ESME_RUNKNOWNERR            equate(0000000FFh)          ! Unknown Error

ESME_RTRANSPORTERR          equate(000000400h)          ! Transport Error
ESME_RGATEWAYFAILURE        equate(000000401h)          ! Gateway Failure (Unable to reach internal gateway)
ESME_RPERMANENTFAILURE      equate(000000402h)          ! Permanent Network Failure (To external gateway)
ESME_RTEMPORARYFAILURE      equate(000000403h)          ! Temporary Network Failure (To external gateway)
ESME_RINVSUBSCRIBER         equate(000000404h)          ! Invalid Subscriber
ESME_RINVMSG                equate(000000405h)          ! Invalid Message
ESME_RPROTOCOLERR           equate(000000406h)          ! Gateway Protocol Error
ESME_RDUPLICATEDMSG         equate(000000407h)          ! Duplicated Messages
ESME_RBARREDBYUSER          equate(000000408h)          ! Barred by User
ESME_RCANCELLEDBYSYS        equate(000000409h)          ! Cancelled by System
ESME_REXPIRED               equate(00000040Ah)          ! Message Expired


! OPTIONAL_PARAMETER
dest_addr_submit            equate(00005h)
dest_network_type           equate(00006h)
dest_bearer_type            equate(00007h)
dest_telematics_id          equate(00008h)

source_addr_submit          equate(0000Dh)
source_network_type         equate(0000Eh)
source_bearer_type          equate(0000Fh)
source_telematics_id        equate(00010h)

qos_time_to_live            equate(00017h)
payload_type                equate(00019h)
additional_status_info_text equate(0001Dh)
receipted_message_id        equate(0001Eh)
ms_msg_wait_facilities      equate(00030h)
privacy_indicator           equate(00201h)

source_subaddress           equate(00202h)
dest_subaddress             equate(00203h)

user_message_reference      equate(00204h)
user_response_code          equate(00205h)

source_port                 equate(0020Ah)
destination_port            equate(0020Bh)

sar_msg_ref_num             equate(0020Ch)
language_indicator          equate(0020Dh)
sar_total_segments          equate(0020Eh)
sar_segment_seqnum          equate(0020Fh)

SC_interface_version        equate(00210h)

callback_num_pres_ind       equate(00302h)
callback_num_atag           equate(00303h)
number_of_messages          equate(00304h)
callback_num                equate(00381h)

dpf_result                  equate(00420h)
set_dpf                     equate(00421h)
ms_availability_status      equate(00422h)
network_error_code          equate(00423h)
message_payload             equate(00424h)

ussd_service_op             equate(00501h)
display_time                equate(01201h)
sms_signal                  equate(01203h)
ms_validity                 equate(01204h)

alert_on_message_delivery   equate(0130Ch)
its_reply_type              equate(01380h)
its_session_info            equate(01383h)



! Information Element Types for UDH formatted payloads
! Currently only concatenated SMS messages are supported by the 
! class itself.
SMPP_IE_CONCATENATED        equate(0)
SMPP_IE_CONCATENATED_LEN    equate(3)

! maximum length of text messages in a single PDU without
! TLV payloads.
SMPP_MAX_SMS_7              equate(160)     ! Maximum character length for 7 bit encoding
SMPP_MAX_SMS_8              equate(140)     ! Max char length for standard 8 bit encoding
SMPP_MAX_SMS_16             equate(70)      !
SMPP_MAX_SMS_LENGTH         equate(140)     ! Default max length (8 bit encoding)

SMPP_UDH_LENGTH             equate(5)       ! Length of the UDH header for concatenated short messages
! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software  - www.capesoft.com


      Member()
         omit('***$***',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
         ***$***
      Include('Equates.CLW'),ONCE
      Include('Keycodes.CLW'),ONCE
      Include('Errors.CLW'),ONCE
      Map
      End ! map
      Include('NetSimp.inc'),ONCE
  ! includes - NetMap Module Map
  include('NetMap.inc'),once

NetFTPClientWriteFileName                string(256),thread
NetFTPClientWriteFileOpen                long, thread

NetFTPClientWriteFile                    file,driver('dos'),thread,name(NetFTPClientWriteFileName),create
NetFTPClientWriteFileRecord                record
NetFTPClientWriteFileData                    string(NET:MaxBinData)
                                           end
                                         end

NetFTPClientReadFileName                 string(256),thread
NetFTPClientReadFileOpen                 long, thread

NetFTPClientReadFile                     file,driver('dos'),thread,name(NetFTPClientReadFileName)
NetFTPClientReadFileRecord                 record
NetFTPClientReadFileData                     string(NET:MaxBinData)
                                           end
                                         end

NetFTPClientControl.Init Procedure  (ulong Mode=NET:SimpleClient)
  code
  PARENT.Init(Mode)

  self.LowerBoundDataPort = 1792
  self.UpperBoundDataPort = 4400
  self.DataPort = random (self.LowerBoundDataPort, self.UpperBoundDataPort)
  self.ProgressUpdateInterval = 15                                     ! 7 times a second (100 / 15 = 6.67)
  self.NoopInterval = 1000                                             ! 15 seconds
  self.LastNoop = 0                                                    ! 15 seconds
  self.LastDirectory = ''

  self.AsyncOpenUse = 1                                                ! Do not change this - uses AsyncOpenUse mode
  self.AsyncOpenTimeOut = 1200                                         ! 12 sec, open connection timeout
  self.InActiveTimeout = 18000                                         ! 3 minutes

  self._LineBreak = '<13,10>'                                          ! Programmers may want to override this

  self.ControlPort = 21                                                ! set ControlPort to default value
  if self.User = ''
    self.User = 'anonymous'                                            ! set default user to 'anonymous'
  end
  if self.Password = ''
    self.Password = 'anonymous@mail.com'                               ! set default password for 'anonymous' user
  end

  self.FullListing = 1                                                 ! GetDirListing command will send a request for a full
                                                                       ! directory listing to the server
  self._DirListingQ &= new (Net:FTPDirListingQType)
  self.DirListingQ &= self._DirListingQ

!------------------------------------------------------------------------------
NetFTPClientControl.Kill Procedure
  code
  PARENT.Kill()

  if ~self._DirListingQ &= NULL
    free (self.DirListingQ)
    dispose (self._DirListingQ)                              ! Free the allocated Memory of DirListingQ
  end

!------------------------------------------------------------------------------
NetFTPClientControl.TakeEvent PROCEDURE
  code
  parent.TakeEvent()
  case event()
  of EVENT:Timer
    self.CalcProgress()
  End
!------------------------------------------------------------------------------
! Checks whether the client is busy of not and calls ErrorTrap if it is already
! busy with a command,
! Parameters
!   caller: Name of the calling procedure, passed to logging and error handling methods
!   cmd: The command ahout to be executed, used for logging.
! Returns True if the Client us busy or False if it is not.
NetFtpClientControl.IsBusy Procedure (string caller, string cmd)
  code
    if self.Busy
        self.Log (caller, 'Warning. Another command is still in progress. This command was ignored')
        self.error = ERROR:AlreadyBusy
        self.WinSockError = 0
        self.SSLError = 0
        self._CallErrorTrap ('Can''t do a ' & Clip(cmd) & ' command, as the FTP object is already busy', caller)
        return true
    else
        return false
    end


!------------------------------------------------------------------------------
! Secure connection commands
! Issue an AUTH TLS command to switch to a secure TLS control connection
!------------------------------------------------------------------------------
NetFTPClientControl.Auth Procedure ()
pn              equate('NetFTPClientControl.Auth')
  code
    self.Log(pn, '')
    self.SendCommand('AUTH', 'TLS')
    self.auth = false                                       ! Clear the AUTH flag
    self._auth = true                                       ! Set the flag to wait for the AUTH response


!------------------------------------------------------------------------------
! A PBSZ command is required before issuing the PROT command to secure the data
! connection.
NetFTPClientControl.Pbsz Procedure ()
pn              equate('NetFTPClientControl.Pbsz')
  code
    self.SendCommand('PBSZ', '0')
    self.pbsz = false                                       ! Clear the PBSZ flag
    self._pbsz = true                                    ! Set the flag to wait for the PBSZ response



!------------------------------------------------------------------------------
! Switch the data connection between clear and secure. Default to the 'P' mode
! for protected (encrypted using TLS)
NetFTPClientControl.Prot Procedure (<string mode>)
pn              equate('NetFTPClientControl.Prot')
_mode           string(1)
  code
    self.Log(pn, '')

    if Omitted(2)
        _mode = 'P'
    else
        case Upper(mode)
        of 'C' orof 'P'                                     ! S and E are also valid modes, but are meaningless and will default to 'P'
            _mode = Upper(mode)
        else
            _mode = 'P'
        end
    end

    if _mode = 'P'
        self.dataConnection.ssl = 1                         ! Calling PROT with mode P enables SSL implicitly
    else
        self.dataConnection.ssl = 0                         ! Setting PROT to C disables SSL implicitly
    end

    self.SendCommand('PROT', _mode)
    self.prot = false
    self._prot = true



!------------------------------------------------------------------------------
NetFTPClientControl.GetDirListing Procedure  (String DirectoryName)
  code
  self.Log ('NetFTPClientControl.GetDirListing', 'Called. DirName = ' & clip (DirectoryName))

  if self.Busy
    self.Log ('NetFTPClientControl.GetDirListing', 'Warning. Another command is still in progress. This command was ignored')
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a GetDir command, as the FTP object is already busy', 'NetFTPClientControl.GetDirListing')
    return             ! Command in Process
  end

  If DirectoryName <> '""'  and  DirectoryName <> '.' ! List "" causes a syntax error in some cases.
    self._DirectoryName = DirectoryName
  else
    clear(self._DirectoryName)
  End
  self._Command = 'GetDirListing'
  self.DataConnection._DataPortIncremented = 0
  self._ControlDone = 0
  self._DataDone = 0
  self.LastNoop = clock()
  self._GetDirListing()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._GetDirListing Procedure
  code
  self.Log ('NetFTPClientControl._GetDirListing', 'Called.')
  Free(self.DirListingQ)
  if self._CommandPrelim('GetDirListing',True)
    self.Log ('NetFTPClientControl._GetDirListing', 'True.')
    self._state = 'getdirlisting'
    ! Initialisation
    self.DataConnection._FirstPacketInDirListing = 1
    self.DataConnection._LastPacketLine = ''           ! Initialise these properties
    self.DataConnection._LastPacketLineLen = 0
    self.DataConnection._DirFormatSet = 0

    clear(self.packet)

    if self.FullListing
      self.Packet.BinData = 'LIST '
    else
      self.Packet.BinData = 'NLST '
    end

    self.Packet.BinData = self.Packet.BinData[1:5] & clip(self._DirectoryName)
    self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.Log ('NetFTPClientControl._GetDirListing', 'Sending ' & clip(self.Packet.BinData))
    self.send()
  end

!------------------------------------------------------------------------------
NetFTPClientControl.ChangeDir Procedure  (String DirectoryName)
pn              equate('NetFTPClientControl.ChangeDir')
  code
    self.Log (pn, 'Called. DirName = ' & clip (DirectoryName))

    if self.IsBusy(pn, 'ChangeDir')
        return
    end

    self._DirectoryName = DirectoryName
    self._Command = 'ChangeDir'
    self.LastDirectory = ''

    self._ChangeDir()
    return
!------------------------------------------------------------------------------
NetFTPClientControl._ChangeDir Procedure()
  code
  if self._CommandPrelim('ChangeDir',False)
    self._state = 'changedir'
    self.SendCommand('CWD', self._directoryName)
    if sub(self._DirectoryName,1,1) = '/'
      self.LastDirectory = self._DirectoryName
    elsif sub(self._DirectoryName,1,2) = './'
      self.LastDirectory = clip(self.LastDirectory) & sub(self._DirectoryName,2,252) ! drop the .
    else
      self.LastDirectory = clip(self.LastDirectory) & '/' & self._DirectoryName
    end
  end
!------------------------------------------------------------------------------
! Simple command sending method - sends the command, arguments
! and a CR,LF pair.
NetFtpClientControl.SendCommand Procedure(string cmd, <string args>)
  code
    if not Omitted(3)
        self.packet.binData = Clip(cmd) & ' ' & Clip(args) & '<13,10>'
    else
        self.packet.binData = Clip(cmd) & '<13,10>'
    end
    self.packet.binDataLen = Len(Clip(self.packet.binData))
    self.Send()
!------------------------------------------------------------------------------
NetFTPClientControl._ChangeToLastDirectory Procedure ()
  code
  self.SendCommand('CWD', self.lastDirectory)
  self.changeToLastDirectory = 0
  self.changingToLastDirectory = 1

!------------------------------------------------------------------------------
NetFTPClientControl.GetRemoteFile Procedure  (String RemoteFileName,String LocalFileName)
  code
  compile ('****', NETTALKLOG=1)
    if self.loggingOn
      self.Log ('NetFTPClientControl.GetRemoteFile', 'Called. RemoteFile = ' & clip (RemoteFileName) & ' LocalFile = ' & clip (LocalFileName))
    end
  ****

  if self.Busy
    compile ('****', NETTALKLOG=1)
      if self.loggingOn
        self.Log ('NetFTPClientControl.GetRemoteFile', 'Warning. Another command is still in progress. This command was ignored')
      end
    ****
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a GetRemoteFile command, as the FTP object is already busy', 'NetFTPClientControl.GetRemoteFile')
    return
  end

  self._RemoteFileName = RemoteFileName
  self._LocalFileName = LocalFileName
  self._Command = 'GetRemoteFile'
  self.DataConnection._DataPortIncremented = 0
  self._ControlDone = 0
  self._DataDone = 0

  if self.ProgressControl
    self.ProgressControl{PROP:RangeLow} = 0
    self.ProgressControl{PROP:RangeHigh} = 100
    self.ProgressControl{prop:Hide} = 0
    self.ProgressControl{prop:Progress} = 0
  end
  self.ProgressPercent = 0

  self.ExpectedFileSize = 0
  self.bytesLeftToReceive = 0
  self.bytesLeftToSend = 0
  self.bytesLeftToSendToDLL = 0


  ! Look for the size in the DirListing Queue
  clear (self.DirListingQ)
  self.DirListingQ.name = clip (self._RemoteFileName)
  get (self.DirListingQ, self.DirListingQ.name)
  if errorcode() = 0
    self.ExpectedFileSize = self.DirListingQ.Size
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientControl.GetRemoteFile', 'Warning. Could not calculate the size of the file to be retrieved. Progress bar will not work. FileName = ' & clip (self._RemoteFileName))
      end
    ****
  end

  self.bytesLeftToReceive = self.ExpectedFileSize

  ! Update Progress Bar
  self.LastNoop = clock()
  self.ProgressUpdateNow = 1
  self.CalcProgress()

  self._GetRemoteFile()
  return

!------------------------------------------------------------------------------
NetFTPClientControl._GetRemoteFile Procedure
  code
  self.DataConnection._LookForExistingFile = 1

  if self._RemoteFileName = ''
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:InvalidNumberOfParameters
    self._CallErrorTrap('Invalid number of parameters','ControlConnection.GetRemoteFile()')
    return
  end

  if self._CommandPrelim('GetRemoteFile',True)
    self._state = 'getremotefile'
    clear(self.packet)
    self.Packet.BinData = 'RETR ' & clip(self._RemoteFileName)
    self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.send()
  end
!------------------------------------------------------------------------------
NetFTPClientControl.PutFile Procedure  (String LocalFileName,String RemoteFileName)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.PutFile', 'Called. RemoteFile = ' & clip (RemoteFileName) & ' LocalFile = ' & clip (LocalFileName))
    end
  ****
  if self.Busy
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientControl.PutFile', 'Warning. Another command is still in progress. This command was ignored')
      end
    ****
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a PutFile command, as the FTP object is already busy', 'NetFTPClientControl.PutFile')
    return
  end

  self._LocalFileName = LocalFileName
  self._RemoteFileName = RemoteFileName
  self._Command = 'PutFile'
  self.DataConnection._DataPortIncremented = 0

  self.bytesLeftToSend = 0
  self.ExpectedFileSize = 0
  self.bytesLeftToSendToDLL = 0
  if 0{prop:timer} > 100 or 0{prop:timer} = 0
    self.TimerSet = 1
    Self.timerWas = 0{prop:timer}
    0{prop:timer} = 100
  end
  self.LastNoop = clock()
  self._PutFile()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._PutFile Procedure
  code
  if self._RemoteFileName = '' or self._LocalFileName = ''
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:InvalidNumberOfParameters
    self._CallErrorTrap('Invalid number of parameters','ControlConnection.PutFile()')
    return
  end

  if self._CommandPrelim('PutFile',True)
    self._state = 'putfile'
    clear(self.packet)
    self.Packet.BinData = 'STOR ' & clip(self._RemoteFileName)
    self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.send()
    if self.PassiveMode                             ! JSR
      self.DataConnection._SendFile                 ! JSR
    end                                             ! JSR
  end
!------------------------------------------------------------------------------
NetFTPClientControl.AppendToFile Procedure  (String LocalFileName,String RemoteFileName)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.AppendToFile', 'Called. RemoteFile = ' & clip (RemoteFileName) & ' LocalFile = ' & clip (LocalFileName))
    end
  ****

  if self.Busy
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientControl.AppendToFile', 'Warning. Another command is still in progress. This command was ignored')
      end
    ****
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do an AppendToFile command, as the FTP object is already busy', 'NetFTPClientControl.AppendToFile')
    return
  end

  self._RemoteFileName = RemoteFileName
  self._LocalFileName = LocalFileName
  self._Command = 'AppendToFile'
  self.DataConnection._DataPortIncremented = 0
  self.LastNoop = clock()
  self._AppendToFile()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._AppendToFile Procedure
  code
  if self._RemoteFileName = '' or self._LocalFileName = ''
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:InvalidNumberOfParameters
    self._CallErrorTrap('Invalid number of parameters','ControlConnection.AppendToFile()')
    return
  end

  if self._CommandPrelim('AppendToFile',True)
    self._state = 'appendtofile'
    clear(self.packet)
    self.Packet.BinData = 'APPE ' & clip(self._RemoteFileName)
    self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.send()
    if self.PassiveMode                             ! JSR
      self.DataConnection._SendFile                 ! JSR
    end                                             ! JSR
  end
!------------------------------------------------------------------------------
NetFTPClientControl.MakeDir Procedure  (String DirectoryName)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.MakeDir', 'Called. DirectoryName = ' & clip (DirectoryName))
    end
  ****

  if self.Busy
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientControl.MakeDir', 'Warning. Another command is still in progress. This command was ignored')
      end
    ****
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a MakeDir command, as the FTP object is already busy', 'NetFTPClientControl.MakeDir')
    return
  end

  self._DirectoryName = DirectoryName
  self._Command = 'MakeDir'

  self._MakeDir()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._MakeDir Procedure
  code
  if self._DirectoryName = ''
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:InvalidNumberOfParameters
    self._CallErrorTrap('Invalid number of parameters','ControlConnection.MakeDir()')
    return
  end

  if self._CommandPrelim('MakeDir',False)
    self._state = 'makedir'
    clear(self.Packet)
    self.Packet.BinData = 'MKD ' & clip(self._DirectoryName)
    self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.send()
  end
!------------------------------------------------------------------------------
NetFTPClientControl.RemoveDir Procedure  (String DirectoryName)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.RemoveDir', 'Called. DirName = ' & clip (DirectoryName))
    end
  ****

  if self.Busy
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientControl.RemoveDir', 'Warning. Another command is still in progress. This command was ignored')
      end
    ****
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a RemoveDir command, as the FTP object is already busy', 'NetFTPClientControl.RemoveDir')
    return
  end

  self._DirectoryName = DirectoryName
  self._Command = 'RemoveDir'
  self._RemoveDir()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._RemoveDir Procedure
  code
  if self._DirectoryName = ''
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:InvalidNumberOfParameters
    self._CallErrorTrap('Invalid number of parameters','ControlConnection.RemoveDir()')
    return
  end

  if self._CommandPrelim('RemoveDir',False)
    self._state = 'removedir'
    clear(self.Packet)
    self.Packet.BinData = 'RMD ' & clip(self._DirectoryName)
    self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.send()
  end
!------------------------------------------------------------------------------
NetFTPClientControl.Rename Procedure  (String RemoteFileName,String RemoteNewFileName)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.Rename', 'Called. RemoteFile = ' & clip (RemoteFileName) & ' New File Name = ' & clip (RemoteNewFileName))
    end
  ****

  if self.Busy
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientControl.Rename', 'Warning. Another command is still in progress. This command was ignored')
      end
    ****
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a Rename command, as the FTP object is already busy', 'NetFTPClientControl.Rename')
    return
  end

  self._RemoteFileName = RemoteFileName
  self._RemoteNewFileName = RemoteNewFileName
  self._Command = 'Rename'

  self._Rename()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._Rename Procedure
  code
  if self._RemoteFileName = '' or self._RemoteNewFileName = ''
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:InvalidNumberOfParameters
    self._CallErrorTrap('Invalid number of parameters','ControlConnection.Rename()')
    return
  end

  if self._CommandPrelim('Rename',False)
    if self._state = 'rename to'
      self._state = 'rename'
      clear(self.Packet)
      self.Packet.BinData = 'RNTO ' & clip(self._RemoteNewFileName)
      self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
      self.Packet.BinDataLen = len(clip(self.Packet.BinData))
      self.send()
    else
      self._state = 'rename from'
      clear(self.Packet)
      self.Packet.BinData = 'RNFR ' & clip(self._RemoteFileName)
      self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
      self.Packet.BinDataLen = len(clip(self.Packet.BinData))
      self.send()
    end
  end

!------------------------------------------------------------------------------
NetFTPClientControl.DeleteFile Procedure  (String RemoteFileName)
  code
  self.Log ('NetFTPClientControl.DeleteFile', 'Called. RemoteFile = ' & clip (RemoteFileName))

  if self.Busy
    self.Log ('NetFTPClientControl.DeleteFile', 'Warning. Another command is still in progress. This command was ignored')
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a DeleteFile command, as the FTP object is already busy', 'NetFTPClientControl.DeleteFile')
    return
  end

  self._RemoteFileName = RemoteFileName
  self._Command = 'DeleteFile'
  self._DeleteFile()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._DeleteFile Procedure
  code
  if self._RemoteFileName = ''
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:InvalidNumberOfParameters
    self._CallErrorTrap('Invalid number of parameters','ControlConnection.RemoveDir()')
    return
  end

  if self._CommandPrelim('DeleteFile',False)
    self._state = 'deletefile'
    clear(self.Packet)
    self.Packet.BinData = 'DELE ' & clip(self._RemoteFileName)
    self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.send()
  end
!------------------------------------------------------------------------------
NetFTPClientControl.ChangeDirUp Procedure  ()
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.ChangeDirUp', 'Called.')
    end
  ****

  if self.Busy
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientControl.ChangeDirUp', 'Warning. Another command is still in progress. This command was ignored')
      end
    ****
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a ChangeUpDir command, as the FTP object is already busy', 'NetFTPClientControl.ChangeDirUp')
    return
  end

  self._Command = 'ChangeDirUp'
  self._ChangeDirUp()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._ChangeDirUp Procedure
  code
  if self._CommandPrelim('ChangeDirUp',False)
    self._state = 'changedirup'
    clear(self.Packet)
    self.Packet.BinData = 'CDUP'
    self.Packet.BinData = clip(self.Packet.BinData) & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.send()
  end
!------------------------------------------------------------------------------
NetFTPClientControl.GetCurrentDirName Procedure  ()
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.GetCurrentDirName', 'Called.')
    end
  ****

  if self.Busy
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientControl.GetCurrentDirName', 'Warning. Another command is still in progress. This command was ignored')
      end
    ****
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a GetCurrentDirName command, as the FTP object is already busy', 'NetFTPClientControl.GetCurrentDirName')
    return
  end

  self._Command = 'GetCurrentDir'
  self._GetCurrentDirName()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._GetCurrentDirName Procedure
  code
  if self._CommandPrelim('GetCurrentDir',False)
    self._state = 'GetCurrentDir'
    clear(self.Packet)
    self.Packet.BinData = 'PWD' & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.send()
  end
!------------------------------------------------------------------------------
NetFTPClientControl.Stop Procedure
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.Stop', 'Called.')
    end
  ****

  self._state = 'aborting'
  self._Command = 'aborting'
  self.DataConnection.GetInfo()
  if self.PassiveMode = 1
    if self.DataConnection.OpenFlag or self.DataConnection.AsyncOpenBusy
      self.DataConnection.abort()   ! Abort current connection
      self._Cancelling = 1
    end
  else
    loop while (records (self.DataConnection.qServerConnections) > 0)
      self._Cancelling = 1 ! Set this if there are connections
      get (self.DataConnection.qServerConnections, 1)
      self.DataConnection.AbortServerConnection(self.DataConnection.qServerConnections.Socket, self.DataConnection.qServerConnections.SockID)
    end
    !if self.DataConnection.OpenFlag
    if self.DataConnection.OpenFlag or self.DataConnection.AsyncOpenBusy ! Prevent recursive calls to .ErrorTrap if Abort fails
      self.DataConnection.Abort()
    end
  end
  if self.ProgressControl
    self.ProgressControl{PROP:RangeLow} = 0
    self.ProgressControl{PROP:RangeHigh} = 100
    self.ProgressControl{prop:Hide} = 0
    self.ProgressControl{prop:Progress} = 0
  end
  self.ProgressPercent = 0
  self.ExpectedFileSize = 0
  self.bytesLeftToReceive = 0
  self.bytesLeftToSend = 0
  self.bytesLeftToSendToDLL = 0
  self._CallDone()

!------------------------------------------------------------------------------
NetFTPClientControl.Quit Procedure  ()
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.Quit', 'Called')
    end
  ****

  if self.Busy
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientControl.Quit', 'Warning. Another command is still in progress. This command was ignored')
      end
    ****
    self.error = ERROR:AlreadyBusy
    self.WinSockError = 0
    self.SSLError = 0
    self._CallErrorTrap ('Can''t do a Quit command, as the FTP object is already busy', 'NetFTPClientControl.Quit')
    return
  end


  self._Command = 'Quit'
  clear(self.Packet)
  self.Packet.BinData = 'quit' & '<13,10>'          ! if file transfer is in process, control connection will be
  self.Packet.BinDataLen = 6                        ! closed once file transfer is completed
  self.send()

  return

!------------------------------------------------------------------------------
NetFTPClientControl._EnterPassword Procedure
  code
  clear(self.Packet)
  self.Packet.BinData = 'PASS ' & clip(self.Password) & '<13,10>'
  self.Packet.BinDatalen = len(clip(self.Packet.BinData))
  self.Send()
  self._state = 'pass'
  self.ChangeToLastDirectory = Choose(self.LastDirectory<>'',1,0)
!------------------------------------------------------------------------------
NetFTPClientControl._EnterNewUser Procedure
  code
  clear(self.Packet)
  self.Packet.BinData = 'USER ' & clip(self.User) & '<13,10>'
  self.Packet.BinDataLen = len(clip(self.Packet.BinData))
  self.Send()
  self._state = 'user'
!------------------------------------------------------------------------------
NetFTPClientControl._PortCommand Procedure
Pos                        long
HighOrder8Bits             long
  code
  clear(self.packet)
  self.Packet.BinData = 'PORT '                             ! prepare ftp PORT command to enable
  self.Packet.BinDataLen = 5                                ! data connection to be established on new port

         ! Host IP and Port addresses are broken down into 8-bit fields
         ! fields contain decimal numbers and are separated by commas
  Pos = 1
  loop
    if instring('.',self._HostIPAddress,1,Pos) = 0
      self.Packet.BinData = self.Packet.BinData[1:self.Packet.BinDataLen] & sub(self._HostIPAddress,Pos,len(clip(self._HostIPAddress))-Pos+1) & ','
      self.Packet.BinDataLen = len(clip(self.Packet.BinData))
      break
    end
    self.Packet.BinData = self.Packet.BinData[1:self.Packet.BinDataLen] & sub(self._HostIPAddress,Pos,instring('.',self._HostIPAddress,1,Pos)- Pos) & ','
    self.Packet.BinDataLen = self.Packet.BinDataLen + instring('.',self._HostIPAddress,1,Pos)-Pos+1
    Pos = instring('.',self._HostIPAddress,1,Pos) + 1
  end
  HighOrder8Bits = self.DataPort / 256
  self.Packet.BinData = clip(self.Packet.BinData) & HighOrder8Bits & ','
  self.Packet.BinData = clip(self.Packet.BinData) & (self.DataPort % 256) & '<13,10>'
  self.Packet.BinDataLen = len(clip(self.Packet.BinData))
  self.send()
  self._state = 'port'

!------------------------------------------------------------------------------
NetFTPClientControl._SetType Procedure  ()
  code
  clear(self.Packet)

  !self.Packet.BinData = 'TYPE ' & clip(self.DataConnection._CurrentDataRepType) & '<13,10>'
  self.Packet.BinData = 'TYPE ' & upper(clip(self.DataConnection._CurrentDataRepType)) & '<13,10>' ! 29/9/2005 - to work with Apache FTP Server 2.053 which requires this capitilised
  self.Packet.BinDataLen = len(clip(self.Packet.BinData))
  self.send()
  self._state = 'type'

!------------------------------------------------------------------------------
NetFTPClientControl._HandleGetRemoteFileReplies Procedure()
loc:temp1       long
pn              equate('NetFTPClientControl._HandleGetRemoteFileReplies')
  code
  self.Log(pn, '')
  self._ErrorFunction = pn

  case self._ReplyCode
  of '125'
    self._replyProcessed = 1
    if (self.OnlyUseSizesInDirList = 0) and (instring(' bytes)',lower(self.Packet.BinData),1,1) <> 0)
      loc:temp1 = self.ExpectedFileSize
      self.ExpectedFileSize = self.Packet.BinData[instring('(',self.Packet.BinData,1,1)+1 : instring(' bytes)',lower(self.Packet.BinData),1,1) -1]
      self.bytesLeftToReceive += self.ExpectedFileSize - loc:temp1
      self.ProgressUpdateNow = 1
      self.calcProgress()
      self.Log (pn, '(125) Incrementing bytesLeftToReceive to ' & self.bytesLeftToReceive)
    end
  of '150'                                                                  ! opening data connection for file transfer
    self._replyProcessed = 1
    loc:temp1 = self.ExpectedFileSize
    if (self.OnlyUseSizesInDirList = 0) and (instring(' bytes)',lower(self.Packet.BinData),1,1) <> 0)
      self.ExpectedFileSize = self.Packet.BinData[instring('(',self.Packet.BinData,1,1)+1 : instring(' bytes)',lower(self.Packet.BinData),1,1) -1]
      if self.ExpectedFileSize = 0
        self.ExpectedFileSize = loc:temp1
      end
    else
      ! Look for the size in the DirListing Queue
      self.ExpectedFileSize = 0                                             ! cannot determine size of file to be transferred
      clear (self.DirListingQ)
      self.DirListingQ.name = clip (self._RemoteFileName)
      get (self.DirListingQ, self.DirListingQ.name)
      if errorcode() = 0
        self.ExpectedFileSize = self.DirListingQ.Size
      else
        self.ExpectedFileSize = loc:temp1                                   ! cannot determine size of file to be transferred
      end
    end
    self.bytesLeftToReceive += self.ExpectedFileSize - loc:temp1
    self.ProgressUpdateNow = 1
    self.calcProgress()
    self.Log (pn, 'Incrementing bytesLeftToReceive to ' & self.bytesLeftToReceive)
  of '200'
    if not self.SecureConnecting()
      case self._State
      of 'port'                                                 ! PORT command successful
        self._ReplyProcessed = 1
        self._State = 'port successful'
        self.DataConnection._DataPortIncremented = 1
        if self.BinaryTransfer                                                ! IF binary transfer is required,
          self.DataConnection._CurrentDataRepType = 'i'                       ! set data representation TYPE to binary (IMAGE)
        else                                                                  ! ELSE
          self.DataConnection._CurrentDataRepType = 'a'                       ! set data representation TYPE to ASCII
        end
        self._SetType()
      of 'type'                                                 ! TYPE command successful
        self._ReplyProcessed = 1
        self._State = 'type successful'
        self._GetRemoteFile()
      of 'NoOp'
        self._ReplyProcessed = 1
        self._State = 'noop successful'
      else ! some servers (eg ProFTPD) do not reply to the NOOPs immediately, but store up all the replies for after the transfer completes.
        self._ReplyProcessed = 1
        self._State = 'reply ignored'
      end
    end
  of '220'                                                                  ! ready for new USER,control connection opened
    ! csTODO: Explicit SSL support
    !if self.SSL and self.explicitSSL and not self.auth                     ! Issue an AUTH before continuing
    !  self.Log(pn, 'Explicit SSL - Call AUTH')
    !  self.Auth()
    !  self._replyProcessed = true
    if self.DataConnection.SSL and not self.pbsz                            ! Issue a PBSZ command on the data connection
      self.Log(pn, 'Call PBSZ to start the process of securings data connection')
      self.Pbsz()
      self._replyProcessed = true
    else
      self._replyProcessed = 1
      self._state = 'opened'
      self._GetRemoteFile()
    end
  of '225'
    if self._state = 'aborting'                                             ! ABORT command successful
      self._replyProcessed = 1
      self._state = 'aborted'
      self._GetRemoteFile()
    end
  of '226'                                                                  ! file transfer complete, connection closed
    self._replyProcessed = 1
    self._ControlDone = 1
    if self._DataDone = 1
      self.Log (pn, 'Data & Control Object have finished. So we will call _CallDone()')
      self._CallDone()
    else
      self.Log (pn, 'Control Object has finished. Need to wait for Data object.')
    end
  of '230'                                                                  ! new USER LOGGED IN, proceed
    if self.changeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._GetRemoteFile()
    end
  !of '234'                                                                  ! AUTH command completed successfully
     ! START TLS - Switch to a secure connection before issuing the PBSZ and PROT commands
     ! csTODO: handle switch to SSL here
  of '250'
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._GetRemoteFile()
    else
      self._replyProcessed = 1
      self._ControlDone = 1
      if self._DataDone = 1
        self._CallDone()
      end
    end
  of '425'                                                                  ! can't open data connection
    self._replyProcessed = 1
    self.DataConnection._DataPortIncremented = 0                            ! increment port and try again
    self._GetRemoteFile()
  of '426'
    if self._state = 'aborting'                                             ! nothing to abort
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._GetRemoteFile()
    end
  of '500'      ! command UNRECOGNIZED
    case self._state
    of 'type'                                                               ! invalid representation TYPE
      self._replyProcessed = 1
      self.DataConnection._CurrentDataRepType = 'i'                         ! TYPE is set to default (IMAGE)
      self._SetType()
      self._NeedToTrapError = 0                                             ! not fatal error, should not be trapped
    of 'NoOp'
      self._ReplyProcessed = 1
      self._State = 'noop successful'
      self._NeedToTrapError = 0
    end
  of '501'                                                                  ! SYNTAX ERROR in command
    if self._state = 'type'                                                 ! invalid representation TYPE
      self._replyProcessed = 1
      self.DataConnection._CurrentDataRepType = 'i'                         ! TYPE is set to default (IMAGE)
      self._SetType()
      self._NeedToTrapError = 0                                             ! not fatal error, should not be trapped
    end
  of '504'                                                                  ! command NOT IMPLEMENTED for that PARAMETER
    if self._state = 'type'                                                 ! invalid representation TYPE
      self._replyProcessed = 1
      self.DataConnection._CurrentDataRepType = 'i'                         ! TYPE is set to default (IMAGE)
      self._SetType()
      self._NeedToTrapError = 0                                             ! not fatal error, should not be trapped
    end
  of '530'                                                                  ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._GetRemoteFile()
    end
  end
!------------------------------------------------------------------------------
NetFTPClientControl._HandlePutFileReplies Procedure
pn              equate('NetFTPClientControl._HandlePutFileReplies')
  code
  self.Log(pn, '')
  self._ErrorFunction = pn

  case self._ReplyCode
  of '125'
    self._replyProcessed = 1
  of '150'
    self._replyProcessed = 1
  of '200'
    if not self.SecureConnecting()
      case self._State
      of 'port'                                            ! PORT command successful
        self._ReplyProcessed = 1
        self._State = 'port successful'
        self.DataConnection._DataPortIncremented = 1
        if self.BinaryTransfer                                           ! IF binary transfer is required,
          self.DataConnection._CurrentDataRepType = 'i'                  ! set data representation TYPE to binary (IMAGE)
        else                                                             ! ELSE
          self.DataConnection._CurrentDataRepType = 'a'                  ! set data representation TYPE to ASCII
        end
        self._SetType()
      of 'type'                                         ! TYPE command successful
        self._ReplyProcessed = 1
        self._State = 'type successful'
        self._PutFile()
      of 'NoOp'
        self._ReplyProcessed = 1
        self._State = 'noop successful'
      else ! some servers (eg ProFTPD) do not reply to the NOOPs immediately, but store up all the replies for after the transfer completes.
        self._ReplyProcessed = 1
        self._State = 'reply ignored'
      end
    end
  of '220'                                                             ! ready for new USER,control connection opened
    if self.DataConnection.SSL and not self.pbsz                            ! Issue a PBSZ command on the data connection
      self.Log(pn, 'Call PBSZ to start the process of securings data connection')
      self.Pbsz()
      self._replyProcessed = true
    else
      self._replyProcessed = 1
      self._state = 'opened'
      self._PutFile()
    end
  of '225'
    if self._state = 'aborting'                                        ! ABORT command successful
      self._replyProcessed = 1
      self._state = 'aborted'
      self._PutFile()
    end
  of '226'                                                             ! file transfer complete, connection closed
    self._replyProcessed = 1
    self.Busy = 0
    self.ProgressUpdateNow = 1
    self.CalcProgress()
    self._CallDone()
  of '230'                                                             ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._PutFile()
    end
  of '250'           ! file transfer complete
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._PutFile()
    else
      self._replyProcessed = 1
      self.Busy = 0
      self.ProgressUpdateNow = 1
      self.CalcProgress()
      self._CallDone()
    end
  of '425'                                                             ! can't open data connection
    self._replyProcessed = 1
    self.DataConnection._DataPortIncremented = 0                       ! increment port and try again
    self._PutFile()
  of '426'                                                             ! nothing to abort
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._PutFile()
    end
  of '500'                                                             ! command UNRECOGNIZED
    case self._state
    of 'type'                    ! invalid representation TYPE
      self._replyProcessed = 1
      self.DataConnection._CurrentDataRepType = 'i'                    ! TYPE is set to default (IMAGE)
      self._SetType()
      self._NeedToTrapError = 0                                        ! not fatal error, should not be trapped
    of 'NoOp'
      self._ReplyProcessed = 1
      self._State = 'noop successful'
      self._NeedToTrapError = 0
    end
  of '501'                                                             ! SYNTAX ERROR in command
    if self._state = 'type'                                            ! invalid representation TYPE
      self._replyProcessed = 1
      self.DataConnection._CurrentDataRepType = 'i'                    ! TYPE is set to default (IMAGE)
      self._SetType()
      self._NeedToTrapError = 0                                        ! not fatal error, should not be trapped
    end
  of '504'                                                             ! command not implemented for that parameter
    if self._state = 'type'                                            ! invalid representation TYPE
      self._replyProcessed = 1
      self.DataConnection._CurrentDataRepType = 'i'                    ! TYPE is set to default (IMAGE)
      self._SetType()
      self._NeedToTrapError = 0                                        ! not fatal error, should not be trapped
    end
  of '530'                                                             ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._PutFile()
    end
  end
!------------------------------------------------------------------------------
NetFTPClientControl._HandleAppendToFileReplies Procedure
pn              equate('NetFTPClientControl._HandleAppendToFileReplies')
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleAppendToFileReplies()'

  case self._ReplyCode
  of '125'
    self._replyProcessed = 1
  of '150'
    self._replyProcessed = 1
  of '200'
    if not self.SecureConnecting()
      case self._State
      of 'port'                                            ! PORT command successful
        self._ReplyProcessed = 1
        self._State = 'port successful'
        self.DataConnection._DataPortIncremented = 1
        if self.BinaryTransfer                                           ! IF binary transfer is required,
          self.DataConnection._CurrentDataRepType = 'i'                  ! set data representation TYPE to binary (IMAGE)
        else                                                             ! ELSE
          self.DataConnection._CurrentDataRepType = 'a'                  ! set data representation TYPE to ASCII
        end
        self._SetType()
      of 'type'                                         ! TYPE command successful
        self._ReplyProcessed = 1
        self._State = 'type successful'
        self._AppendToFile()
      of 'NoOp'
        self._ReplyProcessed = 1
        self._State = 'noop successful'
      end
    end
  of '220'                                                              ! ready for new USER,control connection opened
    if self.DataConnection.SSL and not self.pbsz                        ! Issue a PBSZ command on the data connection
      self.Log(pn, 'Call PBSZ to start the process of securings data connection')
      self.Pbsz()
      self._replyProcessed = true
    else
      self._replyProcessed = 1
      self._state = 'opened'
      self._AppendToFile()
    end
  of '225'
    if self._state = 'aborting'                                         ! ABORT command successful
      self._replyProcessed = 1
      self._state = 'aborted'
      self._AppendToFile()
    end
  of '226'                                                              ! file transfer complete, connection closed
    self._replyProcessed = 1
    self._CallDone()
  of '230'                                                              ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._AppendToFile()
    end
  of '250'                                                              ! file transfer complete
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._AppendToFile()
    else
      self._replyProcessed = 1
      self._CallDone()
    end
  of '425'                                                             ! can't open data connection
    self._replyProcessed = 1
    self.DataConnection._DataPortIncremented = 0                       ! increment port and try again
    self._AppendToFile()
  of '426'                                                             ! nothing to abort
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._AppendToFile()
    end
  of '500'                                                             ! command UNRECOGNIZED
    case self._state
    of 'type'                  ! invalid representation TYPE
      self._replyProcessed = 1
      self.DataConnection._CurrentDataRepType = 'i'                    ! TYPE is set to default (IMAGE)
      self._SetType()
      self._NeedToTrapError = 0                                        ! not fatal error, should not be trapped
    of 'NoOp'
      self._ReplyProcessed = 1
      self._State = 'noop successful'
      self._NeedToTrapError = 0
    end
  of '501'                                                             ! SYNTAX ERROR in command
    if self._state = 'type'                                            ! invalid representation TYPE
      self._replyProcessed = 1
      self.DataConnection._CurrentDataRepType = 'i'                    ! TYPE is set to default (IMAGE)
      self._SetType()
      self._NeedToTrapError = 0                                        ! not fatal error, should not be trapped
    end
  of '504'                                                             ! command not implemented for that parameter
    if self._state = 'type'                                            ! invalid representation TYPE
      self._replyProcessed = 1
      self.DataConnection._CurrentDataRepType = 'i'                    ! TYPE is set to default (IMAGE)
      self._SetType()
      self._NeedToTrapError = 0                                        ! not fatal error, should not be trapped
    end
  of '530'                                                             ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._AppendToFile()
    end
  end
!------------------------------------------------------------------------------
NetFTPClientControl._HandleRenameReplies Procedure
pn              equate('NetFTPClientControl._HandleRenameReplies')
  code
  self._ErrorFunction = pn
  self.Log(pn, '')
  case self._ReplyCode
  of '220'                                                            ! ready for new USER,control connection opened
    self._replyProcessed = 1
    self._state = 'opened'
    self._Rename()
  of '225'                                                            ! ABORT command successful
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'aborted'
      self._Rename()
    end
  of '230'                                                            ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._Rename()
    end
  of '250'                                                            ! RENAME command successful
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._Rename()
    else
      self._replyProcessed = 1
      self._CallDone()
    end
  of '350'
    if self._state = 'rename from'                                    ! ready for DESTINATION name
      self._replyProcessed = 1
      self._state = 'rename to'
      self._Rename()
    end
  of '426'                                                            ! nothing to abort
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._Rename()
    end
  of '530'                                                            ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._Rename()
    end
  end
!-----------------------------------------------------------
! Called to handle the 200 reply code and check whether a secure
! connection sequence is in progress. Handles the PBSZ and PROT command for
! requesting a secure connection. Returns True if securing the connection is
! in progress or False otherwise.
NetFTPClientControl.SecureConnecting Procedure()
pn              equate('NetFTPClientControl.SecureConnecting')
  code
    if self._pbsz                                                       ! PBSZ issued successfully
        self.Log(pn, 'PBSZ issued successfully, clear the flag and call the Prot command')
        self._pbsz = false
        self.pbsz = true
        self.Prot()                                                     ! Issue a prot command
        self._replyProcessed = true
        return true
    elsif self._prot                                                    ! PROT command issued successfully
        self.Log(pn, 'PROT command successful, clear the _prot flag and continue normally')
        self._prot = false
        self.prot = true
        self._replyProcessed = true
        self._GetDirListing()
        return true
    end
    return false



!------------------------------------------------------------------------------
NetFTPClientControl._HandleGetDirListingReplies Procedure
pn              equate('NetFTPClientControl._HandleGetDirListingReplies')
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleGetDirListingReplies()'
  self.Log(self._ErrorFunction, '')
  case self._ReplyCode
  of '125'
    self._replyProcessed = 1
  of '150'
    self._replyProcessed = 1
  of '200'
    if not self.SecureConnecting()
      case self._State
      of 'port'                                           ! PORT command successful
        self._ReplyProcessed = 1
        self._State = 'port successful'
        self.DataConnection._DataPortIncremented = 1
        self.DataConnection._CurrentDataRepType = 'a'
        self._SetType()
      of 'type'                                        ! TYPE command successful
        self._ReplyProcessed = 1
        self._State = 'type successful'
        self._GetDirListing()
      of 'NoOp'
        self._ReplyProcessed = 1
        self._State = 'noop successful'
      else ! some servers (eg ProFTPD) do not reply to the NOOPs immediately, but store up all the replies for after the transfer completes.
        self._ReplyProcessed = 1
        self._State = 'reply ignored'
      end
    end
  of '220'                                                              ! ready for new USER, control connection opened
    if self.DataConnection.SSL and not self.pbsz                        ! Issue a PBSZ command on the data connection
      self.Log(pn, 'Call PBSZ to start the process of securings data connection')
      self.Pbsz()
      self._replyProcessed = true
    else
      self._replyProcessed = 1
      self._state = 'opened'
      self._GetDirListing()
    end
  of '225'
    if self._state = 'aborting'                                       ! ABORT command successful
      self._replyProcessed = 1
      self._state = 'aborted'
      self._GetDirListing()
    end
  of '226'                                                            ! file transfer complete, connection closed
  orof '502'                                                          ! list command not implemented  !5.06
    self._replyProcessed = 1
    self._ControlDone = 1
    if self._DataDone = 1
      self._CallDone()
    end
  of '230'                                                            ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._GetDirListing()
    end
  of '250'                                                            ! file transfer complete
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._GetDirListing()
    else
      self._replyProcessed = 1
      self._ControlDone = 1
      if self._DataDone = 1
        self._CallDone()
      end
    end
  of '425'                                                            ! can't open data connection
    self._replyProcessed = 1
    if self.packet.binDataLen > 1
      self.Log ('NetFTPClientControl._HandleGetDirListingReplies', 'Warning 425 - Will try next data port.')
    end
    self._Command = 'GetDirListing'
    self.DataConnection._DataPortIncremented = 0
    self._GetDirListing()
  of '426'
    if self._state = 'aborting'                                       ! nothing to abort
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._GetDirListing()
    end
  of '530'                                                            ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._GetDirListing()
    end
  end
!------------------------------------------------------------------------------
NetFTPClientControl._HandleChangeDirReplies Procedure
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleChangeDirReplies'
  self.Log(self._ErrorFunction, '')
  case self._ReplyCode
  of '220'                                                            ! ready for new USER,control connection opened
    self._replyProcessed = 1
    self._state = 'opened'
    self._ChangeDir()
  of '225'                                                            ! ABORT command successful
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'aborted'
      self._ChangeDir()
    end
  of '230'                                                            ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._ChangeDir()
    end
  of '250'                                                            ! CHANGE DIR command successful
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._ChangeDir()
    else
      self._replyProcessed = 1
      self._CallDone()
    end
  of '257'                                                            ! CHANGE DIR command successful
    self._replyProcessed = 1
    self._CallDone()
  of '426'                                                            ! nothing to abort
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._ChangeDir()
    end
  of '530'                                                            ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._ChangeDir()
    end
  end
!------------------------------------------------------------------------------
NetFTPClientControl._HandleMakeDirReplies Procedure
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleMakeDirReplies'
  self.Log(self._ErrorFunction, '')
  case self._ReplyCode
  of '220'                                                            ! ready for new USER,control connection opened
    self._replyProcessed = 1
    self._state = 'opened'
    self._MakeDir()
  of '225'                                                            ! ABORT command successful
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'aborted'
      self._MakeDir()
    end
  of '230'                                                            ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._MakeDir()
    end
  of '250'
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._MakeDir()
    end
  of '257'   ! MAKE DIR command successful
    self._replyProcessed = 1
    self._CallDone()
  of '426'                                                            ! nothing to abort
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._MakeDir()
    end
  of '530'                                                            ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._MakeDir()
    end
  end
!------------------------------------------------------------------------------
NetFTPClientControl._HandleRemoveDirReplies Procedure
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleRemoveDirReplies()'
  self.Log(self._ErrorFunction, '')

  case self._ReplyCode
  of '220'                                                            ! ready for new USER,control connection opened
    self._replyProcessed = 1
    self._state = 'opened'
    self._RemoveDir()
  of '225'                                                            ! ABORT command successful
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'aborted'
      self._RemoveDir()
    end
  of '230'                                                            ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._RemoveDir()
    end
  of '250'                                                            ! REMOVE DIR command successful
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._RemoveDir()
    else
      self._replyProcessed = 1
      self._CallDone()
    end
  of '426'                                                            ! nothing to abort
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._RemoveDir()
    end
  of '530'                                                            ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._RemoveDir()
    end
  end
!------------------------------------------------------------------------------
NetFTPClientControl._HandleDeleteFileReplies Procedure
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleDeleteFileReplies()'
  self.Log(self._ErrorFunction, '')
  case self._ReplyCode
  of '220'                                                            ! ready for new USER,control connection opened
    self._replyProcessed = 1
    self._state = 'opened'
    self._DeleteFile()
  of '225'                                                            ! ABORT command successful
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'aborted'
      self._DeleteFile()
    end
  of '230'                                                            ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._DeleteFile()
    end
  of   '250'                                                          ! DELETE command successful
  orof '200'  ! Fudge for Novel FTP server which incorrectly reports 200 instead of 250 ! 22/11/2002 ! Novell NetWare v5.00 FTP server
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._DeleteFile()
    else
      self._replyProcessed = 1
      self._CallDone()
    end
  of '426'                                                            ! nothing to abort
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._DeleteFile()
    end
  of '530'                                                            ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._DeleteFile()
    end
  end
!------------------------------------------------------------------------------
NetFTPClientControl._HandleChangeDirUpReplies Procedure
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleChangeDirUpReplies()'
  self.Log(self._ErrorFunction, '')

  case self._ReplyCode
  of '200'                                                            ! CHANGE DIR UP command successful
    self._replyProcessed = 1
    self._CallDone()
  of '220'                                                            ! ready for new USER,control connection opened
    self._replyProcessed = 1
    self._state = 'opened'
    self._ChangeDirUp()
  of '225'                                                            ! ABORT command successful
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'aborted'
      self._ChangeDirUp()
    end
  of '230'                                                            ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._ChangeDirUp()
    end
  of '250'                                                            ! CHANGE DIR UP command successful
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._ChangeDirUp()
    else
      self._replyProcessed = 1
      self._CallDone()
    end
  of '426'                                                            ! nothing to abort
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._ChangeDirUp()
    end
  of '530'                                                            ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._ChangeDirUp()
    end
  end
!------------------------------------------------------------------------------
! The course of action to take on receipt of the following replies
! is INDEPENDENT of the COMMAND currently being executed
NetFTPClientControl._HandleRepliesIndependently Procedure ()
pn              equate('NetFTPClientControl._HandleRepliesIndependently')
  code
  self.Log(pn, 'self._ReplyCode=' & self._ReplyCode)
  case self._ReplyCode
  of '225'                                                            ! "Data connection open; not transfer in progress"
    if self._state = 'aborting'                                       ! ABORT command successful
      self.Busy = 0                                                   ! command in progress was aborted
    end
  of '227'
    self._replyProcessed = 1
    self._OpenPassiveDataConnection()
    self.Busy = 0
  of '234'
    ! csTODO: Switch to TLS connection - _SSLSwitchToSSL
  of '331'                                                            ! User okay, password for user required
    self._replyProcessed = 1
    self._state = 'user accepted'
    self._EnterPassword()
  of '332'                                                            ! need an account for login
    self._replyProcessed = 1
    self._CallErrorTrap ('332 "need account" - no code for this yet.', 'NetFTPClientControl._HandleRepliesIndependently')
  end
!------------------------------------------------------------------------------
NetFTPClientControl._HandleGetCurrentDirNameReplies Procedure
PosInReply                    long
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleGetCurrentDirReplies()'
  self.Log(self._ErrorFunction, '')

  case self._ReplyCode
  of '220'                                                            ! ready for new USER,control connection opened
    self._replyProcessed = 1
    self._state = 'opened'
    self._GetCurrentDirName()
  of '225'                                                            ! ABORT command successful
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'aborted'
      self._GetCurrentDirName()
    end
  of '230'                                                            ! new USER LOGGED IN, proceed
    if self.ChangeToLastDirectory
      self._ChangeToLastDirectory()
      self._replyProcessed = 1
    else
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._GetCurrentDirName()
    end
  of '250'
    if self.ChangingToLastDirectory = 1
      self.ChangingToLastDirectory = 0
      self._replyProcessed = 1
      self._state = 'logged in'
      self.LoggedIn = 1
      self._GetCurrentDirName()
    end
  of '257'                                                            ! GET CURRENT DIR command successful
    self._replyProcessed = 1
    PosInReply = instring('"',self.Packet.BinData,1,1)
    if PosInReply
      PosInReply = PosInReply + 1
      if instring('"',self.Packet.BinData,1,PosInReply)
        self.CurrentDirectory = self.Packet.BinData[ PosInReply : instring('"',self.Packet.BinData,1,PosInReply)-1]
        self._CallDone()
      else
        self._NeedToTrapError = 1
        self._ErrorString = 'Could not read current directory from server reply'
        self.Error = ERROR:GeneralFTPError
        self.WinSockError = 0
        self.SSLError = 0
        self.FTPError = ERROR:FTP:UnknownReplyFormat
      end
    else
      self._NeedToTrapError = 1
      self._ErrorString = 'Could not read current directory from server reply'
      self.Error = ERROR:GeneralFTPError
      self.WinSockError = 0
      self.SSLError = 0
      self.FTPError = ERROR:FTP:UnknownReplyFormat
    end
  of '426'                                                            ! nothing to abort
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'nothing to abort'
      self._GetCurrentDirName()
    end
  of '530'                                                            ! user is not LOGGED IN
    if self._state = 'aborting'
      self._replyProcessed = 1
      self._state = 'not logged in'
      self._GetCurrentDirName()
    end
  end
!------------------------------------------------------------------------------
NetFTPClientControl._TrapErrorsInProcess Procedure
  code
  case self._ReplyCode
  of '120'                                                                  ! Service ready in NNN minutes
    self._NeedToTrapError = 1
    self._ErrorString = 'Service will only become ready sometime later'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:ServiceNotReady

  of '421'                                                                  ! Service not available. Closing control connection
    self._NeedToTrapError = 1
    self._ErrorString = 'FTP Server closing Control Connection. (Server closing down)'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:ServerClosingDown

  of '426'                                                                  ! TRANSFER ABORTED, connection closed
    if  self._Cancelling = 1
      ! We closed the Data Connection, to cancel a command, so ignore it for now
      self._Cancelling = 0
    else
      self._NeedToTrapError = 1
      self._ErrorString = 'Connection closed, transfer aborted'
      Do AddToErrorString
      self.Error = ERROR:GeneralFTPError
      self.WinSockError = 0
      self.SSLError = 0
      self.FTPError = ERROR:FTP:TransferAborted
    end

  of '450'                                                                  ! FILE UNAVAILABLE
    self._NeedToTrapError = 1
    self._ErrorString = 'File unavailable'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:FileUnavailable

  of '451'                                                                  ! LOCAL ERROR IN PROCESSING
    self._NeedToTrapError = 1
    self._ErrorString = 'Local error in processing'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:LocalProcessingError

  of '452'                                                                  ! INSUFFICIENT STORAGE space in system
    self._NeedToTrapError = 1
    self._ErrorString = 'Insufficient storage space in system'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:InsufficientStorageSpace

  of   '500'
  orof '501'
    if self._state = 'type'
      self.Log ('NetFTPClientControl._TrapErrorsInProcess', 'Warning - TYPE Command Failed. Will ignore and continue')
      self._ReplyCode = 200 ! Pretend reply = 200 !
    elsif (self._IgnorePassiveFailure and self._state = 'passive')
      self.Log ('NetFTPClientControl._TrapErrorsInProcess', 'Warning - PASV (Passive) Command Failed. Will ignore and continue')
      self._ReplyCode = 227 ! Pretend reply = 227 !
    else
      self._NeedToTrapError = 1
      if self._ReplyCode = '500'                                              ! COMMAND UNRECOGNIZED
        self._ErrorString = 'Command unrecognized'
        Do AddToErrorString
        self.Error = ERROR:GeneralFTPError
        self.WinSockError = 0
        self.SSLError = 0
        self.FTPError = ERROR:FTP:CommandUnrecognized

      elsif self._ReplyCode = '501'                                           ! SYNTAX ERROR
        self._ErrorString = 'Syntax error in parameters or arguments'
        Do AddToErrorString
        self.Error = ERROR:GeneralFTPError
        self.WinSockError = 0
        self.SSLError = 0
        self.FTPError = ERROR:FTP:SyntaxError
        if self.PassiveMode !6.23, Filezilla does not close the data connection on LIST "" (Syntax error)
          if self.DataConnection.OpenFlag
            self.DataConnection.Close()
          end
        end
      end

      case clip(self._state)
      of 'user'
      orof 'pass'
      orof 'aborting'
      orof 'port'
        self._ErrorFunction = 'NetFTPClientControl._TrapErrorsInProcess()'
      end
    end

  of '502'                                                                  ! COMAND NOT IMPLEMENTED
    self._NeedToTrapError = 1
    self._ErrorString = 'Command not implemented'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:CommandNotImplemented
    if clip(self._state) = 'aborting'
      self._ErrorFunction = 'NetFTPClientControl._TrapErrorsInProcess()'
    end

  of '503'                                                                  ! BAD SEQUENCE OF COMMANDS
    self._NeedToTrapError = 1
    self._ErrorString = 'Bad sequence of commands'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:BadSequenceOfCommands

  of '504'                                                                  ! COMMAND NOT IMPLEMENTED for that parameter
    self._NeedToTrapError = 1
    self._ErrorString = 'Command not implemented for that parameter'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:InvalidParameter

  of '530'                                                                  ! NOT LOGGED IN
    self._NeedToTrapError = 1
    self._ErrorString = 'Not logged in'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:NotLoggedIn

    if self._state = 'user'                                                 ! ACCESS DENIED to user
      self.Error = ERROR:GeneralFTPError
      self.WinSockError = 0
      self.SSLError = 0
      self._ErrorString = 'User ' & clip(self.User) & ' access denied'
      self._ErrorFunction = 'NetFTPClientControl._TrapErrorsInProcess()'

    elsif self._state = 'pass'
      if self.Password[1] = '-'                                             ! attempt to log in again
        self._EnterNewUser()                                                ! '-' will be removed in password command
        self._NeedToTrapError = 0                                           ! not fatal error, should not be trapped

      else                                                                  ! LOGIN INCORRECT
        self._ErrorString = 'Login incorrect'
        self._ErrorFunction = 'NetFTPClientControl._TrapErrorsInProcess()'
      end
    end

  of '532'                                                                  ! NEED ACCOUNT for storing files
    self._NeedToTrapError = 1
    self._ErrorString = 'Need account for storing files'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:NeedAccountToStoreFiles

  of '550'                                                                  ! FILE UNAVAILABLE
    self._NeedToTrapError = 1
    self._ErrorString = 'File unavailable'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:FileAccessError

  of '552'                                                                  ! EXCEEDED STORAGE allocation
    self._NeedToTrapError = 1
    self._ErrorString = 'Exceeded storage allocation'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:ExceededStorageAllocation

  of '553'                                                                  ! FILE NAME NOT ALLOWED
    self._NeedToTrapError = 1
    self._ErrorString = 'File name not allowed'
    Do AddToErrorString
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.FTPError = ERROR:FTP:FileNameNotAllowed
  end

! ------------------------------------------------------------------------

AddToErrorString Routine
  if self._ReplySoFar <> ''
    self._ErrorString = clip (self._ErrorString) & '<13,10>' & clip (self._ReplySoFar)
  end

!------------------------------------------------------------------------------
NetFTPClientControl.ConnectionClosed Procedure
  code
  PARENT.ConnectionClosed()

  self.LoggedIn = 0

  if self.Busy
    self.Busy = 0
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    if self._state = 'passive'
      self.FTPError = ERROR:FTP:ControlConnectionClosedInPassive
      self._ErrorString = 'Command aborted. Check if the server supports the Passive mode.'
    else
      self.FTPError = ERROR:FTP:ControlConnectionClosed
      self._ErrorString = 'Command aborted.'
    end
    self._ErrorFunction = 'NetFTPClientControl.ConnectionClosed'
    self._CallErrorTrap(self._ErrorString,self._ErrorFunction)
  end

!------------------------------------------------------------------------------
NetFTPClientControl.ErrorTrap Procedure  (string errorStr,string functionName)
  code
  ! This is a virual method - will be called when errors occur.
!------------------------------------------------------------------------------
NetFTPClientControl.Open Procedure  (string Server,uShort Port=0)
  code
  self._Cancelling = 0
  self._ReplySoFarLen = 0
  self._state = 'open'
  PARENT.Open(server, port)
  self.log('NetFTPClientControl.Open','CONTROL OPEN')

!------------------------------------------------------------------------------
NetFTPClientControl.Process Procedure
local       group, pre (loc)
EndFound        byte
Pos1            long
Pos2            long
AnotherLineOfDataStartsHere  long
unknownCommand  long
            end
  code

  case self.Packet.PacketType
  of NET:SimpleAsyncOpenSuccessful
    ! Our asynchronous open worked
    ! Don't send data here, because the server needs to send us data first.
    self._HostIPAddress = self._Connection.LocalIP   ! Record our IP address as seen by the FTP Server
  of NET:SimplePartialDataPacket
    ! We received data
    do ProcessDataPacket

    if self.PassiveMode                                     ! Reset the Data connections timeout (passive mode only)
        NetSimpleResetClientIdleTimeout(self.DataConnection._Connection.Socket, self.DataConnection._Connection.SockID)
    end
  of NET:SimpleIdleConnection
    ! Connection has been
    ! idle for period set
    ! in .InactiveTimeout
    if self.PassiveMode = 1
      if self.DataConnection.OpenFlag or self.DataConnection.AsyncOpenBusy
        self.Log ('NetFTPClientControl.Process', 'Received a Control connection Timeout, but the PASSIVE Data connection is still connected. So will ignore for now')
        NetSimpleResetClientIdleTimeout(self._Connection.Socket, self._Connection.SockID)
        return
      end
    else
      if records(self.DataConnection.qServerConnections) > 0
        self.Log ('NetFTPClientControl.Process', 'Received a Control connection Timeout, but the ACTIVE Data connection is still connected. So will ignore for now')
        return
      end
    end
    self.Log ('NetFTPClientControl.Process', 'Received a legitimate Control connection Timeout.')
    self.ProcessIdleConnection()
  end

! ---------------------------------------------------------------------------------------------
ProcessDataPacket Routine
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      if self.packet.binDataLen > 255
        self.Log ('NetFTPClientControl.Process', 'FTP Control received [' & self.packet.binData [1 : 255] & '] ... (clipped at 255 bytes)')
      elsif self.packet.binDataLen > 0
        self.Log ('NetFTPClientControl.Process', 'FTP Control received [' & self.packet.binData [1 : self.packet.binDataLen] & ']')
      end
    end
  ****

  loc:AnotherLineOfDataStartsHere = 0                       ! Initialise

  if self._ReplySoFarLen > 0                                ! Store packet into ReplySoFar
    if size (self._ReplySoFar) < (self._ReplySoFarLen + self.packet.binDataLen)
      self.error = ERROR:InvalidStringSize
      self.WinSockError = 0
      self.SSLError = 0
      self.FTPError = 0
      self._CallErrorTrap ('Error. All of the memory of the ReplySoFar String has been used. It is too small for the reply we received', 'NetFTPClientControl.Process')
      return
    end

    if self.packet.binDataLen > 0                           ! Append data from self.packet to self._ReplySoFar
      self._ReplySoFar[(self._ReplySoFarLen + 1): (self._ReplySoFarLen + self.packet.binDataLen)] = self.packet.bindata [1 : self.packet.binDataLen]
      self._ReplySofarLen += self.packet.binDataLen
    end
  else
    if self.packet.binDataLen > 0
      self._ReplySoFar = self.packet.binData [1 : self.packet.binDataLen]
      self._ReplySoFarLen = self.packet.binDataLen
    else
      return ! no data received
    end
  end

  loop
    if loc:AnotherLineOfDataStartsHere <> 0
      self._ReplySoFar = self._ReplySoFar[loc:AnotherLineOfDataStartsHere : self._ReplySoFarLen]
      self._ReplySofarLen = (self._ReplySoFarLen - loc:AnotherLineOfDataStartsHere) + 1
    end

    if self._ReplySoFarLen > 0
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          if self.packet.binDataLen > 0
            self.Log ('NetFTPClientControl.Process', 'FTP Whole Line = [' & self._ReplySoFar [1 : self._ReplySoFarLen] & ']')
          end
        end
      ****
    else
      ! No line data
    end

    do CheckForReplyCode

    self._NeedToTrapError = 0
    self._replyProcessed = 0                                ! This is set to 1 when a function processes the reply code

    self._HandleRepliesIndependently()                      ! Handle replies that lead to only one course of action
                                                            ! INDEPENDENT of COMMAND currently being executed
    if self._replyProcessed = 0
      self._TrapErrorsInProcess()                           ! Send FATAL ERRORS to ERRORTRAP
    end

    loc:unknownCommand = 0                                    ! This will be set if the command is not one that is recognised
    !self.log('Process','lower(clip(self._Command))=' & lower(clip(self._Command)) & ' self._replyProcessed=' & self._replyProcessed)
    if self._replyProcessed = 0
      case lower(clip(self._Command))                       ! Handle replies that require unique course of action (DEPENDS on COMMAND currently being executed)
      of 'aborting'
        self._HandleAbortReplies()
      of 'appendtofile'
        self._HandleAppendToFileReplies()
      of 'changedir'
        self._HandleChangeDirReplies()
      of 'changedirup'
        self._HandleChangeDirUpReplies()
      of 'deletefile'
        self._HandleDeleteFileReplies()
      of 'getcurrentdir'
        self._HandleGetCurrentDirNameReplies()
      of 'getdirlisting'
        self._HandleGetDirListingReplies()
      of 'getremotefile'
        self._HandleGetRemoteFileReplies()
      of 'makedir'
        self._HandleMakeDirReplies()
      of 'noop'
        self._HandleNoOpReplies()
      of 'putfile'
        self._HandlePutFileReplies()
      of 'removedir'
        self._HandleRemoveDirReplies()
      of 'rename'
        self._HandleRenameReplies()
      of 'quit'
        self._HandleQuitReplies()
      else
        loc:unknownCommand = 1
      end
    end

    if self._NeedToTrapError
      self._NeedToTrapError = 0
      self._CallErrorTrap(self._ErrorString,self._ErrorFunction)
      self._state = lower(clip(self._ErrorString))
    else
      ! Added the ignoreUnknownReplies option, to allow NetTalk
      ! to work with FTP server that send arbitrary informational replies.
      if self._replyProcessed = 0
        if loc:unknownCommand = 0 and self.IgnoreUnknownReplies = 0     ! [Sean June06]
          self.error = ERROR:GeneralFTPError
          self.WinSockError = 0
          self.SSLError = 0
          self.FTPError = ERROR:FTP:UnknownReplyFormat
          self._CallErrorTrap('Unknown Reply from FTP Server. Reply = ' & self._ReplyCode & |
                              ' Command = ' & clip (self._command), 'NetFTPClientControl.Process')
        end
      end
    end
  until loc:AnotherLineOfDataStartsHere = 0

  if loc:endFound = true
    self._ReplySoFarLen = 0
  end

! ---------------------------------------------------------------------------------
CheckForReplyCode Routine
  ! This routine parses the reply. There are two types of replies.
  ! Single line ones and multiple line ones.
  ! This routine will call a return to wait for more data if the whole reply
  ! has not been received.
  Do CheckReply

! ------------------------------------------------------------------------------------------------
CheckReply Routine
  loc:AnotherLineOfDataStartsHere = 0
  if self._ReplySoFar < 6                       ! Wait for more data
    return                    ! Wait for more data
  end
  self._ReplyCode = self._ReplySoFar[1:3]       ! Store three digit FTP reply code for processing

  loc:EndFound = false                          ! Initialise
  if self._ReplySoFar[4] = ' '                  ! Single line reply
    loc:Pos2 = instring (clip(self._LineBreak), self._ReplySoFar [5 : self._ReplySoFarLen],1,1)
    if loc:Pos2 <> 0
      loc:EndFound = true
      if (loc:Pos2 + 4) < (self._ReplySoFarLen - 1)
        loc:AnotherLineOfDataStartsHere = loc:Pos2 + 2 + 4
      end
    else
      return                                    ! Wait for more data
    end
  elsif self._ReplySoFar[4] = '-'               ! This is a muli-line reply
    self.Log ('NetFTPClientControl.Process', 'This data is in a multi-line format. Will look for the last line.')
    ! MultiLine Data
    loc:Pos1 = 1
    loop
      loc:Pos1 = instring (clip(self._LineBreak) & self._ReplyCode, self._ReplySoFar [1 : self._ReplySoFarLen], 1, loc:Pos1)
      if loc:Pos1 = 0
        return ! wait for more data to arrive
      else
        ! Reply Code found again
        if self._ReplySoFarLen < (loc:Pos1 + 7)
          return
        else
          if self._ReplySoFar [loc:Pos1 + 5] = ' '
            ! Last Reply Code found.
            loc:Pos2 = instring (clip(self._LineBreak), self._ReplySoFar [loc:Pos1 + 6 : self._ReplySoFarLen],1,1)
            if loc:Pos2 <> 0
              loc:Pos2 += loc:Pos1 + 6 - 1
              loc:EndFound = true
              if (loc:Pos2 + 1) < self._ReplySoFarLen
                loc:AnotherLineOfDataStartsHere = loc:Pos2 + 2
              end
              self.Log ('NetFTPClientControl.Process', 'Found the last line.')
              break
            end
          elsif self._ReplySoFar [loc:Pos1 + 5] = '-'
            ! Keep looping, we got another line like '230-'
          else
            self.Log ('NetFTPClientControl.Process', 'Error. Illegal character after 2nd reply code received', NET:LogError)
            self.Log ('NetFTPClientControl.Process', 'Illegal character = [' & self._ReplySoFar[loc:Pos1 + 5] & ']', NET:LogError)
          end
        end
      end
      loc:Pos1 += 1
    end  ! Loop
  else
    ! Illegal character received
    self.Log ('NetFTPClientControl.Process', 'Error. Illegal 3rd character received', NET:LogError)
    self.Log ('NetFTPClientControl.Process', 'Illegal character = [' & self._ReplySoFar[4] & ']', NET:LogError)
    return
  end

  ! Respond
  if loc:EndFound = false
    ! End not found
    return                             ! Wait for the rest of the data
  end

! ---------------------------------------------------------------
  !  110 Restart marker reply.
  !      In this case, the text is exact and not left to the
  !      particular implementation; it must read:
  !           MARK yyyy = mmmm
  !      Where yyyy is User-process data stream marker, and mmmm
  !      server's equivalent marker (note the spaces between markers
  !      and "=").
  !  120 Service ready in nnn minutes.
  !  125 Data connection already open; transfer starting.
  !  150 File status okay; about to open data connection.
  !  200 Command okay.
  !  202 Command not implemented, superfluous at this site.
  !  211 System status, or system help reply.
  !  212 Directory status.
  !  213 File status.
  !  214 Help message.
  !      On how to use the server or the meaning of a particular
  !      non-standard command.  This reply is useful only to the
  !      human user.
  !  215 NAME system type.
  !      Where NAME is an official system name from the list in the
  !      Assigned Numbers document.
  !  220 Service ready for new user.
  !  221 Service closing control connection.
  !      Logged out if appropriate.
  !  225 Data connection open; no transfer in progress.
  !  226 Closing data connection.
  !      Requested file action successful (for example, file
  !      transfer or file abort).
  !  227 Entering Passive Mode (h1,h2,h3,h4,p1,p2).
  !  230 User logged in, proceed.
  !  250 Requested file action okay, completed.
  !  257 "PATHNAME" created.
  !
  !  331 User name okay, need password.
  !  332 Need account for login.
  !  350 Requested file action pending further information.
  !
  !  421 Service not available, closing control connection.
  !      This may be a reply to any command if the service knows it
  !      must shut down.
  !  425 Can't open data connection.
  !  426 Connection closed; transfer aborted.
  !  450 Requested file action not taken.
  !      File unavailable (e.g., file busy).
  !  451 Requested action aborted: local error in processing.
  !  452 Requested action not taken.
  !      Insufficient storage space in system.
  !  500 Syntax error, command unrecognized.
  !      This may include errors such as command line too long.
  !  501 Syntax error in parameters or arguments.
  !  502 Command not implemented.
  !  503 Bad sequence of commands.
  !  504 Command not implemented for that parameter.
  !  530 Not logged in.
  !  532 Need account for storing files.
  !  550 Requested action not taken.
  !      File unavailable (e.g., file not found, no access).
  !  551 Requested action aborted: page type unknown.
  !  552 Requested file action aborted.
  !      Exceeded storage allocation (for current directory or
  !      dataset).
  !  553 Requested action not taken.
  !      File name not allowed.


!------------------------------------------------------------------------------
NetFTPClientControl.Done Procedure
  code

!------------------------------------------------------------------------------
NetFTPClientControl.Send Procedure  ()
  code
    if self.packet.binDataLen > 255
       self.Log ('NetFTPClientControl.Send', 'Sending FTP Control [' & self.packet.binData [1 : 255] & ']... (clipped at 255 bytes)')
    elsif self.packet.binDataLen > 0
       self.Log ('NetFTPClientControl.Send', 'Sending FTP Control [' & self.packet.binData [1 : self.packet.binDataLen] & ']')
    end
    parent.Send()

!------------------------------------------------------------------------------
NetFTPClientControl.NoOp Procedure  ()
  code
  self.log('NetFTPClientControl.NoOp','self._Command=' & clip(self._Command) & ' self._ControlDone=' & self._ControlDone & ' self._DataDone=' & self._DataDone)
  case lower(clip(self._Command))
  of 'getremotefile'
  orof 'putfile'
  orof 'appendtofile'
  orof 'getdirlisting'
    ! allowed to send a noop when busy with data port commands, _command property not changed though.
    if (self._ControlDone = 0 and self._DataDone = 0) or self.busy = 0 ! in case control declared finished before data finished.
      self._NoOp()
    Else
      self.LastNoop = clock()
    End
  else
    if self.Busy
      ! Ignore command - as another command was busy
      self.error = ERROR:AlreadyBusy
      self.WinSockError = 0
      self.SSLError = 0
      self.FTPError = 0
      self._CallErrorTrap ('Can''t do a NoOp command, as the FTP object is already busy', 'NetFTPClientControl.NoOp')
      self.LastNoop = clock()
      return
    end
    self._NoOp()
    self._Command = 'NoOp'
  end
  return

!------------------------------------------------------------------------------
! Basically the NoOp Command is a Non-Operational command that can be used
! to keep a FTP connection alive (ie stop the server closing it down).
! Because this function has no use to a connection that has closed
! it only opens a connection if the self.OpenNewContolConnetion = 1
! and not just if the connection is no longer open.
NetFTPClientControl._NoOp Procedure
  code
  if self.OpenFlag
    if self.busy = 0
      self._Command = 'NoOp'
      self.Busy = 1
    elsif self.NoopInterval = 0
      return
    end
    self._State = 'NoOp'
    clear(self.Packet)
    self.Packet.BinData = 'NOOP' & '<13,10>'
    self.Packet.BinDataLen = len(clip(self.Packet.BinData))
    self.send()
    self.LastNoop = clock()
  end

!------------------------------------------------------------------------------
NetFTPClientControl._HandleNoOpReplies Procedure
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleNoOpReplies()'

  case self._ReplyCode
  of '200'
    self._replyProcessed = 1
    self._CallDone()
  end

!-----------------------------------------------------------
! csTODO: This is probably not needed, can be handled in _HandleRepliesIndependently()
NetFTPClientControl._HandleAuth Procedure ()
  code
    self._ErrorFunction = 'NetFTPClientControl._HandleProt'
    case self._ReplyCode
    of '234'                                                ! AUTH successful
        ! csTODO: call _SSLSwitchToSSL
    else                                                    ! AUTH failed, cannot open a secure connection, fall back to a standard connection
        self.SSL = false
    end

!-----------------------------------------------------------
NetFTPClientControl._HandlePbsz Procedure ()
pn              equate('NetFTPClientControl._HandlePbsz')
  code
    self._ErrorFunction = pn
    self.Log(pn, 'Reply code ' & self._replyCode)

    case self._replyCode
    of '200'
        self._state = 'pbsz'
        if self.DataConnection.ssl = 1                      ! Issue a PROT command to secure the connection
            self.Prot()
        end
    else
        self.DataConnection.ssl = 0                         ! A secure connection cannot be used
    end
    self._replyProcessed = 1



!-----------------------------------------------------------
NetFTPClientControl._HandleProt Procedure()
pn              equate('NetFTPClientControl._HandleProt')
  code
    self._ErrorFunction = pn
    self.Log(pn, 'Reply code: ' & self._replyCode)
    case self._ReplyCode
    of '200'                                                ! Success
        self._state = 'protected'
    else                                                    ! Possible failure codes include: 504, 536, 503, 534, 431, 500, 501, 421, 530
        self.DataConnection.ssl = 0
    end
    self._replyProcessed = 1                                ! Always set this, a failure simply disables SSL for the data connection


!-----------------------------------------------------------
NetFTPClientControl._HandleAbortReplies Procedure
  code
    self._ErrorFunction = 'NetFTPClientControl._HandleNoOpReplies()'

    case self._ReplyCode
    of '425'
    orof '426'
        self._replyProcessed = 1
        self._CallDone()
    end

!------------------------------------------------------------------------------
NetFTPClientControl._CommandPrelim Procedure  (string pCommand,long pIncludeDataLink)
retVal                              long
currentSuppressErrorMsg             byte
  code
  self.Log('NetFTPClientControl._CommandPrelim', 'pCommand: ' & clip(pCommand) & ' self.PassiveMode=' & self.PassiveMode)
  RetVal = 0
  self._Command = pCommand
  self.TransferState = ''
  self.Busy = 1
  self._cancelling = 0

  if self.OpenNewControlConnection                          ! need to open new control connection
    self.OpenNewControlConnection = 0
    self.LoggedIn = 0                                       ! user needs to log in once new connection is established
    if self.AsyncOpenBusy = 1                               ! Busy with Async Open, so abort
      self.abort()
    end
    if self.OpenFlag                                        ! if control connection open
       self.Close()                                         ! close current connection
    end
    if self.PassiveMode = 1
      if self.DataConnection.OpenFlag or self.DataConnection.AsyncOpenBusy
        self.DataConnection.abort()   ! Abort current connection
      end
      self._Cancelling = 0
    else
      loop while (records (self.DataConnection.qServerConnections) > 0)
        self._Cancelling = 1 ! Set this if there are connections
        get (self.DataConnection.qServerConnections, 1)
        self.DataConnection.AbortServerConnection(self.DataConnection.qServerConnections.Socket, self.DataConnection.qServerConnections.SockID)
      end
      if self.DataConnection.OpenFlag
        self.DataConnection.Abort()
      end
    end
  end

  if not(self.OpenFlag)                                     ! control connection is closed
    if self.AsyncOpenBusy
      ! Hold on - just wait for Async Open to work or fail - do nothing
    else
      self.Open(self.ServerHost,self.ControlPort)
    end
  elsif not(self.LoggedIn)                                  ! current user is not logged in
    self._EnterNewUser()
  elsif pIncludeDataLink AND not(self.DataConnection._DataPortIncremented)      ! Data Port address needs to be incremented for
    if self.DataConnection.OpenFlag or self.DataConnection.AsyncOpenBusy
      self.DataConnection.abort()                           ! abort old data connection
    end

    self.Log('NetFTPClientControl._CommandPrelim', ' self.PassiveMode=' & self.PassiveMode)

    if self.PassiveMode
      if self._State <> 'passive'
        self.Packet.BinData = 'PASV<13,10>'
        self.Packet.BinDataLen = len (clip(self.Packet.BinData))
        self.send()
        self._State = 'passive'
      end
    else
      ! Active mode - open a listening port
      self.DataConnection._IncrementDataPort()
      currentSuppressErrorMsg = self.DataConnection.SuppressErrorMsg
      self.DataConnection.SuppressErrorMsg = 1
      loop while not(self.DataConnection.OpenFlag)
        self.DataConnection.Open('',self.DataPort) ! ACTIVE MODE = Synchronous command
        if self.DataConnection.Error                               ! if cannot listen on current data port
          self.DataConnection._IncrementDataPort()                 ! increment data port address and try again
        end
      end
      self.DataConnection.SuppressErrorMsg = currentSuppressErrorMsg
      self._PortCommand()
      self.DataConnection._DataPortIncremented = 1
    end
  else
    RetVal = 1
  end
  return RetVal

!------------------------------------------------------------------------------
NetFTPClientControl._OpenPassiveDataConnection Procedure
locbytes        cstring(5),DIM(6)
locIP_Address   cstring(20)
Lp              long
OK              byte
byteNum         byte
pn              equate('_OpenPassiveDataConnection')
  code
    self.Log(pn, '')
    OK = False
    locIP_Address = ''
    byteNum = 0
    loop lp = 1 to self.Packet.BinDataLen
        case self.Packet.BinData[lp]
        of '('
            byteNum = 1
        of ','
            byteNum += 1
        of ')'
            if byteNum = 6
                !locIP_Address = locbytes[1] & '.' & locbytes[2] & '.' & locbytes[3] & '.' & locbytes[4]
                locIP_Address = self.packet.fromip ! new in 7.11 - Some servers return their local IP address, but chances are the data port and server port are really on the same IP from our point of view.
                self.DataPort = locbytes[5] * 256 + locbytes[6]
                OK = True
            end
            break
        else
          if inrange(byteNum,1,6)
                locbytes[byteNum] = locbytes[byteNum] & self.Packet.BinData[Lp]
          end
        end
    END
    if OK
        self.DataConnection.Open(locIP_Address, self.DataPort)  ! Asynchronous Open
    end
    return
!------------------------------------------------------------------------------
NetFTPClientControl.CalcProgress Procedure
  code
  if clock() < self.LastProgressUpdate then self.LastProgressUpdate = clock().
  if clock() < self.LastNoop then self.LastNoop = clock().
  if (self.ProgressUpdateInterval) and (self.ProgressUpdateNow = 0) and (clock() - self.LastProgressUpdate < self.ProgressUpdateInterval)
    ! Do Nothing
    ! Only update every ProgressInterval (sec/100)
  else
    self.ProgressUpdateNow = 0
    self.LastProgressUpdate = clock()
    self.CalcProgressOptimized()     ! This is where all the code is processed
  end
  if self.ProgressPercent > 0 and self.ProgressPercent < 100 and self.NoopInterval and (clock() - self.LastNoop > self.NoopInterval)
    self.log('NetFTPClientControl.CalcProgress','Calling NOOP')
    self.noop()
  end
  case self._Command
  of 'GetRemoteFile'
  orof 'PutFile'
    if self.openflag = 0 and self._datadone = 1 and self._fromcalldone = 0
      self._CallDone
    End
  End

!------------------------------------------------------------------------------
NetFTPClientControl._CallDone Procedure
  code
  self.Log ('NetFTPClientControl._CallDone', 'Done Called for command = ' & lower(clip(self._Command)) & ', _waitingForComplete set back to 0.')
  self.Busy = 0
  self._waitingForComplete = 0        ! March 2007: This indicates to the Control that there is nothing left for the data connection to do.
  If self.timerSet
    0{prop:timer} = self.TimerWas
    self.timerWas = 0
    self.timerSet = 0
  End
  self.ProgressUpdateNow = 1
  self._fromcalldone = 1
  self.CalcProgress()
  self._fromcalldone = 0
  self.TransferState = ''
  self.Done()
!------------------------------------------------------------------------------
NetFTPClientControl.CalcProgressOptimized Procedure
  code
  ! This method is called from CalcProgress
  ! And is called less often so that performance is not impacted

  if self._Command = 'PutFile'
    if self.ExpectedFileSize <> 0
      self.DataConnection.GetInfo(2, self._DataSocket, self._DataSockID)
      if self.bytesLeftToSend <> self.bytesLeftToSendToDLL + self.DataConnection.OutgoingDLLPacketsbytes
        self.bytesLeftToSend = self.bytesLeftToSendToDLL + self.DataConnection.OutgoingDLLPacketsbytes
        self.ProgressPercent = ((self.ExpectedFileSize - (self.BytesLeftToSend)) / self.ExpectedFileSize) * 100
        if self.ProgressControl
          if self.ProgressControl{prop:progress} <> self.ProgressPercent
            self.ProgressControl{prop:progress} = self.ProgressPercent
            display(self.ProgressControl)
          end
        end
      end
    else
      self.ProgressPercent = 0
      if self.ProgressControl
        if self.ProgressControl{prop:progress} <> 0
          self.ProgressControl{prop:progress} = 0
          display(self.ProgressControl)
        end
      end
    end
  elsif self._Command = 'GetRemoteFile'
    if self.ExpectedFileSize <> 0
      self.ProgressPercent = ((self.ExpectedFileSize - self.BytesLeftToReceive) / self.ExpectedFileSize) * 100
    else
      self.ProgressPercent = 0
    end
    if self.ProgressControl
      if self.ProgressControl{prop:Progress} <> self.ProgressPercent ! Percentage Progress
        self.ProgressControl{prop:Progress} = self.ProgressPercent  ! Percentage Progress
        display(self.ProgressControl)
      end
    else
      if self.ProgressControl{prop:Progress} <> 0
        self.ProgressControl{prop:Progress} = 0
        display(self.ProgressControl)
      end
    end
  end

!------------------------------------------------------------------------------
NetFTPClientControl._CallErrorTrap Procedure  (string errorStr,string functionName)
temp2     string(512)
  code
  ! Stop the current command
  self.Busy = 0
  self.Log ('NetFTPClientControl._CallErrorTrap', 'Set _waitingComplete back to zero, there is no more data waiting.')

  self._waitingForComplete = 0        ! March 2007: This indicates to the Control that there is nothing left for the data connection to do.

  ! This method traps errors that occur in NetFTPClientControl as well as in NetFTPClientData
  !
  ! When an FTP-specific error occurs, self.Error is set to 'General FTP Error'
  ! The FTPError property will be set to the reply code returned by the FTP Server
  ! List of FTP reply codes and corresponding errors:
  !
  ! 421  Service not available, closing control connection.  This may be a reply to any command if the service
  !      knows it must shut down.
  ! 426  Connection closed; transfer aborted.
  ! 450  Requested file action not taken.  File unavailable (e.g. file busy).
  ! 451  Requested action aborted: local error in processing.
  ! 452  Requested action not taken.  Insufficient storage space in system.
  ! 500  Syntax error, command unrecognized.  This may include errors such as command line too long.
  ! 501  Syntax error in parameters or arguments.
  ! 502  Command not implemented.
  ! 503  Bad sequence of commands.
  ! 504  Command not implemented for that parameter.
  ! 530  Not logged in.
  ! 532  Need account for storing files.
  ! 550  Requested action not taken.  File unavailable (e.g. file not found, no access).
  ! 552  Requested file action aborted.  Exceeded storage allocation (for current directory or dataset).
  ! 553  Requested action not taken.  File name not allowed.

  ! Additional error codes for secure connections:
  ! 336 Username okay, need password.  Challenge is "...."
  ! 431 Need some unavailable resource to process security.
  ! 533 Command protection level denied for policy reasons.
  ! 534 Request denied for policy reasons.
  ! 535 Failed security check (hash, sequence, etc).
  ! 536 Requested PROT level not supported by mechanism.
  ! 537 Command protection level not supported by security mechanism.


  ! FTPError may also contain one of these values:
  !
  ! -1   Invalid number of parameters.
  ! -2   Cannot open file.
  ! -3   Cannot write to file.
  ! -4   Unknown format of directory listing.
  ! -5   Unknown reply format.
  ! -6   see below
  ! -7   see below

  temp2 = self.InterpretError()

  self.ErrorString = ''

  case (self.FTPError)
  of ERROR:FTP:TransferAborted
    self.ErrorString = 'Transfer Aborted'
  of ERROR:FTP:FileUnavailable
    self.ErrorString = 'File Unavailable'
  of ERROR:FTP:LocalProcessingError
    self.ErrorString = 'Local Processing Error'
  of ERROR:FTP:InsufficientStorageSpace
    self.ErrorString = 'Insufficient Storage Space'
  of ERROR:FTP:CommandUnrecognized
    self.ErrorString = 'Command Unrecognised'
  of ERROR:FTP:SyntaxError
    self.ErrorString = 'Syntax Error'
  of ERROR:FTP:CommandNotImplemented
    self.errorString = 'Command Not Implemented'
  of ERROR:FTP:BadSequenceOfCommands
    self.ErrorString = 'Bad Sequence of Commands'
  of ERROR:FTP:InvalidParameter
    self.ErrorString = 'Invalid Parameter'
  of ERROR:FTP:NotLoggedIn
    self.ErrorString = 'Not Logged In'
  of ERROR:FTP:NeedAccountToStoreFiles
    self.ErrorString = 'Need Account To Store Files'
  of ERROR:FTP:FileAccessError
    self.ErrorString = 'File Access Error'
  of ERROR:FTP:ExceededStorageAllocation
    self.ErrorString = 'Exceeded Storage Allocation'
  of ERROR:FTP:FileNameNotAllowed
    self.ErrorString = 'File Name Not Allowed'
  of ERROR:FTP:InvalidNumberOfParameters
    self.ErrorString = 'Invalid Number of Parameters'
  of ERROR:FTP:CannotOpenFile
    self.ErrorString = 'Cannot Open File'
  of ERROR:FTP:CannotWriteToFile
    self.ErrorString = 'Cannot Write To File'
  of ERROR:FTP:UnknownFormatOfDirListing
    self.ErrorString = 'Unknown Format Of Dir Listing'
  of ERROR:FTP:UnknownReplyFormat
    self.ErrorString = 'Unknown Reply Format'
  of ERROR:FTP:ServiceNotReady
    self.ErrorString = 'Service Not Ready'
  of ERROR:FTP:ServerClosingDown
    self.ErrorString = 'Server Closing Down'
  of ERROR:FTP:ControlConnectionClosed              ! -6
    self.ErrorString = 'Server closed the connection'
  of ERROR:FTP:ControlConnectionClosedInPassive     ! -7
    self.ErrorString = 'Server closed the connection. Maybe the Passive mode is not supported.'
  end

  if self.ErrorString <> ''
    self.ErrorString = clip (self.ErrorString) & '.'
  end


  if (self.error = 0) or (self.error = ERROR:GeneralFTPError)
    self.ErrorString = clip (self.ErrorString) & clip(errorStr) ! & '.'
  else
    self.ErrorString = clip (self.ErrorString) & |
                       clip(errorStr) & '|' & 'The error number was ' & |
                       self.error & ' which means ' & clip(temp2) & '.'
  end

  if self.packet.PacketType = NET:SimpleAsyncOpenFailed
    self.ErrorString = 'The connection to the FTP Server could not be opened. ' & clip (self.ErrorString)
  end

  if self.suppressErrorMsg = 0
    self.Log ('NetFTPClientControl._CallErrorTrap', |
                   'Error = ' & self.error & ' / ' & self.FTPError & |
                   ' = ' & clip(self.ErrorString) & |
                   ' : ' & clip (temp2) & |
                   ' Command = ' & clip (self._command) & |
                   ' Function = ' & clip(functionName) & |
                   ' Socket = ' & self._connection.socket & |
                   ' SockID = ' & self._connection.sockID & |
                   ' Server = ' & clip (self._connection.Server) & |
                   ' Port = ' & clip (self._connection.Port), NET:LogError)

    message('An FTP error has occurred:||' & clip(self.ErrorString) & |
            '||The error number was ' & self.error & ' / ' & self.FTPError & |
            ' which means ' & clip(temp2) & '.' & |
            '||Command = ' & clip (self._command) & |
            '||Error occurred in function: ' & clip(functionName), |
            clip(NET:ErrorTitle), Icon:Asterisk)
  else                                       ! Only log to file
    self.Log ('NetFTPClientControl._CallErrorTrap', '<<no display> ' & |
              'Error = ' & self.error & '/' & self.FTPError & |
              ' = ' & clip(self.ErrorString) & |
              ' : ' & clip (temp2) & |
              ' Command = ' & clip (self._command) & |
              ' Function = ' & clip(functionName) & |
              ' Server = ' & clip (self._connection.Server) & |
              ' Port = ' & clip (self._connection.port), NET:LogError)
  end

  self.ErrorTrap(errorStr, functionName)

  ! this is a simple error handler. It means every error received interrupts your program
  ! (which you may want to override). The actual message is provided by the
  ! InterpretError method, which can also be overridden.

!------------------------------------------------------------------------------
NetFTPClientControl._HandleQuitReplies Procedure
  code
  self._ErrorFunction = 'NetFTPClientControl._HandleQuitReplies()'

  case self._ReplyCode
  of '221'
    self._replyProcessed = 1
    self._CallDone()
  end

!------------------------------------------------------------------------------
NetFTPClientControl.ProcessIdleConnection Procedure
  code
  ! This method is called when the FTP Control Connection has been idle for the
  ! number of hundreths of a second in the self.InActiveTimeout

  ! To stop this .abort from happening, you can either set self.InActiveTimeout = 0, after
  ! the Init, or you can put a return in your derived .ProcessIdleConnection method before
  ! the parent call

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientControl.ProcessIdleConnection', |
                     'Warning. FTP Control Connection has been idle for more than ' & self.InActiveTimeout & ' hs.' & |
                     ' Will call Abort().' & |
                     ' Socket = ' & self._connection.socket & |
                     ' SockID = ' & self._connection.sockID & |
                     ' Server = ' & clip (self._connection.Server) & |
                     ' Port = ' & clip (self._connection.Port), NET:LogError)
    end
  ****

  if self._waitingForComplete                                ! March 2007: Data is still being tranferred, either on the Data connection object level of the DLL level.
    self.log('NetFTPClientControl.ProcessIdleConnection','Calling NOOP')
    self.NoOp()
    self.Log('NetFTPClientControl.ProcessIdleConnection', 'Ignored timeout - the Data connection or DLL are still busy transferring data.')
    return
  end

  self._state = 'aborting'
  self._Command = 'aborting'
  if self.OpenFlag or self.AsyncOpenBusy ! Prevent recursive calls to .ErrorTrap if Abort fails
    self.abort()
  end

  self.DataConnection.GetInfo()
  if self.PassiveMode = 1
    if self.DataConnection.OpenFlag or self.DataConnection.AsyncOpenBusy ! Prevent recursive calls to .ErrorTrap if Abort fails
      self.DataConnection.abort()
    end
    self._Cancelling = 1
  else
    loop while (records (self.DataConnection.qServerConnections) > 0)
      self._Cancelling = 1                                  ! Set this if there are connections
      get (self.DataConnection.qServerConnections, 1)
      self.DataConnection.AbortServerConnection(self.DataConnection.qServerConnections.Socket, self.DataConnection.qServerConnections.SockID)
    end
    if self.DataConnection.OpenFlag or self.DataConnection.AsyncOpenBusy ! Prevent recursive calls to .ErrorTrap if Abort fails
      self.DataConnection.Abort()
    end
  end
  self.ProgressPercent = 0
  if self.ProgressControl
    self.ProgressControl{PROP:RangeLow} = 0
    self.ProgressControl{PROP:RangeHigh} = 100
    self.ProgressControl{prop:Hide} = 0
    self.ProgressControl{prop:Progress} = 0
  end
  self.ExpectedFileSize = 0
  self.bytesLeftToReceive = 0
  self.bytesLeftToSend = 0
  self.bytesLeftToSendToDLL = 0
  self.Busy = 0

  self.Error = ERROR:IdleTimeout
  self.WinSockError = 0
  self.SSLError = 0
  self.FTPError = 0
  self._Command = 'IdleTimeout' ! This may 'loose' the last command issued, but the last command issued may have completed fine, so better to make a new command up here.
  self.ErrorString = 'The FTP connection was idle and was closed.'
  self._CallErrorTrap ('The FTP connection was idle and was closed', 'NetFTPClientControl.ProcessIdleConnection')

!------------------------------------------------------------------------------
NetFTPClientData._IncrementDataPort Procedure
  code
  if self.ControlConnection.DataPort > self.ControlConnection.UpperBoundDataPort     ! if port reaches upper bound address
    self.ControlConnection.DataPort = self.ControlConnection.LowerBoundDataPort      ! set port back to lower bound address
  elsif self.ControlConnection.DataPort < self.ControlConnection.LowerBoundDataPort  ! if port is set to be below lower bound address
    self.ControlConnection.DataPort = self.ControlConnection.LowerBoundDataPort      ! set port to lower bound address
  else
    self.ControlConnection.DataPort += 1                ! increment port address by 1
  end
!------------------------------------------------------------------------------
NetFTPClientData._SendFile Procedure
local           group, pre (loc)
clocktime         long
bytesRead         long
                end
  code
  self.Packet.ToIP = self.Packet.FromIP            ! send packet to client that has established connection

  if self.ControlConnection.PassiveMode = 0
    self.ControlConnection._DataSocket = self.packet.OnSocket  ! Used for calculating FTP progress
    self.ControlConnection._DataSockID = self.packet.SockID    ! Used for calculating FTP progress
  else
    self.ControlConnection._DataSocket = self._connection.Socket  ! Used for calculating FTP progress
    self.ControlConnection._DataSockID = self._connection.SockID    ! Used for calculating FTP progress
  end

  NetFTPClientReadFileName = self.ControlConnection._LocalFileName
  open(NetFTPClientReadFile,20h)                   ! open file for read only, deny write
  if errorcode()
    self.Error = ERROR:GeneralFTPError
    self.WinSockError = 0
    self.SSLError = 0
    self.ControlConnection.FTPError = ERROR:FTP:CannotOpenFile
    self.ControlConnection.SSLError = 0
    self.ControlConnection.WinSockError = 0
    self._CallErrorTrap('Cannot open file for reading','NetFTPClientData._SendFile()')
    return
  end
  NetFTPClientReadFileOpen = true

  self.ControlConnection.ExpectedFileSize = bytes(NetFTPClientreadfile)
  self.ControlConnection.bytesLeftToSend = self.ControlConnection.ExpectedFileSize
  self.ControlConnection.bytesLeftToSendToDLL = self.ControlConnection.ExpectedFileSize

  if self.ControlConnection.ProgressControl                          ! Update the Progress Control Control if there is one
    self.ControlConnection.ProgressControl{PROP:RangeLow} = 0        ! zero to 100 percent
    self.ControlConnection.ProgressControl{PROP:RangeHigh} = 100     ! zero to 100 percent
    self.ControlConnection.ProgressControl{prop:Hide} = 0
    self.ControlConnection.ProgressControl{prop:Progress} = 0
  end

  self.ControlConnection.ProgressUpdateNow = 1
  self.ControlConnection.CalcProgress()

  loc:ClockTime = clock()
  set(NetFTPClientReadFile)
  self.ControlConnection.TransferState = 'Loading'
  loop
    next(NetFTPClientReadFile)
    if errorcode()
      if self.ControlConnection.bytesLeftToSendtoDLL <> 0
        self.Error = ERROR:NotAllDataSent
        self.WinSockError = 0
        self.SSLError = 0
        self.ControlConnection.WinSockError = 0
        self.ControlConnection.SSLError = 0
        self._CallErrorTrap('Error occured while sending a file, not all data was sent','NetFTPClientData._SendFile()')
      end
      break
    end
    loc:bytesRead = bytes(NetFTPClientReadFile)
    self.ControlConnection.bytesLeftToSendtoDLL -= loc:bytesRead
    self.Packet.BinData = NetFTPClientReadFile.NetFTPClientReadFileRecord.NetFTPClientReadFileData
    self.Packet.BinDataLen = loc:bytesRead
    self.Send()
    if abs(clock() - loc:ClockTime) > 25  ! 1/4 second
      loc:ClockTime = clock()
      if self.ControlConnection.ProgressControl and self.ControlConnection.ProgressAllowLoading
        self.ControlConnection.ProgressControl{prop:progress} = ((self.ControlConnection.bytesLeftToSendtoDLL) / self.ControlConnection.ExpectedFileSize) * 100
        0{prop:text} = 0{prop:text}
      else
        self.ControlConnection.CalcProgress()
      end
    end
  end

  display()
  close(NetFTPClientReadFile)
  NetFTPClientReadFileOpen = false
  if self.ControlConnection._DelayAfterStore
    OS_Sleep (1000) ! Delay a second. Prevents errors from CesarFTP 0.99e and prevents CesarFTP 0.99g from GPF'ing
  end
  if self.ControlConnection.PassiveMode
    self.close()
  else
    self.closeServerConnection(self.packet.OnSocket, self.packet.SockID)
  end
  if self.ControlConnection.ProgressControl                          ! Update the Progress Control Control if there is one
    self.ControlConnection.ProgressControl{prop:Progress} = 0
  end
  self.ControlConnection.TransferState = 'Uploading'
  self.ControlConnection.ProgressUpdateNow = 1
  self.ControlConnection.calcProgress()

!------------------------------------------------------------------------------
NetFTPClientData.ConnectionClosed Procedure
  code
  self.ControlConnection._waitingForComplete = 0        ! March 2007: This indicates to the Control that there is not data being sent
  self.log('NetFTPClientData.ConnectionClosed','self.ControlConnection._State=' & clip(self.ControlConnection._State))
  case self.ControlConnection._State
  of 'getremotefile'
    self._GetRemoteFileCompleted()
  of 'getdirlisting'
    self.ControlConnection._DataDone = 1
    self.ControlConnection.CalcProgress()
    if self.ControlConnection._ControlDone = 1
      self.ControlConnection._CallDone()
    end
  else
    self.ControlConnection.CalcProgress()
  end

!------------------------------------------------------------------------------
NetFTPClientData._GetRemoteFileCompleted  Procedure()
  code
    If self.ControlConnection._DataDone = 1
      return
    end
    if NetFTPClientWriteFileOpen
      close (NetFTPClientWriteFile)
      NetFTPClientWriteFileOpen = false
    else
      ! This could be a zero length file. If it is, then we must create a zero
      ! length file.
      if self.ControlConnection.bytesLeftToReceive = 0
        NetFTPClientWriteFileName = self.ControlConnection._LocalFileName
        open(NetFTPClientWriteFile)
        if not(errorcode())                              ! If destination file already exists in specified directory,
          close(NetFTPClientWriteFile)                   ! ovewrite that file with file received
          remove(NetFTPClientWriteFile)
        end
        create(NetFTPClientWriteFile)                    ! Create file
        if errorcode()
          self.Error = ERROR:GeneralFTPError
          self.WinSockError = 0
          self.SSLError = 0
          self.ControlConnection.WinSockError = 0
          self.ControlConnection.SSLError = 0
          self.ControlConnection.FTPError = ERROR:FTP:CannotOpenFile
          self.ControlConnection._State = 'cannot create destination file'
          self.ControlConnection._Command = ''
          self.ControlConnection._CallErrorTrap('Cannot create destination file','NetFTPClientData._GetRemoteFileCompleted()')
          if self.ControlConnection.OpenFlag or self.ControlConnection.AsyncOpenBusy ! New 7/7/2004 - Prevent recursive calls to .ErrorTrap if Abort fails
            self.ControlConnection.Abort()
          end
          return
        end
      end
    end
    self.Log ('NetFTPClientData._GetRemoteFileCompleted', 'bytesLeftToReceive = ' & self.ControlConnection.bytesLeftToReceive)
    if self.ControlConnection.bytesLeftToReceive > 0
      self.ControlConnection.Error = ERROR:NotAllDataReceived
      self.ControlConnection._CallErrorTrap('Error occured while receiving a file, not all data was received','NetFTPClientData.ConnectionClosed()')
      self.ControlConnection._Command = '' ! Stop doing the FTP get Remote File - the connection died.
      self.ControlConnection._Cancelling = 1
    else
      self.ControlConnection.ProgressUpdateNow = 1
      self.ControlConnection.CalcProgress()
      self.ControlConnection._DataDone = 1
      if self.ControlConnection._ControlDone = 1
        self.Log ('NetFTPClientData._GetRemoteFileCompleted',' Data & Control Object have finished. So we will call _CallDone()')
        self.ControlConnection._CallDone()
      else
        self.Log ('NetFTPClientData._GetRemoteFileCompleted',' Data object is finished - need to wait for Control object.')
      end
    end
!------------------------------------------------------------------------------
NetFTPClientData.Init Procedure  (ulong Mode=NET:SimpleClient)
  code
  self.SuppressErrorMsg = 1                 ! Suppress all errors on this FTP Data object.
  PARENT.Init(Mode)
  self.ControlConnection.BinaryTransfer = 1 ! BINARY data representation type will be used for file
                                            ! transfer by default

  self.AsyncOpenUse = 1                     ! Do not change this - uses AsyncOpenUse mode
  self.AsyncOpenTimeOut = 1200              ! 12 sec, open connection timeout
  self.InActiveTimeout = 9000               ! 90 seconds

!------------------------------------------------------------------------------
NetFTPClientData.Process Procedure
  code
    ! Call the DLL function to reset the Control connection's time-out, otherwise the Control object will
    ! time out even though the data connection is still active
    NetSimpleResetClientIdleTimeout(self.ControlConnection._Connection.Socket, self.ControlConnection._Connection.SockID)

    self.ControlConnection._waitingForComplete = 1          ! This is set to inform the Control connection that the data connection has begun a task. It is cleared by the control connection when it completes or fails.

    case self.Packet.PacketType
    of NET:SimpleAsyncOpenSuccessful                        ! Our asynchronous open worked
        if self.ControlConnection.PassiveMode = 1
            Compile('****', NETTALKLOG=1)
            if self.LoggingOn
                self.Log('NetFTPClientData.Process', 'FTP Data Connection opened to Server = ' & Clip(self._connection.Server) & ' Port = ' & Clip(self._connection.Port))
            end
            ****
            self._DataPortIncremented = 1
            if self.ControlConnection._State = 'passive'
                self.ControlConnection._State = 'passive successful'
                self._DataPortIncremented = 1
                if self.ControlConnection.BinaryTransfer and (lower(self.ControlConnection._Command) <> 'getdirlisting')
                    self._CurrentDataRepType = 'i'          ! TYPE to binary
                else
                    self._CurrentDataRepType = 'a'          ! TYPE to ASCII - must use this for directory listing
                end
                self.ControlConnection._SetType()
            end
        end
    of NET:SimpleNewConnection                              ! We received a new connection (when in ACTIVE mode)
        Do ProcessNewIncomingConnection
    of NET:SimplePartialDataPacket                          ! We received data
        Do ProcessDataPacket
    of NET:SimpleIdleConnection                             ! Connection has been idle for period set in .InactiveTimeout
        self.ProcessIdleConnection()
    end

! --------------------------------------------------------------------------------------
ProcessNewIncomingConnection Routine
    Compile ('****', NETTALKLOG=1)
    if self.LoggingOn
        if self.packet.binDataLen > 1
            self.Log ('NetFTPDataControl.Process', 'FTP Data - New incoming connection from: ' & self.packet.FromIP)
        end
    end
    ****
    ! initial COMMAND sent to server determines the way in which
    ! data received via the data connection is to be handled

    case Lower(Clip(self.ControlConnection._Command))
    of 'putfile'orof 'appendtofile'
        self._ProcessPutFile()
    end


! --------------------------------------------------------------------------------------
ProcessDataPacket Routine
    Compile ('****', NETTALKLOG=1)
    if self.LoggingOn
        if self.packet.binDataLen > 1
            self.Log ('NetFTPDataControl.Process', 'FTP Data received. Size = ' & self.packet.binDataLen)
        end
    end
    ****

    ! initial COMMAND sent to server determines the way in which
    ! data received via the data connection is to be handled

    case Lower(Clip(self.ControlConnection._Command))
    of 'getremotefile'
        self._ProcessGetRemoteFile()
    of 'getdirlisting' orof 'listcurrentdir'
        self._ProcessGetDirListing()
    end
!------------------------------------------------------------------------------
NetFTPClientData.ErrorTrap Procedure  (string errorStr,string functionName)
  code
  ! Note this method is not called. The Control Connection's ErrorTrap is called instead.
  ! So all error handling can be processed in one place.

!------------------------------------------------------------------------------
NetFTPClientData.Open Procedure  (string Server,uShort Port=0)
  code
  self.log('NetFTPClientData.Open','DATA OPEN server=' & clip(Server) & ' Port=' & port)
  self._connection.MaxServerConnections = 1   ! Limit the DLL to only accepting one connection on this Listening Port
  parent.open (Server, Port)

!------------------------------------------------------------------------------
NetFTPClientData.Close Procedure
  code
  self.log('NetFTPClientData.Open','DATA CLOSE')
  if self.OpenFlag = 1
    if NetFTPClientWriteFileOpen = true
      close (NetFTPClientWriteFile)  ! Close the files
      NetFTPClientWriteFileOpen = false
    end
    if NetFTPClientReadFileOpen = true
      close (NetFTPClientReadFile)   ! Close the files
      NetFTPClientReadFileOpen = false
    end
  end

  parent.close()
!------------------------------------------------------------------------------
NetFTPClientData.Abort Procedure
  code
  self.ControlConnection._waitingForComplete = 0        ! March 2007: This indicates to the Control that there is nothing left for the data connection to do.

  if self.OpenFlag = 1
    if NetFTPClientWriteFileOpen = true
      close (NetFTPClientWriteFile)  ! Close the files
      NetFTPClientWriteFileOpen = false
    end
    if NetFTPClientReadFileOpen = true
      close (NetFTPClientReadFile)   ! Close the files
      NetFTPClientReadFileOpen = false
    end
  end

  parent.abort()
!------------------------------------------------------------------------------
NetFTPClientData._ProcessGetDirListing Procedure
local                                group, pre (loc)
PositionInPacket                        long
TempString                              String(400)
JPos1                                   long
ret                                     long
                                      end
  code
  if (self.Packet.BinDataLen = 0) or (self.packet.PacketType <> NET:SimplePartialDataPacket)
    ! This is either an empty packet or a NEW CONNECTION Packet - either way
    ! We won't process it
    return
  end

  if self._LastPacketLineLen > 0
    if (self.Packet.BinDataLen + self._LastPacketLineLen) > (Net:MaxBinData + 200)
      self.ControlConnection._CallErrorTrap ('Error. String too short. ', 'NetFTPClientData.Process')
      return
    end
    self._longerPacketBinData = self._LastPacketLine [1 : self._LastPacketLineLen] & self.Packet.BinData[1 : self.Packet.BinDataLen]
    self._longerPacketBinDataLen = self.Packet.BinDataLen + self._LastPacketLineLen
  else
    loop
      if self.Packet.BinDataLen >= 2
        if self.packet.binData[1:2] = '<13,10>'
          ! Strip out any directory listings that start with CRLF combinations.
          ! We need the first line with real data in it so that we can work out the
          ! correct FTP directory listing format
          if self.packet.binDataLen > 2
            self.packet.binData[1 : (self.packet.binDataLen-2)] = self.packet.binData[3 : self.packet.binDataLen]
            self.packet.binDataLen -= 2
          else
            return ! ignore data in this packet, there is nothing meaningful.
          end
        else
          break ! out of loop
        end
      else
        break
      end
    end
    self._longerPacketBinData = self.Packet.BinData[1 : self.Packet.BinDataLen]
    self._longerPacketBinDataLen = self.Packet.BinDataLen
  end


  if (self._DirFormatSet = 0)

    self._FigureOutDirFormat(self._longerPacketBinData[1 : self._longerPacketBinDatalen])           ! first line of the first packet in
                                         ! the directory listing will be examined
                                         ! to determine the FORMAT of directory
                                         ! listing used by the particular server
    if (self._DirFormatSet) and (self._DirListingFormat = 0)
      ! If you don't want this error to be called. Put the following code in the
      ! virtual method _FigureOutDirFormat() after the parent call:
      ! if self._DirListingFormat = 0
      !   self._DirListingFormat = 255
      ! end
      !
      self.Error = ERROR:GeneralFTPError
      self.WinSockError = 0
      self.SSLError = 0
      self.ControlConnection.WinSockError = 0
      self.ControlConnection.SSLError = 0
      self.ControlConnection.FTPError = ERROR:FTP:UnknownFormatOfDirListing
      self.ControlConnection._CallErrorTrap('Unknown format of directory listing','NetFTPClientData.Process()')
    end

  end

  if self._DirFormatSet = 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientData._ProcessGetDirListing', 'We didnt get the full first line. Saving bytes = ' & self._longerPacketBinDataLen)
      end
    ****
    if self._longerPacketBinDataLen <= len (self._LastPacketLine)
      ! We haven't got enough data to know the format yet, so store the data and wait for more
      self._LastPacketLine = self._longerPacketBinData[1 : self._longerPacketBinDatalen]
      self._LastPacketLineLen = self._longerPacketBinDataLen
      return
    else
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetFTPClientData._ProcessGetDirListing', 'Error. Buffer Overflow with self._LastPacketLine. Data not saved')
        end
      ****
    end
  end
  Do ProcessData

! -----------------------------------------------------------------------
ProcessData Routine
  loc:PositionInPacket = 1
  self._LastPacketLine = ''
  self._LastPacketLineLen = 0

  loop                                                                          ! read line at a time until end of packet
    if loc:PositionInPacket > self._longerPacketBinDataLen
      break  ! We got through the whole lot
    end
    loc:JPos1 = 0
    loc:JPos1 = instring(clip(self.ControlConnection._LineBreak),self._longerPacketBinData[1 : self._longerPacketBinDataLen],1,loc:PositionInPacket)
    if loc:JPos1 = 0
      ! Store last bit of data for next time as there is no CRLF in it.
      self._LastPacketLine = self._longerPacketBinData[loc:PositionInPacket : self._longerPacketBinDatalen]
      self._LastPacketLineLen = self._longerPacketBinDataLen - loc:PositionInPacket + 1
      break
    end

    if (loc:JPos1-1) >= loc:PositionInPacket
      loc:TempString = self._longerPacketBinData[loc:PositionInPacket : (loc:JPos1-1)]
    else
      loc:TempString = ''
    end
    clear (self.ControlConnection.DirListingQ)

    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientData._ProcessGetDirListing', 'Processing dir line = [' & clip (loc:TempString) & ']')
      end
    ****

    case (self._DirListingFormat)
    of 1
      loc:ret = self._FillDirListingQ_Format01(loc:TempString)
    of 2
      loc:ret = self._FillDirListingQ_Format02(loc:TempString)
    of 3
      loc:ret = self._FillDirListingQ_Format03(loc:TempString)
    of 4
      loc:ret = self._FillDirListingQ_Format04(loc:TempString)
    of 200 to 255
      loc:ret = self._FillDirListingQ_FormatCustom(loc:TempString)
    end

    if loc:ret = 0
      ! successfull, so add the data to the queue
      add(self.ControlConnection.DirListingQ)
    end
    ! Jump to the next line
    loc:PositionInPacket = instring(clip(self.ControlConnection._LineBreak), self._longerPacketBinData[1 : self._longerPacketBinDataLen],1,loc:PositionInPacket) + 2
    if loc:PositionInPacket = 0
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetFTPClientData._ProcessGetDirListing', 'Warning - Jono look at this line')
        end
      ****
    end
  end


!------------------------------------------------------------------------------
NetFTPClientData._ProcessGetRemoteFile Procedure
  code
    self.ControlConnection.bytesLeftToReceive = self.ControlConnection.bytesLeftToReceive - self.Packet.BinDataLen
    self.Log ('NetFTPDataControl._ProcessGetRemoteFile', 'bytes Received = ' & self.packet.binDataLen & ' bytesLeftToReceive = ' & self.ControlConnection.bytesLeftToReceive)

    self.ControlConnection.TransferState = 'Downloading'

    if self._LookForExistingFile                       ! First packet of file received
      self._LookForExistingFile = 0

      if self.ControlConnection.ProgressControl                          ! Update the Progress Control if there is one
        self.ControlConnection.ProgressControl{PROP:RangeLow} = 0        ! zero to 100 percent
        self.ControlConnection.ProgressControl{PROP:RangeHigh} = 100     ! zero to 100 percent
        self.ControlConnection.ProgressControl{prop:Hide} = 0
        self.ControlConnection.ProgressControl{prop:Progress} = 0
      end

      NetFTPClientWriteFileName = self.ControlConnection._LocalFileName
      open(NetFTPClientWriteFile)

      if not(errorcode())                               ! If destination file already exists in specified directory,
        close(NetFTPClientWriteFile)                   ! ovewrite that file with file received
        remove(NetFTPClientWriteFile)
      end
      create(NetFTPClientWriteFile)                    ! Create file
      open(NetFTPClientWriteFile)
      NetFTPClientWriteFileOpen = true
      if errorcode()
        self.Error = ERROR:GeneralFTPError
        self.WinSockError = 0
        self.SSLError = 0
        self.ControlConnection.WinSockError = 0
        self.ControlConnection.SSLError = 0
        self.ControlConnection.FTPError = ERROR:FTP:CannotOpenFile
        self.ControlConnection._State = 'cannot open destination file'
        self.ControlConnection._Command = ''
        self.ControlConnection._CallErrorTrap('Cannot open destination file','NetFTPClientData.Process()')
        if self.ControlConnection.OpenFlag or self.ControlConnection.AsyncOpenBusy ! New 7/7/2004 - Prevent recursive calls to .ErrorTrap if Abort fails
          self.ControlConnection.Abort()
        end
        Return
      end
    end

    if self.packet.binDataLen > 0
      NetFTPClientWriteFile.NetFTPClientWriteFileRecord.NetFTPClientWriteFileData = self.Packet.Bindata[1 : self.Packet.BinDataLen]
      add(NetFTPClientWriteFile, self.Packet.BinDataLen)
      if errorcode()
        self.Error = ERROR:GeneralFTPError
        self.WinSockError = 0
        self.SSLError = 0
        self.ControlConnection.WinSockError = 0
        self.ControlConnection.SSLError = 0
        self.ControlConnection.FTPError = ERROR:FTP:CannotWriteToFile
        self.ControlConnection._State = 'data cannot be written to destination file'
        self.ControlConnection._Command = ''
        self.ControlConnection._CallErrorTrap('Data cannot be written to destination file','NetFTPClientData.Process()')
        if self.ControlConnection.OpenFlag or self.ControlConnection.AsyncOpenBusy ! New 7/7/2004 - Prevent recursive calls to .ErrorTrap if Abort fails
          self.ControlConnection.Abort()
        end
        close (NetFTPClientWriteFile)
        NetFTPClientWriteFileOpen = false
        Return
      end
    end
    if self.ControlConnection.BytesLeftToReceive = 0
      self._GetRemoteFileCompleted()
    else
      self.ControlConnection.calcprogress()
    end

!------------------------------------------------------------------------------
NetFTPClientData._ProcessPutFile Procedure
  code
  if self.Packet.PacketType = NET:SimpleNewConnection             ! Client has established new connection
    self._SendFile()                                              ! ready to begin file transfer
  end

!------------------------------------------------------------------------------
NetFTPClientData._FigureOutDirFormat Procedure  (string p_Data)
local                                group, pre (loc)
Column_count                            long
Column_Six                              String(3)
Column_Five                             string (3)
CRpos                                   long
OurPositionInPacket                     long
DataLen                                 long
                                      end

  code
!-------------------------------------------------------------------------------------
! This method is meant to determine the directory format that is returned
! by the FTP Server. The following file format options extist:
! 1 - DOS format                         e.g. [03-21-02  03:07PM                   10 test_file.txt]
! 2 - Unix format (sixth column is date) e.g. [drwxr--r--   1 user     group          0 May 10 14:22 Desktop]
! 3 - Unix format (fifth column is date) e.g. []
! 4 - Short Directory Listing Format
! 200 to 255 - available for Programmers to use. If you set the self._DirListingFormat
!              to one of these options the method self._FillDirListingQ_FormatCustom
!              method will be called for you to populate the
!              self.ControlConnection.DirListingQ queue.
!-------------------------------------------------------------------------------------


  loc:DataLen = len (clip(p_Data))

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientData._FigureOurDirFormat', 'p_data = [' & p_Data & ']')
    end
  ****

  if self.ControlConnection.FullListing = 0
            ! COMMAND = NLST, retrieves listing of directory/file names contained in
            ! directory, data received in packet is broken up into directory/file names
            ! then stored in queue for further processing

    self._DirListingFormat = 4  ! Short Format
    self._DirFormatSet = true
  else
            ! COMMAND = LIST, retrieves long listing of directory's contents
            ! data received in packet is broken up into directory/file names and other
            ! fields carrying information, then stored in queue for further processing

    if lower (sub (p_Data, 1, 5)) = 'total'   ! Dumb dir listing starts with a line like 'total 512' and then starts the dir listing.
      ! Chop out the first line - it just contains something like 'total 512'
      loc:CRpos = instring (clip(self.ControlConnection._LineBreak),p_Data,1,1)
      if loc:CRPos > 0 and (loc:CRPos + 2) <= loc:DataLen
        p_data = p_Data [(loc:CRPos + 2): loc:DataLen]
        loc:DataLen = clip(len(p_Data))
      else
        return
      end
    end

    loc:CRpos = instring (clip(self.ControlConnection._LineBreak),p_Data,1,1)
    if loc:CRPos = 0
      ! We haven't got a whole line yet - wait till we get a whole line
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetFTPClientData._FigureOurDirFormat', 'We didnt get a whole line. Will have to wait for more to arrive. p_data = [' & p_Data & ']')
        end
      ****
      return
    end

    loc:OurPositionInPacket = 1

    loc:OurPositionInPacket = instring(' ',p_data,1,loc:OurPositionInPacket)
    if loc:OurPositionInPacket
      loop while ((loc:OurPositionInPacket <= loc:DataLen) and (p_Data[loc:OurPositionInPacket] = ' '))                         ! skip over blank spaces
        loc:OurPositionInPacket = loc:OurPositionInPacket + 1
      end
    else
      self.Error = ERROR:GeneralFTPError
      self.WinSockError = 0
      self.SSLError = 0
      self.ControlConnection.WinSockError = 0
      self.ControlConnection.SSLError = 0
      self.ControlConnection.FTPError = ERROR:FTP:UnknownFormatOfDirListing
      self.ControlConnection._CallErrorTrap('Unknown format of directory listing','NetFTPClientData.Process()')
      return
    end


    loc:OurPositionInPacket = loc:OurPositionInPacket + 2                                     ! PosInPacket is set to test first column

    if p_Data[3] = '-' and p_Data[6] = '-' and p_Data[loc:OurPositionInPacket] = ':' and p_Data[loc:OurPositionInPacket + 4] = 'M'
      ! first column is DATE column and second column is TIME column
      self._DirFormatSet = true
      self._DirListingFormat = 1                                                      ! format identified as FORMAT 1 = MS-DOS
    else

      loc:OurPositionInPacket = 1

      loop loc:Column_count = 1 to 5                                                      ! PosInPacket is set to test column six
        if loc:Column_count = 5
          loc:column_five = lower(p_Data[loc:OurPositionInPacket : loc:OurPositionInPacket + 2])
          if loc:column_five='jan' or loc:column_five='feb' or loc:column_five='mar' or loc:column_five='apr' or loc:column_five='may' or loc:column_five='jun' or loc:column_five='jul'or loc:column_five='aug' or loc:column_five='sep' or loc:column_five='oct' or loc:column_five='nov' or loc:column_five='dec'
            ! fifth column is DATE column
            self._DirFormatSet = true
            self._DirListingFormat = 3                                                    ! format identified as FORMAT 3 = UNIX without Group
          end
        end
        loc:OurPositionInPacket = instring(' ',p_data, 1, loc:OurPositionInPacket)
        if loc:OurPositionInPacket
          loop while ((loc:OurPositionInPacket <= loc:DataLen) and (p_Data[loc:OurPositionInPacket] = ' '))                   ! skip over blank spaces
            loc:OurPositionInPacket = loc:OurPositionInPacket + 1
          end
        else
          self.Error = ERROR:GeneralFTPError
          self.WinSockError = 0
          self.SSLError = 0
          self.ControlConnection.WinSockError = 0
          self.ControlConnection.SSLError = 0
          self.ControlConnection.FTPError = ERROR:FTP:UnknownFormatOfDirListing
          self.ControlConnection._CallErrorTrap('Unknown format of directory listing','NetFTPClientData.Process()')
          return
        end
      end

      loc:column_six = lower(p_Data[loc:OurPositionInPacket : loc:OurPositionInPacket + 2])

      if self._DirFormatSet = false
        loc:column_six = lower(p_Data[loc:OurPositionInPacket : loc:OurPositionInPacket + 2])
        if  loc:column_six='jan' or loc:column_six='feb' or loc:column_six='mar' or loc:column_six='apr' or loc:column_six='may' or loc:column_six='jun' or loc:column_six='jul'or loc:column_six='aug' or loc:column_six='sep' or loc:column_six='oct' or loc:column_six='nov' or loc:column_six='dec'
          ! sixth column is DATE column
          self._DirFormatSet = true
          self._DirListingFormat = 2                                                    ! format identified as FORMAT 2 = UNIX
        else
          if instring('no such file or directory',lower(p_Data),1,1) <> 0  ! error REPLY has been sent over data connection
            self.Error = ERROR:GeneralFTPError
            self.WinSockError = 0
            self.SSLError = 0
            self.ControlConnection.WinSockError = 0
            self.ControlConnection.SSLError = 0
            self.ControlConnection.FTPError = ERROR:FTP:FileAccessError
            self.ControlConnection._CallErrorTrap('No such file or directory','NetFTPClientData.Process()')
          elsif instring('permission denied',lower(p_data),1,1) <> 0       ! error REPLY has been sent over data connection
            self.Error = ERROR:GeneralFTPError
            self.WinSockError = 0
            self.SSLError = 0
            self.ControlConnection.WinSockError = 0
            self.ControlConnection.SSLError = 0
            self.ControlConnection.FTPError = ERROR:FTP:FileAccessError
            self.ControlConnection._CallErrorTrap('Permission denied','NetFTPClientData.Process()')
          else                                                                          ! format of directory listing UNRECOGNIZED
            self._DirFormatSet = 1       ! Okay we don't know the format
            self._DirListingFormat = 0
          end
        end
      end
    end
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientData._FigureOurDirFormat', 'Orginal NetTalk FTP Code reports Format = ' & self._DirListingFormat)
    end
  ****



!------------------------------------------------------------------------------
NetFTPClientData._FillDirListingQ_FormatCustom Procedure  (string p_Line)
  code
  return (0)
!------------------------------------------------------------------------------
NetFTPClientData._FillDirListingQ_Format01 Procedure  (string p_Line)
local                         group, pre (loc)
PositionInLine                  long
NextBoundary                    long
NextBoundaryLen                 long
LineLength                      long
ret                             long
                              end
  code
  ! directory listing is in FORMAT 1 = MS-DOS FORMAT

  ! Initialise
  loc:ret = 1  ! Fail as a default, until we get the name
  loc:LineLength = len (clip(p_Line))
  loc:PositionInLine = 1

  if instring ('<<DIR>', p_Line,1,1)
    self.ControlConnection.DirListingQ.Attrib = ff_:DIRECTORY
  end

  Do GetNextBoundary

  self.ControlConnection.DirListingQ.Date = Deformat(sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen),@d1) ! p_Line[loc:PositionInLine : instring(' ',p_Line,1,loc:PositionInLine)-1],@d1)
                                            ! DATE STRING is deformated to be stored in a long


  Do NextSpace
  Do GetNextBoundary

            ! STRING TIME is deformated to be stored in a long
  self.ControlConnection.DirListingQ.Time = Deformat(sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen),@t3)

  Do NextSpace
  Do GetNextBoundary

  if sub (p_Line, loc:PositionInLine, 5) = '<<DIR>'
    self.ControlConnection.DirListingQ.Size = -1                                                     ! size field of directory is set to -1
  else
    self.ControlConnection.DirListingQ.Size = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen) !p_Line[loc:PositionInLine : instring(' ',p_Line,1,loc:PositionInLine)-1]
  end

  Do NextSpace

  loc:NextBoundary = loc:LineLength
  loc:NextBoundaryLen = (loc:NextBoundary - loc:PositionInLine + 1)

  self.ControlConnection.DirListingQ.Name = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  if self.ControlConnection.DirListingQ.Name <> ''
    loc:ret = 0
    ! We are happy we got a filename
  end

  return (loc:ret)

! -----------------------------------------------------------------------------------
GetNextBoundary Routine
  loc:NextBoundary = instring(' ', p_Line, 1, loc:PositionInLine)
  if loc:NextBoundary = 0
    loc:NextBoundary = loc:LineLength
  else
    if loc:NextBoundary > loc:LineLength
      return (loc:ret)
    end
    loc:NextBoundary -= 1  ! We don't want the space
  end
  loc:NextBoundaryLen = (loc:NextBoundary - loc:PositionInLine + 1)

! -----------------------------------------------------------------------------------
NextSpace Routine
  loc:PositionInLine = instring(' ', p_Line, 1, loc:PositionInLine)
  if loc:PositionInLine > 0
    loop while p_Line[loc:PositionInLine] = ' '                                ! skip over blank spaces
      loc:PositionInLine = loc:PositionInLine + 1
      if loc:PositionInLine > loc:LineLength
        ! Finished
        return (loc:ret)
      end
    end
  else
    ! Finished
    return (loc:ret)
  end

! -----------------------------------------------------------------------------------


!------------------------------------------------------------------------------
NetFTPClientData._FillDirListingQ_Format02 Procedure  (string p_Line)
local                         group, pre (loc)
YearOrTimeString                String(20)
DateString                      String(20)
PositionInLine                  long
NextBoundary                    long
NextBoundaryLen                 long
LineLength                      long
ret                             long
tempDate                        long
                              end

  code
  ! Initialise
  loc:ret = 1  ! Fail as a default, until we get the name
  loc:LineLength = len (clip(p_Line))
  loc:PositionInLine = 1

  Do GetNextBoundary

          ! file or directory ACCESS PERMISSIONS
  self.ControlConnection.DirListingQ.AccessPermissions = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)
  if instring ('d', self.ControlConnection.DirListingQ.AccessPermissions,1,1)
    self.ControlConnection.DirListingQ.Attrib = ff_:DIRECTORY
  end

  Do NextSpace
  Do GetNextBoundary

  self.ControlConnection.DirListingQ.NoOfLinks = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  self.ControlConnection.DirListingQ.Owner = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  self.ControlConnection.DirListingQ.GroupOwnership = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  self.ControlConnection.DirListingQ.Size = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  loc:DateString = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  loc:DateString = clip (loc:DateString) & ' ' & sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

           ! test column eight (YEAR or TIME)
  loc:YearOrTimeString = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  if instring(':',loc:YearOrTimeString,1,1) = 0                                   ! this string is YEAR
    loc:DateString = clip(loc:DateString) & ',' & clip(loc:YearOrTimeString)      ! and is part of DATE
    self.ControlConnection.DirListingQ.Date = Deformat(loc:DateString,@d3)
    self.ControlConnection.DirListingQ.Time = 0                               ! TIME is not given
  else                                                                        ! this string is TIME
          ! TIME STRING is deformated to be stored in a long
    self.ControlConnection.DirListingQ.Time = Deformat(loc:YearOrTimeString,@t1)

    ! DATE STRING is deformated to be stored in a long
    loc:tempDate = Deformat(clip (loc:DateString) & ',' & year (today()),@d3)
    if loc:tempDate = 0
      self.ControlConnection.DirListingQ.Date = 0
    elsif loc:tempDate > (today() + 1)
      loc:DateString = clip(loc:DateString) & ',' & (year (today()) - 1)
      self.ControlConnection.DirListingQ.Date = Deformat(loc:DateString,@d3)
    else
      self.ControlConnection.DirListingQ.Date = loc:tempDate
    end
  end


  Do NextSpace

  loc:NextBoundary = loc:LineLength
  loc:NextBoundaryLen = (loc:NextBoundary - loc:PositionInLine + 1)

  self.ControlConnection.DirListingQ.Name = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  if self.ControlConnection.DirListingQ.Name <> ''
    loc:ret = 0
    ! We are happy we got a filename
  end

  return (loc:ret)

! -----------------------------------------------------------------------------------
GetNextBoundary Routine
  loc:NextBoundary = instring(' ', p_Line, 1, loc:PositionInLine)
  if loc:NextBoundary = 0
    loc:NextBoundary = loc:LineLength
  else
    if loc:NextBoundary > loc:LineLength
      return (loc:ret)
    end
    loc:NextBoundary -= 1  ! We don't want the space
  end
  loc:NextBoundaryLen = (loc:NextBoundary - loc:PositionInLine + 1)

! -----------------------------------------------------------------------------------
NextSpace Routine
  loc:PositionInLine = instring(' ', p_Line, 1, loc:PositionInLine)
  if loc:PositionInLine > 0
    loop while p_Line[loc:PositionInLine] = ' '                                ! skip over blank spaces
      loc:PositionInLine = loc:PositionInLine + 1
      if loc:PositionInLine > loc:LineLength
        ! Finished
        return (loc:ret)
      end
    end
  else
    ! Finished
    return (loc:ret)
  end

! -----------------------------------------------------------------------------------



!------------------------------------------------------------------------------
NetFTPClientData._FillDirListingQ_Format03 Procedure  (string p_Line)
local                         group, pre (loc)
YearOrTimeString                String(20)
DateString                      String(20)
PositionInLine                  long
NextBoundary                    long
NextBoundaryLen                 long
LineLength                      long
ret                             long
tempDate                        long
                              end

  code
  ! Initialise
  loc:ret = 1  ! Fail as a default, until we get the name
  loc:LineLength = len (clip(p_Line))
  loc:PositionInLine = 1

  Do GetNextBoundary


  ! file or directory ACCESS PERMISSIONS
  self.ControlConnection.DirListingQ.AccessPermissions = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)
  if instring ('d', self.ControlConnection.DirListingQ.AccessPermissions,1,1)
    self.ControlConnection.DirListingQ.Attrib = ff_:DIRECTORY
  end

  Do NextSpace
  Do GetNextBoundary

  self.ControlConnection.DirListingQ.NoOfLinks = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  self.ControlConnection.DirListingQ.Owner = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  self.ControlConnection.DirListingQ.Size = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  loc:DateString = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  loc:DateString = clip(loc:DateString) & ' ' & sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  Do NextSpace
  Do GetNextBoundary

  ! test column eight (YEAR or TIME)
  loc:YearOrTimeString = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  if instring(':',loc:YearOrTimeString,1,1) = 0                                   ! this string is YEAR
    loc:DateString = clip(loc:DateString) & ','  & clip(loc:YearOrTimeString)             ! and is part of DATE
                ! DATE STRING is deformated to be stored in a long

    self.ControlConnection.DirListingQ.Date = Deformat(loc:DateString,@d3)
    self.ControlConnection.DirListingQ.Time = -1                              ! TIME is not given
  else                                                                        ! this string is TIME
          ! TIME STRING is deformated to be stored in a long
    self.ControlConnection.DirListingQ.Time = Deformat(loc:YearOrTimeString,@t1)


    ! DATE STRING is deformated to be stored in a long
    loc:tempDate = Deformat(clip (loc:DateString) & ',' & year (today()),@d3)
    if loc:tempDate = 0
      self.ControlConnection.DirListingQ.Date = 0
    elsif loc:tempDate > (today() + 1)
      loc:DateString = clip(loc:DateString) & ',' & (year (today()) - 1)
      self.ControlConnection.DirListingQ.Date = Deformat(loc:DateString,@d3)
    else
      self.ControlConnection.DirListingQ.Date = loc:tempDate
    end
  end

  Do NextSpace

  loc:NextBoundary = loc:LineLength
  loc:NextBoundaryLen = (loc:NextBoundary - loc:PositionInLine + 1)

  self.ControlConnection.DirListingQ.Name = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)


  if self.ControlConnection.DirListingQ.Name <> ''
    loc:ret = 0
    ! We are happy we got a filename
  end

  return (loc:ret)

! -----------------------------------------------------------------------------------
GetNextBoundary Routine
  loc:NextBoundary = instring(' ', p_Line, 1, loc:PositionInLine)
  if loc:NextBoundary = 0
    loc:NextBoundary = loc:LineLength
  else
    if loc:NextBoundary > loc:LineLength
      return (loc:ret)
    end
    loc:NextBoundary -= 1  ! We don't want the space
  end
  loc:NextBoundaryLen = (loc:NextBoundary - loc:PositionInLine + 1)

! -----------------------------------------------------------------------------------
NextSpace Routine
  loc:PositionInLine = instring(' ', p_Line, 1, loc:PositionInLine)
  if loc:PositionInLine > 0
    loop while p_Line[loc:PositionInLine] = ' '                                ! skip over blank spaces
      loc:PositionInLine = loc:PositionInLine + 1
      if loc:PositionInLine > loc:LineLength
        ! Finished
        return (loc:ret)
      end
    end
  else
    ! Finished
    return (loc:ret)
  end

! -----------------------------------------------------------------------------------



!------------------------------------------------------------------------------
NetFTPClientData._FillDirListingQ_Format04 Procedure  (string p_Line)
local                         group, pre (loc)
PositionInLine                  long
NextBoundary                    long
NextBoundaryLen                 long
LineLength                      long
ret                             long
                              end

  code
  ! This is the Short List Format

  ! Initialise
  loc:ret = 1  ! Fail as a default, until we get the name
  loc:LineLength = len (clip(p_Line))
  loc:PositionInLine = 1

  loc:NextBoundary = loc:LineLength
  loc:NextBoundaryLen = (loc:NextBoundary - loc:PositionInLine + 1)

  self.ControlConnection.DirListingQ.Name = sub (p_Line, loc:PositionInLine, loc:NextBoundaryLen)

  if self.ControlConnection.DirListingQ.Name <> ''
    loc:ret = 0
    ! We are happy we got a filename
  end

  return (loc:ret)

!------------------------------------------------------------------------------
NetFTPClientData.ProcessIdleConnection Procedure
  code
  ! This method is called when the FTP Data Control has been idle for the
  ! number of hundreths of a second in the self.InActiveTimeout

  ! To stop this .abort from happening, you can either set self.InActiveTimeout = 0, after
  ! the Init, or you can put a return in your derived .ProcessIdleConnection method before
  ! the parent call

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientData.ProcessIdleConnection', |
                     'Warning. FTP Control Data has been idle for more than ' & self.InActiveTimeout & ' hs.' & |
                     ' Will call Abort().' & |
                     ' ?Socket = ' & self._connection.socket & |
                     ' ?SockID = ' & self._connection.sockID & |
                     ' Server = ' & clip (self._connection.Server) & |
                     ' Port = ' & clip (self._connection.Port), NET:LogError)
    end
  ****

  loop while (records (self.qServerConnections) > 0)
    get (self.qServerConnections, 1)
    self.AbortServerConnection(self.qServerConnections.Socket, self.qServerConnections.SockID)
  end
  if self.OpenFlag or self.AsyncOpenBusy ! Prevent recursive calls to .ErrorTrap if Abort fails
    self.Abort()
  end

  self.ConnectionClosed() ! Handle this connection closing the same as if the remote server closed the connection

!------------------------------------------------------------------------------
NetFTPClientData._CallErrorTrap Procedure  (string errorStr,string functionName)
SaveSuppressErrorMsg     byte

  code
  if (self.Packet.PacketType = NET:SimpleAsyncOpenFailed) and (self.ControlConnection.PassiveMode)
    ! Passive Asynchronous connection failed.
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientData._CallErrorTrap', |
                       ' FTP Data Connection Open Failed.' & |
                       ' Server = ' & clip (self._connection.Server) & |
                       ' Port = ' & clip (self._connection.Port))
      end
    ****
  end

  if (self.error = 10048) and (self.ControlConnection.PassiveMode = 0)
    ! Active Mode (Listening Port Mode) Address already in use. Could not open listening port
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientData._CallErrorTrap', |
                       ' Could not listen on' & |
                       ' Port = ' & clip (self._connection.port) & |
                       ' Will try the next one')
      end
    ****
    return                      ! Do nothing
  end


  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetFTPClientData._CallErrorTrap', |
                     ' Error ' & self.Error & |
                     ' ErrorStr = ' & clip (errorStr) & |
                     ' Function = ' & clip (functionName))
    end
  ****

  ! Close the files on error
  if NetFTPClientWriteFileOpen
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientData.._CallErrorTrap', 'Will close the FTP file being written to disk')
      end
    ****
    close (NetFTPClientWriteFile)
    NetFTPClientWriteFileOpen = false
  end

  if NetFTPClientReadFileOpen
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetFTPClientData.._CallErrorTrap', 'Will close the FTP file being read from disk')
      end
    ****
    close (NetFTPClientReadFile)
    NetFTPClientReadFileOpen = false
  end

  self.ControlConnection.Error = self.Error
  self.ControlConnection.FTPError = self.ControlConnection.FTPError
  SaveSuppressErrorMsg = self.ControlConnection.SuppressErrorMsg        ! Save
  self.ControlConnection.SuppressErrorMsg = 1                           ! Make sure no error displayed
  self.ControlConnection._CallErrorTrap(errorStr,functionName)          ! send the error details to be trapped by NetFTPClient.ErrorTrap
  self.ControlConnection.SuppressErrorMsg = SaveSuppressErrorMsg

!------------------------------------------------------------------------------
NetSNMP.Process      Procedure
authenticationFailed    byte
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.Process', 'Called')
    end
  ****


  !First decode the message and fill the fields of the pdu
  self._DecodeSNMPMessage(self.packet.bindata, authenticationFailed)
  if authenticationFailed
    if self.sendAuthFailureTrap
      do SendAuthFailureTrap
    end
    return
  end

  !Maybe there was a decoding error
  If self._decodeError <> ''
    self.SNMPVersion = 0
    self.community = ''
    self.requestID = 0
    self.ErrorStatus = 0
    self.ErrorIndex = 0
    self.enterprise = ''
    self.AgentAddr = ''
    self.genericTrap = 0
    self.specificTrap = 0
    self.timestamp = 0
    self._command = 0
    self.V2Trap = 0     !bk
    self.snmpTrapOID = ''
    free(self.qTrapOID)
    clear(self.qTrapOID)

    free(self.qVarBindList)
    self.ErrorTrap('Decoding of the received SNMPPacket failed (' & clip(self._decodeError) &')', clip(self._functionname))
    return
  end

  !Now decide what kind of pdu we received, check community and process further
  if self._command = NetSNMP:Trap
    self.processTrap
  elsif self._command = NetSNMP:GetResponse
    self.processResponse
  elsif self._command = NetSNMP:SetRequest
    self._ProcessSet()
  elsif self._command = NetSNMP:GetRequest
    self._ProcessGet()
  elsif self._command = NetSNMP:GetNextRequest
    self._ProcessGetNext()
  elsif self._command = NetSNMP:TrapV2
     self.ProcessTrapV2
  end

! ---------------------------------------------------------------
! Sends an authentication failure trap packet back
SendAuthFailureTrap routine
  self.GenericTrap      = NetSNMP:AuthFailure
  self.SpecificTrap     = 0
  free(self.qVarBindList)
  self.packet.toIP      = self.packet.fromIP
  self.SendTrap()

!------------------------------------------------------------------------------
NetSNMP.GetValue     Procedure
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.GetValue', 'Called')
    end
  ****

  self.ErrorStatus = 0
  self.ErrorIndex = 0
  self._command = NetSNMP:GetRequest
  self._EncodeSNMPMessage(self.packet.bindata)
  if self._encodeError <> ''
    !Handle this error
    self.ErrorTrap('The message could not be encoded (' &clip(self._encodeError) &')', clip(self._functionname))
  else
    self.packet.UDPToPort = self.ToPort
    self.packet.bindatalen = self._encodedPacketLen
    self.send()
  end
!------------------------------------------------------------------------------
NetSNMP.SendTrap     Procedure
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.SendTrap', 'Called')
    end
  ****

  self._command = NetSNMP:Trap
  self._EncodeSNMPMessage(self.packet.bindata)
  if self._encodeError <> ''
    !Handle this error
    self.ErrorTrap('The message could not be encoded (' &clip(self._encodeError) &')', clip(self._functionname))
  else
    self.packet.UDPToPort = self.TrapToPort
    self.packet.bindatalen = self._encodedPacketLen
    self.send()
  end

!------------------------------------------------------------------------------
NetSNMP.SetValue     Procedure
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.SetValue', 'Called')
    end
  ****

  self._command = NetSNMP:SetRequest
  self._EncodeSNMPMessage(self.packet.bindata)
  if self._encodeError <> ''
    !Handle this error
    self.ErrorTrap('The message could not be encoded (' &clip(self._encodeError) &')', clip(self._functionname))
  else
    self.packet.UDPToPort  = self.ToPort
    self.packet.bindatalen = self._encodedPacketLen
    self.send()
  end


!------------------------------------------------------------------------------
NetSNMP._SendResponse Procedure
counter6    long
varbinds1   long
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._SendResponse', 'Called')
    end
  ****

  self._command = NetSNMP:GetResponse
  self._EncodeSNMPMessage(self.packet.bindata)
  if self._encodeError <> ''
    !Handle this error
    self.ErrorTrap('The message could not be encoded (' &clip(self._encodeError) &')', clip(self._functionname))
    return
  end

  !If packet is too big we have a problem
  if self._encodedPacketLen > NetSNMP:MaxPacketLen

    !Packet too large: Error = tooBig
    self.ErrorStatus = 1
    self.ErrorIndex  = 0

    !Now put nulls in the varbindlist values
    loop counter6 = 1 to records(self.qVarBindList)
      get(self.qVarBindList, counter6)
      if errorcode ()
        !Handle this error
        self.errorTrap('An entry of the varbindlist in the pdu could not be accessed', 'SendResponse')
        return
      end
      self.qVarBindList.valuetype = NetSNMP:NULL
      self.qVarBindList.value     = ''
      self.qVarBindList.length    = 0
      self.qVarBindList.BSbits    = 0
      update(self.qVarBindList)
      if errorcode ()
        !Handle this error
        self.errorTrap('An entry of the varbindlist in the pdu could not be accessed (updated).', 'SendResponse')
        return
      end
    end

    !Now reEncode the message
    self._command = NetSNMP:GetResponse
    self._EncodeSNMPMessage(self.packet.bindata)
    if self._encodeError <> ''
      !Handle this error
      self.ErrorTrap('The message could not be encoded (' &clip(self._encodeError) &')', '_EncodeSNMPMessage')
      return
    end
  end

  !Send it off
  self.packet.UDPToPort  = self.ToPort
  self.packet.bindatalen = self._encodedPacketLen
  self.send()


!------------------------------------------------------------------------------
NetSNMP.GetNextValue Procedure
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.GetNextValue', 'Called')
    end
  ****

  self.ErrorStatus = 0
  self.ErrorIndex = 0
  self._command = NetSNMP:GetNextRequest
  self._EncodeSNMPMessage(self.packet.bindata)
  if self._encodeError <> ''
    !Handle this error
    self.ErrorTrap('The message could not be encoded (' &clip(self._encodeError) &')', clip(self._functionname))
    return
  else
    self.packet.UDPToPort = self.ToPort
    self.packet.bindatalen = self._encodedPacketLen
    self.send()
  end

!------------------------------------------------------------------------------
!Translate V1 traps to v2 traps per RFC 3584 and pull values from bindlist
NetSNMP.ProcessTrap  Procedure()
vbcount         long  !number of varbinds
  code

  free(self.qTrapOID)
  case clip(self.GenericTrap)
  of 0
        self.snmpTrapOID =  NetSNMP:ColdStartOID
  of 1
        self.snmpTrapOID =  NetSNMP:WarmStartOID
  of 2
        self.snmpTrapOID =  NetSNMP:LinkDownOID
  of 3
        self.snmpTrapOID =  NetSNMP:LinkUpOID
  of 4
        self.snmpTrapOID =  NetSNMP:AuthFailureOID
  of 5
        self.snmpTrapOID = NetSNMP:egpOID
  else
        self.snmpTrapOID = clip(self.enterprise) & '.0.' & clip(self.specifictrap)
  end
  loop vbcount = 1 to records(self.qVarBindList)
    get(self.qVarBindList, vbcount)
        clear(self.qTrapOID)
        self.qTrapOID.TrapOID = clip(self.qVarBindList.name)
        self.qTrapOID.TrapValue = clip(self.qVarBindList.value)
        add(self.qTrapOID)
  end

!------------------------------------------------------------------------------
NetSNMP.ProcessResponse Procedure()
  code

!------------------------------------------------------------------------------
NetSNMP.ProcessTrapV2  Procedure()
!  self.Eneterprise will contain the V2 trap OID and the V2Trap flag will be set
vbcount                 long            !  # of items in the variable binding queue
delimiterpos    long            ! location of .0. in the v2 trap
generictrap             string(256) ! the trap identifier in the trap oid
        code
        free(self.qTrapOID)
        self.V2Trap = true              !set flag to identify this as a V2 trap which
        loop vbcount = 1 to records(self.qVarBindList)
                get(self.qVarBindList, vbcount)
                case clip(self.qVarBindList.name)
                of NetSNMP:sysUpTime
                        self.TimeStamp = self.qVarBindList.value
                of NetSNMP:snmpTrapOID
                        self.snmpTrapOID = clip(self.qVarBindList.value)
                of NetSNMP:snmpTrapEnterprise
                        self.enterprise = clip(self.qVarBindList.value)
                else
                        if self.snmpTrapOID
                                clear(self.qTrapOID)
                                self.qTrapOID.TrapOID = clip(self.qVarBindList.name)
                                self.qTrapOID.TrapValue = clip(self.qVarBindList.value)
                                add(self.qTrapOID)
                        end
                end
        end
    if self.snmpTrapOID = '' and records(self.qVarBindList)
                self.snmpTrapOID = clip(self.qVarBindList.value)
        end
        if self.enterprise = ''
                delimiterpos = instring('.', self.snmpTrapOID, 1, len(NetSNMP:enterprise)+2)
                self.enterprise = self.snmpTrapOID[1 : delimiterpos -1]
        end
    case clip(self.snmpTrapOID)
        of NetSNMP:ColdStartOID
                self.GenericTrap = NetSNMP:ColdStart
        of NetSNMP:WarmStartOID
                self.GenericTrap = NetSNMP:WarmStart
        of NetSNMP:LinkDownOID
                self.GenericTrap = NetSNMP:LinkDown
        of NetSNMP:LinkUpOID
                self.GenericTrap = NetSNMP:LinkUp
        of NetSNMP:AuthFailureOID
                self.GenericTrap = NetSNMP:AuthFailure
        of NetSNMP:egpOID
                self.GenericTrap = NetSNMP:egpNeighbourLoss
        else
                self.GenericTrap = NetSNMP:enterpriseSpecific
                delimiterpos = instring('.0.', self.snmpTrapOID, 1, 1)
                if delimiterpos
                        delimiterpos += 3
                        generictrap = self.snmpTrapOID[delimiterpos : len(clip(self.snmpTrapOID))]
                end
                self.SpecificTrap = generictrap
        end
   self.AgentAddr  = self.packet.fromIP


!------------------------------------------------------------------------------
NetSNMP.Init         Procedure  (ulong Mode=NET:SimpleClient)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.Init', 'Called')
    end
  ****

  if self.ToPort = 0
    self.ToPort = 161 ! Default value
  end
  if self.TrapToPort = 0
    self.TrapToPort = 162 ! Default value
  end
  if self.OurPort = 0
!    self.OurPort = 161 ! Default value
         self.OurPort = random(50152, 65535)  !bk - unless this is an agent explicitly set to listen on 161 we want to create a random port so the mnagement station will not conflict with a running agent
                                                                                  !bk - IANA dynamic/private ports are 49152-65535 - start at offset of 1000 to leave a large number of ports available for static allocation of private services
  end
  if self.OurTrapPort = 0
    self.OurTrapPort = 162 ! Default value
  end

  self._Connection.UDPMode = 1
  parent.init(NET:SimpleServer)


  ! Allocate Queues
  if self.qVarBindList &= NULL
    self.qVarBindList &= new(NetSNMP:VarBindListType)
  end

  if self.qMIBEntries &= NULL
    self.qMIBEntries &= new(NetSNMP:MIBQueueType)
  end

  if self.qCommunityProfiles &= NULL
    self.qCommunityProfiles &= new(NetSNMP:ComProfileQueueType)
  end

  if self.qMIBViews &= NULL
    self.qMIBViews &= new(NetSNMP:MIBViewsQueueType)
  end

   if self.qDescriptionMap &= NULL
    self.qDescriptionMap &= new(NetSNMP:DescriptionMapQueueType)
  end

  if self.qTrapOID &= NULL  !bk
    self.qTrapOID &= new(NetSNMP:TrapOIDQueueType)
  end

  ! End Queue Allocation

  if self._TrapListener &= NULL and self.DisableTrap = false   !bk - added code to make sure the trap listener id allowed to run before instantiating
    self._TrapListener &= new(NetTrapListener)
    self._TrapListener.init(Net:SimpleServer)
    if self._TrapListener.error <> 0
      message('Creation of TrapListener Failed', 'Object Creation Failure', icon:asterisk, button:ok)
      return
    end
    self._TrapListener.NetParent &= self
    self._TrapListener._Connection.UDPMode = 1
  end


!------------------------------------------------------------------------------
NetSNMP.Kill         Procedure
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.Kill', 'Called')
    end
  ****

  if self.OpenFlag or self.AsyncOpenBusy ! New 7/7/2004 - Prevent recursive calls to .ErrorTrap if Abort fails
    self.abort()
  end
  if not self._TrapListener &= NULL                 !bk - we have a trap listener
      if self._TrapListener.OpenFlag or self._TrapListener.AsyncOpenBusy ! New 7/7/2004 - Prevent recursive calls to .ErrorTrap if Abort fails
        self._TrapListener.abort()
      end
  end

  if not self.qVarBindList &= NULL
    dispose(self.qVarBindList)
  end

  if not self.qMIBEntries &= NULL
    dispose(self.qMIBEntries)
  end

  if not self.qMIBViews &= NULL
    dispose(self.qMIBViews)
  end

  if not self.qDescriptionMap &= NULL
    dispose(self.qDescriptionMap)
  end

  if not self.qCommunityProfiles &= NULL
    dispose(self.qCommunityProfiles)
  end

  if not self._TrapListener &= NULL
    self._TrapListener.kill()
    dispose(self._TrapListener)
  end

  if not self.qTrapOID &= NULL  ! bk
    dispose(self.qtrapOID)
  end
  parent.kill()

!------------------------------------------------------------------------------
NetSNMP._EncodeLength Procedure  (long p_Position,long p_Size,*long p_SizeLen,*string p_TargetString)
!-----------------------------------------------------------------------------------
! Parameter descriptions
! p_position      -> position in targetstring where size has to be encoded
! p_size          -> the size (length) of to be encoded
! p_sizelen       -> will contain the number of bytes it took to encode the size
! p_targetString  -> the string into which to encode the size
!-----------------------------------------------------------------------------------

local       group, pre (loc)
size          long
sizebytes     byte, dim(4), over(size)
            end

  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._EncodeLength', 'Called')
    end
  ****


  loc:size = P_size

  self._functionname = '_EncodeLength'

  if loc:size < 128     ! Can use the short form encoding here
    p_TargetString[p_Position] = chr(loc:Size)
    p_Sizelen = 1
  elsif loc:Size < 256  ! Use long form with only one byte giving size
    p_TargetString = p_TargetString[1 : p_Position - 1] & |
                   chr(129) & chr(loc:size) & |
                   p_TargetString[p_Position + 1 : self._currpos]
    p_SizeLen = 2
  else              ! Use long form with two bytes giving the size
    p_TargetString = p_TargetString[1 : p_Position - 1] & |
                   chr(130) & chr(loc:Sizebytes[2]) & chr(loc:Sizebytes[1]) & |
                   p_TargetString[p_Position + 1 : self._currpos]
    p_SizeLen = 3
  end
  self._currpos += p_SizeLen - 1
!------------------------------------------------------------------------------
NetSNMP._EncodeObjectID Procedure  (string p_ObjectID,*long p_IDsize,*string p_TargetString)
substring   string(256)
stringpos   long
longval     long
byteval     byte, dim(4), over(longval)
counter1    long
subidsize   long
lost3bits   byte
realval         real
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._EncodeObjectID', 'Called')
    end
  ****


  self._functionname = '_EncodeObjectID'

  p_IDsize = 0

  !The first 2 nodes get encoded into a single byte: = 40*(first) + (second)
  !Parse out the first node
  substring = p_ObjectID
  stringpos = instring('.', substring, 1, 1)
  if stringpos <= 1                          ! There are always at least 2 subidentifiers
    !Handle this error
    self._encodeError = 'The following object identifier is not valid: ' & clip(p_ObjectID)
    return
  end
  if numeric(substring[1 : stringpos - 1]) = 0   ! Make sure we have a valid objectid here
    !Handle this error
    self._encodeError = 'The following object identifier is not valid: ' & clip(p_ObjectID)
    return
  elsif substring[1 : stringpos - 1] < 0
    !Handle this error
    self._encodeError = 'The following object identifier is not valid: ' & clip(p_ObjectID)
    return
  end
  longval = substring[1 : stringpos - 1]
  longval = longval * 40


  !Parse out the second node
  substring = substring[stringpos + 1 : len(substring)]
  stringpos = instring('.', substring, 1, 1)
  if stringpos = 0
    stringpos = len(clip(substring)) + 1    ! Who knows, maybe this could happen
  end
  if numeric(substring[1 : stringpos - 1]) = 0
    ! Make sure we have a valid objectid here
    ! Handle this error
    self._encodeError = 'The following object identifier is not valid: ' & clip(p_ObjectID)
    return
  elsif substring[1 : stringpos - 1] < 0
    ! Handle this error
    self._encodeError = 'The following object identifier is not valid: ' & clip(p_ObjectID)
    return
  end
  longval = longval + substring[1 : stringpos - 1]
  self._AddChar( chr(longval), p_targetString )
  p_IDsize = 1

  !Now fill in the rest of the objectid by parsing the numbers out one by one
  substring = substring[stringpos + 1 : len(substring)]
  stringpos = instring('.', substring, 1, 1)
  if stringpos = 0 then return.
  loop until stringpos = 0
    if numeric(substring[1 : stringpos - 1]) = 0                                  !Make sure we have a valid objectid here
      !Handle this error
      self._encodeError = 'The following object identifier is not valid: ' & clip(p_ObjectID)
      return
    elsif substring[1 : stringpos - 1] < 0
      !Handle this error
      self._encodeError = 'The following object identifier is not valid: ' & clip(p_ObjectID)
      return
    end
    longval = substring[1 : stringpos - 1]
    realval = substring[1 : stringpos - 1]
    do EncodeSubID
    p_IDsize += subidsize
    substring = substring[stringpos + 1 : len(substring)]                         !Cut out the part of the objectid already entered (including the following dot)
    stringpos = instring('.', substring, 1, 1)
  end
  if numeric(substring) = 0                                                       !There should still be one node left
    !Handle this error
    self._encodeError = 'The following object identifier is not valid: ' & clip(p_ObjectID)
    return
  elsif substring < 0
    !Handle this error
    self._encodeError = 'The following object identifier is not valid: ' & clip(p_ObjectID)
    return
  end
  longval = substring
  realval = substring
  do EncodeSubID
  p_IDsize += subidsize


! ----------------------------------------------------------------------------
! BER Encodes a subidentifier of an objectID
EncodeSubID routine
  DATA
Currentbyte             byte
TempString              String(20)
tempval                                 real !for debugging
  code

  subidsize = 0
  TempString = ''
  tempval = realval
  loop while realval > 0
        subidsize += 1
        Currentbyte = realval % 128
        realval = int((realval - Currentbyte) / 128)
        if subidsize > 1
                Currentbyte = bor(Currentbyte, 10000000b)
        end
        TempString[subidsize] = chr(Currentbyte)
  end
  if subidsize = 0
        subidsize = 1
        Tempstring[1] = chr(0)
  end
  counter1 = subidsize
  loop while counter1 > 0
          self._AddChar( TempString[counter1], p_targetString )
      counter1 = counter1 - 1
  end

!------------------------------------------------------------------------------
NetSNMP._EncodeSNMPMessage Procedure  (*string p_TargetString)
counter2         long
messagelen      long                          !Size of the entire message excluding the first 2 bytes
vblsize         long                          !Size of the (encoded) variable bindings structure (in bytes)
vblsizelen      long                          !Length of vblsize in bytes
vblentrysizelen long                          !Size of vblentrylen encoded (in bytes)
vblpos          long                          !Position of varbindlist's size in (encoded) packet
pdupos          long                          !Position of (encoded) pdu's size in (encoded) packet
namepos         long                          !Position of (encoded) varbindlistentry's size
totnamesize     long                          !Size of encoded objectid (including header)
totdatasize     long                          !Size of the encoded data (including header)
datasizelen     long
pdusize         long                          !Size of the encoded pdu (excluding header)
stringval       string(4)

  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._EncodeSNMPMessage', 'Called')
    end
  ****

  self._functionname = '_EncodeSNMPMessage'

  self.packet.bindata = ''
  loop counter2 = 1 to 500
    self._blanks[counter2] = 0
  end
  self._blankspos = 0
  self._encodeError = ''
  self._currpos = 0
  !------------------------------------------------------------------------------------------------------
  ! Start encoding the message (BER)
  !------------------------------------------------------------------------------------------------------
  self._AddChar( chr(030h) , p_targetString)                                      !Universal Sequence for start of message
  self._AddChar( '0' , p_targetString)                                            !message size -> done later
  !------------------------------------------------------------------------------------------------------
  self._AddChar( chr(2) , p_targetString)                                         !Integer for version number
  self._AddChar( chr(1) , p_targetString)                                         !Length of version number -> len = 1 is safe assumption
  if self.UseV2C                                                          !bk - set version number to 1
    self.SNMPVersion = 1
  else
    self.SNMPVersion = 0
  end
  self._AddChar( chr(self.SNMPVersion) , p_targetString)             !Value of version number = 0
  messagelen   = messagelen + 1 + 1 + 1
  !------------------------------------------------------------------------------------------------------
  self._AddChar( chr(4) , p_targetString)                               !String for community name
  self._AddChar(chr(len(clip(self.community))) , p_targetString)   !length of community name
  loop counter2 = 1 to len(clip(self.community))
        self._AddChar( self.community[counter2],  p_targetString  )            !Community string
  end
  messagelen   = messagelen + 1 + 1 + len(clip(self.community))
  !------------------------------------------------------------------------------------------------------
  ! Start encoding the pdu
  !------------------------------------------------------------------------------------------------------
  if self._command = NetSNMP:getRequest
    self._AddChar( chr(0a0h) , p_targetString)                     !Getrequest pdu
  elsif self._command = NetSNMP:getNextRequest
    self._AddChar( chr(0a1h) , p_targetString)                     !GetNextrequest pdu
  elsif self._command = NetSNMP:getResponse
    self._AddChar( chr(0a2h) , p_targetString)                     !Getresponse pdu
  elsif self._command = NetSNMP:setRequest
    self._AddChar( chr(0a3h) , p_targetString)                     !Setrequest pdu
  elsif self._command = NetSNMP:trap
    self._AddChar( chr(0a4h) , p_targetString)                     !Trap pdu
  else
    ! Handle this error
    self._encodeError = 'The given command for the pdu is not valid: ' & self._command
    return
  end
  messagelen = messagelen + 1
  self._AddChar( '0'  , p_targetString)                             !length of the getrequest pdu -> to be inserted later
  pdupos = self._currpos
  !------------------------------------------------------------------------------------------------------
  if self._command <> NetSNMP:trap
    stringval = self.requestID                                      !Encode the requestID
    self._EncodeDataType(stringval, NetSNMP:INTEGER, p_targetString, totdatasize)
    if self._encodeError <> ''
      return
    end
    pdusize += totdatasize
    !--------------------------------------------------------------------------------------------------
    stringval = self.ErrorStatus                                    !Encode the ErrorStatus
    self._EncodeDataType(stringval, NetSNMP:INTEGER, p_targetString, totdatasize)
    if self._encodeError <> ''
      return
    end
    pdusize += totdatasize
    !--------------------------------------------------------------------------------------------------
    stringval = self.ErrorIndex                                     !Encode the ErrorIndex
    self._EncodeDataType(stringval, NetSNMP:INTEGER, p_targetString, totdatasize)
    if self._encodeError <> ''
      return
    end
    pdusize += totdatasize
    !--------------------------------------------------------------------------------------------------
  else                                                                !Encode the Enterprise
    self._EncodeDataType(self.enterprise, NetSNMP:OBJECTIDENTIFIER, p_targetString, totdatasize)
    pdusize += totdatasize
    !--------------------------------------------------------------------------------------------------
    self._EncodeDataType(self.AgentAddr, NetSNMP:NetworkAddress, p_targetString, totdatasize)
    if self._encodeError <> ''
      return
    end
    pdusize += totdatasize                                            !Encode the Agent Address
    !--------------------------------------------------------------------------------------------------
    stringval = self.GenericTrap                                  !Encode the GenericTrap
    self._EncodeDataType(stringval, NetSNMP:INTEGER, p_targetString, totdatasize)
    if self._encodeError <> ''
      return
    end
    pdusize += totdatasize
    !--------------------------------------------------------------------------------------------------
    stringval = self.SpecificTrap                                 !Encode the Specific trap
    self._EncodeDataType(stringval, NetSNMP:INTEGER, p_targetString, totdatasize)
    if self._encodeError <> ''
      return
    end
    pdusize += totdatasize
    !--------------------------------------------------------------------------------------------------
    stringval = self.TimeStamp                                    !Encode the TimeStamp
    self._EncodeDataType(stringval, NetSNMP:TimeTicks, p_targetString, totdatasize)
    if self._encodeError <> ''
      return
    end
    pdusize += totdatasize
    !--------------------------------------------------------------------------------------------------
  end
  !------------------------------------------------------------------------------------------------------
  ! Start encoding the variable bindings list
  !------------------------------------------------------------------------------------------------------
  self._AddChar(  chr(030h), p_targetString)                        !New sequence starts (VarbindList)
  pdusize        =  pdusize + 1
  self._AddChar( '0', p_targetString)                              !length of the VarbindList -> inserted later
  vblpos         =  self._currpos                            !save position of varbindlistsize in packet
  !------------------------------------------------------------------------------------------------------
  ! Insert all entries in varbindlist
  loop counter2 = 1 to records(self.qVarBindList)
    get(self.qVarBindList, counter2)
    if errorcode ()
      cycle
    end
    self.qVarBindList.name = clip(self.qVarBindList.name)

    self._AddChar(  chr(030h) , p_targetString)                    !New entry into VarBinList
    self._AddChar(  '0' , p_targetString)                          !Length of entry -> see later
    namepos = self._currpos

    ! Encode the objectID
    self._EncodeDataType(self.qVarBindList.name, NetSNMP:OBJECTIDENTIFIER, p_targetString, totnamesize)
    if self._encodeError <> ''
      return
    end

    ! Now stick in the value
    if self._command = NetSNMP:GetRequest or self._command = NetSNMP:GetNextRequest
      self._EncodeDataType(self.qVarBindList.value, NetSNMP:NULL, p_targetString, |
                           totdatasize, self.qVarBindList.length, self.qVarBindList.BSbits)
    else
      self._EncodeDataType(self.qVarBindList.value, self.qVarBindList.valuetype, p_targetString, |
                           totdatasize, self.qVarBindList.length, self.qVarBindList.BSbits)
    end
    if self._encodeError <> ''
      return
    end
    ! Fill in the total size of this VBList entry
    self._EncodeLength(namepos, totnamesize + totdatasize, vblentrysizelen, p_targetString)
    vblsize = vblsize + (totnamesize + totdatasize + vblentrysizelen + 1)
  end

  !------------------------------------------------------------------------------------------------------
  ! Now insert the packet, pdu and varbindlist sizes
  !------------------------------------------------------------------------------------------------------

  self._EncodeLength(vblpos, vblsize, vblsizelen, p_targetString)
  pdusize = pdusize + vblsize + vblsizelen
  self._EncodeLength(pdupos, pdusize, datasizelen, p_targetString)
  messagelen = messagelen + pdusize + datasizelen
  self._EncodeLength(2, messagelen, datasizelen, p_targetString)
  self._encodedPacketLen = messagelen + datasizelen + 1

  !------------------------------------------------------------------------------------------------------
  ! For debugging
  !------------------------------------------------------------------------------------------------------

  self._encodedData = p_targetString

!------------------------------------------------------------------------------
NetSNMP._EncodeDataType Procedure  (*string p_Value,long p_DataType,*string p_TargetString,*long p_Datasize,byte p_Length=0,byte p_BSbits=0)
!------------------------------------------------------------------------------------------------
! Parameter description
! p_Value         -> string containing the data p_Value to be encoded
! p_DataType      -> the p_DataType of the p_Value to be encoded
! p_TargetString  -> the string to which the encoded p_Value will be added
! p_datasize      -> size of the encoded data entry (given a p_Value inside this procedure)
! p_BSsize        -> for bitstring : size in bytes
! p_BSbits        -> Number of unused bits in bitstring
!
! The encoded piece of data is added to the end of the string
!------------------------------------------------------------------------------------------------


longval     long
ulongval    long ! 20/7/2005 - was ulong
stringval   string(256)
sizelen     long                 !p_Length in bytes needed to express datasize (which is a long)
pos         long
UIntbytes   byte, dim(4), over(ulongval)
Intbytes    byte, dim(4), over(longval)
counter3    long
stringpos   long
substring   string(256)

  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._EncodeDataType', 'Called')
    end
  ****

  self._functionname = '_EncodeDataType'

  p_datasize = 0

  if p_datatype = NetSNMP:INTEGER
    longval = p_value
    self._AddChar( chr(2) , p_targetString )
    self._AddChar(  '0' , p_targetString )
    pos = self._currpos
    do EncodeIntValue
    self._EncodeLength(pos, p_datasize, sizelen, p_targetString)
    p_datasize = p_datasize + 1 + sizelen                         !Datasize contains the size of the entire encoding

  elsif p_datatype = NetSNMP:BITSTRING
    stringval  = p_value
    self._AddChar( chr(3) , p_targetString )
    self._AddChar( '0' , p_targetString )
    pos = self._currpos
    if p_BSbits > 7
        !Handle this error
        self._encodeError = 'Number of unused bits in bitstring not valid (given p_Value was ' & |
                            p_BSbits &')'
        return
    end
    self._AddChar( chr(p_BSbits) , p_targetString )
    loop counter3 = 1 to p_length
        self._AddChar( p_value[counter3] , p_targetString )
    end
    p_datasize = p_length + 1
    self._EncodeLength(pos, p_datasize, sizelen, p_targetString)
    p_datasize = p_datasize + 1 + sizelen


  elsif p_datatype = NetSNMP:STRING
    stringval  = p_value
    self._AddChar( chr(4) , p_targetString )
    self._AddChar( '0' , p_targetString )                   !p_Length will be inserted here
    pos = self._currpos
        loop counter3 = 1 to len(clip(stringval))
                self._AddChar( p_value[counter3], p_targetString )
        end
        p_datasize = len(clip(stringval))
        self._EncodeLength(pos, p_datasize, sizelen, p_targetString)
    p_datasize = p_datasize + 1 + sizelen

  elsif p_datatype = NetSNMP:NULL
    self._AddChar(  chr(5), p_targetString )
    self._AddChar( chr(0), p_targetString )
    p_datasize = 2

  elsif p_datatype = NetSNMP:OBJECTIDENTIFIER
    stringval  = p_value
    self._AddChar( chr(6) , p_targetString )
    self._AddChar( '0' , p_targetString )
    pos = self._currpos
    self._EncodeObjectID(stringval, p_datasize, p_targetString)
    if self._encodeError <> '' then return.
    self._EncodeLength(pos, p_datasize, sizelen, p_targetString)
    p_datasize = p_datasize + 1 + sizelen

  elsif p_datatype = NetSNMP:IpAddress
    stringval  = p_value
    self._AddChar( chr(040h) , p_targetString)
    if p_value <> ''
      self._AddChar( chr(4) , p_targetString)               !This is always the case with ipaddress
      do EncodeIPAddress
      p_datasize = 4 + 1 + 1
    else
      self._AddChar(  chr(0) , p_targetString)
      p_datasize = 1 + 1
    end

  elsif p_datatype = NetSNMP:Counter
    ulongval   = p_value
    self._AddChar( chr(041h) , p_targetString)
    self._AddChar(  '0' , p_targetString)
    pos = self._currpos
    do EncodeUIntValue
    self._EncodeLength(pos, p_datasize, sizelen, p_targetString)
    p_datasize = p_datasize + 1 + sizelen                          !Datasize contains the size of the entire encoding

  elsif p_datatype = NetSNMP:Gauge
    longval    = p_value
    self._AddChar( chr(042h) , p_targetString)
    self._AddChar( '0' , p_targetString)
    pos = self._currpos
    do EncodeUIntValue
    self._EncodeLength(pos, p_datasize, sizelen, p_targetString)
    p_datasize = p_datasize + 1 + sizelen                          !Datasize contains the size of the entire encoding

  elsif p_datatype = NetSNMP:TimeTicks
    ulongval   = p_value
    self._AddChar( chr(043h) , p_targetString)
    self._AddChar( '0' , p_targetString)
    pos = self._currpos
    do EncodeuIntValue
    self._EncodeLength(pos, p_datasize, sizelen, p_targetString)
    p_datasize = p_datasize + 1 + sizelen                          !Datasize contains the size of the entire encoding

  elsif p_datatype = NetSNMP:Opaque
    stringval  = p_value
    self._AddChar( chr(044h) , p_targetString)
    self._AddChar( '0'  , p_targetString)                  !p_Length will be inserted here
    pos = self._currpos
        loop counter3 = 1 to  len(clip(p_value))
                self._AddChar( p_value[counter3] , p_targetString )
        end
    if p_length > len(clip(p_value))
      loop counter3 = 1 to p_length - len(clip(p_value))
        self._AddChar( ' '  , p_targetString)
      end
    end
    p_datasize = p_Length

    self._EncodeLength(pos, p_datasize, sizelen, p_targetString)
    p_datasize = p_datasize + 1 + sizelen

  elsif p_datatype = NetSNMP:NetworkAddress
    stringval  = p_value
    self._AddChar( chr(045h) , p_targetString)
    if p_value <> ''
      self._AddChar( chr(4) , p_targetString)              !This is always the case with ipaddress
      do EncodeIPAddress
      p_datasize = 4 + 1 + 1
    else
      self._AddChar( chr(0) , p_targetString)
      p_datasize = 1 + 1
    end
  else
    !Handle this error
    self._encodeError = 'The given p_DataType is not valid (the given type was ' & p_datatype &')'
    return
  end

! --------------------------------------------------------------------
EncodeIntValue routine
        DATA
byteoffset              long
        code
 ! bk - above code does not properly handle if a byte within the long has all bits set to 0 - needs to be traversed from most highest byte to lowest byte to derive the data size
  p_datasize = 4
  if intbytes[4] = 0
        p_datasize = 3
        if intbytes[3] = 0
                p_datasize = 2
                if intbytes[2] = 0
                        p_datasize = 1
                else
                        p_datasize = 2
                end
        end
  end
  loop byteoffset = p_datasize to 1 by -1
        self._AddChar( chr(intbytes[byteoffset]) , p_targetString )
  end


! -----------------------------------------------------------------
EncodeUIntValue routine
        DATA
byteoffset              long
        code
  p_datasize = 4
  if uintbytes[4] = 0
        p_datasize = 3
        if uintbytes[3] = 0
                p_datasize = 2
                if uintbytes[2] = 0
                        p_datasize = 1
                else
                        p_datasize = 2
                end
        end
  end
  loop byteoffset = p_datasize to 1 by -1
        self._AddChar( chr(uintbytes[byteoffset]), p_targetString )
  end
! ---------------------------------------------------------
EncodeIPAddress  routine
  substring = stringval
  stringpos = instring('.', substring, 1, 1)
  if stringpos = 0
    !Not a valid IP address
    self._encodeError = 'The given IPAddress is not valid (p_Value was ' & clip(p_value) &')'
    !Handle this error
    return
  end

  loop counter3 = 1 to 3
    if numeric(substring[1 : stringpos - 1]) = 0          !Make sure we have a valid address here
      !Handle this error
      self._encodeError = 'The given IPAddress is not valid (the p_Value was ' & clip(p_value) &')'
      return
    end
    longval = substring[1 : stringpos - 1]
    if longval < 0
      !Handle this error
      self._encodeError = 'The given IPAddress is not valid (the p_Value was ' & clip(p_value) &')'
      return
    end
    if longval > 255
      ! Handle this error
      self._encodeError = 'The given IPAddress is not valid (the p_Value was ' & clip(p_value) &')'
      return
    end
    self._AddChar( chr(longval) , p_targetstring)
    substring = substring[stringpos + 1 : len(substring)] !Cut out the part of the address already entered (including the following dot)
    stringpos = instring('.', substring, 1, 1)
    if stringpos = 0 and counter3 < 3
      !Handle this error
      self._encodeError = 'The given IPAddress is not valid (the p_Value was ' & clip(p_value) &')'
      return
    end
  end

  if numeric(substring) = 0
    !Handle this error
    self._encodeError = 'The given IPAddress is not valid (the p_Value was ' & clip(p_value) &')'
    return
  end
  longval = substring
  if longval < 0
    !Handle this error
    self._encodeError = 'The given IPAddress is not valid (the p_Value was ' & clip(p_value) &')'
    return
  end
  if longval > 255
    !Handle this error
    self._encodeError = 'The given IPAddress is not valid (the p_Value was ' & clip(p_value) &')'
    return
  end
  self._AddChar( chr(longval) , p_targetString )

 !------------------------------------------------------------------------------
NetSNMP._AddChar Procedure  (string p_char, *string p_targetString)
        code
        self._currpos += 1
        if p_char
                p_targetString[self._currpos] = p_char[1]
        end

!------------------------------------------------------------------------------
NetSNMP._DecodeSNMPMessage Procedure  (*string p_BERString,*byte p_AuthenticationFailed)
messagelength       long            !Length of message given in packet
pdulength           long            !Length of pdu given in packet
encdatalength       long            !Length of a decoded piece of data
varbindlistlen      long            !Length of the varbindlist given in the packet
dummy               long            !For arb uses
lenfieldsize        long            !Number of bytes taken up by the encoded length field
datatype            long            !SNMP (ASN.1) Type of the data value
stringval           string(256)     !To convert all longs, ulongs and strings into a string value
authentic           byte            !Wether the message is authentic or not

  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._DecodeSNMPMessage', 'Called')
    end
  ****

  self._functionname = '_DecodeSNMPMessage'

  self.SNMPVersion = 0
  self.community = ''
  self.requestID = 0
  self.ErrorStatus = 0
  self.ErrorIndex = 0
  self.enterprise = ''
  self.AgentAddr = ''
  self.genericTrap = 0
  self.specificTrap = 0
  self.timestamp = 0
  self.V2Trap = 0     !bk
  self.snmpTrapOID = ''
  free(self.qTrapOID)
  clear(self.qTrapOID)
  self._command = 0
  free(self.qVarBindList)

  self._decodeError = ''

  self._encodedData = p_BERString   !For debugging

  !-------------------------------------------------------------------------------------------------------
  ! Start decoding the message
  !-------------------------------------------------------------------------------------------------------
  if p_BERString[1] <> chr(030h)               ! First there should be the message header
    ! Handle this error
    self._decodeError = 'incorrect encoding of construct header (message start)'
    return
  end
  p_BERString = p_BERString[2 : len(p_BERString)]
  self._DecodeLength(p_BERString, messagelength, lenfieldsize)          !Now there should be the message length
  !-------------------------------------------------------------------------------------------------------
  if val(p_BERString[1]) <> 2                  ! Next comes the version number
    !Handle this error
    self._decodeError = 'incorrect encoding of version number'
    return
  end
  p_BERString = p_BERString[2 : len(p_BERString)]
  if val(p_BERString[1]) <> 1                  ! How could there ever be > 127 versions?
    !Handle this error
    self._decodeError = 'incorrect encoding of version number'
    return
  end
  p_BERString = p_BERString[2 : len(p_BERString)]
  self.SNMPversion = val(p_BERString[1])
  p_BERString = p_BERString[2 : len(p_BERString)]
  messagelength = messagelength - 1 - 1 - 1

  ! Check that we have received the correct version
  if self.SNMPversion > 1          !bk - We now support SNMPV2C when decoding - no support for bulk requests/response
    ! Handle this error
    self._decodeError = 'Unsupported SNMP version'
    return
  end
  !-------------------------------------------------------------------------------------------------------
  if val(p_BERString[1]) <> 4                    ! We should now have the community string
    ! Handle this error
    self._decodeError = 'incorrect encoding of community'
    return
  end
  p_BERString = p_BERString[2 : len(p_BERString)]
  self._DecodeLength(p_BERString, encdatalength, lenfieldsize)
  self.community = p_BERString[1 : encdatalength]
  p_BERString = p_BERString[encdatalength + 1 : len(p_BERString)]
  messagelength = messagelength - encdatalength - lenfieldsize - 1
  !-------------------------------------------------------------------------------------------------------
  authentic = 1
  if self.CheckAuthenticity() = false
    self._decodeError = 'authentication failed'
    p_AuthenticationFailed = true
    return
  end
  !-------------------------------------------------------------------------------------------------------
  ! Start decoding the pdu
  !-------------------------------------------------------------------------------------------------------
  if val(p_BERString[1]) = 0a0h                                         !PDU header
    self._command    = NetSNMP:GetRequest
  elsif val(p_BERString[1]) = 0a1h
    self._command    = NetSNMP:GetNextRequest
  elsif val(p_BERString[1]) = 0a2h
    self._command    = NetSNMP:GetResponse
  elsif val(p_BERString[1]) = 0a3h
    self._command    = NetSNMP:SetRequest
  elsif val(p_BERString[1]) = 0a4h
    self._command    = NetSNMP:Trap
  elsif val(p_BERString[1]) = 0a7h
    self._command    = NetSNMP:TrapV2       !bk
  else
    ! Handle this error
    self._decodeError = 'incorrect encoding of pdu header'
    return
  end

  p_BERString = p_BERString[2 : len(p_BERString)]
  self._DecodeLength(p_BERString, pdulength, lenfieldsize)               !PDU length
  messagelength = messagelength - 1 - pdulength - lenfieldsize
  if messagelength <> 0
    ! Handle this error
    self._decodeError = 'incorrect message length'
    return
  end
  !-------------------------------------------------------------------------------------------------------
  if self._command <> NetSNMP:trap       ! Normal pdu
    self._DecodeDataType(p_BERString, stringval, datatype, encdatalength, dummy, dummy) !Decode RequestID
    if self._decodeError <> '' then return.
    if datatype <> NetSNMP:INTEGER
      ! Handle this error
      self._decodeError = 'incorrect encoding of requestID'
      return
    end
    self.requestID = stringval
    pdulength -= encdatalength
    !-----------------------------------------------------------------------------------------------
    self._DecodeDataType(p_BERString, stringval, datatype, encdatalength, dummy, dummy) !Decode ErrorStatus
    if self._decodeError <> '' then return.
    if datatype <> NetSNMP:INTEGER
      ! Handle this error
      self._decodeError = 'incorrect encoding of errorstatus'
      return
    end
    self.ErrorStatus = stringval
    pdulength -= encdatalength
    !-----------------------------------------------------------------------------------------------
    self._DecodeDataType(p_BERString, stringval, datatype, encdatalength, dummy, dummy) !Decode ErrorIndex
    if self._decodeError <> '' then return.
    if datatype <> NetSNMP:INTEGER
      ! Handle this error
      self._decodeError = 'incorrect encoding of errorindex'
      return
    end
    self.ErrorIndex = stringval
    pdulength -= encdatalength
    !-----------------------------------------------------------------------------------------------
  else                                                                !Trap pdu
    self._DecodeDataType(p_BERString, self.Enterprise, datatype, encdatalength, dummy, dummy)
    if self._decodeError <> '' then return.
    if datatype <> NetSNMP:OBJECTIDENTIFIER                                 !Decode the enterprise objectID
      !Handle this error
      self._decodeError = 'incorrect encoding of enterprise'
      return
    end
    pdulength -= encdatalength
    !-----------------------------------------------------------------------------------------------
    self._DecodeDataType(p_BERString, self.AgentAddr, datatype, encdatalength, dummy, dummy)
    if self._decodeError <> '' then return.
    if datatype <> NetSNMP:NetworkAddress  and datatype <> NetSNMP:IPAddress         !Decode Agent address - bk - added check ofr NetSNMP:IPAddress
      !Handle this error
      self._decodeError = 'incorrect encoding of agent address'
      return
    end
    pdulength -= encdatalength
    !-----------------------------------------------------------------------------------------------
    self._DecodeDataType(p_BERString, stringval, datatype, encdatalength, dummy, dummy)
    if self._decodeError <> '' then return.
    if datatype <> NetSNMP:INTEGER                                          !Decode Generic trap
      !Handle this error
      self._decodeError = 'incorrect encoding of generic trap'
      return
    end
    self.GenericTrap = stringval
    pdulength -= encdatalength
    !-----------------------------------------------------------------------------------------------
    self._DecodeDataType(p_BERString, stringval, datatype, encdatalength, dummy, dummy) !Decode Specific Trap
    if self._decodeError <> '' then return.
    if datatype <> NetSNMP:INTEGER
      !Handle this error
      self._decodeError = 'incorrect encoding of specific trap'
      return
    end
    self.SpecificTrap = stringval
    pdulength -= encdatalength
    !-----------------------------------------------------------------------------------------------
    self._DecodeDataType(p_BERString, stringval, datatype, encdatalength, dummy, dummy) !Decode Time Stamp
    if self._decodeError <> '' then return.
    if datatype <> NetSNMP:TimeTicks
      !Handle this error
      self._decodeError = 'incorrect encoding of time stamp'
      return
    end
    self.TimeStamp = stringval
    pdulength -= encdatalength
    !-----------------------------------------------------------------------------------------------
  end
  !-------------------------------------------------------------------------------------------------------
  ! Start decoding the variable bindings list
  !-------------------------------------------------------------------------------------------------------
  if val(p_BERString[1]) <> 030h                                        !We should have the start of the varbindlist here
    !Handle this error
    self._decodeError = 'incorrect encoding of construct header (variable bindings list start)'
    return
  end
  p_BERString = p_BERString[2 : len(p_BERString)]
  self._DecodeLength(p_BERString, varbindlistlen, lenfieldsize)          !Decode the length of the varbindlist
  pdulength -= lenfieldsize
  pdulength -= varbindlistlen
  pdulength -= 1
  if pdulength <> 0
    !Handle this error
    self._decodeError = 'incorrect pdu length'
    return
  end

  !-------------------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------------------
  free(self.qVarBindList)
  loop until varbindlistlen <= 0                                      !Add all the varbindlist entries
    if val(p_BERString[1]) <> 030h                                    !We should have the start of the varbindlist entry
      !Handle this error
      self._decodeError = 'incorrect construct header (variable binding entry start)'
      return
    end
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, encdatalength, lenfieldsize)          !length of varbindlistentry
    varbindlistlen -= encdatalength
    varbindlistlen -= lenfieldsize
    varbindlistlen -=1

    self._DecodeDataType(p_BERString, stringval, datatype, encdatalength, dummy, dummy)
    if self._decodeError <> ''
      return
    end
    if datatype <> NetSNMP:OBJECTIDENTIFIER
      !Handle this error
      self._decodeError = 'incorrect encoding of name in varbindlist'
      return
    end
    self.qVarBindList.Name = stringval
    self._DecodeDataType(p_BERString, stringval, datatype, encdatalength, self.qVarBindList.length, self.qVarBindList.BSbits)
    if self._decodeError <> ''
      return
    end
    self.qVarBindList.Value     = stringval
    self.qVarBindList.Valuetype = datatype
    clear (self.qDescriptionMap)
    self.qDescriptionMap.Name = self.qVarBindList.Name
    get (self.qDescriptionMap, self.qDescriptionMap.Name)
    if errorcode() = 0
      self.qVarBindList.DescriptionIn = self.qDescriptionMap.DescriptionIn
    else
      self.qVarBindList.DescriptionIn = ''
    end
    add(self.qVarBindList)
  end

  if varbindlistlen <> 0
    !Handle this error
    self._decodeError = 'incorrect varbindlist length'
    return
  end



!------------------------------------------------------------------------------
NetSNMP._DecodeLength Procedure  (*string p_BERString,*long p_Length,*long p_LenFieldSize)
!-------------------------------------------------------------------------------------------------
! Parameter information
! p_BERString    -> String containing the p_Length field (assumed to be at beginning of string)
! p_Length       -> the value of the extracted p_Length will be placed in here
! p_LenFieldSize -> size (in bytes) that the actual p_Length field actually took up in the p_BERString
!
! The p_Length field is removed from the string before returning
!-------------------------------------------------------------------------------------------------

initialbyte     byte
lenbytes        byte, dim(4), over(p_Length)
counter5        long
myLen           long
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._DecodeLength', 'Called')
    end
  ****

  self._functionname = '_DecodeLength'

  !First get out the first byte to see how long the p_Length field is
  initialbyte = val(p_BERString[1])
  myLen = len(p_BERString)
  if myLen < 2
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSNMP._DecodeLength', 'Error - Index out of bounds #1', NET:LogError)
    ****
  else
    p_BERString = p_BERString[2 : myLen]
  end
  if initialbyte < 128            !There is only one p_Length octet
    p_Length = initialbyte
    p_LenFieldSize = 1
    Do LogReturn
    return
  end

  initialbyte -= 128
  p_LenFieldSize = initialbyte + 1        ! + 1 for the initial byte
  if initialbyte > 4
    !Handle this error
    Do LogReturn
    return
  end
  if initialbyte = 0
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSNMP._DecodeLength', 'Error - Index out of bounds #2', NET:LogError)
    ****
  else
    loop counter5 = 1 to initialbyte
      lenbytes[counter5] = val(p_BERString[initialbyte - counter5 + 1])
    end
  end

  myLen = len(p_BERString)
  if (initialbyte + 1) > myLen
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSNMP._DecodeLength', 'Error - Index out of bounds #3', NET:LogError)
    ****
  else
    p_BERString = p_BERString[initialbyte + 1 : myLen]
  end

  Do LogReturn


! ----------------------------------------------------------------------
LogReturn Routine
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._DecodeLength', 'Return')
    end
  ****



!------------------------------------------------------------------------------
NetSNMP._DecodeDataType Procedure  (*string p_BERString,*string p_Value,*long p_Datatype,*long p_Datalength,*long p_Length,*long p_BSbits)
!-------------------------------------------------------------------------------------------------
! Paramater information
! p_BERString  -> string contianing data to be decoded (entry assumed to be at beggining of string)
! p_Value      -> string to place extracted p_Value in
! p_Datatype   -> type of the decoded data
! p_DataLength -> p_Length of the entire p_Datatype entry in encoded string
! BSsize     -> only if passing a bitstring: the size of the bitstring in bytes (will be extracted)
! p_BSbits     -> the number of unused bits in the bitstring (will be extracted)
!
! The p_Datatype entry in the p_BERString is cut out of the p_BERString before returning
!-------------------------------------------------------------------------------------------------


counter6        long
longval         long
ulongval        ulong !bk
lenfieldsize    long
Intbytes        byte, dim(4), over(longval)
UIntbytes       byte, dim(4), over(ulongval)
c64counter      REAL    !bk
  code
  if len (p_BERString) < 1
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSNMP._DecodeDataType', 'Called with invalid BERString size', NET:LogError)
    ****
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._DecodeDataType', 'Called. p_BERString[1] = ' & p_BERString[1] & |
                ' p_DataLength = ' & p_DataLength)
    end
  ****

  self._functionname = '_DecodeDataType'

  p_Value = ''
  p_DataLength = 0

  if val(p_BERString[1]) = 2
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    do decodeIntValue
    p_Value = longval
    p_BERString = p_BERString[p_DataLength + 1 : len(p_BERString)]
    p_Datatype = NetSNMP:INTEGER
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 3
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    if p_DataLength < 1
      !Handle this error
      self._decodeError = 'bitstring incorrect p_Length (p_Length was ' &p_DataLength &')'
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSNMP._DecodeDataType', 'Returning. Error BitString Incorrect Length')
        end
      ****
      return
    end
    !Parse out the byte holding the number of unused bits
    p_BSbits = val(p_BERString[1])                        !num unused bits
    p_BERString = p_BERString[2 : len(p_BERString)]
    if p_DataLength > 1
        p_Value = p_BERString[1 : p_DataLength - 1]
        p_BERString = p_BERString[p_DataLength : len(p_BERString)]
    else
        p_Value = ''
    end
    p_Length = p_DataLength - 1
    p_Datatype = NetSNMP:BITSTRING
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 4
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    p_Value = p_BERString[1 : p_DataLength]
    p_BERString = p_BERString[p_DataLength + 1 : len(p_BERString)]
    p_Datatype = NetSNMP:STRING
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 5
    p_BERString = p_BERString[2 : len(p_BERString)]
    if val(p_BERString[1]) <> 0
      !Handle this error
      self._decodeError = 'incorrect null encoding (p_Length was given as ' &val(p_BERString[1]) &')'
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSNMP._DecodeDataType', 'Returning. Error Null Encoding error')
        end
      ****
      return
    end
    p_BERString = p_BERString[2 : len(p_BERString)]
    p_Value = ''
    p_Datatype = NetSNMP:NULL
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 6
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    self._DecodeObjectID(p_BERString, p_DataLength, p_Value)
    if self._decodeError <> ''
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSNMP._DecodeDataType', 'Returning. Error Decode Error ' & clip (self._decodeError))
        end
      ****
      return
    end
    p_Datatype = NetSNMP:OBJECTIDENTIFIER
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 040h
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    if p_DataLength <> 4
      !Handle this error
      self._decodeError = 'IP Address incorrect p_Length (p_Length was ' &p_DataLength &')'
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSNMP._DecodeDataType', 'Returning. Error IP Address incorrect.')
        end
      ****
      return
    end
    do DecodeIPAddress
    p_BERString = p_BERString[4 + 1 : len(p_BERString)]
    p_Datatype = NetSNMP:IPAddress
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 041h
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    do decodeUIntValue
    p_Value = ulongval
    p_BERString = p_BERString[p_DataLength + 1 : len(p_BERString)]
    p_Datatype = NetSNMP:Counter
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 042h
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    do decodeIntValue
    p_Value = longval
    p_BERString = p_BERString[p_DataLength + 1 : len(p_BERString)]
    p_Datatype = NetSNMP:Gauge
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 043h
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    do decodeUIntValue
    p_Value = ulongval
    p_BERString = p_BERString[p_DataLength + 1 : len(p_BERString)]
    p_Datatype = NetSNMP:TimeTicks
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 044h
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    p_Value = p_BERString[1 : p_DataLength]
    p_BERString = p_BERString[p_DataLength + 1 : len(p_BERString)]
    p_Datatype = NetSNMP:Opaque
    p_Length = p_DataLength
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 045h
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    if p_DataLength <> 4
      ! Handle this error
      self._decodeError = 'Network address incorrect p_Length (p_Length was' &p_DataLength &')'
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSNMP._DecodeDataType', 'Returning. Error Network Address Incorrect')
        end
      ****
      return
    end
    do DecodeIPAddress
    p_BERString = p_BERString[4 + 1 : len(p_BERString)]
    p_Datatype = NetSNMP:NetworkAddress
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize

  elsif val(p_BERString[1]) = 046h                  !bk
    p_BERString = p_BERString[2 : len(p_BERString)]
    self._DecodeLength(p_BERString, p_DataLength, lenfieldsize)
    do DecodeC64Value
    p_BERString = p_BERString[p_DataLength + 1 : len(p_BERString)]
    p_Datatype = NetSNMP:Counter64
    p_Length = 0
    p_BSbits = 0
    p_DataLength = p_DataLength + 1 + lenfieldsize
  elsif val(p_BERString[1]) = NetSNMP:NoSuchObject                  !bk - No Such Object for SNMPV2c
        self._decodeError = 'No Such Object Encountered'
  elsif val(p_BERString[1]) = NetSNMP:NoSuchInstance                !bk - No Such Instance for SNMPV2c
        self._decodeError = 'No Such Instance Encountered encountered'
  elsif val(p_BERString[1]) = NetSNMP:EndOfMibView                  !bk - END OF MIB VIEW for SNMPV2c
        self._decodeError = 'End of MIB View has been encountered'
  else
    !Handle this error
    self._decodeError = 'Invalid Datatype specified (p_Datatype given was ' &val(p_BERString[1]) &')'
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSNMP._DecodeDataType', 'Returning. Error Invalid p_DataType')
        end
      ****
    return
  end

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._DecodeDataType', 'Returning Fine.')
    end
  ****


! ---------------------------------------------------------------------------
DecodeIntValue  routine
 counter6 = p_DataLength
 loop while (counter6 > 0) and ((p_DataLength - counter6 + 1) <= 4)
   Intbytes[p_DataLength - counter6 + 1] = val(p_BERString[counter6])
   counter6 = counter6 - 1
 end


! ---------------------------------------------------------------------------
DecodeUIntValue routine
  counter6 = p_DataLength
  loop while (counter6 > 0) and ((p_DataLength - counter6 + 1) <= 4)
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSNMP._DecodeDataType', 'Decode UINT ' & (p_DataLength - counter6 + 1) & ' : ' & counter6)
      end
    ****
    UIntbytes[p_DataLength - counter6 + 1] = val(p_BERString[counter6])
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSNMP._DecodeDataType', 'Decode UInt Made it')
      end
    ****
    counter6 = counter6 - 1
  end

! ---------------------------------------------------------------------------
DecodeIPAddress routine
  loop counter6 = 1 to 4
    longval = val(p_BERString[counter6])
    if longval > 255 or longval < 0
      !Handle this error
      self._decodeError = 'Invalid IP or Network address (subid ' &longval &' is invalid)'
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSNMP._DecodeDataType', 'Returning. Error IP or Network Address Invalid')
        end
      ****
      return
    end
    p_Value = clip(p_Value) & longval
    if counter6 < 4
      p_Value = clip(p_Value) & '.'
    end
  end

! ---------------------------------------------------------------------------
DecodeC64Value routine                      !bk
    loop counter6 = 1 to p_DataLength
        c64Counter = c64Counter * 256 + val(p_BERString[counter6])
    end
   p_value = c64counter
 !------------------------------------------------------------------------------
NetSNMP._DecodeObjectID Procedure  (*string p_BERString,long p_Datalength,*string p_Value)
subidlen    long
remlen      long
longval         real    !bk - some values on snmpv2 > 2^31
bytes       byte, dim(4), over(longval)
counter5    long
lost3bits   byte
templen     long
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._DecodeObjectID', 'Called')
    end
  ****

  self._functionname = '_DecodeObjectID'

  p_Value = ''

  ! The first two subidentifiers were encoded in one byte
  if val(p_BERString[1]) < 40           !first node was zero (ccit)
    p_Value = '0.' & val(p_BERString[1])
  elsif val(p_BERString[1]) < 80        !First node was one  (iso)
    p_Value = '1.' & (val(p_BERString[1]) - 40)
  else                                !First node was two  (joint-iso-ccit)
    p_Value = '2.' & (val(p_BERString[1]) - 80)
  end
  p_BERString = p_BERString[2 : len(p_BERString)]

  ! Now decode the rest of the subidentifiers
  remlen = p_Datalength
  remlen -= 1                             !already did the first byte above
  loop until remlen <= 0
    do DecodeSubID
    p_Value = clip(p_Value) & '.' & longval
    remlen -= subidlen
    p_BERString = p_BERString[subidlen + 1 : len(p_BERString)]
  end
  if remlen < 0
    ! Handle this error
    self._decodeError = 'Invalid object identifier'
    return
  end


!--------------------------------------------------------------------
!Decodes a subidentifier of an objectID
DecodeSubID routine
  DATA
Currentbyte             byte
  code
  longval   = 0
  lost3bits = 0
  subidlen  = 1
  omit('!!!')
  loop while val(p_BERString[subidlen]) > 127
    if subidlen > 5
      !Handle this error -> you probably cant have that big an id
      self._decodeError = 'object identifier too large'
      return
    end
    subidlen += 1
  end

  templen = subidlen
  if subidlen = 5
    templen   = templen - 1
    lost3bits = bshift(band(val(p_BERString[1]), 01111111b), 5)
  end

  loop counter5 = 1 to templen
    bytes[counter5] = val(p_BERString[subidlen + 1 - counter5])
  end

  longval = band(longval, 03FFFFFFFh)
  longval = band(longval, 07Fh)     + bshift(band(longval, 0FFFFFF00h), -1)
  longval = band(longval, 03FFFh)   + bshift(band(longval, 0FFFF8000h), -1)
  longval = band(longval, 01FFFFFh) + bshift(band(longval, 0FFC00000h), -1)

  if lost3bits > 0
    bytes[4] += lost3bits
  end
  !!!
  loop while val(p_BERString[subidlen]) > 127
        Currentbyte = band(val(p_BERString[subidlen]), 01111111b)
        longval = longval * 128 + Currentbyte
        subidlen += 1
  end
  Currentbyte = val(p_BERString[subidlen])
  longval = longval * 128 + Currentbyte

!------------------------------------------------------------------------------
NetSNMP._ProcessGet  Procedure
varbindpos   long
errorValue   long
tempvarbinds queue (NetSNMP:VarbindListType).
getresult    byte
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._ProcessGet', 'Called')
    end
  ****

  !Get the community profile for this guy
  self.qCommunityProfiles.communityName = self.community
  get(self.qCommunityProfiles, self.qCommunityProfiles.communityname)
  if errorcode ()    !Probably No such community
    return
  end

  !Currently the self.varbindlist contains all nulls -> lets keep it that way in case something goes wrong
  loop varbindpos = 1 to records(self.qVarBindList)
    get(self.qVarBindList, varbindpos)
    tempvarbinds.name = self.qVarBindList.name
    add(tempvarbinds)
  end

  !Get all the requested entries
  loop varbindpos = 1 to records(tempvarbinds)
    get(tempvarbinds, varbindpos)
    if errorcode()
      !General error: Error = GenErr
      errorValue = 5
      do sendErrorResponse
      return
    end
    getresult = self._inview (tempvarbinds.name, self.qCommunityProfiles.communityname)
    if getresult = false
      !Entry not found: Error = NoSuchName
      errorValue = 2
      do sendErrorResponse
      return
    elsif getresult = 3
      !General error: Error = GenErr
      errorValue = 5
      do sendErrorResponse
      return
    end

    self.qMIBEntries.name = tempvarbinds.name
    get(self.qMIBEntries, self.qMIBEntries.name)
    if errorcode() = 30
      !Entry not found: Error = NoSuchName
      errorValue = 2
      do sendErrorResponse
      return
    elsif errorcode()
      !General error: Error = GenErr
      errorValue = 5
      do sendErrorResponse
      return
    elsif self.qMIBEntries.access = NetSNMP:WriteOnly or self.qMIBEntries.access = NetSNMP:NotAccessible
      !Entry not found: Error = NoSuchName
      errorValue = 2
      do sendErrorResponse
      return
    end
    tempvarbinds.valuetype = self.qMIBEntries.valuetype
    tempvarbinds.value     = self.qMIBEntries.value
    tempvarbinds.BSBits    = self.qMIBEntries.BSBits
    tempvarbinds.length    = self.qMIBEntries.length
    put(tempvarbinds)
    if errorcode()
      !General error: Error = GenErr
      errorValue = 5
      do sendErrorResponse
      return
    end
  end

  !Now try to send the response pdu
  self.ErrorStatus = 0
  self.ErrorIndex  = 0

  loop varbindpos = 1 to records(self.qVarBindList)
    get(self.qVarBindList, varbindpos)
    get(tempvarbinds, varbindpos)
    self.qVarBindList.value     = tempvarbinds.value
    self.qVarBindList.valuetype = tempvarbinds.valuetype
    self.qVarBindList.length    = tempvarbinds.length
    self.qVarBindList.BSbits    = tempvarbinds.BSbits
    put(self.qVarBindList)
  end

  self.packet.toIP      = self.packet.fromIP
  self._SendResponse()

! ------------------------------------------------------------------------
! One of the mibEntries were not available: Error = NoSuchName
SendErrorResponse routine
  self.ErrorStatus    = errorValue
  self.ErrorIndex     = varbindpos
  self.packet.UDPToPort = 161
  self.packet.toIP      = self.packet.fromIP
  self._SendResponse()

!------------------------------------------------------------------------------
NetSNMP._ProcessSet  Procedure
svarbindpos     long
tempMIBS        queue (NetSNMP:VarbindlistType).
errorValue      long
setresult       byte
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._ProcessSet', 'Called')
    end
  ****

  !Get the community profile for this guy
  self.qCommunityProfiles.communityName = self.community
  get(self.qCommunityProfiles, self.qCommunityProfiles.communityname)
  if errorcode ()    !Probably No such community
    return
  end
  if lower(self.qCommunityProfiles.permission) <> NetSNMP:ReadWrite
    !This guy is not allowed to write this value: Error = NoSuchName
    errorValue = 2
    do sendErrorResponse
    return
  end

  !Save all the changed to be made in tempMIBS first in case something goes wrong
  loop svarbindpos = 1 to records(self.qVarBindList)
    get(self.qVarBindList, svarbindpos)
    if errorcode ()
      !General error: Error = GenErr
      errorValue = 5
      do sendErrorResponse
      return
    end
    setresult = self._inview (self.qVarBindList.name, self.qCommunityProfiles.communityname)
    if setresult = false
      !Entry not found: Error = NoSuchName
      errorValue = 2
      do sendErrorResponse
      return
    elsif setresult = 3
      !General error: Error = GenErr
      errorValue = 5
      do sendErrorResponse
      return
    end
    self.qMIBEntries.name = self.qVarBindList.name
    get(self.qMIBEntries, self.qMIBEntries.name)
    if errorcode () = 30
      !Entry not found: Error = NoSuchName
      errorValue = 2
      do sendErrorResponse
      return
    elsif errorcode ()
      !General error: Error = GenErr
      errorValue = 5
      do sendErrorResponse
      return
    end
    if lower(self.qMIBEntries.access) <> NetSNMP:ReadWrite and lower(self.qMIBEntries.access) <> NetSNMP:WriteOnly
      !This guy is not allowed to write this value: Error = NoSuchName
      errorValue = 2
      do sendErrorResponse
      return
    end
    if self.qMIBEntries.valuetype <> self.qVarBindList.valuetype
      !We have the wrong type of value here: Error = BadValue
      errorValue = 3
      do sendErrorResponse
      return
    end
    tempMIBS.name      = self.qVarBindList.name
    tempMIBS.valuetype = self.qVarBindList.valuetype
    tempMIBS.value     = self.qVarBindList.value
    tempMIBS.length    = self.qVarBindList.length
    tempMIBS.BSbits    = self.qVarBindList.BSbits
    add(tempMIBS)
    if errorcode ()
      !General Error: Error = GenErr
      errorvalue = 5
      do sendErrorResponse
      return
    end
  end

  !Now try to send the response pdu
  self.ErrorStatus      =  0
  self.ErrorIndex       =  0
  self.packet.toIP      = self.packet.fromIP
  self._SendResponse()

  if self.ErrorStatus = 0
    !Now we know the packet was (for example) not too big, so set the value
    loop svarbindpos = 1 to records(tempMIBS)
      get(tempMIBS, svarbindpos)
      self.qMIBEntries.name = tempMIBS.name
      get(self.qMIBEntries, self.qMIBEntries.name)
      self.qMIBEntries.value     = tempMIBS.value
      self.qMIBEntries.length    = tempMIBS.length
      self.qMIBEntries.BSbits    = tempMIBS.BSbits
      put(self.qMIBEntries)
    end
  end


! -----------------------------------------------------------------------------

!Sends a response pdu indicating that something wet wrong during set
sendErrorResponse routine
  self.ErrorStatus      = errorValue
  self.ErrorIndex       = svarbindpos
  self.packet.toIP      = self.packet.fromIP
  self._SendResponse()



!------------------------------------------------------------------------------
NetSNMP._ProcessGetNext Procedure
gnvarbindpos    long
gntempvarbinds  queue (NetSNMP:VarbindlistType).
errorValue      long
varbindName     like  (NetSNMP:VarbindlistType.name)
MIBName         like  (NetSNMP:MIBQueueType.name)
subid           long ! 20/7/2005 - was ulong
mibsubid        long
dotpos          long ! 20/7/2005 - was ulong
comparename     like  (NetSNMP:VarbindlistType.name)
MIBpos          long
result          byte
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._ProcessGetNext', 'Called')
    end
  ****

  !Get the community profile for this guy
  self.qCommunityProfiles.communityName = self.community
  get(self.qCommunityProfiles, self.qCommunityProfiles.communityname)
  if errorcode ()    !Probably No such community
    return
  end

  !work with a copy of self.varbinslist in case an error occurs
  loop gnvarbindpos = 1 to records(self.qVarBindList)
    get(self.qVarBindList, gnvarbindpos)
    gntempvarbinds.name = self.qVarBindList.name
    add(gntempvarbinds)
  end

  !Now get all the 'next' values in the varbindlist
  loop gnvarbindpos = 1 to records(gntempvarbinds)
    get(gntempvarbinds, gnvarbindpos)
    if errorcode () = 30
      !General error: Error = GenErr
      ErrorValue = 5
      do SendErrorResponse
      return
    end
    varbindName = gntempvarbinds.name
    Do FindReturnName

    gntempvarbinds.name         = self.qMIBEntries.name
    gntempvarbinds.valuetype    = self.qMIBEntries.valuetype
    gntempvarbinds.value        = self.qMIBEntries.value
    gntempvarbinds.length       = self.qMIBEntries.length
    gntempvarbinds.BSbits       = self.qMIBEntries.BSbits
    put(gntempvarbinds)
    if errorcode ()
      !General Error: Error = GenErr
      ErrorValue = 5
      do SendErrorResponse
      return
    end
  end

  !Try to send the response pdu
  self.ErrorStatus    = 0
  self.ErrorIndex     = 0

  loop gnvarbindpos = 1 to records(self.qVarBindList)
    get(self.qVarBindList, gnvarbindpos)
    get(gntempvarbinds, gnvarbindpos)
    self.qVarBindList.name      = gntempvarbinds.name
    self.qVarBindList.value     = gntempvarbinds.value
    self.qVarBindList.valuetype = gntempvarbinds.valuetype
    self.qVarBindList.length    = gntempvarbinds.length
    self.qVarBindList.BSbits    = gntempvarbinds.BSbits
    put(self.qVarBindList)
  end

  self.packet.toIP      = self.packet.fromIP
  self._SendResponse()



! ---------------------------------------------------------------------------------
! Sends a response packet indicating that something went wrong during the getnext
SendErrorResponse routine
  self.ErrorStatus    = errorValue
  self.ErrorIndex     = gnvarbindpos
  self.packet.toIP    = self.packet.fromIP
  self._SendResponse()


! ---------------------------------------------------------------------------------
! Finds the objectname in the MIB lexicographically succeding the objectid in varbindName
FindReturnName routine
  varbindname = clip(varbindname) & '.'  !Add a dot to the back of varbindname to make life easy
  mibpos = 1
  get(self.qMIBEntries, mibpos)
  if errorcode () = 30
    !No such name
    ErrorValue = 2
    do SendErrorResponse
    return
  elsif errorcode()
    !General error: Error = GenErr
    ErrorValue = 5
    do SendErrorResponse
    return
  end

  subid = varbindname[1]
  varbindname = varbindname[3 : len(varbindname)]
  compareName = clip(comparename) & subid & '.'

  loop
    mibsubid = 0
    dotpos = instring('.', varbindname, 1, 1)

    if dotpos = 0
      !We have reached the end of the original id
      !So either this the next MIBEntry is the right one or the current one is
      if self.qMIBEntries.name = gntempvarbinds.name
        mibpos = mibpos + 1
        get(self.qMIBEntries, mibpos)
      end
      result = self._inview (self.qMIBEntries.name, self.qCommunityProfiles.communityname)
      if result = 3
        ! General error: Error = GenErr
        errorValue = 5
        do sendErrorResponse
        return
      elsif result = false or self.qMIBEntries.access = NetSNMP:WriteOnly or self.qMIBEntries.access = NetSNMP:NotAccessible
        !Just keep going until we find an entry in view
      else
        !The entry is in view so we can return it
        exit
      end
    end

    subid = varbindname[1 : dotpos - 1]
    varbindname = varbindname[dotpos + 1 : len(varbindname)]

    !Try to find a MIB entry who's first part matches comparename
    loop
      MIBName  = self.qMIBEntries.name
      mibsubid = MIBName [len(clip(comparename)) + 1 : instring('.', MIBName, 1, len(clip(comparename)) + 1)]
      MIBName  = MIBName [len(clip(comparename)) + 1 : len(MIBName)]
      if self.qMIBEntries.name = gntempvarbinds.name [1 : len(clip(self.qMIBEntries.name))]
        !MIBEntry matches the first part of our name, so just continue (mibsubid is 0)
      elsif mibsubid = subid
        !Found an exact match for the first part of the objectid
        break
      end
      if instring(clip(comparename), self.qMIBENtries.name, 1, 1) <> 1 or mibsubid > subid
        !We have gone as far as we could; this is the correct entry to return providing it is in view
        result = self._inview (self.qMIBEntries.name, self.qCommunityProfiles.communityname)
        if result = 3
          !General error: Error = GenErr
          errorValue = 5
          do sendErrorResponse
          return
        elsif result = false
          !Just keep going until we find an entry in view
        else
          !The entry is in view so we can return it
          exit
        end
      end
      mibpos  += 1
      if mibpos > records (self.qMIBentries)
        !There is no variable in the MIB lexicographically following given one: Error = NoSuchName
        errorValue = 2
        do SendErrorResponse
        return
      end
      get(self.qMIBEntries, mibpos)
      if errorcode () = 30
        !No such name
        ErrorValue = 2
        do SendErrorResponse
        return
      elsif errorcode()
        !General error: Error = GenErr
        ErrorValue = 5
        do SendErrorResponse
        return
      end
    end
    compareName = clip(comparename) & subid & '.'
  end


!------------------------------------------------------------------------------
NetSNMP.TakeEvent    Procedure
  code
  parent.TakeEvent()
  if not self._TrapListener &= NULL                                     !bk - only call trap if enabled
    self._TrapListener.TakeEvent()
  end
!------------------------------------------------------------------------------
NetSNMP._InView      Procedure  (string p_Objectid,string p_ComName)
viewspos        long
viewslong       long
objidlong       long
dotpos          long
viewsid         string(256)
objid           string(256)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP._Inview', 'Called')
    end
  ****

  ! Checks whether the current varbind variable is in the community profile view
  loop viewspos = 1 to records(self.qMIBViews)
    get(self.qMIBViews, viewspos)
    if errorcode()
      return 3
    end
    if self.qMIBViews.CommunityName <> p_ComName
      cycle
    end
    if clip(self.qMIBViews.Subtree) = p_ObjectID[1 : len(clip(self.qMIBViews.Subtree))]
      return true
    end
  end
  return false

!------------------------------------------------------------------------------
NetSNMP.CheckAuthenticity Procedure  ()
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.CheckAuthenticity', 'Called')
    end
  ****

  return true
!------------------------------------------------------------------------------
NetSNMP.Open         Procedure  (string Server,uShort Port=0)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.Open', 'Called')
    end
  ****
  parent.open('', self.OurPort)
  if not self._TrapListener &= NULL                                         !bk - only open if trap listener is not disabled
    self._traplistener.open('', self.OurTrapPort)
  end
!------------------------------------------------------------------------------
NetSNMP.Close        Procedure
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSNMP.Close', 'Called')
    end
  ****

  parent.close()
  if not self._TrapListener &= NULL                                             !bk - only open if trap listener is not disabled
    self._traplistener.close()
  end
!------------------------------------------------------------------------------
NetSNMP.Open         Procedure
  code
  ! Wrapper to open normal Open method
  self.Open ('', 0)
!------------------------------------------------------------------------------
NetSNMP.DescriptionMapAdd Procedure  (string p_Name, string p_Description)
! You might call this like:
! self.DescriptionAdd ('1.3.6.1.2.1.1.3.0', 'The time (in hundredths of a second) since the network management portion of the system was last re-initialized.')

  code
  self.qDescriptionMap.Name = p_Name
  self.qDescriptionMap.DescriptionIn = p_Description
  add(self.qDescriptionMap)

!------------------------------------------------------------------------------
NetTrapListener.Process Procedure
  code
  if self.LoggingOn
    self.Log ('NetTrapListener.Process', 'Called')
  end
  self.NetParent.packet = self.packet
  self.NetParent.Process()
!------------------------------------------------------------------------------
NetFTPClientControl.Close Procedure
  code
  PARENT.Close()

  self.LoggedIn = 0
  self.Busy = 0

!------------------------------------------------------------------------------
NetSimple.Send       Procedure  (StringTheory p_str)
l  long
x  long
  code
  l = p_str.Length()
  loop x = 1 to l by net:MaxBinData
    self.packet.bindatalen = choose(x + net:MaxBinData - 1 < l,net:MaxBinData,l-x+1)
    self.packet.bindata = p_str.slice(x , x + self.packet.bindatalen - 1)
    self.send()
  end

!------------------------------------------------------------------------------
NetSimple.Send       Procedure  ()
  code
  if self._initalised = 0
    self.Log ('NetSimple.Send', 'Error - Open being called before Init.', NET:LogError)
    self.Error = ERROR:ObjectInitNotCalled
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('The NetSimple object''s Init() method was not called before the Open() method was called', 'NetSimple.Send')
    return
  end

    if self.LoggingOn
      if self._connection.mode = NET:SimpleClient
        self.Log ('NetSimple.Send (Client Mode)', 'Sending Data to' & |
                                    ' HostName = ' & clip (self._Connection.Server) & |
                                    ' OnSocket = ' & self._Connection.Socket & |
                                    ' SockID = ' & self._Connection.SockID & |
                                    ' Port = ' & self._Connection.Port & |
                                    ' BinDataLen = ' & self.packet.binDataLen)
        if self.LogDatabytes > 0
          if self.packet.binDataLen > 0
            if self.packet.binDataLen > self.LogDatabytes
              self.Log ('NetSimple.Send (Client Mode)', 'First ' & self.LogDatabytes & ' bytes of Data = [' & |
                                        self.packet.binData[1:self.LogDatabytes] & ']...')
            else
              self.Log ('NetSimple.Send (Client Mode)', 'First ' & self.packet.binDataLen & ' bytes of Data = [' & |
                                        self.packet.binData[1:self.packet.binDataLen] & ']')
            end
          end
        end
      else
        if self._Connection.UDPmode = 0
          self.Log ('NetSimple.Send (Server Mode)', 'Sending Data to' & |
                                      ' ToIP = ' & clip (self.packet.toIP) & |
                                      ' OnSocket = ' & self.packet.OnSocket & |
                                      ' SockID = ' & self.packet.SockID & |
                                      ' Port = ' & self._Connection.Port & |
                                      ' BinDataLen = ' & self.packet.binDataLen)
          if self.LogDatabytes > 0
            if self.packet.binDataLen > 0
              if self.packet.binDataLen > self.LogDatabytes
                self.Log ('NetSimple.Send (Server Mode)', 'First ' & self.LogDatabytes & ' bytes of Data = [' & |
                                          self.packet.binData[1:self.LogDatabytes] & ']...')
              else
                self.Log ('NetSimple.Send (Server Mode)', 'First ' & self.packet.binDataLen & ' bytes of Data = [' & |
                                          self.packet.binData[1:self.packet.binDataLen] & ']')
              end
            end
          end
        else
          self.Log ('NetSimple.Send (UDP Mode)', 'Sending Data to' & |
                                      ' ToIP = ' & clip (self.packet.toIP) & |
                                      ' UDPToPort = ' & self.packet.UDPToPort & |
                                      ' BinDataLen = ' & self.packet.binDataLen)
          if self.LogDatabytes > 0
            if self.packet.binDataLen > 0
              if self.packet.binDataLen > self.LogDatabytes
                self.Log ('NetSimple.Send (UDP Mode)', 'First ' & self.LogDatabytes & ' bytes of Data = ' & |
                                          self.packet.binData[1:self.LogDatabytes] & ']...')
              else
                self.Log ('NetSimple.Send (UDP Mode)', 'First ' & self.packet.binDataLen & ' bytes of Data = [' & |
                                          self.packet.binData[1:self.packet.binDataLen] & ']')
              end
            end
          end
        end
      end
    end

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if self.packet.binDataLen > NET:MaxBinData
    self.error = ERROR:InvalidStructureSize
    self._CallErrortrap('Trying to send too much data in self.packet.binData and self.packet.binDataLen', 'NetSimple.Send')
    return
  end

  if (self.openFlag = false) and (self._Connection.UDPmode = 0)
    self.error = ERROR:ConnectionNotOpen
    self._CallErrortrap('The connection is not open and therefore the data can not be sent.', 'NetSimple.Send')
    return
  end

  if self.packet.binDataLen <= 0
    self.error = ERROR:InvalidStructureSize
    self._CallErrortrap('Data length is less than or equal to zero', 'NetSimple.Send')
    return
  end


  if self._connection.mode = NET:SimpleClient
    if clip (self._Connection.Server) = '' or self._Connection.Port = 0
      self.error = ERROR:NoIPorSocketSpecified
      self._CallErrortrap('Hostname or Port are not set to anything. (You must call self.open first).', 'NetSimple.Send')
      return
    end
  else
    if self._Connection.UDPmode = 0
      if clip (self.packet.toIP) = '' or self.packet.OnSocket = 0
        self.error = ERROR:NoIPorSocketSpecified
        self._CallErrortrap('Hostname or Socket are not set to anything. (You must set self.packet.ToIp and OnSocket)', 'NetSimple.Send')
        return
      end
    else
      if clip (self.packet.toIP) = '' or self.packet.UDPToPort = 0
        self.error = ERROR:NoIPorSocketSpecified
        self._CallErrortrap('Hostname or Port are not set to anything. (You must set self.packet.ToIp and UDPToPort)', 'NetSimple.Send')
        return
      end
    end
  end

  Self.Error = NetSimpleSend(self._connection, self.packet)
  if self.error <> 0
    if (self._connection.mode = NET:SimpleClient) and (self.error = ERROR:ClientNotConnected)
      self.openFlag = 0                               ! Mark the connection as closed
    end

    if self.error = ERROR:ClientNotConnected and self.DontErrorTrapInSendIfConnectionClosed
      ! Do nothing - the programmer must handle this one.
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.Send', 'While trying to send data, we discover the connection has been closed down. The programmer has chosen to handle the error himself.')
        end
      ****
    else  ! -34 error and programmer has not chosen to handle the error in self.send()
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.Send', 'Error. Note to the programmer - look at the docs for info on the self.DontErrorTrapInSendIfConnectionClosed in the NetSimpleConnection property section.')
        end
      ****
      self._CallErrortrap('Error sending data', 'NetSimple.Send')
    end
  end

!------------------------------------------------------------------------------
NetSimple.Open       Procedure  (string Server,uShort Port=0)
  code
  self.log('NetSimple.Open','self._connection.mode=' & self._connection.mode & ' server=' & clip(server) & ' port=' & port)
  if self._initalised = 0
    self.Log ('NetSimple.Open', 'Error - Open being called before Init.', NET:LogError)
    self.Error = ERROR:ObjectInitNotCalled
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('The NetSimple object''s Init() method was not called before the Open() method was called', 'NetSimple.Open')
    return
  end

  if (self.OpenFlag or self.AsyncOpenBusy)
    if self._connection.mode = NET:SimpleClient
      self.error = ERROR:ALREADYOPEN
      self.WinSockError = 0
      self.SSLError = 0
        if self.LoggingOn
          self.Log ('NetSimple(Client).Open', 'Connection already open. Please close or abort the connection before trying to open a new connection.')
        end
      self.ErrorTrap ('The client connection could not be opened because the connection was already open', 'NetSimple.Open')
      return
    else
      self.error = ERROR:ALREADYOPEN
      self.WinSockError = 0
      self.SSLError = 0
        if self.LoggingOn
          self.Log ('NetSimple(Server).Open', 'This Server is already listening on a Port. Please close or abort the connection before trying to open a new connection.')
        end
      self.ErrorTrap ('The Server is already listening on a Port.', 'NetSimple.Open')
      return
    end
  end

  if self.LoggingOn
    if self._connection.mode = NET:SimpleClient
      self.Log ('NetSimple(Client).Open', 'Attempting to connect to ' & clip (server) & ' on Port ' & port)
    else
      if Server <> ''
        if self._connection.UDPmode = 0
          self.Log ('NetSimple(Server).Open', 'Attempting Listen on TCP Port ' & port & ' on only IP = ' & clip (server))
        else
          self.Log ('NetSimple(Server).Open', 'Attempting Listen on UDP Port ' & port & ' on only IP = ' & clip (server))
        end
      else
        if self._connection.UDPmode = 0
          self.Log ('NetSimple(Server).Open', 'Attempting Listen on TCP Port ' & port)
        else
          self.Log ('NetSimple(Server).Open', 'Attempting Listen on UDP Port ' & port)
        end
      end
    end
  end

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0
  self.AsyncOpenBusy = 0

  if self._BetweenPhaseOneAnTwo
    self._IgnorePhaseTwo = true
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.Open', 'Setting IgnorePhaseTwo = true. We weren''t able to call PhaseTwo Close Down before this object is being used to open another connection. That''s fine the NetTalk DLL will clean up our old connection for us.')
      end
    ****
  end

  self._connection.Server = clip(Server)
  self._connection.Port = Port

  if self.SSL = 1
    self.SSLCertificateOptions.RemoteCertificateHaveDetails = 0
    self.SSLCertificateOptions.RemoteCertificateVersion = 0
    self.SSLCertificateOptions.RemoteCertificateSubject = ''
    self.SSLCertificateOptions.RemoteCertificateIssuer = ''
    self.SSLCertificateOptions.RemoteCertificateNotBeforeDate = 0
    self.SSLCertificateOptions.RemoteCertificateNotBeforeTime = 0
    self.SSLCertificateOptions.RemoteCertificateNotAfterDate = 0
    self.SSLCertificateOptions.RemoteCertificateNotAfterTime = 0
    self.SSLCertificateOptions.RemoteCertificatePublicKey = ''
    self.SSLCertificateOptions.RemoteCertificateAlgorithm = 0
    self.SSLCertificateOptions.RemoteCertificateSerialNumber = 0
  end

  self._connection.SSL = self.SSL
  self._connection.SSL_Method = self.SSLMethod
  self._connection.SSL_DontConnectOnOpen = self._SSLDontConnectOnOpen

  if self.AsyncOpenUse
    self._connection.AsyncOpenTimeOut = self.AsyncOpenTimeOut
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.Open', 'Will use Asynchronous Open. Timeout (hs) = ' & self._connection.AsyncOpenTimeOut)
      end
    ****
  else
    self._connection.AsyncOpenTimeOut = 0
  end

  if self.InActiveTimeout
    self._connection.InActiveTimeout = self.InActiveTimeout
    if self._connection.mode = NET:SimpleClient
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple(Client).Open', 'Will use InActive (Idle) Timeout (hs) = ' & self._connection.InActiveTimeout)
        end
      ****
    else
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple(Server).Open', 'Will use InActive (Idle) Timeout (hs) = ' & self._connection.InActiveTimeout)
        end
      ****
    end
  else
    self._connection.InActiveTimeout = 0
  end

  self.Error = NetSimpleOpen(self._connection)
  self.WinSockError = 0
  self.SSLError = 0

  if self.error = 0
    if self._connection.mode = NET:SimpleClient
      if self._connection.AsyncOpenTimeOut > 0
        self.openFlag = 0
        self.AsyncOpenBusy = 1
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetSimple(Client).Open', 'Opening Connection Asynchrously (will wait for Process() or Err0rTrap() to be called).' & |
                      ' on Socket = ' & self._connection.Socket & |
                      ' SockID = ' & self._connection.SockID)
          end
        ****
      else
        self.openFlag = 1
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetSimple(Client).Open', 'Connection opened' & |
                      ' on Socket = ' & self._connection.Socket & |
                      ' SockID = ' & self._connection.SockID)
          end
        ****
      end
    else
      self.openFlag = 1
      if self._connection.UDPmode = 0
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetSimple(Server).Open', 'Successfully listening on TCP Port = ' & Port & |
                      ' On socket = ' & self._connection.Socket & |
                      ' SockID = ' & self._connection.SockID)
          end
        ****
      else
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetSimple(Server).Open', 'Successfully listening on UDP Port = ' & Port & |
                      ' On socket = ' & self._connection.Socket & |
                      ' SockID = ' & self._connection.SockID)
          end
        ****
      end
      ! Keep the qServerConnection queue updated
      if self._connection.mode = NET:SimpleServer
        self.RefreshQServerConnections(1) ! 1 = Free only
      end
      self.ServerConnectionsCount = 0
    end
  else
    if self._connection.Mode = NET:SimpleClient
      if self.error = ERROR:CouldNotLoadProcedures and self._connection.SSL
        self._CallErrorTrap('Could not load the SSL (Secure Sockets Layer) DLL libraries. libssl32.dll and libeay32.dll could not be found.', 'NetSimple(Client).Open')
      else
        self._CallErrorTrap('Unable to Open Connection', 'NetSimple(Client).Open')
      end
    else
      if self._Connection.UDPMode = 0
        self._CallErrorTrap('Unable to Listen on TCP Port', 'NetSimple(Server).Open')
      else
        self._CallErrorTrap('Unable to Listen on UDP Port', 'NetSimple(Server).Open')
      end
    end
  end


!------------------------------------------------------------------------------
NetSimple.Close      Procedure  ()
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      if self._connection.mode = NET:SimpleClient
        self.Log ('NetSimple.Close', 'Closing Client Socket = ' & self._connection.Socket & |
                                     ' SockID = ' & self._connection.SockID & |
                                     ' to Server = ' & clip (self._connection.Server) & |
                                     ' Originally Connected to Port = ' & self._connection.Port)
      else
        self.Log ('NetSimple.Close', 'Closing Listening Server Socket = ' & self._connection.Socket & |
                                     ' SockID = ' & self._connection.SockID & |
                                     ' on local Server = ' & |
                                     ' Listening on Port = ' & self._connection.Port)
      end
    end
  ****

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if self.openFlag <> 0
    self.WholePacketFree(self._Connection.SockID)
    self.error = NetSimpleClose(self._connection, 0, 0, 1) ! First Phase Close
    self._BetweenPhaseOneAnTwo = true
    self._IgnorePhaseTwo = false
    if self.error = 0
      self.openFlag = 0
      ! Keep the qServerConnection queue updated
      if self._connection.mode = NET:SimpleServer
        self.RefreshQServerConnections(1) ! 1 = Free only
      end
      self.ServerConnectionsCount = 0
    else
      self.openFlag = 0        ! Hope that because there was an error that the connection has been closed. Jono 17/8/2000
      self._CallErrorTrap('Unable to Close connection', 'NetSimple.Close')
    end
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.Close', 'Warning. Socket not open. Can not close')
      end
    ****
  end

!------------------------------------------------------------------------------
NetSimple.Process    Procedure  ()
  code
!---------------------------------------------------------------------------------
! This is a virtual method handled by derivative classes.
! The packet will contain the data of the received packet.
! To send a reply you will need to call your self.send() method
!---------------------------------------------------------------------------------
!------------------------------------------------------------------------------
NetSimple.Prime      Procedure  ()
  code
  self._Wait()
  assert (~self._initalised)

  if self._initalised = 0
    self._connection.NotifyEvent = NetGetUniqueEvent()
    if self._connection.NotifyEvent = 0
      compile ('****', NETTALKLOG=1)
        self.Log ('NetSimple.Prime', 'Error getting new unique event. None was available.', NET:LogError)
      ****
    end
    self._initalised = 1
  end
  self._Release()

  ! this is a simple method which sets the base message for this object.
  ! self._connection.Event is set so that each object get's it's own events.
  ! Each instance of each object is currently assigned 5 events.
!------------------------------------------------------------------------------
NetSimple.Init       Procedure  (ulong Mode=NET:SimpleClient)
  compile ('****', NETTALKLOG=1)
temp1        String (10)
  ****


  code
  if self._inited = 0            !GT added this 9 /5/ 2008 - bail out if it's already inited.
    self._inited = 1
  ! Allocate Queue structures
    self._qServerConnections &= new (Net:SimpleServerConnectionsQType)
    self.qServerConnections &= self._qServerConnections

    self._WholePacketQueue &= new (Net:SimpleWholePacketQType)
    self.WholePacketQueue &= self._WholePacketQueue

    ! ----------------------- Logging options ------------------------------------------
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        if mode = NET:SimpleClient
          temp1 = 'Client'
        else
          temp1 = 'Server'
        end
        self.Log ('NetSimple.Init', 'Initialising a new ' & clip (temp1))
      end
    ****
    ! ----------------------- End Logging options --------------------------------------


    ! -------------- Initialization --------------------------
    self._connection.Mode = Mode

    self.SSLCertificateOptions._Size = size (self.SSLCertificateOptions)
    self._Connection._SSL_CertificateOptionsPointer = address(self.SSLCertificateOptions)

    self.SSLCertificateOptions.CARootFile = '.\CARoot.pem'  ! Default location for CARoot.pem you can change this though (after the call to Init).

    if self.UseThisThread <> 0                         ! New 6 Feb 2002 (Jono)
      self._connection.NotifyThread = self.UseThisThread
    else
      self._connection.NotifyThread = Thread()
    end
    self.SSLMethod = NET:SSLMethodSSLv3 ! new default for 5.31, was NET:SSLMethodSSLv23

    self.AsyncOpenUse = 1 ! Make all NetSimple objects use Asynchronous Open by default. 12/3/2004
    ! By Default Open is not Asynchronous.
    self.AsyncOpenTimeOut = 1200 ! 12 (was 9 until 26/1/2004) seconds  (To use Async Open set .AsyncOpenUse = 1)

    self._WholePacketAllocSize = 524288 ! 512 * 1024 (half meg)

    self.prime()
  end
!------------------------------------------------------------------------------
NetSimple.TakeEvent  Procedure  ()
Result              long,auto
MyErrorString       string(160),auto
MyError             long,auto
MySSLError          long,auto
MyWinSockError      long,auto
HasClosed           long,auto
MyCertName          string(256)
IsCertMatch         long
lx                  long

  code
  !--------------------------------
  if event() = self._connection.NotifyEvent                        ! There is a packet to receive from the DLL.
  !--------------------------------
    compile ('^^%^^', _NETPOSTS=1)
      Net:DebugCount += 1
      NetDebugTrace ('NetSimple Count = ' & Net:DebugCount)
    ^^%^^

    Result = NetSimpleReceive(self._connection, self.packet)  ! Receive the packet
    !Stop (self.packet.Error & ' ' & self.packet.WinSockError & ' ' & self.packet.SSLError)
    !self.Error = loc:Result ! commented out 5/10/2005
    self.Error = self.packet.NetError
    self.WinSockError = self.packet.WinSockError
    self.SSLError = self.packet.SSLError
    if self.Error = 0
      if self.SSLError <> 0                        ! SSL Errors have priority over WinSock Errors
        self.Error = ERROR:ErrorStoredInSSLError
      elsif self.WinSockError <> 0
        self.Error = ERROR:ErrorStoredInWinSockError
      end
    end

    if Result <> 0
      if Result = -13
        self.Log('NetSimple.TakeEvent', 'Attention. This is not normally a problem. The DLL could not find a packet matching this connection.')
      else
        self._CallErrorTrap('An error occurred while trying receive data','NetSimple.TakeEvent')
      end
      return
    end
    if self._connection.mode = NET:SimpleClient
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent', '(Client Mode) Received a message.' & |
                    ' PacketType = ' & self.packet.PacketType & |
                    ' _DebugPacketNumber = ' & self.packet._DebugPacketNumber & |
                    ' Mode = ' & self._connection.mode)
        end
      ****
    else
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent', '(Server Mode) Received a message.' & |
                    ' PacketType = ' & self.packet.PacketType & |
                    ' _DebugPacketNumber = ' & self.packet._DebugPacketNumber & |
                    ' Mode = ' & self._connection.mode)
        end
      ****
    end

    case self.packet.PacketType
    !==========================================================
    of NET:SimpleSocketClosedPacket1    ! Server closed Remotely (FD_CLOSE)
    !==========================================================
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent', 'PacketType -> Calls ConClosed(1) (Remote)')
        end
      ****
      Do ConClosed1
    !==========================================================
    of NET:SimpleSocketClosedPacket2  ! Server closed locally by Close() method
    !==========================================================
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent', 'PacketType -> Calls ConClosed(2)')
        end
      ****
      Do ConClosed2

    !==========================================================
    of NET:SimpleAsyncOpenSuccessful
    !==========================================================
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent', 'PacketType -> "AsyncOpenSuccessful" to ' & |
                     clip (self._Connection.Server) & ':' & self._Connection.Port & |
                    ' On Socket = ' & self.packet.OnSocket & |
                    ' SockID = ' & self.packet.SockID & |
                    ' Local_IP = ' & self._Connection.LocalIP)
        end
      ****
      self.openflag = 1
      self.AsyncOpenBusy = 0
      if self.SSL = 1
        HasClosed = 0
        self._SSLGetRemoteCertificate(0, 0, HasClosed) ! Client Connection Worked
        if self.SSLCertificateOptions.DontVerifyRemoteCertificateCommonName = 0 and self._connection.mode = NET:SimpleClient
          if HasClosed = 0 ! Don't do this bit if the connection has closed
            MyCertName = lower(clip(self.SSLGetSubjectField('CN')))
            IsCertMatch = False
            if instring('*', MyCertName, 1, 1) = 1   !this is a wildcard certificate
              MyCertName = MyCertName[ 2 : len(clip(MyCertName)) ]
              lx = len(clip(MyCertName))
              if lower(MyCertName) = lower(sub(self._Connection.Server,len(clip(self._Connection.Server)) - lx + 1, lx))
                IsCertMatch = true
              end
            else
              if lower(clip(self._Connection.Server))  = clip(MyCertName)
                IsCertMatch = true
              end
            end
            if IsCertMatch = False
            !if lower(clip(self._Connection.Server)) <> lower(clip(self.SSLGetSubjectField('CN')))
              compile ('****', NETTALKLOG=1)
                self.Log ('NetSimple.TakeEvent', 'SSL. Warning the Common Name (' &lower(clip(self.SSLGetSubjectField('CN'))) & |
                          ') does not match the Server name (' & lower(clip(self._Connection.Server)) & ')', Net:LogError)
              ****
              MyError = ERROR:SSLInvalidCertificateCommonName
              MySSLError = 0
              MyWinSockError = 0
              MyErrorString = 'The Remote SSL Certificate''s Common Name (' &lower(clip(self.SSLGetSubjectField('CN'))) & ') does not match the Server name (' & lower(clip(self._Connection.Server)) & '). And so the connection failed to open'
              Do KillSSLConnection
              return
            end
          else
            compile ('****', NETTALKLOG=1)
              self.Log ('NetSimple.TakeEvent', 'SSL. Won''t compare the Common Name and the Server. The NetTalk DLL knows that this SSL connection is closing down, and therefore the Certificate information is not available. We won''t worry about this for now')
            ****
          end
        end
        !if self.SSLCertificateOptions.DontVerifyRemoteCertificateDates = 0
        !  ! Bear in mind the dates comparisons are not taking GMT to local time into account
        !  loc:MyToday = today()
        !  loc:MyClock = clock()
        !  if (loc:MyToday < self.SSLCertificateOptions.RemoteCertificateNotBeforeDate) or |
        !     (loc:MyToday = self.SSLCertificateOptions.RemoteCertificateNotBeforeDate and loc:MyClock <= self.SSLCertificateOptions.RemoteCertificateNotBeforeTime)
        !    compile ('****', NETTALKLOG=1)
        !      self.Log ('NetSimple.TakeEvent', 'SSL. Warning the Not Before Date (' & format(self.SSLCertificateOptions.RemoteCertificateNotBeforeDate, @D17) & ' ' & format(self.SSLCertificateOptions.RemoteCertificateNotBeforeTime, @T4) & ') Verification failed.', Net:LogError)
        !    ****
        !    loc:MyError = ERROR:SSLInvalidCertificateDates
        !    loc:MyErrorString = 'The Remote SSL Certificate is being used before the Not Before Date (' & format(self.SSLCertificateOptions.RemoteCertificateNotBeforeDate, @D17) & ' ' & format(self.SSLCertificateOptions.RemoteCertificateNotBeforeTime, @T4) & '). And so the connection failed to open'
        !    Do KillSSLConnection
        !    return
        !  end
        !  if (loc:MyToday > self.SSLCertificateOptions.RemoteCertificateNotAfterDate) or |
        !     (loc:MyToday = self.SSLCertificateOptions.RemoteCertificateNotAfterDate and loc:MyClock >= self.SSLCertificateOptions.RemoteCertificateNotAfterTime)
        !    compile ('****', NETTALKLOG=1)
        !      self.Log ('NetSimple.TakeEvent', 'SSL. Warning the Not After Date (' & format(self.SSLCertificateOptions.RemoteCertificateNotAfterDate, @D17) & ' ' & format(self.SSLCertificateOptions.RemoteCertificateNotAfterTime, @T4) & ') Verification failed.', Net:LogError)
        !    ****
        !    loc:MyError = ERROR:SSLInvalidCertificateDates
        !    loc:MyErrorString = 'The Remote SSL Certificate has expired (' & format(self.SSLCertificateOptions.RemoteCertificateNotBeforeDate, @D17) & ' ' & format(self.SSLCertificateOptions.RemoteCertificateNotBeforeTime, @T4) & '). And so the connection failed to open'
        !    Do KillSSLConnection
        !    return
        !  end
        !end
      end
      self.Process()

    !==========================================================
    of NET:SimpleAsyncOpenFailed
    !==========================================================
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent', 'Error. PacketType -> "AsyncOpenFailed" to ' & |
                    clip (self._Connection.Server) & ':' & self._Connection.Port & |
                    ' On Socket = ' & self.packet.OnSocket & |
                    ' SockID = ' & self.packet.SockID)
        end
      ****
      ! new 5/10/2005
      case self.Error
      of ERROR:ErrorStoredInSSLError to ERROR:ErrorStoredInWinSockError ! Reverse order (negetive numbers)
      orof 0
        self.error = ERROR:OpenTimeOut
      end
      ! end 5/10/2005
      self.openflag = 0
      self.AsyncOpenBusy = 0
      NetSimpleClose(self._connection, self.packet.OnSocket, self.packet.SockID, 2)         ! Second Phase close - close all
      self._CallErrorTrap ('The requested connection could not be opened. The Open command timed out or failed to connect', 'NetSimple.TakeEvent')

    !==========================================================
    of NET:SimpleNoConnection
    !==========================================================
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent', 'PacketType -> No Connection!! Warning!')
        end
      ****

    !==========================================================
    of    NET:SimpleNewConnection
    orof  NET:SimplePartialDataPacket
    orof  NET:SimpleIdleConnection
    !==========================================================
      if self.packet.PacketType = NET:SimpleNewConnection
        ! Keep the qServerConnection queue updated
        if self._connection.mode = NET:SimpleServer
          ! Get list of connections to the server, if we are in server mode
          self.RefreshQServerConnections()
        end
        self.ServerConnectionsCount += 1
        if self.SSL = 1
          HasClosed = 0
          self._SSLGetRemoteCertificate(self.packet.OnSocket, self.packet.SockID, HasClosed) ! Server
        end
      end
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          if self.packet.PacketType = NET:SimpleNewConnection
            self.Log ('NetSimple.TakeEvent', 'PacketType -> New Connection' & |
                      ' From IP = ' & self.packet.FromIP & |
                      ' Local Port = ' & self._Connection.Port & |
                      ' On Socket = ' & self.packet.OnSocket & |
                      ' SockID = ' & self.packet.SockID)

          elsif self.packet.PacketType = NET:SimpleIdleConnection
            self.Log ('NetSimple.TakeEvent', 'Received a Idle Connection packet : Connection has been Inactive.' & |
                      ' On Socket = ' & self.packet.OnSocket & |
                      ' SockID = ' & self.packet.SockID)
          else
            self.Log ('NetSimple.TakeEvent', 'PacketType -> Normal Data Packet Received ' & |
                      ' Data Length = ' & self.packet.binDataLen & |
                      ' On Socket = ' & self.packet.OnSocket & |
                      ' SockID = ' & self.packet.SockID)
            if self.LogDatabytes > 0
              if self.packet.binDataLen > 0
                if self.packet.binDataLen > self.LogDatabytes
                  self.Log ('NetSimple.TakeEvent', 'First ' & self.LogDatabytes & ' bytes of Received Data = [' & |
                                            self.packet.binData[1:self.LogDatabytes] & ']...')
                else
                  self.Log ('NetSimple.TakeEvent', 'First ' & self.packet.binDataLen & ' bytes of Received Data = [' & |
                                            self.packet.binData[1:self.packet.binDataLen] & ']')
                end
              end
            end

          end
        end
      ****

      self.process()                                         ! Call Virtual Process() method

      !------------------------------
      if self.WholePacketUseLengthField  ! New 8 Nov 2004 - code for Big Packets
        if self.packet.PacketType = NET:SimplePartialDataPacket
          self.WholePacketStorePartialPacket()
          self.WholePacketCheckForWhole()
        end
      end

    !==========================================================
    else
    !==========================================================
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent', 'Error. PacketType -> Unknown = ' & |
                     self.packet.PacketType)
        end
      ****
    end
  !--------------------------------
  elsif event() = self._connection.NotifyEvent + 1                 ! Client Connection closed remotely by FD_CLOSE ! Only used by NET:SimpleClient
  !--------------------------------
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.TakeEvent', 'Received a message+1 about a ConClose(1) (Remote).' & |
                  ' PacketType = ' & self.packet.PacketType & |
                  ' Mode = ' & self._connection.mode)
      end
    ****
    Do ConClosed1
  !--------------------------------
  elsif event() = self._connection.NotifyEvent + 2                 ! The socket has been closed with the .close() method. Request to clean up data queues in DLL
  !--------------------------------
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.TakeEvent', 'Received a message+2 about a Close (2).' & |
                  ' PacketType = ' & self.packet.PacketType & |
                  ' Mode = ' & self._connection.mode)
      end
    ****
    if self._IgnorePhaseTwo
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent.Message+2', 'Attention. 2nd Phase NOT Called! The Connection property has changed, since calling Phase One. Can''t call Phase Two, the DLL will clear up in 5 minutes.')
        end
      ****
      ! Don't do anything. The DLL will clear in 5 minutes.
    else
      NetSimpleClose(self._connection, 0, 0, 2)         ! Second Phase close - close all
    end
  end



! -------------------------------------------------------------------------------
KillSSLConnection Routine
  if self._connection.mode = NET:SimpleClient
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.TakeEvent', 'The client SSL connection verify failed' & |
                  ' on Socket = ' & self.packet.OnSocket & |
                  ' SockID = ' & self.packet.SockID & |
                  ' needs to be aborted. Will call self.Abort()')
      end
    ****
    self.Abort()
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.TakeEvent', 'A server SSL connection verify failed' & |
                  ' on Socket = ' & self.packet.OnSocket & |
                  ' SockID = ' & self.packet.SockID & |
                  ' needs to be aborted. Will call self.AbortServerConnection()')
      end
    ****
    self.AbortServerConnection(self.packet.OnSocket, self.packet.SockID)
  end

  self.Error = MyError
  self.WinSockError = MyWinSockError
  self.SSLError = MySSLError
  !self.openflag = 0
  !self.AsyncOpenBusy = 0
  !NetSimpleClose(self._connection, self.packet.OnSocket, self.packet.SockID, 2)         ! Second Phase close - close all
  self._CallErrorTrap (clip(MyErrorString), 'NetSimple.TakeEvent')

! -------------------------------------------------------------------------------
ConClosed1 Routine     ! Handles Remote Closing of sockets
  if self.SSL = 1 and self.Error = ERROR:ErrorStoredInSSLError and self.SSLError = NET:SSL_R_CERTIFICATE_VERIFY_FAILED
    !commented out 5/10/2005 self.Error = ERROR:SSLInvalidRemoteCertificate
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.TakeEvent.ConClosed1', 'The Remote SSL Certificate did not verify against the specified CA Root Certificate(s).' & |
                  ' on Socket = ' & self.packet.OnSocket & |
                  ' SockID = ' & self.packet.SockID)
      end
    ****
    if self._connection.mode = NET:SimpleClient and self.OpenFlag = 0 and self.AsyncOpenBusy = 1
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent.ConClosed1', 'Remote SSL Certificate Failed Verify - Will follow up with Async Open Failed')
        end
      ****
      self.openflag = 0
      self.AsyncOpenBusy = 0
      NetSimpleClose(self._connection, self.packet.OnSocket, self.packet.SockID, 2)         ! Second Phase close - close all
      self._CallErrorTrap ('The remote server secure certificate verification failed, and so the connection was not opened', 'NetSimple.TakeEvent')
    else
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent.ConClosed1', 'Remote SSL Certificate Failed Verify - Will follow up with Connection Closed (after failed Secure certificate verification)')
        end
      ****
      Do CallConnectionClosed
    end
  else
    Do CallConnectionClosed
  end


! ------------------------------------------------------------------------------------
CallConnectionClosed Routine
  if self._connection.mode = NET:SimpleClient
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.TakeEvent.ConClosed1', 'The client connection' & |
                  ' on Socket = ' & self.packet.OnSocket & |
                  ' SockID = ' & self.packet.SockID & |
                  ' has been closed remotely. Will call NetSimpleConnectClose and then the' & |
                  ' ConnectionClosed() method.')
      end
    ****
    self.WholePacketFree(self.packet.SockID)
    NetSimpleClose(self._connection, self.packet.OnSocket, self.packet.SockID, 2)         ! Second Phase close - close all
    self.openFlag = 0                                        ! Mark the connection as closed
    self.AsyncOpenBusy = 0
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.TakeEvent.ConClosed1', 'The Server connection' & |
                  ' on Socket = ' & self.packet.OnSocket & |
                  ' SockID = ' & self.packet.SockID & |
                  ' has been closed remotely. Will call NetSimpleConnectClose and then the' & |
                  ' ConnectionClosed() method.')
      end
    ****
    self.WholePacketFree(self.packet.SockID)
    NetSimpleClose(self._connection, self.packet.OnSocket, self.packet.SockID, 2) ! Second Phase close - close all

    ! Keep the qServerConnection queue updated
    ! Get list of connections to the server, if we are in server mode
    self.RefreshQServerConnections()

    self.ServerConnectionsCount += -1
    if self.ServerConnectionsCount < 0
      self.ServerConnectionsCount = 0
    end
  end
  self.ConnectionClosed()                                    ! Socket details are in the packet


! ------------------------------------------------------------------------------------
ConClosed2 Routine
  if self._IgnorePhaseTwo
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.TakeEvent.ConClosed2', 'Attention. 2nd Phase NOT Called! The Connection property has changed, since calling Phase One. Can''t call Phase Two, the DLL will clear up in 5 minutes.' & |
                  ' The server connection' & |
                  ' on Socket = ' & self.packet.OnSocket & |
                  ' SocketID = ' & self.packet.SockID & |
                  ' was closed locally.')
      end
    ****
    ! Don't do anything. The DLL will clear in 5 minutes.
  else
    if self._connection.mode = NET:SimpleClient
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent.ConClosed2', 'The client connection' & |
                    ' on Socket = ' & self.packet.OnSocket & |
                    ' SockID = ' & self.packet.SockID & |
                    ' was closed locally. Will call NetSimpleConnectClose for 2nd Phase')
        end
      ****
      NetSimpleClose(self._connection, self.packet.OnSocket, self.packet.SockID, 2)         ! Second Phase close - close all
      self.openFlag = 0                                        ! Mark the connection as closed
      self.AsyncOpenBusy = 0
    else
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple.TakeEvent.ConClosed2', 'The server connection' & |
                    ' on Socket = ' & self.packet.OnSocket & |
                    ' SocketID = ' & self.packet.SockID & |
                    ' was closed locally. Will call NetSimpleConnectClose for 2nd Phase')
        end
      ****
      NetSimpleClose(self._connection, self.packet.OnSocket, self.packet.SockID, 2) ! Second Phase close - close all
      self.RefreshQServerConnections()
    end
  end

!------------------------------------------------------------------------------
NetSimple.ErrorTrap  Procedure  (string errorStr,string functionName)
temp2    string (512)
  code
  temp2 = self.InterpretError()

  if self.error = 0
    self.ErrorString = clip(errorStr) & '.'
  else
    self.ErrorString = clip(errorStr) & '.' & ' The error number was ' & |
                       self.error & ' which means ' & clip(temp2) & '.'
  end
  case self.error
  of   ERROR:OpenTimeOut
  orof ERROR:SSLGeneralFailure to (ERROR:NetTalkObjectAndDLLDontMatch-1)
  orof 0
    ! Also list the WinSock or SSL Errors
    if self.WinSockError
      temp2 = clip(temp2) & ' - [WinSock Error = ' & self.WinSockError & ' : ' & clip (NetErrorStr (self.WinSockError)) & ']'
    end
    if self.SSLError
      temp2 = clip(temp2) & ' - [SSL Error = ' & self.SSLError & ']'
    end
  end

  if self.SuppressErrorMsg = 0
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.ErrorTrap', |
                'Error = ' & self.error & |
                ' = ' & clip(errorStr) & |
                ' : ' & clip (temp2) & |
                ' Function = ' & clip(functionName) & |
                ' Server = ' & clip (self._connection.Server) & |
                ' Port = ' & clip (self._connection.port), NET:LogError)
    ****
    if self.error = 0
      message('A network communication error has occurred.||Error = ' & clip(errorStr) & '.' & |
              '||Error occurred in function ' & clip(functionName), |
              clip(NET:ErrorTitle), Icon:Asterisk)
    else
      message('A network communication error has occurred.||Error = ' & clip(errorStr) & '.' & |
              '||The error number was ' & self.error & |
              ' which means ' & clip(temp2) & '.' & |
              '||Error occurred in function ' & clip(functionName), |
              clip(NET:ErrorTitle), Icon:Asterisk)
    end

  else                                       ! Only log to file
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.ErrorTrap', '<<no display> ' & |
                'Error = ' & self.error & |
                ' = ' & clip(errorStr) & |
                ' : ' & clip (temp2) & |
                ' Function = ' & clip(functionName) & |
                ' Server = ' & clip (self._connection.Server) & |
                ' Port = ' & clip (self._connection.port), NET:LogError)
    ****
  end

  ! this is a simple error handler. It means every error received interrupts your program
  ! (which you may want to override). The actual message is provided by the
  ! InterpretError method, which can also be overridden.

!------------------------------------------------------------------------------
NetSimple.Kill       Procedure  ()
x            long
  code
  if self.openFlag = 1
    self.Close()                                          ! Close connection(s) if open
  end
  if self.AsyncOpenBusy
    self.abort()
  end

  ! Moved below the Close, because some objects may try and use self.qServerConnections
  ! when doing a close and if this var is disposed before close() is called a GPF can occur.

  ! De-Allocate Queue structures
  NetFreeUniqueEvent(self._connection.NotifyEvent)
  self._connection.NotifyThread = 0

  if ~self._qServerConnections&=NULL
    self.RefreshQServerConnections(1) ! Free only
    dispose (self._qServerConnections)
  end


  if ~self._WholePacketQueue&=NULL
    loop x = 1 to records (self._WholePacketQueue)
      get (self._WholePacketQueue, x)
      if errorcode()
        break
      end
      if ~(self._WholePacketQueue.WholeBinData &= NULL)
        dispose (self._WholePacketQueue.WholeBinData)
      end
    end
    free (self._WholePacketQueue)
    dispose (self._WholePacketQueue)
  end

!------------------------------------------------------------------------------
NetSimple.ConnectionClosed Procedure  ()
  code
!---------------------------------------------------------------------------------
! This is a virtual method handled by derivative classes.
! The method is called when either a NetSimple Client or Server looses a connection
!---------------------------------------------------------------------------------
!------------------------------------------------------------------------------
NetSimple.CloseServerConnection Procedure  (ulong p_Socket,long p_SockID)
p_Socketlong   long, over (p_Socket)
loc:x          long

  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSimple.CloseServerConnection', 'Closing Socket = ' & p_Socketlong & |
                ' SockID = ' & p_SockID & |
                ' Originally Connected to Listening Port = ' & self._connection.port)
    end
  ****

  self.Error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if self.openFlag <> 0
    self.WholePacketFree(p_SockID)
    self.error = NetSimpleClose(self._connection, p_Socketlong, p_SockID, 1) ! First Phase Close
    if self.error <> 0
      self._CallErrorTrap('Unable to Close connection', 'NetSimple.CloseServerConnection')
    else
      ! Keep the qServerConnection queue updated
      if self._connection.mode = NET:SimpleServer
        ! Get list of connections to the server, if we are in server mode
        self.RefreshQServerConnections()
      end
      self.ServerConnectionsCount += -1
      if self.ServerConnectionsCount < 0
        self.ServerConnectionsCount = 0
      end
    end
  end


!------------------------------------------------------------------------------
NetSimple.Abort      Procedure  ()
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      if self._connection.mode = NET:SimpleClient
        self.Log ('NetSimple.Abort', 'Aborting (immediately closing) Client Socket = ' & self._connection.socket & |
                                     ' SockID = ' & self._connection.SockID & |
                                     ' to Server = ' & clip (self._connection.Server) & |
                                     ' Originally Connected to Port = ' & self._connection.Port)
      else
        self.Log ('NetSimple.Abort', 'Aborting (immediately closing) Listening Server Socket = ' & self._connection.socket & |
                                     ' SockID = ' & self._connection.SockID & |
                                     ' on local Server = ' & |
                                     ' Listening on Port = ' & self._connection.port)
      end
    end
  ****

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if (self.openFlag <> 0) or (self._connection.mode = NET:SimpleClient and self.AsyncOpenBusy)
    self.WholePacketFree(self._Connection.SockID)
    self.error = NetSimpleClose(self._connection, 0, 0, -1) ! Abortive Phase Close
    if self.error = 0
      self.openFlag = 0
      self.AsyncOpenBusy = 0
      ! Keep the qServerConnection queue updated
      if self._connection.mode = NET:SimpleServer
        ! Get list of connections to the server, if we are in server mode
        self.RefreshQServerConnections(1) ! 1 = free only
      end
      self.ServerConnectionsCount = 0
    else
      self.openFlag = 0        ! Hope that because there was an error that the connection has been closed. Jono 17/8/2000
      self.AsyncOpenBusy = 0   ! Hope that because there was an error that the connection has been closed.
      self._CallErrorTrap('Unable to Abort (Close) connection', 'NetSimple.Abort')
    end
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.Abort', 'Warning. Socket not open. Can not abort')
      end
    ****
  end


!------------------------------------------------------------------------------
NetSimple.GetInfo    Procedure  (long p_Options=1,long p_Socket=0,long p_SockId=0)
local           group, pre (loc)
x                 long
                end

loc2:SockLookup group
Socket            long ! 20/7/2005 - was ulong
SockID            long
                end

loc2:SockStr    string(size(loc2:SockLookup)), over (loc2:SockLookup)

  code
  if self._connection.mode = NET:SimpleServer
    ! Get list of connections to the server, if we are in server mode
    self.RefreshQServerConnections()
  end

  if p_Socket = 0 and p_SockID = 0
    loc2:SockLookup.Socket = self._Connection.Socket
    loc2:SockLookup.SockID = self._Connection.SockID
  else
    loc2:SockLookup.Socket = p_Socket
    loc2:SockLookup.SockID = p_SockID
  end

  if p_Options = 1
    ! Only counts number of packets waiting to be sent
    loc:x = NetOptions (NET:COUNTOUTGOINGPACKETS, loc2:SockStr)
    self.OutgoingDLLPackets = loc2:SockLookup.SockID
    self.OutgoingDLLPacketsbytes = 0
  elsif p_Options = 2
    ! Counts bytes waiting to be sent as well as number of packets
    loc:x = NetOptions (NET:COUNTOUTGOINGPACKETS2, loc2:SockStr)
    self.OutgoingDLLPackets = loc2:SockLookup.SockID
    self.OutgoingDLLPacketsbytes = loc2:SockLookup.Socket
  else
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.GetInfo', 'Illegal p_Options = ' & p_Options, NET:LogError)
    ****
  end


!------------------------------------------------------------------------------
NetSimple.AbortServerConnection Procedure  (ulong p_Socket,long p_SockID)
p_Socketlong   long, over (p_Socket)
loc:x          long
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSimple.AbortServerConnection', 'Aborting (Immediately Closing) Server Connection on Socket = ' & p_Socketlong & |
                ' SockID = ' & p_SockID & |
                ' Originally Connected to Listening Port = ' & self._connection.port)
    end
  ****

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if self.openFlag <> 0
    self.WholePacketFree(p_SockID)
    self.error = NetSimpleClose(self._connection, p_Socketlong, p_SockID, -1) ! -1 = Abort Phase
    if self.error <> 0
      self._CallErrorTrap('Unable to Abort connection', 'NetSimple.AbortServerConnection')
    else
      ! Keep the qServerConnection queue updated
      if self._connection.mode = NET:SimpleServer
        ! Get list of connections to the server, if we are in server mode
        self.RefreshQServerConnections()
      end
      self.ServerConnectionsCount += -1
      if self.ServerConnectionsCount < 0
        self.ServerConnectionsCount = 0
      end
    end
  end


!------------------------------------------------------------------------------
NetSimple.Ping       Procedure  (string p_IP,long p_TTL)
  code
  return (NetPing (p_IP, p_TTL))
!------------------------------------------------------------------------------
NetSimple.WholePacketSend Procedure  (*string p_String, long p_Length)
OverLengthString   string(4)
be                 string(4)
OverLength         long,over(OverLengthString)   ! These 2 variables occupy the same data in memory

HeaderSize         long
DataSent           long                          ! Length of data sent, not counting headers

  code
  OverLength = p_Length + choose(self.WholePacketLengthInclusive,4,0)
  if self.WholePacketUseBigEndian
    be[1] = OverLengthString[4]
    be[2] = OverLengthString[3]
    be[3] = OverLengthString[2]
    be[4] = OverLengthString[1]
    OverLengthString = be
  end
  if address (p_String) = 0
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigSend', 'Error - p_String in BigSend is a null pointer.', NET:LogError)
    ****
    self.Error = ERROR:InvalidStringSize
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error - Could not Send Data. p_String in BigSend is a null pointer.', 'NetSimple.BigSend')
    return
  end
  if size (p_String) < p_Length
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigSend', 'Error - p_String in BigSend is smaller than the length requested to send.', NET:LogError)
    ****
    self.Error = ERROR:InvalidStringSize
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error - Could not Send Data. p_String in BigSend is smaller than the length requested to send.', 'NetSimple.BigSend')
    return
  end

  if p_Length <= 0
    if p_Length < 0
      compile ('****', NETTALKLOG=1)
        self.Log ('NetSimple.BigSend', 'Error - p_Length in BigSend is less than zero.', NET:LogError)
      ****
      self.Error = ERROR:InvalidStringSize
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Error - Could not Send Data. p_Length in BigSend is less than zero.', 'NetSimple.BigSend')
      return
    end
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigSend', 'Warning - p_Length in BigSend is zero.', NET:LogError)
    ****
    if self.WholePacketUseLengthField
      self.packet.binData[1 : 4] = OverLengthString[1 : 4]
      self.packet.binDataLen = 4
    else
      self.packet.binDataLen = 0
    end
    self.send()
    return
  end

  if self.WholePacketUseLengthField
    HeaderSize = 4 ! For now - will extend this later on for encryption and self defined headers
  else
    HeaderSize = 0
  end

  if p_Length + HeaderSize <= Net:MaxBinData
    ! Really easy-peasy. Everything to send is less than the 16K self.packet.binData buffer size
    self.packet.binDataLen = HeaderSize + p_Length
    if self.WholePacketUseLengthField
      self.packet.binData[1 : self.packet.binDataLen] = OverLengthString[1 : 4] & p_String[1 : p_Length]
    else
      self.packet.binData[1 : self.packet.binDataLen] = p_String[1 : p_Length]
    end
    self.Send()
    return
  else
    ! Multi-packet send
    DataSent = 0
    loop until DataSent >= p_Length
      if DataSent = 0 and self.WholePacketUseLengthField
        ! First Packet - needs Header
        if DataSent + Net:MaxBinData - HeaderSize <= p_Length
          self.packet.binData = OverLengthString[1 : 4] & p_String[1 : Net:MaxBinData - HeaderSize]
          self.packet.binDataLen = Net:MaxBinData
          DataSent += Net:MaxBinData - HeaderSize
        else
          ! You shouldn't get to this line, but just in case
          self.packet.binData = OverLengthString[1 : 4] & p_String[1 : p_Length]
          self.packet.binDataLen = p_Length + HeaderSize
          DataSent += p_Length
        end
      else
        ! Subsequent Packets
        if DataSent + Net:MaxBinData <= p_Length
          ! Can send full 16K
          !self.Log ('NetSimple.BigString', DataSent+1 & ' : ' & Net:MaxBinData - HeaderSize)
          !self.packet.binData = p_String[DataSent+1 : Net:MaxBinData - HeaderSize]  ! GPF City
          self.packet.binData = p_String[DataSent+1 : DataSent+Net:MaxBinData] ! Fix
          self.packet.binDataLen = Net:MaxBinData
          DataSent += Net:MaxBinData
        else
          ! Last packet, won't be full 16K
          self.packet.binData = p_String[DataSent+1 : p_Length]
          self.packet.binDataLen = p_Length - DataSent
          DataSent += self.packet.binDataLen
        end
      end
      self.Send()
    end
    return
  end

!------------------------------------------------------------------------------
NetSimple.WholePacketStorePartialPacket Procedure  ()
TempS                 &string
IncSizeToUse          long
  code
  ! Store Data in Queue
  if self.packet.binDataLen <= 0
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigStorePacket', 'Error - Trying to add less than zero length to BigPacketQueue.', NET:LogError)
    ****
    self.Error = ERROR:InvalidStringSize
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error - Trying to add less than zero length to BigPacketQueue.', 'NetSimple.BigStorePacket')
    return
  end

  clear (self.WholePacketQueue)
  self.WholePacketQueue.SockID = self.packet.SockID
  get (self.WholePacketQueue, self.WholePacketQueue.SockID)
  case errorcode()
  of NoEntryErr
    clear (self.WholePacketQueue)
    self.WholePacketQueue.SockID = self.packet.SockID
    add (self.WholePacketQueue)
  of 0
    ! fine
  else
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigStorePacket', 'Error - Could not access BigPacketQueue Queue', NET:LogError)
    ****
    self.Error = ERROR:QueueAccessError
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error - Could not access BigPacketQueue Queue.', 'NetSimple.BigStorePacket')
    return
  end

  if self.packet.binDataLen > self._WholePacketAllocSize
    IncSizeToUse = self.packet.binDataLen
  else
    IncSizeToUse = self._WholePacketAllocSize
  end

  if self.WholePacketQueue.WholeBinData &= Null
    self.WholePacketQueue.WholeBinData &= New (String(IncSizeToUse))
    if self.WholePacketQueue.WholeBinData &= Null
      compile ('****', NETTALKLOG=1)
        self.Log ('NetSimple.BigStorePacket', 'Error - Could not allocate Memory for BigPacketQueue.', NET:LogError)
      ****
      self.Error = ERROR:CouldNotAllocMemory
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Error - Could not allocate Memory for BigPacketQueue.', 'NetSimple.BigStorePacket')
      return
    end
    if Size(self.WholePacketQueue.WholeBinData) <> IncSizeToUse
      compile ('****', NETTALKLOG=1)
        self.Log ('NetSimple.BigStorePacket', 'Error - Could not allocate Memory for BigPacketQueue (2).', NET:LogError)
      ****
      self.Error = ERROR:CouldNotAllocMemory
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Error - Could not allocate Memory for BigPacketQueue(2).', 'NetSimple.BigStorePacket')
      return
    end
    self.WholePacketQueue._BufferSize = Size(self.WholePacketQueue.WholeBinData)
    self.WholePacketQueue.WholeBinDataLen = 0
  end

  if self.WholePacketQueue.BufferUsed + self.packet.binDataLen > self.WholePacketQueue._BufferSize
    ! Make Buffer Bigger
    if self.WholePacketQueue.BufferUsed > self.WholePacketQueue._BufferSize
      compile ('****', NETTALKLOG=1)
        self.Log ('NetSimple.BigStorePacket', 'Error - BigPacketQueue Corrupt.', NET:LogError)
      ****
      self.Error = ERROR:CorruptPacket
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Error - BigPacketQueue Corrupt.', 'NetSimple.BigStorePacket')
      return
    end
    tempS &= self.WholePacketQueue.WholeBinData
    self.WholePacketQueue.WholeBinData &= New (String(IncSizeToUse + self.WholePacketQueue._BufferSize))
    if self.WholePacketQueue.WholeBinData &= Null
      compile ('****', NETTALKLOG=1)
        self.Log ('NetSimple.BigStorePacket', 'Error - Could not allocate Memory for BigPacketQueue(3).', NET:LogError)
      ****
      self.Error = ERROR:CouldNotAllocMemory
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Error - Could not allocate Memory for BigPacketQueue(3).', 'NetSimple.BigStorePacket')
      return
    end
    if Size(self.WholePacketQueue.WholeBinData) <> IncSizeToUse + self.WholePacketQueue._BufferSize
      compile ('****', NETTALKLOG=1)
        self.Log ('NetSimple.BigStorePacket', 'Error - Could not allocate Memory for BigPacketQueue(4).', NET:LogError)
      ****
      self.Error = ERROR:CouldNotAllocMemory
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Error - Could not allocate Memory for BigPacketQueue(4).', 'NetSimple.BigStorePacket')
      return
    end
    self.WholePacketQueue._BufferSize = Size(self.WholePacketQueue.WholeBinData)
    self.WholePacketQueue.WholeBinData [1 : self.WholePacketQueue.BufferUsed] = tempS [1 : self.WholePacketQueue.BufferUsed]
    dispose (tempS)
  end

  self.WholePacketQueue.WholeBinData [self.WholePacketQueue.BufferUsed+1 : self.WholePacketQueue.BufferUsed+self.packet.binDataLen] = self.packet.binData [1 : self.packet.binDataLen]
  self.WholePacketQueue.BufferUsed += self.packet.binDataLen

  put (self.WholePacketQueue)

!------------------------------------------------------------------------------
NetSimple.WholePacketTruncate Procedure  (long p_bytes)
  code
  ! Assumes BigQueue is pointing to the correct entry
  if self.WholePacketQueue.WholeBinData &= null
    return
  end
  if self.WholePacketQueue.SockID <> self.packet.SockID
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigClearPacket', 'Error - SockID not correct in self.WholePacketQueue.', NET:LogError)
    ****
    self.Error = ERROR:DataIsOutOfTopic
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error - SockID not correct in self.WholePacketQueue.', 'NetSimple.BigClearPacket')
    return
  end

  if p_bytes = 0
    p_bytes = self.WholePacketQueue.WholeBinDataLen
  end

  if p_bytes > self.WholePacketQueue.BufferUsed
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigClearPacket', 'Error - BufferUsed corrupt in BigQueue or you are trying to clear too much data.', NET:LogError)
    ****
    self.Error = ERROR:BadOptions
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error - BufferUsed corrupt in BigQueue or you are trying to clear too much data.', 'NetSimple.BigClearPacket')
    self.WholePacketQueue.WholeBinDataLen = 0
    self.WholePacketQueue.BufferUsed = 0 ! Try and recover - but it's got to be bad.
    put (self.WholePacketQueue)
    return
  end

  if p_bytes = self.WholePacketQueue.BufferUsed
    ! Easiest case
    self.WholePacketQueue.WholeBinDataLen = 0
    self.WholePacketQueue.BufferUsed = 0
    if self._WholePacketDontFreeWholeBinData = 0
      if ~(self.WholePacketQueue.WholeBinData &= NULL)
        dispose (self.WholePacketQueue.WholeBinData)
      end
      self.WholePacketQueue._BufferSize = 0
    end
  else
    if self.WholePacketQueue.BufferUsed > self.WholePacketQueue._BufferSize
      compile ('****', NETTALKLOG=1)
        self.Log ('NetSimple.BigClearPacket', 'Error - BufferUsed corrupt in BigPacketQueue (2).', NET:LogError)
      ****
      self.Error = ERROR:CorruptPacket
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Error - BufferUsed corrupt in BigPacketQueue (2).', 'NetSimple.BigClearPacket')
      self.WholePacketQueue.WholeBinDataLen = 0
      self.WholePacketQueue.BufferUsed = 0 ! Try and recover - but it's got to be bad.
      put (self.WholePacketQueue)
      return
    end
    if Size(self.WholePacketQueue.WholeBinData) <> self.WholePacketQueue._BufferSize
      compile ('****', NETTALKLOG=1)
        self.Log ('NetSimple.BigClearPacket', 'Error - BufferUsed corrupt in BigPacketQueue (3).', NET:LogError)
      ****
      self.Error = ERROR:CorruptPacket
      self.WinSockError = 0
      self.SSLError = 0
      self.ErrorTrap ('Error - BufferUsed corrupt in BigPacketQueue (3).', 'NetSimple.BigClearPacket')
      self.WholePacketQueue.WholeBinDataLen = 0
      self.WholePacketQueue.BufferUsed = 0 ! Try and recover - but it's got to be bad.
      put (self.WholePacketQueue)
      return
    end
    self.WholePacketQueue.WholeBinData [1 :  self.WholePacketQueue.BufferUsed-p_bytes] = self.WholePacketQueue.WholeBinData [p_bytes+1 : self.WholePacketQueue.BufferUsed]
    self.WholePacketQueue.WholeBinDataLen = 0
    self.WholePacketQueue.BufferUsed -= p_bytes
  end
  put (self.WholePacketQueue)


!------------------------------------------------------------------------------
NetSimple.WholePacketCheckForWhole Procedure  ()
OverLengthString   string(4)
OverLength         long,over(OverLengthString)     ! These 2 variables occupy the same data in memory
HeaderSize         long
  code
  ! Assumes WholePacketQueue is pointing to the correct record
  ! Are you happy with this???

  if self.WholePacketQueue.SockID <> self.packet.SockID
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigCheckForPacketEnd', 'Error - SockID not correct in self.WholePacketQueue.', NET:LogError)
    ****
    self.Error = ERROR:DataIsOutOfTopic
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error - SockID not correct in self.WholePacketQueue.', 'NetSimple.BigCheckForPacketEnd')
    return
  end

  if self.WholePacketUseLengthField = 0
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigCheckForPacketEnd', 'Warning - This function (WholePacketUseLengthField) only works if BigUsePacketBoundaries is on.', NET:LogError)
    ****
    return ! This function only works if WholePacketUseLengthField is on
  end

  if self.WholePacketQueue.WholeBinData &= NULL
    return ! No data buffer to work on
  end

  if self.WholePacketQueue.BufferUsed < 4
    return ! Nothing to do yet - not enough data received to work out the packet length
  end

  if size (self.WholePacketQueue.WholeBinData) <> self.WholePacketQueue._BufferSize
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigCheckForPacketEnd', 'Error - BigPacketQueue Corrupt (1).', NET:LogError)
    ****
    self.Error = ERROR:CorruptPacket
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error - BigPacketQueue Corrupt (1).', 'NetSimple.BigCheckForPacketEnd')
    return
  end

  if self.WholePacketQueue.BufferUsed > self.WholePacketQueue._BufferSize
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimple.BigCheckForPacketEnd', 'Error - BigPacketQueue Corrupt (2).', NET:LogError)
    ****
    self.Error = ERROR:CorruptPacket
    self.WinSockError = 0
    self.SSLError = 0
    self.ErrorTrap ('Error - BigPacketQueue Corrupt (2).', 'NetSimple.BigCheckForPacketEnd')
    return
  end

  loop
    if self.WholePacketQueue.BufferUsed < 4
      return ! Nothing to do yet - not enough data received to work out the packet length
    end
    if self.WholePacketUseBigEndian
      OverLengthString[1] = self.WholePacketQueue.WholeBinData [4]
      OverLengthString[2] = self.WholePacketQueue.WholeBinData [3]
      OverLengthString[3] = self.WholePacketQueue.WholeBinData [2]
      OverLengthString[4] = self.WholePacketQueue.WholeBinData [1]
    else
      OverLengthString = self.WholePacketQueue.WholeBinData [1 : 4]
    end
    if self.WholePacketLengthInclusive
      OverLength -= 4
    end
    if OverLength > self.WholePacketQueue.BufferUsed
      return ! Wait for more data to arrive. You don't yet have a full packet.
    end

    ! Okay you have a full packet
    HeaderSize = 4 ! May change in future
    self.WholePacketQueue.WholeBinDataLen = OverLength
    if (self.WholePacketQueue.BufferUsed - HeaderSize) > 0
      self.WholePacketQueue.WholeBinData [1 : self.WholePacketQueue.BufferUsed-HeaderSize] = self.WholePacketQueue.WholeBinData [HeaderSize+1 : self.WholePacketQueue.BufferUsed]
    end
    self.WholePacketQueue.BufferUsed -= HeaderSize
    put (self.WholePacketQueue)
    self.packet.PacketType = NET:SimpleWholeDataPacket
    self.Process()
    self.WholePacketTruncate (OverLength) ! Chop this amount off the packet buffer
  end

!------------------------------------------------------------------------------
NetSimple.WholePacketFree Procedure  (long p_SockID)
  code
  if self.WholePacketQueue &= NULL
    return
  end
  if records(self.WholePacketQueue) <= 0
    return
  end

  clear (self.WholePacketQueue)
  self.WholePacketQueue.SockID = p_SockID
  get (self.WholePacketQueue, self.WholePacketQueue.SockID)
  if errorcode() = 0
    self.WholePacketQueue._BufferSize = 0   ! Good coding
    self.WholePacketQueue.BufferUsed = 0    ! Good coding
    self.WholePacketQueue.WholeBinDataLen = 0 ! Good coding
    if ~(self.WholePacketQueue.WholeBinData &= NULL)
      dispose (self.WholePacketQueue.WholeBinData)
    end
    delete (self.WholePacketQueue)
  end
!------------------------------------------------------------------------------
NetSimple.PartiallyCloseServer Procedure  ()
  code
  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      if self._connection.mode = NET:SimpleClient
        self.Log ('NetSimple.PartiallyCloseServer', 'Warning. This method can only be called on a Server Listening Port. Socket = ' & self._connection.Socket & |
                                     ' SockID = ' & self._connection.SockID & |
                                     ' to Server = ' & clip (self._connection.Server) & |
                                     ' Originally Connected to Port = ' & self._connection.Port)
        return
      else
        self.Log ('NetSimple.PartiallyCloseServer', 'Partially Closing Listening Server Socket = ' & self._connection.Socket & |
                                     ' SockID = ' & self._connection.SockID & |
                                     ' on local Server = ' & |
                                     ' Listening on Port = ' & self._connection.Port)
      end
    end
  ****

  if self.openFlag <> 0
    self.error = NetSimpleClose(self._connection, 0, 0, 3) ! Partially Close Phase
    if self.error = 0
      ! Cool
    else
      self._CallErrorTrap('Unable to Partially Close connection', 'NetSimple.PartiallyCloseServer')
    end
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple.PartiallyCloseServer', 'Warning. Socket not open. Can not close')
      end
    ****
  end

!------------------------------------------------------------------------------
NetSimple.RefreshQServerConnections Procedure  (long p_Free=0)
x  long
  code
  ! If p_Free <> 0 then we don't refresh the list, we just free it
  if self.qServerConnections &= NULL
    ! Do nothing the queue is undefined
  else
    free (self.qServerConnections)
    if p_Free = 0
      x = NetSimpleGetServerConnections (self._Connection, self.qServerConnections)
    end
  end


!------------------------------------------------------------------------------
NetSimple._SSLGetRemoteCertificate Procedure  (long p_Socket, long p_SockID, *long p_Closed)
ret              long
  code
  ret = NetSimpleSSLGetRemoteCerticate(self._connection, p_Socket, p_SockID, p_Closed)
  ! If this function works, then self.SSLCertificateGroup.RemoteCertificateHaveDetails = 1
!------------------------------------------------------------------------------
NetSimple._CallErrorTrap Procedure  (string errorStr,string functionName)
loc:tempStr    string (255)
  code
  ! This method is responsible for cleaning things up before ErrorTrap is called.
  ! This isn't used much in NetSimple per se, but in objects like FTP it's used
  ! so that when the ErrorTrap method goes off the object is ready to take a new command

  compile ('****', NETTALKLOG=1)
    loc:tempStr = self.InterpretError()
    if self.error = 0
      self.Log ('NetSimple._CallErrorTrap', |
                'Error = ' & self.error & |
                ' = ' & clip(errorStr) & |
                ' Function = ' & clip(functionName) & |
                ' Server = ' & clip (self._connection.Server) & |
                ' Port = ' & clip (self._connection.Port), NET:LogError)
    else
      self.Log ('NetSimple._CallErrorTrap', |
                'Error = ' & self.error & |
                ' = ' & clip(errorStr) & |
                ' : ' & clip (loc:tempStr) & |
                ' Function = ' & clip(functionName) & |
                ' Server = ' & clip (self._connection.Server) & |
                ' Port = ' & clip (self._connection.Port), NET:LogError)
    end
  ****

  self.ErrorTrap (ErrorStr, FunctionName)

!------------------------------------------------------------------------------
NetSimple._SSLSwitchToSSL Procedure  (long p_Socket=0, long p_SockID=0)
  ! This method should only be called if the self._SSLDontConnectOnOpen = 1 was set before the initial open
  ! For Server mode you'll need to set the p_Socket and p_SockID parameters
  code
  if self._connection.mode = NET:SimpleClient
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple._SSLSwitchToSSL', 'Switching Client Connection to SSL (Secure) Connection.' & |
                  ' Socket = ' & self._connection.Socket & |
                  ' SockID = ' & self._connection.SockID & |
                  ' Host = ' & self._connection.Server & |
                  ' Port = ' & self._connection.Port)
      end
    ****

    self.WinSockError = 0
    self.SSLError = 0
    self.error = NetSimpleSSLSwitchToSSL (self._Connection)

    if self.error <> 0
      self._CallErrorTrap('SSL (Secure) Client Connection failed.', 'NetSimple._SSLSwitchToSSL')
    else
      ! Wait for .Process to be called with a AsyncOpenSuccessful packet type
    end
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimple._SSLSwitchToSSL', 'Switching Server Connection to SSL (Secure) Connection.' & |
                  ' Socket = ' & p_Socket & |
                  ' SockID = ' & p_SockID & |
                  ' Host = ' & self._connection.Server & |
                  ' Port = ' & self._connection.Port)
      end
    ****

    self.WinSockError = 0
    self.SSLError = 0
    self.error = NetSimpleSSLSwitchToSSL (self._Connection, p_Socket, p_SockID)

    if self.error <> 0
      self._CallErrorTrap('SSL (Secure) Server Connection failed.', 'NetSimple._SSLSwitchToSSL')
    else
      ! Wait for .Process to be called with a AsyncOpenSuccessful packet type
    end
  end

!------------------------------------------------------------------------------
NetDIP.Process       Procedure
local   group, pre (loc)
x         long
y         long
NeedToSend byte
        end

  code
  case self.packet.packetType
  ! ------------------------
  of Net:SimpleIdleConnection
  ! ------------------------
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetDIP.Process', 'Connection Idle - will close now')
      end
    ****
    self.abort() ! Close down

  ! ------------------------
  of NET:SimpleAsyncOpenSuccessful
  ! ------------------------
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetDIP.Process', 'Connection opended Asynchonously')
      end
    ****
    self.SendData()

  ! ------------------------
  of Net:SimplePartialDataPacket
  ! ------------------------
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        if self.packet.binDataLen > 0
          self.Log ('NetDIP.Process', 'Data Received [' & |
                    self.packet.bindata[1 : self.packet.binDataLen] & ']')
        end
      end
    ****
    loc:NeedToSend = true
    if self.TryingToClose
      if instring('OK',self.packet.bindata[1 : self.packet.binDataLen],1,1)
        self.CanCloseNow = 1
      elsif instring('502',self.packet.bindata[1 : self.packet.binDataLen],1,1)
        self.CanCloseNow = 2
      end
    elsif instring('<<Ok> OK ADD',self.packet.bindata[1 : self.packet.binDataLen],1,1)
      ! Add was successful
      if self.AutoConnect
        self.GetFriends()
        loc:NeedToSend = true
      else
        self.close()
      end
    elsif instring('<<Error>',self.packet.bindata[1 : self.packet.binDataLen],1,1)
      ! Error
      self.close()
    elsif instring  ('<<Ok> OK DEL',self.packet.bindata[1 : self.packet.binDataLen],1,1)
      ! Delete was successful
      self.close()
    elsif instring('<<Room>',self.packet.bindata[1 : self.packet.binDataLen],1,1)
      ! Get Friends was successful
      self.friends = self.packet.bindata
      if Self.AutoConnect
        Self.ConnectToFriends()
        loc:NeedToSend = true
      end
      self.close()
    elsif instring('<<IP>',upper(self.packet.bindata[1 : self.packet.binDataLen]),1,1)
      self.MyIPAddress = ''
      loc:x = instring('<<IP>',upper(self.packet.bindata[1 : self.packet.binDataLen]),1,1)
      if loc:x > 0
        loc:y = instring('<</IP>',upper(self.packet.bindata[1 : self.packet.binDataLen]),1,loc:x)
        if loc:y > 0
          self.MyIPAddress = self.packet.bindata[(loc:x+5):(loc:y-2)]
          self.GotWhoAmI()
        end
      end
    end
    if loc:NeedToSend = true and records (self.DataQ)
      self.SendData()
    end

  end
!------------------------------------------------------------------------------
NetDIP.AddMe         Procedure
loc:ourNetID           group (Net:AutoIDType).
loc:ourNetIDStr        string (size(Net:AutoIDType)),over (loc:ourNetID)
  code
  if (self.LastAddedDate = today()) and ((clock() - self.LastAddedTime) < (self.RefreshIntervalSeconds * 100))
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetDIP.AddMe', 'AddMe skipped because minimum time interval not reached')
      end
    ****
  else
    self.LastAddedDate = today()    ! Date AddMe was last called
    self.LastAddedTime = clock()

    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetDIP.AddMe', 'In AddMe')
      end
    ****

    NetOptions(NET:GETNETID, loc:ourNetIDStr)
    self.DataQ.data = 'ADD <<ROOM>'& clip(Self.Room) &'<</ROOM> <<GUID>'& loc:ourNetID.GUID1 & '.' & loc:ourNetID.GUID2  &'<</GUID>'
    self.DataQ.dataLen = len(clip(self.DataQ.data))
    Add (self.DataQ)

    if self.OpenFlag = 0 and self.AsyncOpenBusy = 0   ! Not connected, so try and connect
      self.open (self.Server, self.Port)
      if self.error <> 0
        return             ! Could not open Port
      end
    else
      self.SendData()
    end

    if self.AsyncOpenUse = 0
      self.SendData()
    end
  end


!------------------------------------------------------------------------------
NetDIP.GetFriends    Procedure
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetDIP.GetFriends', 'In GetFriends()')
    end
  ****

  self.DataQ.data = 'GET <<Room>'& clip(Self.Room) &'<</Room>'
  self.DataQ.dataLen = len(clip(self.DataQ.data))
  Add (self.DataQ)

  if self.OpenFlag = 0 and self.AsyncOpenBusy = 0   ! Not connected, so try and connect
    self.open (self.Server, self.Port)
    if self.error <> 0
      return             ! Could not open Port
    end
  else
    self.SendData()
  end

  if self.AsyncOpenUse = 0
    self.SendData()
  end


!------------------------------------------------------------------------------
NetDIP.ConnectToFriends Procedure
local         group, pre (loc)
ourRemote       group(Net:AutoRemoteType) .
x               long
y               long
              end
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetDIP.ConnectToFriends', 'In ConnectToFriends()')
    end
  ****

  loc:x = instring('<<IP>',upper(self.friends),1,1)
  loop while loc:x
    loc:y = instring('<</IP>',upper(self.friends),1,loc:x)  ! upper added 14/3/2002 - Jono
    clear(loc:ourRemote)
    loc:ourRemote.Name = self.friends[(loc:x+5):(loc:y-2)]       ! fixed by Jono 14 Dec 2000 was sub(self.friends,loc:x+4,loc:y-loc:x-4)
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetDIP.ConnectToFriends', 'NetRemote ' & clip (loc:ourRemote.Name))
      end
    ****
    NetAutoRemote(loc:ourRemote, NET:ASYNCHRONOUS, NET:DONT_SHOW_ERRORS)
    loc:x = instring('<<IP>',upper(self.friends),1,loc:x+3)
  end
!------------------------------------------------------------------------------
NetDIP.DeleteMe      Procedure
loc:ourNetID           group (Net:AutoIDType).
loc:ourNetIDStr        string (size(Net:AutoIDType)),over (loc:ourNetID)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetDIP.DeleteMe', 'In DeleteMe()')
    end
  ****

  self.LastAddedDate = 0
  self.LastAddedTime = 0

  NetOptions(NET:GETNETID, loc:ourNetIDStr)
  self.DataQ.data = 'DEL <<ROOM>'& clip(Self.Room) &'<</ROOM> <<GUID>'& loc:ourNetID.GUID1 & '.' & loc:ourNetID.GUID2 &' <</GUID>'
  self.DataQ.dataLen = len(clip(self.DataQ.data))
  Add (self.DataQ)

  if self.OpenFlag = 0 and self.AsyncOpenBusy = 0   ! Not connected, so try and connect
    self.open (self.Server, self.Port)
    if self.error <> 0
      return             ! Could not open Port
    end
  else
    self.SendData()
  end

  if self.AsyncOpenUse = 0
    self.SendData()
  end


!------------------------------------------------------------------------------
NetDIP.ErrorTrap     Procedure  (string errorStr,string functionName)
  code
    if self.TryingToClose
      self.CanCloseNow = 3
    end
    parent.ErrorTrap(errorStr,functionName)
!------------------------------------------------------------------------------
NetDIP.Init          Procedure  (ulong Mode=NET:SimpleClient)
  code
  self._friends &= new(String (8192))    ! 8K
  self.friends &= self._friends          ! Give programmer the chance to use their own variable

  self._DataQ &= new(Net:DIPDataQType)
  self.DataQ &= self._DataQ

  self.AsyncOpenUse = 1                  ! Default to using Async Open
  self.InActiveTimeout = 60 * 100        ! Disconnect after 60 seconds of inactivity

  self.RefreshIntervalSeconds = 60 * 60  ! Refresh (call Addme) every 60 minutes

  parent.init (Mode)
!------------------------------------------------------------------------------
NetDIP.Kill          Procedure
  code
  parent.kill()

  if ~self._friends &= NULL  ! Moved after kill by Jono 15/3/2001
    dispose(self._friends)
  end

  if ~self._DataQ &= NULL
    dispose(self._DataQ)
  end


!------------------------------------------------------------------------------
NetDIP.TakeEvent     Procedure
local      group, pre (loc)
StoreTimer   long
           end
  code
  parent.TakeEvent()
  if event() = event:timer
    self._TimerCount += 0{prop:timer}
    if self._TimerCount >= (self.RefreshIntervalSeconds*100)
      self._TimerCount = 0 ! Start again
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetDIP.TakeEvent', 'Refresh Interval reached (' & self.RefreshIntervalSeconds & ' seconds) - Call AddMe again')
        end
      ****
      loc:StoreTimer = 0{prop:timer} ! Store Timer
      0{prop:timer} = 0
      self.AddMe()
      0{prop:timer} = loc:StoreTimer ! Restore Timer
    end
  end
  if self.DontUpdateAdd = 0
    if self.LastAddedDate
      if self.LastAddedDate <> today()
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetDIP.TakeEvent', 'A new day has dawned - so AddMe again')
          end
        ****
        self.AddMe()             ! Add me if this was last Added on a previous day.
      end
    end
  end

!------------------------------------------------------------------------------
NetDIP.WhoAmI        Procedure
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetDIP.WhoAmI', 'In WhoAmI()')
    end
  ****

  self.DataQ.data = 'WHO'
  self.DataQ.dataLen = len(clip(self.DataQ.data))
  Add (self.DataQ)

  if self.OpenFlag = 0 and self.AsyncOpenBusy = 0   ! Not connected, so try and connect
    self.open (self.Server, self.Port)
    if self.error <> 0
      return             ! Could not open Port
    end
  else
    self.SendData()
  end

  if self.AsyncOpenUse = 0
    self.SendData()
  end


!------------------------------------------------------------------------------
NetDIP.GotWhoAmI     Procedure
  code
!------------------------------------------------------------------------------
NetDIP.SendData      Procedure
local      group, pre (loc)
x            long
           end
  code
  if records (self.DataQ) <= 0
    return
  end

  if self.OpenFlag = 0 and self.AsyncOpenBusy = 0   ! Not connected, so try and connect
    self.open (self.Server, self.Port)
    if self.error <> 0
      return             ! Could not open Port
    end
    return ! Wait till it's open
  elsif self.AsyncOpenBusy
    return ! Wait - connection is opening
  end

  get (self.DataQ, 1)
  if errorcode() = 0
    self.packet.binData = self.DataQ.Data
    self.packet.binDataLen = self.DataQ.DataLen
    delete (self.DataQ)
    self.DontErrorTrapInSendIfConnectionClosed = 1
    if self.packet.binDataLen > 0
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetDIP.SendData', 'About to Send [' & self.packet.binData[1 : self.packet.binDataLen] & ']')
          self.Log ('NetDIP.SendData', 'Records now = ' & records (self.DataQ))
        end
      ****
      self.Send()
      if self.Error = ERROR:ClientNotConnected
        ! This error occurs if the connection closes
        ! and we try and send data on it, before the
        ! object knows the object has actually closed.

        ! Ignore
      end
      if self.packet.binData [1 : 3] = 'DEL'
        self.CanCloseNow = 0
      end
    end
  else
    free (self.DataQ)
  end

!------------------------------------------------------------------------------
NetSimpleMultiClient.Send Procedure  ()
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSimpleMultiClient.Send (Client Mode)', 'Sending Data to' & |
                                  ' HostName = ' & clip (self.q._Connection.Server) & |
                                  ' OnSocket = ' & self.q._Connection.Socket & |
                                  ' SockID = ' & self.q._Connection.SockID & |
                                  ' Port = ' & self.q._Connection.Port & |
                                  ' BinDataLen = ' & self.q.packet.binDataLen)
    end
  ****

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if self.q.packet.binDataLen > NET:MaxBinData
    self.error = ERROR:InvalidStructureSize
    self.errortrap('Trying to send too much data in self.packet.binData and self.packet.binDataLen', 'NetSimpleMultiClient.Send')
    return
  end

  if self.q.openFlag = false
    self.error = ERROR:ConnectionNotOpen
    self.errortrap('The connection is not open and therefore the data can not be sent.', 'NetSimpleMultiClient.Send')
    return
  end

  if self.q.packet.binDataLen <= 0
    self.error = ERROR:InvalidStructureSize
    self.errortrap('Data length is less than or equal to zero', 'NetSimpleMultiClient.Send')
    return
  end


  if clip (self.q._Connection.Server) = '' or self.q._Connection.Port = 0
    self.error = ERROR:NoIPorSocketSpecified
    self.errortrap('Hostname or Port are not set to anything. (You must call self.open first).', 'NetSimpleMultiClient.Send')
    return
  end

  self.Error = NetSimpleSend(self.q._connection, self.q.packet)
  if self.error <> 0
    self.errortrap('Error sending data', 'NetSimpleMultiClient.Send')
  end

!------------------------------------------------------------------------------
NetSimpleMultiClient.Open Procedure  (string Server,uShort Port=0)
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSimpleMultiClient.Open', 'Attempting to connect to ' & clip (server) & ' on Port ' & port)
    end
  ****

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  clear (self.q)

  self.q._connection.Mode = NET:SimpleClient
  if self.UseThisThread <> 0
    self.q._connection.NotifyThread = self.UseThisThread
  else
    self.q._connection.NotifyThread = Thread()
  end
  self.prime()

  self.q._connection.Server = clip (Server)   ! Method parameters
  self.q._connection.Port = Port

  if self.AsyncOpenUse
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClient.Open', 'Will use Asynchronous Open (Timeout = ' & |
                  self.AsyncOpenTimeout & 'hs)')
      end
    ****
    self.q._Connection.AsyncOpenTimeout = self.AsyncOpenTimeout
  else
    self.q._Connection.AsyncOpenTimeout = 0 ! Synchronous Open
  end

  if self.InActiveTimeout
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClient.Open', 'Will use InActive (Idle) Timeout = ' & |
                  self.AsyncOpenTimeout & 'hs')
      end
    ****
    self.q._Connection.InactiveTimeout = self.InactiveTimeout
  else
    self.q._Connection.InactiveTimeout = 0 ! No Inactive Timeout
  end

  self.Error = NetSimpleOpen(self.q._connection)

  if self.error = 0
    if self.q._connection.AsyncOpenTimeOut > 0
      self.q.openFlag = 0
      self.q.AsyncOpenBusy = 1
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimple(Client).Open', 'Opening Connection Asynchrously (will wait for Process() or Err0rTrap() to be called).' & |
                    ' on Socket = ' & self.q._connection.Socket & |
                    ' SockID = ' & self.q._connection.SockID)
        end
      ****
    else
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimpleMultiClient.Open', 'Connection opened' & |
                    ' on Socket = ' & self.q._connection.Socket & |
                    ' SockID = ' & self.q._connection.SockID)

        end
      ****
      self.q.openFlag = 1
    end

    self.q.SockID = self.q._Connection.SockID
    add (self.q, self.q.SockID)
  else
    self.errortrap('Unable to Open Connection', 'NetSimpleMultiClient.Open')
    clear (self.q)
    NetFreeUniqueEvent(self.q._connection.NotifyEvent)
  end

!------------------------------------------------------------------------------
NetSimpleMultiClient.Close Procedure  ()
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSimpleMultiClient.Close', 'Closing Client Socket = ' & self.q._connection.socket & |
                ' SockID = ' & self.q._connection.SockID & |
                ' to Server = ' & clip (self.q._connection.Server) & |
                ' Originally Connected to Port = ' & self.q._connection.port)
    end
  ****

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if self.q.openFlag <> 0
    self.error = NetSimpleClose(self.q._connection, 0, 0, 1) ! First Phase Close
    self.q._BetweenPhaseOneAnTwo = true
    self.q._IgnorePhaseTwo = false
    if self.error = 0
      self.q.openFlag = 0
      put (self.q)               ! error fix - Jono 7/3/2002 - (thanks Craig)
    else
      self.q.openFlag = 0        ! Hope that because there was an error that the connection has been closed. Jono 17/8/2000
      put (self.q)               ! Save the change
      self.errortrap('Unable to Close connection', 'NetSimpleMultiClient.Close')
      delete (self.q)
    end
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClient.Close', 'Warning. Socket not open. Can not close')
      end
    ****
  end

!------------------------------------------------------------------------------
NetSimpleMultiClient.Process Procedure  ()
  code
!---------------------------------------------------------------------------------
! This is a virtual method handled by derivative classes.
! The packet will contain the data of the received packet.
! To send a reply you will need to call your self.send() method
!---------------------------------------------------------------------------------
!------------------------------------------------------------------------------
NetSimpleMultiClient.Prime Procedure  ()
  code
  self._Wait()

  self.q._connection.NotifyEvent = NetGetUniqueEvent()
  if self.q._connection.NotifyEvent = 0
    compile ('****', NETTALKLOG=1)
      self.Log ('NetSimpleMultiClient.Prime', 'Error getting new unique event. None was available.', NET:LogError)
    ****
  end

  self._Release()

  ! this is a simple method which sets the base message for this object.
  ! self.q._connection.Event is set so that each object get's it's own events.
  ! Each instance of each object is currently assigned 5 events.
!------------------------------------------------------------------------------
NetSimpleMultiClient.Init Procedure  ()
  code
  ! Allocate Queue structures
  self._q &= new (Net:SimpleMultiClientQType)
  self.q &= self._q

  self.AsyncOpenTimeOut = 1200 ! 12 seconds  (To use Async Open set .AsyncOpenUse = 1)

  ! ----------------------- Logging options ------------------------------------------
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSimpleMultiClient.Init', 'Initialising a new Multi-Client NetSimple object')
    end
  ****
  ! ----------------------- End Logging options --------------------------------------

!------------------------------------------------------------------------------
NetSimpleMultiClient.TakeEvent Procedure  ()
local        group, pre(loc)
x              long
FoundEvent     long   ! we will only loop through self.q until we find a matching event
Result         long
             end
  code
  loop loc:x = 1 to records (self.q)
    get (self.q, loc:x)
    case (event())
    !--------------------------------
    of self.q._connection.NotifyEvent                        ! There is a packet to receive from the DLL.
    !--------------------------------
      loc:FoundEvent = true
      loc:Result = NetSimpleReceive(self.q._connection,self.q.packet)  ! Receive the packet
      self.Error = loc:Result
      self.WinSockError = self.q.packet.WinSockError
      self.SSLError = self.q.packet.SSLError
      if self.Error = 0
        if self.SSLError <> 0                        ! SSL Errors have priority over WinSock Errors
          self.Error = ERROR:ErrorStoredInSSLError
        elsif self.WinSockError <> 0
          self.Error = ERROR:ErrorStoredInWinSockError
        end
      end

      if loc:Result <> 0
        if loc:Result = -13
          self.Log('NetSimpleMultiClient.TakeEvent', 'Warning. DLL could not find a packet matching this connection. This is not normally a problem.')
        else
          self.ErrorTrap('An error occurred while trying receive data','NetSimpleMultiClient.TakeEvent')
        end
        return
      end
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimpleMultiClient.TakeEvent', 'Received a message.' & |
                    ' PacketType = ' & self.q.packet.PacketType & |
                    ' _DebugPacketNumber = ' & self.q.packet._DebugPacketNumber & |
                    ' Mode = ' & self.q._connection.mode)
        end
      ****

      Case (self.q.packet.PacketType)
      !==========================================================
      of NET:SimpleSocketClosedPacket1    ! Server closed Remotely (FD_CLOSE)
      !==========================================================
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetSimpleMultiClient.TakeEvent', 'PacketType -> Calls ConClosed(1) (Remote)')
          end
        ****
        Do ConClosed1
      !==========================================================
      of NET:SimpleSocketClosedPacket2    ! Server closed locally by Close() method
      !==========================================================
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetSimpleMultiClient.TakeEvent', 'PacketType -> Calls ConClosed(2)')
          end
        ****
        Do ConClosed2


      !==========================================================
      of NET:SimpleAsyncOpenSuccessful
      !==========================================================
        self.Log ('NetSimpleMultiClient.TakeEvent', 'PacketType -> "AsyncOpenSuccessful" to ' & |
                   clip (self.q._Connection.Server) & ':' & self.q._Connection.Port & |
                  ' On Socket = ' & self.q.packet.OnSocket & |
                  ' SockID = ' & self.q.packet.SockID & |
                  ' Local_IP = ' & self.q._Connection.LocalIP)
        self.q.openflag = 1
        self.q.AsyncOpenBusy = 0
        put (self.q)

        self.Process()

      !==========================================================
      of NET:SimpleAsyncOpenFailed
      !==========================================================
        self.Log ('NetSimpleMultiClient.TakeEvent', 'Error. PacketType -> "AsyncOpenFailed" to ' & |
                   clip (self.q._Connection.Server) & ':' & self.q._Connection.Port & |
                  ' On Socket = ' & self.q.packet.OnSocket & |
                  ' SockID = ' & self.q.packet.SockID)
        self.error = ERROR:OpenTimeout ! 14/2/2003 was ERROR:WaitTimeOut - which was incorrect

        Do AsyncOpenFailed

      !==========================================================
      of NET:SimpleNoConnection   ! Ignore NoConnection Packets. The connection has closed!
      !==========================================================
        ! Do nothing

      !==========================================================
      of    NET:SimpleNewConnection
      orof  NET:SimplePartialDataPacket
      orof  NET:SimpleIdleConnection
      !==========================================================
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            case self.q.packet.PacketType
            of NET:SimpleNewConnection
              self.Log ('NetSimpleMultiClient.TakeEvent', 'PacketType -> New Connection to local Port = ' & self.q._Connection.Port & |
                        ' On Socket = ' & self.q.packet.OnSocket & |
                        ' SockID = ' & self.q.packet.SockID)
            of NET:SimplePartialDataPacket
              self.Log ('NetSimpleMultiClient.TakeEvent', 'PacketType -> Normal Data Packet Received' & |
                        ' On Socket = ' & self.q.packet.OnSocket & |
                        ' SockID = ' & self.q.packet.SockID)
            of NET:SimpleIdleConnection
              self.Log ('NetSimpleMultiClient.TakeEvent', 'PacketType -> Idle Connection' & |
                        ' On Socket = ' & self.q.packet.OnSocket & |
                        ' SockID = ' & self.q.packet.SockID)
            end
          end
        ****
        self.process()                                         ! Call Virtual Process() method
      !==========================================================
      else
      !==========================================================
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetSimpleMultiClient.TakeEvent', 'PacketType -> Unknown! Warning!')
          end
        ****
      end
    !--------------------------------
    of self.q._connection.NotifyEvent + 1                 ! Client Connection closed remotely by FD_CLOSE ! Only used by NET:SimpleClient
    !--------------------------------
      loc:FoundEvent = true
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimpleMultiClient.TakeEvent', 'Received a message+1 about a ConClose(1) (Remote).' & |
                    ' PacketType = ' & self.q.packet.PacketType & |
                    ' Mode = ' & self.q._connection.mode)
        end
      ****
      Do ConClosed1
    !--------------------------------
    of self.q._connection.NotifyEvent + 2                 ! The socket has been closed with the .close() method. Request to clean up data queues in DLL
    !--------------------------------
      loc:FoundEvent = true
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimpleMultiClient.TakeEvent', 'Received a message+2 about a Close (2).' & |
                    ' PacketType = ' & self.q.packet.PacketType & |
                    ' Mode = ' & self.q._connection.mode)
        end
      ****
      if self.q._IgnorePhaseTwo
        compile ('****', NETTALKLOG=1)
          if self.LoggingOn
            self.Log ('NetSimpleMultiClient.TakeEvent.ConClosed2', 'Attention. 2nd Phase NOT Called! The Connection property has changed, since calling Phase One. Can''t call Phase Two, the DLL will clear up in 5 minutes.' & |
                      ' The server connection' & |
                      ' on Socket = ' & self.q.packet.OnSocket & |
                      ' SocketID = ' & self.q.packet.SockID & |
                      ' was closed locally.')
          end
        ****
        ! Don't do anything. The DLL will clear in 5 minutes.
      else
        NetSimpleClose(self.q._connection, 0, 0, 2)         ! Second Phase close - close all
      end
      delete (self.q)
    end ! Big Case
    if loc:FoundEvent = true
      break ! Event was found
    end
  end

! -------------------------------------------------------------------------------
AsyncOpenFailed Routine
  self.q.openflag = 0
  self.q.AsyncOpenBusy = 0
  put (self.q)

  NetSimpleClose(self.q._connection, self.q.packet.OnSocket, self.q.packet.SockID, 2)         ! Second Phase close - close all
  self.ErrorTrap ('The requested connection could not be opened.', 'NetSimpleMultiClient.TakeEvent')
  delete (self.q)


! -------------------------------------------------------------------------------
ConClosed1 Routine
  ! Handles Remote Closing of sockets
  if self.Error = ERROR:ErrorStoredInSSLError and self.SSLError = NET:SSL_R_CERTIFICATE_VERIFY_FAILED
    self.Error = ERROR:SSLInvalidRemoteCertificate
    self.WinSockError = 0
    self.SSLError = 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClientTakeEvent.ConClosed1', 'The Remote SSL Certificate did not verify against the specified CA Root Certificate(s).' & |
                  ' on Socket = ' & self.q.packet.OnSocket & |
                  ' SockID = ' & self.q.packet.SockID)
      end
    ****
    if self.q.openFlag = 0 and self.q.AsyncOpenBusy = 1
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimpleMultiClientTakeEvent.ConClosed1', 'Will follow up with Async Open Failed')
        end
      ****
      Do AsyncOpenFailed
    else
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimpleMultiClientTakeEvent.ConClosed1', 'Will follow up with Connection Closed')
        end
      ****
      Do CallConnectionClosed
    end
  else
    Do CallConnectionClosed
  end

! ------------------------------------------------------------------------------------
CallConnectionClosed Routine
  if self.q._connection.mode = NET:SimpleClient
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClientTakeEvent.ConClosed1', 'The client connection' & |
                  ' on Socket = ' & self.q.packet.OnSocket & |
                  ' SockID = ' & self.q.packet.SockID & |
                  ' has been closed remotely. Will call NetSimpleConnectClose and then the' & |
                  ' ConnectionClosed() method.')
      end
    ****
    NetSimpleClose(self.q._connection, self.q.packet.OnSocket, self.q.packet.SockID, 2)         ! Second Phase close - close all
    self.q.openFlag = 0                                        ! Mark the connection as closed
    self.q.AsyncOpenBusy = 0
    ! don't bother with a put, as the record will be deleted
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClientTakeEvent.ConClosed1', 'The Server connection' & |
                  ' on Socket = ' & self.q.packet.OnSocket & |
                  ' SockID = ' & self.q.packet.SockID & |
                  ' has been closed remotely. Will call NetSimpleConnectClose and then the' & |
                  ' ConnectionClosed() method.')
      end
    ****
    NetSimpleClose(self.q._connection, self.q.packet.OnSocket, self.q.packet.SockID, 2) ! Second Phase close - close all
  end
  self.ConnectionClosed()                                    ! Socket details are in the packet
  delete (self.q)

! ------------------------------------------------------------------------------------
ConClosed2 Routine
  if self.q._IgnorePhaseTwo
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClientTakeEvent.ConClosed2', 'Warning. 2nd Phase NOT Called! The Connection property has changed, since calling Phase One. Can''t call Phase Two, the DLL will clear up in 5 minutes.' & |
                  ' The server connection' & |
                  ' on Socket = ' & self.q.packet.OnSocket & |
                  ' SocketID = ' & self.q.packet.SockID & |
                  ' was closed locally.')
      end
    ****
    ! Don't do anything. The DLL will clear in 5 minutes.
  else
    if self.q._connection.mode = NET:SimpleClient
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimpleMultiClientTakeEvent.ConClosed2', 'The client connection' & |
                    ' on Socket = ' & self.q.packet.OnSocket & |
                    ' SockID = ' & self.q.packet.SockID & |
                    ' was closed locally. Will call NetSimpleConnectClose for 2nd Phase')
        end
      ****
      NetSimpleClose(self.q._connection, self.q.packet.OnSocket, self.q.packet.SockID, 2)         ! Second Phase close - close all
      self.q.openFlag = 0                                        ! Mark the connection as closed
      self.q.AsyncOpenBusy = 0
      ! don't bother with a put, as the record will be deleted
    else
      compile ('****', NETTALKLOG=1)
        if self.LoggingOn
          self.Log ('NetSimpleMultiClientTakeEvent.ConClosed2', 'The server connection' & |
                    ' on Socket = ' & self.q.packet.OnSocket & |
                    ' SocketID = ' & self.q.packet.SockID & |
                    ' was closed locally. Will call NetSimpleConnectClose for 2nd Phase')
        end
      ****
      NetSimpleClose(self.q._connection, self.q.packet.OnSocket, self.q.packet.SockID, 2) ! Second Phase close - close all
    end
  end
  delete (self.q)


!------------------------------------------------------------------------------
NetSimpleMultiClient.ErrorTrap Procedure  (string errorStr,string functionName)
temp2    string (512)
  code
  temp2 = self.InterpretError()

  if self.error = 0
    self.ErrorString = clip(errorStr) & '.'
  else
    self.ErrorString = clip(errorStr) & '.' & ' The error number was ' & |
                       self.error & ' which means ' & clip(temp2) & '.'
  end

  if self.error = ERROR:OpenTimeOut
    ! Also list the WinSock or SSL Errors
    if self.WinSockError
      temp2 = clip(temp2) & ' - [WinSock Error = ' & self.WinSockError & ' : ' & clip (NetErrorStr (self.WinSockError)) & ']'
    end
    if self.SSLError
      temp2 = clip(temp2) & ' - [SSL Error = ' & self.SSLError & ']'
    end
  end


  if self.SuppressErrorMsg = 0
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClient.ErrorTrap', |
                       'Error = ' & self.error & |
                       ' = ' & clip(errorStr) & |
                       ' : ' & clip (temp2) & |
                       ' Function = ' & clip(functionName) & |
                       ' Server = ' & clip (self.q._connection.Server) & |
                       ' Port = ' & clip (self.q._connection.Port), NET:LogError)
      end
    ****
    if self.error = 0
      message('A network communication error has occurred.||Error = ' & clip(errorStr) & '.' & |
              '||Error occurred in function ' & clip(functionName), |
              clip(NET:ErrorTitle), Icon:Asterisk)
    else
      message('A network communication error has occurred.||Error = ' & clip(errorStr) & '.' & |
              '||The error number was ' & self.error & |
              ' which means ' & clip(temp2) & '.' & |
              '||Error occurred in function ' & clip(functionName), |
              clip(NET:ErrorTitle), Icon:Asterisk)
    end

  else                                       ! Only log to file
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClient.ErrorTrap', '<<no display> ' & |
                  'Error = ' & self.error & |
                  ' = ' & clip(errorStr) & |
                  ' : ' & clip (temp2) & |
                  ' Function = ' & clip(functionName) & |
                  ' Server = ' & clip (self.q._connection.Server) & |
                  ' Port = ' & clip (self.q._connection.Port), NET:LogError)
      end
    ****
  end

  ! this is a simple error handler. It means every error received interrupts your program
  ! (which you may want to override). The actual message is provided by the
  ! InterpretError method, which can also be overridden.

!------------------------------------------------------------------------------
NetSimpleMultiClient.Kill Procedure  ()
  code
  self.CloseAll()

  ! Moved below the Close, because some objects may try and use self.qServerConnections
  ! when doing a close and if this var is disposed before close() is called a GPF can occur.

  ! De-Allocate Queue structures
  free (self.q)
  if ~self._q&=NULL
    dispose (self._q)
  end

!------------------------------------------------------------------------------
NetSimpleMultiClient.ConnectionClosed Procedure  ()
  code
!---------------------------------------------------------------------------------
! This is a virtual method handled by derivative classes.
! The method is called when either a NetSimple Client or Server looses a connection
!---------------------------------------------------------------------------------
!------------------------------------------------------------------------------
NetSimpleMultiClient.Abort Procedure  ()
  code
  compile ('****', NETTALKLOG=1)
    if self.LoggingOn
      self.Log ('NetSimpleMultiClient.Abort', 'Aborting (immediately closing) Client Socket = ' & self.q._connection.socket & |
                ' SockID = ' & self.q._connection.SockID & |
                ' to Server = ' & clip (self.q._connection.Server) & |
                ' Originally Connected to Port = ' & self.q._connection.Port)
    end
  ****

  self.error = 0
  self.WinSockError = 0
  self.SSLError = 0

  if (self.q.openFlag) or (self.q.AsyncOpenBusy)
    self.error = NetSimpleClose(self.q._connection, 0, 0, -1) ! Abortive Phase Close
    self.q._BetweenPhaseOneAnTwo = true
    self.q._IgnorePhaseTwo = false
    if self.error = 0
      self.q.openFlag = 0
      self.q.AsyncOpenBusy = 0
    else
      self.q.openFlag = 0        ! Hope that because there was an error that the connection has been closed. Jono 17/8/2000
      self.q.AsyncOpenBusy = 0   ! Hope that because there was an error that the connection has been closed. - not a problem as records is deleted
      self.errortrap('Unable to Abort (immediately close) connection', 'NetSimpleMultiClient.Abort')
    end
    delete (self.q)
  else
    compile ('****', NETTALKLOG=1)
      if self.LoggingOn
        self.Log ('NetSimpleMultiClient.Abort', 'Warning. Socket not open. Can not abort.')
      end
    ****
  end

!------------------------------------------------------------------------------
NetSimpleMultiClient.GetInfo Procedure  ()
  code
  ! Doesn't appear to be implemented yet.

!------------------------------------------------------------------------------
NetSimpleMultiClient.CloseAll Procedure  ()
local     group, pre(loc)
x           long
          end

  code
  loop loc:x = 1 to records (self.q)
    get (self.q, loc:x)
    if self.q.openFlag = 1
      self.Close()                                          ! Close connection(s) if open
    end
    if self.q.AsyncOpenBusy = 1
      self.Abort()
    end
  end

!------------------------------------------------------------------------------
NetSimple.SSLGetSubjectField Procedure  (string p_FieldName)
StartPos              long
EndPos                long
  code
  StartPos = Instring ('/' & p_FieldName & '=', self.SSLCertificateOptions.RemoteCertificateSubject, 1, 1)
  if StartPos = 0
    return ''
  end
  StartPos += len (p_FieldName) + 2
  EndPos = Instring ('/', self.SSLCertificateOptions.RemoteCertificateSubject, 1, StartPos)
  if EndPos = 0
    EndPos = len (clip(self.SSLCertificateOptions.RemoteCertificateSubject)) + 1
    loop while EndPos >= 2
      if self.SSLCertificateOptions.RemoteCertificateSubject[EndPos-1] = '<0>'
        EndPos -= 1
      else
        break
      end
    end
  end
  return (Sub(self.SSLCertificateOptions.RemoteCertificateSubject, StartPos, EndPos - StartPos))

!------------------------------------------------------------------------------
NetSimple.SSLGetIssuerField Procedure  (string p_FieldName)
StartPos              long
EndPos                long
  code
  StartPos = Instring ('/' & p_FieldName & '=', self.SSLCertificateOptions.RemoteCertificateIssuer, 1, 1)
  if StartPos = 0
    return ''
  end
  StartPos += len (p_FieldName) + 2
  EndPos = Instring ('/', self.SSLCertificateOptions.RemoteCertificateIssuer, 1, StartPos)
  if EndPos = 0
    EndPos = len (clip(self.SSLCertificateOptions.RemoteCertificateIssuer)) + 1
    loop while EndPos >= 2
      if self.SSLCertificateOptions.RemoteCertificateIssuer[EndPos-1] = '<0>'
        EndPos -= 1
      else
        break
      end
    end
  end
  return (Sub(self.SSLCertificateOptions.RemoteCertificateIssuer, StartPos, EndPos - StartPos))

!------------------------------------------------------------------------------
NetSNMP.LoadMIB      Procedure  (*string mibFile)
sPos            long                ! Current start position for processing and string slicing
ePos            long                ! Current end position
cStart          long                ! Current Line position. Used for processing within a group when sPos or ePos need to retain their value.
cEnd            long                ! Current Line end position, used for finding the end of a substring within the current line
mibLen          long
curLine         string(1024)        ! Store the current line, purely to reduce the amount of string slicing and improve code legibility.
dataType        string(256)         ! The name of the data type
objectName      string(256)         ! The name of the current object (OBJECT-TYPE)
  code
    ! Parameters:
    !     *string mibFile: a string containing the contents of the MIB file. This is processed and
    !                      the results stored in the NetSNMP.qMIBEntries queue.
    ! Return Values:
    !     Returns 0 (zero) for failure and 1 for success. If an error occurs the ErrorTrap method is called
    !     with the detlis of the error
    !
    ! The MIB file defines the data structures use by the SNMP agents, they do not contain any
    ! data themselves, hence the Value field of the queue will be blank for each Object loaded
    ! from the file
    ! Currently this loads each object into the queue, however only ASN.1 simple datatypes
    ! are supported. Sequences are not supported as they cannot current be stored in the queue.

                                                            self.Log('Length of passed string: ' & mibLen, 'NetSNMP.LoadMIB')
    mibLen = Len(Clip(mibFile))

    ! Loop through the file and process each line. Entries where the type are is supported will be processed
    ! those that are unsupported will be ignored, although .ErrorTrap() will be called as notification
    Do NextLine                                             ! Get the next line
                                                            self.Log('Loop through the string and process each line.', 'NetSNMP.LoadMIB')
    loop while sPos
        ! For effeciency the if statement are ordered from most common to least common data structure
        if Instring('OBJECT-TYPE', curLine, 1, 1)           ! An actual Object
                                                            self.Log('Found and OBJECT-TYPE entry.', 'NetSNMP.LoadMIB')
            Clear(self.qMIBEntries)                         ! Prepare the queue for a new addition

            ! The line begins with the object name, followed by the OBJECT_TYPE identifier:
            ! gHandleRow  OBJECT-TYPE
            cEnd = Instring(' ', curLine, 1, 1)
            if not cEnd
                self.MIBError(NetSNMP:InvalidName, sPos)
                Do EndObject                                ! Moves to the end of the object so that processing can continue
                cycle
            end
            objectName = curLine[1 : cEnd - 1]
            if objectName = ''
                self.MIBError(NetSNMP:BlankName, sPos)
                Do EndObject
                cycle
            end

            ! Followed by the TYPE, only ASN.1 simple types are currently supported:
            ! SYNTAX INTEGER
            Do NextLine
            cStart = Instring('SYNTAX', curLine, 1, 1)
            if not cStart
                self.MIBError(NETSNMP:MissingSyntax, sPos)
                Do EndObject
                cycle
            end

            cStart += 7
            cEnd = Len(Clip(curLine))
            if cEnd >= cStart
                self.MIBError(NetSNMP:NoType, sPos)
                Do EndObject
                cycle
            end
            dataType = curLine[cStart : cEnd]
            case Lower(dataType)
            of 'integer'
                self.qMIBEntries.ValueType = NetSNMP:INTEGER
            of 'bit string'
                self.qMIBEntries.ValueType = NetSNMP:BITSTRING
            of 'octet string'
                self.qMIBEntries.ValueType = NetSNMP:STRING
            of 'null'
                self.qMIBEntries.ValueType = NetSNMP:NULL
            of 'object identifier'
                self.qMIBEntries.ValueType = NetSNMP:OBJECTIDENTIFIER
            of 'sequence'
                ! Not supported in this version!
            of 'ipaddress'
                self.qMIBEntries.ValueType = NetSNMP:IpAddress
            of 'counter'
                self.qMIBEntries.ValueType = NetSNMP:Counter
            of 'gauge'
                self.qMIBEntries.ValueType = NetSNMP:Gauge
            of 'timerticks'
                self.qMIBEntries.ValueType = NetSNMP:TimeTicks
            of 'opaque'
                self.qMIBEntries.ValueType = NetSNMP:Opaque
            of 'nsapaddress'
                self.qMIBEntries.ValueType = NetSNMP:NetworkAddress
            else
                self.ErrorTrap('Unsupported OBJECT-TYPE. Object ' & Clip(objectName) & ' of type ' & Clip(dataType) & ' will be ignored.', 'NetSNMP.LoadMIB')
                Do EndObject
                cycle
            end

            !--- ACCESS read-only      - The access permission
            Do NextLine

            !--- STATUS mandatory      - Required, optional etc.
            Do NextLine

            !--- DESCRIPTION           - a multi line desciption field in quotation marks
            ! "The row of the table entry.
            ! This means a valid instance identifier will be reflected as response value.
            ! The mimimum value is 1, the maximum is given by the value of gHandleTableSize."


            !--- ::= { gHandleEntry 1 } - The ObjectID of this object
            Do NextLine

            ! Add this ObjectID along with the ObjectName that was retrieved from the first
            ! line the the self.qDescriptionMap queue.

            ! First find the parent object and get it's ObjectID

            ! Append this object's id and add to the NetSMTP.qDescriptionMap queue
            self.qDescriptionMap.Name = ''                 ! Object ID Number (1.3.6.1.13595.1 etc.)
            self.qDescriptionMap.DescriptionIn = ''        ! The description that matches the ID ('getStatus')


        elsif Instring('OBJECT IDENTIFIER', curLine, 1, 1)  ! An Object Identifier statement
            ! contains the name and ID of the Object. For example the root might be:
            ! emka  OBJECT IDENTIFIER ::= { enterprises 13595 }
            ! which becomes NetSMTP:Enterprise & '.13595' as an object ID  = '1.3.6.1.4.1.13595'

            sPos = Instring('{{', mibFile, 1, 1)                 ! Find the start of the Object ID string
            if not sPos
                return 0
            end

            sPos += 2
            ePos = Instring('}', mibFile[sPos : ePos], 1, sPos) ! Find the end
            if not ePos
                return 0
            end

            ! Add to the NetSMTP.qDescriptionMap queue
            self.qDescriptionMap.Name = ''                 ! Object ID Number (1.3.6.1.13595.1 etc.)
            self.qDescriptionMap.DescriptionIn = ''        ! The description that matches the ID ('getStatus')
        elsif Instring('SEQUENCE', curLine, 1, 1)
            ! The current version checks that the sequence is valid, but ignores it for now (not supported).
            sPos = Instring('{{', mibFile, 1, sPos)
            if not sPos
                self.MIBError(NetSNMP:SequenceStart, sPos)
            else
                ePos = Instring('}', mibFile, 1, sPos)
                if not ePos
                    self.MIBError(NetSNMP:UnterminatedSequence, sPos)
                    return 0
                else
                    self.ErrorTrap('MIB File contains a SEQUENCE statement, which is not supported currently. Processing will continue, but the SEQUENCE will be ignored.', 'NetSNMP.LoadMIB')
                end
            end
        elsif Instring('IMPORTS', curLine, 1, 1)            ! Find the IMPORTS section, not used at the moment, but starts the MIB file.
            if not sPos
                self.MIBError(NetSNMP:NoImports)
                return 0
            end

            sPos = Instring(';', mibFile, 1, sPos)          ! Find the end of the IMPORTS section, which is where the process will start
            if not sPos
                self.MIBError(NetSNMP:ImportsUnterminated)
                return 0
            end

        elsif Instring('TEXTUAL-CONVENTION', mibFile[sPos : ePos], 1, 1)    ! Not supported
            ! STATUS
            ! DESCRIPTION
            ! SYNTAX
            cEnd = Instring('SYNTAX', mibFile, 1, ePos)     ! Find the end of the object
            if not cEnd
                self.MIBError(NetSNMP:UnterminatedTC)
                return 0
            end
        end

        !--- All other lines are simply ignored as blank, comments or unknown.

        Do NextLine
    end

    return 1

! Moves the the end of the object so that process can continue.
EndObject routine
    ePos = Instring('::= {{', mibFile, 1, sPos)
    if not ePos
        self.MIBError(NetSNMP:UnterminatedObject)
    else
        Do NextLine
    end


NextLine routine
    sPos = ePos + 2
    if sPos > mibLen
        sPos = 0
    else
        ePos = Instring('<13,10>', mibFile, 1, sPos)
        if not ePos
            sPos = 0
        else
            curLine = mibFile[sPos : ePos - 1]             ! The current line (excluding any CR,LF pair).
        end
    end
!------------------------------------------------------------------------------
NetSNMP.MIBError     Procedure  (long mibErrorCode, long charPos = 0)
  code
    case mibErrorCode
    of NetSNMP:InvalidName
        self.ErrorTrap('Ignoring Object, could not find the name. There was no space character before OBJECT-TYPE at character ' & charPos, 'NetSNMP.LoadMIB')
    of NetSNMP:BlankName
        self.ErrorTrap('Ignoring object OBJECT-TYPE had a blank name.', 'NetSNMP.LoadMIB')
    of NETSNMP:MissingSyntax
        self.ErrorTrap('Ignoring object. There was no SYNTAX statement for the object', 'NetSNMP.LoadMIB')
    of NetSNMP:NoType
        self.ErrorTrap('Ignoring object. There was no type specified after the SYNTAX statement. Position: ' & charPos, 'NetSNMP.LoadMIB')
    of NetSNMP:UnterminatedObject
        self.ErrorTrap('The OBJECT-TYPE statement was not terminated in the expeced fashion with ::= {{ ObjectName N}. Invalid MIB file', 'NetSNMP.LoadMIB')
    of NetSNMP:SequenceStart
        self.ErrorTrap('Unable to parse SEQUENCE statement in MIB file. There was no {{ to start the SEQUENCE definition. Line starts at character ' & charPos & '.', 'NetSNMP.LoadMIB')
    of NetSNMP:UnterminatedSequence
        self.ErrorTrap('Unterminated SEQUENCE statement. Cannot parse MIB file. At char ' & charPos, 'NetSNMP.LoadMIB')
    of NetSNMP:NoImports
        self.ErrorTrap('Unable to load the MIB file. The IMPORTS section was missing from the file.', 'NetSMTP.LoadMIB')
    of NetSNMP:ImportsUnterminated
        self.ErrorTrap('Unable to load the MIB file. The IMPORTS section was not terminated with a semi colon (;).', 'NetSMTP.LoadMIB')
    of NetSNMP:UnterminatedTC
        self.ErrorTrap('Unable to load MIB file. The TEXTUAL-CONVENTION block at character ' & charPos & ', was not terminated with a SYNTAX statement.', 'NetSNMP.LoadMIB')
    else
        self.ErrorTrap('Unknown MIB file processing error. This error should not occur and indicates and incorrect error code was passed.', 'NetSNMP.MIBError')
    end
!------------------------------------------------------------------------------
NetSimple.HexToLong               Procedure (String p_Hex)
ans   Long
x     Long
  code
  p_hex = left(p_hex)
  loop x = 1 to len(clip(p_hex))
    case p_hex[x]
    of '0' to '9'
      ans = ans * 16 + p_hex[x]
    of 'A' to 'F'
      ans = ans * 16 + val(p_hex[x])-55
    of 'a' to 'f'
      ans = ans * 16 + val(p_hex[x])-87
    else
      break
    end
  end
  Return ans

!------------------------------------------------------------------------------
NetSimple.LongToHex               Procedure (Long p_Long)
hex          string(8)
slon         BYTE,DIM(4),OVER(p_Long)
HexDigitsUp            STRING('0123456789ABCDEF')
HexDigitsLow           STRING('0123456789abcdef')
  CODE
    hex[1] = HexDigitsLow [Bshift(slon[4],-4) + 1]
    hex[2] = HexDigitsLow [Band(slon[4], 0FH) + 1]
    hex[3] = HexDigitsLow [Bshift(slon[3],-4) + 1]
    hex[4] = HexDigitsLow [Band(slon[3], 0FH) + 1]
    hex[5] = HexDigitsLow [Bshift(slon[2],-4) + 1]
    hex[6] = HexDigitsLow [Band(slon[2], 0FH) + 1]
    hex[7] = HexDigitsLow [Bshift(slon[1],-4) + 1]
    hex[8] = HexDigitsLow [Band(slon[1], 0FH) + 1]
  return clip(hex)
!------------------------------------------------------------------------------

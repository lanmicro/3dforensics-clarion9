! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2012 CapeSoft Software  - www.capesoft.com

  Member()

  Omit('EndOmit',_VER_C55)
_ABCDllMode_  EQUATE(0)
_ABCLinkMode_ EQUATE(1)
  EndOmit

    include('Equates.CLW'), once
    include('Keycodes.CLW'), once
    include('Errors.CLW'), once

    map
      Module('WindowsAPI')
        NetSmppDebug(*cstring lpOutputDebugString), raw, pascal, name('OutputDebugStringA')
      end
    end

    include('NetMap.inc'), once
    include('NetSmppBase.inc'), once



!-----------------------------------------------------------
!
! SmppCore class
!
!-----------------------------------------------------------

!!! <summary>
!!!     Outputs the passed string to the system debug output
!!!     after adding a prefix ('[NetSmpp]') to identify it.
!!! </summary>
!!! <returns>Nothing</returns>
SmppCore.Log Procedure (string msg)
w_msg           &cstring
  code
    w_msg &= new cstring(Len(Clip(msg)) + 13)
    w_msg = '[NetSmpp] ' & Clip(msg) & '<13,10>'
    NetSmppDebug(w_msg)
    Dispose(w_msg)
    
    
!-----------------------------------------------------------
!
! UdhMessage class
!
!-----------------------------------------------------------

UdhMessage.Construct Procedure()
  code
  self.message_text &= new StringTheory  
    
    
UdhMessage.Destruct Procedure()
  code
    if not self.message_text &= null
        Dispose(self.message_text)
    end
   

!!! <summary> 
!!!     For UDH Messages, split the message field up into parts
!!! </summary>
!!! <param name="pMsg">A MessagePacketBase object which contains the message to split the UDH formatted
!!!     payload from</param>
!!! <returns>Nothing</returns>
!!! <remarks>The UDH format for concatenated messages is as follows:
!!!
!!! 05 - 1 octet: UDHL Length of the header itself, in octets (05 in this case)
!!!
!!! 00 - 1 octet: IEI - Information Element Identifier, equal to 00 (Concatenated short messages, 8-bit reference number)
!!! 03 - 1 octet: IEL - Information Element Length (03 in this case, indicating three more 
!!!                     byte fields - the message data is not included in this length for concatenation IEs).
!!!
!!! n octets: IED - Information Element Data...
!!!     (All other fields depend on the IEI and the type of UDH. Below are the fields 
!!!      for IEI=0 (concatenated short messages)
!!!
!!! nn - 1 octet: Ref nr = 00-FF, CSMS reference number, must be same for all the SMS parts in the CSMS
!!! nn - 1 octet: Total parts = 00-FF, total number of parts.
!!! nn - 1 octet: This part = 01-FF, the number of this part
!!!
!!! This is followed by the message text. For 7 bit message this is padded onto a 7 bit (septet) boundary.
!!! </remarks>
UdhMessage.Load Procedure(*MessagePacketBase pMsg)
pn              equate('UdhMessage.Load')
pos             long
  code
    self.Reset()                                            ! Clear previous data (if any)
    self.Log(pn)
    if not pMsg.IsUdh()                                     ! A UDH Message, decode the message part into a UDH and data.
        self.Log(pn & '. Error. This is not an UDH message, and cannot be loaded.')
        return false
    elsif pMsg.message.LengthA() < 3
        self.Log(pn & '. Error. The passed Message Packet is too short (' & pMsg.message.LengthA() & ' bytes).')
        return false
    end

    ! Read the header
    pos = 1    
    pMsg.ReadByte(pMsg.message.value, pos, self.udhl)       ! User Data Header Length
    if self.udhl <> 5
        self.Log(pn & '. Error. Invalid use data header length. Length should be 5, length read: ' & self.udhl)
        return false
    end
    
    pMsg.ReadByte(pMsg.message.value, pos, self.iei)        ! Information Element Identifiter

    pMsg.ReadByte(pMsg.message.value, pos, self.iel)        ! Informtation Element Length
    if self.iel <> 3                                        ! The Information Element Length is always 3 for concatenated SMS messages
        self.Log(pn & '. Error, cannot parse the concatenated message UDH. Invalid Information Element ' |
                    & 'Length in the User Data header. Value is ' & self.iel & ' (expected length is 3)')
    end
                                                            self.Log(pn & '. Read User Data Header. UHDL: ' & self.udhl & ', IEI: ' & self.iei & ', IEL: ' & self.IEL)
    if self.iei = SMPP_IE_CONCATENATED
                                                            self.Log(pn & '. Loading the UDH formatted concatenated message. Read the Information Element bytes')
        pMsg.ReadByte(pMsg.message.value, pos, self.refNumber)
        pMsg.ReadByte(pMsg.message.value, pos, self.totalParts)
        pMsg.ReadByte(pMsg.message.value, pos, self.currentPart)
        
        ! The rest of the message contains the message body                
        if pMsg.data_coding =  CODING_ANSI_X3_4             ! csTODO: Add support for 7 bit encoding
            self.Log(pn & '. Warning, 7 bit text encoding is not supported in this release, the message cannot be read!')
        else
            self.Log(pn & '. Read the message body, from byte ' & self.udhl + 2 & ' to byte ' & pMsg.message.LengthA())
        
            self.message_text.SetValue(pMsg.message.Slice(self.udhl + 2))  ! Skip the UDHL byte and the UDH and start at the next byte (the start of the message data)
        end
    else
        self.Log(pn & '. Error, the UDH formatted is not supported by the class and cannot be loaded.')
        return false
    end
  
    return true


!!! <summary>
!!!     Returns the UDH message as a string, which includes the UDH header and the
!!!     message data. This can then be sent as the message part of the PDU.
!!! </summary>
!!! <returns>A string which contains the UDH and message data</summary>
UdhMessage.GetUdhMessage Procedure ()
 code
 ! csTODO: Supports only 8-bit messages for the current release.
     return Chr(5) & Chr(self.iei) & Chr(self.iel) |                            ! IEI header bytes
         & Chr(self.refNumber) & Chr(self.totalParts) & Chr(self.currentPart) | ! IEI bytes
         & self.message_text.GetValue()                                         ! Message content
  
    return ''


!!! <summary>
!!!     Clears all of the properties of the object.
!!! </summary>
!!! <returns>Nothing</string>
UdhMessage.Reset Procedure()
  code
    Clear(self.udhl)
    Clear(self.iei)
    Clear(self.iel)
    Clear(self.refNumber)
    Clear(self.totalParts)
    Clear(self.currentPart)
    self.message_text.Free()


!!! <summary>
!!!     Creates a string which contains the properties and their values for the object.
!!! </summary>
!!! <returns>A string which contains each of the object properties and their values</string>
UdhMessage.ToString Procedure()
  code
    return 'UdhMessage [<13,10>' |
         & '  udhl (UDH length): ' & self.udhl & '<13,10>' |
         & '  iei (Information Element ID): ' & self.iei & '<13,10>' |
         & '  iel (Information Element Length): ' & self.iel & ' bytes<13,10>' |
         & '  refNum: ' & self.refNumber & '<13,10>' |
         & '  totalParts: ' & self.totalParts & '<13,10>' |
         & '  currentPart: ' & self.currentPart & '<13,10>' |
         & '  message_text: ' & self.message_text.GetValue() & '<13,10>' |
         & ']'
 
!-----------------------------------------------------------
!
! PacketBase class
!
!-----------------------------------------------------------
PacketBase.Construct Procedure ()
  code    
    self.sequence_number = self.NextSequenceNumber()
    self.buffer &= new StringTheory()
  

PacketBase.Destruct Procedure ()
  code
    if not self.buffer &= null
        Dispose(self.buffer)
    end
    

!!! <summary>
!!!     Loads from the passed string into this packet object
!!! </summary>
!!! <param name="pby">A string that contains the packet data</param>
!!! <param name="nsz">The number of bytes of data that the packet contains</param>
!!! <returns>True if successful or false otherwise (if the passed length was invalid).</returns>
PacketBase.LoadPacket Procedure (*string pby, long nsz)!, bool, virtual
bi              BInt
pn              equate('PacketBase.LoadPacket')
  code
    self.Log(pn)
    if nsz < 16
        self.Log(pn & '. Error, invalid length (' & nsz & '), packet cannot be loaded! The packet must contain at least 16 bytes.')
        return false
    end
  
    ! commandlen has been stripped off
    
    self.command_id = bi.ReadInt(pby)
    self.command_status = bi.ReadInt(pby, 4)
    self.sequence_number = bi.ReadInt(pby, 8)
    
    return true


!!! <summary>
!!!     Convert the data stored in the object into a packet and store
!!!     it in the internal buffer. Note that this base class method does
!!!     not store the data in the passed parameters (which are used by
!!!     derived methods. This method is not expected to be used except from
!!!     child classes derived methods.
!!! </summary>
!!! <param name="pby">Not used (see note above)</param>
PacketBase.EncodeBody Procedure (*StringTheory pby)!, virtual
body            string(16)
pn              equate('PacketBase.EncodeBody')
bi              BInt
  code
    self.Log(pn)
    bi.WriteInt(body, self.GetCommandLength())
    bi.WriteInt(body, self.command_id, 4)
    bi.WriteInt(body, self.command_status, 8)
    bi.WriteInt(body, self.sequence_number, 12)
    
    self.buffer.AppendA(body)    
  

!!! <summary>Returns the length of the command packet</summary>
!!! <returns>The command length</returns>
PacketBase.GetCommandLength Procedure ()
  code
    return self.GetHeaderLength()


!!! <summary>Returns the length of the command packet</summary>
!!! <returns>The command length</returns>
PacketBase.GetHeaderLength Procedure ()
  code
    return 16
    
    
!!! <summary>Increments and returns the global session sequence number</summary>
!!! <returns>The next sequence number</returns>
PacketBase.NextSequenceNumber Procedure ()
  code
    session_sequence += 1
    return session_sequence


!!! <summary>
!!!     Write an SmppAddress to the buffer. Writes TON and NPI
!!!     as two bytes followed by the address (service short code, 
!!!     international number etc.) and terminated by a null (0)
!!!     character
!!! </summary>
!!! <param name="pAddr">The SMPP address to write to the buffer</param>
!!! <returns>Nothing</returns>
PacketBase.WriteAddress Procedure(*SmppAddress pAddr)
bval            byte
sb              string(1), over(bval)
  code
 
    self.WriteByte(pAddr.ton)
    self.WriteByte(pAddr.npi)
    self.buffer.AppendA(pAddr.addr)
    self.WriteNull()
    
!!! <summary>
!!!     Writes a single byte to the buffer. Truncates the passed
!!!     long and writes the lowest order byte.
!!! </summary>
!!! <param name="ival">An unsigned integer value to convert to a byte 
!!!     and write to the packet buffer</param>
!!! <returns>Nothing</returns>
PacketBase.WriteByte Procedure (long ival)
bval            byte
sb              string(1), over(bval)
  code    
    bval = ival                                             ! truncate to byte
    self.buffer.AppendA(sb)                                 ! Write the byte value to the buffer  

!!! <summary>
!!!     Writes a null (0) to the buffer
!!! </summary>
!!! <returns>Nothing</returns>
PacketBase.WriteNull Procedure ()
  code
    self.buffer.AppendA('<0>')

!!! <summary>
!!!     Retrieves a null terminated string from the position specified by
!!!     the pos parameter and copies it into the dest parameter. Pos is
!!!     then incremented by the length of the copied string to the next
!!!     field position in the packet.
!!! </summary>
!!! <param name="pby">An SMPP packet to retrieve the string field from</param>
!!! <param name="pos">The position that the null terminated string starts at
!!!     in the passed pby string. On return this is incremented to the start
!!!     of the next field in the packet.</param>
!!! <param name="dest_field">The destination field to copy the data into</param>
!!! <returns>Nothing</returns>
!!! <remarks>This method cannot be used to read binary data, or data from fields that 
!!!     are not null terminated. Use the alternative ReadString method which stores
!!!     the read data in a StringTheory object and allows the number of bytes read
!!!     to be specified</remarks>
PacketBase.ReadString Procedure (*string pby, *long pos, *cstring dest_field)
cur         &cstring
pn          equate('PacketBase.ReadString')
  code
                                                            self.Log(pn & '. Buffer at ' & Address(pby) & ', pos: ' & pos & ', read from: ' & (Address(pby) + pos - 1))
    if pos > Len(pby)
        self.Log('The passed position to read from is greater than the string length and is invalid ')
        return
    end

    cur &= (Address(pby) + pos - 1)
    dest_field = cur    
                                                            self.Log(pn & '. Length of string read: ' & Len(cur))
    pos += Len(cur) + 1                                     ! Increment past the null terminator


!!! <summary>
!!!     An alternative to the ReadString method which reads the data into a cstring.
!!!     This method reads the data into a StringTheory object and consequently can 
!!!     handle non null terminated fields, fixed length fields and binary data, as
!!!     well as cstrings (null terminated fields)
!!! </summary>
!!! <param name="pby">An SMPP packet to retrieve the string field from</param>
!!! <param name="pos">The position that the null terminated string starts at
!!!     in the passed pby string. On return this is incremented to the start
!!!     of the next field in the packet.</param>
!!! <param name="dest_field">The destination field to copy the data into</param>
!!! <param name="fLen">Optional parameter which specifies the number of bytes to read.
!!!     use when reading binary data, non null terminated fields, fixed length fields
!!!     and so on. See the Remarks.</params>
!!! <returns>Nothing</returns>
!!! <remarks>This method can be used to read binary data, fixed length fields, or 
!!!     data from fields that  are not null terminated. The fLen parameter should be 
!!!     used to specify thenumber of bytes to read in these cases</remarks>
PacketBase.ReadString Procedure (*string pby, *long pos, *StringTheory dest_field, ulong fLen=0)
cur         &cstring
pn          equate('PacketBase.ReadString.2')
  code
  self.Log(pn)
  if pos > Len(pby)
      self.Log(pn & '. Warning: The passed position to read from is greater than the string length and is invalid ')
      return
  end

    if fLen                                                 ! Byte length passed (for example for binary or non null terminated strings)
        if pos + fLen - 1> Len(pby)
            self.Log(pn & '. Warning: Reading passed length (' & fLen & ') of data from byte ' & pos & ' exceeds the length of the buffer (' & Len(pby) & '). Retrieving data to the end of the buffer.')
            dest_field.SetValue(pby[pos : Len(pby)])
        else
            dest_field.SetValue(pby[pos : pos + fLen - 1])
        end
        pos += dest_field.LengthA()                         ! increment by the field length
    else
        cur &= (Address(pby) + pos - 1)
        dest_field.SetValue(cur)
        pos += dest_field.LengthA() + 1                     ! increment past the null terminator
    end
                                                            self.Log(pn & '. Length of string read: ' & dest_field.LengthA())

!!! <summary>
!!!     Retrieves a BYTE (8 bit octet) from the position specified by
!!!     the pos parameter and copies it into the dest_field parameter. Pos is
!!!     then incremented.
!!! </summary>
!!! <param name="pby">An SMPP packet to retrieve the octet from</param>
!!! <param name="pos">The position in the packet to retrieve the byte value from</param>
!!! <param name="dest_field">The destination field to copy the data into</param>
!!! <returns>Nothing</returns>
PacketBase.ReadByte Procedure (*string pby, *long pos, *long dest_field)
  code
    dest_field = Val(pby[pos])
    pos += 1


!!! <summary>
!!!     Retrieves a SMPP Address from the position specified by
!!!     the pos parameter and copies it into the dest_field parameter. Pos is
!!!     then incremented to the next field position in the packet.
!!! </summary>
!!! <param name="pby">An SMPP packet to retrieve the Address from</param>
!!! <param name="pos">The position in the packet to retrieve the Address value from</param>
!!! <param name="dest_field">The destination field to copy the data into</param>
!!! <returns>Nothing</returns>
PacketBase.ReadAddress Procedure (*string pby, *long pos, *SmppAddress dest_addr)
pn              equate('PacketBase.ReadAddress')    
  code
                                                            self.Log(pn & '. Read Address from byte ' & pos)
    self.ReadByte(pby, pos, dest_addr.ton)
    self.ReadByte(pby, pos, dest_addr.npi)
                                                            self.Log(pn & 'ton: ' & dest_addr.ton & ', npi: ' & dest_addr.npi)
    self.ReadString(pby, pos, dest_addr.addr)     


!!! <summary>Display the data contained in this packet as a human readable string</summary>    
!!! <returns>A string which contains the names and values on each property of the object</returns>
PacketBase.ToString Procedure ()
st              StringTheory
  code
    return '  PacketBase [<13,10>' |
         & '    command_length: ' & self.GetCommandLength() & ' (' & st.LongToHex(self.command_length) & ')<13,10>' |
         & '    command_id: ' & self.command_id & ' (' & st.LongToHex(self.command_id) & ')<13,10>' |
         & '    command_status: ' & self.command_status & ' (' & st.LongToHex(self.command_status) & ')<13,10>' |
         & '    sequence_number: ' & self.sequence_number & ' (' & st.LongToHex(self.sequence_number) & ')<13,10>' |
         & '  ]'
    
!-----------------------------------------------------------
!
! BindPacketBase class
!
!-----------------------------------------------------------
BindPacketBase.Construct Procedure()
  code
    self.address_range &= new SmppAddress
    self.interface_version = SMPP_INTERFACE_VERSION
    
    
BindPacketBase.Destruct Procedure()
  code
    Dispose(self.address_range)
    

!!! <summary>
!!!     Convert the data stored in the object into a packet and store
!!!     the packet data in the passed string.
!!! </summary>
!!! <param name="pby">A pointer to a string to store the passed packet data in. The
!!!     data should be treated as binary</param>
!!! <param name="nsz">A pointer to an integer to store the length of the packet created in (in bytes)</param> 
BindPacketBase.EncodeBody Procedure (*StringTheory pby)!, virtual
pn              equate('BindPacketBase.EncodeBody')
  code
 
    parent.EncodeBody(pby)

    self.buffer.AppendA(self.system_id)
    self.WriteNull()
  
    self.buffer.AppendA(self.password)
    self.WriteNull()
  
    self.buffer.AppendA(self.system_type)
    self.WriteNull()
    
    self.WriteByte(self.interface_version)
    
    self.WriteAddress(self.address_range)

    !nsz = self.buffer.LengthA()   
    pby.SetValue(self.buffer.GetValue())

    self.Log(pn & '. Encoded body length: ' & pby.LengthA() & '. Command length: ' & self.GetCommandLength())
    Assert(self.GetCommandLength() = pby.LengthA(), pn & ' - Error, the length of the packet created is not equal to the length of the data stored!')


!!! <summary>
!!!     Loads from the passed string into this packet object
!!! </summary>
!!! <param name="pby">A string that contains the packet data</param>
!!! <param name="nsz">The number of bytes of data that the packet contains</param>
!!! <returns>True if successful or false otherwise (if the passed length was invalid).</returns> 
BindPacketBase.LoadPacket Procedure (*string pby, long nsz)!, bool, virtual
pn              equate('BindPacketBase.LoadPacket')
pos             long, auto
  code
    self.Log(pn)
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        return false
    end

    pos = 13
    
    self.ReadString(pby, pos, self.system_id)
    self.ReadString(pby, pos, self.password)
    self.ReadString(pby, pos, self.system_id)
  
    self.ReadByte(pby, pos, self.interface_version)
    self.ReadAddress(pby, pos, self.address_range)    
                                                                
    return true


!!! <summary>Retrieve the length of the SMPP packet data</summary>
!!! <returns>The packet length in bytes</summary>
BindPacketBase.GetCommandLength Procedure ()
  code
    self.Log('BindPacketBase.GetCommandLength. sys_id: ' & Len(self.system_id) & ', pwd: ' &  Len(self.password) |
            & ', type: ' & Len(self.system_type) & ', addr: ' & self.address_range.GetLength())
            
    return parent.GetCommandLength() |
         + Len(self.system_id) |
         + Len(self.password) |
         + Len(self.system_type) |
         + self.address_range.GetLength() |
         + 1 + 3


!!! <summary>Sets the address_range property to the passed address.</summary>
!!! <param name="addr">An SmtpAddress object which contains the address to store</param>
!!! <returns>Nothing</summary>
BindPacketBase.SetSourceRange Procedure (*SmppAddress addr)
  code
    self.address_range.Equals(addr)



!!! <summary>Display the data contained in this packet as a human readable string</summary>
!!! <returns>A string which contains the names and values on each property of the object</returns>
BindPacketBase.ToString Procedure ()
st              StringTheory
  code
 
    return 'BindPacketBase [<13,10>' |
         &  parent.ToString() & '<13,10>' |
         & '  system_id: ' & self.system_id & ' [' & Len(self.system_id) & ' bytes]<13,10>' |
         & '  password: ' & All('*', Len(self.password)) & ' [ ' & Len(self.password) & ' bytes]<13,10>' |
         & '  system_type: ' & self.system_type & ' [ ' & Len(self.system_type) & ' bytes]<13,10>' |
         & '  interface_version: ' & self.interface_version & ' (' & st.LongToHex(self.interface_version) & ')<13,10>' |
         & '  address_range: ' & self.address_range.addr & ' (TON: ' & self.address_range.ton & ', NPI: ' & self.address_range.npi & ')<13,10>' |
         & ']'


!-----------------------------------------------------------
!
! MessagePacketBase Class
!
!-----------------------------------------------------------

MessagePacketBase.Construct Procedure()
  code
    self.source &= new SmppAddress
    self.destination &= new SmppAddress
    
    self.scheduled_delivery &= new SmppDate
    self.validity_period &= new SmppDate

    self.message &= new StringTheory
  

MessagePacketBase.Destruct Procedure()
  code
    Dispose(self.source)
    Dispose(self.destination)
    Dispose(self.scheduled_delivery)
    Dispose(self.validity_period)
    
    Dispose(self.message)


!!! <summary>Retrieves the stored message value. Truncates data to the length
!!!     of the passed string if neccessary</summary>
!!! <param name="msg">A string to store the message in</msg>
!!! <param name="nsz">Size of the message returned</param>
MessagePacketBase.GetMessage Procedure (*string msg, *long nsz)
  code
  
    if self.message.LengthA() > Len(msg)            ! Truncate
        nsz = Len(msg)
        msg = self.message.Sub(1, nsz)        
    else
        msg = self.message.GetValue()
        nsz = self.message.LengthA()
    end

!!! <summary>Sets the message to the passed string</summary>
!!! <param name="msg">A string which contains the message</param>
!!! <param name="nsz">The length of the message to store. If this is
!!!     zero or greater than the length of the passed string then
!!!     the length of the string is used</param>
!!! <returns>Nothing</returns>
MessagePacketBase.SetMessage Procedure (string msg, long nsz=0)  
  code
    if nsz <= 0 or nsz > Len(msg)
        nsz = Len(Clip(msg))
    end
    self.message.SetValue(msg[1 : nsz])

!!! <summary>Sets the destination address</summary>
!!! <returns>Nothing</returns>
MessagePacketBase.SetDestination Procedure (*SmppAddress dst)
  code
    self.destination.Equals(dst)
  

!!! <summary>Sets the source address</summary>
!!! <returns>Nothing</returns>
MessagePacketBase.SetSource Procedure (*SmppAddress src)
  code
    self.source.Equals(src)
    

!!! <summary>Sets the data coding</summary>    
!!! <returns>Nothing</returns>
MessagePacketBase.SetDataCoding Procedure (long data_coding)
  code
    self.data_coding = data_coding
    

!!! <summary>Sets the ESM Class</summary>
!!! <>
!!! <returns>Nothing</returns>
MessagePacketBase.SetEsmClass Procedure (long esm_class)
  code
    self.esm_class = esm_class



!!! <summary>Sets an attribute of the ESM class</summary>
!!! <param name="bVal">The value to set the attribute of the esm_class to</param>
!!! <param name="attribute">An equate which indicates which attribute is being set:
!!!     CLASS_MESSAGE_MODE          equate(0)
!!!     CLASS_MESSAGE_TYPE          equate(1)
!!!     CLASS_ANSI41                equate(2)
!!!     CLASS_GSM                   equate(3)
!!! </param>
!!! <remarks>
!!!     The ESM class is made up of 4 sections, which specify
!!!     The message mode, message type, ANSI 41 specific values and GSM specific values
!!!     respectively. The possible values for each section are listed below:
!!!
!!!     Message Mode (bits 1-0)     CLASS_MESSAGE_MODE
!!!     CLASS_DEFAULT               equate(000000000b)          ! Default MC Mode (e.g. Store and Forward)
!!!     CLASS_DATAGRAM              equate(000000001b)          ! Datagram mode
!!!     CLASS_FORWARD               equate(000000010b)          ! Forward (i.e. Transaction) mode
!!!     CLASS_STORE_FORWARD         equate(000000011b)          ! Store and Forward mode (use to select Store and Forward mode if Default MC Mode is non Store and Forward)
!!!     
!!!     ! Message Type (bits 2 and 5) CLASS_MESSAGE_TYPE
!!!     CLASS_DEFAULT_TYPE          equate(000000000b)          ! Default message Type (i.e. normal message)
!!!     CLASS_DELIVERY_RECEIPT      equate(000000100b)          ! Short Message contains MC Delivery Receipt
!!!     CLASS_INTERMEDIATE          equate(000100000b)          ! Short Message contains Intermediate Delivery Notification
!!!     
!!!     ! ANSI-41 specific (bits 5-2) CLASS_ANSI41
!!!     CLASS_DELIVERY_ACK          equate(000001000b)          ! Short Message contains Delivery Acknowledgement
!!!     CLASS_USER_ACK              equate(000010000b)          ! Short Message contains Manual/User Acknowledgement
!!!     CLASS_CONVERSATION_ABORT    equate(000011000b)          ! Short Message contains Conversation Abort (Korean CDMA)
!!!     
!!!     ! GSM Specific (bits 7-6)   CLASS_GSM
!!!     CLASS_NONE                  equate(000000000b)          ! No specific features selected
!!!     CLASS_UDH                   equate(001000000b)          ! UDH Indicator
!!!     CLASS_SET_REPLY_PATH        equate(010000000b)          ! Set Reply Path (only relevant for GSM network)
!!!     CLASS_UDHI_REPLY_PATH       equate(011000000b)          ! Set UDHI and Reply Path (only relevant for GSM network)
!!! </remarks>
!!! <returns>Nothing</returns>
MessagePacketbase.SetEsmClassAttrib Procedure (long bVal, long attrib)
  code
    self.esm_class = Band(self.esm_class, bVal)



!!! <summary>Returns an attribute of the ESM class. The ESM class is a bit mask</summary>
!!! <remarks>See the SetEmsClassAttrib method for a description of the attributes and values</remarks>
!!! <returns>The value of the specified attribute, or -1 if the passed attrib parameter is
!!!   not valid</returns>
MessagePacketbase.GetEsmClassAttrib Procedure (long attrib)
  code
    case attrib
    of CLASS_MESSAGE_MODE
        return Band(self.esm_class, 000000011b)
    of CLASS_MESSAGE_TYPE
        return Band(self.esm_class, 000100100b)
    of CLASS_ANSI41
        return Band(self.esm_class, 000111100b)
    of CLASS_GSM
        return Band(self.esm_class, 011000011b)
    else
        return -1
    end

  
!!! <summary>Converts the passed value into a string describing that value for a particular
!!!     attribute of the esm_class.
!!! </summary>
!!! <param name="attrib">An equate for the attribute that this value is for: CLASS_MESSAGE_MODE,
!!!     CLASS_MESSAGE_TYPE, CLASS_ANSI41 or CLASS_GSM </param>
!!! <param name="attrib_val">The value of the attribute</param>
!!! <returns>A string which contains a description (meaning) of the value</returns>
MessagePacketBase.EsmAttribToString Procedure(long attrib, long attrib_val)  
  code
    case attrib
    of CLASS_MESSAGE_MODE
        case attrib_val
        of CLASS_DEFAULT  
            return 'Default MC Mode (e.g. Store and Forward)'
        of CLASS_DATAGRAM  
            return 'Datagram mode'
        of CLASS_FORWARD         
            return 'Forward (i.e. Transaction) mode'
        of CLASS_STORE_FORWARD   
            return 'Store and Forward mode (use to select Store and Forward mode if Default MC Mode is non Store and Forward)'
        end        
    of CLASS_MESSAGE_TYPE
        case attrib_val
        of CLASS_DEFAULT_TYPE          
            return 'Default message Type (i.e. normal message)'
        of CLASS_DELIVERY_RECEIPT      
            return 'Short Message contains MC Delivery Receipt'
        of CLASS_INTERMEDIATE          
            return 'Short Message contains Intermediate Delivery Notification'
        end
    of CLASS_ANSI41
        case attrib_val
        of CLASS_DELIVERY_ACK          
            return 'Short Message contains Delivery Acknowledgement'
        of CLASS_USER_ACK              
            return 'Short Message contains Manual/User Acknowledgement'
        of CLASS_CONVERSATION_ABORT    
            return 'Short Message contains Conversation Abort (Korean CDMA)'
        end
    of CLASS_GSM
        case attrib_val
        of CLASS_NONE                  
            return 'No specific features selected'
        of CLASS_UDHI                  
            return 'UDH Indicator'
        of CLASS_SET_REPLY_PATH        
            return 'Reply Path (only relevant for GSM network)'
        of CLASS_UDHI_REPLY_PATH       
            return 'UDHI and Reply Path (only relevant for GSM network)'
        end
    end
    return ''


!!! <summary>Returns a string with a values and descriptions 
!!!     of the esm_class attributes
!!! </summary>
MessagePacketBase.EsmClassToString Procedure()
eMode           long
eType           long
eAnsi           long
eGsm            long
pn              equate('MessagePacketBase.EsmClassToString')
  code
    self.Log(pn)
    eMode = self.GetEsmClassAttrib(CLASS_MESSAGE_MODE)    
    eType = self.GetEsmClassAttrib(CLASS_MESSAGE_TYPE)
    eAnsi = self.GetEsmClassAttrib(CLASS_ANSI41)
    eGsm =  self.GetEsmClassAttrib(CLASS_GSM)
                                                            self.Log(pn & ' mode: ' & eMode & ', type: ' & eType & ', ansi 41: ' & eAnsi & ', gsm: ' & eGsm)
    return 'Message Mode (' & eMode & '): '  & self.EsmAttribToString(CLASS_MESSAGE_MODE, eMode) & '<13,10>' |
         & 'Message Type (' & eType & '): ' & self.EsmAttribToString(CLASS_MESSAGE_TYPE, eType) & '<13,10>' |
         & 'ANSI41 Specific (' & eAnsi & '): ' & self.EsmAttribToString(CLASS_ANSI41, eAnsi) & '<13,10>' |
         & 'GSM Specific (' & eGsm & '): ' & self.EsmAttribToString(CLASS_GSM, eGsm)


!!! <summary>
!!!     Converts the data_coding value into a string describing the encoding.
!!! <summary>
!!! <returns>A string which contains a description of the current data_coding value</returns>
!!! <remarks>! Some of the more common encodings are::
!!!    ASCII  (001h)    7 bit, max 160 chars
!!!    LATIN1 (003h)    8 bit, max 140 chars
!!!    UCS2   (008h)    8 bit, max 70
!!! </remarks>
MessagePacketBase.DataCodingToString Procedure()
  code
    case self.data_coding
    of CODING_DEFAULT     
        return '(0) LATIN1, 8 bit, 140 chars max. Default Alphabet'
    of CODING_ANSI_X3_4   
        return '(1) ASCII, 7 bit, 160 chars max. IA5 (CCITT T.50)/ASCII (ANSI X3.4)'
    of CODING_OCTET1      
        return '(2) Binary, 8 bit, 140 chars max. Octet unspecified (8-bit binary)'
    of CODING_ISO8859_1   
        return '(3) LATIN1, 8 bit, 140 chars max. Latin 1 (ISO-8859-1)'
    of CODING_OCTET2      
        return '(4) Binary, 8 bit, 140 chars max. Octet unspecified (8-bit binary)'
    of CODING_JIS         
        return '(5) 8 bit octets stored. JIS (X 0208-1990)'
    of CODING_CYRLLIC     
        return '(6) 8 bit octets stored. Cyrllic (ISO-8859-5)'
    of CODING_LATIN       
        return '(7) LATIN, 8 bit. Latin/Hebrew (ISO-8859-8)'
    of CODING_UCS2        
        return '(8) Unicode, 16 bit. UCS2 (ISO/IEC-10646)'
    of CODING_PICTOGRAM   
        return '(9) 8 bit octets stored. Pictogram Encoding'
    of CODING_ISO_2200_JP 
        return '(10) 8 bit octets stored. ISO-2200-JP'
    else
        return 'Unknown/Unsupported (' & self.data_coding & ')'
    end
  


!!! <summary>
!!!     Convert the data stored in the object into a packet and store
!!!     the packet data in the passed string.
!!! </summary>
!!! <param name="pby">A pointer to a string to store the passed packet data in. The
!!!     data should be treated as binary</param>
!!! <param name="nsz">A pointer to an integer to store the length of the packet created in (in bytes)</param>
!!! <returns>Nothing</returns>
MessagePacketBase.EncodeBody Procedure (*StringTheory pby)
pn              equate('MessagePacketBase.EncodeBody')
  code
    parent.EncodeBody(pby)    
                                                            self.Log(pn & '. Written header bytes: ' & self.buffer.LengthA())
    self.buffer.AppendA(self.service_type)
    self.WriteNull()

    self.WriteAddress(self.source)
    self.WriteAddress(self.destination)
    
    self.WriteByte(self.esm_class)
    self.WriteByte(self.protocol_id)
    self.WriteByte(self.priority_flag)
    
    self.buffer.AppendA(self.scheduled_delivery.ToString())
    self.WriteNull()
    
    self.buffer.AppendA(self.validity_period.ToString())
    self.WriteNull()
    
    self.WriteByte(self.registered_delivery)
    self.WriteByte(self.replace_if_present)

    self.WriteByte(self.data_coding)
    self.WriteByte(self.default_msg_id)
    
    self.WriteByte(self.message.LengthA())
    if self.message.LengthA() > 0
        self.buffer.AppendA(self.message.GetValue())
    end

    pby.SetValue(self.buffer.GetValue())

    Assert(self.GetCommandLength() = pby.LengthA(), 'MessagePacketBase.EncodeBody - Error, the length of the packet created is not equal to the length of the data stored!')

!!! <summary>
!!!     Loads from the passed string into this packet object
!!! </summary>
!!! <param name="pby">A string that contains the packet data</param>
!!! <param name="nsz">The number of bytes of data that the packet contains</param>
!!! <returns>True if successful or false otherwise (if the passed length was invalid).</returns>    
MessagePacketBase.LoadPacket Procedure (*string pby, long nsz)
pn              equate('MessagePacketBase.LoadPacket')
pos             long, auto
smLength        long, auto
delivery        cstring(256)
valid           cstring(256)

  code    
    self.Log(pn)
    
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        self.Log(pn & '. Packet command status indicates an error: ' & self.command_status & '. Packet will not be processed.')
        return false
    end
    self.Log(pn & '. After parent call. Set pos to byte 13 and process ')
    
    pos = 13                                                ! Start at byte 13

    ! Service type 
    self.ReadString(pby, pos, self.service_type)
                                                            self.Log(pn & '. Service type: ' & self.service_type & ', pos: ' & pos) 
    self.ReadAddress(pby, pos, self.source)
                                                            self.Log(pn & '. Source address: ' & self.source.addr & ' [ton: ' & self.source.ton & ', npi: ' & self.source.npi & '], pos: ' & pos)
    self.ReadAddress(pby, pos, self.destination)
                                                            self.Log(pn & '. Destination address: ' & self.destination.addr & ' [ton: ' & self.source.ton & ', npi: ' & self.source.npi & '], pos: '&  pos)
    ! ESM class, protocol Id, priorityFlag
                                                            self.Log(pn & '. Read esm_class from byte ' & pos)
    self.ReadByte(pby, pos, self.esm_class)
    
    ! Check the ESM Class    
                                                            self.Log(pn & '. ESM Class [' & self.esm_class & ']: ' & self.EsmClassToString())
                                                            self.Log(pn & '. Read protocol_id from byte ' & pos)
    self.ReadByte(pby, pos, self.protocol_id)
                                                            self.Log(pn & '. Read priority_flag from byte ' & pos)
    self.ReadByte(pby, pos, self.priority_flag)
    
                                                            self.Log(pn & '. Read delivery from byte ' & pos)
    ! Scheduled delivery and Validity period  
    self.ReadString(pby, pos, delivery)
                                                            self.Log(pn & '. Read validity from byte ' & pos)
    self.ReadString(pby, pos, valid)    
   

    if delivery <> ''
        self.scheduled_delivery.SetDate(delivery)
    end                                                            
    if valid <> ''
        self.validity_period.SetDate(valid)
    end
                                                            self.Log(pn & '. Read four byte flags starting from byte ' & pos)
    ! Registered delivery, replace if present, data coding, 
    ! default msg and message length
    self.ReadByte(pby, pos, self.registered_delivery)
    self.ReadByte(pby, pos, self.replace_if_present)
    self.ReadByte(pby, pos, self.data_coding)
                                                            self.Log(pn & '. data_coding: ' & self.DataCodingToString())
    self.ReadByte(pby, pos, self.default_msg_id)    
                                                            self.Log(pn & '. Read Message length from byte ' & pos)
    self.ReadByte(pby, pos, smLength)                       ! Note that for 7 bit encoded messages this is the number of septets, NOT the number of octets!
                                                            self.Log(pn & '. Total Message Data Length: ' & smLength)
    ! smLength now contains the User Data Length (UDL), which is the total length 
    ! of the data after this octet. For UDH messages this is UDHL + UDH + text. 
    ! For text message this is just the length of the text message.
    ! For 7 bit encoded text this is the number of septets, NOT octets. For UDH messages
    ! The UDH which is included in that length is actually octets with padding 
    ! onto a septet boundary.
    if smLength > 0       
        if self.data_coding =  CODING_ANSI_X3_4             ! 7 bit encoding, just display a warning
            self.Log(pn & '. 7-bit encoding is used, this is not supported yet!')
            ! Calculate the octet length and read the data in.
            ! Decoding the 7-bit packed text is not currently supported
            
            ! Decoding needs to be handled differently 
        else
            self.Log(pn & '. 8 bit encoding is used')            
        end

                                                            self.Log(pn & '. Read the message body (' & smLength & ' bytes) from byte: ' & pos)
        self.ReadString(pby, pos, self.message, smLength)
        ! Note: For UDH messages the UdhMessage class should be used to 
        ! split this message up
    end
                                                            self.Log(self.ToString())
    return true
    


!!! <summary>
!!!     Returns True if a message is a UDH message (the message contains
!!!     a UDH header and fields and the esm_class has the UDHI flag set).
!!! </summary>
MessagePacketBase.IsUdh Procedure ()
  code
    case self.GetEsmClassAttrib(CLASS_GSM)                  ! Get the value
    of CLASS_UDHI orof CLASS_UDHI_REPLY_PATH                ! UDH Indicator is set
        return true
    else
        return false
    end


!!! <summary>Retrieve the length of the SMPP packet data</summary>
!!! <returns>The packet length in bytes</summary>
MessagePacketBase.GetCommandLength Procedure ()
  code 
    return parent.GetCommandLength() |
         + Len(self.service_type) |
         + self.source.GetLength() |
         + self.destination.GetLength() |
         + self.scheduled_delivery.GetLength() |
         + self.validity_period.GetLength() |
         + self.message.LengthA() |   
         + 8 + 3                ! 8 byte values plus 3 added null terminators

!!! <summary>Display the data contained in this packet as a human readable string</summary>
!!! <returns>A string which contains the names and values on each property of the object</returns>
MessagePacketBase.ToString Procedure ()
st              StringTheory
  code
    return 'MessagePacketBase [<13,10>' |
            & parent.ToString() & '<13,10>' |
            & '  service_type: ' & self.service_type & '<13,10>' |
            & '  source: ' & self.source.addr & ' (TON: ' & self.source.ton & ', NPI: ' & self.source.npi & ')<13,10>' |
            & '  destination: ' & self.destination.addr & ' (TON: ' & self.destination.ton & ', NPI: ' & self.destination.npi & ')<13,10>' |
            & '  esm_class: ' & self.esm_class & ' (' & st.LongToHex(self.esm_class) & ')<13,10>' |                        
            & '  protocol_id: ' & self.protocol_id & ' (' & st.LongToHex(self.protocol_id) & ')<13,10>' |
            & '  priority_flag: ' & self.priority_flag & ' (' & st.LongToHex(self.priority_flag) & ')<13,10>' |         
            & '  scheduled_delivery: ' & self.scheduled_delivery.ToString() & '<13,10>' |
            & '  validity_period: ' & self.validity_period.ToString() & '<13,10>' |         
            & '  registered_delivery: ' & self.registered_delivery & '<13,10>' |
            & '  replace_if_present: ' & self.replace_if_present & '<13,10>' |
            & '  data_coding: ' & self.data_coding & '<13,10>' |
            & '  default_msg_id: ' & self.default_msg_id & '<13,10>' |
            & '  message length: ' & self.message.LengthA() & '<13,10>' |
            & '  message: ' & self.message.GetValue() & '<13,10>' |
            & ']'

!-----------------------------------------------------------    
!
! DataPacketBase Class
!
!-----------------------------------------------------------    

!!! <summary>
!!!     Convert the data stored in the object into a packet and store
!!!     the packet data in the passed string.
!!! </summary>
!!! <param name="pby">A pointer to a string to store the passed packet data in. The
!!!     data should be treated as binary</param>
!!! <param name="nsz">A pointer to an integer to store the length of the packet created in (in bytes)</param>
DataPacketBase.EncodeBody Procedure (*StringTheory pby)!, virtual
  code
    parent.EncodeBody(pby)

    self.buffer.AppendA(self.service_type)
    self.WriteNull()

    self.WriteAddress(self.source)
    self.WriteAddress(self.destination)
    
    self.WriteByte(self.esm_class)

    self.WriteByte(self.registered_delivery)
    self.WriteByte(self.data_coding)

    !nsz = self.buffer.LengthA()   
    pby.SetValue(self.buffer.GetValue())

    Assert(self.GetCommandLength() = pby.LengthA(), 'DataPacketBase.EncodeBody - Error, the length ' |
           & 'of the packet created is not equal to the length of the data stored!')

!!! <summary>
!!!     Loads from the passed string into this packet object
!!! </summary>
!!! <param name="pby">A string that contains the packet data</param>
!!! <param name="nsz">The number of bytes of data that the packet contains</param>
!!! <returns>True if successful or false otherwise (if the passed length was invalid).</returns>
DataPacketBase.LoadPacket Procedure (*string pby, long nsz)!, bool, virtual
pn              equate('DataPacketBase.LoadPacket')
pos             long, auto
  code
    self.Log(pn)
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        return false
    end
    
    pos = 13

    ! Service type 
    self.ReadString(pby, pos, self.service_type)
  
    self.ReadAddress(pby, pos, self.source)
    self.ReadAddress(pby, pos, self.destination)

    self.ReadByte(pby, pos, self.esm_class)
    self.ReadByte(pby, pos, self.registered_delivery)
    self.ReadByte(pby, pos, self.data_coding)
  
    return true


DataPacketBase.GetCommandLength Procedure ()!, long, virtual
  code
    return parent.GetCommandLength() |
         + Len(self.service_type) |
         + self.source.GetLength() |
         + self.destination.GetLength() |
         + 1 + 3
         

!!! <summary>Display the data contained in this packet as a human readable string</summary>
!!! <returns>A string which contains the names and values on each property of the object</returns>
DataPacketBase.ToString Procedure ()
st              StringTheory
  code
    return 'DataPacketBase [<13,10>' |
            & parent.ToString() & '<13,10>' |
            & '  service_type: ' & self.service_type & '<13,10>' |
            & '  source: ' & self.source.addr & ' (TON: ' & self.source.ton & ', NPI: ' & self.source.npi & ')<13,10>' |
            & '  destination: ' & self.destination.addr & ' (TON: ' & self.destination.ton & ', NPI: ' & self.destination.npi & ')<13,10>' |
            & '  esm_class: ' & self.esm_class & ' (' & st.LongToHex(self.esm_class) & ')<13,10>' |
            & '  registered_delivery: ' & self.registered_delivery & '<13,10>' |
            & '  data_coding: ' & self.data_coding & '<13,10>' |
            & ']'

!-----------------------------------------------------------    
!
! DataPacketRespBase Class
!
!-----------------------------------------------------------    


!!! <summary>
!!!     Convert the data stored in the object into a packet and store
!!!     the packet data in the passed string.
!!! </summary>
!!! <param name="pby">A pointer to a string to store the passed packet data in. The
!!!     data should be treated as binary</param>
!!! <param name="nsz">A pointer to an integer to store the length of the packet created in (in bytes)</param> 
DataPacketRespBase.EncodeBody Procedure (*StringTheory pby)!, virtual
  code
  
    parent.EncodeBody(pby)
    self.buffer.AppendA(self.message_id)
    self.WriteNull()
    
    pby.SetValue(self.buffer.GetValue())

    Assert(self.GetCommandLength() = pby.LengthA(), 'DataPacketRespBase.EncodeBody - Error, the length of the packet created is not equal to the length of the data stored!')
    


!!! <summary>
!!!     Loads from the passed string into this packet object
!!! </summary>
!!! <param name="pby">A string that contains the packet data</param>
!!! <param name="nsz">The number of bytes of data that the packet contains</param>
!!! <returns>True if successful or false otherwise (if the passed length was invalid).</returns>
DataPacketRespBase.LoadPacket Procedure (*string pby, long nsz)!, bool, virtual
pn              equate('DataPacketRespBase.LoadPacket')
pos             long, auto
  code
    self.Log(pn)
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        return false
    end
    
    pos = 13
    
    self.ReadString(pby, pos, self.message_id)

    return true


!!! <summary>Retrieve the length of the SMPP packet data</summary>
!!! <returns>The packet length in bytes</summary>
DataPacketRespBase.GetCommandLength Procedure ()!, long, virtual
  code
    return parent.GetCommandLength() |
         + Len(self.message_id) |
         + 1


!!! <summary>Display the data contained in this packet as a human readable string</summary>
!!! <returns>A string which contains the names and values on each property of the object</returns>
DataPacketRespBase.ToString Procedure ()
st              StringTheory
  code
  
    return 'DataPacketRespBase [<13,10>' |
          & parent.ToString() & '<13,10>' |
          & '  message_id: ' & self.message_id & '<13,10>' |
          & ']'
  
!-----------------------------------------------------------    
!
! BindRespBase Class
!
!-----------------------------------------------------------    

!!! <summary>
!!!     Convert the data stored in the object into a packet and store
!!!     the packet data in the passed string.
!!! </summary>
!!! <param name="pby">A pointer to a string to store the passed packet data in. The
!!!     data should be treated as binary</param>
!!! <param name="nsz">A pointer to an integer to store the length of the packet created in (in bytes)</param> 
BindRespBase.EncodeBody Procedure (*StringTheory pby)!, virtual
  code
    parent.EncodeBody(pby)

    self.buffer.AppendA(self.system_id)
    self.WriteNull()
    
    pby.SetValue(self.buffer.GetValue())

!!! <summary>
!!!     Loads from the passed string into this packet object
!!! </summary>
!!! <param name="pby">A string that contains the packet data</param>
!!! <param name="nsz">The number of bytes of data that the packet contains</param>
!!! <returns>True if successful or false otherwise (if the passed length was invalid).</returns>
BindRespBase.LoadPacket Procedure (*string pby, long nsz)!, bool, virtual
pos             long, auto
  code
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        return false
    end
    
    pos = 13
    self.ReadString(pby, pos, self.system_id)

    return true


!!! <summary>Retrieve the length of the SMPP packet data</summary>
!!! <returns>The packet length in bytes</summary>
BindRespBase.GetCommandLength Procedure ()!, long, virtual
  code
    return parent.GetCommandLength() |
         + Len(self.system_id) |
         + 1


!!! <summary>Display the data contained in this packet as a human readable string</summary>
!!! <returns>A string which contains the names and values on each property of the object</returns>
BindRespBase.ToString Procedure ()
st              StringTheory
  code
    return 'BindRespBase [<13,10>' |
            & parent.ToString() & '<13,10>' |
            & '  system_id: ' & self.system_id & '<13,10>' |
            & ']'
            
!-----------------------------------------------------------    
!
! MessageRespBase Class
!
!-----------------------------------------------------------    

!!! <summary>
!!!     Convert the data stored in the object into a packet and store
!!!     the packet data in the passed string.
!!! </summary>
!!! <param name="pby">A pointer to a string to store the passed packet data in. The
!!!     data should be treated as binary</param>
!!! <param name="nsz">A pointer to an integer to store the length of the packet created in (in bytes)</param> 
MessageRespBase.EncodeBody Procedure (*StringTheory pby)!, virtual
  code
    parent.EncodeBody(pby)
    
    self.buffer.AppendA(self.message_id)
    self.WriteNull()
    
    pby.SetValue(self.buffer.GetValue())

!!! <summary>
!!!     Loads from the passed string into this packet object
!!! </summary>
!!! <param name="pby">A string that contains the packet data</param>
!!! <param name="nsz">The number of bytes of data that the packet contains</param>
!!! <returns>True if successful or false otherwise (if the passed length was invalid).</returns>
MessageRespBase.LoadPacket Procedure (*string pby, long nsz)!, bool, virtual
pn              equate('MessageRespBase.LoadPacket')
pos             long, auto
  code
    if not parent.LoadPacket(pby, nsz)
        return false
    elsif self.command_status <> 0
        return false
    end

    pos = 13
    self.ReadString(pby, pos, self.message_id)

    return true


!!! <summary>Retrieve the length of the SMPP packet data</summary>
!!! <returns>The packet length in bytes</summary>
MessageRespBase.GetCommandLength Procedure ()!, long, virtual
  code
    return parent.GetCommandLength() |
         + Len(self.message_id) |
         + 1

         
!!! <summary>Display the data contained in this packet as a human readable string</summary>
!!! <returns>A string which contains the names and values on each property of the object</returns>
MessageRespBase.ToString Procedure ()
st              StringTheory
  code
    return  'MessageRespBase [<13,10>' |
            & parent.ToString() & '<13,10>' |
            & '  message_id: ' & self.message_id & '<13,10>' |
            & ']'

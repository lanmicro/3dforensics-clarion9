!ABCIncludeFile(NETTALK)
! NetTalk v7.31 - the premier TCP/IP solution. Copyright � 2000-2013 CapeSoft Software  - www.capesoft.com

    include('NetEmail.inc'),once
    include('StringTheory.inc'), once
! IMAP States

Net:IMAP_NOT_CONNECTED          equate(1)
Net:IMAP_CONNECTED              equate(2)
Net:IMAP_NO_AUTH                equate(3)
Net:IMAP_AUTH                   equate(4)
Net:IMAP_SELECT                 equate(5)

NET:IMAP_IDLE_TIMEOUT   equate(180000)      ! 30 minutes (IMAP clients typically stay connected)

! Imap errors - see the ErrorMessage() method for a description
! of the errors or to translate error message.
NET:IMAP_MAILBOXES_NULL         equate(-100)
NET:IMAP_ERR_NODATA             equate(-101)
NET:IMAP_INVALID_RESPONSE       equate(-102)
NET:IMAP_NO_RESPONSE            equate(-103)
NET:IMAP_INVALID_GREETING       equate(-104)
NET:IMAP_INVALID_RESULT         equate(-105)
NET:IMAP_NODATA_PASSED          equate(-106)
NET:IMAP_NODATA_TO_SEND         equate(-107)
NET:IMAP_CANNOT_ADD_MAILBOX     equate(-108)
NET:IMAP_INVALID_LIST           equate(-109)
NET:IMAP_CANNOT_GET_MAILBOX     equate(-110)
NET:IMAP_ADD_FAILED             equate(-111)
NET:IMAP_INVALID_STATUS         equate(-112)
NET:IMAP_INVALID_FETCH          equate(-113)
NET:IMAP_INVALID_EMAIL          equate(-114)


! Specific messages
NET:IMAP_TOO_SHORT              equate('Too few parts')


! IMAP commands should not be case sensitive, but upper case
! is used through to match the convention used in the IMAP RFCs
IMAP_UNTAGGED           equate('*')

! Server Responses - Status Responses
! Status responses are OK, NO, BAD, PREAUTH and BYE.  OK, NO, and BAD
! can be tagged or untagged.  PREAUTH and BYE are always untagged.
IMAP_OK                 equate('OK')
IMAP_NO                 equate('NO')
IMAP_BAD                equate('BAD')
IMAP_SERVER_BAD         equate('* BAD')
IMAP_SERVER_OK          equate('* OK')

IMAP_R_ALERT            equate('ALERT')
IMAP_R_BADCHARSET       equate('BADCHARSET')
IMAP_R_CAPABILITY       equate('CAPABILITY')
IMAP_R_PARSE            equate('PARSE')
IMAP_R_PERMANENTFLAGS   equate('PERMANENTFLAGS')
IMAP_R_READ_ONLY        equate('READ-ONLY')
IMAP_R_READ_WRITE       equate('READ-WRITE')
IMAP_R_TRYCREATE        equate('TRYCREATE')
IMAP_R_UIDNEXT          equate('UIDNEXT')
IMAP_R_UIDVALIDITY      equate('UIDVALIDITY')
IMAP_R_UNSEEN           equate('UNSEEN')
IMAP_R_PREAUTH          equate('PREAUTH')
IMAP_R_BYE              equate('BYE')
IMAP_R_FLAGS            equate('FLAGS')
IMAP_R_EXISTS           equate('EXISTS')
IMAP_R_RECENT           equate('RECENT')
IMAP_R_LIST             equate('LIST')
IMAP_R_LSUB             equate('LSUB')
IMAP_R_STATUS           equate('STATUS')
IMAP_R_EXPUNGE          equate('EXPUNGE')
IMAP_R_SEARCH           equate('SEARCH')
IMAP_R_FETCH            equate('FETCH')

! Client Commands - Any State
IMAP_CAPABILITY         equate('CAPABILITY')        ! untagged response
IMAP_NOOP               equate('NOOP')
IMAP_LOGOUT             equate('LOGOUT')

! Client Commands - Not Authenticated State
IMAP_STARTTLS           equate('STARTTLS')
IMAP_AUTHENTICATE       equate('AUTHENTICATE')
IMAP_LOGIN              equate('LOGIN')

! Client Commands - Authenticated State
! SELECT, EXAMINE, CREATE, DELETE, RENAME, SUBSCRIBE,
! UNSUBSCRIBE, LIST, LSUB, STATUS, and APPEND.
IMAP_SELECT             equate('SELECT')
IMAP_EXAMINE            equate('EXAMINE')
IMAP_CREATE             equate('CREATE')
IMAP_DELETE             equate('DELETE')
IMAP_RENAME             equate('RENAME')
IMAP_SUBSCRIBE          equate('SUBSCRIBE')
IMAP_UNSUBSCRIBE        equate('UNSUBSCRIBE')
IMAP_LIST               equate('LIST')
IMAP_LSUB               equate('LSUB')
IMAP_STATUS             equate('STATUS')
IMAP_APPEND             equate('APPEND')

! Client Commands - Selected State
! CHECK, CLOSE, EXPUNGE, SEARCH, FETCH, STORE, COPY, and UID.
IMAP_CHECK              equate('CHECK')
IMAP_CLOSE              equate('CLOSE')
IMAP_EXPUNGE            equate('EXPUNGE')
IMAP_SEARCH             equate('SEARCH')
IMAP_FETCH              equate('FETCH')
IMAP_STORE              equate('STORE')
IMAP_UID                equate('UID')
IMAP_COPY               equate('COPY')

! IMAP Extensions
IMAP_IDLE               equate('IDLE')
IMAP_DONE               equate('DONE')

! Experimental commands (not part of the RFCs)
IMAP_XLIST              equate('XLIST')             ! Implemented by Google and Apple to get a list of folder names


! STATUS flags
IMAP_MESSAGES           equate('MESSAGES')          ! The number of messages in the mailbox.
IMAP_RECENT             equate('RECENT')            ! The number of messages with the \Recent flag set.
IMAP_UIDNEXT            equate('UIDNEXT')           ! The next unique identifier value of the mailbox.
IMAP_UIDVALIDITY        equate('UIDVALIDITY')       ! The unique identifier validity value of the mailbox.
IMAP_UNSEEN             equate('UNSEEN')

! FETCH arguments
IMAP_ALL                equate('ALL')
IMAP_FAST               equate('FAST')
IMAP_FULL               equate('FULL')
IMAP_BODY               equate('BODY')

! Body part specifiers
IMAP_HEADER             equate('HEADER')
IMAP_FIELDS             equate('HEADER.FIELDS')
IMAP_FIELDS_NOT         equate('HEADER.FIELDS.NOT')
IMAP_MIME               equate('MIME')
IMAP_TEXT               equate('TEXT')

IMAP_BODY_PEEK          equate('BODY.PEEK')

IMAP_BODYSTRUCTURE      equate('BODYSTRUCTURE')
IMAP_ENVELOPE           equate('ENVELOPE')
IMAP_FLAGS              equate('FLAGS')
IMAP_INTERNALDATE       equate('INTERNALDATE')
IMAP_RFC822             equate('RFC822')
IMAP_RFC822_HEADER      equate('RFC822.HEADER')
IMAP_RFC822_SIZE        equate('RFC822.SIZE')
IMAP_RFC822_TEXT        equate('RFC822.TEXT')

! Store arguments
IMAP_SILENT             equate('.SILENT')

!-----------------------------------------------------------

! Extensions
IMAP_GETQUOTA           equate('GETQUOTAROOT')
IMAP_QUOTA_RESPONSE     equate('QUOTA')
IMAP_APPEND_RESPONSE_START equate('[')
IMAP_APPEND_RESPONSE_END equate(']')
IMAP_GO_AHEAD_RESPONSE  equate('+')
IMAP_UIDSEARCH          equate('UID SEARCH')
IMAP_UIDFETCH           equate('UID FETCH')
IMAP_UIDSTORE           equate('UID STORE')


IMAP_EOL                equate('<13,10>')
IMAP_MESSAGE_NIL        equate('NIL')
IMAP_MESSAGE_HEADER_EOL equate('<13,10>')
IMAP_MESSAGE_SIZE_START equate('{{')
IMAP_MESSAGE_SIZE_END   equate('}')

! Standard (persistent) flags
MSG_FLAG_SEEN           equate('\Seen')                     ! Message has been read
MSG_FLAG_ANSWERED       equate('\Answered')                 ! Message has been answered
MSG_FLAG_FLAGGED        equate('\Flagged')                  ! Message is "flagged" for urgent/special attention
MSG_FLAG_DELETED        equate('\Deleted')                  ! Message is "deleted" for removal by later EXPUNGE
MSG_FLAG_DRAFT          equate('\Draft')                    ! Message has not completed composition (marked as a draft).
MSG_FLAG_RECENT         equate('\Recent')                   ! Message is "recently" arrived in this mailbox.

! Search keys
IMAP_S_ALL              equate('ALL')
IMAP_S_ANSWERED         equate('ANSWERED')
IMAP_S_BCC              equate('BCC')
IMAP_S_BEFORE           equate('BEFORE')
IMAP_S_BODY             equate('BODY')
IMAP_S_CC               equate('CC')
IMAP_S_DELETED          equate('DELETED')
IMAP_S_DRAFT            equate('DRAFT')
IMAP_S_FLAGGED          equate('FLAGGED')
IMAP_S_FROM             equate('FROM')
IMAP_S_HEADER           equate('HEADER')
IMAP_S_KEYWORD          equate('KEYWORD')
IMAP_S_LARGER           equate('LARGER')
IMAP_S_NEW              equate('NEW')
IMAP_S_NOT              equate('NOT')
IMAP_S_OLD              equate('OLD')
IMAP_S_ON               equate('ON')
IMAP_S_OR               equate('OR')
IMAP_S_RECENT           equate('RECENT')
IMAP_S_SEEN             equate('SEEN')
IMAP_S_SENTBEFORE       equate('SENTBEFORE')
IMAP_S_SENTON           equate('SENTON')
IMAP_S_SENTSINCE        equate('SENTSINCE')
IMAP_S_SINCE            equate('SINCE')
IMAP_S_SMALLER          equate('SMALLER')
IMAP_S_SUBJECT          equate('SUBJECT')
IMAP_S_TEXT             equate('TEXT')
IMAP_S_TO               equate('TO')
IMAP_S_UID              equate('UID')
IMAP_S_UNANSWERED       equate('UNANSWERED')
IMAP_S_UNDELETED        equate('UNDELETED')
IMAP_S_UNDRAFT          equate('UNDRAFT')
IMAP_S_UNFLAGGED        equate('UNFLAGGED')
IMAP_S_UNKEYWORD        equate('UNKEYWORD')
IMAP_S_UNSEEN           equate('UNSEEN')

!-----------------------------------------------------------

! MIME and mail part handling
IMAP_MESSAGE_CONTENT_TYPE  equate('content-type')
IMAP_MESSAGE_RFC822     equate('message/rfc822')
IMAP_MESSAGE_ID         equate( 'message-id')

IMAP_MESSAGE_MULTIPART  equate('multipart')
IMAP_MESSAGE_CONTENT_ENCODING equate('content-transfer-encoding')
IMAP_MESSAGE_CONTENT_DESC equate('content-description')
IMAP_MESSAGE_CONTENT_DISP equate('content-disposition')
IMAP_MESSAGE_CONTENT_SIZE equate('content-size')
IMAP_MESSAGE_CONTENT_LINES equate('content-lines')

IMAP_MESSAGE_BASE64_ENCODING equate('base64')
IMAP_MSG_DEFAULT_PART   equate('1')
IMAP_HEADER_SENDER_TAG  equate('sender')
IMAP_HEADER_FROM_TAG    equate('from')
IMAP_HEADER_IN_REPLY_TO_TAG equate('in-reply-to')
IMAP_HEADER_REPLY_TO_TAG equate('reply-to')
IMAP_HEADER_TO_TAG      equate('to')
IMAP_HEADER_CC_TAG      equate('cc')
IMAP_HEADER_BCC_TAG     equate('bcc')
IMAP_HEADER_SUBJECT_TAG equate('subject')
IMAP_HEADER_DATE_TAG    equate('date')
IMAP_PLAIN_TEXT         equate('text/plain')
IMAP_AUDIO_WAV          equate('audio/wav')
IMAP_VIDEO_MPEG4        equate('video/mpeg4')
IMAP_MAX_MSG_FLAGS      equate(10)


! Pending data queue (queued up while waiting for a
! a continuation response).
ImapPending             queue, type
data                        &string
                        end

! Queue to store each line of the response for processing.
ImapResponseLines       queue, type
text                        &string
                        end

! Imap Mailboxes - parsed from the response to the LIST command
ImapMailboxes           queue, type
reference                   cstring(100)
name                        cstring(100)
nocase                      cstring(100)            ! UPPER case name for case insensitive lookup
attributes                  cstring(256)            ! \HasNoChildren, \HasChildren, \Marked, \UnMarked, \Noselect etc.
messages                    ulong                   ! The number of messages in the mailbox.
recent                      ulong                   ! The number of messages with the \Recent flag set.
unseen                      ulong                   ! The number of messages which do not have the \Seen flag set.
uidnext                     ulong                   ! The next unique identifier value of the mailbox
uidvalidity                 ulong                   ! The unique identifier validity value of the mailbox.
                        end


! Queue of IMAP fields to store a split up response
! in the ImapFields class.
ImapFieldsQ             queue, type
value                       &string                 ! The field contents
type                        long                    ! ImapValueType (whether this was a string, a parameter list, a literal etc.)
                        end

! Class which represents an IMAP response which is split up
! and parsed into individual fields
ImapFields              Class, type,Module('NetImap.Clw'),LINK('NetImap.Clw',_ABCLinkMode_),DLL(_ABCDllMode_)
value                       &string         ! The current field (points to the current string the queue
type                        long            ! The current type

fields                      &ImapFieldsQ
pos                         long

lastError                   cstring(256)
lastErrorCode               long

Construct                   Procedure ()
Destruct                    Procedure ()

Fetch                       Procedure (long pos), bool, proc, virtual ! Get a field at the specified position
Insert                      Procedure (string val, long type=0), bool, proc, virtual     ! Add a field
Remove                      Procedure (), bool, proc, virtual    ! Delete a field
FetchType                   Procedure (long type), bool, virtual

Free                        Procedure (), virtual

Count                       Procedure (), long, virtual

Set                         Procedure (), virtual
Next                        Procedure (), long, virtual
                        end


! Stores a parameter list which has been split up.
ImapParams              queue
name                        cstring(100)
value                       cstring(256)
                        end

! The type of the part of the response after it has been tokenized
Net:IMAP_STRING                 equate(1)        ! A simple string
Net:IMAP_QUOTE                  equate(2)        ! A quoted string
Net:IMAP_LITERAL                equate(3)        ! A literal - numeric values in curly braces such as {223}
Net:IMAP_PARAM                  equate(4)        ! A parameter list - a list of one or more values seperated by spaces. Indicated by a pair of brackets: (\Answered \Flagged \Draft \Deleted \Seen)
Net:IMAP_RESP                   equate(5)        ! A reponse within pair of square brackets (can contain parameter lists etc.): [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen \*)]

! Responses to the STATUS command
ImapStatus              group, type
mailbox                     cstring(100)            ! Mailbox name
messages                    ulong                   ! The number of messages in the mailbox.
recent                      ulong                   ! The number of messages with the \Recent flag set.
unseen                      ulong                   ! The number of messages which do not have the \Seen flag set.
uidnext                     ulong                   ! The next unique identifier value of the mailbox
uidvalidity                 ulong                   ! The unique identifier validity value of the mailbox.
                        end


! Represents an address such as "Sean Cameron" <sean.cameron@capesoft.com>
ImapAddress             group, type
name                        string(80)      ! Name: "Sean Cameron"
route                       string(12)      ! contains the @ prefix or NIL (which means the default @)
mailbox                     string(80)      ! name of the mailbox, eg: "sean.cameron"
domain                      string(80)      ! capesoft.com
                        end

! Represents an RF2822 style envelope, returned from FETCH
ImapEnvelope            group, type
date                        string(48)          ! Date: "Mon, 27 Sep 2004 03:38:16 -0700 (PDT)"
subject                     string(256)         ! Subject: "Hello World"
from                        like(ImapAddress)
sender                      like(ImapAddress)
reply_to                    like(ImapAddress)
mail_to                     like(ImapAddress)
cc                          like(ImapAddress)
bcc                         like(ImapAddress)
in_reply_to                 string(256)
message_id                  string(256)
                        end

! Information for an individual mail message, including the Envelope
! headers, flags, size etc.
ImapMailInfo           group(ImapEnvelope)
internaldate                string(32)
flags                       string(256)
mail_size                   ulong
                        end

! A queue of mail items returned for a particular folder
ImapMailItems           queue(ImapMailInfo), type
                        end

! Queue of commands issued
ImapCommands            queue(), type
cmd                       string(24)     ! Command issued
args                      string(1024)   ! The arguments (only the first 1024 bytes are stored for later use)
issued                    long            ! A timestamp
                        end

!-----------------------------------------------------------
!
!    The NetImap class which implements an IMAP client
!
!-----------------------------------------------------------
NetImap             Class(NetEmail),Type,Module('NetImap.Clw'),LINK('NetImap.Clw',_ABCLinkMode_),DLL(_ABCDllMode_)
! Properties

! Internal IMAP handling
prefix                  cstring(12)

commands                &ImapCommands
result                  cstring(12)         ! Result of the last command

_commandBuffer          &StringTheory       ! Allows multiple commands to be buffered before sending when a large number of "small" command (such as STATUS) are being issued)
_bufferCommands         bool

BufferCommands          Procedure (), virtual
FlushCommands           Procedure (), virtual

buffer                  &string             ! Current response buffer
bufferLen               long                ! Amount of data in the buffer

! Error handling
lastError               long                ! Last error code
errorFunc               string(64)          ! Name of the method that the last error occured in

cErr                    string(255)         ! String returned by the Clarion Error function when the last error occured
cErrCode                long                ! Clarion error code when the last error occured

! Method behaviour options


! IMAP Data - stores returned data from the IMAP server
status                  like(ImapStatus)
envelope                like(ImapEnvelope)

mailboxes               &ImapMailboxes      ! Mailboxes (Folders) list
responses               &ImapResponseLines  ! Each response line is store in the responses queue
pending                 &ImapPending        ! Stores pending data for commands that have a continuation request response

fields                  &ImapFields         ! A response split into fields

! Methods
Construct               Procedure ()
Destruct                Procedure ()
Init                    Procedure (uLong mode=NET:SimpleClient), virtual
Connect                 Procedure (<string server>, <string user>, <string password>, <ushort port>), virtual

ThrowError              Procedure (long errcode, string methodName, <string info>), bool, proc, virtual

SendCommand             Procedure (string command, <string arguments>, bool noprefix = false), bool, proc, virtual                   ! Send the last command added to the commands queue
Process                 Procedure (), virtual
ProcessResponses        Procedure (), virtual

FirstCommand            Procedure (), string, virtual
LastCommand             Procedure (), string, virtual
CountCommands           Procedure (), long, virtual
AddCommand              Procedure (string command, string arguments), virtual
DeleteCommand           Procedure (long pos = 0), virtual
GetCommand              Procedure (long pos), string, virtual
FreeCommands            Procedure (), virtual


BufferData              Procedure (), virtual
GetBuffer               Procedure (), string, virtual

SendPending             Procedure (), bool, proc, virtual
StorePending            Procedure (*string pData), bool, virtual
FreePending             Procedure (), virtual

DeleteResponse          Procedure (), virtual
FreeResponses           Procedure (), virtual

TakeConnected           Procedure (), virtual       ! Connected to the server
TakeCompletion          Procedure (), virtual       ! A completion response
TakeFatal               Procedure (), virtual       ! response-fatal
TakeContinuation        Procedure (), virtual       ! A continuation request was received

TakeResponse            Procedure (), virtual       ! The server sent some informational data
TakeResponseData        Procedure (), virtual       ! Response data received

DeleteLastResponse      Procedure (), virtual

SplitResponse           Procedure (string str, *ImapFields vals, bool recurse=false), bool, proc, virtual
SplitParams             Procedure (*string str, *ImapParams params, bool pairs = false), virtual

GetFieldValue           Procedure(*ImapFields fields, long pos, *string pv, bool isImapAddress = false, long numFields=1), virtual

AddressToEmail          Procedure(*string imapAddr), string, virtual

ParseStatus             Procedure (string sr, <*ImapStatus mStatus>), bool, proc, virtual

Flags                   Procedure (<string f1>, <string f2>, <string f3>, <string f4>, <string f5>, <string f6>, <string f7>, <string f8>, <string f9>, <string f10>), string, virtual
ErrorMessage            Procedure (long errorCode), string, virtual
OK                      Procedure (), string, virtual
NO                      Procedure (), string,  virtual
BAD                     Procedure (), string,  virtual

! Wrapper methods - encapsulate IMAP functionality to perform typical
! IMAP client tasks.
GetMailboxes            Procedure (*ImapMailboxes mbq), bool, proc, virtual
GetFoldersInfo          Procedure (<string mailbox>, <string flags>), virtual
SyncFolder              Procedure (*ImapMailboxes mbq, bool all = false), bool, proc, virtual

!--- Basic methods, wrap up the sending of core IMAP commands
! Client Commands - Any State
! CAPABILITY, NOOP, LOGOUT
Capability              Procedure (), bool, proc, virtual
Noop                    Procedure (), bool, proc, virtual
Quit                    Procedure (), bool, proc, virtual                   ! Logout cannot be used for a method name

! Client Commands - Not Authenticated State
! STARTTLS, AUTHENTICATE, LOGIN
StartTLS                Procedure (), bool, proc, virtual
Authenticate            Procedure (string authMechanism), bool, proc, virtual
Login                   Procedure (<string user>, <string password>), bool, proc, virtual

! Client Commands - Authenticated State
! SELECT, EXAMINE, CREATE, DELETE, RENAME, SUBSCRIBE,
! UNSUBSCRIBE, LIST, LSUB, STATUS, and APPEND.
Select                  Procedure (string mailbox), bool, proc, virtual
Examine                 Procedure (string mailbox), bool, proc, virtual
CreateMailbox           Procedure (string mailbox), bool, proc, virtual
Delete                  Procedure (string mailbox), bool, proc, virtual
Rename                  Procedure (string mailbox, string newName), bool, proc, virtual
Subscribe               Procedure (string mailbox), bool, proc, virtual
Unsubscribe             Procedure (string mailbox), bool, proc, virtual
List                    Procedure (string reference, string mailbox), bool, proc, virtual
LSub                    Procedure (string reference, string mailbox), bool, proc, virtual
Status                  Procedure (string mailbox, string statusItems), bool, proc, virtual
Append                  Procedure (string mailbox, <string flags>, <string dateTime>, *string msgData), bool, proc, virtual

! Client Commands - Selected State
! CHECK, CLOSE, EXPUNGE, SEARCH, FETCH, STORE, COPY, and UID.
Check                   Procedure (), bool, proc, virtual
CloseMailbox            Procedure (), bool, proc, virtual
Expunge                 Procedure (), bool, proc, virtual
Search                  Procedure (<string charset>, string search), bool, proc, virtual
Fetch                   Procedure (string sequenceSet, string itemNames), bool, proc, virtual
Store                   Procedure (string sequenceSet, string itemName, string itemValue), bool, proc, virtual
Copy                    Procedure (string sequenceSet, string mailbox), bool, proc, virtual
Uid                     Procedure (string commandName, string commandArguments), bool, proc, virtual

! eXperimental commands
xList                   Procedure(string reference, string mailbox), bool, proc, virtual

! Extensions to the IMAPv4rev1 protocol
exIdle                  Procedure (), bool, proc, virtual
exDone                  Procedure (), bool, proc, virtual

! Completion methods - called when a command completes successfully
DoneCapability          Procedure (), virtual
DoneLogin               Procedure (), virtual
DoneQuit                Procedure (), virtual
DoneStartTLS            Procedure (), virtual
DoneAuthenticate        Procedure (), virtual
DoneSelect              Procedure (), virtual
DoneExamine             Procedure (), virtual
DoneCreateMailbox       Procedure (), virtual
DoneDelete              Procedure (), virtual
DoneRename              Procedure (), virtual
DoneSubscribe           Procedure (), virtual
DoneUnsubscribe         Procedure (), virtual
DoneList                Procedure (), virtual
DoneLSub                Procedure (), virtual
DoneStatus              Procedure (), bool, proc, virtual
DoneAppend              Procedure (), virtual
DoneCheck               Procedure (), virtual
DoneCloseMailbox        Procedure (), virtual
DoneExpunge             Procedure (), virtual
DoneSearch              Procedure (), virtual
DoneFetch               Procedure (), bool, proc, virtual
DoneStore               Procedure (), virtual
DoneCopy                Procedure (), virtual
DoneUid                 Procedure (), virtual
DonexList               Procedure (), virtual
DoneExIdle              Procedure (), virtual
DoneExDone              Procedure (), virtual

! Additional methods called when received data has been processed

! FETCH response handling
TakeFetchBody           Procedure (long messageNumber), virtual
TakeFetchText           Procedure (long messageNumber), virtual
TakeFetchMail           Procedure (long messageNumber), virtual
TakeFetchMailText       Procedure (long messageNumber), virtual
TakeFetchMailHeader     Procedure (long messageNumber), virtual

TakeFetchHeader         Procedure (*ImapFields fields, long messageNumber), virtual
TakeFetchMime           Procedure (*ImapFields fields, long messageNumber), virtual

TakeFetchBodyStructure  Procedure (*ImapFields fields, long messageNumber), virtual
TakeFetchEnvelope       Procedure (*ImapFields fields, long messageNumber), virtual
TakeFetchFlags          Procedure (*ImapFields fields, long messageNumber), virtual
TakeFetchInternalDate   Procedure (*ImapFields fields, long messageNumber), virtual
TakeFetchMailSize       Procedure (*ImapFields fields, long messageNumber), virtual
TakeFetchUid            Procedure (*ImapFields fields, long messageNumber), virtual

TakeFetchOther          Procedure (long messageNumber), virtual

! Getting and Setting Flags
GetFlags                Procedure(String sequenceSet), bool, proc, virtual                  !done fetch
SetFlags                Procedure(String sequenceSet,string flags), bool, proc, virtual     !done store
                    end


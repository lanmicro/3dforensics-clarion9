 ! ThinN@ include file

 MAP
  MODULE('ThinN@.lib')
RisNet:START  PROCEDURE(_PROC ProcedureName,UNSIGNED Stack=0),SIGNED,PROC,NAME('RisNet:START'),DLL(_ABCDllMode_) 
RisNet:START1 PROCEDURE(_PROC1 ProcedureName,UNSIGNED Stack=0,STRING Val1),SIGNED,PROC,NAME('RisNet:START1'),DLL(_ABCDllMode_) 
RisNet:START2 PROCEDURE(_PROC2 ProcedureName,UNSIGNED Stack=0,STRING Val1,STRING Val2),SIGNED,PROC,NAME('RisNet:START2'),DLL(_ABCDllMode_) 
RisNet:START3 PROCEDURE(_PROC3 ProcedureName,UNSIGNED Stack=0,STRING Val1,STRING Val2,STRING Val3),SIGNED,PROC,NAME('RisNet:START3'),DLL(_ABCDllMode_) 
RisNet:MouseX PROCEDURE,SIGNED,NAME('RisNet:MouseX'),DLL(_ABCDllMode_)
RisNet:MouseY PROCEDURE,SIGNED,NAME('RisNet:MouseY'),DLL(_ABCDllMode_)
RisNet:Message PROCEDURE(STRING pText,<STRING pCaption>,<STRING pIcon>,<STRING pButtons>,UNSIGNED pDefault=0,UNSIGNED pStyle=0),UNSIGNED,PROC,NAME('RisNet:Message'),DLL(_ABCDllMode_) 
RisNet:Popup   PROCEDURE(STRING Selections,SIGNED XPar=_nopos,SIGNED Ypar=_nopos),SIGNED,NAME('RisNet:Popup'),DLL(_ABCDllMode_)       
RisNet:Destroy PROCEDURE(SIGNED FirstControl=0,SIGNED EndControl=0),NAME('RisNet:Destroy'),DLL(_ABCDllMode_) 
RisNet:GetFileWriteTime PROCEDURE(STRING FileName,*LONG pDay,*LONG pMonth,*LONG pYear,*LONG pHour,*LONG pMinute,*LONG pSecond,*LONG pMilliseconds),LONG,PROC,DLL(_ABCDllMode_)
RisNet:GetFileWriteTime PROCEDURE(STRING FileName),STRING,DLL(_ABCDllMode_)
RisNet:SetFileWriteTime PROCEDURE(STRING FileName,LONG wDay,LONG wMonth,LONG wYear,LONG wHour,LONG wMinute,LONG wSecond,LONG wMilliseconds),STRING,PROC,DLL(_ABCDllMode_)
RisNet:DownloadFiles PROCEDURE(*QUEUE SourceQueue,*? FileName,STRING Sign,<STRING FilePath>,<*? ClientDirChoice>),STRING,DLL(_ABCDllMode_),NAME('RisNet:DownloadFiles')
RisNet:DownloadFile  PROCEDURE(STRING FileName,<STRING Sign>,<STRING FilePath>,<STRING TargetName>,<*? ChangeCheck>,<*? ClientDirChoice>),STRING,PROC,DLL(_ABCDllMode_),NAME('RisNet:DownloadFile')
RisNet:UploadFile PROCEDURE(STRING FileName,<STRING FilePath>),DLL(_ABCDllMode_),NAME('RisNet:UploadFile')
RisNet:ReturnItem PROCEDURE(STRING Expression, *LONG Pos,*? RetVal1,*? RetVal2,<*LONG pSize>),DLL(_ABCDllMode_),NAME('RisNet:ReturnItem')
RisNet:CancelCloseWindow PROCEDURE(),NAME('RisNet:CancelCloseWindow'),DLL(_ABCDllMode_)
RisNet:Encode PROCEDURE(? Expression),STRING,DLL(_ABCDllMode_),NAME('RisNet:Encode')
RisNet:Decode PROCEDURE(? Expression),STRING,DLL(_ABCDllMode_),NAME('RisNet:Decode')
RisNet:FileDialog PROCEDURE(<STRING Title>,*? FileName,<STRING Extensions>,SIGNED Flag=0),PROC,BOOL,NAME('RisNet:FileDialog'),DLL(_ABCDllMode_)
RisNet:FileDialoga PROCEDURE(<STRING Title>,*? FileName,<STRING Extensions>,SIGNED Flag=0,<*SIGNED pIndex>),PROC,BOOL,NAME('RisNet:FileDialoga'),DLL(_ABCDllMode_)
RisNet:ColorDialog PROCEDURE(<STRING Title>,*? Rgb,SIGNED Suppress=0),SIGNED,PROC,NAME('RisNet:ColorDialog'),DLL(_ABCDllMode_)
RisNet:FontDialog  PROCEDURE(<STRING Title>,*? TypeFace,<*? Size>,<*? Color>,<*? Style>,SIGNED Added= 0),BOOL,PROC,NAME('Risnet:FontDialog'),DLL(_ABCDllMode_)
RisNet:FontDialoga PROCEDURE(<STRING Title>,*? TypeFace,<*? Size>,<*? Color>,<*? Style>,<*? Charset>,SIGNED Added= 0),BOOL,PROC,NAME('RisNet:FontDialoga'),DLL(_ABCDllMode_)
RisNet:PrinterDialog PROCEDURE(<STRING Title>,BOOL Flag=FALSE),BOOL,PROC,NAME('RisNet:PrinterDialog'),DLL(_ABCDllMode_)
RisNet:RunOnClient PROCEDURE(? ExecuteString,? WaitFlag,<? ShowFlag>),LONG,PROC,NAME('RisNet:RunOnClient'),DLL(_ABCDllMode_)
RisNet:GetIpAddress PROCEDURE(*CSTRING IpAddress),LONG,DLL(_ABCDllMode_),NAME('RisNet:GetIpAddress')
RisNet:GetIpAddressByHost PROCEDURE(*CSTRING HostName,*CSTRING IpAddress,LONG num),NAME('RisNet:GetIpAddressByHost'),DLL(_ABCDllMode_)
RisNet:GetPeerAddress PROCEDURE(LONG Handle,*CSTRING IpAddress),NAME('RisNet:GetPeerAddress'),DLL(_ABCDllMode_)
RisNet:SetMessage PROCEDURE(LONG MessageId,STRING MessageText),NAME('RisNet:SetMessage'),DLL(_ABCDllMode_)
RisNet:Compress_ PROCEDURE(? Source,*LONG outsize,LONG SourceSize=0),NAME('RisNet:Compress_'),STRING,DLL(_ABCDllMode_)
RisNet:Compress PROCEDURE(? Source,LONG SourceSize=0),NAME('RisNet:Compress'),STRING,DLL(_ABCDllMode_)  
RisNet:Decompress PROCEDURE(? Source,LONG SourceSize=0, BYTE gzip=0),NAME('RisNet:Decompress'),STRING,DLL(_ABCDllMode_)  
RisNet:Decompress_ PROCEDURE(? Source,*LONG outsize,LONG SourceSize=0, BYTE gzip=0),NAME('RisNet:Decompress_'),STRING,DLL(_ABCDllMode_)  
RisNet:GetCId PROCEDURE(BYTE Par=0),STRING,NAME('RisNet:GetCId'),DLL(_ABCDllMode_)
RisNet:Select PROCEDURE(SIGNED Control=0,SIGNED Position=0,SIGNED EndPosition=0),NAME('RisNet:Select'),DLL(_ABCDllMode_)
RisNet:ReturnModul PROCEDURE(STRING ParIzraz,LONG Duzina,LONG Modul,BYTE IsStr=0),LONG,NAME('RisNet:ReturnModul'),DLL(_ABCDllMode_)
RisNet:ForceCloseWindow PROCEDURE(),BYTE,NAME('RisNet:ForceCloseWindow'),DLL(_ABCDllMode_) 
RisNet:CheckReturnSpecifics PROCEDURE(BYTE ShowErr=1),NAME('RisNet:CheckReturnSpecifics'),DLL(_ABCDllMode_)
RisNet:PressKey PROCEDURE(UNSIGNED pKeyCode),NAME('RisNet:PressKey'),DLL(_ABCDllMode_) 
RisNet:ShowProgressDialog PROCEDURE(? pDone,? pTotal,BYTE pCancel=0,<? pTitle>,<? pFormatExpression>),BYTE,PROC,NAME('RisNet:ShowProgressDialog'),DLL(_ABCDllMode_) 
RisNet:Press PROCEDURE(STRING Expression),NAME('RisNet:Press'),DLL(_ABCDllMode_)
RisNet:GetClientPath PROCEDURE(),STRING,NAME('RisNet:GetClientPath'),DLL(_ABCDllMode_) 
RisNet:SetClientPath PROCEDURE(STRING PathExpression),STRING,NAME('RisNet:SetClientPath'),DLL(_ABCDllMode_)
RisNet:Sleep PROCEDURE(ULONG Timeout),NAME('RisNet:Sleep'),DLL(_ABCDllMode_) 
RisNet:SetLocking PROCEDURE(BYTE LockingStatus),NAME('RisNet:SetLocking'),DLL(_ABCDllMode_)
RisNet:Halt PROCEDURE(UNSIGNED errorlevel=0,<STRING message>),NAME('RisNet:Halt'),DLL(_ABCDllMode_)
RisNet:DragId PROCEDURE(<*? Thread>,<*? Control>),STRING,NAME('RisNet:DragId'),DLL(_ABCDllMode_) 
RisNet:SetOCXProperty PROCEDURE(LONG Feq,STRING Expression,<STRING SetValue>,<*WINDOW WindowHandle>),NAME('RisNet:SetOCXProperty'),DLL(_ABCDllMode_)
RisNet:GetOCXProperty PROCEDURE(LONG Feq,STRING Property,<*WINDOW WindowHandle>,BYTE BindResult = 0),STRING,NAME('RisNet:GetOCXProperty'),DLL(_ABCDllMode_) 
RisNet:OCXRegisterEventProc PROCEDURE(LONG Feq,LONG EventProcAddress),NAME('RisNet:OCXRegisterEventProc'),DLL(_ABCDllMode_)
RisNet:OCXGetLastEventName PROCEDURE(SHORT Reference),STRING,NAME('RisNet:OCXGetLastEventName'),DLL(_ABCDllMode_)
RisNet:OCXGetParamCount PROCEDURE(SHORT Reference),STRING,NAME('RisNet:OCXGetParamCount'),DLL(_ABCDllMode_) 
RisNet:OCXGetParam PROCEDURE(SHORT Reference,LONG Count),STRING,NAME('RisNet:OCXGetParam'),DLL(_ABCDllMode_)
RisNet:OCXRegister PROCEDURE(STRING FileName,BYTE ForceRegistration = 0),NAME('RisNet:OCXRegister'),BYTE,DLL(_ABCDllMode_),PROC 
RisNet:OCXBind PROCEDURE(STRING BindName, <*? Variable>, <BYTE BindType>),NAME('RisNet:OCXbind'),DLL(_ABCDllMode_) 
RisNet:OCXAddSkipEvent PROCEDURE(LONG Feq, LONG pEvent=0, <STRING EventExpression>,<*WINDOW WindowHandle>),NAME('RisNet:OCXAddSkipEvent',)DLL(_ABCDllMode_) 
RisNet:GetWindowMaxState PROCEDURE(*WINDOW WindowHandle),BYTE,NAME('RisNet:GetWindowMaxState'),BYTE,DLL(_ABCDllMode_),PROC
RisNet:SetWindowMaxState PROCEDURE(*WINDOW WindowHandle, BYTE MaxState),NAME('RisNet:SetWindowMaxState'),DLL(_ABCDllMode_)
RisNet:ForwardKey PROCEDURE(LONG Feq, UNSIGNED pKeyCode, LONG SelStart=0),NAME('RisNet:ForwardKey'),DLL(_ABCDllMode_) 
RisNet:ForwardKey_ PROCEDURE(LONG Feq),NAME('RisNet:ForwardKey_'),DLL(_ABCDllMode_) 
RisNet:SetDefaultListboxHeaderColors PROCEDURE(BYTE Enabled=1, LONG cNormal = -2, LONG cAccentTxt = -2, LONG cListHeaderBack = -2, LONG cListHeaderFore = -2, LONG cListHeaderGRBack = -2, LONG cListHeaderGrFore = -2, LONG cListHeaderArrow = -2, LONG cListHeaderArrow3D = -2),NAME('RisNet:SetDefaultListboxHeaderColors'),DLL(_ABCDllMode_) 
RisNet:SetListboxHeaderColors PROCEDURE(*Window WindowHandle, LONG Feq, SHORT Enabled=1, LONG cNormal = -2, LONG cAccentTxt = -2, LONG cListHeaderBack = -2, LONG cListHeaderFore = -2, LONG cListHeaderGRBack = -2, LONG cListHeaderGrFore = -2, LONG cListHeaderArrow = -2, LONG cListHeaderArrow3D = -2),NAME('RisNet:SetListboxHeaderColors'),DLL(_ABCDllMode_)
RisNet:SetDefaultTabStyle PROCEDURE(BYTE pTabStyle = 1),NAME('RisNet:SetDefaultTabStyle'),DLL(_ABCDllMode_) 
RisNet:SetTabStyle PROCEDURE(*Window WindowHandle, LONG Feq, BYTE pTabStyle = 1),NAME('RisNet:SetTabStyle'),DLL(_ABCDllMode_)
RisNet:RunThinetApplication PROCEDURE(STRING ApplicationName),NAME('RisNet:RunThinetApplication'),DLL(_ABCDllMode_)
RisNet:GroupToXML PROCEDURE(*GROUP pGroup),STRING,NAME('RisNet:GroupToXML'),DLL(_ABCDllMode_)
RisNet:XMLToGroup PROCEDURE(STRING pString, *GROUP pGroup, <STRING pIgnoreString>),BYTE,NAME('RisNet:XMLToGroup'),DLL(_ABCDllMode_) 
RisNet:QueueToXML PROCEDURE(*QUEUE pQueue),STRING,NAME('RisNet:QueueToXML'),DLL(_ABCDllMode_) 
RisNet:XMLToQueue PROCEDURE(STRING pString, *QUEUE pQueue),BYTE,NAME('RisNet:XMLToQueue'),DLL(_ABCDllMode_) 
RisNet:Exec PROCEDURE(STRING pCommand,LONG Wait=0, LONG Show=1, LONG Timeout=0, <STRING pWindowStation>, <STRING pDesktop>, <STRING pCurrentDirectory>,BYTE Priority=2),LONG,PROC,NAME('RisNet:Exec'),DLL(_ABCDllMode_) 
RisNet:Exists PROCEDURE(STRING PathExpression),LONG,PROC,NAME('RisNet:Exists'),DLL(_ABCDllMode_) 
RisNet:SetTopWindow PROCEDURE(STRING WindowCaption),ULONG,PROC,NAME('RisNet:SetTopWindow'),DLL(_ABCDllMode_)
RisNet:ShellExecute PROCEDURE(ULONG HWND, STRING pOperation, STRING pFile, STRING pParameters, STRING pDirectory, ULONG pShowCmd),ULONG,NAME('RisNet:ShellExecute'),PROC,DLL(_ABCDllMode_)
RisNet:SendMail PROCEDURE(STRING SendTo, STRING Subject, STRING MessageText, STRING Attachment, BYTE pSend = 0, BYTE SendFromServer = 0),PROC,LONG,NAME('RisNet:SendMail'),DLL(_ABCDllMode_) 
RisNet:ShowWindow PROCEDURE(ULONG HWND, ULONG ShowCmd),BYTE,PROC,NAME('RisNet:ShowWindow'),DLL(_ABCDllMode_) 
RisNet:Active PROCEDURE(),BYTE,NAME('RisNet:Active'),DLL(_ABCDllMode_) 
RisNet:TakeEvent PROCEDURE(*WINDOW Wind),NAME('RisNet:TakeEvent'),DLL(_ABCDllMode_) 
RisNet:GetClientParameters PROCEDURE,STRING,NAME('RisNet:GetClientParameters'),DLL(_ABCDllMode_)
RisNet:CreatePDFfromWMF PROCEDURE(*QUEUE vSourceQueue, *? vFileName, STRING PdfFileName),NAME('RisNet:CreatePDFfromWMF'),DLL(_ABCDllMode_)
RisNet:SetScaleParameters PROCEDURE(LONG GlobalMarginLeft = -1, LONG GlobalMarginTop = -1, LONG GlobalMarginRight = -1, LONG GlobalMarginBottom = -1),NAME('RisNet:SetScaleParameters'),DLL(_ABCDllMode_)
RisNet:ScaleWindow PROCEDURE(*WINDOW WindowName, BYTE ScaleWindow=1, BYTE MaxWidth=0, BYTE MaxHeight=0, LONG MarginX=0, LONG MarginY=0, LONG MarginBottom=0, LONG MarginRight=0, BYTE DefaultXYPosition=0, LONG PreviousWindowXpos = 0, LONG PreviousWindowYPos = 0, LONG PreviousWindowThread = 0),NAME('RisNet:ScaleWindow'),DLL(_ABCDllMode_) 
RisNet:DisableWindowScaling PROCEDURE(BYTE DisableWindowScaling=0, BYTE DisableWindowResizing=0),NAME('RisNet:DisableWindowScaling'),DLL(_ABCDllMode_) 
RisNet:GetClientTempPath PROCEDURE(),STRING,NAME('RisNet:GetClientTempPath'),DLL(_ABCDllMode_)
RisNet:DisplayThread PROCEDURE(LONG ThreadNo=0),NAME('RisNet:DisplayThread'),DLL(_ABCDllMode_) 
RisNet:OpenWindow PROCEDURE(*WINDOW Wind),NAME('RisNet:OpenWindow'),DLL(_ABCDllMode_)
RisNet:CloseWindow PROCEDURE(*WINDOW Wind),NAME('RisNet:CloseWindow'),DLL(_ABCDllMode_) 
RisNet:ClientVersion PROCEDURE,STRING,NAME('RisNet:ClientVersion'),DLL(_ABCDllMode_)
RisNet:SetActiveThread PROCEDURE(LONG ThreadNo),NAME('RisNet:SetActiveThread'),DLL(_ABCDllMode_) 
RisNet:AddOption PROCEDURE(*Window WindowHandle,LONG Id,? OptionVar),NAME('RisNet:AddOption'),DLL(_ABCDllMode_) 
RisNet:TabInsteadEnter PROCEDURE(BYTE Param = 1),NAME('RisNet:TabInsteadEnter'),DLL(_ABCDllMode_) 
RisNet:ExpandComboBox PROCEDURE(LONG Feq, BYTE Clear = False),NAME('RisNet:ExpandComboBox'),DLL(_ABCDllMode_) 
RisNet:GetResolution PROCEDURE(*? xRes, *? yRes),NAME('RisNet:GetResolution'),DLL(_ABCDllMode_)
RisNet:PauseNewThreads PROCEDURE(LONG Pause),NAME('RisNet:PauseNewThreads'),DLL(_ABCDllMode_)
  END
 END

! Downloadfile function equates
RisNet:FileDialog   EQUATE('<-4->1') 
RisNet:NoDirCreate  EQUATE('<-6->0') 
RisNet:DirCreate    EQUATE('<-6->1') 
RisNet:AskDirCreate EQUATE('<-6->2') 

StatisticsClass CLASS,TYPE,DLL(_ABCDllMode_),EXTERNAL,MODULE('ThinN@.lib'),LINK('ThinN@.lib',_ABCLinkMode_)
Initialized      BYTE
ProcessId        ULONG
AppName          CSTRING(51)
UserName         CSTRING(51)
Password         CSTRING(51)
ClientIpAddress  CSTRING(51)
ClientWebAddress CSTRING(51)
OldClientSerial  CSTRING(51) 
ClientSerial     CSTRING(51)
ClientType       CSTRING(51) ! Values: WinClient6, WinClient7
ClientResolution CSTRING(51) ! Format: 1920x1080
ClientOSVersion  CSTRING(51) ! Values: Win3.1,Win95,Win98,WinME,WinXP64Bit,WinXP,NT_3.51,NT_4.0,Win2000,WinServer2003,Win2000,WinVista,WinServer2008,Win7,WinServer2008R2
WriteIt          BYTE    
LastRefreshTime  LONG
LastRefreshDate  LONG
LastRefreshTimea LONG
LastRefreshDatea LONG
StartDate        LONG
StartTime        LONG
LongRunning      BYTE
ActiveTime       LONG 
FileName         CSTRING(FILE:MaxFileName)
Active           BYTE
Marked           BYTE
UserActive       BYTE
Init      PROCEDURE(STRING FilePath,STRING ApplicationName)
WriteDown PROCEDURE()
Kill      PROCEDURE()
 END

NetManager CLASS,TYPE,DLL(_ABCDllMode_),EXTERNAL,MODULE('ThinN@.lib'),LINK('ThinN@.lib',_ABCLinkMode_)
FilePath              CSTRING(FILE:MaxFileName) ! Default file path for downloaded files
FileDirPath           CSTRING(FILE:MaxFileName) ! Default path with /username in temp directory
ClientPath            CSTRING(FILE:MaxFileName) ! Default file path for downloaded files on client
ClientStartPath       CSTRING(FILE:MaxFileName) ! Default client starting path
ClientPrinter         CSTRING(800)              ! Default printer on client side PRINTER{PROPPRINT:DEVICE}
Stats                 &StatisticsClass
Initialized           LONG ! Indikator if listener is initialized
Port                  LONG ! Listener port
ChannelPort           LONG ! NetPort service enabled - will contain application port to channel 
ServerName            CSTRING(100) ! Server name
ProxyServer           CSTRING(100) ! Proxy server
ProxyPort             LONG ! Proxy port
UseProxy              BYTE ! Indicator of proxy usage
ProxyUserName         CSTRING(100) ! Proxy username
ProxyPassword         CSTRING(100) ! Proxy password
ProxyBuffer           LONG ! Proxy buffer
Active                BYTE ! Indicator of RisNet application
Client                BYTE ! Is this client side
BufferSize            LONG ! Transfer buffer size
Timeout               LONG ! Timeout
LastTimeout           LONG ! Last set timeout
ListenerTimeout       LONG ! Just listener rcv timeout
LongTimeout           LONG ! Timeout of long running processes
Reconnect             BYTE ! Reconnect parameter
Handle                LONG
ExchgBuffer           &STRING ! send/recieve buffer
ExchgBufferSize       LONG ! buffer memory size
ExchgBufferLength     LONG ! buffer length
FirstWindow           LONG ! Handle of first present window
ApplicationWindow     LONG ! Handle of applicationwindow
State                 BYTE ! 0 - waiting for ping, 1 - sending response, 2 - skipping to another window accept without waiting for ping
Windows               &WindowsQueue
SkipCurrentAccept     LONG ! Indiacted procedure is run on different thread and so skip current accept loop
RepeatRequest         LONG ! Indicate that server want client to ask additional request after response
CurrentThread         LONG ! Currently active thread that should be checked, ignore other threads
PreviousThread        LONG ! Previous active thread
PreviousHandle        LONG ! Handle to previous active window
ThreadBusy            LONG ! Indicator if any of the threads is busy
ThreadStarting        LONG ! Indicator if any of the threds is starting
TransactionId         LONG ! Id of transaction
TransactionState      BYTE ! 1 - server recieve, client send, 2 - cliend recieve server send
ActiveAccept          LONG ! Is accept loop on client active
MouseX                SIGNED ! Cordinates of mouse
MouseY                SIGNED ! Cordinates of mouse
MouseXOld             SIGNED
MouseYOld             SIGNED 
ClientLastActivityTime LONG ! Last activity date (client)
ClientLastActivityDate LONG ! Last activity time (client)
ClientTimeout         LONG ! Timeout period on client
qDownloadFiles        &DownloadFilesQueue
TraceFileName         CSTRING(FILE:MaxFileName)
MaxUploadFileSize     ULONG ! Max upload file size
CompressionType       BYTE ! Compression type 0 - 7zip (req 7z.exe), 1 - rar (req rar.exe)
Printed               BYTE ! Indicator that last report send to client is printed
PendingTimerClass     &PendingTimerClassType ! Pending timer class
EqualizeWindowOptions PROCEDURE(*WindowOptionsGroup SourceOptions,*WindowOptionsGroup DestinationOptions)
EqualizeControlQueues PROCEDURE(*ControlsQueue SourceControlQueue, *ControlsQueue DestControlQueue)
EqControlQueue        PROCEDURE(*ControlsQueue SourceControlQueue, *ControlsQueue DestControlQueue)
CompareControl        PROCEDURE(? Value1, ? Value2, LONG Number)
GetPaintStatus        PROCEDURE(LONG ControlNumber),LONG
ScanWindow 				    PROCEDURE(*Window WindowHandle,BYTE Force=0, BYTE SkipFocus=0, LONG ListboxScrollingFeq = 0, <STRING FeqEvents>),NAME('ScanWindow@F10NETMANAGER')
StoreWindow           PROCEDURE(? WindowDesc)
CreateWindow          PROCEDURE(? WindowDesc)
PaintWindow           PROCEDURE(LONG WindowHandle)
Kill                  PROCEDURE()
KillControl           PROCEDURE(*ControlsQueue DestControlQueue)
KillWindow            PROCEDURE()
OpenWindow            PROCEDURE(*WINDOW Wind)
CloseWindow           PROCEDURE(*WINDOW Wind),NAME('CloseWindow@F10NETMANAGER')
StartLongRunningProcess PROCEDURE()
EndLongRunningProcess   PROCEDURE()
TakeEvent             PROCEDURE(*WINDOW Wind)
CloseReport           PROCEDURE()
DescribeWindow        PROCEDURE(STRING Diff,? WHandle,? WHandle2,BYTE Store=1),PROC,BYTE,NAME('DescribeWindow@F10NETMANAGER') 
!
SendDataFromServer    PROCEDURE(LONG Timeout=0),PROC,LONG,NAME('SendDataFromServer@F10NETMANAGER')
SendDataFromClient    PROCEDURE,PROC,ULONG
RecieveDataFromClient PROCEDURE(LONG Timeout=0),PROC,LONG,NAME('RecieveDataFromClient@F10NETMANAGER')
!
AddListControl        PROCEDURE(*Window WindowHandle,LONG Feq,*QUEUE ListQueue)
AddListControl        PROCEDURE(*Window WindowHandle,LONG Feq,? DataString)
DropListControl       PROCEDURE(LONG Feq)
AddListEIPControl     PROCEDURE(LONG pListFeq,LONG Column,LONG ControlFeq),NAME('AddListEIPControl@F10NETMANAGER')
AddOption             PROCEDURE(*Window WindowHandle,LONG Id,? OptionVar)
DownloadFiles         PROCEDURE(*QUEUE SourceQueue,*? FileName,STRING Sign,<STRING FilePath>,<*? ClientDirChoice>),STRING,PROC,NAME('DownloadFiles@F10NETMANAGER')
SetBufferSize         PROCEDURE(LONG BufferSize)
SetTimeout            PROCEDURE(LONG Timeout)
Construct             PROCEDURE()
Destruct              PROCEDURE()
Connect               PROCEDURE(LONG vReconnect = 0,*? ErrorStr),PROC,LONG
Disconnect            PROCEDURE()
CreateLissener        PROCEDURE(),LONG,PROC
UploadFile            PROCEDURE(*? FileName,<STRING FilePath>)
SetBuffer             PROCEDURE(? Expression,LONG SourceSize=0,BYTE AddToEnd=0),NAME('SetBuffer@F10NETMANAGER')
GetBuffer             PROCEDURE(),STRING,NAME('GetBuffer@F10NETMANAGER')
CompressFile          PROCEDURE(STRING FileName,STRING Archive),NAME('CompressFile@F10NETMANAGER'),VIRTUAL
CompressFiles         PROCEDURE(*QUEUE QF,*? FileName,STRING Archive),NAME('CompressFiles@F10NETMANAGER'),VIRTUAL
DecompressFile        PROCEDURE(STRING Archive,STRING FilePath,<STRING vPassword>,<STRING vParams>,LONG vTimeout=0),NAME('DecompressFile@F10NETMANAGER'),VIRTUAL
PrintPreview          PROCEDURE(*QUEUE PrintPreviewQueue,*? FileName,LONG pLandscape,BYTE SkipPreview=0,LONG vZoom=0),NAME('PrintPreview@F10NETMANAGER'),BYTE,VIRTUAL
SetClientPrinter      PROCEDURE(STRING PrinterName),NAME('SetClientPrinter@F10NETMANAGER')
ExecuteClientSource   PROCEDURE(STRING Var1,<STRING Var2>,<STRING Var3>,<STRING Var4>,<STRING Var5>,<STRING Var6>,<STRING Var7>),NAME('ExecuteClientSource@F10NETMANAGER'),STRING,VIRTUAL,PROC  
Start                 PROCEDURE(<STRING TemplateVersion>),NAME('Start@F10NETMANAGER')
DisplayControlImage   PROCEDURE(LONG Feq),NAME('DisplayControlImage@F10NETMANAGER')
DisplayThread         PROCEDURE(LONG ThreadNo=0),NAME('DisplayThread@F10NETMANAGER')
AddRtfControl         PROCEDURE(*Window WindowHandle, LONG Feq, *RTFControlClass RTFCClass),NAME('AddRtfControl@F10NETMANAGER')
AddImageFolder        PROCEDURE(STRING ImagePath,BYTE OnTop=0),NAME('AddImageFolder@F10NETMANAGER') 
AddImageLibrary       PROCEDURE(STRING ImageLibrary,<STRING ImageLibrarySubdirectory>,<STRING ImageLibraryPassword>,BYTE ForceDownloadCheck=0),NAME('AddImageLibrary@F10NETMANAGER')
Display               PROCEDURE(<*Window WindowPtr>,BYTE ForceEnd=0, LONG vWindowHandle = 0),BYTE,PROC,NAME('Display@F10NETMANAGER') ! 03.02.11
AddOCXControl         PROCEDURE(*Window WindowHandle, LONG Feq, STRING CreateStatement),NAME('AddOCXControl@F10NETMANAGER')
AddFileToDownloadList PROCEDURE(*? FilePath, BYTE Compression=0, BYTE Recheck = 0),NAME('AddFileToDownloadList@F10NETMANAGER')
! Virtual draw functions
AfterCreatingWindow   PROCEDURE(),VIRTUAL,NAME('AfterCreatingWindow@F10NETMANAGER')
BeforePaintingControl PROCEDURE(LONG Feq,BYTE Created=0,BYTE Last=0),BYTE,VIRTUAL,NAME('BeforePaintingControl@F10NETMANAGER')
AfterPaintingControl  PROCEDURE(LONG Feq,BYTE Created=0,BYTE Last=0),VIRTUAL,NAME('AfterPaintingControl@F10NETMANAGER')
GetControlOption      PROCEDURE(LONG Feq,STRING pOption),STRING,NAME('GetControlOption@F10NETMANAGER')
TakeClientEvent       PROCEDURE(*Window WindowHandle),BYTE,PROC,VIRTUAL,NAME('TakeClientEvent@F10NETMANAGER')
 END
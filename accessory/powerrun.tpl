#!------------------------------------------------------------------------------------------
#! PowerRUN and CWPRINT Code Template
#! (c) Copyright 1996/2003 by Greg Berthume; All Rights Reserved Worldwide
#!
#! Version 5.0  01/30/03
#!
#! Edit History:
#! -----------------------------------------------------------------------------------------
#! [18] gsb 01/30/03  Released as Freeware
#! [17] gsb 05/18/01  Added MAILTO: hint to ShellExecute code template interface.
#! [16] gsb 07/17/00  Change size of GBWinCommand from 120 to 201.
#! [15] gsb 01/28/98  Make "ABC" template chain compatible.
#! [14] gsb 12/03/97  Set GBPowerRUNMode=1 in the CWPRINT templates to run in foreground.
#! [13] gsb 10/09/97  Added "GB" to some variables to avoid conflict with PCL Tools.
#! [12] gsb 09/09/97  Set GBPowerRUNWait=1 in the CWPRINT template and add the new 2003 CWPRINT
#!                    template that has new features.
#! [11] gsb 09/01/97  Added code template to play wave files.
#! [10] gsb 08/19/97  PRUNAPI.CLW & PRUNEQU.CLW now used instead of WINDOWS.CLW & WINEQU.CLW
#! [9]  gsb 08/16/97  Added ShellExecute() support.
#! [8]  gsb 06/22/97  Added option to set the CreateProcess Priority - 32bit only.
#! [7]  gsb 06/03/97  Use CreateProcess() and GetExitCodeProcess() if 32bit app.
#!                    Added 'Run Hidden - User Cannot Access' execution option.
#! [6]  gsb 04/22/97  Increased GBWinCommand template token from 100 to 130.
#! [5]  gsb 02/21/97  Added Ship List update to include: DOSPROC.PIF & DOSPROC.BAT
#! [4]  gsb 02/07/97  Added CWPRINT code template courtesy of Jeroen van Helvoort
#!                    and use PowerRUN not RUN!  Doesn't appear that CWPRINT can be launched
#!                    as minimized.
#! [3]  tm  12/06/96  Tom Moseley added Multi-dll / DET support.
#! [2]  gsb 11/13/96  Added Printer Property inclusion option.
#! [1]  gsb 10/15/96  Created
#!
#!------------------------------------------------------------------------------------------
#TEMPLATE (PowerRUN, 'PowerRUN v5.0/ABC'),FAMILY('ABC')
#EXTENSION (GBPowerRUNGlobal,'PowerRUN Global Extension'),APPLICATION,LAST
#BOXED(''),AT(,,195)
#BUTTON('About PowerRUN Version 5.0/ABC'),AT(7,,189)
   #DISPLAY('PowerRUN Version 5.0 for Clarion/ABC'),AT(26,,,)
   #DISPLAY('16/32 bit, multi-dll'),AT(55,,,)
   #DISPLAY('')
   #DISPLAY('Copyright (C) 1996/2003 by GREG BERTHUME'),AT(29,,,)
   #DISPLAY('All Rights Reserved Worldwide'),AT(50,,,)
   #DISPLAY('')
   #DISPLAY('TECHNICAL SUPPORT'),AT(52,,,)
   #DISPLAY('     None!  It''s Freeware!'),AT(25,,,)
   #DISPLAY('')
   #DISPLAY('INTERNET WORLD WIDE WEB SITE:'),AT(30,,,)
   #DISPLAY('http://www.berthume.com'),AT(35,,,)
#ENDBUTTON
#ENDBOXED
#BOXED('Global Support'),AT(,,195)
#DISPLAY ('')
#PROMPT('Include Printer Properties: \LIBSRC\PRNPROP.CLW',CHECK),%IncludePrnProp,DEFAULT(0),AT(10,,200)
#DISPLAY ('Disable if you use CPCS Reporting Tools as they are')
#DISPLAY ('probably included for you.')
#DISPLAY ('')
#PROMPT('Include Windows Naming Conventions',CHECK),%IncludeWinEqu,DEFAULT(1),AT(10,,200)
#DISPLAY ('PRUNEQU.CLW is necessary for the WinAPI')
#DISPLAY ('functions used.')
#DISPLAY ('')
#PROMPT('Include WinAPI Prototypes: PRUNAPI.CLW',CHECK),%IncludeWinAPI,DEFAULT(1),AT(10,,200)
#DISPLAY ('Includes: WinExec, GetModuleFileName, CreateProcess,')
#DISPLAY ('GetExitCodeProcess, ShellExecute, and')
#DISPLAY ('GetDeskTopWindow.')
#DISPLAY ('')
#DISPLAY ('Disable this include if you already have these prototyped.')
#DISPLAY ('Add additional WinAPI prototypes to this file as needed.')
#ENDBOXED
#AT(%GlobalData)
  #IF(%GlobalExternal)
GBWinCommand      CSTRING(201),EXTERNAL,DLL(dll_mode)
GBPowerRUNMode     USHORT,EXTERNAL,DLL(dll_mode)  ! 7=background; 1=foreground
GBPowerRUNWait     BYTE,EXTERNAL,DLL(dll_mode)    ! 0=No; 1=Yes
GBExecReturn      USHORT,EXTERNAL,DLL(dll_mode)
GBDocument        CSTRING(256),EXTERNAL,DLL(dll_mode)
GBOPN             CSTRING(80),EXTERNAL,DLL(dll_mode)
GBNSTR            CSTRING(10),EXTERNAL,DLL(dll_mode)
GBWaveFile        CSTRING(81),EXTERNAL,DLL(dll_mode)
  #ELSE
GBWinCommand      CSTRING(201)
GBPowerRUNMode     USHORT  ! 7=background; 1=foreground
GBPowerRUNWait     BYTE    ! 0=No; 1=Yes
GBExecReturn      USHORT
GBDocument        CSTRING(256)
GBOPN             CSTRING(80)
GBNSTR            CSTRING(10)
GBWaveFile        CSTRING(81)
  #ENDIF
#ENDAT
#GLOBALDATA
#ENDGLOBALDATA
#AT (%AfterGlobalIncludes)
#IF(%IncludeWinEqu)
   INCLUDE('PRUNEQU.CLW')
#ENDIF
#IF(%IncludePrnProp)
   INCLUDE('PRNPROP.CLW')
#ENDIF
#IF(%Target32)
GBdwCreateFlags       DWORD
GBExitCode            DWORD
GBApplicationName     CSTRING(255)
GBStartupInfo         GROUP
cb                    DWORD
lpReserved            ULONG
lpDesktop             ULONG
lpTitle               ULONG
dwX                   DWORD
dwY                   DWORD
dwXSize               DWORD
dwYSize               DWORD
dwXCountChars         DWORD
dwYCountChars         DWORD
dwFillAttribute       DWORD
dwFlags               DWORD
wShowWindow           WORD
cbReserved2           WORD
lpReserved2           ULONG
hStdInput             HANDLE
hStdOutput            HANDLE
hStdError             HANDLE
                    END
GBProcessInformation  GROUP
hProcess              HANDLE
hThread               HANDLE
dwProcessId           DWORD
dwThreadId            DWORD
                    END
#ENDIF
#ENDAT
#AT(%GlobalMap)
#IF(%IncludeWinAPI)
   include('PRUNAPI.CLW')
#ENDIF
#ENDAT
#AT(%DLLExportList)
#DECLARE(%ExpLineNumber)
  #IF(%ProgramExtension='DLL')
    #IF(NOT %GlobalExternal)
      #SET(%ExpLineNumber,'?')
$GBWinCommand @%ExpLineNumber
      #SET(%ExpLineNumber,'?')
$GBPowerRUNMode @%ExpLineNumber
      #SET(%ExpLineNumber,'?')
$GBPowerRUNWait @%ExpLineNumber
      #SET(%ExpLineNumber,'?')
$GBExecReturn @%ExpLineNumber
      #SET(%ExpLineNumber,'?')
$GBWaveFile @%ExpLineNumber
      #SET(%ExpLineNumber,'?')
$GBDocument @%ExpLineNumber
      #SET(%ExpLineNumber,'?')
$GBOPN @%ExpLineNumber
      #SET(%ExpLineNumber,'?')
$GBNSTR @%ExpLineNumber
    #ENDIF
  #ENDIF
#ENDAT
#AT (%ShipList)

The following is required by PowerRUN DOS Application Processing:

___    DOSPROC.BAT
___    DOSPROC.PIF

#ENDAT
#!----------------------------------------------------------------------------------------------
#EXTENSION (GBPowerRUNLocal, 'PowerRUN Local Extension'),PROCEDURE
#BOXED('')
#DISPLAY ('Don''t forget to add the Global Extension!')
#DISPLAY ('')
#DISPLAY ('For a background process, create a generic Window')
#DISPLAY ('Procedure with a Timer setting of 1 and a Cursor setting')
#DISPLAY ('of Wait.  Display a message in the window such as:')
#DISPLAY ('"Processing Occurring - Please Wait..."  Call the procedure')
#DISPLAY ('from another.')
#DISPLAY ('')
#DISPLAY ('Command: (leave blank if you''re using the Code Template)')
#PROMPT('',OPENDIALOG('Select a File','Program|*.EXE|Batch|*.BAT|PIF|*.PIF|All|*.*')),%GBCommand,AT(10,,182)
#DISPLAY ('Use a prefix of ! to translate the command literally.')
#DISPLAY ('IE: If you want to use/pass variables')
#BUTTON('&Execution Mode: Min, Max, Background, Wait, etc.'),AT(8,,180)
    #PROMPT('Execution &Mode: ',OPTION),%PExecutionOption,DEFAULT(7),AT(8,,200)
    #PROMPT('0 Run Hidden - User Cannot Access.',RADIO),AT(11,,190)
    #PROMPT('1 Run as a Foreground Window with app getting focus.',RADIO),AT(11,,190)
    #PROMPT('2 Run Minimized with the app getting focus.',RADIO),AT(11,,190)
    #PROMPT('3 Run Maximized with the app getting focus.',RADIO),AT(11,,190)
    #PROMPT('7 Run Minimized with the Calling app retaining focus.',RADIO),AT(11,,190)
    #PROMPT('8 Run as a Window behind the calling app.',RADIO),AT(11,,190)
    #DISPLAY('')
    #PROMPT('&Wait for Background App to Complete/Exit',CHECK),%GBWait,DEFAULT(1),AT(8,,200)
#ENDBUTTON
#ENABLE(%Target32)
#BUTTON('&CreateProcess Priority - 32bit Only'),AT(8,,180)
    #PROMPT('Priority: ',OPTION),%PRPriority,DEFAULT(2),AT(8,,200)
    #PROMPT('1 Low - Runs only when system idle.',RADIO),AT(11,,190)
    #PROMPT('2 Normal - Default',RADIO),AT(11,,190)
    #PROMPT('3 High - Can affect multi-tasking, highly responsive.',RADIO),AT(11,,190)
    #PROMPT('4 Realtime - Preempts even operating system actions.',RADIO),AT(11,,190)
    #DISPLAY('')
#ENDBUTTON
#ENDENABLE
#PROMPT('&Display PreProcess Message',CHECK),%DisplayPPMSG,AT(8,,200)
#ENABLE(%DisplayPPMSG)
    #PROMPT('&Message:',@S50),%PPMsg,DEFAULT(''),REQ,AT(,,150),PROMPTAT(8,,34)
#ENDENABLE
#PROMPT('&Display Process Completion Message',CHECK),%DisplayMSG,AT(8,,200)
#ENABLE(%DisplayMSG)
    #PROMPT('&Message:',@S50),%GBCmpMsg,DEFAULT('The Process Completed Successfully!'),REQ,AT(,,150),PROMPTAT(8,,34)
#ENDENABLE
#ENDBOXED
#AT (%BeforeAccept)
#IF(%DisplayPPMSG)
beep
case message('%PPMsg','',ICON:Question,BUTTON:Ok+BUTTON:Cancel,BUTTON:Ok,1)
   of BUTTON:Cancel
   RETURN
.
#ENDIF
#IF(sub(%GBCommand,1,1)='!')
#DECLARE(%SetCommand)
#SET(%SetCommand,sub(%GBCommand,2,130))
GBWinCommand=%SetCommand
#ELSIF(%GBCommand<>'')
GBWinCommand='%GBCommand'
#ENDIF
#EMBED(%AfterCommandSetEmbed,'After Command String is Set - GBWinCommand')
#IF(%GBCommand<>'')
    GBPowerRUNMode=sub('%PExecutionOption',1,1)
    #IF(%Target32)
        gbp#=sub('%PRPriority',1,1)
        GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + NORMAL_PRIORITY_CLASS
        case gbp#
            of 1
                GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + IDLE_PRIORITY_CLASS
            of 2
                GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + NORMAL_PRIORITY_CLASS
            of 3
                GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + HIGH_PRIORITY_CLASS
            of 4
                GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + REALTIME_PRIORITY_CLASS
        .
    #ENDIF
    #IF(%GBWait)
    GBPowerRUNWait=1
    #ELSE
    GBPowerRUNWait=0
    #ENDIF
    do GBPowerRUN
    #IF(%DisplayMSG)
    beep
    message('%GBCmpMsg','',ICON:Exclamation)
    #ENDIF
    Post(EVENT:CloseWindow)
#ENDIF
#ENDAT
#AT (%ProcedureRoutines)
GBPowerRUN      Routine
  setcursor(cursor:wait)
#IF(%Target32)
  do GBCreateProcess
  exit
#ENDIF
#IF(%Target32=0)
  GBExecReturn = WinExec(GBWinCommand,GBPowerRUNMode)
  if GBExecReturn<=32 then
      case GBExecReturn
      of 0
        beep;Message('Insufficient Memory!  Could Not Launch Command.' & clip(GBWinCommand) & |
                      '|Please notify technical support immediately!', |
                      'Critical Error Message',ICON:Exclamation)
      of 1 orof 2
        beep;Message('EXE, Batch File or PIF File Not Found:' & clip(GBWinCommand) & |
                      '|Please notify technical support immediately!', |
                      'Critical Error Message',ICON:Exclamation)
      of 24
        beep;Message('Bad length in command! Must be CSTRING!:' & clip(GBWinCommand) & |
                      '|Please notify technical support immediately!', |
                      'Critical Error Message',ICON:Exclamation)
      .
  else
      if GBPowerRUNWait
        accept
            case event()
                of EVENT:Timer
                if GetModuleFileName(GBExecReturn,GBWinCommand,80)=0
                   break
                .
            .
        .
      .
  .
  setcursor()
  exit
#ELSE
GBCreateProcess     Routine
  GBApplicationName=GBWinCommand
  GBStartupInfo.cb = size(GBStartupInfo)
  GBStartupInfo.dwFlags=STARTF_USESHOWWINDOW
  case GBPowerRUNMode
    of 0
        GBStartupInfo.wShowWindow=PRUN:SW_HIDE
    of 1
        GBStartupInfo.wShowWindow=PRUN:SW_SHOWNORMAL
    of 2
        GBStartupInfo.wShowWindow=PRUN:SW_SHOWMINIMIZED
    of 3
        GBStartupInfo.wShowWindow=PRUN:SW_SHOWMAXIMIZED
    of 7
        GBStartupInfo.wShowWindow=PRUN:SW_SHOWMINNOACTIVE
    of 8
        GBStartupInfo.wShowWindow=PRUN:SW_SHOWNA
  .
  gbi# = CreateProcess(0,address(GBApplicationName),0,0,0,GBdwCreateFlags, |
                     0,0,address(GBStartupInfo),address(GBProcessInformation))
  if gbi#=0
     beep;Message('EXE, Batch File or PIF File Not Found:' & clip(GBWinCommand) & |
          '|Please notify technical support immediately!', |
          'Critical Error Message',ICON:Exclamation)
  .
  if GBPowerRUNWait
     accept
        case event()
            of EVENT:Timer
               gbi#=GetExitCodeProcess(GBProcessInformation.hProcess,address(GBExitCode))
               if GBExitCode=0
                  break
               .
        .
     .
  .
  setcursor()
  exit
#ENDIF
#ENDAT
#!----------------------------------------------------------------------------------------------
#!GBPowerRUNCode - Code template for utilizing WinExec and CreateProcess
#!----------------------------------------------------------------------------------------------
#CODE(GBPowerRUNCode,'Setup PowerRUN Parameters and Execute')
#DISPLAY ('PowerRUN Parameters')
#DISPLAY ('(Remember to Add the Local Extension to this Procedure)')
#DISPLAY ('Command:')
#PROMPT('',OPENDIALOG('Select a File','Program|*.EXE|Batch|*.BAT|PIF|*.PIF|All|*.*')),%CGBCommand,AT(10,,197)
#DISPLAY ('Use a prefix of ! to translate the command literally.')
#DISPLAY ('IE: If you want to use/pass variables')
#!DISPLAY('')
#PROMPT('Execution &Mode: ',OPTION),%ExecutionOption,DEFAULT(1),AT(8,,200)
#PROMPT('0 Run Hidden - User Cannot Access.',RADIO),AT(11,,190)
#PROMPT('1 Run as a Foreground Window with app getting focus.',RADIO),AT(11,,190)
#PROMPT('2 Run Minimized with the app getting focus.',RADIO),AT(11,,190)
#PROMPT('3 Run Maximized with the app getting focus.',RADIO),AT(11,,190)
#PROMPT('7 Run Minimized with the Calling app retaining focus.',RADIO),AT(11,,190)
#PROMPT('8 Run as a Window behind the calling app.',RADIO),AT(11,,190)
#ENABLE(%Target32)
#PROMPT('CreateProcess Priority - 32bit Only: ',OPTION),%CPRPriority,DEFAULT(2),AT(8,,200)
#PROMPT('1 Low - Runs only when system idle.',RADIO),AT(11,,190)
#PROMPT('2 Normal - Default',RADIO),AT(11,,190)
#PROMPT('3 High - Can affect multi-tasking, highly responsive.',RADIO),AT(11,,190)
#PROMPT('4 Realtime - Preempts even operating system actions.',RADIO),AT(11,,190)
#ENDENABLE
GBPowerRUNMode=sub('%ExecutionOption',1,1)
GBPowerRUNWait=0
#IF(%Target32)
gbp#=sub('%CPRPriority',1,1)
GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + NORMAL_PRIORITY_CLASS
case gbp#
  of 1
      GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + IDLE_PRIORITY_CLASS
  of 2
      GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + NORMAL_PRIORITY_CLASS
  of 3
      GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + HIGH_PRIORITY_CLASS
  of 4
      GBdwCreateFlags=CREATE_DEFAULT_ERROR_MODE + REALTIME_PRIORITY_CLASS
.
#ENDIF
#IF(sub(%CGBCommand,1,1)='!')
#DECLARE(%CSetCommand)
#SET(%CSetCommand,sub(%CGBCommand,2,130))
GBWinCommand=%CSetCommand
#ELSIF(%CGBCommand<>'')
GBWinCommand='%CGBCommand'
#ENDIF
do GBPowerRUN
#!----------------------------------------------------------------------------------------------
#!GBShellExec - Code template for utilizing ShellExecute().
#!----------------------------------------------------------------------------------------------
#CODE(GBShellExec,'Load Document via ShellExecute()')
#DISPLAY('The PowerRUN Local Extension is NOT necessary to')
#DISPLAY('use this code template.')
#DISPLAY('')
#DISPLAY('Document/URL or mailto: or mailto:???@???.??? to send email')
#DISPLAY('add ?Subject to the end of the email to set the subject')
#PROMPT('',@s200),%GBDocument,DEFAULT('http://www.?.com'),AT(10,,190)
#PROMPT('&Show Maximized',CHECK),%GBShowMax,AT(10,,190)
#DISPLAY('Use a prefix of ! to translate the command literally.')
#DISPLAY('IE: If you want to use/pass variables')
#IF(sub(%GBDocument,1,1)='!')
#DECLARE(%SetCommand)
#SET(%SetCommand,sub(%GBDocument,2,200))
GBDocument=%SetCommand
#ELSE
GBDocument='%GBDocument'
#ENDIF
GBOPN='open'
GBNSTR=''
#IF(%GBShowMax)
ShellExecute(GetDesktopWindow(),GBOPN,GBDocument,GBNSTR,GBNSTR,PRUN:SW_SHOWMAXIMIZED)
#ELSE
ShellExecute(GetDesktopWindow(),GBOPN,GBDocument,GBNSTR,GBNSTR,PRUN:SW_SHOW)
#ENDIF
#!
#!----------------------------------------------------------------------------------------------
#!GBPlayWave - Code template for playing WAV sound files.
#!----------------------------------------------------------------------------------------------
#CODE(GBPlayWave,'Play Wave File via SndPlaySound')
#DISPLAY('')
#DISPLAY('Wave File Name:')
#PROMPT('',@s200),%GBWaveFile,DEFAULT('.wav'),AT(10,,190)
GBWaveFile='%GBWaveFile'
if GBWaveFile<>''
   GBPlayit#=SndPlaySound(GBWaveFile,1)
.
#!
#CODE(CallCWPrintEngine,'Use CW Print Engine (Report Writer)')
#!---------------------------------------------------------------------------------------
#! Template: Clarion For Windows Print Engine Template, courtesy of Jeroen van Helvoort
#!---------------------------------------------------------------------------------------
#PROMPT('Name of report library:',OPENDIALOG('Report Library','report|*.TXR')),%ReportLibrary
#PROMPT('Name of report label:',@s20),%ReportLabel
#!PROMPT('Execution &Mode: ',OPTION),%CWPrintOption,AT(8,,200)
#BUTTON('Optionlist')
  #PROMPT('Use a password',CHECK),%UsePassword,DEFAULT(%FALSE)
  #ENABLE(%UsePassword)
    #PROMPT('Password:',@s20),%Password
  #ENDENABLE
  #PROMPT('Preview',CHECK),%UsePreview,DEFAULT(%TRUE)
  #ENABLE(%UsePreview)
    #PROMPT('Pages to preview at once:',@n_3),%HowManyPages,DEFAULT(0)
    #DISPLAY('(0 is preview all at once)')
  #ENDENABLE
  #PROMPT('Specify printer',CHECK),%UsePrinter,DEFAULT(%FALSE)
  #ENABLE(%UsePrinter)
    #PROMPT('Windows printer name',@s30),%WindowsPrinterName
  #ENDENABLE
  #BUTTON('Runtime Variables'),MULTI(%RunTimeVariables,%RwRunTimeVariable & ' = ' & %AppRuntimeVariable)
    #PROMPT('Runtime variable (Report)',@s30),%RwRuntimeVariable
    #PROMPT('Runtime variable (App)',FIELD),%AppRuntimeVariable
  #ENDBUTTON
  #PROMPT('Use report INI file',CHECK),%UseReportINIfile,DEFAULT(%FALSE)
  #ENABLE(%UseReportINIFile)
    #PROMPT('INI file name:',OPENDIALOG('Report INI file','INI|*.INI')),%ReportINIFile
  #ENDENABLE
#ENDBUTTON
#!---------------------------------------------------------------------------------------
#! Generated Code
#!---------------------------------------------------------------------------------------
#LOCALDATA
CWPrintString   STRING(100)
#ENDLOCALDATA
  #DECLARE(%OptionList)
  #SET(%OptionList,'')
  #IF(%UsePassword)
    #SET(%OptionList,'/P' & %Password)
  #ENDIF
  #IF(%UsePreview)
    #IF(%HowManyPages=0)
      #SET(%OptionList,%OptionList & ' /W')
    #ELSE
      #SET(%OptionList,%OptionList & ' /W' & %HowManyPages)
    #ENDIF
  #ENDIF
  #IF(%UsePrinter)
    #SET(%OptionList,%OptionList & ' /D' & %WindowsPrinterName)
  #ENDIF
  #IF(%UseReportINIfile)
    #SET(%OptionList,%OptionList & ' /I' & %ReportINIFile)
  #ENDIF
  %Window{PROP:Cursor} = CURSOR:Wait
  CWPrintString = 'CWPrint %ReportLibrary %ReportLabel %OptionList'
  #FOR(%RunTimeVariables)
  CWPrintString = CLIP(CWPrintString) & ' /V%RwRunTimeVariable="' &CLIP(%AppRuntimeVariable)& '"'
  #ENDFOR
  GBPowerRUNMode=1
  GBPowerRUNWait=1
  GBWinCommand=CWPrintString
  do GBPowerRUN
  !RUN(CWPrintString)
  IF ERROR() THEN STOP(ERROR()).
  %Window{PROP:Cursor} = CURSOR:Arrow
#!---------------------------------------------------------------------------------------
#CODE(CallCWPrintEngine2,'Use CW Print Engine (Report Writer)')
#!---------------------------------------------------------------------------------------
#! Template: Clarion For Windows Print Engine Template, courtesy of Jeroen van Helvoort
#!---------------------------------------------------------------------------------------
#! Template: Clarion For Windows Print Engine Template 2003
#! Author  : Jeroen van Helvoort (C)opyright 1997
#! E-Mail  : 106230.2744@compuserve.com
#! Date    : February 22, 1997
#! Warning : You can you use this template at your own risk.
#!           I'am NOT responsible for any kind of damages/loss, etc.
#! -----------------------------------------------------------------------------
#! Template Input
#! -----------------------------------------------------------------------------
#PROMPT('Name of report library:',OPENDIALOG('Report Library','report|*.TXR')),%ReportLibrary
#PROMPT('Name of report label:',@s20),%ReportLabel
#BUTTON('Optionlist')
  #BOXED('Print Engine Optionlist')
    #PROMPT('Use a password',CHECK),%UsePassword,DEFAULT(%FALSE)
    #ENABLE(%UsePassword)
      #PROMPT('Password:',@s40),%Password
    #ENDENABLE
    #PROMPT('Preview',CHECK),%UsePreview,DEFAULT(%TRUE)
    #ENABLE(%UsePreview)
      #PROMPT('Pages to preview at once:',@s40),%HowManyPages,DEFAULT(0)
      #DISPLAY('(0 is preview all at once)')
    #ENDENABLE
    #PROMPT('Number of copies',@s40),%CWPRCopies,DEFAULT(0)
    #PROMPT('INI file name:',OPENDIALOG('Report INI file','INI|*.INI')),%ReportINIFile
  #ENDBOXED
  #DISPLAY('Use a ! when making use of variables.')
  #DISPLAY('e.g. !LOC:Copies')
  #SHEET
  #TAB('Runtime Variables')
    #BUTTON('Runtime Variables'),MULTI(%RunTimeVariables,%RwRunTimeVariable & ' = ' & %AppRuntimeVariable),INLINE
      #PROMPT('Runtime variable (Report)',@s30),%RwRuntimeVariable
      #PROMPT('Runtime variable (App)',FIELD),%AppRuntimeVariable
    #ENDBUTTON
  #ENDTAB
  #TAB('Printer setup')
    #PROMPT('Ask for Printer Setup',CHECK),%CWPRPrinterSetup
    #ENABLE(%CWPRPrinterSetup=%True)
      #PROMPT('Text in title bar',@s40),%WindowsPrinterTitle
    #ENDENABLE
    #ENABLE(%CWPRPrinterSetup=%False)
      #PROMPT('Windows printer name',@s40),%WindowsPrinterName
    #ENDENABLE
    #DISPLAY('(Double quotes will be automatically included)')
  #ENDTAB
  #TAB('Page range')
    #PROMPT('Start page',@s40),%CWPRStartPage
    #PROMPT('End page',@s40),%CWPREndPage
    #DISPLAY('')
    #BOXED('Warning!')
      #DISPLAY('A 0 start page will result in showing no pages.')
      #DISPLAY('A 0 end page will result in showing all pages.')
      #DISPLAY('No input will result in showing all pages.')
    #ENDBOXED
  #ENDTAB
  #ENDSHEET
  #IMAGE('CWPRINT.ICO'),AT(225,70)
#ENDBUTTON
#DISPLAY('(C)opyright 1997 by Jeroen van Helvoort')
#DISPLAY('Permission granted by Jeroen van Helvoort')
#DISPLAY('to include with PowerRUN.')
#IMAGE('CWPRINT.ICO'),AT(150,35)
#!
#! =============================================================================
#! Generated Code
#! =============================================================================
#!
#DECLARE(%CWPRVariable)
#LOCALDATA
CWPrintString   STRING(100)
#ENDLOCALDATA
#!
CWPrintString = 'CWPrint %ReportLibrary %ReportLabel'
#! ----------------------------------------------------------------------------
#! Password
#! ----------------------------------------------------------------------------
#IF(%UsePassword)
  #IF(SUB(%Password,1,1) = '!')
    #SET(%CWPRVariable,SUB(%Password,2,LEN(CLIP(%Password))))
CWPrintString = CLIP(CWPrintString) & ' /P' & %CWPRVariable
  #ELSE
CWPrintString = CLIP(CWPrintString) & ' /P%Password'
  #ENDIF
#ENDIF
#! ----------------------------------------------------------------------------
#! Preview
#! ----------------------------------------------------------------------------
#IF(%UsePreview)
  #IF(SUB(%HowManyPages,1,1) = '!')
    #SET(%CWPRVariable,SUB(%HowManyPages,2,LEN(CLIP(%HowManyPages))))
CWPrintString = CLIP(CWPrintString) & ' /W' & %CWPRVariable
  #ELSE
    #IF(%HowManyPages=0)
CWPrintString = CLIP(CWPrintString) & ' /W'
    #ELSE
CWPrintString = CLIP(CWPrintString) & ' /W%HowManyPages'
    #ENDIF
  #ENDIF
#ENDIF
#! ----------------------------------------------------------------------------
#! Windows Printer Name
#! ----------------------------------------------------------------------------
#IF(%CWPRPrinterSetup = %False)
  #IF(%WindowsPrinterName <> '')
    #IF(SUB(%WindowsPrinterName,1,1) = '!')
      #SET(%CWPRVariable,SUB(%WindowsPrinterName,2,LEN(CLIP(%WindowsPrinterName))))
CWPrintString = CLIP(CWPrintString) & ' /D"' & %CWPRVariable & '"'
    #ELSE
CWPrintString = CLIP(CWPrintString) & ' /D"%WindowsPrinterName"'
    #ENDIF
  #ENDIF
#ELSE
  #IF(%WindowsPrinterTitle <> '')
    #IF(SUB(%WindowsPrinterTitle,1,1) = '!')
      #SET(%CWPRVariable,SUB(%WindowsPrinterTitle,2,LEN(CLIP(%WindowsPrinterTitle))))
CWPrintString = CLIP(CWPrintString) & ' /D?"' & %CWPRVariable & '"'
    #ELSE
CWPrintString = CLIP(CWPrintString) & ' /D?"%WindowsPrinterTitle"'
    #ENDIF
  #ELSE
CWPrintString = CLIP(CWPrintString) & ' /D?'
  #ENDIF
#ENDIF
#! ----------------------------------------------------------------------------
#! Page Range
#! ----------------------------------------------------------------------------
#IF(%CWPRStartPage<>'' AND %CWPREndPage<>'')
  #IF(SUB(%CWPRStartPage,1,1) = '!')
      #SET(%CWPRVariable,SUB(%CWPRStartPage,2,LEN(CLIP(%CWPRStartPage))))
CWPrintString = CLIP(CWPrintString) & ' /R' & %CWPRVariable & '-'
  #ELSE
CWPrintString = CLIP(CWPrintString) & ' /R%CWPRStartPage-'
  #ENDIF
  #IF(SUB(%CWPREndPage,1,1) = '!')
    #SET(%CWPRVariable,SUB(%CWPREndPage,2,LEN(CLIP(%CWPREndPage))))
CWPrintString = CLIP(CWPrintString) & %CWPRVariable
    #ELSE
CWPrintString = CLIP(CWPrintString) & '%CWPREndPage'
  #ENDIF
#ENDIF
#! ----------------------------------------------------------------------------
#! Copies
#! ----------------------------------------------------------------------------
#IF(SUB(%CWPRCopies,1,1) = '!')
  #SET(%CWPRVariable,SUB(%CWPRCopies,2,LEN(CLIP(%CWPRCopies))))
CWPrintString = CLIP(CWPrintString) & ' /C' & %CWPRVariable
#ELSE
  #IF(%CWPRCopies > 0)
CWPrintString = CLIP(CWPrintString) & ' /C%CWPRCopies'
  #ENDIF
#ENDIF
#! ----------------------------------------------------------------------------
#! INI File
#! ----------------------------------------------------------------------------
#IF(%ReportINIfile <> '')
  #IF(SUB(%ReportINIFile,1,1) = '!')
    #SET(%CWPRVariable,SUB(%ReportINIFile,1,LEN(CLIP(%ReportINIFile))))
CWPrintString = CLIP(CWPrintString) & ' /I' & %CWPRVariable
  #ELSE
CWPrintString = CLIP(CWPrintString) & ' /I%ReportINIFile'
  #ENDIF
#ENDIF
#! ----------------------------------------------------------------------------
#! Runtime Variables
#! ----------------------------------------------------------------------------
  #FOR(%RunTimeVariables)
CWPrintString = CLIP(CWPrintString) & ' /V%RwRunTimeVariable="' &CLIP(%AppRuntimeVariable)& '"'
  #ENDFOR
#! ----------------------------------------------------------------------------
#! Run CWPRINT
#! ----------------------------------------------------------------------------
!RUN(CWPrintString)
GBPowerRUNMode=1
GBPowerRUNWait=1
GBWinCommand=CWPrintString
do GBPowerRUN

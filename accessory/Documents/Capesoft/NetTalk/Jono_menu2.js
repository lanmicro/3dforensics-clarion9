/**
 * fw_menu 24OCT2000 Version 4.0
 * John Ahlquist, October 2000
 * Copyright (c) 2000 Macromedia, Inc.
 *
 * based on menu.js
 * by gary smith, July 1997
 * Copyright (c) 1997-1999 Netscape Communications Corp.
 *
 * Netscape grants you a royalty free license to use or modify this
 * software provided that this copyright notice appears on all copies.
 * This software is provided "AS IS," without a warranty of any kind.
 */

function fwLoadMenus() {
  if (window.fw_menu_0) return;

  // blank menu 
  window.fw_menu_0 = new Menu("root",1,1,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_0.fontWeight="normal";
  fw_menu_0.hideOnMouseOut=true;
  

  window.fw_menu_1 = new Menu("root",190,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_1.addMenuItem("<b><font color=#000000> Learning NetTalk </font></b>","");
  fw_menu_1.addMenuItem("<img src=images/recommended.gif> I'm new to NetTalk","location='nettalk.htm#New to NetTalk'");
  fw_menu_1.addMenuItem("<img src=images/recommended.gif> Introduction to NetTalk","location='nettalk.htm#Introduction'");
  fw_menu_1.addMenuItem("<img src=images/recommended.gif> <b>Jump Starts </b>","location='nettalk.htm#Jump Starts'");
  fw_menu_1.addMenuItem("<img src=images/recommended.gif> <b>Examples</b>","location='nettalk.htm#Examples'");
  fw_menu_1.addMenuItem("<img src=images/recommended.gif> Object Overview","location='nettalk.htm#Object Overview'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Copyright, licence & distribution","location='nettalk.htm#Copyright distribution and license'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Where can you get NetTalk from","location='nettalk.htm#Where can you get it'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Installation instructions","location='nettalk.htm#Installing'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetTalk Background","location='nettalk.htm#NetTalk background'");
  fw_menu_1.addMenuItem("<img src=images/recommended.gif> What is a Client and a Server?","location='nettalk.htm#Client Server'");
  fw_menu_1.addMenuItem("<img src=images/recommended.gif> <b>Adding NetTalk to your app</b>","location='nettalk.htm#Using NetTalk in your application'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Introduction to TCP/IP","location='nettalk.htm#Intro to TCP/IP'");
  fw_menu_1.fontWeight="normal";
  fw_menu_1.hideOnMouseOut=true;         
  
  window.fw_menu_2_1 = new Menu("<img src=images/recommended.gif> <b>Control Templates</b>",250,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_2_1.addMenuItem("<b><font color=#000000>Control Templates</font></b>","");
  fw_menu_2_1.addMenuItem("<img src=images/recommended.gif> Chat Controls (Jump Start)","location='NetTalk2.htm#ChatControls'");
  fw_menu_2_1.addMenuItem("<img src=images/recommended.gif> CloseApp Controls (Jump Start)","location='NetTalk2.htm#CloseAppControls'");
  fw_menu_2_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;UseRemoteMachine Controls (Jump Start)","location='NetTalk2.htm#UseRemoteMachineControls'");
  fw_menu_2_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Using the NetDIP (Dynamic IP Manager) object","location='NetTalk2.htm#The NetDIP Object'");
  fw_menu_2_1.addMenuItem("<img src=images/recommended.gif> &nbsp;&nbsp;NetDIP (Jump Start)","location='NetTalk2.htm#Jump Start NetDIP'");
  fw_menu_2_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NetDIP Hand Code Example","location='NetTalk2.htm#NetDIP Example Code'");
  fw_menu_2_1.addMenuItem("<img src=images/recommended.gif> SendEmail Control Template","location='NetTalk3.htm#The Send Email Control Template'");
  fw_menu_2_1.fontWeight="normal";
  fw_menu_2_1.hideOnMouseOut=true;
  
  window.fw_menu_2_2 = new Menu("&nbsp;&nbsp;&nbsp;&nbsp;<b>Global Templates</b>",180,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_2_2.addMenuItem("<b><font color=#000000>Global Templates</font></b>","");
  fw_menu_2_2.addMenuItem("NetTalk Global Templates &#149","location='NetTalk2.htm#Global Templates'");
  fw_menu_2_2.addMenuItem("Use_<b>NetRefresh</b> Global Template","location='NetTalk2.htm#NetRefresh'");
  fw_menu_2_2.fontWeight="normal";
  fw_menu_2_2.hideOnMouseOut=true;
  
  window.fw_menu_2_3 = new Menu("<img src=images/recommended.gif> <b>Programming Scenarios</b>",230,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_2_3.addMenuItem("<b><font color=#000000>Programming Scenarios</font></b>","");
  fw_menu_2_3.addMenuItem("<img src=images/recommended.gif> Scenario One-Single Packet Client/Server","location='NetTalk2.htm#Scenario One'");
  fw_menu_2_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Scenario Two-Broadcasting Client/Client","location='NetTalk2.htm#Scenario Two'");
  fw_menu_2_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Scenario Three-Multi-Packet Client/Server","location='NetTalk2.htm#Scenario Three'");
  fw_menu_2_3.fontWeight="normal";
  fw_menu_2_3.hideOnMouseOut=true;
  
  window.fw_menu_2_4 = new Menu("<img src=images/recommended.gif> <b>Ready Built Objects</b>",130,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_2_4.addMenuItem("<b><font color=#000000>Ready Built Objects</font></b>","");
  fw_menu_2_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetChat","location='NetTalk2.htm#Using the Other NetTalk Objects'");
  fw_menu_2_4.addMenuItem("<img src=images/recommended.gif> NetCloseApps","location='NetTalk2.htm#Using the Other NetTalk Objects'");
  fw_menu_2_4.addMenuItem("<img src=images/recommended.gif> NetRefresh","location='NetTalk2.htm#Using the Other NetTalk Objects'");
  fw_menu_2_4.fontWeight="normal";
  fw_menu_2_4.hideOnMouseOut=true;

  window.fw_menu_2_5 = new Menu("&nbsp;&nbsp;&nbsp;&nbsp;<b>DLL Functions</b>",140,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_2_5.addMenuItem("<b><font color=#000000>DLL Functions</font></b>","");
  fw_menu_2_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetGetNetName","location='NetTalk2.htm#NetGetNetName'");
  fw_menu_2_5.addMenuItem("<img src=images/recommended.gif> NetOptions","location='NetTalk2.htm#NetOptions'");
  fw_menu_2_5.addMenuItem("<img src=images/recommended.gif> NetGetIPInfo","location='NetTalk2.htm#NetGetIPInfo'");
  fw_menu_2_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetAutoRemote","location='NetTalk2.htm#NetAutoRemote'");
  fw_menu_2_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetAutoSendBroadCast","location='NetTalk2.htm#NetAutoSendBroadCast'");
  fw_menu_2_5.fontWeight="normal";
  fw_menu_2_5.hideOnMouseOut=true;

  window.fw_menu_2_6 = new Menu("&nbsp;&nbsp;&nbsp;&nbsp;<b>Advanced Topics</b>",270,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_2_6.addMenuItem("<b><font color=#000000>Advanced Topics</font></b>","");
  fw_menu_2_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Configuring firewalls and proxy servers for NetTalk","location='NetTalk2.htm#Configuring Firewalls and Proxy Servers for NetTalk Ports'");
  fw_menu_2_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Inside NetAuto - How it all works","location='NetTalk2.htm#Inside NetTalk'");
  fw_menu_2_6.fontWeight="normal";
  fw_menu_2_6.hideOnMouseOut=true;

  window.fw_menu_2 = new Menu("root",200,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_2.addMenuItem("<b><font color=#000000>NetTalk Objects and Protocol</font></b>","");
  fw_menu_2.addMenuItem(fw_menu_2_1);
  //fw_menu_2.addMenuItem(fw_menu_2_2);
  fw_menu_2.addMenuItem("<img src=images/recommended.gif> The NetTalk objects","location='NetTalk2.htm#Programming - The NetTalk Protocol and the NetTalk Objects'");
  fw_menu_2.addMenuItem(fw_menu_2_3);
  fw_menu_2.addMenuItem(fw_menu_2_4);
  fw_menu_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_2.addMenuItem("<img src=images/recommended.gif> Handy programming tips","location='NetTalk2.htm#Handy Reference and Tips'");
  fw_menu_2.addMenuItem("<img src=images/recommended.gif> What to set to send (as a client) ","location='NetTalk2.htm#What data must I set before sending a packet'");
  fw_menu_2.addMenuItem("<img src=images/recommended.gif> What to set to reply (client/server)","location='NetTalk2.htm#What data must I set before auto replying to a packet'");
  fw_menu_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;How do I suppress error messages","location='NetTalk2.htm#How do I suppress error messages'");
  fw_menu_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Changing the error messages","location='NetTalk2.htm#Changing the Error Messages'");
  fw_menu_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_2.addMenuItem("<img src=images/recommended.gif> Object <b>properties &amp; methods </b>","location='NetTalk2.htm#NetTalk Object Properties &amp; Methods'");
  fw_menu_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Object <b>data structures</b>","location='NetTalk2.htm#Data Structures'");
  fw_menu_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetTalk <b>equates</b>","location='NetTalk2.htm#NetTalk Equates'");
  fw_menu_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_2.addMenuItem(fw_menu_2_5);
  fw_menu_2.addMenuItem(fw_menu_2_6);
  fw_menu_2.fontWeight="normal";
  fw_menu_2.hideOnMouseOut=true; 
  //fw_menu_2.childMenuIcon="arrows.png";

  window.fw_menu_3_1 = new Menu("<img src=images/recommended.gif> <b>NetSimple Object</b>",240,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_3_1.addMenuItem("<b><font color=#000000>NetSimple</font></b>","");
  fw_menu_3_1.addMenuItem("<img src=images/recommended.gif> NetSimple Object - talking directly to a port","location='NetTalk3.htm#NetSimple Object - Using NetSimple to talk directly to a Port'");
  fw_menu_3_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Using NetSimple as a Client or a Server","location='NetTalk3.htm#How to use NetSimple as a Client or a Server'");
  fw_menu_3_1.addMenuItem("<img src=images/recommended.gif> NetSimple (Jump Start)","location='NetTalk3.htm#NetSimpleJumpStart'");
  fw_menu_3_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetSimple <b>Methods</b>","location='NetTalk3.htm#NetSimple Methods'");
  fw_menu_3_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetSimple <b>Properties</b>","location='NetTalk3.htm#NetSimple Properties'");
  fw_menu_3_1.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetSimple Equates &amp; Structures","location='NetTalk3.htm#NetSimple Equates'");
  fw_menu_3_1.fontWeight="normal";
  fw_menu_3_1.hideOnMouseOut=true;

  window.fw_menu_3_2 = new Menu("&nbsp;&nbsp;&nbsp;&nbsp;<b>MultiClient</b>",180,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_3_2.addMenuItem("<b><font color=#000000>NetSimpleMultiClient</font></b>","");
  fw_menu_3_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetSimpleMultiClient Object","location='NetTalk3.htm#NetSimpleMultiClient'");
  fw_menu_3_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetSimpleMultiClient <b>Methods</b>","location='NetTalk3.htm#NetSimpleMultiClient Methods'");
  fw_menu_3_2.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetSimpleMultiClient <b>Properties</b>","location='NetTalk3.htm#NetSimpleMultiClient Properties'");
  fw_menu_3_2.fontWeight="normal";
  fw_menu_3_2.hideOnMouseOut=true;

  window.fw_menu_3_3 = new Menu("<img src=images/recommended.gif> <b>Email</b>",200,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_3_3.addMenuItem("<b><font color=#000000>Email</font></b>","");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;The Two Email objects","location='NetTalk3.htm#The NetEmailSend and NetEmailReceive Objects'");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_3_3.addMenuItem("<b><font color=#000000>Email Send</font></b>","");
  fw_menu_3_3.addMenuItem("<img src=images/recommended.gif> The Send Email control template","location='NetTalk3.htm#The Send Email Control Template'");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetEmailSend <b>Methods</b>","location='NetTalk3.htm#The NetEmailSend Methods'");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetEmailSend <b>Properties</b>","location='NetTalk3.htm#The NetEmailSend Properties'");
  fw_menu_3_3.addMenuItem("<img src=images/recommended.gif> Sending Emails (Jump Start)","location='NetTalk3.htm#NetEmailSend Example Code'");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_3_3.addMenuItem("<b><font color=#000000>Email Receive</font></b>","");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetEmailReceive <b>Methods</b>","location='NetTalk3.htm#The NetEmailReceive Methods'");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetEmailReceive <b>Properties</b>","location='NetTalk3.htm#The NetEmailReceive Properties'");
  fw_menu_3_3.addMenuItem("<img src=images/recommended.gif> Receiving Emails (Jump Start)","location='NetTalk3.htm#NetEmailReceive Examples'");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_3_3.addMenuItem("<b><font color=#000000>Email Tips</font></b>","");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Tip: Writing an Email Newsletter Application","location='NetTalk3.htm#Tips for Writing a Email Newsletter Program'");
  fw_menu_3_3.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Tip: Sending PDF reports via Email (PDFExchange)","location='NetTalk3.htm#PDFExchange'");
  fw_menu_3_3.fontWeight="normal";
  fw_menu_3_3.hideOnMouseOut=true;

  window.fw_menu_3_4 = new Menu("&nbsp;&nbsp;&nbsp;&nbsp;<b>News</b>",160,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_3_4.addMenuItem("<b><font color=#000000>News</font></b>","");
  fw_menu_3_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;The Two News objects","location='NetTalk3.htm#News Objects'");
  fw_menu_3_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_3_4.addMenuItem("<b><font color=#000000>News Receive</font></b>","");
  fw_menu_3_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetNewsReceive (extended) Properties","location='NetTalk3.htm#NetNewsReceive Properties'");
  fw_menu_3_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_3_4.addMenuItem("<b><font color=#000000>News Send</font></b>","");
  fw_menu_3_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetNewsSend (extended) Properties","location='NetTalk3.htm#NetNewsSend Properties'");
  fw_menu_3_4.fontWeight="normal";
  fw_menu_3_4.hideOnMouseOut=true;

  window.fw_menu_3_5 = new Menu("<img src=images/recommended.gif> <b>Web</b>",200,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_3_5.addMenuItem("<b><font color=#000000>Web Client</font></b>","");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;The NetWebClient Object","location='NetTalk3.htm#NetWebClient Object'");
  fw_menu_3_5.addMenuItem("<img src=images/recommended.gif> Web (Jump Start)","location='NetTalk3.htm#Jump Start for Web'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Posting Data to a web page form","location='NetTalk3.htm#Posting Data to a Web page Form'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetWebClient <b>Methods</b>","location='NetTalk3.htm#NetWebClient Methods'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetWebClient <b>Properties</b>","location='NetTalk3.htm#NetWebClient Properties'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_3_5.addMenuItem("<b><font color=#000000>Web Server</font></b>","");
  fw_menu_3_5.addMenuItem("<img src=images/recommended.gif> Basic Techniques","location='NetTalkWebBasic.htm'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Template Reference","location='NetTalkWebFunctionality.htm'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Using Reports","location='NetWebReports.htm'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Building Secure (SSL) Web Sites","location='NetTalkWebSecure.htm'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Deploying a Web server","location='NetTalkWebDeployment.htm'");  
  fw_menu_3_5.addMenuItem("<img src=images/recommended.gif> WebServer FAQ","location='NetTalkWebFAQ.htm'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Selecting a Tool for Building Web Sites","location='NetTalkWebTool.htm'");
  fw_menu_3_5.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Pre-Sales FAQ","location='NetTalkWebPreFAQ.htm'");
  fw_menu_3_5.fontWeight="normal";
  fw_menu_3_5.hideOnMouseOut=true;

  window.fw_menu_3_6 = new Menu("<img src=images/recommended.gif> <b>FTP</b>",200,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_3_6.addMenuItem("<b><font color=#000000>FTP Client</font></b>","");
  fw_menu_3_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;The Two FTP Objects","location='NetTalk3.htm#The NetFtpClientControl and NetFTPClientData Object'");
  fw_menu_3_6.addMenuItem("<img src=images/recommended.gif> The FTP Control Template (New)","location='NetTalk3.htm#The FTP Control Template'");
  fw_menu_3_6.addMenuItem("<img src=images/recommended.gif> FTP (Jump Start)","location='NetTalk3.htm#Jump Start FTP'");
  fw_menu_3_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetFTPClientControl <b>Methods</b>","location='NetTalk3.htm#The NetFTPClientControl Methods'");
  fw_menu_3_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetFTPClientControl <b>Properties</b>","location='NetTalk3.htm#The NetFTPClientControl Properties'");
  fw_menu_3_6.fontWeight="normal";
  fw_menu_3_6.hideOnMouseOut=true;

  window.fw_menu_3_7 = new Menu("<img src=images/recommended.gif> <b>SNMP</b>",180,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_3_7.addMenuItem("<b><font color=#000000>SNMP</font></b>","");
  fw_menu_3_7.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;The SNMP Objects","location='NetSNMP.htm#The NetSNMP Object'");
  fw_menu_3_7.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;SNMP (Jump Start)","location='NetSNMP.htm#Jump Start SNMP'");
  fw_menu_3_7.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;SNMP <b>Equates & Structures</b>","location='NetSNMP.htm#The NetSNMP Equates &amp; Structures'");
  fw_menu_3_7.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;SNMP <b>Methods</b>","location='NetSNMP.htm#The NetSNMP Methods &amp; Properties'");
  fw_menu_3_7.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;SNMP <b>Properties</b>","location='NetSNMP.htm#The NetSNMP Properties'");
  fw_menu_3_7.fontWeight="normal";
  fw_menu_3_7.hideOnMouseOut=true;

  window.fw_menu_3 = new Menu("root",140,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_3.addMenuItem("<b><font color=#000000>NetSimple Objects</font></b>","");
  fw_menu_3.addMenuItem(fw_menu_3_1);
  fw_menu_3.addMenuItem(fw_menu_3_2);
  fw_menu_3.addMenuItem(fw_menu_3_3);
  fw_menu_3.addMenuItem(fw_menu_3_4);
  fw_menu_3.addMenuItem(fw_menu_3_5);
  fw_menu_3.addMenuItem(fw_menu_3_6);
  fw_menu_3.addMenuItem(fw_menu_3_7);
  fw_menu_3.fontWeight="normal";
  fw_menu_3.hideOnMouseOut=true;
  //fw_menu_3.childMenuIcon="arrows.png";

  window.fw_menu_4 = new Menu("root",180,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_4.addMenuItem("<b><font color=#000000>NetDUN (Dial-Up)</font></b>","");
  fw_menu_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetDUN - Dial-Up functionality","location='NetTalk4.htm#NetDUN Object'");
  fw_menu_4.addMenuItem("<img src=images/recommended.gif> NetDUN (Jump Start)","location='NetTalk4.htm#NetDUN Jump Start'");
  fw_menu_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetDUN <b>Properties</b>","location='NetTalk4.htm#NetDUN Properties'");
  fw_menu_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetDUN <b>Methods</b>","location='NetTalk4.htm#NetDUN Methods'");
  fw_menu_4.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;NetDUN Data Structures","location='NetTalk4.htm#NetDUN Structures'");
  fw_menu_4.fontWeight="normal";
  fw_menu_4.hideOnMouseOut=true;
  //fw_menu_4.childMenuIcon="arrows.png";

  window.fw_menu_6 = new Menu("root",250,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_6.addMenuItem("<b><font color=#000000>Support</font></b>","");
  fw_menu_6.addMenuItem("<img src=images/recommended.gif> How to get Support (<b>Read this first</b>)","location='NetTalkSupport.htm#Support'");
  fw_menu_6.addMenuItem("<img src=images/recommended.gif> <b>Top 10</b> Questions","location='NetTalkSupport.htm#Top10'");
  fw_menu_6.addMenuItem("<img src=images/recommended.gif> <b>FAQ</b> (Frequently Asked Question)","location='NetTalkSupport.htm#FAQ'");
  fw_menu_6.addMenuItem("<img src=images/recommended.gif> Error Messages","location='NetTalkSupport.htm#Errors'");
  fw_menu_6.addMenuItem("<img src=images/recommended.gif> LOG files - How to create LOG files","location='NetTalkSupport.htm#How to create NetTalk Log Files'");
  fw_menu_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;<b>Multi-DLL</b> applications and NetTalk","location='NetTalkSupport.htm#MultiDLL'");
  fw_menu_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;<b>Legacy</b> applications and NetTalk","location='NetTalkSupport.htm#Legacy Applications'");
  fw_menu_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Web Builder applications and NetTalk","location='NetTalkSupport.htm#WebBuilder and NetTalk'");
  fw_menu_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;How to check TCP/IP is on a machine","location='NetTalkTCP.htm'");
  fw_menu_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Glossary","location='NetTalkSupport.htm#Glossary'");
  fw_menu_6.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_6.addMenuItem("<img src=images/recommended.gif> Submit a Question to CapeSoft Support","location='NetTalkSupportRequest.htm#SubmitQuestion'");
  fw_menu_6.fontWeight="normal";
  fw_menu_6.hideOnMouseOut=true;
  //fw_menu_6.childMenuIcon="arrows.png";

  window.fw_menu_7 = new Menu("root",120,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_7.addMenuItem("<b><font color=#000000>Version History</font></b>","location='NetTalkHistory.htm'");
  fw_menu_7.addMenuItem("<img src=images/recommended.gif> Version History","location='NetTalkHistory.htm#Features History'");
  fw_menu_7.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Future Ideas","location='NetTalkHistory.htm#What will NetTalk do in the future'");
  fw_menu_6.fontWeight="normal";
  fw_menu_6.hideOnMouseOut=true;
  //fw_menu_6.childMenuIcon="arrows.png";

  window.fw_menu_8 = new Menu("root",180,15,"Tahoma,Arial, Helvetica, sans-serif",11,"#2B4F91","#2B4F91","#FFFFFF","#EBF4FE");
  fw_menu_8.addMenuItem("<b><font color=#000000>Common Features</font></b>","");
  fw_menu_8.addMenuItem("<img src=images/recommended.gif> NetTalk <b>Global Templates</b>","location='NetTalk2.htm#Global Templates'");
  fw_menu_8.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Use_<b>NetRefresh</b> Global Template","location='NetTalk2.htm#NetRefresh'");
  fw_menu_8.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_8.addMenuItem("<img src=images/recommended.gif> Adding NetTalk to your app","location='nettalk.htm#Using NetTalk in your application'");
  fw_menu_8.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;————————————————","");
  fw_menu_8.addMenuItem("<img src=images/recommended.gif> <b>NetOptions</b> - DLL function","location='NetTalk2.htm#NetOptions'");
  fw_menu_8.addMenuItem("<img src=images/recommended.gif> <b>NetGetIPInfo</b> - DLL function","location='NetTalk2.htm#NetGetIPInfo'");
  fw_menu_8.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Introduction to TCP/IP","location='nettalk.htm#Intro to TCP/IP'");
  fw_menu_8.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;For Fun","location='nettalk.htm#For Fun'");
  fw_menu_8.fontWeight="normal";
  fw_menu_8.hideOnMouseOut=true; 
  //fw_menu_8.childMenuIcon="arrows.png";


  // Only do this next line once
  fw_menu_1.writeMenus();         

} 

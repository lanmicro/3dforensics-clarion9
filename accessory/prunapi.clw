  MODULE('')
    WinExec(*CSTRING,USHORT),RAW,USHORT,PASCAL ! For straightforward execute
               ! use can now use Clarion RUN
      OMIT('***',_WIDTH32_)
    GetModuleFileName(HINSTANCE, *LPSTR, SIGNED),SIGNED,PASCAL,RAW
    ***
    COMPILE('***',_WIDTH32_)
    GetModuleFileName(HINSTANCE,*LPSTR,DWORD),DWORD,PASCAL,RAW,NAME('GetModuleFileNameA')
     ***
    COMPILE('***',_WIDTH32_)
    GetExitCodeProcess (HANDLE hProcess,ULONG lpExitCode),BOOL,PASCAL
     ***
    COMPILE('***',_WIDTH32_)
    CreateProcess (ULONG lpApplicationName, |
      ULONG lpCommandLine, |
      ULONG lpProcessAttributes, |
      ULONG lpThreadAttributes, |
      BOOL bInheritHandles, |
      DWORD dwCreationFlags, |
      LPVOID lpEnvironment, |
      ULONG lpCurrentDirectory, |
      ULONG lpStartupInfo, |
      ULONG lpProcessInformation |
      ),BOOL,RAW,PASCAL,NAME('CreateProcessA')
    ***
    GetDesktopWindow(),UNSIGNED,PASCAL
    OMIT('***',_WIDTH32_)
    ShellExecute(UNSIGNED,*CSTRING,*CSTRING,*CSTRING,*CSTRING,SIGNED),UNSIGNED,PASCAL,RAW,PROC
    ***
    COMPILE('***',_WIDTH32_)
    ShellExecute(UNSIGNED,*CSTRING,*CSTRING,*CSTRING,*CSTRING,SIGNED),UNSIGNED,PASCAL,RAW,PROC,NAME('ShellExecuteA')
    ***
    OMIT('***',_WIDTH32_)
    sndPlaySound(*LPCSTR,UNSIGNED),BOOL,PROC,pascal,raw
    ***
    COMPILE('***',_WIDTH32_)
    sndPlaySound(*LPCSTR,UNSIGNED),BOOL,PROC,pascal,raw,NAME('sndPlaySoundA')
    ***
 END
